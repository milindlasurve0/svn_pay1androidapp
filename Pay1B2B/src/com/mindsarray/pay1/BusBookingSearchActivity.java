package com.mindsarray.pay1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mindsarray.pay1.adapterhandler.BusBookingSearchAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;

public class BusBookingSearchActivity extends Activity {

	private TextView textView_Route, textView_Date;
	private ImageView imageView_Filter, imageView_Previous, imageView_Next;
	private Button button_FilterByFare, button_FilterByTime;
	private Button button_FilterByRatting;
	private Spinner spinner;
	private ListView listView_SearchResult;
	private BusBookingSearchAdapter busBookingSearchAdapter;
	private String res = null;

	private ArrayList<HashMap<String, String>> buslist = new ArrayList<HashMap<String, String>>();
	private ArrayList<HashMap<String, String>> buslistTemp = new ArrayList<HashMap<String, String>>();

	private String source = null, source_id = null, destination = null,
			destination_id = null, date = null;

	String months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
			"Sep", "Oct", "Nov", "Dec" };

	private GetSearchTask getSearchTask;

	Drawable drawable_clicked, drawable_unclicked;

	private int year;
	private int month;
	private int day;
	static final int DATE_PICKER_ID = 1111;
	private static final String TAG = "Bus Booking";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.busbookingsearch_activity);

		try {
			Bundle extras = getIntent().getExtras();
			if (extras != null) {
				source = extras.getString("SOURCE");
				source_id = extras.getString("SOURCE_ID");
				destination = extras.getString("DESTINATION");
				destination_id = extras.getString("DESTINATION_ID");
				date = extras.getString("DATE");
				// seats = extras.getString("SEATS");
			}

			drawable_clicked = getResources().getDrawable(
					android.R.drawable.btn_star_big_on);
			drawable_unclicked = getResources().getDrawable(
					android.R.drawable.btn_star_big_off);
			textView_Route = (TextView) findViewById(R.id.textView_Route);
			textView_Route.setText(source + " to " + destination);
			// textView_Seats = (TextView) findViewById(R.id.textView_Seats);
			// textView_Seats.setText("Number of Travelers : " + seats);
			textView_Date = (TextView) findViewById(R.id.textView_Date);
			textView_Date.setText(date);
			textView_Date.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showDialog(DATE_PICKER_ID);
				}
			});

			// imageView_Filter = (ImageView)
			// findViewById(R.id.imageView_Filter);
			// imageView_Filter.setOnClickListener(new View.OnClickListener() {
			//
			// @Override
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// Constants.showCustomToast(BusBookingSearchActivity.this,
			// "Filter",
			// Toast.LENGTH_LONG).show();
			// }
			// });

			imageView_Previous = (ImageView) findViewById(R.id.imageView_Previous);
			imageView_Previous.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// DateFormat dateFormat = new
					// SimpleDateFormat("dd-MMM-yyyy");
					try {
						Date selected_date = new Date(textView_Date.getText()
								.toString().trim());

						Calendar c = Calendar.getInstance();
						c.set((1900 + selected_date.getYear()),
								selected_date.getMonth(),
								selected_date.getDate());
						c.add(Calendar.DATE, -1);
						// dateFormat.format(c.getTime());

						// Show current date

						String dt = "";
						if (c.get(Calendar.DAY_OF_MONTH) < 10)
							dt = "0" + c.get(Calendar.DAY_OF_MONTH);
						else
							dt = c.get(Calendar.DAY_OF_MONTH) + "";
						textView_Date.setText(new StringBuilder().append(dt)
								.append("-")
								.append(months[c.get(Calendar.MONTH)])
								.append("-").append(c.get(Calendar.YEAR)));
						Date todays_date = new Date();
						if (new Date(textView_Date.getText().toString().trim())
								.before(todays_date)
								|| selected_date.equals(todays_date)) {
							imageView_Previous.setVisibility(View.GONE);
						} else {
							imageView_Previous.setVisibility(View.VISIBLE);
						}
						getSearchTask = new GetSearchTask();
						getSearchTask.execute(new String[] { source_id,
								destination_id,
								textView_Date.getText().toString().trim() });
					} catch (Exception e) {

					}
				}
			});

			imageView_Next = (ImageView) findViewById(R.id.imageView_Next);
			imageView_Next.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						Date selected_date = new Date(textView_Date.getText()
								.toString().trim());

						imageView_Previous.setVisibility(View.VISIBLE);
						Calendar c = Calendar.getInstance();
						c.set((1900 + selected_date.getYear()),
								selected_date.getMonth(),
								selected_date.getDate());
						c.add(Calendar.DATE, +1);
						// dateFormat.format(c.getTime());

						// Show current date

						String dt = "";
						if (c.get(Calendar.DAY_OF_MONTH) < 10)
							dt = "0" + c.get(Calendar.DAY_OF_MONTH);
						else
							dt = c.get(Calendar.DAY_OF_MONTH) + "";
						textView_Date.setText(new StringBuilder().append(dt)
								.append("-")
								.append(months[c.get(Calendar.MONTH)])
								.append("-").append(c.get(Calendar.YEAR)));

						getSearchTask = new GetSearchTask();
						getSearchTask.execute(new String[] { source_id,
								destination_id,
								textView_Date.getText().toString().trim() });
					} catch (Exception e) {

					}
				}
			});

			button_FilterByFare = (Button) findViewById(R.id.button_FilterByFare);
			button_FilterByFare.setCompoundDrawablesWithIntrinsicBounds(null,
					null, drawable_unclicked, null);
			button_FilterByFare.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					button_FilterByTime
							.setCompoundDrawablesWithIntrinsicBounds(null,
									null, drawable_unclicked, null);
					if (button_FilterByFare.getCompoundDrawables()[2]
							.equals(drawable_unclicked)) {
						button_FilterByFare
								.setCompoundDrawablesWithIntrinsicBounds(null,
										null, drawable_clicked, null);
						Collections.sort(buslist, new SortFareDescending(
								Constants.BUS_FARE));
						busBookingSearchAdapter.notifyDataSetChanged();
					} else {
						button_FilterByFare
								.setCompoundDrawablesWithIntrinsicBounds(null,
										null, drawable_unclicked, null);
						Collections.sort(buslist, new SortFareAscending(
								Constants.BUS_FARE));
						busBookingSearchAdapter.notifyDataSetChanged();
					}
				}
			});

			button_FilterByTime = (Button) findViewById(R.id.button_FilterByTime);
			button_FilterByTime.setCompoundDrawablesWithIntrinsicBounds(null,
					null, drawable_unclicked, null);
			button_FilterByTime.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					button_FilterByFare
							.setCompoundDrawablesWithIntrinsicBounds(null,
									null, drawable_unclicked, null);
					if (button_FilterByTime.getCompoundDrawables()[2]
							.equals(drawable_unclicked)) {
						button_FilterByTime
								.setCompoundDrawablesWithIntrinsicBounds(null,
										null, drawable_clicked, null);
						Collections.sort(buslist, new SortTimeDescending(
								Constants.BUS_DEPARTURE_TIME));
						busBookingSearchAdapter.notifyDataSetChanged();
					} else {
						button_FilterByTime
								.setCompoundDrawablesWithIntrinsicBounds(null,
										null, drawable_unclicked, null);
						Collections.sort(buslist, new SortTimeAscending(
								Constants.BUS_DEPARTURE_TIME));
						busBookingSearchAdapter.notifyDataSetChanged();
					}
				}
			});

			button_FilterByRatting = (Button) findViewById(R.id.button_FilterByRatting);
			button_FilterByRatting
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							spinner.performClick();
						}
					});
			spinner = (Spinner) findViewById(R.id.spinner);
			List<String> filter = new ArrayList<String>();
			filter.add("All");
			filter.add("A/C");
			filter.add("Non A/C");
			filter.add("Sleeper");
			filter.add("Non Sleeper");

			spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view,
						int pos, long id) {
					// TODO Auto-generated method stub
					button_FilterByRatting
							.setCompoundDrawablesWithIntrinsicBounds(null,
									null, drawable_unclicked, null);
					if (pos != 0) {
						button_FilterByRatting
								.setCompoundDrawablesWithIntrinsicBounds(null,
										null, drawable_clicked, null);
						filterByBusType(pos);
					} else {
						buslist.clear();
						buslist.addAll(buslistTemp);
						busBookingSearchAdapter.notifyDataSetChanged();
					}
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}
			});
			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
					BusBookingSearchActivity.this,
					android.R.layout.simple_spinner_item, filter);

			dataAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

			spinner.setAdapter(dataAdapter);
			// button_FilterByRatting.setOnClickListener(new
			// View.OnClickListener()
			// {
			//
			// @Override
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			//
			// }
			// });

			Date selected_date = new Date(textView_Date.getText().toString()
					.trim());
			Date todays_date = new Date();
			if (selected_date.before(todays_date)
					|| selected_date.equals(todays_date))
				imageView_Previous.setVisibility(View.GONE);

			listView_SearchResult = (ListView) findViewById(R.id.listView_SearchResult);
			busBookingSearchAdapter = new BusBookingSearchAdapter(
					BusBookingSearchActivity.this, buslist);
			listView_SearchResult.setAdapter(busBookingSearchAdapter);
			listView_SearchResult
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(
									BusBookingSearchActivity.this,
									BusBookingSeatActivity.class);
							// intent.putExtra("RESULT", res);
							intent.putExtra("SOURCE", source);
							intent.putExtra("SOURCE_ID", source_id);
							intent.putExtra("DESTINATION", destination);
							intent.putExtra("DESTINATION_ID", destination_id);
							intent.putExtra("DATE", textView_Date.getText()
									.toString());
							// intent.putExtra("SEATS", seats);
							HashMap<String, String> map = new HashMap<String, String>();
							map = buslist.get(position);
							intent.putExtra(Constants.BUS_ID,
									map.get(Constants.BUS_ID).toString().trim());
							intent.putExtra(Constants.BUS_NAME,
									map.get(Constants.BUS_NAME).toString()
											.trim());
							intent.putExtra(Constants.BUS_TIME,
									map.get(Constants.BUS_TIME).toString()
											.trim());
							intent.putExtra(Constants.BUS_FARE,
									map.get(Constants.BUS_FARE).toString()
											.trim());
							intent.putExtra(Constants.BUS_TYPE,
									map.get(Constants.BUS_TYPE).toString()
											.trim());
							intent.putExtra(Constants.BUS_AC,
									map.get(Constants.BUS_AC).toString().trim());
							intent.putExtra(Constants.BUS_NONAC,
									map.get(Constants.BUS_NONAC).toString()
											.trim());
							intent.putExtra(Constants.BUS_SLEEPER,
									map.get(Constants.BUS_SLEEPER).toString()
											.trim());
							intent.putExtra(Constants.BUS_NONSLEEPER,
									map.get(Constants.BUS_NONSLEEPER)
											.toString().trim());
							intent.putExtra(Constants.BUS_ARRIVAL_TIME, map
									.get(Constants.BUS_ARRIVAL_TIME).toString()
									.trim());
							intent.putExtra(Constants.BUS_DEPARTURE_TIME, map
									.get(Constants.BUS_DEPARTURE_TIME)
									.toString().trim());
							intent.putExtra(Constants.BUS_CANCELLATION_POLICY,
									map.get(Constants.BUS_CANCELLATION_POLICY)
											.toString().trim());
							startActivity(intent);
						}
					});

			getSearchTask = new GetSearchTask();
			getSearchTask.execute(new String[] { source_id, destination_id,
					date });
		} catch (Exception exception) {

		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants.showNavigationBar(BusBookingSearchActivity.this);
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_PICKER_ID:

			final Calendar c = Calendar.getInstance();
			year = c.get(Calendar.YEAR);
			month = c.get(Calendar.MONTH);
			day = c.get(Calendar.DAY_OF_MONTH);

			// open datepicker dialog.
			// set date picker for current date
			// add pickerListener listner to date picker
			// set date picker as current date
			DatePickerDialog _date = new DatePickerDialog(this, pickerListener,
					year, month, day) {
				@Override
				public void onDateChanged(DatePicker view, int myear,
						int monthOfYear, int dayOfMonth) {
					if (myear < year)
						view.updateDate(year, month, day);

					if (monthOfYear < month && myear == year)
						view.updateDate(year, month, day);

					if (dayOfMonth < day && myear == year
							&& monthOfYear == month)
						view.updateDate(year, month, day);

				}
			};
			return _date;
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		@Override
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {

			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			String dt = "";
			if (day < 10)
				dt = "0" + day;
			else
				dt = day + "";
			textView_Date.setText(new StringBuilder().append(dt).append("-")
					.append(months[month]).append("-").append(year));
			Date todays_date = new Date();
			if (new Date(textView_Date.getText().toString().trim())
					.before(todays_date)) {
				imageView_Previous.setVisibility(View.GONE);
			} else {
				imageView_Previous.setVisibility(View.VISIBLE);
			}
			getSearchTask = new GetSearchTask();
			getSearchTask.execute(new String[] { source_id, destination_id,
					textView_Date.getText().toString() });
		}
	};

	private void filterByBusType(int index) {
		try {
			String keyToSearch = "true";

			if (index != 0) {
				buslist.clear();
				String key = "";
				switch (index) {
				case 1:
					key = Constants.BUS_AC;
					break;
				case 2:
					key = Constants.BUS_NONAC;
					break;
				case 3:
					key = Constants.BUS_SLEEPER;
					break;
				case 4:
					key = Constants.BUS_NONSLEEPER;

				default:
					break;
				}
				for (Map<String, String> map : buslistTemp) {

					if (map.get(key).toLowerCase()
							.startsWith(keyToSearch.toLowerCase())) {

						HashMap<String, String> map1 = new HashMap<String, String>();

						map1.put(Constants.BUS_ID, map.get(Constants.BUS_ID));
						map1.put(Constants.BUS_NAME,
								map.get(Constants.BUS_NAME));
						map1.put(Constants.BUS_TIME,
								map.get(Constants.BUS_TIME));
						map1.put(Constants.BUS_TYPE,
								map.get(Constants.BUS_TYPE));
						map1.put(Constants.BUS_DURATION,
								map.get(Constants.BUS_DURATION));
						map1.put(Constants.BUS_SEATS,
								map.get(Constants.BUS_SEATS));
						map1.put(Constants.BUS_AC, map.get(Constants.BUS_AC));
						map1.put(Constants.BUS_NONAC,
								map.get(Constants.BUS_NONAC));
						map1.put(Constants.BUS_SLEEPER,
								map.get(Constants.BUS_SLEEPER));
						map1.put(Constants.BUS_NONSLEEPER,
								map.get(Constants.BUS_NONSLEEPER));
						map1.put(Constants.BUS_ARRIVAL_TIME,
								map.get(Constants.BUS_ARRIVAL_TIME));
						map1.put(Constants.BUS_DEPARTURE_TIME,
								map.get(Constants.BUS_DEPARTURE_TIME));
						map1.put(Constants.BUS_CANCELLATION_POLICY,
								map.get(Constants.BUS_CANCELLATION_POLICY));
						map1.put(Constants.BUS_FARE,
								map.get(Constants.BUS_FARE));

						buslist.add(map1);
					}
				}
				BusBookingSearchActivity.this.runOnUiThread(new Runnable() {

					public void run() {
						// TODO Auto-generated method
						// stub
						busBookingSearchAdapter.notifyDataSetChanged();
					}
				});
				if (button_FilterByFare.getCompoundDrawables()[2]
						.equals(drawable_clicked)) {
					Collections.sort(buslist, new SortFareDescending(
							Constants.BUS_FARE));
					busBookingSearchAdapter.notifyDataSetChanged();
				}
				if (button_FilterByTime.getCompoundDrawables()[2]
						.equals(drawable_clicked)) {
					Collections.sort(buslist, new SortTimeDescending(
							Constants.BUS_DEPARTURE_TIME));
					busBookingSearchAdapter.notifyDataSetChanged();
				}
			} else {
				buslist.clear();
				buslist.addAll(buslistTemp);
				busBookingSearchAdapter.notifyDataSetChanged();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	class SortFareAscending implements Comparator<HashMap<String, String>> {
		private final String key;

		public SortFareAscending(String key) {
			this.key = key;
		}

		@Override
		public int compare(HashMap<String, String> first,
				HashMap<String, String> second) {
			// TODO: Null checking, both for maps and values
			try {
				String fStr = first.get(key);
				String sStr = second.get(key);
				int firstValue = 0, secondValue = 0;
				if (fStr.contains("/"))
					firstValue = Integer.parseInt(fStr.substring(0,
							fStr.indexOf("/")));
				else
					firstValue = Integer.parseInt(fStr);
				if (sStr.contains("/"))
					secondValue = Integer.parseInt(sStr.substring(0,
							sStr.indexOf("/")));
				else
					secondValue = Integer.parseInt(sStr);
				// // Log.e("Comp Val", "F = " + firstValue + "  S = " +
				// secondValue
				// + " Key " + key);
				return Integer.valueOf(firstValue).compareTo(
						Integer.valueOf(secondValue));
			} catch (Exception e) {
				return 0;
			}
		}
	}

	class SortFareDescending implements Comparator<HashMap<String, String>> {
		private final String key;

		public SortFareDescending(String key) {
			this.key = key;
		}

		@Override
		public int compare(HashMap<String, String> first,
				HashMap<String, String> second) {
			// TODO: Null checking, both for maps and values
			try {
				String fStr = first.get(key);
				String sStr = second.get(key);
				int firstValue = 0, secondValue = 0;
				if (fStr.contains("/"))
					firstValue = Integer.parseInt(fStr.substring(0,
							fStr.indexOf("/")));
				else
					firstValue = Integer.parseInt(fStr);
				if (sStr.contains("/"))
					secondValue = Integer.parseInt(sStr.substring(0,
							sStr.indexOf("/")));
				else
					secondValue = Integer.parseInt(sStr);
				// // Log.e("Comp Val", "F = " + firstValue + "  S = " +
				// secondValue
				// + " Key " + key);
				return Integer.valueOf(secondValue).compareTo(
						Integer.valueOf(firstValue));
			} catch (Exception e) {
				return 0;
			}
		}
	}

	class SortTimeAscending implements Comparator<HashMap<String, String>> {
		private final String key;

		public SortTimeAscending(String key) {
			this.key = key;
		}

		@Override
		public int compare(HashMap<String, String> first,
				HashMap<String, String> second) {
			// TODO: Null checking, both for maps and values
			// String firstValue = first.get(key);
			// String secondValue = second.get(key);
			// // // Log.e("Comp Val", "F = " + firstValue + "  S = " +
			// secondValue
			// // + " Key " + key);
			// return firstValue.compareTo(secondValue);
			try {
				int firstValue = Integer.parseInt(first.get(key));
				int secondValue = Integer.parseInt(second.get(key));
				// // Log.e("Comp Val", "F = " + firstValue + "  S = " +
				// secondValue
				// + " Key " + key);
				return Integer.valueOf(firstValue).compareTo(
						Integer.valueOf(secondValue));
			} catch (Exception e) {
				return 0;
			}
		}
	}

	class SortTimeDescending implements Comparator<HashMap<String, String>> {
		private final String key;

		public SortTimeDescending(String key) {
			this.key = key;
		}

		@Override
		public int compare(HashMap<String, String> first,
				HashMap<String, String> second) {
			// TODO: Null checking, both for maps and values
			try {
				int firstValue = Integer.parseInt(first.get(key));
				int secondValue = Integer.parseInt(second.get(key));
				// // Log.e("Comp Val", "F = " + firstValue + "  S = " +
				// secondValue
				// + " Key " + key);
				return Integer.valueOf(secondValue).compareTo(
						Integer.valueOf(firstValue));
			} catch (Exception e) {
				return 0;
			}
		}
	}

	public class GetSearchTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				Date date = new Date(params[2]);

				Calendar c = Calendar.getInstance();
				c.setTime(date);
				// String response = RequestClass.getInstance().readPay1Request(
				// BusBookingSearchActivity.this,
				// Constants.BUS_API_URL
				// + "method=busBooking&action=availabletrips&source="
				// + params[0]
				// + "&destination="
				// + params[1]
				// + "&doj="
				// + new StringBuilder().append(c.get(Calendar.YEAR))
				// .append("-")
				// .append((c.get(Calendar.MONTH) + 1))
				// .append("-")
				// .append(c.get(Calendar.DAY_OF_MONTH)));

				ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

				listValuePair
						.add(new BasicNameValuePair("method", "busBooking"));
				listValuePair.add(new BasicNameValuePair("action",
						"availabletrips"));
				listValuePair.add(new BasicNameValuePair("source", params[0]));
				listValuePair.add(new BasicNameValuePair("destination",
						params[1]));
				listValuePair.add(new BasicNameValuePair("doj",
						new StringBuilder().append(c.get(Calendar.YEAR))
								.append("-")
								.append((c.get(Calendar.MONTH) + 1))
								.append("-")
								.append(c.get(Calendar.DAY_OF_MONTH))
								.toString()));

				String response = RequestClass.getInstance().readPay1Request(
						BusBookingSearchActivity.this, Constants.API_URL,
						listValuePair);

				return response;
			} catch (Exception exception) {
				return null;
			}

		}

		@Override
		protected void onPostExecute(String result) {
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			super.onPostExecute(result);
			try {
				if (!result.startsWith("Error")) {
					res = result;
					buslist.clear();
					JSONObject jsonObjectResult = new JSONObject(result);
					String status = jsonObjectResult.getString("status");
					if (status.equalsIgnoreCase("SUCCESS")) {

						// JSONObject jsonObjectDescription = jsonObjectResult
						// .getJSONObject("description");
						// for (int i = 0; i < jsonObjectDescription.length();
						// i++)
						// {

						JSONArray jsonArrayAvailableTrips = jsonObjectResult
								.getJSONArray("availableTrips");

						for (int j = 0; j < jsonArrayAvailableTrips.length(); j++) {

							HashMap<String, String> map = new HashMap<String, String>();

							map.put(Constants.BUS_ID, jsonArrayAvailableTrips
									.getJSONObject(j).getString("id")
									.toString().trim());
							map.put(Constants.BUS_NAME, jsonArrayAvailableTrips
									.getJSONObject(j).getString("travels")
									.toString().trim());
							map.put(Constants.BUS_TIME, jsonArrayAvailableTrips
									.getJSONObject(j)
									.getString("departureTime").toString()
									.trim()
									+ " - "
									+ jsonArrayAvailableTrips.getJSONObject(j)
											.getString("arrivalTime")
											.toString().trim());
							map.put(Constants.BUS_TYPE, jsonArrayAvailableTrips
									.getJSONObject(j).getString("busType")
									.toString().trim());
							map.put(Constants.BUS_DURATION,
									Constants
											.convertMinutesToHours(
													jsonArrayAvailableTrips
															.getJSONObject(j)
															.getString(
																	"arrivalTimeM")
															.toString().trim(),
													jsonArrayAvailableTrips
															.getJSONObject(j)
															.getString(
																	"departureTimeM")
															.toString().trim()));
							map.put(Constants.BUS_SEATS,
									jsonArrayAvailableTrips.getJSONObject(j)
											.getString("availableSeats")
											.toString().trim()
											+ " seats");
							map.put(Constants.BUS_AC, jsonArrayAvailableTrips
									.getJSONObject(j).getString("AC")
									.toString().trim());
							map.put(Constants.BUS_NONAC,
									jsonArrayAvailableTrips.getJSONObject(j)
											.getString("nonAC").toString()
											.trim());
							map.put(Constants.BUS_SLEEPER,
									jsonArrayAvailableTrips.getJSONObject(j)
											.getString("sleeper").toString()
											.trim());
							map.put(Constants.BUS_NONSLEEPER,
									jsonArrayAvailableTrips.getJSONObject(j)
											.getString("seater").toString()
											.trim());
							map.put(Constants.BUS_ARRIVAL_TIME,
									jsonArrayAvailableTrips.getJSONObject(j)
											.getString("arrivalTimeM")
											.toString().trim());
							map.put(Constants.BUS_DEPARTURE_TIME,
									jsonArrayAvailableTrips.getJSONObject(j)
											.getString("departureTimeM")
											.toString().trim());
							map.put(Constants.BUS_CANCELLATION_POLICY,
									jsonArrayAvailableTrips.getJSONObject(j)
											.getString("cancellationPolicy")
											.toString().trim());

							JSONArray jsonArrayFare = jsonArrayAvailableTrips
									.getJSONObject(j).getJSONArray("fares");
							StringBuffer fare = new StringBuffer();
							fare.append(jsonArrayFare.get(0));
							for (int k = 0; k < jsonArrayFare.length(); k++)
								if (k != 0)
									fare.append("/").append(
											jsonArrayFare.get(k));

							map.put(Constants.BUS_FARE, fare.toString());

							buslist.add(map);
						}

						buslistTemp.clear();
						buslistTemp.addAll(buslist);
						busBookingSearchAdapter.notifyDataSetChanged();

						filterByBusType(spinner.getSelectedItemPosition());

						if (button_FilterByFare.getCompoundDrawables()[2]
								.equals(drawable_clicked)) {
							Collections.sort(buslist, new SortFareDescending(
									Constants.BUS_FARE));
							busBookingSearchAdapter.notifyDataSetChanged();
						}
						if (button_FilterByTime.getCompoundDrawables()[2]
								.equals(drawable_clicked)) {
							Collections.sort(buslist, new SortTimeDescending(
									Constants.BUS_DEPARTURE_TIME));
							busBookingSearchAdapter.notifyDataSetChanged();
						}
					} else {
						Constants.showOneButtonFailureDialog(
								BusBookingSearchActivity.this,
								Constants.checkCode(result), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(
							BusBookingSearchActivity.this,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(
						BusBookingSearchActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(
						BusBookingSearchActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(BusBookingSearchActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							GetSearchTask.this.cancel(true);
							finish();
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			GetSearchTask.this.cancel(true);
			dialog.cancel();
			finish();
		}

	}
}
