package com.mindsarray.pay1;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

public class OTPVerificationNumber extends Activity {
	EditText editText_OTP;
	Context mContext;
	private static final String TAG = "OTP verification";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.otp_verification_activity);
		mContext = OTPVerificationNumber.this;
		editText_OTP = (EditText) findViewById(R.id.editText_OTP);
		TextView textView_Mobile=(TextView)findViewById(R.id.textView_Mobile);
		textView_Mobile.setText(Utility.getUserMobileNumber(mContext));
		Button button_Cancel = (Button) findViewById(R.id.button_Cancel);
		ImageView imageView_Edit = (ImageView) findViewById(R.id.imageView_Edit);
		imageView_Edit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		button_Cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		Button button_Submit = (Button) findViewById(R.id.button_Submit);
		button_Submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (editText_OTP.getText().toString().trim().length() != 0) {
					new OTPVerificationNumberTask().execute(editText_OTP
							.getText().toString().trim());
				} else {
					editText_OTP.setBackgroundDrawable(mContext.getResources().getDrawable(
							R.drawable.edittext_valid_background_selector));
					editText_OTP.setPadding(10, 10, 10, 10);
					editText_OTP.setTextSize(14);
					editText_OTP.setError("Please enter correct OTP");
					editText_OTP.setTextColor(mContext.getResources()
							.getColor(R.color.Black));
					
				}
			}
		});
	}

	public class OTPVerificationNumberTask extends
			AsyncTask<String, String, String> implements OnDismissListener {

		private ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
			listValuePair.add(new BasicNameValuePair("method",
					"authenticateMobileNumberChange"));
			listValuePair.add(new BasicNameValuePair("otp", params[0]));

			
			  String response = RequestClass.getInstance().readPay1Request(
			  mContext, Constants.API_URL, listValuePair);
			 

			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Log.e("post execute", result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}

			//result = "([{\"status\":\"success\",\"description\":\"<message>\"}]);";
			try {
				if (!result.startsWith("Error")) {
					// result = result.substring(result.indexOf("([{"),
					// result.lastIndexOf("}])") + 3);
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONArray jsonArray = new JSONArray(replaced);
					JSONObject jsonObject = jsonArray.getJSONObject(0);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						/*
						 * startActivity(new Intent(mContext,
						 * OTPVerificationNumber.class));
						 */
						Constants.showOneButtonSuccessDialog(mContext,
								jsonObject.getString("description"),
								Constants.OTP_SETTINGS);
					} else {
						Constants.showOneButtonFailureDialog(mContext,
								Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(mContext,
							Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
							Constants.OTHER_ERROR);
				}
			} catch (JSONException e) {
				e.printStackTrace();
				Constants.showOneButtonFailureDialog(mContext,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				e.printStackTrace();
				Constants.showOneButtonFailureDialog(mContext,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(mContext);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							OTPVerificationNumberTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			OTPVerificationNumberTask.this.cancel(true);
			dialog.cancel();
		}

	}
}
