package com.mindsarray.pay1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.mindsarray.pay1.adapterhandler.SupportListAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.utilities.Utility;

public class SupportActivity extends Activity {
	
	private static String TAG = "Support";
	private SupportListAdapter mSupportListAdapter;
	private Context mContext = SupportActivity.this;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.support);
		Constants.showNavigationBar(mContext);
		String[] textArray = {
			"Chat Support",
			"Simply write to us on help@pay1.in",
			"Call us on 022 67242288 for any queries",
			"Bank Details"
		};
		
		int[] iconArray = {
			R.drawable.ic_chat,
			R.drawable.ic_mail,
			R.drawable.ic_misscall,
			R.drawable.ic_details
		};
		
		ListView supportListView = (ListView) findViewById(R.id.supportListView);
		mSupportListAdapter = new SupportListAdapter(mContext, textArray, iconArray);
		supportListView.setAdapter(mSupportListAdapter);
		
		supportListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent intent = new Intent();
				switch(position){
					case 0:
						intent.setClass(mContext,
								ChatActivity.class);
						startActivity(intent);
						break;
					case 1:
						Intent emailIntent = new Intent(Intent.ACTION_SEND);
						emailIntent.setData(Uri.parse("mailto:"));
						emailIntent.setType("text/plain");
						emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"help@pay1.in"});
						emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Retailer Help - Mobile: " + Utility.getMobileNumber(mContext,
								Constants.SHAREDPREFERENCE_MOBILE_NUMBER));
						startActivity(emailIntent);
						break;	
					case 2:
						Intent callIntent = new Intent(Intent.ACTION_CALL);
						callIntent.setData(Uri.parse("tel:02267242288"));
						startActivity(callIntent);
						break;
					case 3:
						intent.setClass(mContext,
								BankCashDepositActivity.class);
						startActivity(intent);
						break;
				}
			}
		});
		
		
	}
	
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Constants.showNavigationBar(mContext);
	}
	
	public View getViewByPosition(int pos, ListView listView) {
	    final int firstListItemPosition = listView.getFirstVisiblePosition();
	    final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

	    if (pos < firstListItemPosition || pos > lastListItemPosition ) {
	        return listView.getAdapter().getView(pos, null, listView);
	    } else {
	        final int childIndex = pos - firstListItemPosition;
	        return listView.getChildAt(childIndex);
	    }
	}
}	