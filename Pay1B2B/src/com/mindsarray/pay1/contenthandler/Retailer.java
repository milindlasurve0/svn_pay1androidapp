package com.mindsarray.pay1.contenthandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class Retailer {

	private static final String TAG = "Retailer";

	public String retailer_id;
	public String distributor_id;
	public String salesman_id;
	public String mobile;
	public String name;
	public String shop_name;
	public String shop_type;
	public String location_type;
	public String rental_flag;
	public String address;
	public String area;
	public String city;
	public String state;
	public String pin_code;
	public String latitude;
	public String longitude;
	public String verify_flag;
	public String trial_flag;
	public String created_on;

	public Map documents;

	public Retailer(JSONObject jsonRetailer) {
		try {
			Iterator<?> keys = jsonRetailer.keys();

			while (keys.hasNext()) {
				String key = (String) keys.next();
				if (jsonRetailer.get(key).toString().equals("null")) {
					jsonRetailer.put(key, "");
				}
			}

			set_retailer_id(jsonRetailer.getString("id"));
			set_distributor_id(jsonRetailer.getString("parent_id"));
			set_salesman_id(jsonRetailer.getString("maint_salesman"));
			set_mobile(jsonRetailer.getString("mobile"));
			set_name(jsonRetailer.getString("name"));
			set_shop_name(jsonRetailer.getString("shopname"));
			set_shop_type(jsonRetailer.getString("shop_type"));
			set_location_type(jsonRetailer.getString("location_type"));
			set_rental_flag(jsonRetailer.getString("rental_flag"));
			set_address(jsonRetailer.getString("address"));
			set_area(jsonRetailer.getString("area"));
			set_city(jsonRetailer.getString("city"));
			set_state(jsonRetailer.getString("state"));
			set_pin_code(jsonRetailer.getString("pin"));
			set_latitude(jsonRetailer.getString("latitude"));
			set_longitude(jsonRetailer.getString("longitude"));
			set_verify_flag(jsonRetailer.getString("verify_flag"));
			set_trial_flag(jsonRetailer.getString("trial_flag"));
			set_created_on(jsonRetailer.getString("created"));

			if(jsonRetailer.has("kyc_states")){
				this.documents = documents(jsonRetailer);
			}
		} catch (Exception e) {
			e.printStackTrace();

			set_retailer_id(null);
			set_distributor_id(null);
			set_salesman_id(null);
			set_mobile(null);
			set_name(null);
			set_shop_name(null);
			set_location_type(null);
			set_rental_flag(null);
			set_address(null);
			set_area(null);
			set_city(null);
			set_state(null);
			set_pin_code(null);
			set_latitude(null);
			set_longitude(null);
			set_verify_flag(null);
			set_trial_flag(null);
			set_created_on(null);
		}
	}

	public String retailer_id() {
		return retailer_id;
	}

	public String distributor_id() {
		return distributor_id;
	}

	public String salesman_id() {
		return salesman_id;
	}

	public String mobile() {
		return mobile;
	}

	public String name() {
		return name;
	}

	public String shop_name() {
		return shop_name;
	}

	public String shop_type() {
		return shop_type;
	};

	public String location_type() {
		return location_type;
	};

	public String rental_flag() {
		return rental_flag;
	};

	public String address() {
		return address;
	};

	public String area() {
		return area;
	};

	public String city() {
		return city;
	};

	public String state() {
		return state;
	};

	public String pin_code() {
		return pin_code();
	};

	public String latitude() {
		return latitude;
	};

	public String longitude() {
		return longitude;
	};

	public String verify_flag() {
		return verify_flag;
	};

	public Map documents(JSONObject jsonRetailer) {
		Map documents = new HashMap<String, ArrayList<Document>>();
		ArrayList<Document> idProofList = new ArrayList<Document>();
		ArrayList<Document> addressProofList = new ArrayList<Document>();
		ArrayList<Document> shopPhotosList = new ArrayList<Document>();

		try {
			if(jsonRetailer.has("kyc_states")){
				JSONArray retailerKYCStates = jsonRetailer
						.getJSONArray("kyc_states");
				
				String document_state = "", verify_state = "", reason = "";
				for (int i = 0; i < retailerKYCStates.length(); i++) {
					JSONArray images = retailerKYCStates.getJSONObject(i).getJSONObject("rks").getJSONArray("documents");
					document_state = retailerKYCStates.getJSONObject(i)
							.getJSONObject("rks").getString("document_state");
					if(document_state.equals("1")){
						verify_state = "-1";
					}	
					else if(document_state.equals("0")){
						verify_state = "0";
					}
					else if(document_state.equals("2")){
						verify_state = "1";
					}
					
					reason = retailerKYCStates.getJSONObject(i)
							.getJSONObject("rks").getString("comment");
					
					String type = "", url = "";
					String id = "0";
					for (int j = 0; j < images.length(); j++) {
						type = images.getJSONObject(j)
								.getJSONObject("rd").getString("type");
						url = images.getJSONObject(j)
								.getJSONObject("rd").getString("image_name");
						id = images.getJSONObject(j)
								.getJSONObject("rd").getString("id");
						
						if (type.equals("PAN_CARD")){
							idProofList.add(new Document(id, type, url,
									verify_state, created_on, reason));
						}	
						if (type.equals("ADDRESS_PROOF")){
							addressProofList.add(new Document(id, type, url,
									verify_state, created_on, reason));
						}	
						if (type.equals("SHOP_PHOTO")){
							shopPhotosList.add(new Document(id, type, url,
									verify_state, created_on, reason));
						}	
					}
				}
				
				documents.put("idProof", idProofList);
				documents.put("addressProof", addressProofList);
				documents.put("shopPhotos", shopPhotosList);
			}
//			if (jsonRetailer.has("image_detail")) {
//				Object imageDetails = jsonRetailer.get("image_detail");
//				if (imageDetails instanceof JSONArray) {
//					JSONArray retailerDetails = jsonRetailer
//							.getJSONArray("image_detail");
//					
//					
//					String type = "", url = "", verify_flag = "", created_on = "",comment="";
//					String id = "0";
//					for (int i = 0; i < retailerDetails.length(); i++) {
//						type = retailerDetails.getJSONObject(i)
//								.getJSONObject("rd").getString("type");
//						url = retailerDetails.getJSONObject(i)
//								.getJSONObject("rd").getString("image_name");
//						id = retailerDetails.getJSONObject(i)
//								.getJSONObject("rd").getString("id");
////						verify_flag = retailerDetails.getJSONObject(i)
////								.getJSONObject("rd").getString("verify_flag");
////						created_on = retailerDetails.getJSONObject(i)
////								.getJSONObject("rd").getString("created");
////						comment = retailerDetails.getJSONObject(i)
////								.getJSONObject("rd").getString("comment");
//						
//						if (type.equals("PAN_CARD")){
//							verify_flag = (String) kyc_states.get("PAN_CARD");
//							idProofList.add(new Document(id, type, url,
//									verify_flag, created_on,comment));
//						}	
//						if (type.equals("ADDRESS_PROOF")){
//							verify_flag = (String) kyc_states.get("ADDRESS_PROOF");
//							addressProofList.add(new Document(id, type, url,
//									verify_flag, created_on,comment));
//						}	
//						if (type.equals("SHOP_PHOTO")){
//							verify_flag = (String) kyc_states.get("SHOP_PHOTO");
//							shopPhotosList.add(new Document(id, type, url,
//									verify_flag, created_on,comment));
//						}	
//					}
//
//					documents.put("idProof", idProofList);
//					documents.put("addressProof", addressProofList);
//					documents.put("shopPhotos", shopPhotosList);
//				} else if (imageDetails instanceof JSONObject) {
//					JSONObject retailerDetails = jsonRetailer
//							.getJSONObject("image_detail");
//					String type = "", url = "", verify_flag = "", created = "";
//					String id = "0",comment="";
//					Iterator<?> documentKeys = retailerDetails.keys();
//
//					while (documentKeys.hasNext()) {
//						String key = (String) documentKeys.next();
//						JSONObject documents1 = retailerDetails.getJSONObject(
//								key).getJSONObject("rd");
//						type = documents1.getString("type");
//						url = documents1.getString("image_name");
////						verify_flag = documents1.getString("verify_flag");
////						created = documents1.getString("created");
////						comment = documents1.getString("comment");
//						id = documents1.getString("id");
//
//						if (type.equals("idProof"))
//							idProofList.add(new Document(id, type, url,
//									verify_flag, created_on,comment));
//						if (type.equals("addressProof"))
//							addressProofList.add(new Document(id, type, url,
//									verify_flag, created_on,comment));
//						if (type.equals("shop"))
//							shopPhotosList.add(new Document(id, type, url,
//									verify_flag, created_on,comment));
//					}
//
//					documents.put("PAN_CARD", idProofList);
//					documents.put("ADDRESS_PROOF", addressProofList);
//					documents.put("SHOP_PHOTO", shopPhotosList);
//				}
//			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return documents;
	};

	public void set_retailer_id(String retailer_id) {
		this.retailer_id = retailer_id;
	}

	public void set_distributor_id(String distributor_id) {
		this.distributor_id = distributor_id;
	}

	public void set_salesman_id(String salesman_id) {
		this.salesman_id = salesman_id;
	}

	public void set_mobile(String mobile) {
		this.mobile = mobile;
	}

	public void set_name(String name) {
		this.name = name;
	}

	public void set_shop_name(String shop_name) {
		this.shop_name = shop_name;
	}

	public void set_shop_type(String shop_type) {
		this.shop_type = shop_type;
	};

	public void set_location_type(String location_type) {
		this.location_type = location_type;
	};

	public void set_rental_flag(String rental_flag) {
		this.rental_flag = rental_flag;
	};

	public void set_address(String address) {
		this.address = address;
	};

	public void set_area(String area) {
		this.area = area;
	};

	public void set_city(String city) {
		this.city = city;
	};

	public void set_state(String state) {
		this.state = state;
	};

	public void set_pin_code(String pin_code) {
		this.pin_code = pin_code;
	};

	public void set_latitude(String latitude) {
		this.latitude = latitude;
	};

	public void set_longitude(String longitude) {
		this.longitude = longitude;
	};

	public void set_verify_flag(String verify_flag) {
		this.verify_flag = verify_flag;
	};

	public void set_trial_flag(String trial_flag) {
		this.trial_flag = trial_flag;
	};

	public void set_created_on(String created_on) {
		this.created_on = created_on;
	};

	public void set_documents(Map documents) {
		this.documents = documents;
	}
}