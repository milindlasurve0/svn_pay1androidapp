package com.mindsarray.pay1.databasehandler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.servicehandler.CircleIntentService;
import com.mindsarray.pay1.utilities.Utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

public class NumbberWithCircleDataSource {
	// Database fields
	private SQLiteDatabase database;
	private Pay1SQLiteHelper dbHelper;
	private Context mContext;
	
	public NumbberWithCircleDataSource(Context context) {
		dbHelper = new Pay1SQLiteHelper(context);
		mContext = context;
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	int i = 0;

	public void createNumberAndCircleList(String areaName, String areaCode,
			String operatorName, String operatorCode, String operatorId,
			String number, String timeStamp) {

		ContentValues values = new ContentValues();
		values.put(Pay1SQLiteHelper.NAC_AREA_NAME, areaName);
		values.put(Pay1SQLiteHelper.NAC_AREA_CODE, areaCode);
		values.put(Pay1SQLiteHelper.NAC_OPERATOR_NAME, operatorName);
		values.put(Pay1SQLiteHelper.NAC_OPERATOR_CODE, operatorCode);
		values.put(Pay1SQLiteHelper.NAC_OPERATOR_ID, operatorId);
		values.put(Pay1SQLiteHelper.NAC_MOBILE_NUMBER, number);
		values.put(Pay1SQLiteHelper.NAC_UPDATE_TIME, timeStamp);
		try {
			database.insert(Pay1SQLiteHelper.TABLE_NUMBERANDCIRCLE, null,
					values);
			Log.d("NAC", i++ + " row inserted");
		} catch (Exception ee) {
			ee.printStackTrace();
		}
	}
	public void deleteAndInsertNumberAndCircle(String areaName, String areaCode,
			String operatorName, String operatorCode, String operatorId,
			String number, String timeStamp){
		database.execSQL("	delete from " + Pay1SQLiteHelper.TABLE_NUMBERANDCIRCLE + " where "
							+ Pay1SQLiteHelper.NAC_AREA_CODE + " = '" + areaCode 
							+ "' and " + Pay1SQLiteHelper.NAC_MOBILE_NUMBER + " = '" + number +"'");
		createNumberAndCircleList(areaName, areaCode,
				operatorName, operatorCode, operatorId,
				number, timeStamp);
	}
	public void deleteCircle() {
		database.delete(Pay1SQLiteHelper.TABLE_NUMBERANDCIRCLE, null, null);
	}

	public void deleteAddress(Address Address) {
		long id = Address.getAddressID();
		database.delete(Pay1SQLiteHelper.TABLE_NUMBERANDCIRCLE,
				Pay1SQLiteHelper.ADDRESS_ID + " = " + id, null);
	}

	public List<NumberWithCircle> getAllNumberDetails() {

		List<NumberWithCircle> numberWithCircle = new ArrayList<NumberWithCircle>();
		try {
			Cursor cursor = database.query(
					Pay1SQLiteHelper.TABLE_NUMBERANDCIRCLE, null, null, null,
					null, null, Pay1SQLiteHelper.NAC_UPDATE_TIME + " DESC");

			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				NumberWithCircle numberWithCircle1 = cursorToNumberWithCircle(cursor);
				numberWithCircle.add(numberWithCircle1);
				cursor.moveToNext();
			}
			// make sure to close the cursor
			cursor.close();
		} catch (SQLException se) {
			Log.d("NAC", "getAll " + se.getMessage());
		}
		return numberWithCircle;
	}

	List<String> operatorCodes;

	public List<String> getAllOperatorCode() {

		try {
			operatorCodes = new ArrayList<String>();
			String selecet = "SELECT DISTINCT "
					+ Pay1SQLiteHelper.NAC_OPERATOR_ID + " from "
					+ Pay1SQLiteHelper.TABLE_NUMBERANDCIRCLE + " ORDER BY "
					+ Pay1SQLiteHelper.NAC_OPERATOR_ID;

			Cursor cursor = database.rawQuery(selecet, null);

			/*
			 * Cursor cursor = database.query(
			 * Pay1SQLiteHelper.TABLE_NUMBERANDCIRCLE, null, null, null, null,
			 * null, Pay1SQLiteHelper.NAC_UPDATE_TIME + " DESC");
			 */
			cursor.moveToFirst();
			if (cursor.moveToFirst()) {
				do {
					operatorCodes.add(cursor.getString(cursor
							.getColumnIndex(Pay1SQLiteHelper.NAC_OPERATOR_ID)));

				} while (cursor.moveToNext());
			}

			cursor.close();
		} catch (SQLException se) {
			Log.d("NAC", "getAll " + se.getMessage());
		} catch (Exception e) {
			Log.d("NAC", "getAll" + e.getMessage());
		}
		return operatorCodes;
	}

	public NumberWithCircle getNumberDetails(String mobileNumber) {
		mobileNumber = mobileNumber.substring(0,
				Math.min(mobileNumber.length(), 4));
		NumberWithCircle numberWithCircle = new NumberWithCircle();

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_NUMBERANDCIRCLE,
				null, Pay1SQLiteHelper.NAC_MOBILE_NUMBER + " = '"

				+ mobileNumber + "'", null, null, null, null);

		cursor.moveToFirst();
		int count = cursor.getCount();
		
		if (count > 0){
			numberWithCircle = cursorToNumberWithCircle(cursor);
		}	

		// make sure to close the cursor
		cursor.close();
		return numberWithCircle;
	}

	private NumberWithCircle cursorToNumberWithCircle(Cursor cursor) {
		NumberWithCircle numberWithCircle1 = new NumberWithCircle();
		numberWithCircle1.setAreaID(cursor.getInt(0));
		numberWithCircle1.setAreaCode(cursor.getString(1));
		numberWithCircle1.setAreaName(cursor.getString(2));
		numberWithCircle1.setOperatorName(cursor.getString(3));
		numberWithCircle1.setOperatorId(cursor.getString(4));
		numberWithCircle1.setOperatorCode(cursor.getString(5));
		numberWithCircle1.setNumber(cursor.getString(6));
		numberWithCircle1.setTimeUpdated(cursor.getString(7));

		return numberWithCircle1;
	}
	
	public void updateCircleTime(String time){
		try{
			database.execSQL("	update " + Pay1SQLiteHelper.TABLE_NUMBERANDCIRCLE 
							+ " set " + Pay1SQLiteHelper.NAC_UPDATE_TIME + " = '" + time + "'");
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public boolean parseResponse(String response) {
		try {
			String time = String.valueOf(System.currentTimeMillis());
			if (!response.startsWith("Error")) {
				String replaced = response.replace("(", "")
						.replace(")", "").replace(";", "");

				replaced = replaced.substring(1, replaced.length() - 1);
				
				JSONObject jsonObject = new JSONObject(replaced);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("success")) {
					String details = jsonObject.getString("details");
					JSONArray detailsArray = new JSONArray(details);
					for (int i = 0; i < detailsArray.length(); i++) {
						JSONObject object = detailsArray.getJSONObject(i);
						String areaName = object.getString("area_name");
						String areaCode = object.getString("area");
						String operatorName = object.getString("opr_name");
						String operatorCode = object.getString("operator");
						String operatorId = object.getString("product_id");
						String number = object.getString("number");
						
						deleteAndInsertNumberAndCircle(areaName, areaCode,
								operatorName, operatorCode, operatorId,
								number, time);
					}

				}

			}
			Utility.setSharedPreference(mContext,
					Constants.LAST_CIRCLE_UPDATED,
					String.valueOf(System.currentTimeMillis()));
			return true;
		} catch (JSONException je) {
			Log.d("NAC", "row inserted   " + je.getMessage());
			je.printStackTrace();
		} catch (SQLException exception) {
			Log.d("NAC", "row inserted   " + exception.getMessage());
		} catch (Exception e) {
			Log.d("NAC", "row inserted   " + e.getMessage());

		} finally {
			try{

			}catch(SQLException see){
				Log.d("NAC", "row inserted   " + see.getMessage());
			}
		}
		return false;
	}
}
