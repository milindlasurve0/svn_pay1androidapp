package com.mindsarray.pay1.databasehandler;

import java.util.Date;

public class Recharges {
    
    //private variables
    int _id;
    String _amount;
    String _phone_number;
     Date _datetime;
    // Empty constructor
    public Recharges(){
         
    }

	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public String get_amount() {
		return _amount;
	}

	public void set_amount(String _amount) {
		this._amount = _amount;
	}

	public String get_phone_number() {
		return _phone_number;
	}

	public void set_phone_number(String _phone_number) {
		this._phone_number = _phone_number;
	}

	public Date get_datetime() {
		return _datetime;
	}

	public void set_datetime(Date _datetime) {
		this._datetime = _datetime;
	}
   
}
