package com.mindsarray.pay1.databasehandler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class RechargeDBHelper extends SQLiteOpenHelper {
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "contactsManager";
	private static final String TABLE_RECHARGES = "recharges";
	private static final String KEY_ID = "id";
	private static final String KEY_AMOUNT = "amount";
	private static final String KEY_MOB_NO = "mobile_number";
	private static final String KEY_RECHARGE_DATETIME = "recharge_datetime";

	public RechargeDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub

		String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_RECHARGES + "("
				+ KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_AMOUNT
				+ " TEXT," + KEY_MOB_NO + " TEXT," + KEY_RECHARGE_DATETIME
				+ " DATETIME" + ")";

		db.execSQL(CREATE_CONTACTS_TABLE);
	}

	public void addRecharges(Recharges recharges) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_AMOUNT, recharges.get_amount());
		values.put(KEY_MOB_NO, recharges.get_phone_number());

		values.put(KEY_RECHARGE_DATETIME, getDateTime());

		db.insert(TABLE_RECHARGES, null, values);

		db.close();
	}

	public boolean getRechargeStatus(String mob_number, String amount) {
		try {
			List<Recharges> contactList = new ArrayList<Recharges>();
			// Select All Query
			String selectQuery = "SELECT  * FROM " + TABLE_RECHARGES
					+ " WHERE " + KEY_MOB_NO + " = " + mob_number + " AND "
					+ KEY_AMOUNT + " = " + amount + " ORDER BY "
					+ KEY_RECHARGE_DATETIME + " DESC";
			/*
			 * String selectQuery = "SELECT  * FROM " + TABLE_RECHARGES +
			 * " WHERE " + KEY_MOB_NO + " = " + mob_number + " AND " +
			 * KEY_AMOUNT + " = " + amount + " AND " + KEY_RECHARGE_DATETIME +
			 * " < datetime('now',-1 hours)";
			 */

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				Recharges recharges = new Recharges();
				String mob_number1 = cursor.getString(cursor
						.getColumnIndex(RechargeDBHelper.KEY_MOB_NO));
				Date lastRechargeTime = new Date(
						cursor.getLong(cursor
								.getColumnIndex(RechargeDBHelper.KEY_RECHARGE_DATETIME)));
				Log.d("ss", "dddd   " + lastRechargeTime);

			}

			db.close();
			return false;
		} catch (Exception exc) {
			return false;
		}
	}

	private String getDateTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss", Locale.getDefault());
		Date date = new Date();
		return dateFormat.format(date);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECHARGES);

	}

}
