package com.mindsarray.pay1.databasehandler;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class MostVisitedOperatorDataSource {

	// Database fields
	private SQLiteDatabase database;
	private Pay1SQLiteHelper dbHelper;

	public MostVisitedOperatorDataSource(Context context) {
		dbHelper = new Pay1SQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public void createMVO(int op_id, String op_code, String op_name,
			String rechargeType) {
		ContentValues values = new ContentValues();
		values.put(Pay1SQLiteHelper.OPERATOR_ID, op_id);
		values.put(Pay1SQLiteHelper.OPERATOR_NAME, op_name);
		values.put(Pay1SQLiteHelper.OPERATOR_CODE, op_code);
		values.put(Pay1SQLiteHelper.RECHARGE_TYPE, rechargeType);
		values.put(Pay1SQLiteHelper.RECHARGE_COUNT, 0);

		database.insert(Pay1SQLiteHelper.TABLE_MOST_VISITED_OPERATOR, null,
				values);
		// Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_VMN,
		// null, Pay1SQLiteHelper.VMN_ID + " = " + insertId, null,
		// null, null, null);
		// cursor.moveToFirst();
		// VMN neVMN = cursorToVMN(cursor);
		// cursor.close();
		// return newVMN;
	}

	public int checkCount(int op_code) {
		Cursor mCount = database.rawQuery(
				"select count(*) from recentlyVisited where op_id=" + op_code,
				null);
		mCount.moveToFirst();
		int count = mCount.getInt(0);
		mCount.close();
		return count;

	}

	public List<MostVisitedOperator> getFirstFive() {
		List<MostVisitedOperator> MVOS = new ArrayList<MostVisitedOperator>();

		Cursor cursor = database.query(
				Pay1SQLiteHelper.TABLE_MOST_VISITED_OPERATOR, null, null, null,
				null, null, Pay1SQLiteHelper.RECHARGE_COUNT + " DESC limit 5");

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			MostVisitedOperator MVO = cursorToMOVS(cursor);
			MVOS.add(MVO);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return MVOS;
	}

	private MostVisitedOperator cursorToMOVS(Cursor cursor) {
		MostVisitedOperator movs = new MostVisitedOperator();
		movs.setOperatorId(cursor.getInt(0));
		movs.setOperatorName(cursor.getString(1));
		movs.setOperatorCode(cursor.getString(2));
		movs.setRechargeType(cursor.getString(3));
		movs.setCount(cursor.getInt(cursor
				.getColumnIndex(Pay1SQLiteHelper.RECHARGE_COUNT)));
		return movs;
	}

	public void updateRow(int op_id) {
		Cursor c = database.rawQuery("UPDATE "
				+ Pay1SQLiteHelper.TABLE_MOST_VISITED_OPERATOR + " SET "
				+ Pay1SQLiteHelper.RECHARGE_COUNT + " = "
				+ (Pay1SQLiteHelper.RECHARGE_COUNT + " + 1") + " WHERE "
				+ Pay1SQLiteHelper.OPERATOR_ID + " = \"" + op_id + "\"", null);
		c.close();
	}
}
