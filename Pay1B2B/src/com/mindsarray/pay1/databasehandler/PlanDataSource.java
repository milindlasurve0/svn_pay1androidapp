package com.mindsarray.pay1.databasehandler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.servicehandler.PlanIntentService;
import com.mindsarray.pay1.utilities.Utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


public class PlanDataSource {

	// Database fields
	private SQLiteDatabase database;
	private Pay1SQLiteHelper dbHelper;
	private Context mContext;
	
	public PlanDataSource(Context context) {
		dbHelper = new Pay1SQLiteHelper(context);
		mContext = context;
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}
	
	
	
	
int i=0;
	public void createPlan(String OperatorID, String OperatorName,
			String CircleID, String CircleName, String PlanType, String Amount,
			String Validity, String Description, String Time,String showFlag) {
		
		try{
			ContentValues values = new ContentValues();
			values.put(Pay1SQLiteHelper.PLAN_OPERATOR_ID, OperatorID);
			values.put(Pay1SQLiteHelper.PLAN_OPERATOR_NAME, OperatorName);
			values.put(Pay1SQLiteHelper.PLAN_CIRCLE_ID, CircleID);
			values.put(Pay1SQLiteHelper.PLAN_CIRCLE_NAME, CircleName);
			values.put(Pay1SQLiteHelper.PLAN_TYPE, PlanType);
			values.put(Pay1SQLiteHelper.PLAN_AMOUNT, Amount);
			values.put(Pay1SQLiteHelper.PLAN_VALIDITY, Validity);
			values.put(Pay1SQLiteHelper.PLAN_DESCRIPTION, Description);
			values.put(Pay1SQLiteHelper.PLAN_UPDATE_TIME, Time);
	
			database.insert(Pay1SQLiteHelper.TABLE_PLAN, null, values);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		//Log.d("PLAN SYNC", "row inserted "+ i++);
		// Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_Plan,
		// null, Pay1SQLiteHelper.Plan_ID + " = " + insertId, null,
		// null, null, null);
		// cursor.moveToFirst();
		// Plan nePlan = cursorToPlan(cursor);
		// cursor.close();
		// return newPlan;
	}
	
	public void deleteAndInsertPlan(String OperatorID, String OperatorName,
			String CircleID, String CircleName, String PlanType, String Amount,
			String Validity, String Description, String Time,String showFlag){
		try{
			database.execSQL("	delete from " + Pay1SQLiteHelper.TABLE_PLAN + " where "
								+ Pay1SQLiteHelper.PLAN_OPERATOR_ID + " = '" + OperatorID
								+ "' and " + Pay1SQLiteHelper.PLAN_CIRCLE_ID + " = '" + CircleID
								+ "' and " + Pay1SQLiteHelper.PLAN_AMOUNT + " = '" + Amount + "'");
			
			createPlan(OperatorID, OperatorName, CircleID, CircleName, PlanType, Amount,
					Validity, Description, Time, showFlag);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void deletePlan() {
		database.delete(Pay1SQLiteHelper.TABLE_PLAN, null, null);
	}

/*	public void deletePlan(Plan plan) {
		long id = plan.getPlanID();
		database.delete(Pay1SQLiteHelper.TABLE_PLAN, Pay1SQLiteHelper.PLAN_ID
				+ " = " + id, null);
	}
*/
	public ArrayList<String> getPlanType(String operator, String circle,
			int isDataCardPlan) {
		ArrayList<String> list = new ArrayList<String>();
		Cursor cursor = null;
		if (isDataCardPlan == 0) {// false
			cursor = database.query(Pay1SQLiteHelper.TABLE_PLAN, null,
					Pay1SQLiteHelper.PLAN_OPERATOR_ID + " = '" + operator
							+ "' AND " + Pay1SQLiteHelper.PLAN_CIRCLE_ID
							+ " = '" + circle + "'", null,
					Pay1SQLiteHelper.PLAN_TYPE, null,
					Pay1SQLiteHelper.PLAN_TYPE+" DESC");
		} else {// true
			cursor = database.query(Pay1SQLiteHelper.TABLE_PLAN, null,
					Pay1SQLiteHelper.PLAN_OPERATOR_ID + " = '" + operator
							+ "' AND " + Pay1SQLiteHelper.PLAN_CIRCLE_ID
							+ " = '" + circle + "' AND ("
							+ Pay1SQLiteHelper.PLAN_TYPE
							+ " = 'Data-Plans' OR "
							+ Pay1SQLiteHelper.PLAN_TYPE + " = '3G-Plans')",
					null, Pay1SQLiteHelper.PLAN_TYPE, null,
					Pay1SQLiteHelper.PLAN_TYPE+" DESC");
		}
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Plan plan = cursorToPlan(cursor);
			list.add(plan.getPlanType());
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return list;
	}

	public List<Plan> getAllPlan() {
		List<Plan> plans = new ArrayList<Plan>();

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_PLAN, null, null,
				null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Plan plan = cursorToPlan(cursor);
			plans.add(plan);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return plans;
	}

	
	
	
	public List<Plan> getAllPlan(String operator, String circle) {
		List<Plan> plans = new ArrayList<Plan>();

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_PLAN, null,
				Pay1SQLiteHelper.PLAN_OPERATOR_ID + " = '" + operator
						+ "' AND " + Pay1SQLiteHelper.PLAN_CIRCLE_ID + " = '"
						+ circle + "'", null, null, null, null);

		/*Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_PLAN, null,
				Pay1SQLiteHelper.PLAN_SHOW_FLAG+" '1' AND "+ Pay1SQLiteHelper.PLAN_OPERATOR_ID + " = '" + operator
						+ "' AND " + Pay1SQLiteHelper.PLAN_CIRCLE_ID + " = '"
						+ circle + "'", null, null, null, null);*/
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Plan plan = cursorToPlan(cursor);
			plans.add(plan);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return plans;
	}

	public JSONArray getAllPlanDetails(String operator, String circle,
			String plantype, int isDataCardPlan) {
		ArrayList<HashMap<String, String>> wordList = new ArrayList<HashMap<String, String>>();
		Cursor cursor = null;
		if (plantype.equalsIgnoreCase("") || plantype == null) {
			if (isDataCardPlan == 0) {// false
				cursor = database.query(Pay1SQLiteHelper.TABLE_PLAN, null,
						Pay1SQLiteHelper.PLAN_OPERATOR_ID + " = '" + operator
								+ "' AND " + Pay1SQLiteHelper.PLAN_CIRCLE_ID
								+ " = '" + circle + "'", null, null, null,
						Pay1SQLiteHelper.PLAN_AMOUNT);
			} else {// true
				cursor = database
						.query(Pay1SQLiteHelper.TABLE_PLAN, null,
								Pay1SQLiteHelper.PLAN_OPERATOR_ID + " = '"
										+ operator + "' AND "
										+ Pay1SQLiteHelper.PLAN_CIRCLE_ID
										+ " = '" + circle + "' AND ("
										+ Pay1SQLiteHelper.PLAN_TYPE
										+ " = 'Data-Plans' OR "
										+ Pay1SQLiteHelper.PLAN_TYPE
										+ " = '3G-Plans')", null, null, null,
								Pay1SQLiteHelper.PLAN_AMOUNT);

			}
		} else {
			cursor = database.query(Pay1SQLiteHelper.TABLE_PLAN, null,
					Pay1SQLiteHelper.PLAN_OPERATOR_ID + " = '" + operator
							+ "' AND " + Pay1SQLiteHelper.PLAN_CIRCLE_ID
							+ " = '" + circle + "' AND "
							+ Pay1SQLiteHelper.PLAN_TYPE + " = '" + plantype
							+ "'", null, null, null,
					Pay1SQLiteHelper.PLAN_AMOUNT);
		}

		JSONArray array = new JSONArray();
		JSONObject json1 = null;

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Plan plan = cursorToPlan(cursor);
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("description", plan.getPlanDescription());
			map.put("validity", plan.getPlanValidity());
			map.put("amount", "" + plan.getPlanAmount());
			map.put("plantype", plan.getPlanType());
			wordList.add(map);
			json1 = new JSONObject(map);

			array.put(json1);
			cursor.moveToNext();
		}
		cursor.close();
		// return contact list
		// // Log.e("json", "json " + array);
		return array;
	}

	public List<Plan> getPlanDetails(String operator, String circle,
			int amount, int isDataCardPlan) {
		List<Plan> plans = new ArrayList<Plan>();

		Cursor cursor = null;
		if (isDataCardPlan == 0) {// false
			cursor = database.query(Pay1SQLiteHelper.TABLE_PLAN, null,
					Pay1SQLiteHelper.PLAN_OPERATOR_ID + " = '" + operator
							+ "' AND " + Pay1SQLiteHelper.PLAN_CIRCLE_ID
							+ " = '" + circle + "' AND "
							+ Pay1SQLiteHelper.PLAN_AMOUNT + " = " + amount,
					null, null, null, null);
		} else {// true
			cursor = database.query(Pay1SQLiteHelper.TABLE_PLAN, null,
					Pay1SQLiteHelper.PLAN_OPERATOR_ID + " = '" + operator
							+ "' AND " + Pay1SQLiteHelper.PLAN_CIRCLE_ID
							+ " = '" + circle + "' AND "
							+ Pay1SQLiteHelper.PLAN_AMOUNT + " = " + amount
							+ " AND (" + Pay1SQLiteHelper.PLAN_TYPE
							+ " = 'Data-Plans' OR "
							+ Pay1SQLiteHelper.PLAN_TYPE + " = '3G-Plans')",
					null, null, null, null);
		}
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Plan plan = cursorToPlan(cursor);
			plans.add(plan);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return plans;
	}

	private Plan cursorToPlan(Cursor cursor) {
		Plan plan = new Plan();
		plan.setPlanID(cursor.getInt(0));
		plan.setPlanOpertorID(cursor.getString(1));
		plan.setPlanOperatorName(cursor.getString(2));
		plan.setPlanCircleID(cursor.getString(3));
		plan.setPlanCircleName(cursor.getString(4));
		plan.setPlanType(cursor.getString(5));
		plan.setPlanAmount(cursor.getString(6));
		plan.setPlanValidity(cursor.getString(7));
		plan.setPlanDescription(cursor.getString(8));
		plan.setPlanUptateTime(cursor.getString(9));

		return plan;
	}
	
	public void updatePlansTime(String time){
		try{
			database.execSQL("	update " + Pay1SQLiteHelper.TABLE_PLAN 
							+ " set " + Pay1SQLiteHelper.PLAN_UPDATE_TIME + " = '" + time + "'");
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public boolean savePlansTodataBase(String planJson) {
		try {
			JSONArray array = new JSONArray(planJson);
			JSONObject planJsonObject = array.getJSONObject(0);

			Iterator<String> operatorKey = planJsonObject.keys();
			long time = System.currentTimeMillis();
			if (operatorKey.hasNext()) {
				
				JSONObject jsonObject2 = planJsonObject
						.getJSONObject(operatorKey.next());

				String opr_name = jsonObject2.getString("opr_name");
				String prod_code_pay1 = jsonObject2.getString("prod_code_pay1");

				JSONObject circleJsonObject = jsonObject2.getJSONObject("circles");
				Iterator<String> circleKey = circleJsonObject.keys();
				if (circleKey.hasNext()) {
					JSONObject circleJsonObject2 = circleJsonObject
							.getJSONObject(circleKey.next().toString());
					String circleName = circleJsonObject2
							.getString("circle_name");

					JSONObject plansJsonObject = circleJsonObject2
							.getJSONObject("plans");
					String circle_id = circleJsonObject2.getString("circle_id");
					Iterator<String> planKey = plansJsonObject.keys();
					Iterator<String> planKeyDB = plansJsonObject.keys();

					while (planKey.hasNext()) {
						String planType = planKey.next().toString();
						JSONArray planArray = plansJsonObject
								.getJSONArray(planType);

						for (int i = 0; i < planArray.length(); i++) {
							JSONObject jsonObject = planArray.getJSONObject(i);
							String plan_desc = jsonObject.getString("plan_desc");

							String plan_amt = jsonObject.getString("plan_amt");

							String plan_validity = jsonObject.getString("plan_validity");
							String show_flag = jsonObject.getString("show_flag");
							
							deleteAndInsertPlan(prod_code_pay1, opr_name,
									circle_id, circleName, planType, plan_amt,
									plan_validity, plan_desc, "" + time,show_flag);
						}

					}
					
				}

			}
			return true;
		} catch (JSONException exception) {
			exception.printStackTrace();
			Log.e("Er ", exception.getMessage());
		} catch (SQLException exception) {
			// TODO: handle exception
			Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			// // Log.e("Er ", e.getMessage());
		} finally {
			
		}
		return false;
	}
}
