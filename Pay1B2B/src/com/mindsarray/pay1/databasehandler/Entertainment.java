package com.mindsarray.pay1.databasehandler;

public class Entertainment {

	private int entertainmentID;
	private String entertainmentProductID;
	private String entertainmentProductCode;
	private String entertainmentProductName;
	private String entertainmentProductPrice;
	private String entertainmentProductValidity;
	private String entertainmentProductShortDescription;
	private String entertainmentProductLongDescription;
	private String entertainmentProductImage;
	private String entertainmentProductDataField;
	private String entertainmentProductDataType;
	private String entertainmentProductDataLength;
	private String entertainmentProductUpdateTime;

	public int getEntertainmentID() {
		return entertainmentID;
	}

	public void setEntertainmentID(int entertainmentID) {
		this.entertainmentID = entertainmentID;
	}

	public String getEntertainmentProductID() {
		return entertainmentProductID;
	}

	public void setEntertainmentProductID(String entertainmentProductID) {
		this.entertainmentProductID = entertainmentProductID;
	}

	public String getEntertainmentProductCode() {
		return entertainmentProductCode;
	}

	public void setEntertainmentProductCode(String entertainmentProductCode) {
		this.entertainmentProductCode = entertainmentProductCode;
	}

	public String getEntertainmentProductName() {
		return entertainmentProductName;
	}

	public void setEntertainmentProductName(String entertainmentProductName) {
		this.entertainmentProductName = entertainmentProductName;
	}

	public String getEntertainmentProductPrice() {
		return entertainmentProductPrice;
	}

	public void setEntertainmentProductPrice(String entertainmentProductPrice) {
		this.entertainmentProductPrice = entertainmentProductPrice;
	}

	public String getEntertainmentProductValidity() {
		return entertainmentProductValidity;
	}

	public void setEntertainmentProductValidity(
			String entertainmentProductValidity) {
		this.entertainmentProductValidity = entertainmentProductValidity;
	}

	public String getEntertainmentProductShortDescription() {
		return entertainmentProductShortDescription;
	}

	public void setEntertainmentProductShortDescription(
			String entertainmentProductShortDescription) {
		this.entertainmentProductShortDescription = entertainmentProductShortDescription;
	}

	public String getEntertainmentProductLongDescription() {
		return entertainmentProductLongDescription;
	}

	public void setEntertainmentProductLongDescription(
			String entertainmentProductLongDescription) {
		this.entertainmentProductLongDescription = entertainmentProductLongDescription;
	}

	public String getEntertainmentProductImage() {
		return entertainmentProductImage;
	}

	public void setEntertainmentProductImage(String entertainmentProductImage) {
		this.entertainmentProductImage = entertainmentProductImage;
	}

	public String getEntertainmentProductDataField() {
		return entertainmentProductDataField;
	}

	public void setEntertainmentProductDataField(
			String entertainmentProductDataField) {
		this.entertainmentProductDataField = entertainmentProductDataField;
	}

	public String getEntertainmentProductDataType() {
		return entertainmentProductDataType;
	}

	public void setEntertainmentProductDataType(
			String entertainmentProductDataType) {
		this.entertainmentProductDataType = entertainmentProductDataType;
	}

	public String getEntertainmentProductDataLength() {
		return entertainmentProductDataLength;
	}

	public void setEntertainmentProductDataLength(
			String entertainmentProductDataLength) {
		this.entertainmentProductDataLength = entertainmentProductDataLength;
	}

	public String getEntertainmentProductUpdateTime() {
		return entertainmentProductUpdateTime;
	}

	public void setEntertainmentProductUpdateTime(
			String entertainmentProductUpdateTime) {
		this.entertainmentProductUpdateTime = entertainmentProductUpdateTime;
	}

}