package com.mindsarray.pay1;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.mindsarray.pay1.adapterhandler.BusBookingSummaryAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

public class BusBookingSummaryActivity extends Activity {

	private String source = null, source_id = null, destination = null,
			destination_id = null, date = null, seats = null, bus_id = null,
			bus_name = null, bus_time = null, bus_type = null, bus_fare = null,
			ac = null, nonac = null, sleeper = null, nonsleeper = null,
			arrival_time = null, departure_time = null,
			boarding_point_name = null, boarding_point_id = null,
			boarding_point_time = null, email = null, mobile = null,
			cancellation_policy = null;
	int seat_count = 0;
	private Button button_Book;
	private TextView textView_Route, textView_Date, textView_Boarding,
			textView_Seats, textView_SelectedSeats, textView_BusName,
			textView_Time, textView_BusType, textView_Fare, textView_AC,
			textView_NonAC, textView_Sleeper, textView_NonSleeper;

	String months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
			"Sep", "Oct", "Nov", "Dec" };

	static final int BOARDING_POINT_PICKER_ID = 1;

	private BusBookingSummaryAdapter busBookingSummaryAdapter;
	private ListView listView_Travellers;
	ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

	boolean bNetOrSms;
	String uuid;
	private SharedPreferences mSettingsData;
	long timestamp;

	BusBookingTask busBookingTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.busbookingsummary_activity);

		try {
			Bundle extras = getIntent().getExtras();
			if (extras != null) {
				source = extras.getString("SOURCE");
				source_id = extras.getString("SOURCE_ID");
				destination = extras.getString("DESTINATION");
				destination_id = extras.getString("DESTINATION_ID");
				date = extras.getString("DATE");
				seats = extras.getString("SEATS");
				seat_count = extras.getInt("SEATS_COUNT", 0);
				bus_id = extras.getString(Constants.BUS_ID);
				bus_name = extras.getString(Constants.BUS_NAME);
				bus_time = extras.getString(Constants.BUS_TIME);
				bus_type = extras.getString(Constants.BUS_TYPE);
				bus_fare = extras.getString(Constants.BUS_FARE);
				boarding_point_id = extras
						.getString(Constants.BUS_BOARDING_POINT_ID);
				boarding_point_name = extras
						.getString(Constants.BUS_BOARDING_POINT_NAME);
				boarding_point_time = extras
						.getString(Constants.BUS_BOARDING_POINT_TIME);
				cancellation_policy = extras
						.getString(Constants.BUS_CANCELLATION_POLICY);
				data = (ArrayList<HashMap<String, String>>) getIntent()
						.getSerializableExtra("DATA");
				email = extras.getString("EMAIL");
				mobile = extras.getString("MOBILE");
			}

			try {
				TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
				uuid = tManager.getDeviceId();
				if (uuid == null) {
					uuid = Secure
							.getString(BusBookingSummaryActivity.this
									.getContentResolver(), Secure.ANDROID_ID);
				}
				if (Utility.getUUID(BusBookingSummaryActivity.this,
						Constants.SHAREDPREFERENCE_UUID) == null)
					Utility.setUUID(BusBookingSummaryActivity.this,
							Constants.SHAREDPREFERENCE_UUID, uuid);
			} catch (Exception exception) {
			}

			mSettingsData = getSharedPreferences(
					SettingsSmsActivity.SETTINGS_NAME, 0);
			bNetOrSms = mSettingsData.getBoolean(
					SettingsSmsActivity.KEY_NETWORK, true);
			textView_Route = (TextView) findViewById(R.id.textView_Route);
			textView_Route.setText(source + " to " + destination);
			textView_Date = (TextView) findViewById(R.id.textView_Date);
			textView_Date.setText("Date of Travel : " + date);
			textView_Seats = (TextView) findViewById(R.id.textView_Seats);
			textView_Seats.setText("Number of Travelers : " + seat_count);
			textView_SelectedSeats = (TextView) findViewById(R.id.textView_SelectedSeats);
			textView_SelectedSeats.setText("Selected seats : " + seats);
			textView_BusName = (TextView) findViewById(R.id.textView_BusName);
			textView_BusName.setText(bus_name);
			textView_Time = (TextView) findViewById(R.id.textView_Time);
			textView_Time.setText(bus_time);
			textView_BusType = (TextView) findViewById(R.id.textView_BusType);
			textView_BusType.setText(bus_type);
			textView_Fare = (TextView) findViewById(R.id.textView_Fare);
			if (bus_fare.contains("/")) {
				String[] fare = bus_fare.split("/");
				textView_Fare
						.setText(""
								+ (Integer.parseInt(fare[fare.length - 1]) * seat_count));
			} else {
				textView_Fare.setText(""
						+ (Integer.parseInt(bus_fare) * seat_count));
			}

			textView_Boarding = (TextView) findViewById(R.id.textView_Boarding);
			textView_Boarding.setText(boarding_point_name + " ("
					+ boarding_point_time + ")");

			listView_Travellers = (ListView) findViewById(R.id.listView_Travellers);
			busBookingSummaryAdapter = new BusBookingSummaryAdapter(
					BusBookingSummaryActivity.this, data);
			listView_Travellers.setAdapter(busBookingSummaryAdapter);

			button_Book = (Button) findViewById(R.id.button_Book);
			button_Book.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (bNetOrSms) {

						if (Utility.getLoginFlag(
								BusBookingSummaryActivity.this,
								Constants.SHAREDPREFERENCE_IS_LOGIN)) {
							timestamp = System.currentTimeMillis();
							// busBookingTask = new BusBookingTask();
							// busBookingTask.execute(new String[] { mobile,
							// email, textView_Fare.getText().toString(),
							// source_id, destination_id, bus_id,
							// boarding_point_id, seats,
							// String.valueOf(seat_count) });
							Constants.showOneButtonFailureDialog(
									BusBookingSummaryActivity.this, "Booked.",
									"Bus Booking", Constants.OTHER_ERROR);

						} else {
							Intent intent = new Intent(
									BusBookingSummaryActivity.this,
									LoginActivity.class);
							startActivity(intent);
						}
					} else {
						Constants.showOneButtonFailureDialog(
								BusBookingSummaryActivity.this,
								"Please enable internet settings.",
								"Bus Booking", Constants.OTHER_ERROR);
					}
				}
			});
		} catch (Exception exception) {

		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		// if (resultCode == RESULT_CANCELED) {
		// mBtnLogin.setVisibility(View.GONE);
		// mBtnLogOut.setVisibility(View.VISIBLE);
		//
		// }
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants.showNavigationBar(BusBookingSummaryActivity.this);
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	public class BusBookingTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			// String response = RequestClass
			// .getInstance()
			// .readPay1Request(
			// BusBookingSummaryActivity.this,
			// Constants.BUS_API_URL
			// +
			// "method=busBooking&action=blockTicket&mobileNumber=9769059438&product=36&operator=1&subId=9769959488&amount="
			// + params[0]
			// +
			// "&circle=&special=0&type=flexi&Title0=-1&fname0=Hridayesh&sex0=male&age0=28&Title1=-1&fname1=Sandeep&sex1=male&age1=28&mobile=9004777614&email_id=hridayesh.mishra%40gmail.com&address=MindsArray+Pvt.+Ltd.&id_no=ASDF12345&id_proof=Pan+Card&chosensource="
			// + params[1] + "&chosendestination="
			// + params[2] + "&chosenbus=" + params[3]
			// + "&boardingpointsList=" + params[4]
			// + "&chkchk=2&seatnames=" + params[5]);

			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair.add(new BasicNameValuePair("method", "busBooking"));
			listValuePair.add(new BasicNameValuePair("action", "bookTicket"));
			listValuePair.add(new BasicNameValuePair("mobile", params[0]));
			listValuePair.add(new BasicNameValuePair("email_id", params[1]));
			listValuePair.add(new BasicNameValuePair("amount", params[2]));
			listValuePair
					.add(new BasicNameValuePair("chosensource", params[3]));
			listValuePair.add(new BasicNameValuePair("chosendestination",
					params[4]));
			listValuePair.add(new BasicNameValuePair("chosenbus", params[5]));
			listValuePair.add(new BasicNameValuePair("boardingpointsList",
					params[6]));
			listValuePair.add(new BasicNameValuePair("seatnames", params[7]));
			listValuePair.add(new BasicNameValuePair("chkchk", params[8]));

			listValuePair.add(new BasicNameValuePair("type", "flexi"));

			for (int i = 0; i < data.size(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				map = data.get(i);
				if (map.get(Constants.BUS_TRAVELLER_GENDER).equalsIgnoreCase(
						"male"))
					listValuePair
							.add(new BasicNameValuePair("Title" + i, "Mr."));
				else
					listValuePair
							.add(new BasicNameValuePair("Title" + i, "Ms."));
				listValuePair.add(new BasicNameValuePair("fname" + i, map.get(
						Constants.BUS_TRAVELLER_NAME).toString()));
				listValuePair.add(new BasicNameValuePair("sex" + i, map.get(
						Constants.BUS_TRAVELLER_GENDER).toString()));
				listValuePair.add(new BasicNameValuePair("age" + i, map.get(
						Constants.BUS_TRAVELLER_AGE).toString()));
			}

			listValuePair.add(new BasicNameValuePair("timestamp", String
					.valueOf(timestamp)));
			listValuePair.add(new BasicNameValuePair("profile_id", Utility
					.getProfileID(BusBookingSummaryActivity.this,
							Constants.SHAREDPREFERENCE_PROFILE_ID)));
			listValuePair.add(new BasicNameValuePair("hash_code", Constants
					.encryptPassword(Utility.getUUID(
							BusBookingSummaryActivity.this,
							Constants.SHAREDPREFERENCE_UUID)
							+ params[0] + params[2] + timestamp)));
			listValuePair.add(new BasicNameValuePair("device_type", "android"));

			String response = RequestClass.getInstance().readPay1Request(
					BusBookingSummaryActivity.this, Constants.API_URL,
					listValuePair);

			return response;

		}

		@Override
		protected void onPostExecute(String result) {
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			super.onPostExecute(result);
			try {
				// Log.w("REsp ", result);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(BusBookingSummaryActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							BusBookingTask.this.cancel(true);
							finish();
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			BusBookingTask.this.cancel(true);
			dialog.cancel();
			finish();
		}

	}
}
