package com.mindsarray.pay1;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.AddressDataSource;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.EditTextValidator;
import com.mindsarray.pay1.utilities.MyLocation;
import com.mindsarray.pay1.utilities.MyLocation.LocationResult;
import com.mindsarray.pay1.utilities.Utility;

public class SettingsUpdateAddressActivity extends FragmentActivity {

	private EditText editAddress, editArea, editCity, editState, editZip;
	String longitude = "", latitude = "";
	String provider;
	private Button btnSave, btnCancel;
	// Google Map
	private GoogleMap googleMap;

	private final int MAP_INTENT = 1;
	private Location location = null;
	private AddressDataSource addressDataSource;
	boolean bNetOrSms;
	private SharedPreferences mSettingsData;
	private static final String TAG = "Update address";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settingsupdateaddress_activity);
		try {
			//analytics = GoogleAnalytics.getInstance(this);
			tracker = Constants.setAnalyticsTracker(this, tracker);
			addressDataSource = new AddressDataSource(
					SettingsUpdateAddressActivity.this);
			mSettingsData = getSharedPreferences(
					SettingsSmsActivity.SETTINGS_NAME, 0);
			bNetOrSms = mSettingsData.getBoolean(
					SettingsSmsActivity.KEY_NETWORK, true);

			editAddress = (EditText) findViewById(R.id.editAddress);
			editAddress.addTextChangedListener(new TextWatcher() {
				public void afterTextChanged(Editable s) {
					EditTextValidator.hasText(
							SettingsUpdateAddressActivity.this, editAddress,
							Constants.ERROR_ADDRESS_BLANK_FIELD);
				}

				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
				}

				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
				}
			});
			editArea = (EditText) findViewById(R.id.editArea);
			editArea.addTextChangedListener(new TextWatcher() {
				public void afterTextChanged(Editable s) {
					EditTextValidator.hasText(
							SettingsUpdateAddressActivity.this, editArea,
							Constants.ERROR_AREA_BLANK_FIELD);
				}

				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
				}

				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
				}
			});
			editCity = (EditText) findViewById(R.id.editCity);
			editCity.addTextChangedListener(new TextWatcher() {
				public void afterTextChanged(Editable s) {
					EditTextValidator.hasText(
							SettingsUpdateAddressActivity.this, editCity,
							Constants.ERROR_CITY_BLANK_FIELD);
				}

				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
				}

				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
				}
			});
			editState = (EditText) findViewById(R.id.editState);
			editState.addTextChangedListener(new TextWatcher() {
				public void afterTextChanged(Editable s) {
					EditTextValidator.hasText(
							SettingsUpdateAddressActivity.this, editState,
							Constants.ERROR_STATE_BLANK_FIELD);
				}

				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
				}

				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
				}
			});
			editZip = (EditText) findViewById(R.id.editZip);
			editZip.addTextChangedListener(new TextWatcher() {
				public void afterTextChanged(Editable s) {
					EditTextValidator.hasText(
							SettingsUpdateAddressActivity.this, editZip,
							Constants.ERROR_ZIP_BLANK_FIELD);
				}

				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
				}

				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
				}
			});
			btnSave = (Button) findViewById(R.id.btnSave);
			btnSave.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (!checkValidation(SettingsUpdateAddressActivity.this)) {
					} else {
						if (bNetOrSms) {

							if (Utility.getLoginFlag(
									SettingsUpdateAddressActivity.this,
									Constants.SHAREDPREFERENCE_IS_LOGIN)) {
								new UpdateAddressTask().execute();
							} else {
								Intent intent = new Intent(
										SettingsUpdateAddressActivity.this,
										LoginActivity.class);
								startActivity(intent);
							}

						}
					}

				}
			});

			btnCancel = (Button) findViewById(R.id.btnCancel);
			btnCancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					finish();
				}
			});

			setUpMapIfNeeded();

			try {
				addressDataSource.open();

				if (addressDataSource.getAllAddress().size() == 0) {
					LocationResult locationResult = new LocationResult() {
						@Override
						public void gotLocation(Location loc) {
							// Got the location!

							location = loc;
							Log.e("Location", location + "");
							setUpMap(loc);

						}
					};
					MyLocation myLocation = new MyLocation();
					myLocation.getLocation(this, locationResult);

				} else {
					List<com.mindsarray.pay1.databasehandler.Address> addresses = addressDataSource
							.getAllAddress();

					Location loc = new Location("test");
					loc.setLatitude(Double.parseDouble(addresses.get(0)
							.getAddressLatitude()));
					loc.setLongitude(Double.parseDouble(addresses.get(0)
							.getAddressLongitude()));
					location = loc;
					setUpMap(loc);

					editAddress.setText(addresses.get(0).getAddressLine());
					editArea.setText(addresses.get(0).getAddressArea());
					editCity.setText(addresses.get(0).getAddressCity());
					editState.setText(addresses.get(0).getAddressState());
					editZip.setText(addresses.get(0).getAddressZip());
				}
			} catch (SQLException exception) {
				// TODO: handle exception
				// // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // Log.e("Er ", e.getMessage());
			} finally {
				addressDataSource.close();
			}

		} catch (Exception exception) {
		}
	}

	public static GoogleAnalytics analytics;
	public static Tracker tracker;

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
		// EasyTracker.getInstance(this).activityStart(this);
		//tracker.setScreenName(TAG);
		Constants.trackOnStart(tracker, TAG);
	}

	private boolean checkValidation(Context context) {
		boolean ret = true;

		if (!EditTextValidator.hasText(context, editAddress,
				Constants.ERROR_ADDRESS_BLANK_FIELD)) {
			ret = false;
			editAddress.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editArea,
				Constants.ERROR_AREA_BLANK_FIELD)) {
			ret = false;
			editArea.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editCity,
				Constants.ERROR_CITY_BLANK_FIELD)) {
			ret = false;
			editCity.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editState,
				Constants.ERROR_STATE_BLANK_FIELD)) {
			ret = false;
			editState.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editZip,
				Constants.ERROR_ZIP_BLANK_FIELD)) {
			ret = false;
			editZip.requestFocus();
			return ret;
		} else
			return ret;
	}

	private void setUpMapIfNeeded() {
		try {

			// Do a null check to confirm that we have not already instantiated
			// the
			// map.
			if (googleMap == null) {
				// Try to obtain the map from the SupportMapFragment.
				googleMap = ((SupportMapFragment) getSupportFragmentManager()
						.findFragmentById(R.id.map)).getMap();
				googleMap.getUiSettings().setZoomControlsEnabled(false);
				googleMap.getUiSettings().setScrollGesturesEnabled(false);
				googleMap.getUiSettings().setAllGesturesEnabled(false);
				googleMap
						.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

							@Override
							public void onMapClick(LatLng arg0) {
								// TODO Auto-generated method stub
								Intent intent = new Intent(
										SettingsUpdateAddressActivity.this,
										SettingsMapActivity.class);
								intent.putExtra("LOCATION", location);
								startActivityForResult(intent, MAP_INTENT);
							}
						});
				// Check if we were successful in obtaining the map.
				if (googleMap != null) {
					// setUpMap();
				}
			} else {
				// setUpMap(location);
			}
		} catch (Exception exception) {
		}
	}

	private void setUpMap(Location location) {
		try {

			double lat = location.getLatitude(), lng = location.getLongitude();
			latitude = String.valueOf(location.getLatitude());
			longitude = String.valueOf(location.getLongitude());
			Log.e("lat long:", latitude + longitude);
			Utility.setLatitude(SettingsUpdateAddressActivity.this,
					Constants.SHAREDPREFERENCE_LATITUDE, latitude);
			Utility.setLongitude(SettingsUpdateAddressActivity.this,
					Constants.SHAREDPREFERENCE_LONGITUDE, longitude);

			LatLng latLng = new LatLng(lat, lng);

			googleMap.clear();
			MarkerOptions options = new MarkerOptions();
			options.position(latLng);
			// options.title("You are here");
			options.draggable(false);
			options.icon(BitmapDescriptorFactory
					.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
			// options.icon(BitmapDescriptorFactory.fromResource(R.drawable.close));
			googleMap.addMarker(options);

			// CameraPosition camPos = new CameraPosition.Builder()
			// .target(new LatLng(latitude, longitude)).zoom(18)
			// .bearing(location.getBearing()).tilt(70).build();

			CameraPosition camPos = new CameraPosition.Builder().target(latLng)
					.zoom(15).build();

			CameraUpdate camUpd3 = CameraUpdateFactory
					.newCameraPosition(camPos);

			googleMap.animateCamera(camUpd3);

			Geocoder geocoder;
			List<Address> addresses;
			geocoder = new Geocoder(SettingsUpdateAddressActivity.this,
					Locale.getDefault());
			addresses = geocoder.getFromLocation(lat, lng, 1);

			String address = addresses.get(0).getAddressLine(0);
			String area = addresses.get(0).getSubLocality();
			String city = addresses.get(0).getLocality();
			String state = addresses.get(0).getAdminArea();
			// String country = addresses.get(0).getCountryName();
			String zip = addresses.get(0).getPostalCode();

			// Log.w("Address: ", "Add : " + address + " City :" + city
			// + " Country :" + country + " Zip :" + zip);
			editAddress.setText(address);
			editArea.setText(area);
			editCity.setText(city);
			editState.setText(state);
			editZip.setText(zip);

		} catch (Exception exception) {
			// Log.w("Error", exception.getMessage());
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		try {
			if (requestCode == MAP_INTENT) {
				if (resultCode == RESULT_OK) {
					location = data.getExtras().getParcelable("LOCATION");
					setUpMap(location);
				}
			}
		} catch (Exception exception) {
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants.showNavigationBar(SettingsUpdateAddressActivity.this);
		// setUpMapIfNeeded();
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	public class UpdateAddressTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair.add(new BasicNameValuePair("method",
					"updateRetailerAddress"));
			listValuePair.add(new BasicNameValuePair("mobile", Utility
					.getMobileNumber(SettingsUpdateAddressActivity.this,
							Constants.SHAREDPREFERENCE_MOBILE_NUMBER)));
			listValuePair.add(new BasicNameValuePair("longitude", longitude));
			listValuePair.add(new BasicNameValuePair("latitude", latitude));
			listValuePair.add(new BasicNameValuePair("address", editAddress
					.getText().toString().trim()));
			listValuePair.add(new BasicNameValuePair("area", editArea.getText()
					.toString().trim()));
			listValuePair.add(new BasicNameValuePair("city", editCity.getText()
					.toString().trim()));
			listValuePair.add(new BasicNameValuePair("state", editState
					.getText().toString().trim()));
			listValuePair.add(new BasicNameValuePair("pincode", editZip
					.getText().toString().trim()));

			String response = RequestClass.getInstance().readPay1Request(
					SettingsUpdateAddressActivity.this, Constants.API_URL,
					listValuePair);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}

			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = array.getJSONObject(0);
					String status = jsonObject.getString("status");

					// ContentValues contentValues = new ContentValues();
					// contentValues.put("notification_msg", rechargeString);
					// contentValues.put("timestamp ", "12345678902");
					// contentValues.put("netOrSms ", 0);
					// controller.insertNotifications(contentValues);
					if (status.equalsIgnoreCase("success")) {
						try {
							addressDataSource.open();

							JSONObject jsonObject2 = jsonObject
									.getJSONObject("description");
							String area = jsonObject2.getString("area");
							String city = jsonObject2.getString("city");
							String state = jsonObject2.getString("state");
							String lat = jsonObject2.getString("latitude");
							String lng = jsonObject2.getString("longitude");
							String zip = jsonObject2.getString("pincode");
							List<com.mindsarray.pay1.databasehandler.Address> addresses = addressDataSource
									.getAllAddress();
							if (addresses.size() == 0) {
								addressDataSource.createAddress(editAddress
										.getText().toString().trim(), area,
										city, state, editZip.getText()
												.toString().trim(), lat, lng);
							} else {
								addressDataSource.updateAddress(editAddress
										.getText().toString().trim(), area,
										city, state, zip, lat, lng, addresses
												.get(0).getAddressID());
							}
						} catch (SQLException exception) {
							// TODO: handle exception
							// // Log.e("Er ", exception.getMessage());
						} catch (Exception e) {
							// TODO: handle exception
							// // Log.e("Er ", e.getMessage());
						} finally {
							addressDataSource.close();
						}
						Constants.showOneButtonSuccessDialog(
								SettingsUpdateAddressActivity.this,
								"Address updated successfully",
								Constants.RECHARGE_SETTINGS);
					} else {
						Constants.showOneButtonFailureDialog(
								SettingsUpdateAddressActivity.this,
								Constants.checkCode(replaced), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(
							SettingsUpdateAddressActivity.this,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(
						SettingsUpdateAddressActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(
						SettingsUpdateAddressActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(SettingsUpdateAddressActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							UpdateAddressTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			UpdateAddressTask.this.cancel(true);
			dialog.cancel();
		}

	}
}
