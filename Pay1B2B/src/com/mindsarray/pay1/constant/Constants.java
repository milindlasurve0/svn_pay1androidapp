package com.mindsarray.pay1.constant;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.SSLHandshakeException;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.mindsarray.pay1.AnalyticsApplication;
import com.mindsarray.pay1.DthRechargeTabFragment;
import com.mindsarray.pay1.EntertainmentRechargeTabFragment;
import com.mindsarray.pay1.LogOutActivity;
import com.mindsarray.pay1.LoginActivity;
import com.mindsarray.pay1.LoginDialogActivity;
import com.mindsarray.pay1.MainActivity;
import com.mindsarray.pay1.MobileBillTabFragment;
import com.mindsarray.pay1.MobileRechargeTabFragment;
import com.mindsarray.pay1.NotificationActivity;
import com.mindsarray.pay1.PGActivity;
import com.mindsarray.pay1.R;
import com.mindsarray.pay1.RegistrationActivity;
import com.mindsarray.pay1.RegistrationLayout;
import com.mindsarray.pay1.RetailerKYCActivity;
import com.mindsarray.pay1.SearchActivity;
import com.mindsarray.pay1.SettingsMainActivity;
import com.mindsarray.pay1.SettingsSmsActivity;
import com.mindsarray.pay1.SettingsUpdatePinActivity;
import com.mindsarray.pay1.SplashScreenActivity;
import com.mindsarray.pay1.SupportActivity;
import com.mindsarray.pay1.UtilityBillTabFragment;
import com.mindsarray.pay1.WalletTabFragment;
import com.mindsarray.pay1.MainActivity.GetComplaintStats;
import com.mindsarray.pay1.adapterhandler.MenuAdapter;
import com.mindsarray.pay1.adapterhandler.RechargeMainAdapter;
import com.mindsarray.pay1.databasehandler.MostVisitedOperatorDataSource;
import com.mindsarray.pay1.databasehandler.NotificationDataSource;
import com.mindsarray.pay1.databasehandler.VMNDataSource;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.CollapseAnimation;
import com.mindsarray.pay1.utilities.ExpandAnimation;
import com.mindsarray.pay1.utilities.GridMenuItem;
import com.mindsarray.pay1.utilities.Utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnDismissListener;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.SQLException;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.ParcelFormatException;
import android.os.StatFs;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Constants {

	public static final String SENDER_ID = "147201449673";// "545526596212";
	// public static final String DATABASE_INIT_TIME =
	// "1429747200000";//"1445341064351";//"1429747200000";//"1429833600000";//"1429025373000";

	public static final String GOOGLE_MAPS_BROWSER_API_KEY = "AIzaSyC4Q1dpyD0zDXnK5j9aCx4WHM-Ib57WQoc";

	public static final String REGISTRATION_COMPLETE = "registrationComplete";

	public static final String PAY1_CUSTOMER_CARE = "022 67242288";
	public static ArrayList<String> notificationList = new ArrayList<String>();
	static NotificationDataSource notificationDataSource;
	// public static final String IS_LOGIN =
	// Constants.SHAREDPREFERENCE_IS_LOGIN;
	public final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	public static int RECHARGE_TYPE;
	public static String SERVICE_TYPE = "1";
	public static final int LOGIN = 100;
	public static final String DATE = "date";
	public static final String ENTERTAINMENT = "entertainment";
	public static final String NETWORK = "Network";
	public static final String SMS = "sms";
	public static final String PLAN_JSON = "plan_json";
	public static final int PLAN_REQUEST = 200;
	public static final String IS_RESET = "isReset";
	public static String OPERATOR_ID = "operator_id";
	public static String OPERATOR_LOGO = "operator_logo";
	public static String OPERATOR_NAME = "operator_name";
	public static String IS_STATUS_TAB = "status_tab";
	public static String REQUEST_FOR = "requestFor";
	public static String PG_DATA = "pgData";
	public static String REQUEST_FOR_TAB = "requestFor";

	public static String PRODUCT_NAME = "productName";
	public static String PRODUCT_ID = "productId";
	public static String PRODUCT_DETAILS = "productDetails";

	public static int MOBILE_TAB = 1;
	public static int DTH_TAB = 2;
	public static int ENTERTAINMENT_TAB = 3;
	public static int BILL_TAB = 4;
	public static int STATUS_TAB = 1;
	public static int REQUEST_TAB = 2;
	public static int RECHARGE_TAB = 0;
	public static String IS_REQUEST_TAB = "request_tab";

	public static final int OPERATOR_AIRCEL = 1;
	public static final int OPERATOR_AIRTEL = 2;
	public static final int OPERATOR_BSNL = 3;
	public static final int OPERATOR_IDEA = 4;
	public static final int OPERATOR_LOOP = 5;
	public static final int OPERATOR_MTS = 6;
	public static final int OPERATOR_RELIANCE_CDMA = 7;
	public static final int OPERATOR_RELIANCE_GSM = 8;
	public static final int OPERATOR_DOCOMO = 9;
	public static final int OPERATOR_TATA_INDICOM = 10;
	public static final int OPERATOR_UNINOR = 11;
	public static final int OPERATOR_VIDEOCON = 12;
	public static final int OPERATOR_VODAFONE = 15;
	public static final int OPERATOR_MTNL = 30;

	public static final int OPERATOR_DTH_AIRTEL = 16;
	public static final int OPERATOR_DTH_BIG = 17;
	public static final int OPERATOR_DTH_DISHTV = 18;
	public static final int OPERATOR_DTH_SUN = 19;
	public static final int OPERATOR_DTH_TATA_SKY = 20;
	public static final int OPERATOR_DTH_VIDEOCON = 21;

	public static final int OPERATOR_BILL_DOCOMO = 36;
	public static final int OPERATOR_BILL_LOOP = 37;
	public static final int OPERATOR_BILL_CELLONE = 38;
	public static final int OPERATOR_BILL_IDEA = 39;
	public static final int OPERATOR_BILL_TATATELESERVICE = 40;
	public static final int OPERATOR_BILL_VODAFONE = 41;
	public static final int OPERATOR_BILL_AIRTEL = 42;
	public static final int OPERATOR_BILL_RELIANCE = 43;

	public static final int OPERATOR_PAY1_WALLET = 44;
	public static final int OPERATOR_ONGO_WALLET = 65;
	
	public static final int OPERATOR_UITLITY_RELIANCE = 45;
	public static final int OPERATOR_UITLITY_BSES_RAJDHANI = 46;
	public static final int OPERATOR_UITLITY_BSES_YAMUNA = 47;
	public static final int OPERATOR_UITLITY_NDPL = 48;
	public static final int OPERATOR_UITLITY_AIRTEL = 49;
	public static final int OPERATOR_UITLITY_MTNL_DELHI = 50;
	public static final int OPERATOR_UITLITY_MAHANAGAR_GAS = 51;

	public static int RECHARGE_MOBILE = 1;
	public static int RECHARGE_DTH = 2;
	public static int RECHARGE_ENTERTAINMENT = 3;
	public static int BILL_PAYMENT = 4;
	public static int WALLET_PAYMENT = 5;
	public static int BILL_UTILITY = 6;
	public static int CHAT_SETTINGS = 9;
	public static String LoginDescription = "Desc";

	// public static String URL = "http://dev.pay1.in/";
	public static String URL = "http://apptesting.pay1.in/";
	
	public static String KYC_URL = "https://panel.pay1.in/";
	public static String MPOS_URL = "https://ext.pay1.in/";
	// public static String URL = "http://192.168.0.158/";
	// public static String URL = "http://cc.pay1.in/";
	public static String URL_API_PART = "apis/receiveWeb/mindsarray/mindsarray/json?";
	public static String CC_API_URL = URL+ URL_API_PART;
	// public static String CC_API_URL = "http://192.168.0.158/" + URL_API_PART;

	// public static String URL = "http://dev.pay1.in/";
	// public static String URL="http://192.168.0.189/";
	// public static String CC_API_URL = "http://192.168.0.189/" + URL_API_PART;
	// public static String URL="http://192.168.0.61/";
	public static String API_URL = URL
			+ "apis/receiveWeb/mindsarray/mindsarray/json?";
	public static String MPOS_URL_FULL = MPOS_URL
			+ "apis/receiveWeb/mindsarray/mindsarray/json?";
	/*
	 * public static String API_URL =
	 * "/apis/receiveWeb/mindsarray/mindsarray/json?";
	 */

	// public static String API_URL =
	// "http://panel.activestores.in/apis/receiveWeb/mindsarray/mindsarray/json?";

	public static final String BUS_CITY_ID = "city_id";
	public static final String BUS_CITY_NAME = "city_name";
	public static final String BUS_BOARDING_POINT_ID = "boarding point id";
	public static final String BUS_BOARDING_POINT_NAME = "boarding point name";
	public static final String BUS_BOARDING_POINT_TIME = "boarding point time";
	public static final String BUS_SEAT_ROW_LENGTH = "row_len";
	public static final String BUS_SEAT_COLUMN_LENGTH = "column_len";
	public static final String BUS_SEAT_ROW = "row";
	public static final String BUS_SEAT_COLUMN = "column";
	public static final String BUS_SEAT_AVAILABLE = "available";
	public static final String BUS_SEAT_LADIES = "ladiesSeat";
	public static final String BUS_SEAT_NAME = "name";
	public static final String BUS_SEAT_ZINDEX = "zIndex";
	public static final String BUS_ID = "bus id";
	public static final String BUS_NAME = "bus name";
	public static final String BUS_TIME = "bus time";
	public static final String BUS_TYPE = "bus type";
	public static final String BUS_DURATION = "bus duration";
	public static final String BUS_SEATS = "bus seats";
	public static final String BUS_FARE = "bus fare";
	public static final String BUS_AC = "bus ac";
	public static final String BUS_NONAC = "bus non ac";
	public static final String BUS_SLEEPER = "bus sleeper";
	public static final String BUS_NONSLEEPER = "bus non sleeper";
	public static final String BUS_ARRIVAL_TIME = "arrival time";
	public static final String BUS_CANCELLATION_POLICY = "cancellation policy";
	public static final String BUS_DEPARTURE_TIME = "departure time";
	public static final String BUS_TRAVELLER_NAME = "name";
	public static final String BUS_TRAVELLER_AGE = "age";
	public static final String BUS_TRAVELLER_GENDER = "gender";

	public static final String DEAL_NAME = "name";
	public static final String DEAL_ID = "id";

	public static final String ERROR_INTERNET = "Error! Please check your internet connection.";
	public static final String ERROR_SERVER = "Error connecting to pay1 server. Please try again.";
	public static final String ERROR_MESSAGE_FOR_ALL_REQUEST = "Some error has occured. Please try again.";
	public static final String SHAREDPREFERENCE_PROFILE_ID = "PROFILE_ID";
	public static final String SHAREDPREFERENCE_RETAILER_ID = "retailer_id";
	public static final String SHAREDPREFERENCE_MOBILE_NUMBER = "MOBILE_NUMBER";
	public static final String SHAREDPREFERENCE_MOBILE_NUMBER_NEW = "MOBILE_NUMBER_NEW";
	public static final String SHAREDPREFERENCE_TRIAL_FLAG = "trial_flag";
	public static final String SHAREDPREFERENCE_CREATED_ON = "created_on";
	public static final String SHAREDPREFERENCE_PG_ABLE = "PG_ABLE";
	public static final String SHAREDPREFERENCE_IS_LOGIN = "isLogin";
	public static final String SHAREDPREFERENCE_BALANCE = "Balance";
	public static final String SHAREDPREFERENCE_USER_NAME = "User_Name";
	public static final String SHAREDPREFERENCE_COOKIE_VERSION = "version";
	public static final String SHAREDPREFERENCE_COOKIE_NAME = "name";
	public static final String SHAREDPREFERENCE_COOKIE_VALUE = "value";
	public static final String SHAREDPREFERENCE_COOKIE_DOMAIN = "domain";
	public static final String SHAREDPREFERENCE_COOKIE_PATH = "path";
	public static final String SHAREDPREFERENCE_COOKIE_EXPIRY = "expiry";
	public static final String SHAREDPREFERENCE_CHAT_TIME = "chat_time";
	public static final String SHAREDPREFERENCE_CHAT_FLAG = "chat_flag";
	public static final String SHAREDPREFERENCE_CHAT_SERVICE_TIME = "chat_service";
	public static final String SHAREDPREFERENCE_CHAT_SERVICE_ID = "chat_id";
	public static final String SHAREDPREFERENCE_CITY_LAST_UPDATE_TIME = "city_time";
	public static final String SHAREDPREFERENCE_LATITUDE = "latitude";
	public static final String SHAREDPREFERENCE_LONGITUDE = "longitude";
	public static final String SHAREDPREFERENCE_CURRENT_LATITUDE = "clat";
	public static final String SHAREDPREFERENCE_CURRENT_LONGITUDE = "clng";
	public static final String SHAREDPREFERENCE_DOCUMENTS = "documents";
	public static final String SHAREDPREFERENCE_GCMID = "gcm";
	public static final String SHAREDPREFERENCE_UUID = "uuid";
	public static final String SHAREDPREFERENCE_MS = "milisec";
	public static final String SHAREDPREFERENCE_OS_VERSION = "od_version";
	public static final String SHAREDPREFERENCE_OS_MANUFACTURER = "os_manufacturer";
	public static final String SHAREDPREFERENCE_DEVICE_SERIAL_NO = "device_serial_no";

	public static final String SHAREDPREFERENCE_LAST_NOTIFICATION = "last_notification";
	public static final String SHAREDPREFERENCE_LAST_5_TRANSACTIONS = "last_5_transactions";
	public static final String SHAREDPREFERENCE_COMPLAINT_STATS = "complaint_stats";

	public static final String SHAREDPREFERENCE_COOKIE_NAME_RESPONSE = "name_response";
	public static final String SHAREDPREFERENCE_COOKIE_VALUE_RESPONSE = "value_response";

	public static final String SHAREDPREFERENCE_RETAILER_FORM_INTERESTED_IN = "retailer_form_interested_in";
	public static final String SHAREDPREFERENCE_RETAILER_FORM_NAME = "retailer_form_name";
	public static final String SHAREDPREFERENCE_RETAILER_FORM_EMAIL = "retailer_form_email";
	public static final String SHAREDPREFERENCE_RETAILER_FORM_MOBILE = "retailer_form_mobile";
	public static final String SHAREDPREFERENCE_RETAILER_FORM_CITY = "retailer_form_city";
	public static final String SHAREDPREFERENCE_RETAILER_FORM_STATE = "retailer_form_state";
	public static final String SHAREDPREFERENCE_RETAILER_FORM_MESSAGE = "retailer_form_message";

	public static final String ERROR_NAME_BLANK_FIELD = "Please enter name.";
	public static final String ERROR_EMAIL_BLANK_FIELD = "Please enter email.";
	public static final String ERROR_EMAIL_VALID_FIELD = "Please enter valid email.";
	public static final String ERROR_MOBILE_BLANK_FIELD = "Please enter mobile number.";
	public static final String ERROR_PHONE_BLANK_FIELD = "Please enter phone number.";
	public static final String ERROR_ACCOUNT_BLANK_FIELD = "Please enter account number.";
	public static final String ERROR_CYCLE_BLANK_FIELD = "Please enter cycle number.";
	public static final String ERROR_PIN_BLANK_FIELD = "Please enter pin.";
	public static final String ERROR_AMOUNT_BLANK_FIELD = "Please enter amount.";
	public static final String ERROR_MOBILE_LENGTH_FIELD = "Please enter a valid mobile number.";
	public static final String ERROR_SUB_ID_BLANK_FIELD = "Sub ID can not be blank.";
	public static final String ERROR_ADDRESS_BLANK_FIELD = "Please enter address.";
	public static final String ERROR_AREA_BLANK_FIELD = "Please enter area.";
	public static final String ERROR_CITY_BLANK_FIELD = "Please enter city.";
	public static final String ERROR_STATE_BLANK_FIELD = "Please enter state.";
	public static final String ERROR_ZIP_BLANK_FIELD = "Please enter zip code.";
	public static final String ERROR_LOGIN_PIN_BLANK_FIELD = "Please enter pin code.";
	public static final String ERROR_LOGIN_PIN_VALID_FIELD = "Pin must be more than 4 characters.";
	public static final String ERROR_BUS_CITY_FIELD = "Please enter travel city.";
	public static final String ERROR_BUS_TRAVEL_NAME_FIELD = "Please enter  traveller's name.";
	public static final String ERROR_BUS_TRAVEL_AGE_FIELD = "Please enter  traveller's age.";
	public static final String ERROR_BUS_TRAVEL_GENDER_FIELD = "Please enter  traveller's gender.";

	public static final String LAST_CIRCLE_UPDATED = "circle_updated_time";
	public static final String LAST_PLANS_UPDATED = "plans_updated_time";

	public static final String PG_SERVICE_CHARGE = "pg_service_charge";

	public static final String PG_SUCCESS = "pg_success";

	public static int OTHER_ERROR = 1;

	public static int SMS_SETTINGS = 0;
	public static int RECHARGE_SETTINGS = 1;
	public static int QUICK_SETTINGS = 2;
	public static int CHANGE_PIN_SETTINGS = 3;
	public static int COMPLAINT_SETTINGS = 5;
	public static int DEAL_SETTINGS = 6;
	public static int MAP_SETTINGS = 7;
	public static int PG_SETTINGS = 8;
	public static int OTP_SETTINGS = 11;
	public static int DISTRIBUTOR_SETTINGS = 9;
	public static int COUNT_NOTIFY = 0;
	public static int COUNT_CHAT = 0;

	public static boolean isExpanded = false;

	private static ListView listView_Menu;
	// private static GridView gridview_Menu;
	private static RelativeLayout slidingPanel;
	private static RelativeLayout home;
	private static View toggleView;

	public static final int SUCCESS_RESULT = 0;
	public static final int FAILURE_RESULT = 1;
	public static final String PACKAGE_NAME = "com.mindsarray.pay1";
	public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
	public static final String RESULT_DATA_KEY = PACKAGE_NAME
			+ ".RESULT_DATA_KEY";
	public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME
			+ ".LOCATION_DATA_EXTRA";

	public static final int REQUEST_GET_MAP_LOCATION = 123;
	public static final int REQUEST_IMAGE_CAPTURE = 1;
	public static final int SELECT_PHOTO = 100;

	public static int getBitmapById(int id, Context context) {
		switch (id) {
		case 1:
			return R.drawable.m_aircel;
		case 2:
			return R.drawable.m_airtel;
		case 3:
		case 34:
			return R.drawable.m_bsnl;
		case 4:
			return R.drawable.m_idea;
		case 5:
			return R.drawable.m_loop;
		case 6:
			return R.drawable.m_mts;
		case 7:
			return R.drawable.m_reliance_cdma;
		case 8:
			return R.drawable.m_reliance;
		case 9:
			return R.drawable.m_docomo;
		case 27:
			return R.drawable.m_docomo;
		case 10:
			return R.drawable.m_indicom;
		case 11:
		case 29:
			return R.drawable.m_uninor;
		case 12:
			return R.drawable.m_videocon;
		case 15:
			return R.drawable.m_vodafone;
		case 16:
			return R.drawable.m_airtel;
		case 17:
			return R.drawable.d_bigtv;
		case 18:
			return R.drawable.d_dishtv;
		case 19:
			return R.drawable.d_sundirect;
		case 20:
			return R.drawable.d_tatasky;
		case 21:
			return R.drawable.d_videocon;
		case 28:
			return R.drawable.m_videocon;
		case 22:
			return R.drawable.e_love;
		case 23:
			return R.drawable.e_fun;
		case 30:
		case 31:
			return R.drawable.m_mtnl;
		case 35:
			return R.drawable.e_ditto;
		case 36:
			return R.drawable.m_docomo;
		case 37:
			return R.drawable.m_loop;
		case 38:
			return R.drawable.m_bsnl;
		case 39:
			return R.drawable.m_idea;
		case 40:
			return R.drawable.m_indicom;
		case 41:
			return R.drawable.m_vodafone;
		case 42:
			return R.drawable.m_airtel;
		case 43:
			return R.drawable.m_reliance;
		case 44:
			return R.drawable.ic_pay1_launcher;
		case 45:
			return R.drawable.reliance_energy;
		case 46:
			return R.drawable.bses_rajdhani;
		case 47:
			return R.drawable.bses_yamuna;
		case 48:
			return R.drawable.ndpl;
		case 49:
			return R.drawable.airtel_landline;
		case 50:
			return R.drawable.mtnl;
		case 51:
			return R.drawable.mahangar_gas;
		case 65:
			return R.drawable.ic_og_mw_icon;
		default:
			return 1;
		}

	}

	public static String LoadData(String inFile, Context mContext) {
		String tContents = "";

		try {
			InputStream stream = mContext.getAssets().open(inFile);

			int size = stream.available();
			byte[] buffer = new byte[size];
			stream.read(buffer);
			stream.close();
			tContents = new String(buffer);
		} catch (IOException e) {
			// Handle exceptions here
		}

		return tContents;

	}

	public static int getBitmapByStatusId(int id, Context context) {
		switch (id) {
		case 5:
			return R.drawable.double_tick;
		case 2:
			return R.drawable.not_sent;
		case 3:
			return R.drawable.not_sent;
		case 1:
			return R.drawable.sent;
		case 4:
			return R.drawable.hourglass;
		case 6:
			return R.drawable.resend;
		case 0:
			return R.drawable.sent;
		default:
			return R.drawable.not_sent;
		}
	}

	public static int getTransactionStatusBitmap(int status) {
		if (status == 0 || status == 1 || status == 4 || status == 5) {
			return R.drawable.sent;
		} else if (status == 2 || status == 3) {
			return R.drawable.not_sent;
		}
		return 0;
	}

	public static int getComplaintStatusBitmap(int status,
			int complaintResolveFlag) {
		if (status != 2 && status != 3) {
			switch (complaintResolveFlag) {
			case 1:
				return R.drawable.double_tick;
			case 0:
				return R.drawable.hourglass;
			case -1:
				return R.drawable.resend;
			}
		}
		return 0;
	}

	public static boolean displayPromptForEnablingGPS(final Activity context) {
		final boolean isOn = false;
		AlertDialog.Builder dialog = new AlertDialog.Builder(context);
		LocationManager lm = null;
		boolean gps_enabled = false, network_enabled = false;
		if (lm == null)
			lm = (LocationManager) context
					.getSystemService(Context.LOCATION_SERVICE);
		try {
			gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
		} catch (Exception ex) {
		}
		try {
			network_enabled = lm
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		} catch (Exception ex) {
		}

		if (!gps_enabled) {
			dialog = new AlertDialog.Builder(context);
			dialog.setMessage("Location Service is off");
			dialog.setPositiveButton("On",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(
								DialogInterface paramDialogInterface,
								int paramInt) {
							// TODO Auto-generated method stub
							Intent myIntent = new Intent(
									Settings.ACTION_LOCATION_SOURCE_SETTINGS);
							context.startActivity(myIntent);
							// get gps
						}
					});
			dialog.setNegativeButton("Off",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(
								DialogInterface paramDialogInterface,
								int paramInt) {
							// TODO Auto-generated method stub

						}
					});
			dialog.show();

		}
		return gps_enabled;

		/*
		 * final AlertDialog.Builder builder = new
		 * AlertDialog.Builder(activity); final String action =
		 * Settings.ACTION_LOCATION_SOURCE_SETTINGS; final String message =
		 * "Enable either GPS or any other location" +
		 * " service to find current location.  Click OK to go to" +
		 * " location services settings to let you do so.";
		 * 
		 * builder.setMessage(message) .setPositiveButton("OK", new
		 * DialogInterface.OnClickListener() { public void
		 * onClick(DialogInterface d, int id) { activity.startActivity(new
		 * Intent(action)); d.dismiss(); } }) .setNegativeButton("Cancel", new
		 * DialogInterface.OnClickListener() { public void
		 * onClick(DialogInterface d, int id) { d.cancel(); } });
		 * builder.create().show();
		 */}

	public static String checkCode(String result) {
		try {
			String replaced = result.replace("(", "")
					.replace(")", "").replace(";", "");
			JSONArray array = new JSONArray(replaced);
			JSONObject jsonObject = array.getJSONObject(0);
			String code1 = jsonObject.getString("code");
			int code = Integer.parseInt(code1);

			if (code == 404) {
				return "Invalid Access";
			} else if (code == 403)
				return code + "";
			// else if (code == 30 || code == 2 || code == 28 || code == 4
			// || code == 5 || code == 6 || code == 7 || code == 32
			// || code == 8 || code == 9 || code == 29 || code == 33
			// || code == 14 || code == 31) {
			//
			// return description;
			// }

			else {
				String description = jsonObject.getString("description");
				return description;
			}

		} catch (JSONException e) {
			e.printStackTrace();
			return ERROR_MESSAGE_FOR_ALL_REQUEST;
		}
	}

	/** To select random VMN number for sending recharges request . */
	public static String selectRandomVMN(Context context) {
		String str = "9821232431";
		VMNDataSource vmnDataSource = new VMNDataSource(context);
		try {
			vmnDataSource.open();
			int length = vmnDataSource.getAllVMN().size();

			if (length != 0) {
				int i = 1;
				Random r = new Random();
				i = r.nextInt(length) + 1;
				str = vmnDataSource.getVMN(i).getVmnNumber();
			}
		} catch (SQLException exception) {
			// TODO: handle exception
			// // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			// // Log.e("Er ", e.getMessage());
		} finally {
			vmnDataSource.close();
		}

		return str;
	}

	/** To send SMS for recharges. */
	public static void sendSMSMessage(Context context, int type,
			int operatorId, String subId, String phoneNo, String amount,
			String stv, String operatorName) {
		// Log.i("Send SMS", "");
		String rechargeString = null;

		if (type == Constants.RECHARGE_DTH) {
			rechargeString = "*" + operatorId + "*" + subId + "*" + phoneNo
					+ "*" + amount;
		} else if (type == Constants.RECHARGE_MOBILE) {
			if (stv.equalsIgnoreCase("1")) {
				rechargeString = "*" + operatorId + "*" + phoneNo + "*"
						+ amount + "#";
			} else {
				rechargeString = "*" + operatorId + "*" + phoneNo + "*"
						+ amount;
			}
		} else if (type == Constants.WALLET_PAYMENT) {
			rechargeString = "*" + operatorId + "*" + phoneNo + "*" + amount;
		}

		/*
		 * Constants.mCurrentBalance = String
		 * .valueOf(Integer.parseInt(Constants.mCurrentBalance) -
		 * Integer.parseInt(amount));
		 */
		try {
			SmsManager smsManager = SmsManager.getDefault();
			smsManager.sendTextMessage(selectRandomVMN(context), null,
					rechargeString, null, null);
			// showCustomToast(context, "SMS sent.");

			NotificationDataSource notificationDataSource = new NotificationDataSource(
					context);
			MostVisitedOperatorDataSource dataSource = new MostVisitedOperatorDataSource(
					context);

			try {
				notificationDataSource.open();
				dataSource.open();
				notificationDataSource.createNotification(rechargeString, 0, ""
						+ System.currentTimeMillis());
				if (dataSource.checkCount(operatorId) == 0) {
					dataSource.createMVO(operatorId,
							String.valueOf(operatorId), operatorName,
							String.valueOf(type));
				} else {
					dataSource.updateRow(operatorId);
				}
			} catch (SQLException exception) {
				// TODO: handle exception
				// // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // Log.e("Er ", e.getMessage());
			} finally {
				notificationDataSource.close();
				dataSource.close();
			}
			// ContentValues contentValues = new ContentValues();
			// contentValues.put("notification_msg", rechargeString);
			// contentValues.put("timestamp ", "1234567890123");
			// contentValues.put("netOrSms ", "1");
			showOneButtonSuccessDialog(context, "SMS sent Successfully.",
					Constants.RECHARGE_SETTINGS);

		} catch (Exception e) {
			// showCustomToast(context, "SMS failed, please try again.");
			// e.printStackTrace();
		}
	}

	/** To generate recharges string while storing in local notification DB . */
	public static String getRechargeString(Context context, int type,
			int operatorId, String subId, String phoneNo, String amount,
			String stv) {
		// Log.i("Send SMS", "");
		String rechargeString = null;

		if (type == Constants.RECHARGE_DTH) {
			rechargeString = "*" + operatorId + "*" + subId + "*" + phoneNo
					+ "*" + amount;
		} else if (type == Constants.RECHARGE_MOBILE) {
			if (stv.equalsIgnoreCase("1")) {
				rechargeString = "*" + operatorId + "*" + phoneNo + "*"
						+ amount + "#";
			} else {
				rechargeString = "*" + operatorId + "*" + phoneNo + "*"
						+ amount;
			}
		}
		return rechargeString;

	}

	/** To send SMS for help. */
	public static void senDirectSms(Context context, String msgString) {
		try {
			SmsManager smsManager = SmsManager.getDefault();
			smsManager.sendTextMessage("+919821232431", null, msgString, null,
					null);
			notificationDataSource = new NotificationDataSource(context);
			try {
				notificationDataSource.open();
				notificationDataSource.createNotification(msgString, 0, ""
						+ System.currentTimeMillis());
			} catch (SQLException exception) {
				// TODO: handle exception
				// // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				// // Log.e("Er ", e.getMessage());
			} finally {
				notificationDataSource.close();
			}
			showOneButtonSuccessDialog(context, "SMS sent.", 0);
		} catch (Exception e) {
			// showCustomToast(context, "SMS failed, please try again.");
			// e.printStackTrace();
		}
	}

	/** To generate SHA-1 algorithm for transaction verification. */
	public static String encryptPassword(String password) {
		String sha1 = "";
		try {
			MessageDigest crypt = MessageDigest.getInstance("SHA-1");
			crypt.reset();
			crypt.update(password.getBytes("UTF-8"));
			sha1 = byteToHex(crypt.digest());
		} catch (NoSuchAlgorithmException e) {
			// e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// e.printStackTrace();
		}
		return sha1;
	}

	/** To convert byte to hexadecimal code. */
	private static String byteToHex(final byte[] hash) {
		Formatter formatter = new Formatter();
		for (byte b : hash) {
			formatter.format("%02x", b);
		}
		String result = formatter.toString();
		formatter.close();
		return result;
	}

	/** To show plan circles from assert package. */
	public static String loadJSONFromAsset(Context context, String filename) {
		String json = null;
		try {

			InputStream is = context.getAssets().open(filename);

			int size = is.available();

			byte[] buffer = new byte[size];

			is.read(buffer);

			is.close();

			json = new String(buffer, "UTF-8");

		} catch (IOException ex) {
			// ex.printStackTrace();
			return null;
		}
		return json;

	}

	/** To check whether Internet connectivity available or not. */
	public static boolean isOnline(Context context) {
		try {
			ConnectivityManager connectivityManager = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo activeNetworkInfo = connectivityManager
					.getActiveNetworkInfo();
			return activeNetworkInfo != null && activeNetworkInfo.isConnected();
		} catch (Exception e) {
			return true;
		}
	}

	/** To show customize toast. */
	// public static void showCustomToast(Context context, String message) {

	/**
	 * LayoutInflater inflater = ((Activity) context).getLayoutInflater();
	 * 
	 * // Call toast.xml file for toast layout View toastRoot =
	 * inflater.inflate(R.layout.custom_toast, null);
	 * 
	 * Toast toast = new Toast(context); TextView textView = (TextView)
	 * toastRoot .findViewById(R.id.textdView_Message);
	 * textView.setText(message); // Set layout to toast
	 * toast.setView(toastRoot); toast.setGravity(Gravity.FILL_HORIZONTAL |
	 * Gravity.TOP, 0, 0); toast.setDuration(Toast.LENGTH_LONG); toast.show();
	 **/
	// }

	/** To show success dialog. */
	public static void showOneButtonDialog(final Context context, String title,
			String message, String buttonText, final int id) {
		try {
			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.dialog_one_button);
			TextView titleText = (TextView) dialog
					.findViewById(R.id.textView_Title);
			titleText.setText(title);
			TextView text = (TextView) dialog
					.findViewById(R.id.textView_Message);
			text.setText(message);

			Button dialogButton = (Button) dialog.findViewById(R.id.button_Ok);
			dialogButton.setText(buttonText);
			dialogButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					// id =1 for mpos lead activity
					if (id == 1) {
						((Activity) context).finish();
					}
				}
			});

			dialog.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void showOneButtonSuccessDialog(final Context context,
			String message, final int id) {
		try {
			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			// dialog.getWindow().setBackgroundDrawableResource(
			// android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_success_one_button);
			// dialog.setTitle(null);

			// set the custom dialog components - text, image and button
			TextView text = (TextView) dialog
					.findViewById(R.id.textView_Message);
			text.setText(message);

			ImageView closeButton = (ImageView) dialog
					.findViewById(R.id.imageView_Close);
			// if button is clicked, close the custom dialog
			closeButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			Button dialogButton = (Button) dialog.findViewById(R.id.button_Ok);
			// if button is clicked, close the custom dialog
			dialogButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					if (id == Constants.CHANGE_PIN_SETTINGS) {
						context.startActivity(new Intent(context,
								LoginActivity.class));
						((Activity) context).finish();

					} else if (id == Constants.RECHARGE_SETTINGS) {
						((Activity) context).finish();

					} else if (id == Constants.COMPLAINT_SETTINGS
							|| id == Constants.QUICK_SETTINGS) {
						// ((Activity) context).finish();
					} else if (id == Constants.DEAL_SETTINGS) {
						context.startActivity(new Intent(context,
								MainActivity.class));
						((Activity) context).finish();
					} else if (id == Constants.PG_SETTINGS) {
						/*
						 * context.startActivity(new Intent(context,
						 * MainActivity.class)); ((Activity) context).finish();
						 */
					} else if (id == Constants.OTP_SETTINGS) {
						
						  context.startActivity(new Intent(context,
						  RegistrationLayout.class)); ((Activity) context).finish();
						 
					}
				}
			});

			dialog.show();
		} catch (Exception exception) {
		}
	}

	/** To show failure dialog. */
	public static void showOneButtonFailureDialog(final Context context,
			String message, String title, final int id) {

		try {
			if (message.equalsIgnoreCase("403")) {
				context.startActivity(new Intent(context,
						LoginDialogActivity.class));
			} else {
				final Dialog dialog = new Dialog(context);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.dialog_failure_one_button);
				TextView textView_Title = (TextView) dialog
						.findViewById(R.id.textView_Title);
				TextView text = (TextView) dialog
						.findViewById(R.id.textView_Message);
				text.setText(message);
				textView_Title.setText(title);
				ImageView closeButton = (ImageView) dialog
						.findViewById(R.id.imageView_Close);
				closeButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});
				Button dialogButtonOk = (Button) dialog
						.findViewById(R.id.button_Ok);
				dialogButtonOk.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
						if (id == Constants.SMS_SETTINGS) {
							context.startActivity(new Intent(context,
									SettingsSmsActivity.class));
						} else if (id == Constants.PG_SETTINGS) {

							// ((Activity) context).finish();
						}

					}
				});
				dialog.show();
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	/** To show failure dialog. */
	public static void showTwoButtonFailureDialog(final Context context,
			String message, String title, final int id) {
		try {
			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			// dialog.getWindow().setBackgroundDrawableResource(
			// android.R.color.transparent);
			dialog.setContentView(R.layout.dialog_failure_two_buttons);
			// // dialog.setTitle(null);

			// set the custom dialog components - text, image and button
			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			TextView text = (TextView) dialog
					.findViewById(R.id.textView_Message);
			text.setText(message);
			textView_Title.setText(title);
			ImageView closeButton = (ImageView) dialog
					.findViewById(R.id.imageView_Close);
			// if button is clicked, close the custom dialog
			closeButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			Button dialogButtonOk = (Button) dialog
					.findViewById(R.id.button_Ok);
			// if button is clicked, close the custom dialog
			dialogButtonOk.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (id == Constants.SMS_SETTINGS) {
						context.startActivity(new Intent(context,
								SettingsSmsActivity.class));
					} else if (id == Constants.MAP_SETTINGS) {
						Intent viewIntent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						context.startActivity(viewIntent);
					}
					dialog.dismiss();
				}
			});

			Button dialogButtonCancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			// if button is clicked, close the custom dialog
			dialogButtonCancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			dialog.show();
		} catch (Exception exception) {
		}
	}

	public static void showTwoButtonDialog(final Context context,
			String message, String title, String buttonOK, String buttonCancel,
			final int type) {
		try {
			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.dialog_two_button);

			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			TextView text = (TextView) dialog
					.findViewById(R.id.textView_Message);
			text.setText(message);
			textView_Title.setText(title);

			Button dialogButtonOk = (Button) dialog
					.findViewById(R.id.button_Ok);
			dialogButtonOk.setText(buttonOK);
			dialogButtonOk.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (type == 1) {
						Intent intent = new Intent(context,
								RegistrationActivity.class);
						context.startActivity(intent);
					} else if (type == 0) {

					} else {
						context.startActivity(new Intent(context,
								RetailerKYCActivity.class));
					}
					dialog.dismiss();
				}
			});

			Button dialogButtonCancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			dialogButtonCancel.setText(buttonCancel);
			dialogButtonCancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			dialog.show();
		} catch (Exception exception) {
		}
	}

	public static void showTwoButtonDialog(final Context context,
			String message, String title, String buttonOK,
			final Runnable buttonNow, String buttonCancel) {
		try {
			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.dialog_two_button);

			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			TextView text = (TextView) dialog
					.findViewById(R.id.textView_Message);
			text.setText(message);
			textView_Title.setText(title);

			Button dialogButtonOk = (Button) dialog
					.findViewById(R.id.button_Ok);
			dialogButtonOk.setText(buttonOK);
			dialogButtonOk.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					buttonNow.run();
				}
			});

			Button dialogButtonCancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			dialogButtonCancel.setText(buttonCancel);
			dialogButtonCancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			dialog.show();
		} catch (Exception exception) {
		}
	}

	/** To show navigation bar. */
	public static void showNavigationBar(final Context context) {
		try {
			/*
			 * ((Activity) context).finish(); ((Activity)
			 * context).startActivity(((Activity) context).getIntent());
			 */
			final class ClickToCall extends AsyncTask<String, String, String>
					implements OnDismissListener {
				ProgressDialog dialog;

				private Context context;

				public ClickToCall(Context context) {
					this.context = context;
				}

				@Override
				protected String doInBackground(String... params) {
//					if (Utility.getLoginFlag(context,
//							Constants.SHAREDPREFERENCE_IS_LOGIN)) {

						/*
						 * Utility.setLoginFlag(LogOutActivity.this,
						 * Constants.SHAREDPREFERENCE_IS_LOGIN, false);
						 */

						ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

						listValuePair.add(new BasicNameValuePair("method",
								"clickToCall"));
						listValuePair.add(new BasicNameValuePair("mobile",
								Utility.getSharedPreferences(context,
										SHAREDPREFERENCE_MOBILE_NUMBER)));
						listValuePair.add(new BasicNameValuePair("device_type",
								"android"));

						String response = RequestClass.getInstance()
								.readPay1Request(context, Constants.API_URL,
										listValuePair);
						listValuePair.clear();
						Log.e("Complaint stats:", "" + response);
						return response;
//					} else {
//						return "Error Login First";
//					}
				}

				@Override
				protected void onPostExecute(String result) {
					if (this.dialog.isShowing()) {
						this.dialog.dismiss();
					}
					String TAG = "TOLL-FREE CALL";
					super.onPostExecute(result);
					try {
						if (!result.startsWith("Error")) {
							String replaced = result.replace("(", "")
									.replace(")", "").replace(";", "");

							JSONArray array = new JSONArray(replaced);
							JSONObject jsonObject = array.getJSONObject(0);
							String status = jsonObject.getString("status");
							if (status.equalsIgnoreCase("success")) {
								// String message =
								// "You will receive a call shortly";
								String description = jsonObject
										.getString("description");
								showOneButtonDialog(context, "TOLL-FREE CALL",
										description, "OK", 0);
							} else {
								/*
								 * String message =
								 * "Become a star performer to avail this service"
								 * ; showOneButtonDialog(context,
								 * "TOLL-FREE CALL", message, "OK", 0);
								 */
								String description = jsonObject
										.getString("description");

								if (jsonObject.has("code")) {
									String errorCode = jsonObject
											.getString("code");

									if (errorCode.equals("403")) {

										Constants.showOneButtonFailureDialog(
												context, checkCode(result),
												TAG, Constants.OTHER_ERROR);
									} else if (errorCode.equals("E103")
											|| errorCode.equals("E104")) {
										showOneButtonDialog(context,
												"TOLL-FREE CALL", description,
												"OK", 0);
									} else {
										String buttonOK = "OK";
										if (errorCode.equals("E102")
												|| errorCode.equals("E103")) {
											buttonOK = "Update";
										} else {
											buttonOK = "Now";
										}
										Runnable buttonNow = new Runnable() {
											@Override
											public void run() {
												context.startActivity(new Intent(
														context,
														RetailerKYCActivity.class));
											}
										};
										showTwoButtonDialog(context,
												description, "TOLL-FREE CALL",
												buttonOK, buttonNow, "Later");
									}
								} else {
									Constants.showOneButtonDialog(context,
											"TOLL-FREE CALL", description,
											"OK", 0);
								}
								// String kyc_score = jsonObject
								// .getString("kyc_score");
								// double kycScore =
								// Double.parseDouble(kyc_score);
								// if (kycScore < 1) {
								// showTwoButtonDialog(context, description,
								// "TOLL-FREE CALL", "OK", "CANCEL", 2);
								//
								// /*
								// * showOneButtonDialog(context,
								// * "TOLL-FREE CALL", description, "OK", );
								// */
								// } else {
								// showOneButtonDialog(context,
								// "TOLL-FREE CALL", description,
								// "OK", 0);
								// }

							}

						}  else {
							Constants.showOneButtonFailureDialog(
									context,
									Constants.ERROR_INTERNET, TAG,
									Constants.OTHER_ERROR);
						}

					} catch (Exception e) {
						Constants.showOneButtonFailureDialog(context,
								Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
								Constants.OTHER_ERROR);
					
					} 
				}

				@Override
				protected void onPreExecute() {
					dialog = new ProgressDialog(context);
					this.dialog.setMessage("Please wait.....");
					this.dialog.setCancelable(true);
					this.dialog
							.setOnCancelListener(new DialogInterface.OnCancelListener() {

								@Override
								public void onCancel(DialogInterface dialog) {
									ClickToCall.this.cancel(true);
									dialog.cancel();
								}
							});
					this.dialog.show();
					super.onPreExecute();

				}

				@Override
				public void onDismiss(DialogInterface dialog) {
					ClickToCall.this.cancel(true);
					dialog.cancel();
				}
			}

			LinearLayout searchView = (LinearLayout) ((Activity) context)
					.findViewById(R.id.search_header);
			searchView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					context.startActivity(new Intent(context,
							SearchActivity.class));
				}
			});

			PackageManager packageManager = context.getPackageManager();

			SharedPreferences mSettingsData = context.getSharedPreferences(
					SettingsSmsActivity.SETTINGS_NAME, 0);
			final boolean bNetOrSms = mSettingsData.getBoolean(
					SettingsSmsActivity.KEY_NETWORK, true);
			int NotiCount = Utility.getNotificationCount(context, "NotiCount");
			int ChatCount = Utility.getNotificationCount(context, "ChatCount");

			ActivityInfo info = packageManager.getActivityInfo(
					((Activity) context).getComponentName(), 0);
			TextView textNotiCount = (TextView) ((Activity) context)
					.findViewById(R.id.textNotiCount);

			if (NotiCount == 0 && ChatCount == 0) {
				textNotiCount.setVisibility(View.GONE);
			} else {
				textNotiCount.setVisibility(View.VISIBLE);
				textNotiCount.setText((NotiCount + ChatCount) + "");
			}

			RelativeLayout panelTap = (RelativeLayout) ((Activity) context)
					.findViewById(R.id.panelTap);
			ImageView imageView_Menu = (ImageView) ((Activity) context)
					.findViewById(R.id.imageView_Menu);
			Button mBtnHome = (Button) ((Activity) context)
					.findViewById(R.id.header_home_button);
			LinearLayout mHeaderView = (LinearLayout) ((Activity) context)
					.findViewById(R.id.header_view);
			TextView header_title = (TextView) ((Activity) context)
					.findViewById(R.id.header_title);
			header_title.setClickable(false);
			header_title.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					((Activity) context).finish();
				}
			});

			LinearLayout callHeader = (LinearLayout) ((Activity) context)
					.findViewById(R.id.call_header);
			callHeader.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					String message = "Would you like to contact Pay1 Customer Care?";
					Runnable callBlock = new Runnable() {
						@Override
						public void run() {
							new ClickToCall(context).execute();
						}
					};

					showTwoButtonDialog(context, message, "TOLL-FREE CALL",
							"CALL", callBlock, "CANCEL");
				}
			});

			if (info.name.equalsIgnoreCase("com.mindsarray.pay1.MainActivity")) {
				header_title.setBackgroundDrawable(context.getResources()
						.getDrawable(R.drawable.ic_top_logo));
				header_title.setText("");
				header_title.setClickable(false);
				mHeaderView.setVisibility(View.GONE);
			} else {
				// String[] getName = info.name.split("\\.");
				header_title.setText(info.loadLabel(packageManager));
				header_title.setBackgroundDrawable(null);
				header_title.setClickable(false);
				mHeaderView.setVisibility(View.VISIBLE);
			}

			// if
			// (info.name.equalsIgnoreCase("com.mindsarray.pay1.MainActivity"))
			// mBtnHome.setVisibility(View.GONE);
			TextView textBalance = (TextView) ((Activity) context)
					.findViewById(R.id.textBalance);

			
			
			slidingPanel = (RelativeLayout) ((Activity) context)
					.findViewById(R.id.slidingPanel);

			home = (RelativeLayout) ((Activity) context)
					.findViewById(R.id.home);

			listView_Menu = (ListView) ((Activity) context)
					.findViewById(R.id.listView_Menu);
			// gridview_Menu = (GridView) ((Activity) context)
			// .findViewById(R.id.gridview_Menu);
			final TextView textView_MenuName = (TextView) ((Activity) context)
					.findViewById(R.id.textView_MenuName);
			if (!isExpanded) {
				listView_Menu.setVisibility(View.GONE);
				// gridview_Menu.setVisibility(View.GONE);
				home.setVisibility(View.GONE);
			}

			MenuAdapter menuAdapter;

			if (Utility.getLoginFlag(context,
					Constants.SHAREDPREFERENCE_IS_LOGIN)) {
				if (Utility.getUserName(context,
						Constants.SHAREDPREFERENCE_USER_NAME).equals("null")
						|| Utility.getUserName(context,
								Constants.SHAREDPREFERENCE_USER_NAME)
								.equals("")) {
					textView_MenuName.setText("Merchant");
				} else
					textView_MenuName.setText(Utility.getUserName(context,
							Constants.SHAREDPREFERENCE_USER_NAME));
				String[] slide_Menu_LogOut = {
						"HOME",
						context.getResources().getString(R.string.notification),
						"TOLL-FREE CALL",
						context.getResources().getString(
								R.string.online_balance),
						context.getResources().getString(R.string.support),
						context.getResources().getString(R.string.settings),
						context.getResources().getString(
								R.string.contact_details),
						context.getResources().getString(R.string.change_pin),
						context.getResources().getString(R.string.logout) };
				int[] slide_Menu_LogOut_icons = { R.drawable.ic_home,
						R.drawable.ic_notif, R.drawable.ic_menu_call,
						R.drawable.ic_menu_online_limit,
						R.drawable.ic_live_support, R.drawable.ic_settings,
						R.drawable.ic_my_location, R.drawable.ic_change_pin,
						R.drawable.ic_logout };
				menuAdapter = new MenuAdapter(context, slide_Menu_LogOut,
						slide_Menu_LogOut_icons);

			} else {
				textView_MenuName.setText("Merchant");
				String[] slide_Menu_Login = {
						"HOME",
						context.getResources().getString(R.string.login),
						context.getResources().getString(R.string.notification),
						"TOLL-FREE CALL",
						context.getResources().getString(
								R.string.online_balance),
						context.getResources().getString(R.string.support),
						context.getResources().getString(R.string.settings),
						context.getResources().getString(
								R.string.contact_details),
						context.getResources().getString(R.string.change_pin) };
				int[] slide_Menu_Login_icons = { R.drawable.ic_home,
						R.drawable.ic_logout, R.drawable.ic_notif,
						R.drawable.ic_menu_call,
						R.drawable.ic_menu_online_limit,
						R.drawable.ic_live_support, R.drawable.ic_settings,
						R.drawable.ic_my_location, R.drawable.ic_change_pin };
				menuAdapter = new MenuAdapter(context, slide_Menu_Login,
						slide_Menu_Login_icons);

			}

			// final ArrayList<GridMenuItem> gridArray = new
			// ArrayList<GridMenuItem>();
			// RechargeMainAdapter customGridAdapter;
			//
			// int mMobileIcon = R.drawable.mobile_recharge;
			// int mDthIcon = R.drawable.dth_recharge;
			// int mWalletIcon = R.drawable.ic_pay1_launcher;
			// int mUtility = R.drawable.utility_bill;
			// int mBillPaymentIcon = R.drawable.bill_payment;
			// int mEntertainment = R.drawable.entertainment;
			//
			// gridArray.add(new GridMenuItem(mMobileIcon,
			// context.getResources()
			// .getString(R.string.mobile_recharge), 1, 1));
			// gridArray.add(new GridMenuItem(mDthIcon, context.getResources()
			// .getString(R.string.dth), 2, 1));
			// gridArray.add(new GridMenuItem(mBillPaymentIcon, context
			// .getResources().getString(R.string.postpaid_mobile), 3, 1));
			//
			// gridArray.add(new GridMenuItem(mUtility, context.getResources()
			// .getString(R.string.utility_bill), 4, 1));
			// gridArray.add(new GridMenuItem(mWalletIcon,
			// context.getResources()
			// .getString(R.string.pay1_topup), 5, 1));
			// gridArray.add(new GridMenuItem(mEntertainment, context
			// .getResources().getString(R.string.entertainment), 6, 1));
			//
			// customGridAdapter = new RechargeMainAdapter(context,
			// R.layout.rechargemenu_adapter, gridArray,true);
			// gridview_Menu.setAdapter(customGridAdapter);

			listView_Menu.setAdapter(menuAdapter);

			toggleView = new View(context);
			LinearLayout.LayoutParams trparams = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			toggleView.setLayoutParams(trparams);
			toggleView.setBackgroundColor(Color.TRANSPARENT);
			toggleView.setClickable(true);
			toggleView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// isExpanded = false;
					// new CollapseAnimation(slidingPanel, 0,
					// TranslateAnimation.RELATIVE_TO_SELF, -0.44f,
					// TranslateAnimation.RELATIVE_TO_SELF, 0.0f, 0, 0.0f,
					// 0, 0.0f);
					// listView_Menu.setVisibility(View.GONE);
					// gridview_Menu.setVisibility(View.GONE);
					// home.setVisibility(View.GONE);
					// slidingPanel.removeView(toggleView);

					collapsView();
				}
			});

			home.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// isExpanded = false;
					// new CollapseAnimation(slidingPanel, 0,
					// TranslateAnimation.RELATIVE_TO_SELF, -0.44f,
					// TranslateAnimation.RELATIVE_TO_SELF, 0.0f, 0, 0.0f,
					// 0, 0.0f);
					// listView_Menu.setVisibility(View.GONE);
					// gridview_Menu.setVisibility(View.GONE);
					// home.setVisibility(View.GONE);
					// slidingPanel.removeView(toggleView);
					collapsView();
					context.startActivity(new Intent(context,
							MainActivity.class));
					((Activity) context).finish();
				}
			});

			// gridview_Menu.setOnItemClickListener(new OnItemClickListener() {
			//
			// @Override
			// public void onItemClick(AdapterView<?> arg0, View arg1,
			// int pos, long arg3) {
			// // TODO Auto-generated method stub
			// // isExpanded = false;
			// // new CollapseAnimation(slidingPanel, 0,
			// // TranslateAnimation.RELATIVE_TO_SELF, -0.44f,
			// // TranslateAnimation.RELATIVE_TO_SELF, 0.0f, 0, 0.0f,
			// // 0, 0.0f);
			// // listView_Menu.setVisibility(View.GONE);
			// // gridview_Menu.setVisibility(View.GONE);
			// // home.setVisibility(View.GONE);
			// // slidingPanel.removeView(toggleView);
			//
			// collapsView();
			//
			// switch (gridArray.get(pos).getId()) {
			// case 1:
			// context.startActivity(new Intent(context,
			// MobileRechargeTabFragment.class));
			// Constants.SERVICE_TYPE = "1";
			// break;
			// case 2:
			// context.startActivity(new Intent(context,
			// DthRechargeTabFragment.class));
			// Constants.SERVICE_TYPE = "2";
			// break;
			// case 3:
			// context.startActivity(new Intent(context,
			// MobileBillTabFragment.class));
			// Constants.SERVICE_TYPE = "4";
			// break;
			// case 4:
			// context.startActivity(new Intent(context,
			// UtilityBillTabFragment.class));
			// Constants.SERVICE_TYPE = "6";
			// break;
			// case 5:
			// context.startActivity(new Intent(context,
			// WalletTabFragment.class));
			// Constants.SERVICE_TYPE = "5";
			//
			// break;
			// case 6:
			// context.startActivity(new Intent(context,
			// EntertainmentRechargeTabFragment.class));
			// Constants.SERVICE_TYPE = "3";
			// break;
			// default:
			// break;
			// }
			// }
			// });

			listView_Menu
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {

							try {
								boolean flag = false;
								// isExpanded = false;
								// new CollapseAnimation(slidingPanel, 0,
								// TranslateAnimation.RELATIVE_TO_SELF,
								// -0.44f,
								// TranslateAnimation.RELATIVE_TO_SELF, 0.0f,
								// 0, 0.0f, 0, 0.0f);
								// listView_Menu.setVisibility(View.GONE);
								// gridview_Menu.setVisibility(View.GONE);
								// home.setVisibility(View.GONE);
								// slidingPanel.removeView(toggleView);

								collapsView();

								// if (bNetOrSms) {

								// if (Utility.getLoginFlag(context,
								// Constants.SHAREDPREFERENCE_IS_LOGIN)) {

								switch (position) {
								case 0:
									context.startActivity(new Intent(context,
											MainActivity.class));
									break;
								case 1:
									if (bNetOrSms) {
										if (Utility
												.getLoginFlag(
														context,
														Constants.SHAREDPREFERENCE_IS_LOGIN)) {
											context.startActivity(new Intent(
													context,
													NotificationActivity.class));
										} else {
											Intent intent = new Intent(context,
													LoginActivity.class);
											intent.putExtra("is_login", "login");
											context.startActivity(intent);
											((Activity) context).finish();
										}
									} else {
										if (Utility
												.getLoginFlag(
														context,
														Constants.SHAREDPREFERENCE_IS_LOGIN)) {
											flag = false;
											context.startActivity(new Intent(
													context,
													NotificationActivity.class));
										} else
											flag = true;
									}
									break;
								case 2:
									String message = "Would you like to contact Pay1 Customer Care?";
									if (bNetOrSms) {
										if (Utility
												.getLoginFlag(
														context,
														Constants.SHAREDPREFERENCE_IS_LOGIN)) {

											Runnable callBlock = new Runnable() {
												@Override
												public void run() {
													new ClickToCall(context)
															.execute();
												}
											};

											showTwoButtonDialog(context,
													message, "TOLL-FREE CALL",
													"CALL", callBlock, "CANCEL");
										} else {
											context.startActivity(new Intent(
													context,
													NotificationActivity.class));
										}
									} else {
										if (Utility
												.getLoginFlag(
														context,
														Constants.SHAREDPREFERENCE_IS_LOGIN)) {
											flag = false;
											showTwoButtonDialog(context,
													message, "TOLL-FREE CALL",
													"CALL", "CANCEL", 0);
										} else
											flag = true;
									}
									break;
								case 3:
									if (bNetOrSms) {
										if (Utility
												.getLoginFlag(
														context,
														Constants.SHAREDPREFERENCE_IS_LOGIN)) {
											if (Utility.getIsAbleForPG(context)) {
												context.startActivity(new Intent(
														context,
														PGActivity.class));
											} else {
												Constants
														.showOneButtonFailureDialog(
																context,
																"You are not authorize to use this service",
																"Online", 1);
											}
										} else {
											String message2 = "Would you like to contact Pay1 Customer Care?";
											Runnable callBlock = new Runnable() {
												@Override
												public void run() {
													new ClickToCall(context)
															.execute();
												}
											};

											showTwoButtonDialog(context,
													message2, "TOLL-FREE CALL",
													"CALL", callBlock, "CANCEL");
										}
									} else {
										flag = true;
									}

									break;
								case 4:
									if (bNetOrSms) {
										if (Utility
												.getLoginFlag(
														context,
														Constants.SHAREDPREFERENCE_IS_LOGIN)) {
											context.startActivity(new Intent(
													context,
													SupportActivity.class));
										} else {
											if (Utility.getIsAbleForPG(context)) {
												context.startActivity(new Intent(
														context,
														PGActivity.class));
											} else {
												Constants
														.showOneButtonFailureDialog(
																context,
																"You are not authorize to use this service",
																"Online", 1);
											}
										}
									} else {
										flag = true;
									}

									break;
								case 5:
									if (bNetOrSms) {
										if (Utility
												.getLoginFlag(
														context,
														Constants.SHAREDPREFERENCE_IS_LOGIN)) {
											/*context.startActivity(new Intent(
													context,
													SettingsSmsActivity.class));*/
											
											context.startActivity(new Intent(
													context,
													SettingsMainActivity.class));
										} else {
											context.startActivity(new Intent(
													context,
													SupportActivity.class));
										}
									} else {
										flag = false;
										context.startActivity(new Intent(
												context,
												SettingsMainActivity.class));
									}

									break;
								case 6:
									if (bNetOrSms) {
										if (Utility
												.getLoginFlag(
														context,
														Constants.SHAREDPREFERENCE_IS_LOGIN)) {
											boolean gps_enabled = false;
											boolean network_enabled = false;
											LocationManager lm = (LocationManager) context
													.getSystemService(Context.LOCATION_SERVICE);
											try {
												gps_enabled = lm
														.isProviderEnabled(LocationManager.GPS_PROVIDER);
											} catch (Exception ex) {
											}
											try {
												network_enabled = lm
														.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
											} catch (Exception ex) {
											}

											if (gps_enabled || network_enabled) {
												context.startActivity(new Intent(
														context,
														RetailerKYCActivity.class));
											} else {
												Constants
														.showTwoButtonFailureDialog(
																context,
																"To enhance your Shop Locator experience:\n\n1.Turn on GPS and mobile network location\n2.Turn on Wi-Fi",
																"Update address",
																Constants.MAP_SETTINGS);
											}
										} else {
											context.startActivity(new Intent(
													context,
													SettingsMainActivity.class));
										}
									} else {
										if (Utility
												.getLoginFlag(
														context,
														Constants.SHAREDPREFERENCE_IS_LOGIN)) {
											flag = true;
										} else {
											flag = false;
											context.startActivity(new Intent(
													context,
													SettingsUpdatePinActivity.class));
										}
									}
									break;
								case 7:
									if (bNetOrSms) {
										if (Utility
												.getLoginFlag(
														context,
														Constants.SHAREDPREFERENCE_IS_LOGIN)) {
											context.startActivity(new Intent(
													context,
													SettingsUpdatePinActivity.class));
										} else {
											boolean gps_enabled = false;
											boolean network_enabled = false;
											LocationManager lm = (LocationManager) context
													.getSystemService(Context.LOCATION_SERVICE);
											try {
												gps_enabled = lm
														.isProviderEnabled(LocationManager.GPS_PROVIDER);
											} catch (Exception ex) {
											}
											try {
												network_enabled = lm
														.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
											} catch (Exception ex) {
											}

											if (gps_enabled || network_enabled) {
												context.startActivity(new Intent(
														context,
														RetailerKYCActivity.class));
											} else {
												Constants
														.showTwoButtonFailureDialog(
																context,
																"To enhance your Shop Locator experience:\n\n1.Turn on GPS and mobile network location\n2.Turn on Wi-Fi",
																"Update address",
																Constants.MAP_SETTINGS);
											}
										}
									} else {
										flag = true;
									}

									break;

								case 8:
									if (bNetOrSms) {
										if (Utility
												.getLoginFlag(
														context,
														Constants.SHAREDPREFERENCE_IS_LOGIN)) {
											context.startActivity(new Intent(
													context,
													LogOutActivity.class));
										} else {
											context.startActivity(new Intent(
													context,
													SettingsUpdatePinActivity.class));
										}
									} else {
										if (Utility
												.getLoginFlag(
														context,
														Constants.SHAREDPREFERENCE_IS_LOGIN)) {
											flag = true;
										} else {
											flag = false;
											context.startActivity(new Intent(
													context,
													SettingsUpdatePinActivity.class));
										}
									}

									break;

								default:
									break;
								}

								if (flag) {
									flag = false;
									showTwoButtonFailureDialog(
											context,
											"Internet setting is not enabled.\nWould you like to change settings?",
											"Settings", Constants.SMS_SETTINGS);
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

					});

			RelativeLayout balance_layout = (RelativeLayout) ((Activity) context)
					.findViewById(R.id.balance_layout);

			mBtnHome.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					((Activity) context).finish();

				}
			});
			mHeaderView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					((Activity) context).finish();

				}
			});
			panelTap.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						// TODO Auto-generated method stub
						// Intent intent = new Intent(context,
						// LoginActivity.class);
						// ((Activity) context).startActivityForResult(intent,
						// 100);
						InputMethodManager imm = (InputMethodManager) context
								.getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

						DisplayMetrics metrics = new DisplayMetrics();
						((Activity) context).getWindowManager()
								.getDefaultDisplay().getMetrics(metrics);
						int panelWidth = (int) ((metrics.widthPixels) * 0.80);

						if (!isExpanded) {

							// Expand
							// isExpanded = true;
							// new ExpandAnimation(slidingPanel, panelWidth,
							// Animation.RELATIVE_TO_SELF, 0.0f,
							// Animation.RELATIVE_TO_SELF, -0.44f, 0,
							// 0.0f, 0, 0.0f);
							// listView_Menu.setVisibility(View.VISIBLE);
							// gridview_Menu.setVisibility(View.VISIBLE);
							// home.setVisibility(View.VISIBLE);
							// slidingPanel.addView(toggleView);

							expandView(panelWidth);

						} else {

							// Collapse

							// isExpanded = false;
							// new CollapseAnimation(slidingPanel, panelWidth,
							// TranslateAnimation.RELATIVE_TO_SELF,
							// -0.44f,
							// TranslateAnimation.RELATIVE_TO_SELF, 0.0f,
							// 0, 0.0f, 0, 0.0f);
							// listView_Menu.setVisibility(View.GONE);
							// gridview_Menu.setVisibility(View.GONE);
							// home.setVisibility(View.GONE);
							// slidingPanel.removeView(toggleView);

							collapsView();

						}
					} catch (Exception exception) {
					}
				}
			});

			if (Utility.getLoginFlag(context,
					Constants.SHAREDPREFERENCE_IS_LOGIN)) {
				balance_layout.setVisibility(View.VISIBLE);
				imageView_Menu.setVisibility(View.VISIBLE);
				DecimalFormat decimalFormat = new DecimalFormat(".00");
				if ((Utility.getBalance(context,
						Constants.SHAREDPREFERENCE_BALANCE)) != null) {
					textBalance.setText(decimalFormat.format(Double
							.parseDouble(Utility.getBalance(context,
									Constants.SHAREDPREFERENCE_BALANCE))));
				} else {
					textBalance.setText(decimalFormat.format(Double
							.parseDouble("0")));
				}
			} else {
				balance_layout.setVisibility(View.GONE);
				imageView_Menu.setVisibility(View.VISIBLE);
				textBalance.setText("");
			}
		} catch (NameNotFoundException e) {
		} catch (Exception e) {
			Log.e("sas", "sd");
		}

		/*
		 * try{ Locale myLocale = new
		 * Locale(Utility.getLanguageSharedPreferences(context)); Resources res
		 * = context.getResources(); DisplayMetrics dm =
		 * res.getDisplayMetrics(); Configuration conf = res.getConfiguration();
		 * conf.locale = myLocale; res.updateConfiguration(conf, dm);
		 * 
		 * }catch(Exception ee){
		 * 
		 * }
		 */
	}

	public static String getPlanData(Context context, String urlStr) {
		StringBuilder builder = new StringBuilder();
		try {
			URL url = new URL(urlStr);
			HttpURLConnection urlConnection = (HttpURLConnection) url
					.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection.setDoOutput(true);
			urlConnection.connect();
			int file_size = urlConnection.getContentLength();
			if (file_size < availableMemory()) {
				Utility.setAvailableMemoryFlag(context, "memoryFlag", true);
				InputStream inputStream = urlConnection.getInputStream();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(inputStream));
				builder = new StringBuilder();
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} else {
				Utility.setAvailableMemoryFlag(context, "memoryFlag", false);
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (SSLHandshakeException e) {
			e.printStackTrace();
			Log.e("Er ", e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}

	public static long availableMemory() {
		StatFs stat = new StatFs(Environment.getExternalStorageDirectory()
				.getPath());
		long bytesAvailable = (long) stat.getBlockSize()
				* (long) stat.getAvailableBlocks();
		long megAvailable = bytesAvailable / (1024 * 1024);
		Log.e("", "Available MB : " + megAvailable);
		/*
		 * MemoryInfo mi = new MemoryInfo(); ActivityManager activityManager =
		 * (ActivityManager)getSystemService(ACTIVITY_SERVICE);
		 * activityManager.getMemoryInfo(mi); long availableMegs = mi.availMem /
		 * 1048576L;
		 */
		return bytesAvailable;
	}

	public static void expandView(int panelWidth) {
		isExpanded = true;
		new ExpandAnimation(slidingPanel, panelWidth,
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
				-0.44f, 0, 0.0f, 0, 0.0f);
		listView_Menu.setVisibility(View.VISIBLE);
		// gridview_Menu.setVisibility(View.VISIBLE);
		home.setVisibility(View.VISIBLE);
		slidingPanel.addView(toggleView);
	}

	/*public static void collapsView() {
		isExpanded = false;
		new CollapseAnimation(slidingPanel, 0,
				TranslateAnimation.RELATIVE_TO_SELF, -0.44f,
				TranslateAnimation.RELATIVE_TO_SELF, 0.0f, 0, 0.0f, 0, 0.0f);
		listView_Menu.setVisibility(View.GONE);
		// gridview_Menu.setVisibility(View.GONE);
		home.setVisibility(View.GONE);
		slidingPanel.removeView(toggleView);
	}*/
	
	public static void collapsView() {
		isExpanded = false;
		new CollapseAnimation(slidingPanel, 0,
				TranslateAnimation.RELATIVE_TO_SELF, -0.44f,
				TranslateAnimation.RELATIVE_TO_SELF, 0.0f, 0, 0.0f, 0, 0.0f);
		listView_Menu.setVisibility(View.GONE);
		// gridview_Menu.setVisibility(View.GONE);
		home.setVisibility(View.GONE);
		slidingPanel.removeView(toggleView);
	}

	public static GoogleAnalytics analytics;
	public static Tracker tracker;

	public static void sendGoogleAnalytics(Context context, String TAG) {
		// EasyTracker easyTracker = EasyTracker.getInstance(context);
		// easyTracker.set(Fields.SCREEN_NAME, TAG);
		analytics = GoogleAnalytics.getInstance(context);
		tracker = analytics.newTracker(R.string.ga_trackingId);
		tracker.setScreenName(TAG);
		tracker.send(new HitBuilders.EventBuilder().build());
	}

	/** To convert minutes to 24 hours clock to show bus time. */
	public static String convertMinutesTo24Hours(String minutes) {
		try {
			double totalTime = Double.parseDouble(minutes);

			totalTime = totalTime / 60;

			DecimalFormat myFormatter = new DecimalFormat("00.00");
			String formatedTime = myFormatter.format(totalTime);

			int dot = formatedTime.indexOf('.');
			int hrs = Integer.parseInt(formatedTime.substring(0, dot));
			int mins = Integer.parseInt(formatedTime.substring(dot + 1));

			if (mins / 60 > 0) {
				hrs = hrs + mins / 60;
				mins = mins % 60;
			}
			return convertMinutesTo12Hours(formatedTime)
					+ new StringBuffer().append(" (").append(hrs).append(".")
							.append(mins).append(")").toString();
		} catch (Exception e) {
			return "";
		}

	}

	public static void writeToSDCard(Context context, String data) {
		try {
			String clsname = context.getClass().getSimpleName();
			File myFile = new File("/sdcard/sdfile.txt");
			if (!myFile.exists()) {
				myFile.createNewFile();
			}
			BufferedWriter buf = new BufferedWriter(
					new FileWriter(myFile, true));
			buf.write(clsname + "\n" + data);
			buf.newLine();
			buf.flush();
			buf.close();

			//

			// FileOutputStream fOut = new FileOutputStream(myFile);
			// OutputStreamWriter myOutWriter =
			// new OutputStreamWriter(fOut);
			// myOutWriter.append(clsname+"\n"+data);
			// myOutWriter.close();
			// fOut.close();

		} catch (Exception e) {

		}
	}

	/** To convert minutes to 14 hours clock to show bus time. */
	public static String convertMinutesTo12Hours(String time) {
		try {

			int dot = time.indexOf('.');
			int hrs = Integer.parseInt(time.substring(0, dot));
			int mins = Integer.parseInt(time.substring(dot + 1));

			if (mins / 60 > 0) {
				hrs = hrs + mins / 60;
				mins = mins % 60;
			}

			StringBuffer t = new StringBuffer();
			if (hrs == 24)
				hrs -= 24;
			if (hrs < 12) {
				if (hrs <= 9 && mins <= 9) {
					t.append("0").append(hrs).append(":0").append(mins)
							.append(" AM");
				} else if (hrs <= 9 && mins >= 9) {
					t.append("0").append(hrs).append(":").append(mins)
							.append(" AM");
				} else if (hrs >= 9 && mins <= 9) {
					t.append(hrs).append(":0").append(mins).append(" AM");
				} else {
					t.append(hrs).append(":").append(mins).append(" AM");
				}
			} else if (hrs >= 12) {
				if (hrs > 12) {
					hrs = hrs - 12;
				}

				if (hrs <= 9 && mins <= 9) {
					t.append("0").append(hrs).append(":0").append(mins)
							.append(" PM");
				} else if (hrs <= 9 && mins >= 9) {
					t.append("0").append(hrs).append(":").append(mins)
							.append(" PM");
				} else if (hrs >= 9 && mins <= 9) {
					t.append(hrs).append(":0").append(mins).append(" PM");
				} else {
					t.append(hrs).append(":").append(mins).append(" PM");
				}
			} else {
				hrs = hrs - 12;
				t.append("0").append(hrs).append(":0").append(mins)
						.append(" PM");
			}

			return t.toString();
		} catch (Exception e) {
			return "";
		}

	}

	/** To convert minutes to hours to show total bus duration. */
	public static String convertMinutesToHours(String arrival, String departure) {
		try {
			double totalTime = Double.parseDouble(arrival)
					- Double.parseDouble(departure);

			totalTime = totalTime / 60;

			DecimalFormat myFormatter = new DecimalFormat("##.##");
			String formatedTime = myFormatter.format(totalTime);

			int hrs = 0, mins = 0;
			if (formatedTime.contains(".")) {
				int dot = formatedTime.indexOf('.');
				hrs = Integer.parseInt(formatedTime.substring(0, dot));
				mins = Integer.parseInt(formatedTime.substring(dot + 1));

				if (mins / 60 > 0) {
					hrs = hrs + mins / 60;
					mins = mins % 60;
				}
				return new StringBuffer().append(hrs).append(".").append(mins)
						.append(" hrs").toString();
			} else {
				hrs = Integer.parseInt(formatedTime);

				return new StringBuffer().append(hrs).append(" hrs").toString();
			}

		} catch (Exception e) {
			return "hrs";
		}

	}

	/** To check whether Email address is valid or not. */
	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	/** Get message time on create message screen. */
	public static String getMessageTime(String miliSec) {

		String dateString = "";
		try {

			// SimpleDateFormat formatter = new SimpleDateFormat(
			// "yyyy-MM-dd HH:mm:ss");
			// Date d = formatter.parse(temp_dates);
			// SimpleDateFormat formatter1 = new SimpleDateFormat(
			// "dd MMM, hh:mm a");
			// dateString = formatter1.format(d);

			Date dt = new Date(Long.parseLong(miliSec));
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMM, hh:mm a");
			dateString = formatter.format(dt);

		} catch (ParcelFormatException e) {
			// // Log.e("Error in Global Class", "Error parsing data " +
			// e.toString());
		} catch (Exception e) {
			// // Log.e("Error in Global Class", "Error parsing data " +
			// e.toString());
		}
		return dateString;
	}

	// private static Handler myHandler = new Handler() {
	// public void handleMessage(Message msg) {
	// final int what = msg.what;
	// // Log.d("Handler", "" + msg);
	// /*
	// * switch(what) { case DO_UPDATE_TEXT: doUpdate(); break; case
	// * DO_THAT: doThat(); break; }
	// */
	//
	// }
	// };

	public static String rechargeTypeOperatorName(String product_id) {

		switch (Integer.parseInt(product_id)) {
		case OPERATOR_AIRCEL:
			RECHARGE_TYPE = Constants.RECHARGE_MOBILE;
			return "Aircel";
		case Constants.OPERATOR_AIRTEL:
			RECHARGE_TYPE = Constants.RECHARGE_MOBILE;
			return "Airtel";
		case OPERATOR_BSNL:
			RECHARGE_TYPE = Constants.RECHARGE_MOBILE;
			return "Bsnl";
		case OPERATOR_IDEA:
			RECHARGE_TYPE = Constants.RECHARGE_MOBILE;
			return "Idea";
		case OPERATOR_LOOP:
			RECHARGE_TYPE = Constants.RECHARGE_MOBILE;
			return "Loop";
		case OPERATOR_MTS:
			RECHARGE_TYPE = Constants.RECHARGE_MOBILE;
			return "Mts";
		case OPERATOR_RELIANCE_CDMA:
			RECHARGE_TYPE = Constants.RECHARGE_MOBILE;
			return "Reliance Cdma";
		case OPERATOR_RELIANCE_GSM:
			RECHARGE_TYPE = Constants.RECHARGE_MOBILE;
			return "Reliance Gsm";
		case OPERATOR_DOCOMO:
			RECHARGE_TYPE = Constants.RECHARGE_MOBILE;
			return "Tata Docomo";
		case OPERATOR_TATA_INDICOM:
			RECHARGE_TYPE = Constants.RECHARGE_MOBILE;
			return "Tata Indicom";

		case OPERATOR_UNINOR:
			RECHARGE_TYPE = Constants.RECHARGE_MOBILE;
			return "Uninor";
		case OPERATOR_VIDEOCON:
			RECHARGE_TYPE = Constants.RECHARGE_MOBILE;
			return "Videocon";
		case OPERATOR_VODAFONE:
			RECHARGE_TYPE = Constants.RECHARGE_MOBILE;
			return "Vodafone";
		case OPERATOR_MTNL:
			RECHARGE_TYPE = Constants.RECHARGE_MOBILE;
			return "Mtnl";

		case Constants.OPERATOR_BILL_AIRTEL:
			RECHARGE_TYPE = Constants.BILL_PAYMENT;
			return "Airtel PostPaid";
		case OPERATOR_BILL_RELIANCE:
			RECHARGE_TYPE = Constants.BILL_PAYMENT;
			return "Reliance PostPaid";

		case OPERATOR_BILL_LOOP:
			RECHARGE_TYPE = Constants.BILL_PAYMENT;
			return "Loop PostPaid";
		case OPERATOR_BILL_CELLONE:
			RECHARGE_TYPE = Constants.BILL_PAYMENT;
			return "Cellone PostPaid";
		case OPERATOR_BILL_IDEA:
			RECHARGE_TYPE = Constants.BILL_PAYMENT;
			return "Idea PostPaid";
		case OPERATOR_BILL_TATATELESERVICE:
			RECHARGE_TYPE = Constants.BILL_PAYMENT;
			return "Tata Teleservices PostPaid";
		case OPERATOR_BILL_VODAFONE:
			RECHARGE_TYPE = Constants.BILL_PAYMENT;
			return "Vodafone PostPaid";

		case OPERATOR_DTH_AIRTEL:
			RECHARGE_TYPE = Constants.RECHARGE_DTH;
			return "Airtel Dth";
		case OPERATOR_DTH_BIG:
			RECHARGE_TYPE = Constants.RECHARGE_DTH;
			return "Big Tv";

		case OPERATOR_DTH_DISHTV:
			RECHARGE_TYPE = Constants.RECHARGE_DTH;
			return "Dish Tv";
		case OPERATOR_DTH_SUN:
			RECHARGE_TYPE = Constants.RECHARGE_DTH;
			return "Sun Tv";
		case OPERATOR_DTH_TATA_SKY:
			RECHARGE_TYPE = Constants.RECHARGE_DTH;
			return "Tata Sky";
		case OPERATOR_DTH_VIDEOCON:
			RECHARGE_TYPE = Constants.RECHARGE_DTH;
			return "Videocon";

		default:
			break;
		}
		return "Pay1";

	}

	public static void promptForGPS(final Activity activity) {
		LocationManager lm;
		boolean gps_enabled = false;

		lm = (LocationManager) activity
				.getSystemService(Context.LOCATION_SERVICE);
		try {
			gps_enabled = lm
					.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (!gps_enabled) {
			showTurnGPSOnDialog(activity,
					"Turn GPS on to get accurate location");
		}
	}

	public static void showTurnGPSOnDialog(final Context context,
			String dialogText) {
		final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
		dialog.setMessage(dialogText);
		dialog.setPositiveButton("On", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface paramDialogInterface,
					int paramInt) {
				Intent sIntent = new Intent(
						Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				context.startActivity(sIntent);
			}
		});
		dialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface paramDialogInterface,
							int paramInt) {

					}
				});
		dialog.show();
	}

	private static String LOG;

	public static void log(Context context, String message) {
		try {
			// SimpleDateFormat sdf = new
			// SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			// SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMddHHmmss");
			// String currentDateandTime = sdf.format(new Date());
			// Date currentDate = new Date();
			//
			// Date oldDate = new Date(currentDate.getTime() - 24 * 60 * 60 *
			// 1000);
			// String oldDateString = sdf2.format(oldDate);
			// SharedPreferences settings = context
			// .getSharedPreferences(LOG, 0);
			// SharedPreferences.Editor editor = settings.edit();
			// Map<String, ?> allEntries = settings.getAll();
			// String oldDatePref = "Log" + oldDateString;
			// for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
			// if(entry.getValue() != null &&
			// entry.getValue().toString().contains(oldDatePref))
			// editor.remove(entry.getKey());
			// }
			//
			// String currentDateandTime2 = sdf2.format(currentDate);
			// String name = "Log" + currentDateandTime2;
			// String text = currentDateandTime + "|" + message + "|"
			// + Utility.getSharedPreferences(context,
			// Constants.SHAREDPREFERENCE_MOBILE_NUMBER);
			// editor.putString(name, text);
			// editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void clear_logs(Context context) {
		try {
			SharedPreferences settings = context.getSharedPreferences(LOG, 0);
			SharedPreferences.Editor editor = settings.edit();

			Map<String, ?> allEntries = settings.getAll();
			String logPref = "Log2015";
			for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
				if (entry.getValue() != null
						&& entry.getValue().toString().contains(logPref))
					editor.remove(entry.getKey());
				editor.commit();
			}

			Log.e("Logs: ", "Logs cleared");
		} catch (Exception e) {

		}
	}

	public static ArrayList<NameValuePair> getLogs(Context context) {

		ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
		try {
			listValuePair.add(new BasicNameValuePair("method", "log"));

			SharedPreferences settings = context.getSharedPreferences(LOG, 0);
			Map<String, ?> allEntries = settings.getAll();
			for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
				Object value = entry.getValue();
				if (value == null)
					value = "";
				listValuePair.add(new BasicNameValuePair(entry.getKey(), value
						.toString()));
				Log.e("map values", entry.getKey() + ": " + value.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listValuePair;
	}

	public static void logStackTrace(Context context, Exception e) {
		String traceString = "Trace: " + e.getMessage() + ": ";
		StackTraceElement[] trace = e.getStackTrace();
		for (StackTraceElement t : trace) {
			traceString += t.toString() + "; ";
		}
		// log(context, traceString);
	}

	public static Tracker setAnalyticsTracker(Context context, Tracker tracker) {
		try {
			GoogleAnalytics analytics;
			Tracker tracker1;
			analytics = GoogleAnalytics.getInstance(context);
			tracker1 = analytics.newTracker(R.xml.global_tracker);
			return tracker1;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public static void trackOnStart(Tracker tracker, String TAG) {
		try {
			tracker.setScreenName(TAG);
			tracker.send(new HitBuilders.EventBuilder().build());
		} catch (Exception e) {

		}
	}

	public static boolean isMobileValid(String mobile) {
		return mobile.matches("\\d{10}")
				&& (mobile.startsWith("7") || mobile.startsWith("8") || mobile
						.startsWith("9"));
	}

	public static boolean checkPlayServices(Activity activity) {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(activity);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, activity,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i("Check Play Services", "This device is not supported.");
				activity.finish();
			}
			return false;
		}
		return true;
	}

	public static String fetchLastRecharge(Context context, String number,
			String product_id) {
		ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

		listValuePair.add(new BasicNameValuePair("method", "lastTransaction"));
		listValuePair.add(new BasicNameValuePair("device_type", "android"));
		listValuePair.add(new BasicNameValuePair("number", number));
		listValuePair.add(new BasicNameValuePair("product_id", product_id));
		listValuePair.add(new BasicNameValuePair("retailer_id", Utility
				.getSharedPreferences(context, SHAREDPREFERENCE_RETAILER_ID)));

		String response = RequestClass.getInstance().readPay1Request(context,
				Constants.API_URL, listValuePair);

		return response;
	}

	public static void showTwoButtonBigDialog(Context context, String message,
			String title, String buttonText, String cancelText,
			int imageResource, final Runnable confirmBlock,
			final Runnable cancelBlock) {
		try {
			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.big_dialog_two_button);
			TextView messageView = (TextView) dialog.findViewById(R.id.message);
			messageView.setText(message);
			TextView titleView = (TextView) dialog.findViewById(R.id.title);
			ImageView image = (ImageView) dialog.findViewById(R.id.image);
			image.setImageResource(imageResource);
			if (title == null)
				titleView.setVisibility(View.GONE);
			else
				titleView.setText(title);

			Button buttonConfirm = (Button) dialog
					.findViewById(R.id.buttonConfirm);
			buttonConfirm.setText(buttonText);
			buttonConfirm.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					confirmBlock.run();
				}
			});

			Button buttonCancel = (Button) dialog
					.findViewById(R.id.buttonCancel);
			buttonCancel.setText(cancelText);
			buttonCancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					if (cancelBlock != null)
						cancelBlock.run();
				}
			});

			dialog.show();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public static void showOneButtonBigDialog(Context context, String message,
			String message2, String title, String buttonText,
			int imageResource, final Runnable confirmBlock) {
		try {
			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.big_dialog_one_button);
			TextView messageView = (TextView) dialog.findViewById(R.id.message);
			messageView.setText(message);
			TextView message2View = (TextView) dialog
					.findViewById(R.id.message2);
			message2View.setText(message2);
			TextView titleView = (TextView) dialog.findViewById(R.id.title);
			ImageView image = (ImageView) dialog.findViewById(R.id.image);
			image.setImageResource(imageResource);
			if (title == null)
				titleView.setVisibility(View.GONE);
			else
				titleView.setText(title);

			Button buttonConfirm = (Button) dialog.findViewById(R.id.button);
			buttonConfirm.setText(buttonText);
			buttonConfirm.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					confirmBlock.run();
				}
			});

			dialog.show();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return false;
		} catch (NullPointerException e) {
			return false;
		}

		return true;
	}

	public static void showTwoButtonDialogForUpload(Context context,
			String message, String title, String leftButtonText,
			String rightButtonText, final Runnable confirmBlock,
			final Runnable cancelBlock) {
		try {
			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.dialog_two_button);
			TextView messageView = (TextView) dialog
					.findViewById(R.id.textView_Message);
			messageView.setText(message);
			TextView titleView = (TextView) dialog
					.findViewById(R.id.textView_Title);
			if (title == null)
				titleView.setVisibility(View.GONE);
			else
				titleView.setText(title);

			Button buttonConfirm = (Button) dialog.findViewById(R.id.button_Ok);
			buttonConfirm.setText(leftButtonText);
			buttonConfirm.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					if (confirmBlock != null)
						confirmBlock.run();
				}
			});

			Button buttonCancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			buttonCancel.setText(rightButtonText);
			buttonCancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					if (cancelBlock != null)
						cancelBlock.run();
				}
			});

			dialog.show();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

}