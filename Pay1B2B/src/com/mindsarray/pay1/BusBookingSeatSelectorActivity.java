package com.mindsarray.pay1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.mindsarray.pay1.constant.Constants;

public class BusBookingSeatSelectorActivity extends Activity {

	private Button button_Done;
	private TableLayout tableLayout_Seats;
	private TextView textView_SelectedSeats;

	String sleeper = null;
	ArrayList<HashMap<String, String>> seat_list = new ArrayList<HashMap<String, String>>();

	ArrayList<Integer> cols = new ArrayList<Integer>();
	ArrayList<Integer> rows = new ArrayList<Integer>();
	ArrayList<String> selected_seats = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.busbookingseatselector_activity);
		try {
			tableLayout_Seats = (TableLayout) findViewById(R.id.tableLayout_Seats);
			textView_SelectedSeats = (TextView) findViewById(R.id.textView_SelectedSeats);

			// seats = getIntent().getExtras().getString("SEATS");
			sleeper = getIntent().getExtras().getString(Constants.BUS_SLEEPER);
			seat_list.addAll((ArrayList<HashMap<String, String>>) getIntent()
					.getSerializableExtra("SEAT_LIST"));

			for (int i = 0; i < seat_list.size(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				map = seat_list.get(i);
				try {
					cols.add(Integer.parseInt(map
							.get(Constants.BUS_SEAT_COLUMN)));
					rows.add(Integer.parseInt(map.get(Constants.BUS_SEAT_ROW)));
				} catch (Exception e) {
				}
			}
			HashSet<Integer> hs_cols = new HashSet<Integer>();
			hs_cols.addAll(cols);
			cols.clear();
			cols.addAll(hs_cols);
			Collections.sort(cols);

			HashSet<Integer> hs_rows = new HashSet<Integer>();
			hs_rows.addAll(rows);
			rows.clear();
			rows.addAll(hs_rows);
			Collections.sort(rows);

			for (int c = 0; c < cols.size(); c++) {
				Log.w("COLS ", "" + cols.get(c));
			}

			for (int r = 0; r < rows.size(); r++) {
				Log.w("ROWS ", "" + rows.get(r));
			}

			TextView[][] new_seat = new TextView[rows.size()][cols.size()];
			for (int r = 0; r < rows.size(); r++) {
				for (int c = 0; c < cols.size(); c++) {

					// Log.w("Array", "Row " + String.valueOf(rows.get(r))
					// + " Col " + String.valueOf(cols.get(c)));
					final TextView tv = new TextView(
							BusBookingSeatSelectorActivity.this);
					tv.setText("NA");
					tv.setGravity(Gravity.CENTER);
					// new_seat[r][c]
					// .setBackgroundResource(android.R.drawable.checkbox_off_background);
					tv.setBackgroundColor(android.R.color.white);
					tv.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							if (!tv.isSelected()) {
								if (selected_seats.size() == 5) {
									Constants
											.showOneButtonFailureDialog(
													BusBookingSeatSelectorActivity.this,
													"You can select maximum 5 seats.",
													"Bus Booking",
													Constants.OTHER_ERROR);
								} else {
									tv.setBackgroundColor(R.color.green);
									tv.setSelected(true);
									selected_seats.add(tv.getText().toString());
									textView_SelectedSeats
											.setText("Selected seats :"
													+ getSelectedSeats());
								}
							} else {
								tv.setBackgroundColor(android.R.color.white);
								tv.setSelected(false);
								for (int i = 0; i < selected_seats.size(); i++) {
									if (selected_seats.get(i).equalsIgnoreCase(
											tv.getText().toString()))
										selected_seats.remove(i);
								}
								textView_SelectedSeats
										.setText("Selected seats :"
												+ getSelectedSeats());
							}
						}
					});
					for (int i = 0; i < seat_list.size(); i++) {

						HashMap<String, String> map = new HashMap<String, String>();
						map = seat_list.get(i);

						// Log.w("Map", "Row " + map.get(Constants.BUS_SEAT_ROW)
						// + " Col " + map.get(Constants.BUS_SEAT_COLUMN));
						//
						// Log.w("Array", "Row " + String.valueOf(rows.get(r))
						// + " Col " + String.valueOf(cols.get(c)));

						if (map.get(Constants.BUS_SEAT_COLUMN)
								.equalsIgnoreCase(String.valueOf(cols.get(c)))
								&& map.get(Constants.BUS_SEAT_ROW)
										.equalsIgnoreCase(
												String.valueOf(rows.get(r)))) {

							if (map.get(Constants.BUS_SEAT_AVAILABLE)
									.equalsIgnoreCase("false")) {
								tv.setBackgroundColor(R.color.gray);
								tv.setSelected(true);
								tv.setEnabled(false);
							}
							if (map.get(Constants.BUS_SEAT_ZINDEX)
									.equalsIgnoreCase("0")) {
								tv.setText(map.get(Constants.BUS_SEAT_NAME));
								int r_l = 1, c_l = 1;
								try {
									r_l = Integer
											.parseInt(map
													.get(Constants.BUS_SEAT_ROW_LENGTH));
									c_l = Integer
											.parseInt(map
													.get(Constants.BUS_SEAT_COLUMN_LENGTH));
								} catch (Exception e) {
									r_l = 1;
									c_l = 1;
								}
								tv.setWidth(r_l * 50);
								tv.setHeight(c_l * 50);

								Log.w("Seat name ",
										"" + map.get(Constants.BUS_SEAT_NAME));
							}
						}

					}
					new_seat[r][c] = tv;
				}

			}

			// for (int r = 0; r < rows.size(); r++) {
			// for (int c = 0; c < cols.size(); c++) {
			// Log.w("Rev Seates at r[" + r + "]c[" + c + "]", ""
			// + new_seat[r][c].getText().toString().trim());
			// }
			// }

			TextView[][] trans_seat = new TextView[cols.size()][rows.size()];
			for (int r = 0; r < rows.size(); r++) {
				for (int c = 0; c < cols.size(); c++) {
					trans_seat[c][r] = new_seat[r][c];
				}
			}

			// TextView[][] reverse_seat = new
			// TextView[cols.size()][rows.size()];
			// for (int c = 0; c < cols.size(); c++) {
			// for (int r = 0; r < rows.size(); r++) {
			// reverse_seat[c][r] = trans_seat[c][r];
			// }
			// }

			for (int c = 0; c < cols.size(); c++) {
				for (int r = 0; r < rows.size(); r++) {
					Log.w("Rev Seates at r[" + c + "]c[" + r + "]", ""
							+ trans_seat[c][r].getText().toString().trim());
				}
			}

			// if (sleeper.equalsIgnoreCase("false")) {
			for (int r = 0; r < cols.size(); r++) {

				TableRow row = new TableRow(this);
				row.setBackgroundColor(android.R.color.white);
				TableRow.LayoutParams params = new TableRow.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				params.setMargins(20, 20, 20, 20);

				for (int c = rows.size() - 1; c >= 0; c--) {
					trans_seat[r][c].setLayoutParams(params);
					// TextView tv = new TextView(
					// BusBookingSeatSelectorActivity.this);
					// tv.setGravity(Gravity.CENTER);
					// tv.setLayoutParams(params);
					// tv.setBackgroundResource(android.R.drawable.checkbox_off_background);
					// // Log.w("Seates at r[" + r + "]c[" + c + "]", ""
					// // + reverse_seat[r][c]);
					// if (reverse_seat[r][c] != null) {
					// tv.setText(reverse_seat[r][c]);
					// } else {
					// tv.setText("N/a");
					// }
					String str = trans_seat[r][c].getText().toString().trim();
					if (str.equalsIgnoreCase("NA")) {
						trans_seat[r][c].setText("");
						trans_seat[r][c]
								.setBackgroundColor(android.R.color.white);
						trans_seat[r][c].setEnabled(false);

					}
					row.addView(trans_seat[r][c]);
				}
				tableLayout_Seats.addView(row, r);
			}
			// } else {
			// for (int r = 0; r < cols.size(); r++) {
			// for (int c = rows.size() - 1; c >= 0; c--) {
			// Log.w("Seates at r[" + r + "]c[" + c + "]", ""
			// + reverse_seat[cols.get(r)][rows.get(c)]
			// .getText().toString().trim());
			// }
			// }
			// }

			button_Done = (Button) findViewById(R.id.button_Done);
			button_Done.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent returnIntent = new Intent();
					returnIntent.putExtra("SEATS", getSelectedSeats());
					returnIntent.putExtra("SEATS_COUNT", selected_seats.size());
					setResult(RESULT_OK, returnIntent);
					finish();
				}
			});
		} catch (Exception exception) {

		}
	}

	private String getSelectedSeats() {
		if (selected_seats.size() != 0) {
			StringBuffer s = new StringBuffer();
			s.append(selected_seats.get(0));
			for (int i = 0; i < selected_seats.size(); i++) {
				if (i != 0)
					s.append(", ").append(selected_seats.get(i));
			}
			return s.toString();
		} else
			return "";
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
}
