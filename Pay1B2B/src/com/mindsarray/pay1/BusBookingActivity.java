package com.mindsarray.pay1;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.EditTextValidator;
import com.mindsarray.pay1.utilities.Utility;

public class BusBookingActivity extends Activity {

	private EditText editText_Source, editText_Destination;
	private TextView textView_Date;
	// textView_Seats;
	// private SeekBar seekBar_Seats;
	private Button button_Search;
	private String SOURCE_ID = null;
	private String DESTINATION_ID = null;
	private GetCityTask getCityTask;
	private final String fileName = "Cities";

	String months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
			"Sep", "Oct", "Nov", "Dec" };

	private int year;
	private int month;
	private int day;

	static final int DATE_PICKER_ID = 1111;
	static final int SOURCE_CITY_PICKER_ID = 1;
	static final int DESTINATION_CITY_PICKER_ID = 2;
	private static final String TAG = "Bus Booking";

	ArrayList<HashMap<String, String>> city_list = new ArrayList<HashMap<String, String>>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.busbooking_activity);
		try {

			editText_Source = (EditText) findViewById(R.id.editText_Source);
			editText_Source.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(BusBookingActivity.this,
							BusBookingCityActivity.class);
					intent.putExtra("CITY_LIST", city_list);
					startActivityForResult(intent, SOURCE_CITY_PICKER_ID);
				}
			});

			editText_Destination = (EditText) findViewById(R.id.editText_Destination);
			editText_Destination.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(BusBookingActivity.this,
							BusBookingCityActivity.class);
					intent.putExtra("CITY_LIST", city_list);
					startActivityForResult(intent, DESTINATION_CITY_PICKER_ID);
				}
			});
			// textView_Seats = (TextView) findViewById(R.id.textView_Seats);
			// textView_Seats.setText("1");

			textView_Date = (TextView) findViewById(R.id.textView_Date);
			final Calendar c = Calendar.getInstance();
			year = c.get(Calendar.YEAR);
			month = c.get(Calendar.MONTH);
			day = c.get(Calendar.DAY_OF_MONTH);

			// Show current date

			String dt = "";
			if (day < 10)
				dt = "0" + day;
			else
				dt = day + "";

			textView_Date.setText(new StringBuilder().append(dt).append("-")
					.append(months[month]).append("-").append(year));

			textView_Date.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showDialog(DATE_PICKER_ID);
				}
			});

			// seekBar_Seats = (SeekBar) findViewById(R.id.seekBar_Seats);
			// seekBar_Seats
			// .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
			// {
			//
			// @Override
			// public void onStopTrackingTouch(SeekBar seekBar) {
			// // TODO Auto-generated method stub
			//
			// }
			//
			// @Override
			// public void onStartTrackingTouch(SeekBar seekBar) {
			// // TODO Auto-generated method stub
			//
			// }
			//
			// @Override
			// public void onProgressChanged(SeekBar seekBar,
			// int progress, boolean fromUser) {
			// // TODO Auto-generated method stub
			// textView_Seats.setText("" + (progress + 1));
			// }
			// });

			button_Search = (Button) findViewById(R.id.button_Search);
			button_Search.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (!checkValidation(BusBookingActivity.this)) {

					} else if (editText_Source
							.getText()
							.toString()
							.trim()
							.equalsIgnoreCase(
									editText_Destination.getText().toString()
											.trim())) {
						Constants.showOneButtonFailureDialog(
								BusBookingActivity.this,
								"Source and destination can not be same.",
								"Bus Booking", 1);

					} else {
						Intent intent = new Intent(BusBookingActivity.this,
								BusBookingSearchActivity.class);
						intent.putExtra("SOURCE", editText_Source.getText()
								.toString().trim());
						intent.putExtra("SOURCE_ID", SOURCE_ID);
						intent.putExtra("DESTINATION", editText_Destination
								.getText().toString().trim());
						intent.putExtra("DESTINATION_ID", DESTINATION_ID);
						intent.putExtra("DATE", textView_Date.getText()
								.toString().trim());
						// intent.putExtra("SEATS", textView_Seats.getText()
						// .toString().trim());
						startActivity(intent);
					}
				}
			});

			getCityTask = new GetCityTask();
			getCityTask.execute();
		} catch (Exception exception) {
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_PICKER_ID:

			final Calendar c = Calendar.getInstance();
			year = c.get(Calendar.YEAR);
			month = c.get(Calendar.MONTH);
			day = c.get(Calendar.DAY_OF_MONTH);

			// open datepicker dialog.
			// set date picker for current date
			// add pickerListener listner to date picker
			// set date picker as current date
			DatePickerDialog _date = new DatePickerDialog(this, pickerListener,
					year, month, day) {
				@Override
				public void onDateChanged(DatePicker view, int myear,
						int monthOfYear, int dayOfMonth) {
					if (myear < year)
						view.updateDate(year, month, day);

					if (monthOfYear < month && myear == year)
						view.updateDate(year, month, day);

					if (dayOfMonth < day && myear == year
							&& monthOfYear == month)
						view.updateDate(year, month, day);

				}
			};
			return _date;
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		@Override
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {

			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			String dt = "";
			if (day < 10)
				dt = "0" + day;
			else
				dt = day + "";
			// Show selected date
			textView_Date.setText(new StringBuilder().append(dt).append("-")
					.append(months[month]).append("-").append(year));

		}
	};

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}

	private boolean checkValidation(Context context) {
		boolean ret = true;

		if (!EditTextValidator.hasText(context, editText_Source,
				Constants.ERROR_BUS_CITY_FIELD)) {
			ret = false;
			editText_Source.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editText_Destination,
				Constants.ERROR_BUS_CITY_FIELD)) {
			ret = false;
			editText_Destination.requestFocus();
			return ret;
		} else
			return ret;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == SOURCE_CITY_PICKER_ID) {
			if (resultCode == RESULT_OK) {
				editText_Source.setText(data
						.getStringExtra(Constants.BUS_CITY_NAME));
				SOURCE_ID = data.getStringExtra(Constants.BUS_CITY_ID);
				EditTextValidator.hasText(BusBookingActivity.this,
						editText_Source, Constants.ERROR_BUS_CITY_FIELD);
			}
		}
		if (requestCode == DESTINATION_CITY_PICKER_ID) {
			if (resultCode == RESULT_OK) {
				editText_Destination.setText(data
						.getStringExtra(Constants.BUS_CITY_NAME));
				DESTINATION_ID = data.getStringExtra(Constants.BUS_CITY_ID);
				EditTextValidator.hasText(BusBookingActivity.this,
						editText_Destination, Constants.ERROR_BUS_CITY_FIELD);
			}
		}
		// if (resultCode == RESULT_CANCELED) {
		// mBtnLogin.setVisibility(View.GONE);
		// mBtnLogOut.setVisibility(View.VISIBLE);
		//
		// }
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants.showNavigationBar(BusBookingActivity.this);
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	public class GetCityTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			// String response = RequestClass.getInstance().readPay1Request(
			// BusBookingActivity.this,
			// Constants.BUS_API_URL
			// + "method=busBooking&action=allsources");
			String response = null;
			try {

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String str = new StringBuilder().append(year).append("-")
						.append((month + 1)).append("-").append(day).toString();
				Date date1 = sdf.parse(str);
				Date date2 = sdf
						.parse(sdf.format(new Date(
								Utility.getCityLastUpdateTime(
										BusBookingActivity.this,
										Constants.SHAREDPREFERENCE_CITY_LAST_UPDATE_TIME))));

				Log.e("Date 1", sdf.format(date1));
				Log.e("Date 2", sdf.format(date2));

				if (date1.compareTo(date2) > 0) {
					Log.e("Date 1", "Date1 is after Date2");

					ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

					listValuePair.add(new BasicNameValuePair("method",
							"busBooking"));
					listValuePair.add(new BasicNameValuePair("action",
							"allsources"));

					response = RequestClass.getInstance().readPay1Request(
							BusBookingActivity.this, Constants.API_URL,
							listValuePair);
					if (!response.startsWith("Error")) {
						try {
							FileOutputStream fileOutputStream = BusBookingActivity.this
									.openFileOutput(fileName,
											Context.MODE_PRIVATE);
							ObjectOutputStream objectOutputStream = new ObjectOutputStream(
									fileOutputStream);
							objectOutputStream.writeObject(response);
							objectOutputStream.close();
						} catch (Exception exception) {
						}
						Utility.setCityLastUpdateTime(
								BusBookingActivity.this,
								Constants.SHAREDPREFERENCE_CITY_LAST_UPDATE_TIME,
								System.currentTimeMillis());
					}
				} else {
					try {
						FileInputStream fileInputStream = BusBookingActivity.this
								.openFileInput(fileName);
						ObjectInputStream objectInputStream = new ObjectInputStream(
								fileInputStream);
						Object yourObject = (Object) objectInputStream
								.readObject();
						objectInputStream.close();
						response = String.valueOf(yourObject);
					} catch (Exception exception) {
					}
				}
				// else if (date1.compareTo(date2) < 0) {
				// Log.e("Date 1", "Date1 is before Date2");
				// } else if (date1.compareTo(date2) == 0) {
				// Log.e("Date 1", "Date1 is equal to Date2");
				// } else {
				// Log.e("Date 1", "How to get here?");
				// }

			} catch (Exception ex) {
				ex.printStackTrace();
			}

			return response;

		}

		@Override
		protected void onPostExecute(String result) {
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			super.onPostExecute(result);
			try {
				if (!result.startsWith("Error")) {
					JSONObject jsonObject = new JSONObject(result);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("SUCCESS")) {

						JSONArray jsonArray = jsonObject.getJSONArray("cities");

						for (int i = 0; i < jsonArray.length(); i++) {
							HashMap<String, String> map = new HashMap<String, String>();
							map.put(Constants.BUS_CITY_ID, jsonArray
									.getJSONObject(i).getString("id")
									.toString().trim());
							map.put(Constants.BUS_CITY_NAME, jsonArray
									.getJSONObject(i).getString("name")
									.toString().trim());

							city_list.add(map);
						}
					} else {
						Constants.showOneButtonFailureDialog(
								BusBookingActivity.this,
								Constants.checkCode(result), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(
							BusBookingActivity.this, Constants.ERROR_INTERNET,
							TAG, Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(BusBookingActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(BusBookingActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(BusBookingActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							GetCityTask.this.cancel(true);
							dialog.cancel();
							finish();
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			GetCityTask.this.cancel(true);
			dialog.cancel();
			finish();
		}
	}
}
