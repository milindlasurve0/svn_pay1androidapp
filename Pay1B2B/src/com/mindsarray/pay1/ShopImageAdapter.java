package com.mindsarray.pay1;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mindsarray.pay1.utilities.Utility;

public class ShopImageAdapter extends PagerAdapter {
	Context context;

	ArrayList<Integer> drawables;
	LayoutInflater inflater;

	public ShopImageAdapter(Context context, ArrayList<Integer> drawables) {
		this.context = context;
		this.drawables = drawables;
		
	}

	@Override
	public int getCount() {
		return drawables.size();
	}

	/*@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((LinearLayout) object);
	}*/
	
	@Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View itemView = inflater.inflate(R.layout.slide_image_activity,
				container, false);

		ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
		imageView.setImageDrawable(context.getResources().getDrawable(drawables.get(position)));
		container.addView(itemView);

		return itemView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((LinearLayout) object);
	}

	

}

