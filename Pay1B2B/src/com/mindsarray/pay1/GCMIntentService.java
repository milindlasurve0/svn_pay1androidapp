package com.mindsarray.pay1;

import static com.mindsarray.pay1.constant.Constants.SENDER_ID;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Vibrator;
import android.util.Log;

//import com.google.android.gcm.GCMBaseIntentService;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.NotificationDataSource;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

public class GCMIntentService { //extends GCMBaseIntentService {
	Context mContext;
	int i = 1234;
	public static int count = 0;

	public GCMIntentService() {
//		super(SENDER_ID);
	}

	// private static final String TAG = "===GCMIntentService===";

//	@Override
//	protected void onRegistered(Context arg0, String registrationId) {
//		// Log.i(TAG, "Device registered: regId = " + registrationId);
//		Utility.setSharedPreference(arg0, Constants.SHAREDPREFERENCE_GCMID, registrationId);
//			/*Utility.setGCMID(this, Constants.SHAREDPREFERENCE_GCMID,
//					registrationId);*/
//	}
//
//	@Override
//	protected void onUnregistered(Context arg0, String arg1) {
//		 Log.i(TAG, "unregistered = " + arg1);
//
//	}
//
//	@Override
//	protected void onMessage(Context aContext, Intent arg1) {
//
//		
//		new CheckBalanceTask().execute();
//		
//		String msg = arg1.getExtras().getString("data");
//		String description = "", title = "", type = "";
//		try {
//			JSONObject jsonObject = new JSONObject(msg);
//			// JSONObject jsonObject2 = jsonObject.getJSONObject("msg");
//			description = jsonObject.getString("msg");
//			title = jsonObject.getString("title");
//			type = jsonObject.getString("type");
//			
//			if (type.equalsIgnoreCase("notification")) {
//				callNotification(aContext, type, description, title);
//				Notify(aContext, title, description);
//			} else if (type.equalsIgnoreCase("banner")) {
//				showBanner(aContext, type, description, msg);
//			}
//		} catch (Exception e) {
//			// e.printStackTrace();
//		}
//
//		// Log.i(TAG, msg);
//
//	}

	private void showBanner(Context aContext, String type, String description,
			String msg) {
		// TODO Auto-generated method stub
		NotificationDataSource notificationDataSource = new NotificationDataSource(
				aContext);
		try {
			notificationDataSource.open();
			notificationDataSource.createNotification(description, 1, ""
					+ System.currentTimeMillis());
		} catch (SQLException exception) {
			// TODO: handle exception
			// // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			// // Log.e("Er ", e.getMessage());
		} finally {
			notificationDataSource.close();
		}
//		Intent notifyIntent = new Intent(this, NotificationActivity.class);
//		notifyIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
//				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
//		//
//		notifyIntent.putExtra("pushBundle", msg);
//
//		aContext.startActivity(notifyIntent);
	}

	private void callNotification(Context aContext, String type,
			String description, String msg) {

		NotificationDataSource notificationDataSource = new NotificationDataSource(
				aContext);
		try {
			notificationDataSource.open();
			notificationDataSource.createNotification(description, 1, ""
					+ System.currentTimeMillis());
		} catch (SQLException exception) {
			// TODO: handle exception
			// // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			// // Log.e("Er ", e.getMessage());
		} finally {
			notificationDataSource.close();
		}
		/*
		 * Intent notifyIntent = new Intent(this, NotificationActivity.class);
		 * notifyIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
		 * Intent.FLAG_ACTIVITY_SINGLE_TOP); //
		 * notifyIntent.putExtra("pushBundle", msg);
		 * 
		 * aContext.startActivity(notifyIntent);
		 */

	}

//	@Override
//	protected void onError(Context arg0, String errorId) {
//		// Log.i(TAG, "Received error: " + errorId);
//	}
//
//	@Override
//	protected boolean onRecoverableError(Context context, String errorId) {
//		return super.onRecoverableError(context, errorId);
//	}
//
//	private void Notify(Context mContext, String notificationTitle,
//			String notificationMessage) {
//		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//
//		Notification notification = new Notification(
//				R.drawable.ic_b2b_notif, "Pay1 notification",
//				System.currentTimeMillis());
//		// Constants.notificationList.add(notificationMessage);
//		notification.number = Constants.COUNT_NOTIFY++;
//		Utility.setNotificationCount(mContext, "NotiCount",
//				Constants.COUNT_NOTIFY);
//		Intent notificationIntent = new Intent(this, NotificationActivity.class);
//		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
//				notificationIntent, 0);
//
//		try {
//			Uri notification1 = RingtoneManager
//					.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//			Ringtone r = RingtoneManager.getRingtone(getApplicationContext(),
//					notification1);
//			r.play();
//
//			Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
//
//			// Vibrate for 1000 milliseconds
//			v.vibrate(1000);
//		} catch (Exception e) {
//		}
//		// notification.number=count++;
//		notification.setLatestEventInfo(mContext, notificationTitle,
//				notificationMessage, pendingIntent);
//		notification.flags |= Notification.FLAG_AUTO_CANCEL;
//		notificationManager.notify(9999, notification);
//	}

	
//	public class CheckBalanceTask extends AsyncTask<String, String, String> {
//
//		@Override
//		protected String doInBackground(String... params) {
//			// TODO Auto-generated method stub
//			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
//
//			listValuePair.add(new BasicNameValuePair("method", "updateBal"));
//			String response = RequestClass.getInstance()
//					.readPay1Request(GCMIntentService.this,
//							Constants.API_URL, listValuePair);
//			listValuePair.clear();
//
//			return response;
//
//		}
//
//		/*
//		 * (non-Javadoc)
//		 * 
//		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
//		 */
//		@Override
//		protected void onPostExecute(String result) {
//			// TODO Auto-generated method stub
//			super.onPostExecute(result);
//			try {
//				if (!result.startsWith("Error")) {
//					String replaced = result.replace("(", "").replace(")", "")
//							.replace(";", "");
//
//					JSONArray array = new JSONArray(replaced);
//					JSONObject jsonObject = array.getJSONObject(0);
//					String status = jsonObject.getString("status");
//					// {"status":"success","description":"0","login":0}
//					if (status.equalsIgnoreCase("success")) {
//						if (jsonObject.getString("login").equalsIgnoreCase("1")) {
//							Utility.setBalance(GCMIntentService.this,
//									Constants.SHAREDPREFERENCE_BALANCE,
//									jsonObject.getString("description"));
//						} else {
//
//						}
//					}
//				}
//
//			} catch (JSONException e) {
//				// e.printStackTrace();
//				/*
//				 * Constants.showOneButtonFailureDialog(SplashScreenActivity.this
//				 * , Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
//				 * Constants.OTHER_ERROR);
//				 */
//
//			} catch (Exception e) {
//				// TODO: handle exception
//				/*
//				 * Constants.showOneButtonFailureDialog(SplashScreenActivity.this
//				 * , Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
//				 * Constants.OTHER_ERROR);
//				 */
//
//			}
//		}
//	}
	
}
