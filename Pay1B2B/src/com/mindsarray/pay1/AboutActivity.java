package com.mindsarray.pay1;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.widget.TextView;

public class AboutActivity extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_activity);
		TextView textViewVersion=(TextView)findViewById(R.id.textVersion);
		PackageManager manager = AboutActivity.this.getPackageManager();
		PackageInfo info;
		try {
			info = manager.getPackageInfo(
					AboutActivity.this.getPackageName(), 0);
			String version = info.versionName;
			textViewVersion.setText("Version "+version);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
