package com.mindsarray.pay1.adapterhandler;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mindsarray.pay1.DiscountStructureDthFragment;
import com.mindsarray.pay1.DiscountStructureEntertainmentFragment;
import com.mindsarray.pay1.DiscountStructureMobileFragment;
import com.mindsarray.pay1.DiscountStuctureOnlinePayment;
import com.mindsarray.pay1.DiscountStucturePay1TopUp;

public class DiscountStructureFragmentAdapter extends FragmentPagerAdapter {
	protected static final String[] CONTENT = new String[] { "Mobile", "DTH",
			"Entertainment","Wallet Topup","Online Payment" };

	private int mCount = CONTENT.length;

	public DiscountStructureFragmentAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		// return DiscountStructureFragment.newInstance("" + (position + 1));
		switch (position) {

		case 0:
			return DiscountStructureMobileFragment.newInstance("1");
		case 1:
			return DiscountStructureDthFragment.newInstance("2");
		case 2:
			return DiscountStructureEntertainmentFragment.newInstance("3");
		case 3:
			return DiscountStucturePay1TopUp.newInstance("5");
		case 4:
			return DiscountStuctureOnlinePayment.newInstance("7");
		default:
			return null;
		}
	}

	@Override
	public int getCount() {
		return mCount;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return DiscountStructureFragmentAdapter.CONTENT[position
				% CONTENT.length];
	}

	public void setCount(int count) {
		if (count > 0 && count <= 10) {
			mCount = count;
			notifyDataSetChanged();
		}
	}
}