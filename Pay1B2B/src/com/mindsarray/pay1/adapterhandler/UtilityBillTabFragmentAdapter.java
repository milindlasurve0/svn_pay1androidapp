package com.mindsarray.pay1.adapterhandler;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mindsarray.pay1.ReversalFragement;
import com.mindsarray.pay1.StatusFragment;
import com.mindsarray.pay1.UtilityBillMainFragment;

public class UtilityBillTabFragmentAdapter extends FragmentPagerAdapter {
	protected static final String[] CONTENT = new String[] { "BILL",
		"TRANSACTION HISTORY", "COMPLAINT STATUS" };

	private int mCount = CONTENT.length;

	public UtilityBillTabFragmentAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		// return TestFragment.newInstance(CONTENT[position % CONTENT.length]);
		switch (position) {

		case 0:
			return UtilityBillMainFragment
					.newInstance("FirstFragment, Instance 1");
		case 1:
			return ReversalFragement.newInstance("ThirdFragment, Default");
		case 2:
			
			return StatusFragment.newInstance("SecondFragment, Instance 1");
		default:
			return null;
		}
	}

	@Override
	public int getCount() {
		return mCount;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return UtilityBillTabFragmentAdapter.CONTENT[position
				% CONTENT.length];
	}

	public void setCount(int count) {
		if (count > 0 && count <= 10) {
			mCount = count;
			notifyDataSetChanged();
		}
	}
}