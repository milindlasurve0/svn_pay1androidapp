package com.mindsarray.pay1.adapterhandler;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.mindsarray.pay1.ReversalStatusDthFragment;
import com.mindsarray.pay1.ReversalStatusEntertainmentFragment;
import com.mindsarray.pay1.ReversalStatusMobileBillFragment;
import com.mindsarray.pay1.ReversalStatusMobileFragment;
import com.mindsarray.pay1.ReversalStatusUtilityBillFragment;
import com.mindsarray.pay1.ReversalStatusWalletFragment;

public class ReversalStatusTabFragmentAdapter extends FragmentStatePagerAdapter {
	protected static final String[] CONTENT = new String[] { "Mobile", "Dth",
			"Mobile Bill", "Entertainment", "Pay1 Topup", "Utility Bill","Online Payment" };

	private int mCount = CONTENT.length;

	public ReversalStatusTabFragmentAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		// return TestFragment.newInstance(CONTENT[position % CONTENT.length]);
		switch (position) {

		case 0:
			return ReversalStatusMobileFragment
					.newInstance("1");
		case 1:
			return ReversalStatusDthFragment
					.newInstance("2");
		case 2:
			return ReversalStatusMobileBillFragment
					.newInstance("4");
		case 3:
			return ReversalStatusEntertainmentFragment
					.newInstance("3");
		case 4:
			return ReversalStatusWalletFragment
					.newInstance("5");
		case 5:
			return ReversalStatusUtilityBillFragment
					.newInstance("6");
		case 6:
			return ReversalStatusUtilityBillFragment
					.newInstance("7");
		
		default:
			return null;
		}
	}

	@Override
	public int getCount() {
		return mCount;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return TransactionTabFragmentAdapter.CONTENT[position % CONTENT.length];
	}

	public void setCount(int count) {
		if (count > 0 && count <= 10) {
			mCount = count;
			notifyDataSetChanged();
		}
	}
}