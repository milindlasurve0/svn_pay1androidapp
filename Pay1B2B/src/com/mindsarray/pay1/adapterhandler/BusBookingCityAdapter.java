package com.mindsarray.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.HashMap;

import com.mindsarray.pay1.R;
import com.mindsarray.pay1.constant.Constants;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class BusBookingCityAdapter extends BaseAdapter {

	Context context;
	ArrayList<HashMap<String, String>> data;

	public BusBookingCityAdapter(Context context,
			ArrayList<HashMap<String, String>> data) {
		this.context = context;
		this.data = data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.busbookingcity_adapter,
					null);
			viewHolder = new ViewHolder();
			viewHolder.textView_CityName = (TextView) convertView
					.findViewById(R.id.textView_CityName);
			viewHolder.textView_CityID = (TextView) convertView
					.findViewById(R.id.textView_CityID);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		HashMap<String, String> map = new HashMap<String, String>();
		map = data.get(position);
		viewHolder.textView_CityName.setText(map.get(Constants.BUS_CITY_NAME)
				.toString().trim());
		viewHolder.textView_CityID.setText(map.get(Constants.BUS_CITY_ID)
				.toString().trim());

		return convertView;
	}

	private class ViewHolder {
		TextView textView_CityName, textView_CityID;
	}

}
