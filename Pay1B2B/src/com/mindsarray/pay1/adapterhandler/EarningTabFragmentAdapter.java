package com.mindsarray.pay1.adapterhandler;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mindsarray.pay1.EarningMainFragment;

public class EarningTabFragmentAdapter extends FragmentPagerAdapter {
	protected static final String[] CONTENT = new String[] { "DTH", "Status",
			"Request" };

	private int mCount = CONTENT.length;

	public EarningTabFragmentAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		// return TestFragment.newInstance(CONTENT[position % CONTENT.length]);
		switch (position) {

		case 0:
			return EarningMainFragment
					.newInstance("FirstFragment, Instance 1");
		case 1:
			return EarningMainFragment.newInstance("SecondFragment, Instance 1");
		case 2:
			return EarningMainFragment.newInstance("ThirdFragment, Default");
		default:
			return null;
		}
	}

	@Override
	public int getCount() {
		return mCount;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return EarningTabFragmentAdapter.CONTENT[position
				% CONTENT.length];
	}

	public void setCount(int count) {
		if (count > 0 && count <= 10) {
			mCount = count;
			notifyDataSetChanged();
		}
	}
}