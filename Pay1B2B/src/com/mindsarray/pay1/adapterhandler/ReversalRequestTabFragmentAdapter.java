package com.mindsarray.pay1.adapterhandler;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.mindsarray.pay1.ReversalRequestDthFragment;
import com.mindsarray.pay1.ReversalRequestEntertainmentFragment;
import com.mindsarray.pay1.ReversalRequestMobileBillFragment;
import com.mindsarray.pay1.ReversalRequestMobileFragment;
import com.mindsarray.pay1.ReversalRequestUtilityBillFragment;
import com.mindsarray.pay1.ReversalRequestWalletFragment;

public class ReversalRequestTabFragmentAdapter extends
		FragmentStatePagerAdapter {
	protected static final String[] CONTENT = new String[] { "Mobile", "Dth",
			"Mobile Bill", "Entertainment", "Pay1 Topup", "Utility Bill","Online Payment" };

	private int mCount = CONTENT.length;

	public ReversalRequestTabFragmentAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		// return TestFragment.newInstance(CONTENT[position % CONTENT.length]);
		switch (position) {

		case 0:
			return ReversalRequestMobileFragment
					.newInstance("1");
		case 1:
			return ReversalRequestDthFragment
					.newInstance("2");
		case 2:
			return ReversalRequestMobileBillFragment
					.newInstance("4");
		case 3:
			return ReversalRequestEntertainmentFragment
					.newInstance("3");
		case 4:
			return ReversalRequestWalletFragment
					.newInstance("5");
		case 5:
			return ReversalRequestUtilityBillFragment
					.newInstance("6");
		case 6:
			return ReversalRequestUtilityBillFragment
					.newInstance("7");
		default:
			return null;
		}
	}

	@Override
	public int getCount() {
		return mCount;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return ReversalRequestTabFragmentAdapter.CONTENT[position
				% CONTENT.length];
	}

	public void setCount(int count) {
		if (count > 0 && count <= 10) {
			mCount = count;
			notifyDataSetChanged();
		}
	}
}