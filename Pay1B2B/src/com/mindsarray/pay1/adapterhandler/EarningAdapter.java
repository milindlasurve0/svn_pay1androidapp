package com.mindsarray.pay1.adapterhandler;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.net.ParseException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mindsarray.pay1.R;

public class EarningAdapter extends ArrayAdapter<JSONObject> {
	List<JSONObject> list;
	Context mContext;
	LayoutInflater inflater;

	public EarningAdapter(Context context, int textViewResourceId,
			List<JSONObject> objects) {
		super(context, textViewResourceId, objects);
		// TODO Auto-generated constructor stub
		this.list = objects;
		this.mContext = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		try {

			if (convertView == null) {
				inflater = (LayoutInflater) mContext
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.earning_adapter, null);
			}

			if (position % 2 == 1) {
				view.setBackgroundColor(Color.WHITE);
			} else {
				// view.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.border_blue));/*Color(Color.WHITE);*/
				view.setBackgroundColor(mContext.getResources().getColor(
						R.color.app_faint_theme_secondry));
			}
			view.setBackgroundColor(Color.WHITE);
			TextView textDate = (TextView) view.findViewById(R.id.textDate);
			TextView textSale = (TextView) view.findViewById(R.id.textSale);
			TextView textEarnings = (TextView) view
					.findViewById(R.id.textEarnings);
			try {
				SimpleDateFormat dateFormat = new SimpleDateFormat(
						"dd-MMM-yyyy");
				String d = list.get(position).getString("date").toString()
						.trim().replaceAll("-", "/");
				Date date = new Date(d);// new Date(new Date());
				DecimalFormat decimalFormat = new DecimalFormat(".00");
				textSale.setText("Sales : Rs "
						+ decimalFormat.format(Double.parseDouble(list
								.get(position).getString("amount").toString()
								.trim())));
				textEarnings.setText("Earning : Rs "
						+ list.get(position).getString("income"));
				textDate.setText(dateFormat.format(date));
			} catch (ParseException e) {
				// e.printStackTrace();
			} catch (Exception exception) {
				// exception.printStackTrace();
			}
		} catch (Exception exception) {
		}
		return view;

	}

}
