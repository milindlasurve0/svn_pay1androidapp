package com.mindsarray.pay1.adapterhandler;

import java.util.ArrayList;

import com.mindsarray.pay1.R;
import com.mindsarray.pay1.contenthandler.Document;
import com.mindsarray.pay1.utilities.AttachImage;
import com.mindsarray.pay1.utilities.DocumentButton;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by rohan on 23/5/15.
 */
public class ImageAdapter extends BaseAdapter {

    Context mContext;
    public ArrayList<String> mImageURIRemovalList;
    public ArrayList<Document> mDocuments;
    ImageButton mButton;
    DocumentButton mDocumentButton;

    public ImageAdapter(Context context, ArrayList<Document> documents, ImageButton button, DocumentButton documentButton) {
        mContext = context;
        mDocuments = documents;
        mButton = button;
        mDocumentButton = documentButton;
        mImageURIRemovalList = new ArrayList<>();
    }

    public int getCount() {
        return mDocuments.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return mDocuments.size();
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.adapter_image, null);
            viewHolder = new ViewHolder();
            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.imageview_photo);
            Picasso.with(mContext)
                    .load(mDocuments.get(position).uri)
                    .resize(AttachImage.THUMBNAIL_WIDTH, AttachImage.THUMBNAIL_HEIGHT)
                    .centerCrop()
                    .placeholder(R.drawable.ic_imgfloader)
                    .into(viewHolder.imageView);
            viewHolder.button = (Button) convertView.findViewById(R.id.button_remove_photo);
            viewHolder.button.setTag(position);
            
            viewHolder.text_reject=(TextView)convertView.findViewById(R.id.text_reject);


            if( mDocuments.get(position).comment!=null){

            }else{

            }
            viewHolder.text_reject.setText(mDocuments.get(position).comment);
            
            viewHolder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int tg = (int)v.getTag();
                    if(mDocuments.get(tg).uri.startsWith("http"))
                        mImageURIRemovalList.add(mDocuments.get(tg).uri);
                    mDocuments.remove(tg);
                    if(mDocuments.size() > 0) {
                        mDocumentButton.setPresent(true);
                        if(mDocuments.get(0).verify_flag.equals("1"))
                            mDocumentButton.setVerified(true);
                        else
                            mDocumentButton.setVerified(false);
                    }
                    else
                        mDocumentButton.setPresent(false);
                    notifyDataSetChanged();
                    mButton.setVisibility(View.VISIBLE);
                }
            });

            if(mDocuments.size() > 0) {
                mDocumentButton.setPresent(true);
                if(mDocuments.get(0).verify_flag.equals("1"))
                    mDocumentButton.setVerified(true);
                else
                    mDocumentButton.setVerified(false);
            }
            else
                mDocumentButton.setPresent(false);
        }
        return convertView;
    }

    public class ViewHolder {
        ImageView imageView;
        Button button;
        TextView text_reject;
    }
}