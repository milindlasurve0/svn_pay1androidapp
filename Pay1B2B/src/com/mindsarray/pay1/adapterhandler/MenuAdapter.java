package com.mindsarray.pay1.adapterhandler;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindsarray.pay1.LoginActivity;
import com.mindsarray.pay1.R;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.utilities.Utility;

public class MenuAdapter extends BaseAdapter {

	Context context;
	String[] slide_Menu;
	int[] slide_Icon;

	public MenuAdapter(Context context) {
		this.context = context;
	}

	public MenuAdapter(Context context, String[] slide_Menu, int[] slide_Icon) {
		this.context = context;
		this.slide_Menu = slide_Menu;
		this.slide_Icon = slide_Icon;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return slide_Menu.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater
					.inflate(R.layout.menu_listview_adapter, null);
			viewHolder = new ViewHolder();
			viewHolder.textView_MenuName = (TextView) convertView
					.findViewById(R.id.textView_MenuName);
			viewHolder.imageView_Icon = (ImageView) convertView
					.findViewById(R.id.imageView_Icon_Side);
			viewHolder.textViewNotifySide = (TextView) convertView
					.findViewById(R.id.textViewNotifySide);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		convertView.setBackgroundColor(Color.parseColor("#1C1C1C"));
		viewHolder.textViewNotifySide.setVisibility(View.INVISIBLE);
		if (Utility.getLoginFlag(context, Constants.SHAREDPREFERENCE_IS_LOGIN)) {
			if (position == 1) {
				int NotiCount = Utility.getNotificationCount(context,
						"NotiCount");
				if (NotiCount != 0) {
					viewHolder.textViewNotifySide.setVisibility(View.VISIBLE);

					viewHolder.textViewNotifySide.setText("" + NotiCount);
					viewHolder.textViewNotifySide.bringToFront();
				}
			}
			if (position == 4) {
				int ChatCount = Utility.getChatCount(context,
						"ChatCount");
				if (ChatCount != 0) {
					viewHolder.textViewNotifySide.setVisibility(View.VISIBLE);

					viewHolder.textViewNotifySide.setText("" + ChatCount);
					viewHolder.textViewNotifySide.bringToFront();
				}
			}
		} else {
			if (position == 2) {
				int NotiCount = Utility.getNotificationCount(context,
						"NotiCount");
				if (NotiCount != 0) {
					viewHolder.textViewNotifySide.setVisibility(View.VISIBLE);

					viewHolder.textViewNotifySide.setText("" + NotiCount);
					viewHolder.textViewNotifySide.bringToFront();
				}
			}
			if (position == 5) {
				int ChatCount = Utility.getChatCount(context,
						"ChatCount");
				if (ChatCount != 0) {
					viewHolder.textViewNotifySide.setVisibility(View.VISIBLE);

					viewHolder.textViewNotifySide.setText("" + ChatCount);
					viewHolder.textViewNotifySide.bringToFront();
				}
			}
		}
		viewHolder.textView_MenuName.setText(slide_Menu[position].toUpperCase());
		viewHolder.imageView_Icon.setImageResource(slide_Icon[position]);
		viewHolder.textView_MenuName.setTextColor(Color.WHITE);
		
		return convertView;
	}

	private class ViewHolder {
		TextView textView_MenuName, textViewNotifySide;
		ImageView imageView_Icon;

	}

}
