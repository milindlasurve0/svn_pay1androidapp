package com.mindsarray.pay1.adapterhandler;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.mindsarray.pay1.constant.Constants;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

public class PlaceArrayAdapter extends
		ArrayAdapter<String> implements Filterable {
	private static final String TAG = "PlaceArrayAdapter";
	private GoogleApiClient mGoogleApiClient;
	private AutocompleteFilter mPlaceFilter;
	private LatLngBounds mBounds;
	private ArrayList<String> mResultList;
	private LatLngBounds.Builder mBoundsBuilder;

	private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
	private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
	private static final String OUT_JSON = "/json";

	/**
	 * Constructor
	 *
	 * @param context
	 *            Context
	 * @param resource
	 *            Layout resource
	 * @param bounds
	 *            Used to specify the search bounds
	 * @param filter
	 *            Used to specify place types
	 */
	public PlaceArrayAdapter(Context context, int resource) {
		super(context, resource);

		mBoundsBuilder = new LatLngBounds.Builder();
		mBoundsBuilder.include(new LatLng(38, 68));
		mBoundsBuilder.include(new LatLng(5, 68));
		mBoundsBuilder.include(new LatLng(5, 100));
		mBoundsBuilder.include(new LatLng(38, 100));
		mBounds = mBoundsBuilder.build();

		List<Integer> filterTypes = new ArrayList<Integer>();
		filterTypes.add(Place.TYPE_GEOCODE);
		// filterTypes.add(Place.TYPE_LOCALITY);
		// filterTypes.add(Place.TYPE_ADMINISTRATIVE_AREA_LEVEL_3);
		mPlaceFilter = AutocompleteFilter.create(filterTypes);
	}

	public void setGoogleApiClient(GoogleApiClient googleApiClient) {
		if (googleApiClient == null || !googleApiClient.isConnected()) {
			mGoogleApiClient = null;
		} else {
			mGoogleApiClient = googleApiClient;
		}
	}

	@Override
	public int getCount() {
		return mResultList.size();
	}

	@Override
	public String getItem(int position) {
		return mResultList.get(position);
	}

	private ArrayList<String> getPredictions(CharSequence constraint) {
		if (mGoogleApiClient != null) {
			Log.i(TAG, "Executing autocomplete query for: " + constraint);

			PendingResult<AutocompletePredictionBuffer> results = Places.GeoDataApi
					.getAutocompletePredictions(mGoogleApiClient,
							constraint.toString(), mBounds, mPlaceFilter);
			// Wait for predictions, set the timeout.
			AutocompletePredictionBuffer autocompletePredictions = results
					.await(60, TimeUnit.SECONDS);
			final Status status = autocompletePredictions.getStatus();
			if (!status.isSuccess()) {
				// Toast.makeText(getContext(), "Error: " + status.toString(),
				// Toast.LENGTH_SHORT).show();
				Log.e(TAG,
						"Error getting place predictions: " + status.toString());
				autocompletePredictions.release();
				return null;
			}

			Log.i(TAG,
					"Query completed. Received "
							+ autocompletePredictions.getCount()
							+ " predictions.");
			Iterator<AutocompletePrediction> iterator = autocompletePredictions
					.iterator();
			ArrayList<String> resultList = new ArrayList<String>(
					autocompletePredictions.getCount());
			while (iterator.hasNext()) {
				AutocompletePrediction prediction = iterator.next();
				if (prediction.getDescription().contains("India"))
					resultList.add(prediction.getDescription());
			}
			// Buffer release
			autocompletePredictions.release();

			if (resultList.size() == 0) {
				return null;
			}
			return resultList;
		}
		Log.e(TAG, "Google API client is not connected.");
		return null;
	}

	public static ArrayList autocomplete(String input) {
		ArrayList resultList = null;

		HttpURLConnection conn = null;
		StringBuilder jsonResults = new StringBuilder();
		try {
			StringBuilder sb = new StringBuilder(PLACES_API_BASE
					+ TYPE_AUTOCOMPLETE + OUT_JSON);
			sb.append("?key=" + Constants.GOOGLE_MAPS_BROWSER_API_KEY);
			sb.append("&components=country:in");
			sb.append("&types=(cities)");
			sb.append("&input=" + URLEncoder.encode(input, "utf8"));

			URL url = new URL(sb.toString());
			conn = (HttpURLConnection) url.openConnection();
			InputStreamReader in = new InputStreamReader(conn.getInputStream());

			// Load the results into a StringBuilder
			int read;
			char[] buff = new char[1024];
			while ((read = in.read(buff)) != -1) {
				jsonResults.append(buff, 0, read);
			}
		} catch (MalformedURLException e) {
			Log.e(TAG, "Error processing Places API URL", e);
			return resultList;
		} catch (IOException e) {
			Log.e(TAG, "Error connecting to Places API", e);
			return resultList;
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

		try {
			// Create a JSON object hierarchy from the results
			JSONObject jsonObj = new JSONObject(jsonResults.toString());
			JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

			// Extract the Place descriptions from the results
			resultList = new ArrayList(predsJsonArray.length());
			for (int i = 0; i < predsJsonArray.length(); i++) {
				Log.e("locations", predsJsonArray.getJSONObject(i).getString("description"));
				resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
			}
		} catch (JSONException e) {
			Log.e(TAG, "Cannot process JSON results", e);
		}

		return resultList;
	}

	@Override
	public Filter getFilter() {
		Filter filter = new Filter() {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
				if (constraint != null) {
					// Query the autocomplete API for the entered constraint
//					mResultList = getPredictions(constraint);
					mResultList = autocomplete(constraint.toString());
					if (mResultList != null) {
						// Results
						results.values = mResultList;
						results.count = mResultList.size();
					}
				}
				return results;
			}

			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				if (results != null && results.count > 0) {
					// The API returned at least one result, update the data.
					notifyDataSetChanged();
				} else {
					// The API did not return any results, invalidate the data
					// set.
					notifyDataSetInvalidated();
				}
			}
		};
		return filter;
	}
}