package com.mindsarray.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mindsarray.pay1.R;

public class NotificationNewAdapter extends
		ArrayAdapter<HashMap<String, String>> {
	Context context;
	int layoutResourceId;
	ArrayList<HashMap<String, String>> data;
	private LinearLayout wrapper;
	private TextView countryName;

	public NotificationNewAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
	}

	public NotificationNewAdapter(Context context, int layoutResourceId,
			ArrayList<HashMap<String, String>> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}

	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
try{
		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(R.layout.listitem_discuss, parent, false);
		}
			wrapper = (LinearLayout) row.findViewById(R.id.wrapper);

			countryName = (TextView) row.findViewById(R.id.comment);

			TextView textTime = (TextView) row.findViewById(R.id.textViewTime);

			// String string = data.get(position).get("id");
			// holder.txtTitle.setText(data.get(position).get("notification") +
			// "\n"
			// + string + "\n");
			countryName.setText(data.get(position).get("notification"));
			textTime.setText(data.get(position).get("time"));

			String netorSms = data.get(position).get("netOrSms");
			if (netorSms.equalsIgnoreCase("1")) {

//				countryName.setBackgroundResource(R.drawable.bubble_yellow);

				wrapper.setGravity(Gravity.LEFT);
			} else if (netorSms.equalsIgnoreCase("0")) {
//				countryName.setBackgroundResource(R.drawable.bubble_green);
				wrapper.setGravity(Gravity.RIGHT);

			}
			
			if (position % 2 == 1) {
				wrapper.setBackgroundColor(Color.WHITE);  
			} else {
				wrapper.setBackgroundColor(Color.parseColor("#F3F3F3"));  
			}
			/*
			 * if (netorSms.equalsIgnoreCase("0")) {
			 * row.setBackgroundColor(context
			 * .getResources().getColor(R.color.white)); } else if
			 * (netorSms.equalsIgnoreCase("1")) {
			 * row.setBackgroundColor(context.getResources().getColor(
			 * R.color.LightGrey)); }
			 */
		
		return row;
}catch(Exception exc){
	return row;
}

	}

	public Bitmap decodeToBitmap(byte[] decodedByte) {
		return BitmapFactory
				.decodeByteArray(decodedByte, 0, decodedByte.length);
	}

}