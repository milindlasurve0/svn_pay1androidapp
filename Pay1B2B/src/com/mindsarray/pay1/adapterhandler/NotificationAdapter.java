package com.mindsarray.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mindsarray.pay1.R;

public class NotificationAdapter extends ArrayAdapter<HashMap<String, String>> {
	Context context;
	int layoutResourceId;
	ArrayList<HashMap<String, String>> data;

	public NotificationAdapter(Context context, int layoutResourceId,
			ArrayList<HashMap<String, String>> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		// RecordHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			// holder = new RecordHolder();
			TextView txtTitle = (TextView) row.findViewById(R.id.comment);
			TextView txtTime = (TextView) row.findViewById(R.id.textViewTime);

			// String string = data.get(position).get("id");
			// holder.txtTitle.setText(data.get(position).get("notification") +
			// "\n"
			// + string + "\n");
			txtTitle.setText(data.get(position).get("notification"));
			txtTime.setText(data.get(position).get("time"));
			String netorSms = data.get(position).get("netOrSms");
			if (netorSms.equalsIgnoreCase("0")) {
				row.setBackgroundColor(context.getResources().getColor(
						R.color.white));
			} else if (netorSms.equalsIgnoreCase("1")) {
				row.setBackgroundColor(context.getResources().getColor(
						R.color.LightGrey));

			}

		}
		return row;
	}

}