package com.mindsarray.pay1.adapterhandler;

import java.util.List;
import java.util.zip.Inflater;

import com.mindsarray.pay1.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SettingsAdapter extends ArrayAdapter<String> {
	Context context;
	List<String> objects;
	LayoutInflater inflater;

	public SettingsAdapter(Context context, int textViewResourceId,
			List<String> objects) {
		super(context, textViewResourceId, objects);
		this.objects = objects;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		View view = convertView;
		try {

			if (convertView == null) {
				inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.settings_adapter, null);
			}

			TextView textViewSetting = (TextView) view
					.findViewById(R.id.textViewSetting);
			textViewSetting.setText(objects.get(position));

		} catch (Exception exc) {

		}

		return view;

	}

}
