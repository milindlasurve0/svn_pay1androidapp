package com.mindsarray.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mindsarray.pay1.R;
import com.mindsarray.pay1.constant.Constants;

public class BusBookingSummaryAdapter extends BaseAdapter {

	Context context;
	ArrayList<HashMap<String, String>> data;

	public BusBookingSummaryAdapter(Context context,
			ArrayList<HashMap<String, String>> data) {
		this.context = context;
		this.data = data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.busbookingsummary_adapter,
					null);
			viewHolder = new ViewHolder();

			viewHolder.textView_Name = (TextView) convertView
					.findViewById(R.id.textView_Name);

			viewHolder.textView_Age = (TextView) convertView
					.findViewById(R.id.textView_Age);

			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		HashMap<String, String> map = new HashMap<String, String>();
		map = data.get(position);

		if (map.get(Constants.BUS_TRAVELLER_GENDER).equalsIgnoreCase("male"))
			viewHolder.textView_Name.setText("Mr. "
					+ map.get(Constants.BUS_TRAVELLER_NAME));
		else
			viewHolder.textView_Name.setText("Ms. "
					+ map.get(Constants.BUS_TRAVELLER_NAME));
		viewHolder.textView_Age.setText(map.get(Constants.BUS_TRAVELLER_AGE)
				+ " years");

		return convertView;
	}

	private class ViewHolder {
		TextView textView_Name, textView_Age;
	}
}
