package com.mindsarray.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.HashMap;

import com.mindsarray.pay1.R;
import com.mindsarray.pay1.constant.Constants;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class BusBookingBoardingPointAdapter extends BaseAdapter {

	Context context;
	ArrayList<HashMap<String, String>> data;

	public BusBookingBoardingPointAdapter(Context context,
			ArrayList<HashMap<String, String>> data) {
		this.context = context;
		this.data = data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(
					R.layout.busbookingboardingpoint_adapter, null);
			viewHolder = new ViewHolder();
			viewHolder.textView_BoardingPointName = (TextView) convertView
					.findViewById(R.id.textView_BoardingPointName);
			viewHolder.textView_BoardingPointTime = (TextView) convertView
					.findViewById(R.id.textView_BoardingPointTime);
			viewHolder.textView_BoardingPointID = (TextView) convertView
					.findViewById(R.id.textView_BoardingPointID);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		HashMap<String, String> map = new HashMap<String, String>();
		map = data.get(position);		

		viewHolder.textView_BoardingPointName.setText(map
				.get(Constants.BUS_BOARDING_POINT_NAME).toString().trim());
		viewHolder.textView_BoardingPointTime.setText(map
				.get(Constants.BUS_BOARDING_POINT_TIME).toString().trim());
		viewHolder.textView_BoardingPointID.setText(map
				.get(Constants.BUS_BOARDING_POINT_ID).toString().trim());
		
		return convertView;
	}

	private class ViewHolder {
		TextView textView_BoardingPointName, textView_BoardingPointTime,
				textView_BoardingPointID;
	}
}
