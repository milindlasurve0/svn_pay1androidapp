package com.mindsarray.pay1.adapterhandler;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.mindsarray.pay1.MobileBillMainFragment;
import com.mindsarray.pay1.ReversalFragement;
import com.mindsarray.pay1.StatusFragment;

public class MobileBillTabFragmentAdapter extends FragmentStatePagerAdapter {
	protected static final String[] CONTENT = new String[] { "MOBILE BILL",
			"TRANSACTION HISTORY", "COMPLAINT STATUS"
	 };

	private int mCount = CONTENT.length;

	public MobileBillTabFragmentAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		// return TestFragment.newInstance(CONTENT[position % CONTENT.length]);
		switch (position) {

		case 0:
			return MobileBillMainFragment
					.newInstance("FirstFragment, Instance 1");
		case 1:
			return ReversalFragement.newInstance("ThirdFragment, Default");
		case 2:
			return StatusFragment.newInstance("SecondFragment, Instance 1");
		default:
			return null;
		}
	}

	@Override
	public int getCount() {
		return mCount;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return MobileBillTabFragmentAdapter.CONTENT[position % CONTENT.length];
	}

	public void setCount(int count) {
		if (count > 0 && count <= 10) {
			mCount = count;
			notifyDataSetChanged();
		}
	}
}