package com.mindsarray.pay1.adapterhandler;

import java.util.ArrayList;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.mindsarray.pay1.R;

public class BusBookingTravellerInformationAdapter extends BaseAdapter {

	Context context;
	int seats;
	static ArrayList<String> names;
	String str = "";

	public BusBookingTravellerInformationAdapter(Context context, int seats) {
		this.context = context;
		this.seats = seats;
		names = new ArrayList<String>();
	}

	public static ArrayList<String> getName() {
		// for (int i = 0; i < 3; i++) {
		//
		// }
		return names;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return seats;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(
					R.layout.busbookingtravellerinformation_adapter, null);
			viewHolder = new ViewHolder();

			viewHolder.textView_Seat = (TextView) convertView
					.findViewById(R.id.textView_Seat);

			viewHolder.editText_Name = (EditText) convertView
					.findViewById(R.id.editText_Name);
			viewHolder.editText_Age = (EditText) convertView
					.findViewById(R.id.editText_Age);
			viewHolder.radioGroup_Gender = (RadioGroup) convertView
					.findViewById(R.id.radioGroup_Gender);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		viewHolder.textView_Seat.setText("Traveller " + (position + 1));
		viewHolder.editText_Name.setTag(str);
		names.add(viewHolder.editText_Name.getTag().toString() + "");

		viewHolder.editText_Name.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				// names.add(viewHolder.editText_Name.getText().toString().trim());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				// Log.w("", s.toString());
				str = s.toString();
			}
		});

		return convertView;
	}

	private class ViewHolder {
		TextView textView_Seat;
		EditText editText_Name, editText_Age;
		RadioGroup radioGroup_Gender;
	}
}
