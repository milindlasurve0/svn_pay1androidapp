package com.mindsarray.pay1.adapterhandler;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mindsarray.pay1.DataCardMainFragment;
import com.mindsarray.pay1.TestFragment;

public class DataCardTabFragmentAdapter extends FragmentPagerAdapter {
	protected static final String[] CONTENT = new String[] { "Data Card",
			"Status", "Request" };

	private int mCount = CONTENT.length;

	public DataCardTabFragmentAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		// return TestFragment.newInstance(CONTENT[position % CONTENT.length]);
		switch (position) {

		case 0:
			return DataCardMainFragment
					.newInstance("FirstFragment, Instance 1");
		case 1:
			return TestFragment.newInstance("SecondFragment, Instance 1");
		case 2:
			return TestFragment.newInstance("ThirdFragment, Default");
		default:
			return null;
		}
	}

	@Override
	public int getCount() {
		return mCount;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return DataCardTabFragmentAdapter.CONTENT[position % CONTENT.length];
	}

	public void setCount(int count) {
		if (count > 0 && count <= 10) {
			mCount = count;
			notifyDataSetChanged();
		}
	}
}