package com.mindsarray.pay1.adapterhandler;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mindsarray.pay1.R;
import com.mindsarray.pay1.constant.Constants;

public class BusBookingSearchAdapter extends BaseAdapter {

	Context context;
	ArrayList<HashMap<String, String>> data;

	public BusBookingSearchAdapter(Context context,
			ArrayList<HashMap<String, String>> data) {
		this.context = context;
		this.data = data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.busbookingsearch_adapter,
					null);
			viewHolder = new ViewHolder();

			viewHolder.textView_BusName = (TextView) convertView
					.findViewById(R.id.textView_BusName);
			viewHolder.textView_Time = (TextView) convertView
					.findViewById(R.id.textView_Time);
			viewHolder.textView_BusType = (TextView) convertView
					.findViewById(R.id.textView_BusType);
			viewHolder.textView_Seats = (TextView) convertView
					.findViewById(R.id.textView_Seats);
			viewHolder.textView_Duration = (TextView) convertView
					.findViewById(R.id.textView_Duration);
			viewHolder.textView_Fare = (TextView) convertView
					.findViewById(R.id.textView_Fare);

			viewHolder.textView_AC = (TextView) convertView
					.findViewById(R.id.textView_AC);
			viewHolder.textView_NonAC = (TextView) convertView
					.findViewById(R.id.textView_NonAC);
			viewHolder.textView_Sleeper = (TextView) convertView
					.findViewById(R.id.textView_Sleeper);
			viewHolder.textView_NonSleeper = (TextView) convertView
					.findViewById(R.id.textView_NonSleeper);
			viewHolder.textView_ArrivalTime = (TextView) convertView
					.findViewById(R.id.textView_ArrivalTime);
			viewHolder.textView_DepartureTime = (TextView) convertView
					.findViewById(R.id.textView_DepartureTime);
			viewHolder.textView_BusID = (TextView) convertView
					.findViewById(R.id.textView_BusID);
			viewHolder.textView_CancellationPolicy = (TextView) convertView
					.findViewById(R.id.textView_CancellationPolicy);

			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		HashMap<String, String> map = new HashMap<String, String>();
		map = data.get(position);

		viewHolder.textView_BusID.setText(map.get(Constants.BUS_ID).toString()
				.trim());
		viewHolder.textView_BusName.setText(map.get(Constants.BUS_NAME)
				.toString().trim());
		viewHolder.textView_Time.setText(map.get(Constants.BUS_TIME).toString()
				.trim());
		viewHolder.textView_BusType.setText(map.get(Constants.BUS_TYPE)
				.toString().trim());
		viewHolder.textView_Seats.setText(map.get(Constants.BUS_SEATS)
				.toString().trim());
		viewHolder.textView_Duration.setText(map.get(Constants.BUS_DURATION)
				.toString().trim());
		viewHolder.textView_Fare.setText(map.get(Constants.BUS_FARE).toString()
				.trim());
		viewHolder.textView_AC.setText(map.get(Constants.BUS_AC).toString()
				.trim());
		viewHolder.textView_NonAC.setText(map.get(Constants.BUS_NONAC)
				.toString().trim());
		viewHolder.textView_Sleeper.setText(map.get(Constants.BUS_SLEEPER)
				.toString().trim());
		viewHolder.textView_NonSleeper.setText(map
				.get(Constants.BUS_NONSLEEPER).toString().trim());
		viewHolder.textView_ArrivalTime.setText(map
				.get(Constants.BUS_ARRIVAL_TIME).toString().trim());
		viewHolder.textView_DepartureTime.setText(map
				.get(Constants.BUS_DEPARTURE_TIME).toString().trim());
		viewHolder.textView_CancellationPolicy
				.setText(Constants.BUS_CANCELLATION_POLICY);

		return convertView;
	}

	private class ViewHolder {
		TextView textView_BusName, textView_Time, textView_BusType,
				textView_Seats, textView_Duration, textView_Fare, textView_AC,
				textView_NonAC, textView_Sleeper, textView_NonSleeper,
				textView_ArrivalTime, textView_DepartureTime, textView_BusID,
				textView_CancellationPolicy;
	}
}
