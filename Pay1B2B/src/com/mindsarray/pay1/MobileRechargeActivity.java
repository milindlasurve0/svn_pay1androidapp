package com.mindsarray.pay1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.MostVisitedOperatorDataSource;
import com.mindsarray.pay1.databasehandler.NumbberWithCircleDataSource;
import com.mindsarray.pay1.databasehandler.NumberWithCircle;
import com.mindsarray.pay1.databasehandler.Plan;
import com.mindsarray.pay1.databasehandler.PlanDataSource;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.EditTextValidator;
import com.mindsarray.pay1.utilities.EnglishNumberToWords;
import com.mindsarray.pay1.utilities.Utility;

public class MobileRechargeActivity extends Activity {
	RadioGroup radioRchTypeGroup;
	RadioButton radioTopUp, radioSTV;
	ImageView mImageOprLogo;
	TextView mTextOperatorName, textView_Details, mRechargeAmountInWords,
			mLastRecharge;
	Button mBtnRchNow, btnPlans;
	MostVisitedOperatorDataSource visitedOperatorDataSource;
	EditText editMobileNumber, editRechargeAmount, editConfirmAmount;
	Bundle bundle;
	int mOperatorId;
	String circle_code = null;
	String mobileNumber, amount, planJson, amountInWords, confAmount;
	String rechargeFlag = "0";
	Context mContext;
	String uuid;
	boolean bNetOrSms;
	private SharedPreferences mSettingsData;// Shared preferences for setting
											// data
	long timestamp;
	public static GoogleAnalytics analytics;
	public static Tracker tracker;
	// NotificationDataSource notificationDataSource;
	PlanDataSource planDataSource;
	ProgressBar progressBar1;
	NumbberWithCircleDataSource circleDataSource;
	private static final String TAG = "Mobile recharge";
	int flag = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.mobilerecharge_activity);
		flag = 1;
		findViewById();
		// tracker = Constants.setAnalyticsTracker(this, tracker);
		// //easyTracker.set(Fields.SCREEN_NAME, TAG);
		tracker = Constants.setAnalyticsTracker(this, tracker);
		visitedOperatorDataSource = new MostVisitedOperatorDataSource(
				MobileRechargeActivity.this);
		mRechargeAmountInWords = (TextView) findViewById(R.id.rechargeAmountInWords);
		mLastRecharge = (TextView) findViewById(R.id.lastRecharge);
		try {
			TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
			uuid = tManager.getDeviceId();
			if (uuid == null) {
				uuid = Secure.getString(
						MobileRechargeActivity.this.getContentResolver(),
						Secure.ANDROID_ID);
			}
			if (Utility.getUUID(MobileRechargeActivity.this,
					Constants.SHAREDPREFERENCE_UUID) == null)
				Utility.setUUID(MobileRechargeActivity.this,
						Constants.SHAREDPREFERENCE_UUID, uuid);
		} catch (Exception exception) {

		}

		LinearLayout adLayout = (LinearLayout) findViewById(R.id.adLayout);
		String notificationText = Utility.getSharedPreferences(
				MobileRechargeActivity.this, "notification");
		// String notificationText="";
		if (notificationText.equals("") || notificationText.length() == 0) {
			adLayout.setVisibility(View.GONE);
		} else {
			adLayout.setVisibility(View.VISIBLE);
			String html = "<html><body height:50px  bgcolor='#26a9e0'><marquee><div style='color:#1b1b1b; padding: 10px'>"
					+ notificationText + "</div></marquee></body></html>";

			WebView myWebView = (WebView) this.findViewById(R.id.myWebView);
			myWebView.getSettings().setJavaScriptEnabled(true);

			myWebView.loadData(html, "text/html", null);
		}

		/*
		 * notificationDataSource = new NotificationDataSource(
		 * MobileRechargeActivity.this);
		 */
		planDataSource = new PlanDataSource(MobileRechargeActivity.this);
		circleDataSource = new NumbberWithCircleDataSource(
				MobileRechargeActivity.this);
		try {
			visitedOperatorDataSource.open();

		} catch (Exception e) {

		} finally {
			visitedOperatorDataSource.close();
		}

		btnPlans.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MobileRechargeActivity.this,
						PlanFragmentActivity.class);
				if (editMobileNumber.getText().toString().trim().length() != 0) {
					intent.putExtra(Constants.PLAN_JSON, planJson);
					intent.putExtra("CIRCLE_CODE", circle_code);
					intent.putExtra("DATA_CARD_PLAN", 0);
				} else {
					intent.putExtra(Constants.PLAN_JSON, planJson);
					intent.putExtra("CIRCLE_CODE", "");
					intent.putExtra("DATA_CARD_PLAN", 0);
				}
				startActivityForResult(intent, Constants.PLAN_REQUEST);
				// Constants.overridePendingTransitionActivity(
				// RechargeActivity.this, 4);
			}
		});
		mContext = MobileRechargeActivity.this;
		mSettingsData = getSharedPreferences(SettingsSmsActivity.SETTINGS_NAME,
				0);
		bNetOrSms = mSettingsData.getBoolean(SettingsSmsActivity.KEY_NETWORK,
				true);
		bundle = getIntent().getExtras();
		mOperatorId = bundle.getInt(Constants.OPERATOR_ID);
		planJson = bundle.getString(Constants.PLAN_JSON);
		if (mOperatorId == Constants.OPERATOR_AIRCEL
				|| mOperatorId == Constants.OPERATOR_AIRTEL
				|| mOperatorId == Constants.OPERATOR_IDEA
				|| mOperatorId == Constants.OPERATOR_LOOP
				|| mOperatorId == Constants.OPERATOR_MTS
				|| mOperatorId == Constants.OPERATOR_RELIANCE_CDMA
				|| mOperatorId == Constants.OPERATOR_RELIANCE_GSM
				|| mOperatorId == Constants.OPERATOR_VODAFONE
				|| mOperatorId == Constants.OPERATOR_TATA_INDICOM) {
			radioSTV.setVisibility(View.GONE);
			radioTopUp.setVisibility(View.GONE);
		} else {

		}

		editMobileNumber.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				try {
					EditTextValidator.hasText(MobileRechargeActivity.this,
							editMobileNumber,
							Constants.ERROR_MOBILE_BLANK_FIELD);
					if (editMobileNumber.length() == 10) {
						editRechargeAmount.requestFocus();
						// new
						// GetLastRechargeTask().execute(editMobileNumber.getText().toString());
					}
					if (editMobileNumber.length() == 4) {
						/*
						 * if (Utility.getAvailableMemoryFlag(mContext,
						 * "memoryFlag")) {
						 */
						try {

							circleDataSource.open();
							// new GetPlansCircletask().execute();
							NumberWithCircle numberWithCircle = circleDataSource
									.getNumberDetails(editMobileNumber
											.getText().toString().trim());
							circle_code = numberWithCircle.getAreaCode();
						} catch (Exception se) {
							Log.d("aaa", "Error    " + se.getMessage());
							circleDataSource.close();

							// new GetPlansCircletask().execute();
						} finally {
							circleDataSource.close();
						}
						/*
						 * } else { new GetPlansCircletask().execute(); }
						 */
					}
				} catch (Exception exception) {
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		editRechargeAmount.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				try {
					if (editRechargeAmount.getText().toString().trim()
							.equals("")) {
						mRechargeAmountInWords.setText("");
					} else
						mRechargeAmountInWords.setText(EnglishNumberToWords
								.convert(Long.parseLong(editRechargeAmount
										.getText().toString().trim())));
					EditTextValidator.hasText(MobileRechargeActivity.this,
							editRechargeAmount,
							Constants.ERROR_AMOUNT_BLANK_FIELD);
					textView_Details.setVisibility(View.GONE);
					if (editMobileNumber.length() == 10
							&& editRechargeAmount.getText().toString().trim()
									.length() >= 2) {
						if (circle_code != null) {
							progressBar1.setVisibility(View.GONE);
							getPlan(String.valueOf(mOperatorId),
									circle_code,
									Integer.parseInt(editRechargeAmount
											.getText().toString().trim()));
						}
					}

					if (Integer.parseInt(editRechargeAmount.getText()
							.toString().trim()) >= Integer.parseInt(Utility
							.getMaximumAmount(mContext))) {
						editConfirmAmount.setText("");
						editConfirmAmount.setVisibility(View.VISIBLE);
					} else {
						editConfirmAmount.setText("");
						editConfirmAmount.setVisibility(View.GONE);
					}

				} catch (Exception exception) {
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		});

		mImageOprLogo.setImageResource(getIntent().getIntExtra(
				Constants.OPERATOR_LOGO, 0));
		mTextOperatorName.setText(bundle.getString(Constants.OPERATOR_NAME));

		mBtnRchNow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				bNetOrSms = mSettingsData.getBoolean(
						SettingsSmsActivity.KEY_NETWORK, true);
				timestamp = System.currentTimeMillis();
				mobileNumber = editMobileNumber.getText().toString();
				amount = editRechargeAmount.getText().toString();
				if (editConfirmAmount.getVisibility() == View.VISIBLE) {
					confAmount = editConfirmAmount.getText().toString();
					if (amount.equalsIgnoreCase(confAmount)) {

					} else {

						int ecolor = R.color.black;
						ForegroundColorSpan fgcspan = new ForegroundColorSpan(
								ecolor);
						SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
								"Please enter same amount");
						ssbuilder.setSpan(fgcspan, 0,
								"Please enter same amount".length(), 0);
						editConfirmAmount.setError(ssbuilder);
						return;
					}
				}

				if (amount.equals("")) {
					amountInWords = "";
				} else
					amountInWords = EnglishNumberToWords.convert(Long
							.parseLong(amount));

				if (!checkValidation(MobileRechargeActivity.this)) {

				} else {

					if (bNetOrSms) {

						if (Utility.getLoginFlag(MobileRechargeActivity.this,
								Constants.SHAREDPREFERENCE_IS_LOGIN)) {

							int selectedId = radioRchTypeGroup
									.getCheckedRadioButtonId();
							radioSTV = (RadioButton) findViewById(selectedId);

							new AlertDialog.Builder(MobileRechargeActivity.this)
									.setTitle("Recharge")
									.setMessage(
											"Operator: "
													+ mTextOperatorName
															.getText()
													+ "\nMobile Number: "
													+ mobileNumber
													+ "\nAmount: "
													+ amount
													+ " ( "
													+ amountInWords
													+ ") "
													+ "\nAre you sure you want to Recharge?")
									.setPositiveButton(
											"Yes",
											new DialogInterface.OnClickListener() {
												public void onClick(
														DialogInterface dialog,
														int which) {
													dialog.dismiss();

													/*
													 * new RechargeTask()
													 * .execute("0");
													 */

													new RechargeTask().execute(
															"0",
															"0",
															String.valueOf(mOperatorId));

													// ShowFullScreenDialogForRepeat("asdf");

												}
											})
									.setNegativeButton(
											"No",
											new DialogInterface.OnClickListener() {
												public void onClick(
														DialogInterface dialog,
														int which) {
													dialog.dismiss();
												}
											}).show();

						} else {
							Intent intent = new Intent(
									MobileRechargeActivity.this,
									LoginActivity.class);
							startActivity(intent);
						}
					} else {
						int selectedId = radioRchTypeGroup
								.getCheckedRadioButtonId();
						radioSTV = (RadioButton) findViewById(selectedId);
						new AlertDialog.Builder(MobileRechargeActivity.this)
								.setTitle("Recharge (SMS)")
								.setMessage(
										"Operator: "
												+ mTextOperatorName.getText()
												+ "\nMobile Number: "
												+ mobileNumber
												+ "\nAmount: "
												+ amount
												+ " ( "
												+ amountInWords
												+ ") "
												+ "\nAre you sure you want to Recharge?")
								.setPositiveButton("Yes",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int which) {
												dialog.dismiss();
												Constants
														.sendSMSMessage(
																mContext,
																Constants.RECHARGE_MOBILE,
																mOperatorId,
																mobileNumber,
																mobileNumber,
																amount,
																radioSTV.getTag()
																		.toString(),
																bundle.getString(Constants.OPERATOR_NAME));
												// finish();
											}
										})
								.setNegativeButton("No",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int which) {
												dialog.dismiss();
											}
										}).show();

					}

				}

			}
		});

	}

	private boolean checkValidation(Context context) {
		boolean ret = true;

		if (!EditTextValidator.hasText(context, editMobileNumber,
				Constants.ERROR_MOBILE_BLANK_FIELD)) {
			ret = false;
			editMobileNumber.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidMobileNumber(context,
				editMobileNumber, Constants.ERROR_MOBILE_LENGTH_FIELD)) {
			ret = false;
			editMobileNumber.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editRechargeAmount,
				Constants.ERROR_AMOUNT_BLANK_FIELD)) {
			ret = false;
			editRechargeAmount.requestFocus();
			return ret;
		} else
			return ret;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == Constants.PLAN_REQUEST) {
				editRechargeAmount
						.setText(data.getExtras().getString("result"));
			}
		}
	}

	private void findViewById() {
		radioRchTypeGroup = (RadioGroup) findViewById(R.id.radioRechargeType);
		radioSTV = (RadioButton) findViewById(R.id.radioSTV);
		radioTopUp = (RadioButton) findViewById(R.id.radioTopup);
		mImageOprLogo = (ImageView) findViewById(R.id.operator_logo);
		mTextOperatorName = (TextView) findViewById(R.id.operator_name);
		mBtnRchNow = (Button) findViewById(R.id.mBtnRchNow);
		editMobileNumber = (EditText) findViewById(R.id.editMobileNumber);
		editRechargeAmount = (EditText) findViewById(R.id.editRechargeAmount);
		editConfirmAmount = (EditText) findViewById(R.id.editConfirmAmount);
		textView_Details = (TextView) findViewById(R.id.textView_Details);
		btnPlans = (Button) findViewById(R.id.btnPlans);
		progressBar1 = (ProgressBar) findViewById(R.id.progressBar1);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants.showNavigationBar(MobileRechargeActivity.this);
		flag = 1;
		super.onResume();
	}

	public class RechargeTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair.add(new BasicNameValuePair("method", "mobRecharge"));
			listValuePair.add(new BasicNameValuePair("mobileNumber",
					mobileNumber));
			listValuePair.add(new BasicNameValuePair("operator", params[2]));
			listValuePair.add(new BasicNameValuePair("subId", mobileNumber));
			listValuePair.add(new BasicNameValuePair("amount", amount));
			listValuePair.add(new BasicNameValuePair("type", "flexi"));
			listValuePair.add(new BasicNameValuePair("circle", ""));
			listValuePair.add(new BasicNameValuePair("special", radioSTV
					.getTag().toString()));
			listValuePair.add(new BasicNameValuePair("recharge_prompt_flag",
					"1"));
			listValuePair.add(new BasicNameValuePair("no_operator_check_flag",
					params[1]));

			listValuePair.add(new BasicNameValuePair(
					"no_prompt_within_one_hour_flag", params[0]));
			listValuePair.add(new BasicNameValuePair("timestamp", String
					.valueOf(timestamp)));
			listValuePair.add(new BasicNameValuePair("profile_id", Utility
					.getProfileID(MobileRechargeActivity.this,
							Constants.SHAREDPREFERENCE_PROFILE_ID)));
			listValuePair.add(new BasicNameValuePair("hash_code", Constants
					.encryptPassword(Utility.getUUID(
							MobileRechargeActivity.this,
							Constants.SHAREDPREFERENCE_UUID)
							+ mobileNumber + amount + timestamp)));
			listValuePair.add(new BasicNameValuePair("device_type", "android"));
			// String
			// url="http://panel.activestores.in/apis/receiveWeb/mindsarray/mindsarray/json?method=mobRecharge&Mobile=9819032643&operator=4&subId=9819032643&amount=10&type=flexi&circle=&special=0&timestamp=1389870880878&hash_code=92da34555032fda9ff31d7fbdfe2c6482bc094ca&device_type=android";

			String response = RequestClass.getInstance().readPay1Request(
					MobileRechargeActivity.this, Constants.API_URL,
					listValuePair);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}

			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = array.getJSONObject(0);
					String status = jsonObject.getString("status");

					if (status.equalsIgnoreCase("success")) {

						Constants.showOneButtonSuccessDialog(
								MobileRechargeActivity.this,
								"Recharge request sent successfully.\nTransaction id : "
										+ jsonObject.getString("description"),
								Constants.RECHARGE_SETTINGS);
						String balance = jsonObject.getString("balance");
						Utility.setBalance(MobileRechargeActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE, balance);

					} else {

						String code1 = jsonObject.getString("code");
						int code = Integer.parseInt(code1);

						if (code == 42) {
							String lastRecharge = jsonObject
									.getString("lastRecharge");

							ShowFullScreenDialogForRepeat(lastRecharge);

						} else if (code == 43) {
							String product_id = jsonObject
									.getString("product_id");
							String non_prepaid_operator_flag = jsonObject
									.getString("non_prepaid_operator_flag");

							String opeName2 = Constants
									.rechargeTypeOperatorName(product_id);

							String opeId2 = product_id;
							ShowFullScreenDialogForMNP(
									bundle.getString(Constants.OPERATOR_NAME),
									opeName2, String.valueOf(mOperatorId),
									opeId2, non_prepaid_operator_flag);

						} else {
							Constants.showOneButtonFailureDialog(
									MobileRechargeActivity.this,
									Constants.checkCode(replaced), TAG,
									Constants.OTHER_ERROR);
						}

					}

				} else {
					Constants.showOneButtonDialog(MobileRechargeActivity.this,
							"NO INTERNET CONNECTION",
							"Oops!\nLooks like we lost you.", "RETRY", 0);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(
						MobileRechargeActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(
						MobileRechargeActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(MobileRechargeActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							RechargeTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			RechargeTask.this.cancel(true);
			dialog.cancel();
		}

	}

	public class GetPlansCircletask extends AsyncTask<String, String, String> {
		ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair.add(new BasicNameValuePair("method",
					"getMobileDetails"));
			listValuePair.add(new BasicNameValuePair("mobile", editMobileNumber
					.getText().toString().trim()));

			String response = RequestClass.getInstance().readPay1Request(
					MobileRechargeActivity.this, Constants.API_URL,
					listValuePair);
			String replaced = response.replace("(", "").replace(")", "")
					.replace(";", "");

			return replaced;

		}

		@Override
		protected void onPostExecute(String result) {
			// if (dialog.isShowing()) {
			// dialog.dismiss();
			// }
			super.onPostExecute(result);
			try {

				JSONArray array = new JSONArray(result);
				JSONObject jsonObject = array.getJSONObject(0);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("SUCCESS")) {

					JSONObject jsonObjectdetails = jsonObject
							.getJSONObject("details");
					circle_code = jsonObjectdetails.getString("area")
							.toString().trim();
				}
				/*
				 * circle_code = result.getAreaCode(); circleDataSource.close();
				 */
			} catch (Exception e) {
			}
		}

		@Override
		protected void onPreExecute() {
			// dialog = new ProgressDialog(PlansActivity.this);
			// this.dialog.setMessage("Please wait.....");
			// this.dialog.show();
			super.onPreExecute();

		}

	}

	protected void getPlan(String operator, String circle, int amount) {

		try {
			planDataSource.open();

			List<Plan> plans = planDataSource.getPlanDetails(operator, circle,
					amount, 0);
			List<Plan> data = planDataSource.getAllPlan(operator, circle);
			if (plans.size() == 0 && data.size() == 0) {
				// if (Utility.getAvailableMemoryFlag(mContext, "memoryFlag")) {
				//
				// } else {
				// new GetPlanstask().execute();
				// }
			} else {
				if (plans.size() != 0) {
					// String LastUpdateTime = plans.get(0).getPlanUptateTime();
					// int days = (int) ((System.currentTimeMillis() - Long
					// .parseLong(LastUpdateTime)) / (1000 * 60 * 60 * 24));
					//
					// if (days >= 2) {
					// //planDataSource.deletePlan();
					// new GetPlanstask().execute();
					// } else {
					textView_Details.setText(plans.get(0).getPlanDescription());
					textView_Details.setVisibility(View.VISIBLE);
					progressBar1.setVisibility(View.GONE);
					textView_Details.setTextColor(getResources().getColor(
							R.color.Black));
					// }
				} else {
					// textView_Details
					// .setText("No such plan exist in our system. Kindly check with operator.");
					// textView_Details.setVisibility(View.VISIBLE);
					// progressBar1.setVisibility(View.GONE);
					// textView_Details.setTextColor(getResources().getColor(
					// R.color.Red));
				}
			}

		} catch (SQLException exception) {

			// TODO: handle exception
			// // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Er ", e.getMessage());
		} finally {
			planDataSource.close();
			// progressBar1.setVisibility(View.GONE);
		}

	}

	public class GetPlanstask extends AsyncTask<String, String, String> {
		// ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair
					.add(new BasicNameValuePair("method", "getPlanDetails"));
			listValuePair.add(new BasicNameValuePair("operator", getIntent()
					.getExtras().getString(Constants.PLAN_JSON)));
			listValuePair.add(new BasicNameValuePair("circle", circle_code));

			String response = RequestClass.getInstance().readPay1Request(
					MobileRechargeActivity.this, Constants.API_URL,
					listValuePair);
			String replaced = response.replace("(", "").replace(")", "")
					.replace(";", "");
			planDataSource.open();
			planDataSource.savePlansTodataBase(replaced);
			planDataSource.close();

			return replaced;

		}

		@Override
		protected void onPostExecute(String result) {
			// if (dialog.isShowing()) {
			// dialog.dismiss();
			// }

			super.onPostExecute(result);
			try {
				if (result != null && !result.startsWith("Error") && flag != 0) {
					flag--;
					getPlan(String.valueOf(mOperatorId),
							circle_code,
							Integer.parseInt(editRechargeAmount.getText()
									.toString().trim()));
				} else {
					textView_Details
							.setText("No such plan exist in our system. Kindly check with operator.");
					// textView_Details.setVisibility(View.VISIBLE);
					textView_Details.setTextColor(getResources().getColor(
							R.color.Red));
				}
			} catch (Exception e) {
			}

			progressBar1.setVisibility(View.GONE);
		}

		@Override
		protected void onPreExecute() {
			// dialog = new ProgressDialog(RechargeActivity.this);
			// this.dialog.setMessage("Please wait.....");
			// this.dialog.show();
			progressBar1.setVisibility(View.GONE);
			super.onPreExecute();

		}

	}

	private void savePlansTodataBase(String planJson) {
		try {
			planDataSource.open();
			// ContentValues contentValues = new ContentValues();
			JSONArray array = new JSONArray(planJson);
			JSONObject planJsonObject = array.getJSONObject(0);

			Iterator<String> operatorKey = planJsonObject.keys();

			if (operatorKey.hasNext()) {

				long time = System.currentTimeMillis();

				JSONObject jsonObject2 = planJsonObject
						.getJSONObject(operatorKey.next());

				String opr_name = jsonObject2.getString("opr_name");
				String prod_code_pay1 = jsonObject2.getString("prod_code_pay1");
				// contentValues.put("operator_id", prod_code_pay1);
				// contentValues.put("operator_name", opr_name);
				// contentValues.put("prod_code_pay1", prod_code_pay1);
				JSONObject circleJsonObject = jsonObject2
						.getJSONObject("circles");
				Iterator<String> circleKey = circleJsonObject.keys();
				if (circleKey.hasNext()) {
					JSONObject circleJsonObject2 = circleJsonObject
							.getJSONObject(circleKey.next().toString());
					String circleName = circleJsonObject2
							.getString("circle_name");

					// contentValues.put("circle_name", circleName);

					JSONObject plansJsonObject = circleJsonObject2
							.getJSONObject("plans");
					String circle_id = circleJsonObject2.getString("circle_id");
					Iterator<String> planKey = plansJsonObject.keys();
					Iterator<String> planKeyDB = plansJsonObject.keys();
					// contentValues.put("circle_id", circle_id);

					while (planKey.hasNext()) {
						String planType = planKey.next().toString();
						JSONArray planArray = plansJsonObject
								.getJSONArray(planType);
						// contentValues.put("planType", planType);

						for (int i = 0; i < planArray.length(); i++) {
							JSONObject jsonObject = planArray.getJSONObject(i);
							String plan_desc = jsonObject
									.getString("plan_desc");
							// contentValues.put("plan_desc", plan_desc);
							String plan_amt = jsonObject.getString("plan_amt");
							// contentValues.put("plan_amt", plan_amt);
							String plan_validity = jsonObject
									.getString("plan_validity");
							String show_flag = jsonObject
									.getString("show_flag");

							// contentValues.put("plan_validity",
							// plan_validity);
							// // Log.e("cORRECT", "cORRECT   " + i + "  "
							// + jsonObject.toString());
							// controller.insertPlans(contentValues);
							planDataSource.createPlan(prod_code_pay1, opr_name,
									circle_id, circleName, planType, plan_amt,
									plan_validity, plan_desc, "" + time,
									show_flag);
						}

					}

				}

			}
		} catch (JSONException exception) {

		} catch (SQLException exception) {
			// // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// // Log.e("Er ", e.getMessage());
		} finally {
			planDataSource.close();
		}
	}

	@Override
	public void onBackPressed() {
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}

	}

	public class GetLastRechargeTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			String response = Constants.fetchLastRecharge(mContext, params[0],
					getIntent().getExtras().getString(Constants.PLAN_JSON));
			Log.e("Last transaction:", "" + response);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			super.onPostExecute(result);
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = array.getJSONObject(0);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONObject jsonDescription = jsonObject
								.getJSONObject("description");
						String time_stamp = jsonDescription
								.getString("time_stamp");
						String amount = jsonDescription.getString("amount");

						mLastRecharge.setText("Last "
								+ bundle.getString(Constants.OPERATOR_NAME)
								+ " recharge was done on " + time_stamp
								+ " Amount Rs. " + amount);
						mLastRecharge.setVisibility(View.VISIBLE);
					} else {
						mLastRecharge.setVisibility(View.GONE);
					}

				}
			} catch (JSONException e) {
				// e.printStackTrace();

			} catch (Exception e) {

			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(mContext);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							GetLastRechargeTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			GetLastRechargeTask.this.cancel(true);
			dialog.cancel();
		}
	}
	
	public void ShowFullScreenDialogForRepeat(String time) {
		final Dialog fDialog = new Dialog(MobileRechargeActivity.this);
		fDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		fDialog.getWindow().setBackgroundDrawableResource(
				android.R.color.transparent);

		fDialog.setContentView(R.layout.dialog_full_screen_repeat);
		fDialog.setCancelable(false);
		Button buttonContinue = (Button) fDialog
				.findViewById(R.id.buttonContinue);
		Button buttonCancel = (Button) fDialog.findViewById(R.id.buttonCancel);

		TextView textViewMobNumber = (TextView) fDialog
				.findViewById(R.id.textViewMobNumber);
		TextView textViewAmount = (TextView) fDialog
				.findViewById(R.id.textViewAmount);

		TextView textViewMsg = (TextView) fDialog
				.findViewById(R.id.textViewMsg);

		try {
			JSONObject lastRecharge = new JSONObject(time);
			String recharge_time = lastRecharge.getString("recharge_time");
			String time_elapsed = lastRecharge.getString("time_elapsed");
			// textViewTimeElapsed.setText(time_elapsed + " MIN AGO");
			String msg = "Last successful recharge on "
					+ mobileNumber
					+ " number was "
					+ "<font color='red'>" + time_elapsed
					+ " mins ago</font>. Are you sure you want to repeat this recharge?";
			// textViewTime.setText(recharge_time);
			textViewMsg.setText(Html.fromHtml(msg));
		} catch (JSONException je) {

		} catch (Exception e) {
			// TODO: handle exception
		}

		/*
		 * ImageView imageViewOperator = (ImageView) mDialog
		 * .findViewById(R.id.imageViewOperator);
		 * 
		 * imageViewOperator.setImageResource(getIntent().getIntExtra(
		 * Constants.OPERATOR_LOGO, 0));
		 */
		// textViewOperator.setText(bundle.getString(Constants.OPERATOR_NAME));
		// textViewTime.setText(time);
		textViewMobNumber.setText(mobileNumber);
		textViewAmount.setText("Rs. " + amount);

		buttonContinue.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new RechargeTask().execute("1", "0",
						String.valueOf(mOperatorId));
			}
		});

		buttonCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		fDialog.show();
		fDialog.setOnKeyListener(new Dialog.OnKeyListener() {

			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK) {
                    fDialog.dismiss();
                }
                return true;
			}
        });


	}
	
	public void ShowFullScreenDialogForMNP(String opeName1, String opeName2,
			final String opeId1, final String opeId2,
			final String non_prepaid_operator_flag) {
		final Dialog mnpDialog = new Dialog(MobileRechargeActivity.this/*
																	 * ,
																	 * android.
																	 * R.style.
																	 * Theme_Light
																	 */);
		mnpDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mnpDialog.getWindow().setBackgroundDrawableResource(
				android.R.color.transparent);
		mnpDialog.setContentView(R.layout.dialog_full_screen_wrong_operator);
		mnpDialog.setCancelable(false);
		
		ImageView operatorImage1 = (ImageView) mnpDialog
				.findViewById(R.id.operatorImage1);

		ImageView operatorImage2 = (ImageView) mnpDialog
				.findViewById(R.id.operatorImage2);

		operatorImage2.setImageResource(Constants.getBitmapById(
				Integer.parseInt(opeId2), MobileRechargeActivity.this));
		/*
		 * Button buttonCancel = (Button)
		 * mDialog.findViewById(R.id.buttonCancel);
		 */
		TextView textViewOperatorName1 = (TextView) mnpDialog
				.findViewById(R.id.textViewOperatorName1);

		TextView textViewOperatorName2 = (TextView) mnpDialog
				.findViewById(R.id.textViewOperatorName2);
		textViewOperatorName2.setText(opeName2);
		TextView textViewMsg = (TextView) mnpDialog
				.findViewById(R.id.textViewMsg);
		textViewMsg.setText("This number is registered as " + opeName2
				+ " operator. Kindly select the operator again.");

		TextView textViewMobNumber = (TextView) mnpDialog
				.findViewById(R.id.textViewMobNumber);

		TextView textViewAmount = (TextView) mnpDialog
				.findViewById(R.id.textViewAmount);

		operatorImage1.setImageResource(getIntent().getIntExtra(
				Constants.OPERATOR_LOGO, 0));

		textViewOperatorName1
				.setText(bundle.getString(Constants.OPERATOR_NAME));

		textViewMobNumber.setText(mobileNumber);
		textViewAmount.setText("Rs. " + amount);
		
		LinearLayout operatorSelection1 = (LinearLayout) mnpDialog.findViewById(R.id.operatorSelection1);
		LinearLayout operatorSelection2 = (LinearLayout) mnpDialog.findViewById(R.id.operatorSelection2);
		
		operatorSelection1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mnpDialog.dismiss();
				new RechargeTask().execute("0", "1", opeId1);
			}
		});

		operatorSelection2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mnpDialog.dismiss();
				if (non_prepaid_operator_flag.equalsIgnoreCase("1")) {
					Intent intent = new Intent(MobileRechargeActivity.this,
							MobileBillMainActivity.class);
					startActivity(intent);
				} else {
					new RechargeTask().execute("0", "1", opeId2);
				}
			}
		});

		mnpDialog.show();
		
		mnpDialog.setOnKeyListener(new Dialog.OnKeyListener() {

			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					mnpDialog.dismiss();
                }
                return true;
			}
        });
	}
}
