package com.mindsarray.pay1.utilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.mindsarray.pay1.constant.Constants;

public class Utility {

	private static String PREFERENCE;

	// Preferences for Entertainment
	public static void setSharedPreference(Context context, String name,
			String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getSharedPreferences(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, "");
	}
	
	
	
	public static void setImageSharedPreference(Context context,
			String value,String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString("Op_Image_"+name, value);
		editor.commit();
	}

	public static String getImageSharedPreferences(Context context,String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString("Op_Image_"+name, "");
	}
	
	
	
	
	public static Boolean getSharedPreferencesBoolean(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getBoolean(name, false);
	}
	
	public static void setLanguageSharedPreference(Context context,
			String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString("LANG", value);
		editor.commit();
	}

	public static String getLanguageSharedPreferences(Context context) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString("LANG", "en");
	}

	
	public static void setCookieList(Context context, Set<String> value){
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putStringSet("CookieStore", value);
		editor.commit();
	}
	public static Set<String> getCookieList(Context context) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getStringSet("CookieStore", null);
	}
	
	
	public static void setCookieObject(Context context, String value){
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString("CookieStoreObject", value);
		editor.commit();
	}
	public static String getCookieObject(Context context) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString("CookieStoreObject", null);
	}
	
	public static void setHeaderCookieString(Context context, String value){
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString("HeaderCookieString", value);
		editor.commit();
	}
	public static String getHeaderCookieString(Context context) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString("HeaderCookieString", "");
	}
	
	public static void setLastSendingTime(Context context, long value){
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putLong("SendingTime", value);
		editor.commit();
	}
	public static Long getLastSendingTime(Context context) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getLong("SendingTime", 1);
	}
	
	public static void setLastRecieveTime(Context context, long value){
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putLong("RecievingTime", value);
		editor.commit();
	}
	public static Long getLastRecieveTime(Context context) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getLong("RecievingTime", 1);
	}
	
	
	
	// for notification count
	public static void setNotificationCount(Context context, String name,
			int value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putInt(name, value);
		editor.commit();
	}

	public static int getNotificationCount(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getInt(name, 0);
	}

	// for chat count
	public static void setChatCount(Context context, String name, int value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putInt(name, value);
		editor.commit();
	}

	public static int getChatCount(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getInt(name, 0);
	}

	public static void setLoginFlag(Context context, String name, boolean value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putBoolean(name, value);
		editor.commit();
	}

	public static boolean getLoginFlag(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getBoolean(name, false);
	}

	public static void setCookieVersion(Context context, String name, int value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(name, value);
		editor.commit();
	}

	public static int getCookieVersion(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);

		return settings.getInt(name, 0);
	}

	public static void setCookieName(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getCookieName(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);

		return settings.getString(name, "");
	}
	
	
	
	public static void setMaximumAmount(Context context, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("MINIMUM_AMOUNT", value);
		editor.commit();
	}

	public static String getMaximumAmount(Context context) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);

		return settings.getString("MINIMUM_AMOUNT", "500");
	}
	
	
	
	
	public static void setIsAppUsed(Context context, Boolean value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean("IsAppUsed", value);
		editor.commit();
	}

	public static Boolean getIsAppUsed(Context context) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);

		return settings.getBoolean("IsAppUsed", false);
	}
	
	
	

	public static void setCookieValue(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getCookieValue(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);

		return settings.getString(name, "");
	}

	public static void setCookieDomain(Context context, String name,
			String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getCookieDomain(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);

		return settings.getString(name, null);
	}

	public static void setCookiePath(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getCookiePath(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);

		return settings.getString(name, null);
	}

	public static void setCookieExpiry(Context context, String name,
			String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getCookieExpiry(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);

		return settings.getString(name, null);
	}

	public static void setChatFlag(Context context, String name, boolean value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putBoolean(name, value);
		editor.commit();
	}

	public static boolean getChatFlag(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getBoolean(name, false);
	}

	public static void setChatTime(Context context, String name, long value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putLong(name, value);
		editor.commit();
	}

	public static long getChatTime(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getLong(name, 0);
	}

	public static void setChatServiceTime(Context context, String name,
			String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getChatServiceTime(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setChatServiceID(Context context, String name,
			String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getChatServiceID(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setCityLastUpdateTime(Context context, String name,
			long value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putLong(name, value);
		editor.commit();
	}

	public static long getCityLastUpdateTime(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getLong(name, 0);
	}

	public static void setProfileID(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getProfileID(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setBalance(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getBalance(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}
	
	
	public static void setRentalFlag(Context context, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString("rental_flag", value);
		editor.commit();
	}

	public static String getRentalFlag(Context context) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString("rental_flag", "0");
	}
	
	
	

	public static void setUserName(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getUserName(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setMobileNumber(Context context, String name,
			String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getMobileNumber(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}
	
	public static void setUserMobileNumber(Context context,
			String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(Constants.SHAREDPREFERENCE_MOBILE_NUMBER_NEW, value);
		editor.commit();
	}

	public static String getUserMobileNumber(Context context) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(Constants.SHAREDPREFERENCE_MOBILE_NUMBER_NEW, null);
	}
	
	public static void setIsAbleForPG(Context context,
			Boolean value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putBoolean(Constants.SHAREDPREFERENCE_PG_ABLE, value);
		editor.commit();
	}

	public static boolean getIsAbleForPG(Context context) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getBoolean(Constants.SHAREDPREFERENCE_PG_ABLE, false);//settings.getString(Constants.SHAREDPREFERENCE_MOBILE_NUMBER, null);
	}
	
	
	public static void setServiceChargeForPG(Context context, int value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putInt(Constants.PG_SERVICE_CHARGE, value);
		editor.commit();
	}

	public static int getServiceChargeForPG(Context context) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getInt(Constants.PG_SERVICE_CHARGE, 0);
	}

	public static void setLatitude(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getLatitude(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setLongitude(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getLongitude(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setCurrentLatitude(Context context, String name,
			String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getCurrentLatitude(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setCurrentLongitude(Context context, String name,
			String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getCurrentLongitude(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}
	
	
	

	public static void setGCMID(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getGCMID(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setUUID(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getUUID(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setMiliSecForNewLable(Context context, String name,
			long value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putLong(name, value);
		editor.commit();
	}
	public static long getMiliSecForNewLable(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getLong(name, 0);
	}
	public static void setAvailableMemoryFlag(Context context, String name,
			boolean value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putBoolean(name, value);
		editor.commit();
	}

	public static boolean getAvailableMemoryFlag(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getBoolean(name, false);
	}

	public static void setOSVersion(Context context, String name, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getOSVersion(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setOSManufacturer(Context context, String name,
			String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static String getOSManufacturer(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static void setPlanUpdateFlag(Context context,
			Boolean value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putBoolean("PLAN_UPDATE", value);
		editor.commit();
	}
	
	public static boolean getPlanUpdateFlag(Context mContext) {
		SharedPreferences settings = mContext
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getBoolean("PLAN_UPDATE", false);
	}
	
	
	public static void setCircleUpdateFlag(Context context,
			Boolean value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putBoolean("CIRCLE_UPDATE", value);
		editor.commit();
	}
	
	public static boolean getCircleUpdateFlag(Context mContext) {
		SharedPreferences settings = mContext
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getBoolean("CIRCLE_UPDATE", false);
	}
	
	public static void setPlanVersionCode(Context context, int value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("PLAN_VERSION_CODE", value);
		editor.commit();
	}
	
	public static int getPlanVersionCode(Context context) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getInt("PLAN_VERSION_CODE", 0);
	}
	
	public static void setCircleVersionCode(Context context, int value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("CIRCLE_VERSION_CODE", value);
		editor.commit();
	}
	
	public static int getCirlceVersionCode(Context context) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getInt("CIRCLE_VERSION_CODE", 0);
	}
	
	public static String convertDateTime(String dateTime, String oldFormat, String newFormat){
		SimpleDateFormat oldDateTimeFormat = new SimpleDateFormat(oldFormat);
		Date oldDateTime = null;
		try {
			oldDateTime = oldDateTimeFormat.parse(dateTime);
	    }
		catch(Exception e){
	        e.printStackTrace();
	    }
		SimpleDateFormat newDateTimeFormat = new SimpleDateFormat(newFormat);
		return newDateTimeFormat.format(oldDateTime);
	}
	
	public static String convertMillisToDateTime(String milliseconds, String format){
		 SimpleDateFormat formatter = new SimpleDateFormat(format);
		 if(milliseconds == ""){
			return "";
		 }
		 Calendar calendar = Calendar.getInstance();
	     calendar.setTimeInMillis(Long.parseLong(milliseconds));
	     return formatter.format(calendar.getTime());
	}
	
	public static void setAppVersionCode(Context context, int value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("APP_VERSION_CODE", value);
		editor.commit();
	}
	
	public static int getAppVersionCode(Context context) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getInt("APP_VERSION_CODE", 0);
	}
	
	public static void setLastSyncTime(Context context, String value) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("SYNC_TIME_0", value);
		editor.commit();
	}
	
	public static String getLastSyncTime(Context context) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString("SYNC_TIME_0", "");
	}
	
	public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter(); 
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight+50 + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
	
}// final class ends here
