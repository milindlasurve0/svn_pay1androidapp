package com.mindsarray.pay1.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.mindsarray.pay1.R;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.servicehandler.FetchAddressIntentService;

/**
 * Created by rohan on 21/5/15.
 */
public class GooglePlayServicesApiClient implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public GoogleApiClient mGoogleApiClient;
    private Context mContext;
    public Location mLastLocation;
    public String latitude;
    public String longitude;
    private AddressResultReceiver mResultReceiver;
    private Boolean mReverseGeocode = false;

    public GooglePlayServicesApiClient(Context context, Api<Api.ApiOptions.NoOptions> api, Boolean reverseGeocode){
        mContext = context;
        buildGoogleApiClient(api);
        mReverseGeocode = reverseGeocode;
    }

    public synchronized void buildGoogleApiClient(Api<Api.ApiOptions.NoOptions> api) {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(api)
                .build();
        mGoogleApiClient.connect();
        Log.e("API: ", String.valueOf(mGoogleApiClient));
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        // Connected to Google Play services!
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        Log.e("Last location: ", String.valueOf(mLastLocation));
        if (mLastLocation != null) {
            latitude = String.valueOf(mLastLocation.getLatitude());
            longitude = String.valueOf(mLastLocation.getLongitude());
            Log.e("Lat/ Long: ", latitude + " " + longitude);
            if (!Geocoder.isPresent()) {
//                Toast.makeText(mContext, "No geocoder available",
//                        Toast.LENGTH_LONG).show();
            	Log.e("Geocoder", "No geocode available");
                return;
            }
            if(mReverseGeocode)
            	startIntentService();
        }
    }

    protected void startIntentService() {
        Log.e("Intent service: ", "starting..");
        mResultReceiver = new AddressResultReceiver(new Handler(), mContext);
        Intent intent = new Intent(mContext, FetchAddressIntentService.class);
        intent.putExtra(Constants.RECEIVER, mResultReceiver);
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, mLastLocation);
        mContext.startService(intent);
    }


    @Override
    public void onConnectionSuspended(int cause) {
        // The connection has been interrupted.
        // Disable any UI components that depend on Google APIs
        // until onConnected() is called.
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // This callback is important for handling errors that
        // may occur while attempting to connect with Google.
        //
        // More about this in the next section.
        Log.e("Google API services: ", "Connection failed");
    }

}

class AddressResultReceiver extends ResultReceiver {

    String mAddressOutput;
    Context mContext;
    Activity mActivity;

    public AddressResultReceiver(Handler handler, Context context) {
        super(handler);
        mContext = context;
        mActivity = (Activity) context;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        // Display the address string
        // or an error message sent from the intent service.
        mAddressOutput = resultData.getString(Constants.RESULT_DATA_KEY);
        displayAddressOutput();

        // Show a toast message if an address was found.
        if (resultCode == Constants.SUCCESS_RESULT) {
//            Toast.makeText(mContext, "Address Found",
//                    Toast.LENGTH_LONG).show();
        }
    }

    protected void displayAddressOutput(){
        try {
//            String address[] = TextUtils.split(mAddressOutput, System.getProperty("line.separator"));
//
////            EditText editText1 = (EditText) mActivity.findViewById(R.id.retailer_address);
////            editText1.setText(address[0]);
//            EditText editText2 = (EditText) mActivity.findViewById(R.id.editArea);
//            editText2.setText(address[1]);
//            EditText editText3 = (EditText) mActivity.findViewById(R.id.editCity);
//            editText3.setText(address[2].substring(0, address[2].indexOf(',')));
//            EditText editText4 = (EditText) mActivity.findViewById(R.id.editState);
//            editText4.setText(TextUtils.split(address[2], " ")[1]);
//            EditText editText5 = (EditText) mActivity.findViewById(R.id.editPincode);
//            editText5.setText(TextUtils.split(address[2], " ")[2]);
            
            String address[] = TextUtils.split(mAddressOutput, System.getProperty("line.separator"));

//            EditText editAddress = (EditText) mActivity.findViewById(R.id.retailer_address);
            EditText editArea = (EditText) mActivity.findViewById(R.id.editArea);
            EditText editCity = (EditText) mActivity.findViewById(R.id.editCity);
            EditText editState = (EditText) mActivity.findViewById(R.id.editState);
            EditText editPinCode = (EditText) mActivity.findViewById(R.id.editPincode);

            editArea.setText(address[0]);
            editCity.setText(address[1]);
            editState.setText(address[2]);
            editPinCode.setText(address[3]);

//            if(!address[0].equals("")){
//                editArea.setEnabled(false);
//                editArea.setFocusable(false);
//            }
//            else {
//                editArea.setEnabled(true);
//                editArea.setFocusable(true);
//            }
            if(!address[1].equals("")){
                editCity.setEnabled(false);
                editCity.setFocusable(false);
            }
            else {
                editCity.setEnabled(true);
                editCity.setFocusable(true);
            }
            if(!address[2].equals("")){
                editState.setEnabled(false);
                editState.setFocusable(false);
            }
            else {
                editState.setEnabled(true);
                editState.setFocusable(true);
            }
            if(!address[3].equals("")){
                editPinCode.setEnabled(false);
                editPinCode.setFocusable(false);
            }
            else {
                editPinCode.setEnabled(true);
                editPinCode.setFocusable(true);
            }
        }
        catch(Exception e){
            e.printStackTrace();
            Log.e("address parse error", "" + e.getMessage());
        }
    }
}
