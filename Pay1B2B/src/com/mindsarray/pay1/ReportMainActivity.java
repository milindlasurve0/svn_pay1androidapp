package com.mindsarray.pay1;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.ReportMainAdapter;
import com.mindsarray.pay1.constant.Constants;

public class ReportMainActivity extends Activity {
	ArrayList<String> mReportList;
	ListView mReportListView;
	ReportMainAdapter adapter;
	private static final String TAG = "Report Main Activity";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);
		 tracker = Constants.setAnalyticsTracker(this, tracker);
		setContentView(R.layout.reportmain_activity);
		ArrayList<String> arrayList = new ArrayList<String>();
		arrayList.add(
				getResources().getString(R.string.earnings));
		arrayList.add(
				getResources().getString(R.string.transaction));
		arrayList.add(
				getResources().getString(R.string.discount_structure));
		arrayList.add(
				getResources().getString(R.string.top_up));
		arrayList.add(getResources().getString(R.string.account_history));
		adapter = new ReportMainAdapter(ReportMainActivity.this, 1, arrayList);
		mReportListView = (ListView) findViewById(R.id.mReportListView);
		mReportListView.setAdapter(adapter);

		mReportListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				if (position == 0) {
					Intent intent = new Intent(ReportMainActivity.this,
							EarningActivity.class);
					startActivity(intent);
				} else if (position == 1) {
					Intent intent = new Intent(ReportMainActivity.this,
							TransactionTabFragmentActivity.class);
					startActivity(intent);
				} else if (position == 2) {
					Intent intent = new Intent(ReportMainActivity.this,
							DiscountStructureTabFragment.class);
					startActivity(intent);
				} else if (position == 3) {
					Intent intent = new Intent(ReportMainActivity.this,
							TopUpActivity.class);
					startActivity(intent);
				} else if (position == 4) {
					Intent intent = new Intent(ReportMainActivity.this,
							AccountHistoryActivity.class);
					startActivity(intent);
				}
			}

		});
	}
	public static GoogleAnalytics analytics; public static Tracker tracker;	
	@Override
		public void onStart() {
			super.onStart();
			// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
			// EasyTracker.getInstance(this).activityStart(this);
			//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
		}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants.showNavigationBar(ReportMainActivity.this);
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}
}
