package com.mindsarray.pay1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.mindsarray.pay1.adapterhandler.BusBookingBoardingPointAdapter;
import com.mindsarray.pay1.constant.Constants;

public class BusBookingBoardingPointActivity extends Activity {

	private EditText editText_SelectBoardingPoint;
	private ListView listView_BoardingPoints;
	private ImageView imageView_Clear;

	private BusBookingBoardingPointAdapter adapter;
	ArrayList<HashMap<String, String>> boarding_point_list = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> boarding_point_temp = new ArrayList<HashMap<String, String>>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.busbookingboardingpoint_activity);

		try {

			boarding_point_list
					.addAll((ArrayList<HashMap<String, String>>) getIntent()
							.getSerializableExtra("BOARDING_POINT_LIST"));

			imageView_Clear = (ImageView) findViewById(R.id.imageView_Clear);
			imageView_Clear.setVisibility(View.GONE);
			imageView_Clear.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					editText_SelectBoardingPoint.setText("");
					editText_SelectBoardingPoint.requestFocus();
				}
			});

			editText_SelectBoardingPoint = (EditText) findViewById(R.id.editText_SelectBoardingPoint);
			editText_SelectBoardingPoint
					.addTextChangedListener(new TextWatcher() {

						@Override
						public void onTextChanged(CharSequence s, int start,
								int before, int count) {
							// TODO Auto-generated method stub
							if (editText_SelectBoardingPoint.getText()
									.toString().trim().equalsIgnoreCase(""))
								imageView_Clear.setVisibility(View.GONE);
							else
								imageView_Clear.setVisibility(View.VISIBLE);
						}

						@Override
						public void beforeTextChanged(CharSequence s,
								int start, int count, int after) {
							// TODO Auto-generated method stub

						}

						@Override
						public void afterTextChanged(Editable s) {
							// TODO Auto-generated method stub
							try {
								String keyToSearch = editText_SelectBoardingPoint
										.getText().toString().trim();

								if (!keyToSearch.equalsIgnoreCase("")) {
									boarding_point_temp.clear();
									for (Map<String, String> map : boarding_point_list) {

										if (map.get(
												Constants.BUS_BOARDING_POINT_NAME)
												.toLowerCase()
												.startsWith(
														keyToSearch
																.toLowerCase())) {
											// // Log.e("VAl ", "Found : " + key
											// + " / value : " + map.values());

											// HashMap
											HashMap<String, String> map1 = new HashMap<String, String>();

											map1.put(
													Constants.BUS_BOARDING_POINT_NAME,
													map.get(Constants.BUS_BOARDING_POINT_NAME));
											map1.put(
													Constants.BUS_BOARDING_POINT_TIME,
													map.get(Constants.BUS_BOARDING_POINT_TIME));
											map1.put(
													Constants.BUS_BOARDING_POINT_ID,
													map.get(Constants.BUS_BOARDING_POINT_ID));
											boarding_point_temp.add(map1);
										}
									}
									BusBookingBoardingPointActivity.this
											.runOnUiThread(new Runnable() {

												public void run() {
													// TODO Auto-generated
													// method
													// stub
													adapter.notifyDataSetChanged();
												}
											});
								} else {
									boarding_point_temp.clear();
									boarding_point_temp
											.addAll(boarding_point_list);
									adapter.notifyDataSetChanged();
								}
							} catch (Exception e) {
								// TODO: handle exception
							}
						}
					});

			listView_BoardingPoints = (ListView) findViewById(R.id.listView_BoardingPoints);
			// Create adapter
			boarding_point_temp.addAll(boarding_point_list);
			adapter = new BusBookingBoardingPointAdapter(
					BusBookingBoardingPointActivity.this, boarding_point_temp);

			// Set adapter to AutoCompleteTextView
			listView_BoardingPoints.setAdapter(adapter);
			listView_BoardingPoints
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							Intent returnIntent = new Intent();
							HashMap<String, String> map = new HashMap<String, String>();
							map = boarding_point_temp.get(position);
							returnIntent.putExtra(
									Constants.BUS_BOARDING_POINT_NAME,
									""
											+ map.get(Constants.BUS_BOARDING_POINT_NAME));
							returnIntent.putExtra(
									Constants.BUS_BOARDING_POINT_TIME,
									""
											+ map.get(Constants.BUS_BOARDING_POINT_TIME));
							returnIntent.putExtra(
									Constants.BUS_BOARDING_POINT_ID,
									""
											+ map.get(Constants.BUS_BOARDING_POINT_ID));
							setResult(RESULT_OK, returnIntent);
							finish();
						}
					});
		} catch (Exception exception) {

		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

}
