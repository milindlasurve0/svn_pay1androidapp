package com.mindsarray.pay1;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mindsarray.pay1.RegistrationActivity.CreateRetailerTask;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

public class RegistrationPaymentOptionActivity extends Activity {
	
	private String TAG = "Registration Payment Option Activity";
	private Context mContext = RegistrationPaymentOptionActivity.this;
	private boolean isDistributor = false;
	
	private SendOTPToRetailer mSendOTPToRetailer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.registration_payment_option_activity);
		
		Utility.setSharedPreference(mContext, "Retailer_Registration_Preference", "");
		Utility.setSharedPreference(mContext, "Retailer_Under_Distributor", "");
		String registrationInterest = Utility.getSharedPreferences(mContext, "Registration_interest");
		if(registrationInterest.equals("Distributor"))
			isDistributor = true;
		LinearLayout channelPartnerLayout = (LinearLayout) findViewById(R.id.channel_partner);
		if(isDistributor){
			channelPartnerLayout.setVisibility(View.GONE);
		}			
			
		final ImageView radio1 = (ImageView) findViewById(R.id.radio1);
		final ImageView radio2 = (ImageView) findViewById(R.id.radio2);
		final ImageView checkbox = (ImageView) findViewById(R.id.checkbox);
		
		radio1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((ImageView) v).setImageResource(R.drawable.ic_radio_sel);
				radio2.setImageResource(R.drawable.ic_radio_unsel);
				Utility.setSharedPreference(mContext, "Retailer_Registration_Preference",
						"Netbanking");
			}
        });
		radio2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((ImageView) v).setImageResource(R.drawable.ic_radio_sel);
				radio1.setImageResource(R.drawable.ic_radio_unsel);
				Utility.setSharedPreference(mContext, "Retailer_Registration_Preference",
						"Deposit");
			}
        });
		checkbox.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(Utility.getSharedPreferences(mContext, "Retailer_Under_Distributor") == "true"){
					((ImageView) v).setImageResource(R.drawable.ic_chkbox_unsel);
					Utility.setSharedPreference(mContext, "Retailer_Under_Distributor",
							"");
				}
				else {
					((ImageView) v).setImageResource(R.drawable.ic_chkbox_sel);
					Utility.setSharedPreference(mContext, "Retailer_Under_Distributor",
							"true");
				}				
			}
        });
		
		Button paymentOptionContinue = (Button) findViewById(R.id.payment_option_continue);
		paymentOptionContinue.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String registrationPreference = Utility.getSharedPreferences(mContext, "Retailer_Registration_Preference");
				if(registrationPreference.equals(""))
					Constants.showOneButtonFailureDialog(mContext, "Select an option", "", 8);
				else {
//					startActivity(new Intent(mContext, RetailerVerificationActivity.class));
					mSendOTPToRetailer = new SendOTPToRetailer();
					mSendOTPToRetailer.execute();
				}	
			}
		});	
	}	
	
	public class SendOTPToRetailer extends AsyncTask<String, String, String>
														implements OnDismissListener {
		private ProgressDialog dialog;
		
		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
			listValuePair.add(new BasicNameValuePair("method", "sendOTPToRetailer"));
			listValuePair.add(new BasicNameValuePair("r_m", Utility.getSharedPreferences(mContext, "Retailer_Mobile")));

			String response = RequestClass.getInstance()
					.readPay1Request(mContext,
							Constants.API_URL, listValuePair);
		
			return response;
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
		
					JSONArray jsonArray = new JSONArray(replaced);
					JSONObject jsonObject = jsonArray.getJSONObject(0);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						if(jsonObject.has("OTA_Fee")){
							String OTA_Fee = jsonObject.getString("OTA_Fee");
							Utility.setSharedPreference(mContext, "OTA_Fee", OTA_Fee);
						}
						startActivity(new Intent(mContext, RetailerVerificationActivity.class));
					} else {
						Constants.showOneButtonFailureDialog(
								mContext,
								Constants.checkCode(replaced), TAG,
								Constants.OTHER_ERROR);
					}
		
				} else {
					Constants.showOneButtonFailureDialog(
							mContext,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);
		
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(mContext,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(mContext,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}
		
		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(mContext);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {
		
						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							SendOTPToRetailer.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}
		
		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			SendOTPToRetailer.this.cancel(true);
			dialog.cancel();
		}
	
	}
}	