package com.mindsarray.pay1;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.DiscountAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

public class DiscountStructureActivity extends Activity {
	ListView mDiscountList;
	// static final int DATE_DIALOG_ID = 0;
	ArrayList<JSONObject> mDiscountArrayList;

	String serviceType = "1";
	TextView textMobile, textDTH, textEntertainment;
	DiscountAdapter discountAdapter;
	private static final String TAG = "Discount structure";
	public static GoogleAnalytics analytics; public static Tracker tracker;	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.discountstructure_activity);
		if (Utility.getLoginFlag(DiscountStructureActivity.this,
				Constants.SHAREDPREFERENCE_IS_LOGIN)) {
			mInsideonCreate();
			
			//analytics = GoogleAnalytics.getInstance(this);
			tracker = Constants.setAnalyticsTracker(this, tracker);

			//tracker.setScreenName(TAG);
			Constants.trackOnStart(tracker, TAG);
				//.build());
//			tracker = Constants.setAnalyticsTracker(this, tracker);
		} else {
			Intent intent = new Intent(DiscountStructureActivity.this,
					LoginActivity.class);

			startActivityForResult(intent, Constants.LOGIN);

		}

	}
	
	@Override
		public void onStart() {
			super.onStart();
			// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
			// EasyTracker.getInstance(this).activityStart(this);
			//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
		}
	private void mInsideonCreate() {
		// TODO Auto-generated method stub

		findViewById();
		mDiscountArrayList = new ArrayList<JSONObject>();
		discountAdapter = new DiscountAdapter(DiscountStructureActivity.this,
				1, mDiscountArrayList);
		mDiscountList.setAdapter(discountAdapter);
		textMobile.setTextColor(getResources().getColor(R.color.white));
		textDTH.setTextColor(getResources().getColor(R.color.black));
		textEntertainment.setTextColor(getResources().getColor(R.color.black));
		serviceType = "1";
		new DiscountTask().execute();

		textMobile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mDiscountArrayList.clear();
				textMobile.setTextColor(getResources().getColor(R.color.white));
				textDTH.setTextColor(getResources().getColor(R.color.black));
				textEntertainment.setTextColor(getResources().getColor(
						R.color.black));
				serviceType = "1";
				if (RequestClass.getInstance() == null) {
					Constants.showOneButtonFailureDialog(
							DiscountStructureActivity.this,
							"Please try again or login first.",
							"Discount Structure", Constants.OTHER_ERROR);
				} else {
					new DiscountTask().execute();
				}
			}
		});

		textDTH.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mDiscountArrayList.clear();
				// TODO Auto-generated method stub
				textMobile.setTextColor(getResources().getColor(R.color.black));
				textDTH.setTextColor(getResources().getColor(R.color.white));
				textEntertainment.setTextColor(getResources().getColor(
						R.color.black));
				serviceType = "2";
				if (RequestClass.getInstance() == null) {
					Constants.showOneButtonFailureDialog(
							DiscountStructureActivity.this,
							"Please try again or login first.",
							"Discount Structure", Constants.OTHER_ERROR);
				} else {
					new DiscountTask().execute();
				}

			}
		});

		textEntertainment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mDiscountArrayList.clear();
				// TODO Auto-generated method stub
				textMobile.setTextColor(getResources().getColor(R.color.black));
				textDTH.setTextColor(getResources().getColor(R.color.black));
				textEntertainment.setTextColor(getResources().getColor(
						R.color.white));
				serviceType = "3";
				if (RequestClass.getInstance() == null) {
					Constants.showOneButtonFailureDialog(
							DiscountStructureActivity.this,
							"Please try again or login first.",
							"Discount Structure", Constants.OTHER_ERROR);
				} else {
					new DiscountTask().execute();
				}
			}
		});

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == Constants.LOGIN) {
				mInsideonCreate();
				new DiscountTask().execute();

			} else {

			}
		} else {
			finish();
		}
	}

	private void findViewById() {
		// TODO Auto-generated method stub
		textMobile = (TextView) findViewById(R.id.textMobile);
		textDTH = (TextView) findViewById(R.id.textDTH);
		textEntertainment = (TextView) findViewById(R.id.textEntertainment);
		mDiscountList = (ListView) findViewById(R.id.mDiscountList);
	}

	public class DiscountTask extends AsyncTask<String, String, String>
			implements OnDismissListener {

		private ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair
					.add(new BasicNameValuePair("method", "getCommissions"));
			listValuePair.add(new BasicNameValuePair("service", serviceType));
			listValuePair.add(new BasicNameValuePair("device_type", "android"));
			String response = RequestClass.getInstance().readPay1Request(
					DiscountStructureActivity.this, Constants.API_URL,
					listValuePair);

			listValuePair.clear();
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = array.getJSONObject(0);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONArray descriptionArray = jsonObject
								.getJSONArray("description");
						if (jsonObject.toString().contains("false")) {
							Constants
									.showOneButtonFailureDialog(
											DiscountStructureActivity.this,
											"Please try again or login first.",
											"Discount Structure",
											Constants.OTHER_ERROR);
						} else {
							JSONObject jsonObject2 = descriptionArray
									.getJSONObject(0);
							JSONArray rateJsonArray = jsonObject2
									.getJSONArray("R");
							for (int i = 0; i < rateJsonArray.length(); i++) {
								JSONArray array2 = rateJsonArray
										.getJSONArray(i);
								JSONObject object = array2.getJSONObject(0);
								mDiscountArrayList.add(object);

							}
						}
					} else {
						Constants.showOneButtonFailureDialog(
								DiscountStructureActivity.this,
								Constants.checkCode(replaced), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(
							DiscountStructureActivity.this,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(
						DiscountStructureActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(
						DiscountStructureActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(DiscountStructureActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							DiscountTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			DiscountTask.this.cancel(true);
			dialog.cancel();
		}

	}

	@Override
	protected void onResume() {
		Constants.showNavigationBar(DiscountStructureActivity.this);
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}
}
