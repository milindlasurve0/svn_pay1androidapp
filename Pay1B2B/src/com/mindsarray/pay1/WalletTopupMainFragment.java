package com.mindsarray.pay1;

import java.util.ArrayList;

import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.RechargeMainAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.utilities.GridMenuItem;

public class WalletTopupMainFragment extends Fragment {
	GridView gridView;
	ArrayList<GridMenuItem> gridArray = new ArrayList<GridMenuItem>();
	RechargeMainAdapter customGridAdapter;
	// String planJson;
	JSONObject jsonObject2;
	private static final String TAG = "Wallet Topup Main Activity";
	View vi = null;
	
	public static WalletTopupMainFragment newInstance(String content) {
		WalletTopupMainFragment fragment = new WalletTopupMainFragment();

		Bundle b = new Bundle();
		b.putString("msg", content);

		fragment.setArguments(b);

		return fragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		tracker = Constants.setAnalyticsTracker(getActivity(), tracker);
		
		vi = inflater.inflate(R.layout.rechargemain_activity, container,
				false);
		
		int mPay1Wallet = R.drawable.ic_pay1_launcher;
		int mOngoWallet = R.drawable.ic_og_mw_icon;
		
		gridArray = new ArrayList<GridMenuItem>();
		gridArray.add(new GridMenuItem(mPay1Wallet, "Pay1 Wallet",
				Constants.OPERATOR_PAY1_WALLET, Constants.WALLET_PAYMENT));
		gridArray.add(new GridMenuItem(mOngoWallet, "Ongo Wallet",
				Constants.OPERATOR_ONGO_WALLET, Constants.WALLET_PAYMENT));
		gridView = (GridView) vi.findViewById(R.id.gridView1);
		customGridAdapter = new RechargeMainAdapter(getActivity(),
				R.layout.rechargemain_adapter, gridArray,false);

		// planJson = Constants.loadJSONFromAsset(getApplicationContext(),
		// "plans.json");
		gridView.setAdapter(customGridAdapter);

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// Constants.showCustomToast(MobileRechargeActivity.this,
				// gridArray.get(position).getId()+"",
				// Toast.LENGTH_LONG).show();
				try {
					/*
					 * JSONObject jsonObject=new JSONObject(planJson);
					 * jsonObject2
					 * =jsonObject.getJSONObject(String.valueOf(gridArray
					 * .get(position).getId()));
					 */
					Intent intent = new Intent(getActivity(),
							WalletTopupActivity.class);
					intent.putExtra(Constants.OPERATOR_ID,
							gridArray.get(position).getId());
					/*
					 * intent.putExtra(Constants.PLAN_JSON,
					 * jsonObject2.toString());
					 */
					intent.putExtra(Constants.OPERATOR_LOGO,
							gridArray.get(position).getImage());
					intent.putExtra(Constants.OPERATOR_NAME,
							gridArray.get(position).getTitle());
					startActivity(intent);

				} catch (Exception e) {
					// e.printStackTrace();
				}

			}

		});
		
		return vi;
	}
	
	public static GoogleAnalytics analytics; public static Tracker tracker;	
	@Override
		public void onStart() {
			super.onStart();
			// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
			// EasyTracker.getInstance(this).activityStart(this);
			//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
		}
}
