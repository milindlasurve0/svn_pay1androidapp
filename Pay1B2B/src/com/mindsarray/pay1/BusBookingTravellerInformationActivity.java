package com.mindsarray.pay1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.mindsarray.pay1.adapterhandler.BusBookingTravellerInformationAdapter;
import com.mindsarray.pay1.constant.Constants;

public class BusBookingTravellerInformationActivity extends Activity {

	private String source = null, destination = null, date = null,
			seats = null, bus_id = null, bus_name = null, bus_time = null,
			bus_type = null, bus_fare = null;
	private TextView textView_Route, textView_Seats, textView_Date;
	private ListView listView_Information;
	private Button button_Book;

	String months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
			"Sep", "Oct", "Nov", "Dec" };

	static final int BOARDING_POINT_PICKER_ID = 1;

	BusBookingTravellerInformationAdapter busBookingInformationAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.busbookingtravellerinformation_activity);

		try {
			Bundle extras = getIntent().getExtras();
			if (extras != null) {
				source = extras.getString("SOURCE");
				destination = extras.getString("DESTINATION");
				date = extras.getString("DATE");
				seats = extras.getString("SEATS");
				bus_id = extras.getString(Constants.BUS_ID);
				bus_name = extras.getString(Constants.BUS_NAME);
				bus_time = extras.getString(Constants.BUS_TIME);
				bus_type = extras.getString(Constants.BUS_TYPE);
				bus_fare = extras.getString(Constants.BUS_FARE);
			}
		} catch (Exception exception) {

		}

		button_Book = (Button) findViewById(R.id.button_Book);
		button_Book.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				for (int i = 0; i < busBookingInformationAdapter.getName()
						.size(); i++) {
					// Log.w("Name :",
					//		busBookingInformationAdapter.getName().get(i));
				}

			}
		});

		listView_Information = (ListView) findViewById(R.id.listView_Information);
		busBookingInformationAdapter = new BusBookingTravellerInformationAdapter(
				BusBookingTravellerInformationActivity.this,
				Integer.parseInt(seats));
		listView_Information.setAdapter(busBookingInformationAdapter);

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		// if (resultCode == RESULT_CANCELED) {
		// mBtnLogin.setVisibility(View.GONE);
		// mBtnLogOut.setVisibility(View.VISIBLE);
		//
		// }
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants
				.showNavigationBar(BusBookingTravellerInformationActivity.this);
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
}
