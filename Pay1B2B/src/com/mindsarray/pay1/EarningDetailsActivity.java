package com.mindsarray.pay1;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.SalesDetailsAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

public class EarningDetailsActivity extends Activity {
	LinearLayout layoutMobile;
	private static final String TAG = "Sales details";
ArrayList<LinearLayout> linearLayouts;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.salesdetails_activity);
		layoutMobile = (LinearLayout) findViewById(R.id.layoutMobile);
		tracker = Constants.setAnalyticsTracker(this, tracker);
		linearLayouts=new ArrayList<LinearLayout>();
		new EarningTask().execute();
	}

	public static GoogleAnalytics analytics;
	public static Tracker tracker;

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
		// EasyTracker.getInstance(this).activityStart(this);
		// tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
	}

	public class EarningTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		ProgressDialog dialog;
		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair.add(new BasicNameValuePair("method", "saleReport"));
			listValuePair.add(new BasicNameValuePair("device_type", "android"));
			listValuePair.add(new BasicNameValuePair("date", getIntent()
					.getExtras().getString(Constants.DATE)));
			String response = RequestClass.getInstance().readPay1Request(
					EarningDetailsActivity.this, Constants.API_URL,
					listValuePair);
			listValuePair.clear();
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (!result.startsWith("Error")) {

				try {
					if (!result.startsWith("Error")) {
						String replaced = result.replace("(", "")
								.replace(")", "").replace(";", "");
						JSONArray array = new JSONArray(replaced);
						JSONObject jsonObject = array.getJSONObject(0);
						String status = jsonObject.getString("status");
						if (status.equalsIgnoreCase("success")) {
							JSONArray descArray = jsonObject
									.getJSONArray("description");
							JSONObject jsonObject2 = descArray.getJSONObject(0);
							Iterator keyss = jsonObject2.keys();
							while (keyss.hasNext()) {
								double amount = 0, counts = 0, income = 0;

								String type = (String) keyss.next().toString();
								Log.d("1", type);
								ArrayList<JSONObject> mMobArrayList = new ArrayList<JSONObject>();

								layoutMobile.setVisibility(View.VISIBLE);
								JSONObject jsonObject3 = jsonObject2
										.getJSONObject(type);
								JSONArray dataArray = jsonObject3
										.getJSONArray("data");
								for (int i = 0; i < dataArray.length(); i++) {
									JSONObject jsonObject4 = dataArray
											.getJSONObject(i);

									JSONObject jsonObject5 = jsonObject4
											.getJSONObject("0");
									mMobArrayList.add(jsonObject4);
									
									amount = amount
											+ Double.parseDouble(jsonObject5
													.getString("amount"));
									counts = counts
											+ Double.parseDouble(jsonObject5
													.getString("counts"));
									income = income
											+ Double.parseDouble(jsonObject5
													.getString("income"));

								}
								
								View view;
								LayoutInflater mInflater = (LayoutInflater) EarningDetailsActivity.this
										.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

								view = mInflater.inflate(
										R.layout.earning_details_layout,
										layoutMobile, false);

								TextView textTotalSale = (TextView) view
										.findViewById(R.id.textTotalSale);
								TextView textTotalIncome1 = (TextView) view
										.findViewById(R.id.textTotalIncome1);
								TextView textTotalTransaction = (TextView) view
										.findViewById(R.id.textTotalTransaction);

								ListView mMobileList = (ListView) view
										.findViewById(R.id.mMobileList);
								SalesDetailsAdapter mMobSaleAdapter = new SalesDetailsAdapter(
										EarningDetailsActivity.this, 1,
										mMobArrayList);
								
								mMobileList.setAdapter(mMobSaleAdapter);
								
								Utility.setListViewHeightBasedOnChildren(mMobileList);

								textTotalSale.setText("" + amount);
								textTotalIncome1.setText(""
										+ new DecimalFormat("##.##")
												.format(income));
								textTotalTransaction.setText("" + counts);
								layoutMobile.addView(view);
								//linearLayouts.add(layoutMobile);
								//mMobSaleAdapter.notifyDataSetChanged();

							}
							
						} else {
							Constants.showOneButtonFailureDialog(
									EarningDetailsActivity.this,
									Constants.checkCode(replaced), TAG,
									Constants.OTHER_ERROR);
						}

					} else {
						Constants.showOneButtonFailureDialog(
								EarningDetailsActivity.this,
								Constants.ERROR_INTERNET, TAG,
								Constants.OTHER_ERROR);

					}
				} catch (JSONException e) {
					// e.printStackTrace();
					Constants.showOneButtonFailureDialog(
							EarningDetailsActivity.this,
							Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
							Constants.OTHER_ERROR);
				} catch (Exception e) {
					// TODO: handle exception
					Constants.showOneButtonFailureDialog(
							EarningDetailsActivity.this,
							Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
							Constants.OTHER_ERROR);
				}
			}
			if (this.dialog.isShowing())
				this.dialog.dismiss();
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(EarningDetailsActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							EarningTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			EarningTask.this.cancel(true);
			dialog.cancel();
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants.showNavigationBar(EarningDetailsActivity.this);
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}
}
