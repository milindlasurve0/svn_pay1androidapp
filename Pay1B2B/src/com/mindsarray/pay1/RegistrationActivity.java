package com.mindsarray.pay1;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.EditTextValidator;
import com.mindsarray.pay1.utilities.GooglePlayServicesApiClient;
import com.mindsarray.pay1.utilities.Utility;

public class RegistrationActivity extends Activity {

	EditText editName, editShopName, editEmail, editMobile, editArea, editCity, editState, editPincode, editComment;
	Button mBtnRegister, mBtnCancel;
	RegistrationTask registrationTask;
	private static final String TAG = "Registration";
	private GoogleApiClient mGoogleApiClient;
	private SpinnerTextView spinnerTextView;
	
	private static final int REQUEST_RESOLVE_ERROR = 1001;
	private boolean mResolvingError = false;
	 
//	private AutoCompleteTextView editCity;
//	private PlaceArrayAdapter mPlaceArrayAdapter;
	
	private Context mContext = RegistrationActivity.this;
	CreateRetailerTask mCreateRetailerTask;
	
	GooglePlayServicesApiClient mGooglePlayServicesApiClient;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);
		try {
			setContentView(R.layout.registration_activity);
			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//			View registerPay1MerchantLogo = findViewById(R.id.register_pay1_merchant_logo);
//			registerPay1MerchantLogo.setScaleX(0.5f);
//			registerPay1MerchantLogo.setScaleY(0.5f);
			Utility.setSharedPreference(RegistrationActivity.this, "Registration_interest", "");
			Utility.setSharedPreference(RegistrationActivity.this, "Distributor_interest", "");
			Utility.setSharedPreference(RegistrationActivity.this, "Retailer_Mobile", "");
			
			spinnerTextView = (SpinnerTextView) findViewById(R.id.registration_interests_spinner);

//			mGoogleApiClient = new GoogleApiClient
//		            .Builder(this)
//		            .addApi(Places.GEO_DATA_API)
//		            .addConnectionCallbacks(this)
//		            .addOnConnectionFailedListener(this)
//		            .build();
			Constants.promptForGPS((Activity) mContext);
            mGooglePlayServicesApiClient = new GooglePlayServicesApiClient(mContext, LocationServices.API, true);
            mGoogleApiClient = mGooglePlayServicesApiClient.mGoogleApiClient;
			
//			editCity = (AutoCompleteTextView) findViewById(R.id
//		                .editCity);
//			editCity.setThreshold(3);
//			
//	        mPlaceArrayAdapter = new PlaceArrayAdapter(this, R.layout.registration_spinner_style);
//	        editCity.setAdapter(mPlaceArrayAdapter);
	        
			findViewById();
			tracker = Constants.setAnalyticsTracker(this, tracker);		
			
			editName.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					EditTextValidator.hasText(RegistrationActivity.this,
							editName, Constants.ERROR_NAME_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});
			editShopName.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					EditTextValidator.hasText(RegistrationActivity.this,
							editShopName, Constants.ERROR_NAME_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});
			editEmail.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					EditTextValidator.isEmailAddress(RegistrationActivity.this,
							editEmail, Constants.ERROR_EMAIL_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});
			editMobile.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					EditTextValidator.hasText(RegistrationActivity.this,
							editMobile, Constants.ERROR_MOBILE_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});
			editCity.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					EditTextValidator.hasText(RegistrationActivity.this,
							editCity, Constants.ERROR_CITY_BLANK_FIELD);
					
//					String query = editCity.getText().toString().trim();
//					mPlaceArrayAdapter.getFilter().filter(query);
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});
			
//			editCity.setOnItemClickListener(new OnItemClickListener() {
//			    @Override
//			    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
//			                            long arg3) {
//			    	String cityState = arg0.getItemAtPosition(arg2).toString();
//			    	Log.e("city state", cityState);
//			    	String[] locationArray = cityState.split(", ");
//			    	if(locationArray.length > 0){
//				    	editCity.setText(locationArray[0]);
//				    	if(locationArray.length > 1){
//				    		editState.setText(locationArray[1]);
//				    	}	
//			    	}
//			    }
//			});
			
			editState.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					EditTextValidator.hasText(RegistrationActivity.this,
							editState, Constants.ERROR_STATE_BLANK_FIELD);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});
			mBtnRegister.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (!checkValidation(RegistrationActivity.this)) {
						// Constants.showCustomToast(RegistrationActivity.this,
						// "Please Enter a valid Number");
					} else {
						// String string = Constants.encryptPassword(strPin);
						Utility.setSharedPreference(RegistrationActivity.this, "Retailer_Mobile", editMobile
								.getText().toString().trim());
						if(spinnerTextView.getSelectedItemPosition() == 0){
							Utility.setSharedPreference(RegistrationActivity.this, "Registration_interest",
									"Distributor");
//							registrationTask = new RegistrationTask();
//							registrationTask.execute();
							
							
							try {
								final Dialog dialog = new Dialog(mContext);
								dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
								dialog.setContentView(R.layout.dialog_failure_two_buttons);
								TextView textView_Title = (TextView) dialog
										.findViewById(R.id.textView_Title);
								TextView text = (TextView) dialog
										.findViewById(R.id.textView_Message);
								text.setText("DO YOU WANT TO START WITH A TRIAL RETAILER ACCOUNT");
								textView_Title.setText("");
								ImageView closeButton = (ImageView) dialog
										.findViewById(R.id.imageView_Close);
								closeButton.setOnClickListener(new View.OnClickListener() {
									@Override
									public void onClick(View v) {
										dialog.dismiss();
									}
								});

								Button dialogButtonOk = (Button) dialog
										.findViewById(R.id.button_Ok);
								dialogButtonOk.setOnClickListener(new View.OnClickListener() {
									@Override
									public void onClick(View v) {
//										registrationTask = new RegistrationTask();
//										registrationTask.execute();
										Utility.setSharedPreference(RegistrationActivity.this, "Distributor_interest", "Retailer");
										mCreateRetailerTask = new CreateRetailerTask();
										mCreateRetailerTask.execute();
										
										dialog.dismiss();
										
										
//										startActivity(new Intent(RegistrationActivity.this, RegistrationPaymentOptionActivity.class));
									}
								});

								Button dialogButtonCancel = (Button) dialog
										.findViewById(R.id.button_Cancel);
								dialogButtonCancel.setOnClickListener(new View.OnClickListener() {

									@Override
									public void onClick(View v) {
//										registrationTask = new RegistrationTask();
//										registrationTask.execute();
										Utility.setSharedPreference(RegistrationActivity.this, "Distributor_interest", "Distributor");
										mCreateRetailerTask = new CreateRetailerTask();
										mCreateRetailerTask.execute();
										dialog.dismiss();
										
//										startActivity(new Intent(RegistrationActivity.this, DistributorInterestActivity.class));
									}
								});

								dialog.show();
							} catch (Exception exception) {
							}
						}
						else if(spinnerTextView.getSelectedItemPosition() == 1){
							Utility.setSharedPreference(RegistrationActivity.this, "Registration_interest",
									"Retailer");
							mCreateRetailerTask = new CreateRetailerTask();
							mCreateRetailerTask.execute();
							
//							startActivity(new Intent(RegistrationActivity.this, RegistrationPaymentOptionActivity.class));
						}
						// finish();
					}
				}
			});

			mBtnCancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					startActivity(new Intent(RegistrationActivity.this, LoginActivity.class));
				}
			});

		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Registration Activity:  ", e.getMessage());
		} finally {
		}

		editComment.setOnEditorActionListener(new OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				mBtnRegister.requestFocus();
				return false;
			}
		});



	}

//	@Override
//	public void onConnected(Bundle bundle) {
////		mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
//		Log.i(TAG, "Google Places API connected.");
//		
//	}
//	
//	@Override
//	public void onConnectionFailed(ConnectionResult connectionResult) {
//		Log.e(TAG, "Google Places API connection failed with error code: "
//		        + connectionResult.getErrorCode());
//		
//		Toast.makeText(this,
//		        "Google Places API connection failed with error code:" +
//		                connectionResult.getErrorCode(),
//	        Toast.LENGTH_LONG).show();
//	}
//	
//	@Override
//	public void onConnectionSuspended(int i) {
//		mPlaceArrayAdapter.setGoogleApiClient(null);
//		Log.e(TAG, "Google Places API connection suspended.");
//	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (requestCode == REQUEST_RESOLVE_ERROR) {
	        mResolvingError = false;
	        if (resultCode == RESULT_OK) {
	            // Make sure the app is not already connected or attempting to connect
	            if (!mGoogleApiClient.isConnecting() &&
	                    !mGoogleApiClient.isConnected()) {
	                mGoogleApiClient.connect();
	            }
	        }
	    }
	}
	
//    @Override
//    public void onConnectionFailed(ConnectionResult result) {
//    	if (mResolvingError) {
//            // Already attempting to resolve an error.
//            return;
//        } else if (result.hasResolution()) {
//            try {
//                mResolvingError = true;
//                result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
//            } catch (SendIntentException e) {
//                // There was an error with the resolution intent. Try again.
//                mGoogleApiClient.connect();
//            }
//        } else {
//            // Show dialog using GooglePlayServicesUtil.getErrorDialog()
////            showErrorDialog(result.getErrorCode());
//            mResolvingError = true;
//        } 
//    	Log.e("Reg Act: ", "failed " + result.getErrorCode());
//    }
    
    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }
    
	public static GoogleAnalytics analytics; public static Tracker tracker;	
	
	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
		// EasyTracker.getInstance(this).activityStart(this);
		//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
		if (!mResolvingError) {  // more about this later
            mGoogleApiClient.connect();
        }
	}
	
	private boolean checkValidation(Context context) {
		boolean ret = true;

		if(spinnerTextView.getSelectedItemPosition() < 0){
			ret = false;
			spinnerTextView.requestFocus();
			int ecolor = R.color.black; // whatever color you want
			String estring = "Select a value!";
			ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
			SpannableStringBuilder ssbuilder = new SpannableStringBuilder(estring);
			ssbuilder.setSpan(fgcspan, 0, estring.length(), 0);
			spinnerTextView.setError(ssbuilder);
			
			return ret;
		}
		else if (!EditTextValidator.hasText(context, editName,
				Constants.ERROR_NAME_BLANK_FIELD)) {
			ret = false;
			editName.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editShopName,
				Constants.ERROR_NAME_BLANK_FIELD)) {
			ret = false;
			editShopName.requestFocus();
			return ret;
		} else if (!EditTextValidator.isEmailAddress(context, editEmail,
				Constants.ERROR_EMAIL_BLANK_FIELD)) {
			ret = false;
			editEmail.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editMobile,
				Constants.ERROR_MOBILE_BLANK_FIELD)) {
			ret = false;
			editMobile.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidMobileNumber(context, editMobile,
				Constants.ERROR_MOBILE_LENGTH_FIELD)) {
			ret = false;
			editMobile.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editArea,
				Constants.ERROR_AREA_BLANK_FIELD)) {
			ret = false;
			editArea.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editCity,
				Constants.ERROR_CITY_BLANK_FIELD)) {
			ret = false;
			editCity.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editState,
				Constants.ERROR_STATE_BLANK_FIELD)) {
			ret = false;
			editState.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editPincode,
				Constants.ERROR_LOGIN_PIN_BLANK_FIELD)) {
			ret = false;
			editPincode.requestFocus();
			return ret;
		} else if(!editMobile.getText().toString().matches("[7-9][0-9]{9}$")){
			ret = false;
			editMobile.requestFocus();
			return ret;
		}
		else
			return ret;
	}

	private void findViewById() {
		editName = (EditText) findViewById(R.id.editName);
		editShopName = (EditText) findViewById(R.id.editShopName);
		editEmail = (EditText) findViewById(R.id.editEmail);
		editMobile = (EditText) findViewById(R.id.editMobile);
		editArea = (EditText) findViewById(R.id.editArea);
		editCity = (EditText) findViewById(R.id.editCity);
		editState = (EditText) findViewById(R.id.editState);
		editPincode = (EditText) findViewById(R.id.editPincode);
		editComment = (EditText) findViewById(R.id.editComment);
		mBtnRegister = (Button) findViewById(R.id.mBtnRegister);
		mBtnCancel = (Button) findViewById(R.id.mBtnCancel);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event){
	    if ((keyCode == KeyEvent.KEYCODE_BACK)){
	        finish();
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	public class RegistrationTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
			listValuePair.add(new BasicNameValuePair("method", "addLeads"));
			listValuePair.add(new BasicNameValuePair("full_name", editName
					.getText().toString().trim()));
			listValuePair.add(new BasicNameValuePair("email", editEmail
					.getText().toString().trim()));
			listValuePair.add(new BasicNameValuePair("contact_no", editMobile
					.getText().toString().trim()));
			listValuePair.add(new BasicNameValuePair("area", editArea.getText()
					.toString().trim()));
			listValuePair.add(new BasicNameValuePair("city", editCity.getText()
					.toString().trim()));
			listValuePair.add(new BasicNameValuePair("state", editState
					.getText().toString().trim()));
			listValuePair.add(new BasicNameValuePair("pin_code", editPincode.getText()
					.toString().trim()));
			listValuePair.add(new BasicNameValuePair("comment", editComment
					.getText().toString().trim()));
			listValuePair.add(new BasicNameValuePair("req_by", "android"));

			String response = RequestClass.getInstance()
					.readPay1Request(RegistrationActivity.this,
							Constants.API_URL, listValuePair);

			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONArray jsonArray = new JSONArray(replaced);
					JSONObject jsonObject = jsonArray.getJSONObject(0);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
//						Constants.showOneButtonSuccessDialog(RegistrationActivity.this,
//								"Thank you for your interest in \n becoming a PAY1 retail partner.\n"
//								+ "\n We will be in touch with you soon. \n Feel free to call us on 022 0123456."
//										.trim(), Constants.RECHARGE_SETTINGS);
						if(Utility.getSharedPreferences(RegistrationActivity.this, "Distributor_interest") == "Distributor"){
							startActivity(new Intent(RegistrationActivity.this, DistributorInterestActivity.class));
						}
					} else {
						Constants.showOneButtonFailureDialog(
								RegistrationActivity.this,
								Constants.checkCode(replaced), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(
							RegistrationActivity.this,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(RegistrationActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(RegistrationActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(RegistrationActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							RegistrationTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			RegistrationTask.this.cancel(true);
			dialog.cancel();
		}

	}
	
	public class CreateRetailerTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private ProgressDialog dialog;
		
		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
			listValuePair.add(new BasicNameValuePair("method", "createRetailerLeads"));
			listValuePair.add(new BasicNameValuePair("r_n", editName
					.getText().toString().trim()));
			listValuePair.add(new BasicNameValuePair("r_sn", editShopName
					.getText().toString().trim()));
			listValuePair.add(new BasicNameValuePair("r_e", editEmail
					.getText().toString().trim()));
			listValuePair.add(new BasicNameValuePair("r_m", editMobile
					.getText().toString().trim()));
			listValuePair.add(new BasicNameValuePair("r_a", editArea.getText()
					.toString().trim()));
			listValuePair.add(new BasicNameValuePair("r_c", editCity.getText()
					.toString().trim()));
			listValuePair.add(new BasicNameValuePair("r_s", editState
					.getText().toString().trim()));
			listValuePair.add(new BasicNameValuePair("r_p", editPincode.getText()
					.toString().trim()));
			listValuePair.add(new BasicNameValuePair("c", editComment
					.getText().toString().trim()));
			listValuePair.add(new BasicNameValuePair("req_by", "android"));
			listValuePair.add(new BasicNameValuePair("reg_i", Utility.getSharedPreferences(RegistrationActivity.this, "Registration_interest")));
			
			String response = RequestClass.getInstance()
					.readPay1Request(RegistrationActivity.this,
							Constants.API_URL, listValuePair);
		
			return response;
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
		
					JSONArray jsonArray = new JSONArray(replaced);
					JSONObject jsonObject = jsonArray.getJSONObject(0);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						if(Utility.getSharedPreferences(RegistrationActivity.this, "Registration_interest") == "Retailer"){
							startActivity(new Intent(RegistrationActivity.this, RegistrationPaymentOptionActivity.class));
						}
						else if(Utility.getSharedPreferences(RegistrationActivity.this, "Distributor_interest") == "Retailer"){
							startActivity(new Intent(RegistrationActivity.this, RegistrationPaymentOptionActivity.class));
						}
						else if(Utility.getSharedPreferences(RegistrationActivity.this, "Distributor_interest") == "Distributor"){
							startActivity(new Intent(RegistrationActivity.this, DistributorInterestActivity.class));
						}
					} else {
						Constants.showOneButtonFailureDialog(
								RegistrationActivity.this,
								Constants.checkCode(replaced), TAG,
								Constants.OTHER_ERROR);
					}
		
				} else {
					Constants.showOneButtonFailureDialog(
							RegistrationActivity.this,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);
		
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(RegistrationActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(RegistrationActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}
		
		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(RegistrationActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {
		
						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							CreateRetailerTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}
		
		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			CreateRetailerTask.this.cancel(true);
			dialog.cancel();
		}
		
		}
}
