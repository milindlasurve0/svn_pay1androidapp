package com.mindsarray.pay1.receiverhandler;

import com.mindsarray.pay1.R;
import com.mindsarray.pay1.constant.Constants;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;

public class AddressResultReceiver extends ResultReceiver {

    String mAddressOutput;
    Context mContext;
    Activity mActivity;

    public AddressResultReceiver(Handler handler, Context context) {
        super(handler);
        mContext = context;
        mActivity = (Activity) context;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        // Display the address string
        // or an error message sent from the intent service.
        mAddressOutput = resultData.getString(Constants.RESULT_DATA_KEY);

        // Show a toast message if an address was found.
        if (resultCode == Constants.SUCCESS_RESULT) {
            displayAddressOutput();
        }
        else if(resultCode == Constants.FAILURE_RESULT){
            String title = "Address not found";
           
            Constants.showOneButtonDialog(mContext, mAddressOutput, title, "OK",0);
        }
    }

    protected void displayAddressOutput(){
        try {
            String address[] = TextUtils.split(mAddressOutput, System.getProperty("line.separator"));

            EditText editAddress = (EditText) mActivity.findViewById(R.id.retailer_address);
            EditText editArea = (EditText) mActivity.findViewById(R.id.retailer_area);
            EditText editCity = (EditText) mActivity.findViewById(R.id.retailer_city);
            EditText editState = (EditText) mActivity.findViewById(R.id.retailer_state);
            EditText editPinCode = (EditText) mActivity.findViewById(R.id.retailer_pin_code);

            editArea.setText(address[0]);
            editCity.setText(address[1]);
            editState.setText(address[2]);
            editPinCode.setText(address[3]);

            if(!address[0].equals("")){
                editArea.setEnabled(false);
                editArea.setFocusable(false);
            }
            else {
                editArea.setEnabled(true);
                editArea.setFocusable(true);
            }
            if(!address[1].equals("")){
                editCity.setEnabled(false);
                editCity.setFocusable(false);
            }
            else {
                editCity.setEnabled(true);
                editCity.setFocusable(true);
            }
            if(!address[2].equals("")){
                editState.setEnabled(false);
                editState.setFocusable(false);
            }
            else {
                editState.setEnabled(true);
                editState.setFocusable(true);
            }
            if(!address[3].equals("")){
                editPinCode.setEnabled(false);
                editPinCode.setFocusable(false);
            }
            else {
                editPinCode.setEnabled(true);
                editPinCode.setFocusable(true);
            }
        }
        catch(Exception e){
            e.printStackTrace();
            Log.e("address parse error", "error: " + e.getMessage());
        }
    }
}