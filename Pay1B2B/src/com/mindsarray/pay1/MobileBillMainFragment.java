package com.mindsarray.pay1;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.RechargeMainAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.utilities.GridMenuItem;

public class MobileBillMainFragment extends Fragment {

	View vi = null;
	GridView gridView;
	ArrayList<GridMenuItem> gridArray = new ArrayList<GridMenuItem>();
	RechargeMainAdapter customGridAdapter;
	private static final String TAG = "Mobile Bill Fragement";
	// String planJson;
	// JSONObject jsonObject2;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		vi = inflater.inflate(R.layout.rechargemain_activity, container, false);
//		analytics = GoogleAnalytics.getInstance(getActivity()); tracker = Constants.setAnalyticsTracker(this, tracker);//easyTracker.set(Fields.SCREEN_NAME, TAG);
//		analytics = GoogleAnalytics.getInstance(getActivity());
		tracker = Constants.setAnalyticsTracker(getActivity(), tracker);
		if (gridArray.size() == 0) {
			int mAirtel = R.drawable.m_airtel;
			int mCellone = R.drawable.m_bsnl;
			int mVodafone = R.drawable.m_vodafone;
			int mIdea = R.drawable.m_idea;
			int mLoop = R.drawable.m_loop;
			int mTataDocomo = R.drawable.m_docomo;
			int mTataIndicom = R.drawable.m_indicom;
			int mReliance = R.drawable.m_reliance;

			gridArray.add(new GridMenuItem(mTataDocomo, "Tata Docomo",
					Constants.OPERATOR_BILL_DOCOMO, Constants.BILL_PAYMENT));
			/*gridArray.add(new GridMenuItem(mLoop, "Loop",
					Constants.OPERATOR_BILL_LOOP, Constants.BILL_PAYMENT));*/
			gridArray.add(new GridMenuItem(mCellone, "Cellone",
					Constants.OPERATOR_BILL_CELLONE, Constants.BILL_PAYMENT));
			gridArray.add(new GridMenuItem(mIdea, "Idea",
					Constants.OPERATOR_BILL_IDEA, Constants.BILL_PAYMENT));
			gridArray.add(new GridMenuItem(mTataIndicom, "Tata TeleServices",
					Constants.OPERATOR_BILL_TATATELESERVICE,
					Constants.BILL_PAYMENT));
			gridArray.add(new GridMenuItem(mVodafone, "Vodafone",
					Constants.OPERATOR_BILL_VODAFONE, Constants.BILL_PAYMENT));
			gridArray.add(new GridMenuItem(mAirtel, "Airtel",
					Constants.OPERATOR_BILL_AIRTEL, Constants.BILL_PAYMENT));
			gridArray.add(new GridMenuItem(mReliance, "Reliance",
					Constants.OPERATOR_BILL_RELIANCE, Constants.BILL_PAYMENT));

		}

		gridView = (GridView) vi.findViewById(R.id.gridView1);

		customGridAdapter = new RechargeMainAdapter(getActivity(),
				R.layout.rechargemain_adapter, gridArray,false);

		// planJson = Constants.loadJSONFromAsset(getActivity(), "plans.json");
		gridView.setAdapter(customGridAdapter);

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// Constants.showCustomToast(MobileRechargeActivity.this,
				// gridArray.get(position).getId()+"",
				// Toast.LENGTH_LONG).show();
				try {
					/*
					 * JSONObject jsonObject=new JSONObject(planJson);
					 * jsonObject2
					 * =jsonObject.getJSONObject(String.valueOf(gridArray
					 * .get(position).getId()));
					 */
					Intent intent = new Intent(getActivity(),
							MobileBillActivity.class);
					intent.putExtra(Constants.OPERATOR_ID,
							gridArray.get(position).getId());
					/*
					 * intent.putExtra(Constants.PLAN_JSON,
					 * jsonObject2.toString());
					 */
					intent.putExtra(Constants.PLAN_JSON,
							String.valueOf(gridArray.get(position).getId()));
					intent.putExtra(Constants.OPERATOR_LOGO,
							gridArray.get(position).getImage());
					intent.putExtra(Constants.OPERATOR_NAME,
							gridArray.get(position).getTitle());
					getActivity().startActivity(intent);

				} catch (Exception e) {
					// e.printStackTrace();
				}

			}

		});
		return vi;

	}
	public static GoogleAnalytics analytics; public static Tracker tracker;	
	@Override
		public void onStart() {
			super.onStart();
			// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
			// EasyTracker.getInstance(this).activityStart(this);
			//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
		}
	public static MobileBillMainFragment newInstance(String content) {
		MobileBillMainFragment fragment = new MobileBillMainFragment();

		Bundle b = new Bundle();
		b.putString("msg", content);

		fragment.setArguments(b);

		return fragment;
	}

	// private String mContent = "???";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// if ((savedInstanceState != null)
		// && savedInstanceState.containsKey(KEY_CONTENT)) {
		// mContent = savedInstanceState.getString(KEY_CONTENT);
		// }
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// outState.putString(KEY_CONTENT, mContent);
	}
}
