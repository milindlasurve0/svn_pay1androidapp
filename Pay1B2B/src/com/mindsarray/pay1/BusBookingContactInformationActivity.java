package com.mindsarray.pay1;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.utilities.EditTextValidator;

public class BusBookingContactInformationActivity extends Activity {

	private String source_id = null, source = null, destination_id = null,
			destination = null, date = null, seats = null, bus_id = null,
			bus_name = null, bus_time = null, bus_type = null, bus_fare = null,
			arrival_time = null, departure_time = null,
			boarding_point_name = null, boarding_point_id = null,
			boarding_point_time = null, cancellation_policy = null;
	int seat_count = 0;
	private TextView textView_Route, textView_Seats, textView_Date;
	private LinearLayout linearLayout_Information1, linearLayout_Information2,
			linearLayout_Information3, linearLayout_Information4,
			linearLayout_Information5;
	private EditText editText_Name1, editText_Name2, editText_Name3,
			editText_Name4, editText_Name5, editText_Email, editText_Mobile;
	private EditText editText_Age1, editText_Age2, editText_Age3,
			editText_Age4, editText_Age5;
	private RadioGroup radioGroup_Gender1, radioGroup_Gender2,
			radioGroup_Gender3, radioGroup_Gender4, radioGroup_Gender5;

	private Button button_Book;

	String months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
			"Sep", "Oct", "Nov", "Dec" };

	static final int BOARDING_POINT_PICKER_ID = 1;
	ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.busbookingcontactinformation_activity);

		try {
			Bundle extras = getIntent().getExtras();
			if (extras != null) {
				source = extras.getString("SOURCE");
				source_id = extras.getString("SOURCE_ID");
				destination = extras.getString("DESTINATION");
				destination_id = extras.getString("DESTINATION_ID");
				date = extras.getString("DATE");
				seats = extras.getString("SEATS");
				seat_count = extras.getInt("SEATS_COUNT", 0);
				bus_id = extras.getString(Constants.BUS_ID);
				bus_name = extras.getString(Constants.BUS_NAME);
				bus_time = extras.getString(Constants.BUS_TIME);
				bus_type = extras.getString(Constants.BUS_TYPE);
				bus_fare = extras.getString(Constants.BUS_FARE);
				boarding_point_id = extras
						.getString(Constants.BUS_BOARDING_POINT_ID);
				boarding_point_name = extras
						.getString(Constants.BUS_BOARDING_POINT_NAME);
				boarding_point_time = extras
						.getString(Constants.BUS_BOARDING_POINT_TIME);
				cancellation_policy = extras
						.getString(Constants.BUS_CANCELLATION_POLICY);
			}

			textView_Route = (TextView) findViewById(R.id.textView_Route);
			textView_Route.setText(source + " to " + destination);
			textView_Seats = (TextView) findViewById(R.id.textView_Seats);
			textView_Seats.setText("Number of Travelers : " + seat_count);
			textView_Date = (TextView) findViewById(R.id.textView_Date);
			textView_Date.setText("Date of Travel : " + date);

			linearLayout_Information1 = (LinearLayout) findViewById(R.id.linearLayout_Information1);
			linearLayout_Information2 = (LinearLayout) findViewById(R.id.linearLayout_Information2);
			linearLayout_Information3 = (LinearLayout) findViewById(R.id.linearLayout_Information3);
			linearLayout_Information4 = (LinearLayout) findViewById(R.id.linearLayout_Information4);
			linearLayout_Information5 = (LinearLayout) findViewById(R.id.linearLayout_Information5);

			editText_Name1 = (EditText) findViewById(R.id.editText_Name1);
			editText_Name2 = (EditText) findViewById(R.id.editText_Name2);
			editText_Name3 = (EditText) findViewById(R.id.editText_Name3);
			editText_Name4 = (EditText) findViewById(R.id.editText_Name4);
			editText_Name5 = (EditText) findViewById(R.id.editText_Name5);

			editText_Age1 = (EditText) findViewById(R.id.editText_Age1);
			editText_Age2 = (EditText) findViewById(R.id.editText_Age2);
			editText_Age3 = (EditText) findViewById(R.id.editText_Age3);
			editText_Age4 = (EditText) findViewById(R.id.editText_Age4);
			editText_Age5 = (EditText) findViewById(R.id.editText_Age5);

			radioGroup_Gender1 = (RadioGroup) findViewById(R.id.radioGroup_Gender1);
			radioGroup_Gender2 = (RadioGroup) findViewById(R.id.radioGroup_Gender2);
			radioGroup_Gender3 = (RadioGroup) findViewById(R.id.radioGroup_Gender3);
			radioGroup_Gender4 = (RadioGroup) findViewById(R.id.radioGroup_Gender4);
			radioGroup_Gender5 = (RadioGroup) findViewById(R.id.radioGroup_Gender5);

			editText_Email = (EditText) findViewById(R.id.editText_Email);
			editText_Mobile = (EditText) findViewById(R.id.editText_Mobile);

			switch (seat_count) {
			case 1:
				linearLayout_Information1.setVisibility(View.VISIBLE);
				linearLayout_Information2.setVisibility(View.GONE);
				linearLayout_Information3.setVisibility(View.GONE);
				linearLayout_Information4.setVisibility(View.GONE);
				linearLayout_Information5.setVisibility(View.GONE);
				editText_Age1.setImeOptions(EditorInfo.IME_ACTION_DONE);
				break;
			case 2:
				linearLayout_Information1.setVisibility(View.VISIBLE);
				linearLayout_Information2.setVisibility(View.VISIBLE);
				linearLayout_Information3.setVisibility(View.GONE);
				linearLayout_Information4.setVisibility(View.GONE);
				linearLayout_Information5.setVisibility(View.GONE);
				editText_Age2.setImeOptions(EditorInfo.IME_ACTION_DONE);
				break;
			case 3:
				linearLayout_Information1.setVisibility(View.VISIBLE);
				linearLayout_Information2.setVisibility(View.VISIBLE);
				linearLayout_Information3.setVisibility(View.VISIBLE);
				linearLayout_Information4.setVisibility(View.GONE);
				linearLayout_Information5.setVisibility(View.GONE);
				editText_Age3.setImeOptions(EditorInfo.IME_ACTION_DONE);
				break;
			case 4:
				linearLayout_Information1.setVisibility(View.VISIBLE);
				linearLayout_Information2.setVisibility(View.VISIBLE);
				linearLayout_Information3.setVisibility(View.VISIBLE);
				linearLayout_Information4.setVisibility(View.VISIBLE);
				linearLayout_Information5.setVisibility(View.GONE);
				editText_Age4.setImeOptions(EditorInfo.IME_ACTION_DONE);
				break;
			case 5:
				linearLayout_Information1.setVisibility(View.VISIBLE);
				linearLayout_Information2.setVisibility(View.VISIBLE);
				linearLayout_Information3.setVisibility(View.VISIBLE);
				linearLayout_Information4.setVisibility(View.VISIBLE);
				linearLayout_Information5.setVisibility(View.VISIBLE);
				editText_Age5.setImeOptions(EditorInfo.IME_ACTION_DONE);
				break;
			default:
				linearLayout_Information1.setVisibility(View.VISIBLE);
				linearLayout_Information2.setVisibility(View.GONE);
				linearLayout_Information3.setVisibility(View.GONE);
				linearLayout_Information4.setVisibility(View.GONE);
				linearLayout_Information5.setVisibility(View.GONE);
				editText_Age1.setImeOptions(EditorInfo.IME_ACTION_DONE);
				break;
			}

			button_Book = (Button) findViewById(R.id.button_Book);
			button_Book.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						if (!EditTextValidator.hasText(
								BusBookingContactInformationActivity.this,
								editText_Email,
								Constants.ERROR_EMAIL_BLANK_FIELD)
								|| !EditTextValidator
										.isEmailAddress(
												BusBookingContactInformationActivity.this,
												editText_Email,
												Constants.ERROR_EMAIL_VALID_FIELD)) {
							editText_Email.requestFocus();
							return;
						} else if (!EditTextValidator.hasText(
								BusBookingContactInformationActivity.this,
								editText_Mobile,
								Constants.ERROR_MOBILE_BLANK_FIELD)
								|| !EditTextValidator
										.isValidMobileNumber(
												BusBookingContactInformationActivity.this,
												editText_Mobile,
												Constants.ERROR_MOBILE_LENGTH_FIELD)) {
							// Constants.showCustomToast(
							// BusBookingContactInformationActivity.this,
							// "Please enter correct mobile address.");
							editText_Mobile.requestFocus();
							return;
						} else {
							ArrayList<EditText> editText_Name = new ArrayList<EditText>();
							ArrayList<EditText> editText_Age = new ArrayList<EditText>();
							ArrayList<RadioGroup> radioGroup_Gender = new ArrayList<RadioGroup>();

							editText_Name.add(editText_Name1);
							editText_Name.add(editText_Name2);
							editText_Name.add(editText_Name3);
							editText_Name.add(editText_Name4);
							editText_Name.add(editText_Name5);
							editText_Age.add(editText_Age1);
							editText_Age.add(editText_Age2);
							editText_Age.add(editText_Age3);
							editText_Age.add(editText_Age4);
							editText_Age.add(editText_Age5);
							radioGroup_Gender.add(radioGroup_Gender1);
							radioGroup_Gender.add(radioGroup_Gender2);
							radioGroup_Gender.add(radioGroup_Gender3);
							radioGroup_Gender.add(radioGroup_Gender4);
							radioGroup_Gender.add(radioGroup_Gender5);

							if (!checkValidation(
									BusBookingContactInformationActivity.this,
									editText_Name, editText_Age,
									radioGroup_Gender, seat_count)) {

							} else {
								data.clear();
								for (int i = 0; i < seat_count; i++) {
									HashMap<String, String> map = new HashMap<String, String>();

									int id = radioGroup_Gender.get(i)
											.getCheckedRadioButtonId();
									View radioButton = radioGroup_Gender.get(i)
											.findViewById(id);
									int radioId = radioGroup_Gender.get(i)
											.indexOfChild(radioButton);
									RadioButton btn = (RadioButton) radioGroup_Gender
											.get(i).getChildAt(radioId);
									String selection = (String) btn.getText();
									// Log.e("Rad Val", editText_Name.get(i)
									// .getText().toString().trim()
									// + "  "
									// + editText_Age.get(i).getText()
									// .toString().trim()
									// + "  "
									// + selection);

									map.put(Constants.BUS_TRAVELLER_NAME,
											editText_Name.get(i).getText()
													.toString().trim());
									map.put(Constants.BUS_TRAVELLER_AGE,
											editText_Age.get(i).getText()
													.toString().trim());
									map.put(Constants.BUS_TRAVELLER_GENDER,
											selection);
									data.add(map);
								}

								Intent intent = new Intent(
										BusBookingContactInformationActivity.this,
										BusBookingSummaryActivity.class);
								intent.putExtra("SOURCE", source);
								intent.putExtra("SOURCE_ID", source_id);
								intent.putExtra("DESTINATION", destination);
								intent.putExtra("DESTINATION_ID",
										destination_id);
								intent.putExtra("DATE", date);
								intent.putExtra("SEATS", seats);
								intent.putExtra("SEATS_COUNT", seat_count);
								intent.putExtra(Constants.BUS_ID, bus_id);
								intent.putExtra(Constants.BUS_NAME, bus_name);
								intent.putExtra(Constants.BUS_TIME, bus_time);
								intent.putExtra(Constants.BUS_FARE, bus_fare);
								intent.putExtra(Constants.BUS_TYPE, bus_type);
								intent.putExtra(
										Constants.BUS_BOARDING_POINT_ID,
										boarding_point_id);
								intent.putExtra(
										Constants.BUS_BOARDING_POINT_NAME,
										boarding_point_name);
								intent.putExtra(
										Constants.BUS_BOARDING_POINT_TIME,
										boarding_point_time);
								intent.putExtra(
										Constants.BUS_CANCELLATION_POLICY,
										cancellation_policy);
								intent.putExtra("DATA", data);
								intent.putExtra("EMAIL", editText_Email
										.getText().toString().trim());
								intent.putExtra("MOBILE", editText_Mobile
										.getText().toString().trim());
								startActivity(intent);
							}
						}
					} catch (Exception e) {

					}
				}
			});

		} catch (Exception e) {
		}
	}

	private boolean checkValidation(Context context,
			ArrayList<EditText> editText_Name,
			ArrayList<EditText> editText_Age,
			ArrayList<RadioGroup> radioGroup_Gender, int size) {
		boolean flag = true;
		for (int i = 0; i < size; i++) {
			if (!EditTextValidator.hasText(context, editText_Name.get(i),
					Constants.ERROR_BUS_TRAVEL_NAME_FIELD)) {
				// Constants.showCustomAlert(BusBookingContactInformationActivity.this,
				// "Please enter traveller's name.");
				editText_Name.get(i).requestFocus();
				flag = false;
				break;
			} else if (!EditTextValidator.hasText(context, editText_Age.get(i),
					Constants.ERROR_BUS_TRAVEL_AGE_FIELD)) {
				// Constants.showCustomAlert(BusBookingContactInformationActivity.this,
				// "Please enter traveller's age.");
				editText_Age.get(i).requestFocus();
				flag = false;
				break;
			} else if (radioGroup_Gender.get(i).getCheckedRadioButtonId() == -1) {
				// Constants.showCustomAlert(BusBookingContactInformationActivity.this,
				// "Please enter traveller's gender.");
				Constants.showOneButtonFailureDialog(
						BusBookingContactInformationActivity.this,
						"Please enter traveller's gender.", "Bus Booking",
						Constants.OTHER_ERROR);
				flag = false;
				break;
			} else {
				flag = true;

			}
			// if (!flag) {
			// HashMap<String, String> map = new HashMap<String, String>();
			// map.put(Constants.BUS_TRAVELLER_NAME, editText_Name.get(i)
			// .getText().toString().trim());
			// map.put(Constants.BUS_TRAVELLER_AGE, editText_Age.get(i)
			// .getText().toString().trim());
			// data.add(map);
			// }
		}

		return flag;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		// if (resultCode == RESULT_CANCELED) {
		// mBtnLogin.setVisibility(View.GONE);
		// mBtnLogOut.setVisibility(View.VISIBLE);
		//
		// }
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants.showNavigationBar(BusBookingContactInformationActivity.this);
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
}
