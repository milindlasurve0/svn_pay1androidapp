package com.mindsarray.pay1;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mindsarray.pay1.SettingsSmsActivity.SendLogs;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class SettingsDebugActivity extends Activity {
	Button buttonSendLogs,btnClearCache;
	private static final String TAG = "Maintenance Logs";
	Context mContext;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_debug_activity);
		mContext=SettingsDebugActivity.this;
		buttonSendLogs = (Button) findViewById(R.id.sendLogs);
		btnClearCache=(Button)findViewById(R.id.btnClearCache);
		
		buttonSendLogs.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mSendLogs = new SendLogs();
				mSendLogs.execute();
			}
		});
		
		btnClearCache.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String message = "Are you sure you want to clear cache? This will delete all app data.";
				Runnable buttonNow = new Runnable() {
					@Override
					public void run() {
						AnalyticsApplication.getInstance().clearApplicationData();
						Intent intent = new Intent(SettingsDebugActivity.this,
								SplashScreenActivity.class);
						intent.addCategory(Intent.CATEGORY_HOME);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
								| Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
					}
				};
				Constants.showTwoButtonDialog(mContext, message, "Clear Cache", "Yes", buttonNow, "No");				
			}
		});
		
		
		
	}

	
SendLogs mSendLogs;
	
	public class SendLogs extends AsyncTask<String, String, String>
															implements OnDismissListener {
		
		private ProgressDialog dialog;
		
		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
			listValuePair = Constants.getLogs(mContext);
			
			String response = RequestClass.getInstance()
					.readPay1Request(mContext,
							Constants.API_URL, listValuePair);
			return response;
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);Log.e("post execute", result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {
			if (!result.startsWith("Error")) {
//				result = result.substring(result.indexOf("([{"), result.lastIndexOf("}])") + 3);
				String replaced = result.replace("(", "").replace(")", "")
						.replace(";", "");
			
				JSONArray jsonArray = new JSONArray(replaced);
				JSONObject jsonObject = jsonArray.getJSONObject(0);
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("success")) {
					Constants.showOneButtonFailureDialog(mContext,
							"Logs sent successfully.", TAG,
							Constants.OTHER_ERROR);
			} else {
				Constants.showOneButtonFailureDialog(mContext,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
			
			} else {
				Constants.showOneButtonFailureDialog(mContext,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
			} catch (JSONException e) {
				e.printStackTrace();
				Constants.showOneButtonFailureDialog(mContext,
				Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
				Constants.OTHER_ERROR);
			} catch (Exception e) {
				e.printStackTrace();
				Constants.showOneButtonFailureDialog(mContext,
				Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
				Constants.OTHER_ERROR);
			}
		}
		
		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(mContext);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
			.setOnCancelListener(new DialogInterface.OnCancelListener() {
			
				@Override
				public void onCancel(DialogInterface dialog) {
					SendLogs.this.cancel(true);
				}
			});
			this.dialog.show();
			super.onPreExecute();
		}
			
		@Override
		public void onDismiss(DialogInterface dialog) {
			SendLogs.this.cancel(true);
			dialog.cancel();
		}
		
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants.showNavigationBar(SettingsDebugActivity.this);

		super.onResume();
	}
	
}
