package com.mindsarray.pay1.servicehandler;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;

import com.mindsarray.pay1.MainActivity;
import com.mindsarray.pay1.NotificationActivity;
import com.mindsarray.pay1.R;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.NotificationDataSource;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;
import com.google.android.gms.gcm.GcmListenerService;

import java.util.ArrayList;
import java.util.Date;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class gcmListenerService extends GcmListenerService {

    String TAG = "gcmListenerService";
    Context mContext = gcmListenerService.this;
    Intent intent;
	public static final String BROADCAST_ACTION = "com.mindsarray.pay1.servicehandler.displayevent";
    @Override
    public void onMessageReceived(String from, Bundle data) {
        try {
            Log.e(TAG, "From: " + from);
            Log.e(TAG, "Data: " + data.toString());
        new CheckBalanceTask().execute();
		
		String msg = data.getString("data");
		String description = "", title = "", type = "";
		JSONObject jsonObject = new JSONObject(msg);
		description = jsonObject.getString("msg");
		title = jsonObject.getString("title");
		type = jsonObject.getString("type");
		
	/*	if (type.equalsIgnoreCase("notification")) {
			callNotification(type, description, title);
			Notify(title, description);
		} else if (type.equalsIgnoreCase("banner")) {
			showBanner(type, description, msg);
		}else if(type.equalsIgnoreCase("upload")){
			callNotification(type, description, title);
			Notify(title, description);
		}*/
		
		callNotification(type, description, title);
		Notify(title, description);
		
		
		
		Utility.setSharedPreference(mContext, Constants.SHAREDPREFERENCE_LAST_NOTIFICATION, "" + description);
		
		//updateMainActivity(mContext);
		//Constants.showNavigationBar(mContext);
		} catch (Exception e) {
			// e.printStackTrace();
		}
        
    }
    
    static void updateMainActivity(Context context) {
        Intent intent = new Intent("last_notification");
        context.sendBroadcast(intent);
    }
    
    private void callNotification(String type,
			String description, String msg) {

		NotificationDataSource notificationDataSource = new NotificationDataSource(
				mContext);
		try {
			notificationDataSource.open();
			notificationDataSource.createNotification(description, 1, ""
					+ System.currentTimeMillis());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			notificationDataSource.close();
		}
		
		
	}
    
    private void Notify(String notificationTitle,
			String notificationMessage) {
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		Notification notification = new Notification(
				R.drawable.ic_b2b_notif, "Pay1 notification",
				System.currentTimeMillis());

		notification.number = Constants.COUNT_NOTIFY++;
intent = new Intent(BROADCAST_ACTION);	
		
		intent.putExtra("time", new Date().toLocaleString());
    	intent.putExtra("counter", String.valueOf(Constants.COUNT_NOTIFY));
    	sendBroadcast(intent);
		Utility.setNotificationCount(mContext, "NotiCount",
				Constants.COUNT_NOTIFY);
		Intent notificationIntent = new Intent(this, NotificationActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
				notificationIntent, 0);

		try {
			Uri notification1 = RingtoneManager
					.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			Ringtone r = RingtoneManager.getRingtone(getApplicationContext(),
					notification1);
			r.play();

			Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

			v.vibrate(1000);
		} catch (Exception e) {
		}

		notification.setLatestEventInfo(mContext, notificationTitle,
				notificationMessage, pendingIntent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(9999, notification);
	}
    
    private void showBanner(String type, String description,
			String msg) {
		NotificationDataSource notificationDataSource = new NotificationDataSource(
				mContext);
		try {
			notificationDataSource.open();
			notificationDataSource.createNotification(description, 1, ""
					+ System.currentTimeMillis());
		} catch (SQLException exception) {

		} catch (Exception e) {

		} finally {
			notificationDataSource.close();
		}
		Intent notifyIntent = new Intent(this, NotificationActivity.class);
		notifyIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);

		notifyIntent.putExtra("pushBundle", msg);

		mContext.startActivity(notifyIntent);
	}
    
    public class CheckBalanceTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair.add(new BasicNameValuePair("method", "updateBal"));
			String response = RequestClass.getInstance()
					.readPay1Request(gcmListenerService.this,
							Constants.API_URL, listValuePair);
			listValuePair.clear();

			return response;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = array.getJSONObject(0);
					String status = jsonObject.getString("status");

					if (status.equalsIgnoreCase("success")) {
						if (jsonObject.getString("login").equalsIgnoreCase("1")) {
							Utility.setBalance(gcmListenerService.this,
									Constants.SHAREDPREFERENCE_BALANCE,
									jsonObject.getString("description"));
						} else {

						}
					}
				}

			} catch (JSONException e) {

			} catch (Exception e) {

			}
		}
	}
}
