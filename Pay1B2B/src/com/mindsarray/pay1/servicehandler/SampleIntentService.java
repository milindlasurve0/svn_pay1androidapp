package com.mindsarray.pay1.servicehandler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.AsyncTask;
import android.util.Log;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.NumbberWithCircleDataSource;
import com.mindsarray.pay1.databasehandler.Pay1SQLiteHelper;
import com.mindsarray.pay1.utilities.Utility;

public class SampleIntentService extends IntentService {
	NumbberWithCircleDataSource circleDataSource;
	private SQLiteDatabase database;
	private Pay1SQLiteHelper dbHelper;

	public SampleIntentService() {
		super("CircleService");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		dbHelper = new Pay1SQLiteHelper(SampleIntentService.this);
		database = dbHelper.getWritableDatabase();
		circleDataSource = new NumbberWithCircleDataSource(
				SampleIntentService.this);
		new SyncCircleDataTask().execute();
	}

	public class SyncCircleDataTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			dbHelper = new Pay1SQLiteHelper(SampleIntentService.this);
			database = dbHelper.getWritableDatabase();
			circleDataSource = new NumbberWithCircleDataSource(
					SampleIntentService.this);
			String time = Utility.getSharedPreferences(
					SampleIntentService.this, Constants.LAST_CIRCLE_UPDATED);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String currentDateandTime = dateFormat.format(new Date());
			
			String response = Constants.getPlanData(SampleIntentService.this, Constants.API_URL
					+ "method=getMobileDetails&mobile=all&timestamp" + time);
			
			Date date = new Date();
			
			Log.d("Start Time", "Plan Start Time Circle   " + currentDateandTime);
			try {
			  circleDataSource.open();
				String sql = "INSERT INTO "
						+ Pay1SQLiteHelper.TABLE_NUMBERANDCIRCLE
						+ " VALUES (?,?,?,?,?,?,?,?);";
				SQLiteStatement statement = database.compileStatement(sql);
				database.beginTransaction();
				String timeStamp = dateFormat.format(date);
				if (!response.startsWith("Error")) {
					String replaced = response.replace("(", "")
							.replace(")", "").replace(";", "");
					//Log.d("Circle  ", "Plan  "+response);
					replaced = replaced.substring(1, replaced.length() - 1);
					/*JSONArray array = new JSONArray(replaced);*/
					JSONObject jsonObject = new JSONObject(replaced);
					/*String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {*/
						String details = jsonObject.getString("details");
						JSONArray detailsArray = new JSONArray(details);
						for (int i = 0; i < detailsArray.length(); i++) {
							JSONObject object = detailsArray.getJSONObject(i);
							String areaName = object.getString("area_name");
							String areaCode = object.getString("area");
							String operatorName = object.getString("opr_name");
							String operatorCode = object.getString("operator");
							String operatorId = object.getString("product_id");
							String number = object.getString("number");

							

							//statement.bindString(1, "1");
							statement.bindString(2, areaCode);
							statement.bindString(3, areaName);
							statement.bindString(4, operatorCode);
							statement.bindString(5, operatorName);
							statement.bindString(6, operatorId);
							statement.bindString(7, number);
							statement.bindString(8, timeStamp);

							statement.execute();

							
							/* circleDataSource
							 * .createNumberAndCircleList(areaName, areaCode,
							 * operatorName, operatorCode, operatorId, number,
							 * timeStamp);
							 */
						//}

					}

				}
				Utility.setSharedPreference(
						SampleIntentService.this,
						Constants.LAST_CIRCLE_UPDATED,
						String.valueOf(System.currentTimeMillis()));
				
				
				database.setTransactionSuccessful();
				database.endTransaction();
			} catch (JSONException je) {
				Log.d("NAC", "row inserted   " + je.getMessage());
				je.printStackTrace();
			} catch (SQLException exception) {
				Log.d("NAC", "row inserted   " + exception.getMessage());
				// TODO: handle exception
				// // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				Log.d("NAC", "row inserted   " + e.getMessage());
				// TODO: handle exception
				// // Log.e("Er ", e.getMessage());
			} finally {
				circleDataSource.close();
				stopSelf();
			}
			DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String currentDateandTime1 = dateFormat1.format(new Date());
			Log.d("Start Time", "Plan Start Time End Circle   " + currentDateandTime1);
			return response;

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			try {
				stopSelf();
			} catch (Exception exception) {
				Log.e("Error:", "Error    ");
			}

		}

	}

}
