package com.mindsarray.pay1.servicehandler;

import com.google.android.gms.iid.InstanceIDListenerService;

import android.content.Intent;
import android.util.Log;

/**
 * Created by rohan on 27/8/15.
 */
public class instanceIDListenerService extends InstanceIDListenerService {
    private static final String TAG = "InstanceIDLS";

    @Override
    public void onTokenRefresh() {
        Log.e(TAG, "onTokenRefresh");
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
    }
}
