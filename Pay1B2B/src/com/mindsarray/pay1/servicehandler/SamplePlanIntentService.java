package com.mindsarray.pay1.servicehandler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.database.DatabaseUtils.InsertHelper;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;
import android.os.AsyncTask;
import android.util.Log;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.NumbberWithCircleDataSource;
import com.mindsarray.pay1.databasehandler.Pay1SQLiteHelper;
import com.mindsarray.pay1.databasehandler.PlanDataSource;
import com.mindsarray.pay1.utilities.Utility;

public class SamplePlanIntentService extends IntentService {
	PlanDataSource planDataSource;
	private SQLiteDatabase database;
	private Pay1SQLiteHelper dbHelper;
	NumbberWithCircleDataSource circleDataSource;
	DateFormat dateFormat;
	String values = "";

	public SamplePlanIntentService() {
		super("PlanService");
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		dbHelper = new Pay1SQLiteHelper(SamplePlanIntentService.this);
		database = dbHelper.getWritableDatabase();
		planDataSource = new PlanDataSource(SamplePlanIntentService.this);
		circleDataSource = new NumbberWithCircleDataSource(
				SamplePlanIntentService.this);
		dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			planDataSource.open();
			String LastUpdateTime = Utility.getSharedPreferences(
					SamplePlanIntentService.this, Constants.LAST_PLANS_UPDATED);
			if (LastUpdateTime != null) {
				int days = (int) ((System.currentTimeMillis() - Long
						.parseLong(LastUpdateTime)) / (1000 * 60 * 60 * 24));

				if (days >= 2) {
					planDataSource.deletePlan();
					new PlanIntentServiceTask().execute();
				}
			} else {
				new PlanIntentServiceTask().execute();
			}
		} catch (SQLiteException exception) {
			planDataSource.close();
		}
	}

	public class PlanIntentServiceTask extends
			AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			planDataSource.open();
			String time = Utility.getSharedPreferences(SamplePlanIntentService.this,
					Constants.LAST_PLANS_UPDATED);
			
			Date date = new Date();

			String timeStamp = dateFormat.format(date);
			Log.d("Start Time", "Plan Start Time  response " + timeStamp);
			/*
			 * ArrayList<NameValuePair> listValuePair = new
			 * ArrayList<NameValuePair>();
			 * 
			 * listValuePair .add(new BasicNameValuePair("method",
			 * "getPlanDetails")); listValuePair.add(new
			 * BasicNameValuePair("operator", "all")); listValuePair.add(new
			 * BasicNameValuePair("circle", "all")); String response =
			 * RequestClass.getInstance().readPay1RequestForPlan(
			 * PlanIntentService.this, Constants.API_URL, listValuePair);
			 */

			String response = Constants.getPlanData(SamplePlanIntentService.this,
					Constants.API_URL
							+ "method=getPlanDetails&operator=all&circle=all");
			
			Date date2 = new Date();

			String timeStamp2 = dateFormat.format(date2);
			Log.d("Start Time", "Plan Start Time  end response " + timeStamp2);
			try {

				if (!response.startsWith("Error")) {
					String replaced = response.replace("(", "")
							.replace(")", "").replace(";", "");
					
					replaced = replaced.substring(1, replaced.length() - 1);
					//replaced = replaced.substring(0, replaced.length() - 1);
					//Log.d("Plan  ", "Plan  " + response);
					savePlansTodataBase(replaced);
					/*JSONArray array = new JSONArray(replaced);
					//int count = array.length();
					for (int i = 0; i < array.length(); i++) {
						JSONObject jsonObject = array.getJSONObject(i);

						savePlansTodataBase(jsonObject.toString());
					}*/
				}
			} catch (Exception ee) {

			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			try {
				stopSelf();
			} catch (Exception exception) {
				Log.e("Error:", "Error    ");
			}

		}

	}

	private void savePlansTodataBase(String planJson) {
		try {

			String sql = "INSERT INTO " + Pay1SQLiteHelper.TABLE_PLAN
					+ " VALUES (?,?,?,?,?,?,?,?,?,?);";
			SQLiteStatement statement = database.compileStatement(sql);
			database.beginTransaction();
			/*
			 * planDataSource.open(); circleDataSource.open();
			 */
			long time = System.currentTimeMillis();
			JSONObject planJsonObject = new JSONObject(planJson);
			Iterator<String> operatorKey = planJsonObject.keys();
			
			Date date3 = new Date();

			String timeStamp3 = dateFormat.format(date3);
			Log.d("Start Time", "Plan Start Time  before loop " + timeStamp3);
			while (operatorKey.hasNext()) {
				String operKey = operatorKey.next().toString();
				JSONObject jsonObject2 = planJsonObject.getJSONObject(operKey);
				String opr_name = jsonObject2.getString("opr_name");
				// Log.w("Oper_id", "oper   " + operKey + "   opr_name" +
				// opr_name);
				//String prod_code_pay1 = jsonObject2.getString("prod_code_pay1");
				JSONObject circleJsonObject = jsonObject2
						.getJSONObject("circles");
				Iterator<String> circleKey = circleJsonObject.keys();
				while (circleKey.hasNext()) {
					JSONObject circleJsonObject2 = circleJsonObject
							.getJSONObject(circleKey.next().toString());
					String circleName = circleJsonObject2
							.getString("circle_name");
					String circle_id = circleJsonObject2.getString("circle_id");
					JSONObject plansJsonObject = circleJsonObject2
							.getJSONObject("plans");
					Iterator<String> planKey = plansJsonObject.keys();
					while (planKey.hasNext()) {
						String planType = planKey.next().toString();
						JSONArray planArray = plansJsonObject
								.getJSONArray(planType);

						for (int i = 0; i < planArray.length(); i++) {
							JSONObject jsonObject = planArray.getJSONObject(i);
							String plan_desc = jsonObject
									.getString("plan_desc");
							// contentValues.put("plan_desc", plan_desc);
							String plan_amt = jsonObject.getString("plan_amt");
							// contentValues.put("plan_amt", plan_amt);
							String plan_validity = jsonObject
									.getString("plan_validity");

							statement.clearBindings();

							// statement.bindString(1, prod_code_pay1);
							statement.bindString(2, operKey);
							statement.bindString(3, opr_name);
							statement.bindString(4, circle_id);
							statement.bindString(5, circleName);
							statement.bindString(6, planType);
							statement.bindString(7, plan_amt);
							statement.bindString(8, plan_validity);
							statement.bindString(9, plan_desc);
							statement.bindString(10, String.valueOf(time));
							
							statement.execute();

							/*
							 * Log.e("PLAN SYNC", "row inserted  circle  " +
							 * circleName + "  plantype  " + planType);
							 */

							// values=values+
							// "('"+prod_code_pay1+"','"+opr_name+"','"+circle_id+"','"+circleName+"','"+planType+"','"+plan_amt+"','"+plan_amtplan_amtplan_amt+"','"+plan_desc+"','"+time+"'),";
							// String
							// values2="('2','AIRTEL','MP','MADHYA PRADESH','FULL','20','3 Days','GOOD PLAN','2014-20-20')";
							/*
							 * planDataSource.createPlan(prod_code_pay1,
							 * opr_name, circle_id, circleName, planType,
							 * plan_amt, plan_validity, plan_desc, "" + time);
							 */
							
						}

					}

				}
			}
			Utility.setSharedPreference(SamplePlanIntentService.this,
					Constants.LAST_PLANS_UPDATED,
					String.valueOf(time));
			database.setTransactionSuccessful();
			database.endTransaction();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date1 = new Date();

			String timeStamp1 = dateFormat.format(date1);
			
			Log.d("Start Time", "Plan Start Time End   " + timeStamp1);
			
		} catch (JSONException exception) {
			exception.printStackTrace();
			Log.d("text", "planTest  ");
		} catch (SQLException exception) {
			// TODO: handle exception
			Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Er ", e.getMessage());
		} finally {
			planDataSource.close();
			// stopSelf();
		}
	}

}
