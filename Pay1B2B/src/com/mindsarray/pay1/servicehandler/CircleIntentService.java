package com.mindsarray.pay1.servicehandler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.NumbberWithCircleDataSource;
import com.mindsarray.pay1.databasehandler.Pay1SQLiteHelper;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

public class CircleIntentService extends IntentService {
	NumbberWithCircleDataSource circleDataSource;
//	private SQLiteDatabase database;
//	private Pay1SQLiteHelper dbHelper;
	private String jString = "";
	Context mContext;
	private int currentVersionCode;
	public CircleIntentService() {
		super("CircleService");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		mContext=CircleIntentService.this;
//		dbHelper = new Pay1SQLiteHelper(CircleIntentService.this);
//		database = dbHelper.getWritableDatabase();
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		mContext=CircleIntentService.this;
		circleDataSource = new NumbberWithCircleDataSource(
				CircleIntentService.this);
		//new SyncCircleDataTask().execute();
		Log.d("Plan","Plan Start Circle Thread Time loop");
		
//		if(!Utility.getCircleUpdateFlag(mContext) || Utility.getCirlceVersionCode(mContext) != 21){
//			Utility.setSharedPreference(
//					CircleIntentService.this, Constants.LAST_PLANS_UPDATED, Constants.JSON_UPDATE_TIME);
////			String planJsonFromFile=Constants.LoadData("getAllMobileNumber.json",mContext);
////			parseResponse(planJsonFromFile);
//			Utility.setCircleUpdateFlag(mContext, true);
//			try{
//				currentVersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
//				Utility.setCircleVersionCode(mContext, currentVersionCode);
//			}
//			catch(NameNotFoundException e){
//				
//			}
//		}else{
//			callApi();
//		}
		
		if(Utility.getCirlceVersionCode(mContext) != 22){
			Utility.setSharedPreference(
					CircleIntentService.this, Constants.LAST_CIRCLE_UPDATED, "");
			try{
				currentVersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
				Utility.setCircleVersionCode(mContext, currentVersionCode);
			}
			catch(NameNotFoundException e){
				
			}
		}
		callApi();
		
	}

	
	public void callApi(){
		
		
		new Thread(new Runnable() {
			public void run() {
				
				
				//HttpClient httpclient = new DefaultHttpClient();
				//Log.d(TAG,HomeView.webserviceUrl+"applogout");
				String time = Utility.getSharedPreferences(
						CircleIntentService.this, Constants.LAST_CIRCLE_UPDATED);
				String timestamp = Utility.convertMillisToDateTime(time, "yyyy-MM-dd HH:mm:ss");
				/*HttpPost httppost = new HttpPost(Constants.API_URL
						+ "method=getMobileDetails&mobile=all&timestamp" + time);*/
				Log.d("Plan","Plan Start Circle Start Thread Time "+jString);
				
				ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

				listValuePair
						.add(new BasicNameValuePair("method", "getMobileDetails"));
				listValuePair.add(new BasicNameValuePair("mobile", "all"));
				listValuePair.add(new BasicNameValuePair("timestamp", timestamp));
				
				try {
//				    jString = Constants.getPlanData(CircleIntentService.this, Constants.API_URL
//							+ "method=getMobileDetails&mobile=all&timestamp=" + timestamp);
					jString = RequestClass.getInstance()
							.readPay1Request(CircleIntentService.this,
									Constants.API_URL, listValuePair);
					Log.d("Plan","Plan Start Circle Time "+jString);
					
				} catch (Exception e) {
					e.printStackTrace();
					Log.d("Plan","Plan  IOException "+e);
				}
		
				try {
					circleDataSource.open();
					circleDataSource.parseResponse(jString);
					circleDataSource.close();
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
//					circleDataSource.close();
				}
			}

			
		}).start();		
	}
	
//	private void parseResponse(String response) {
//		// TODO Auto-generated method stub
//		try {
//			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			Date date = new Date();
//			String timeStamp = dateFormat.format(date);
//			Log.d("Plan","Plan Start Circle Start Thread Time loop"+timeStamp);
//			
////			  circleDataSource.open();
//				String sql = "INSERT INTO "
//						+ Pay1SQLiteHelper.TABLE_NUMBERANDCIRCLE
//						+ " VALUES (?,?,?,?,?,?,?,?);";
//				SQLiteStatement statement = database.compileStatement(sql);
//				database.beginTransaction();
//				
//				if (!response.startsWith("Error")) {
//					String replaced = response.replace("(", "")
//							.replace(")", "").replace(";", "");
//					//Log.d("Circle  ", "Plan  "+response);
//					replaced = replaced.substring(1, replaced.length() - 1);
//					/*JSONArray array = new JSONArray(replaced);*/
//					JSONObject jsonObject = new JSONObject(replaced);
//					String status = jsonObject.getString("status");
//					if (status.equalsIgnoreCase("success")) {
//						String details = jsonObject.getString("details");
//						JSONArray detailsArray = new JSONArray(details);
//						for (int i = 0; i < detailsArray.length(); i++) {
//							JSONObject object = detailsArray.getJSONObject(i);
//							String areaName = object.getString("area_name");
//							String areaCode = object.getString("area");
//							String operatorName = object.getString("opr_name");
//							String operatorCode = object.getString("operator");
//							String operatorId = object.getString("product_id");
//							String number = object.getString("number");
//
//							
//
//							//statement.bindString(1, "1");
//							statement.bindString(2, areaCode);
//							statement.bindString(3, areaName);
//							statement.bindString(4, operatorCode);
//							statement.bindString(5, operatorName);
//							statement.bindString(6, operatorId);
//							statement.bindString(7, number);
//							statement.bindString(8, timeStamp);
//
//							statement.execute();
//
//							
//							/* circleDataSource
//							 * .createNumberAndCircleList(areaName, areaCode,
//							 * operatorName, operatorCode, operatorId, number,
//							 * timeStamp);
//							 */
//						}
//
//					}
//
//				}
//				Utility.setSharedPreference(
//						CircleIntentService.this,
//						Constants.LAST_CIRCLE_UPDATED,
//						String.valueOf(System.currentTimeMillis()));
//				String currentDateandTime = dateFormat.format(new Date());
//				Log.d("Plan","Plan Start Circle End Thread Time loop"+currentDateandTime);
//				database.setTransactionSuccessful();
//				database.endTransaction();
//			} catch (JSONException je) {
//				Log.d("NAC", "row inserted   " + je.getMessage());
//				je.printStackTrace();
//			} catch (SQLException exception) {
//				Log.d("NAC", "row inserted   " + exception.getMessage());
//				// TODO: handle exception
//				// // Log.e("Er ", exception.getMessage());
//			} catch (Exception e) {
//				Log.d("NAC", "row inserted   " + e.getMessage());
//				// TODO: handle exception
//				// // Log.e("Er ", e.getMessage());
//			} finally {
//				try{
////				circleDataSource.close();
//				}catch(SQLException see){
//					Log.d("NAC", "row inserted   " + see.getMessage());
//				}
//				stopSelf();
//			}
//	}
	
}
	
	/*public class SyncCircleDataTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			dbHelper = new Pay1SQLiteHelper(CircleIntentService.this);
			database = dbHelper.getWritableDatabase();
			circleDataSource = new NumbberWithCircleDataSource(
					CircleIntentService.this);
			String time = Utility.getSharedPreferences(
					CircleIntentService.this, Constants.LAST_CIRCLE_UPDATED);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String currentDateandTime = dateFormat.format(new Date());
			
			String response = Constants.getPlanData(CircleIntentService.this, Constants.API_URL
					+ "method=getMobileDetails&mobile=all&timestamp" + time);
			
			Date date = new Date();
			
			Log.d("Start Time", "Plan Start Circle Time Circle   " + currentDateandTime);
			try {
			  circleDataSource.open();
				String sql = "INSERT INTO "
						+ Pay1SQLiteHelper.TABLE_NUMBERANDCIRCLE
						+ " VALUES (?,?,?,?,?,?,?,?);";
				SQLiteStatement statement = database.compileStatement(sql);
				database.beginTransaction();
				String timeStamp = dateFormat.format(date);
				if (!response.startsWith("Error")) {
					String replaced = response.replace("(", "")
							.replace(")", "").replace(";", "");
					//Log.d("Circle  ", "Plan  "+response);
					replaced = replaced.substring(1, replaced.length() - 1);
					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = new JSONObject(replaced);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						String details = jsonObject.getString("details");
						JSONArray detailsArray = new JSONArray(details);
						for (int i = 0; i < detailsArray.length(); i++) {
							JSONObject object = detailsArray.getJSONObject(i);
							String areaName = object.getString("area_name");
							String areaCode = object.getString("area");
							String operatorName = object.getString("opr_name");
							String operatorCode = object.getString("operator");
							String operatorId = object.getString("product_id");
							String number = object.getString("number");

							

							//statement.bindString(1, "1");
							statement.bindString(2, areaCode);
							statement.bindString(3, areaName);
							statement.bindString(4, operatorCode);
							statement.bindString(5, operatorName);
							statement.bindString(6, operatorId);
							statement.bindString(7, number);
							statement.bindString(8, timeStamp);

							statement.execute();

							
							 circleDataSource
							 * .createNumberAndCircleList(areaName, areaCode,
							 * operatorName, operatorCode, operatorId, number,
							 * timeStamp);
							 
						//}

					}

				}
				Utility.setSharedPreference(
						CircleIntentService.this,
						Constants.LAST_CIRCLE_UPDATED,
						String.valueOf(System.currentTimeMillis()));
				
				
				database.setTransactionSuccessful();
				database.endTransaction();
			} catch (JSONException je) {
				Log.d("NAC", "row inserted   " + je.getMessage());
				je.printStackTrace();
			} catch (SQLException exception) {
				Log.d("NAC", "row inserted   " + exception.getMessage());
				// TODO: handle exception
				// // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				Log.d("NAC", "row inserted   " + e.getMessage());
				// TODO: handle exception
				// // Log.e("Er ", e.getMessage());
			} finally {
				circleDataSource.close();
				stopSelf();
			}
			DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String currentDateandTime1 = dateFormat1.format(new Date());
			Log.d("Start Time", "Plan Start Circle Time End Circle   " + currentDateandTime1);
			return response;

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			try {
				stopSelf();
			} catch (Exception exception) {
				Log.e("Error:", "Error    ");
			}

		}

	}

}
*/