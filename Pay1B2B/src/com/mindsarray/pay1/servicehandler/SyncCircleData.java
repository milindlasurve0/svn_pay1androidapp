package com.mindsarray.pay1.servicehandler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mindsarray.pay1.DataCardActivity;
import com.mindsarray.pay1.MainActivity;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.NumbberWithCircleDataSource;
import com.mindsarray.pay1.databasehandler.NumberWithCircle;
import com.mindsarray.pay1.databasehandler.Pay1SQLiteHelper;
import com.mindsarray.pay1.databasehandler.VMNDataSource;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import android.app.Service;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class SyncCircleData extends Service {

	NumbberWithCircleDataSource circleDataSource;
	private SQLiteDatabase database;
	private Pay1SQLiteHelper dbHelper;
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		dbHelper = new Pay1SQLiteHelper(SyncCircleData.this);
		database = dbHelper.getWritableDatabase();
		circleDataSource = new NumbberWithCircleDataSource(SyncCircleData.this);
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		
		super.onStart(intent, startId);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		
		new SyncCircleDataTask().execute();
		
		//getCircleData();
		return super.onStartCommand(intent, flags, startId);
	}


	@Override
	public boolean onUnbind(Intent intent) {
		// TODO Auto-generated method stub
		return super.onUnbind(intent);
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	public class SyncCircleDataTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			circleDataSource.open();
			String time = Utility.getSharedPreferences(SyncCircleData.this,
					Constants.LAST_CIRCLE_UPDATED);
			
		String response=	Constants.getPlanData(SyncCircleData.this, Constants.API_URL+"method=getMobileDetails&mobile=all&timestamp"+time);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			  
			try {
				String sql = "INSERT INTO "+ Pay1SQLiteHelper.TABLE_NUMBERANDCIRCLE +" VALUES (?,?,?,?,?,?,?,?);";
				SQLiteStatement statement = database.compileStatement(sql);
				String timeStamp = dateFormat.format(date);
				if (!response.startsWith("Error")) {
					String replaced = response.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = array.getJSONObject(0);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						String details = jsonObject.getString("details");
						JSONArray detailsArray = new JSONArray(details);
						for (int i = 0; i < detailsArray.length(); i++) {
							JSONObject object = detailsArray.getJSONObject(i);
							String areaName = object.getString("area_name");
							String areaCode = object.getString("area");
							String operatorName = object.getString("opr_name");
							String operatorCode = object.getString("operator");
							String operatorId = object.getString("product_id");
							String number = object.getString("number");

							Utility.setSharedPreference(SyncCircleData.this,
									Constants.LAST_CIRCLE_UPDATED,String.valueOf(System.currentTimeMillis()));

							statement.bindString(1, "1");
							statement.bindString(2, areaCode);
							statement.bindString(3, areaName);
							statement.bindString(4, operatorCode);
							statement.bindString(5, operatorName);
							statement.bindString(6, operatorId);
							statement.bindString(7, number);
							statement.bindString(8, timeStamp);

							statement.execute();

							/*
							 * circleDataSource
							 * .createNumberAndCircleList(areaName, areaCode,
							 * operatorName, operatorCode, operatorId, number,
							 * timeStamp);
							 */
						}
						
						
						// NumberWithCircle circle=
						// circleDataSource.getNumberDetails("8422057400");
						// Log.d("aaa","ddd"+circle);
					}

				}
				database.setTransactionSuccessful();	
			    database.endTransaction();
			} catch (JSONException je) {
				Log.d("NAC", "row inserted   " + je.getMessage());
				je.printStackTrace();
			} catch (SQLException exception) {
				Log.d("NAC", "row inserted   " + exception.getMessage());
				// TODO: handle exception
				// // Log.e("Er ", exception.getMessage());
			} catch (Exception e) {
				Log.d("NAC", "row inserted   " + e.getMessage());
				// TODO: handle exception
				// // Log.e("Er ", e.getMessage());
			} finally {
				circleDataSource.close();
				stopSelf();
			}
			
			
			
			
			
			
			
			
			
			
			return response;

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
try{
	stopSelf();
}catch(Exception exception){
	Log.e("Error:", "Error    ");
}

		}

	}

}
