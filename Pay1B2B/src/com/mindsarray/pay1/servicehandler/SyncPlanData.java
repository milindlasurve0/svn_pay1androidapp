package com.mindsarray.pay1.servicehandler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Service;
import android.content.Intent;
import android.database.DatabaseUtils.InsertHelper;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.NumbberWithCircleDataSource;
import com.mindsarray.pay1.databasehandler.Pay1SQLiteHelper;
import com.mindsarray.pay1.databasehandler.PlanDataSource;
import com.mindsarray.pay1.utilities.Utility;

public class SyncPlanData extends Service {
	PlanDataSource planDataSource;
	private SQLiteDatabase database;
	private Pay1SQLiteHelper dbHelper;
	NumbberWithCircleDataSource circleDataSource;
	String values = "";

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		dbHelper = new Pay1SQLiteHelper(SyncPlanData.this);
		database = dbHelper.getWritableDatabase();
		planDataSource = new PlanDataSource(SyncPlanData.this);
		circleDataSource = new NumbberWithCircleDataSource(SyncPlanData.this);
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		planDataSource.open();
		String LastUpdateTime = Utility.getSharedPreferences(SyncPlanData.this,
				Constants.LAST_PLANS_UPDATED);
		if (LastUpdateTime.length() > 0) {
			int days = (int) ((System.currentTimeMillis() - Long
					.parseLong(LastUpdateTime)) / (1000 * 60 * 60 * 24));

			if (days >= 2) {
				//planDataSource.deletePlan();
				new SyncPlanDataTask().execute();
			}
		} else {
			new SyncPlanDataTask().execute();
		}
		/*
		 * planDataSource.createPlan(prod_code_pay1, opr_name, circle_id,
		 * circleName, planType, plan_amt, plan_validity, plan_desc, "" + time);
		 * String values1=
		 * "('1','AIRCEL','MU','MUMBAI','TOPUP','20','3','GOOD PLAN','2014-20-20')"
		 * ; String values2=
		 * "('2','AIRTEL','MP','MADHYA PRADESH','FULL','20','3 Days','GOOD PLAN','2014-20-20')"
		 * ;
		 */

		/*
		 * //for(int k=0;k<4;k++){ String sql1 = "insert into " +
		 * Pay1SQLiteHelper.TABLE_PLAN + " (" +
		 * Pay1SQLiteHelper.PLAN_OPERATOR_ID + ", " +
		 * Pay1SQLiteHelper.PLAN_OPERATOR_NAME+ ", " +
		 * Pay1SQLiteHelper.PLAN_CIRCLE_ID+ ", " +
		 * Pay1SQLiteHelper.PLAN_CIRCLE_NAME+ ", " + Pay1SQLiteHelper.PLAN_TYPE+
		 * ", " + Pay1SQLiteHelper.PLAN_AMOUNT+ ", " +
		 * Pay1SQLiteHelper.PLAN_VALIDITY+ ", " +
		 * Pay1SQLiteHelper.PLAN_DESCRIPTION+ ", " +
		 * Pay1SQLiteHelper.PLAN_UPDATE_TIME +
		 * ") values "+values1+","+values2+";";
		 * planDataSource.insertToPlan(sql1);
		 * 
		 * //}
		 */

		return super.onStartCommand(intent, flags, startId);

	}

	public class SyncPlanDataTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			planDataSource.open();
			String time = Utility.getSharedPreferences(SyncPlanData.this,
					Constants.LAST_PLANS_UPDATED);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();

			String timeStamp = dateFormat.format(date);
			Log.d("Start Time", "Start Time   " + timeStamp);
			/*
			 * ArrayList<NameValuePair> listValuePair = new
			 * ArrayList<NameValuePair>();
			 * 
			 * listValuePair .add(new BasicNameValuePair("method",
			 * "getPlanDetails")); listValuePair.add(new
			 * BasicNameValuePair("operator", "all")); listValuePair.add(new
			 * BasicNameValuePair("circle", "all")); String response =
			 * RequestClass.getInstance().readPay1RequestForPlan(
			 * SyncPlanData.this, Constants.API_URL, listValuePair);
			 */

			String response = Constants.getPlanData(SyncPlanData.this,
					Constants.API_URL
							+ "method=getPlanDetails&operator=all&circle=all");

			try {

				if (!response.startsWith("Error")) {
					String replaced = response.replace("(", "")
							.replace(")", "").replace(";", "");

					JSONArray array = new JSONArray(replaced);
					int count = array.length();
					for (int i = 0; i < array.length(); i++) {
						JSONObject jsonObject = array.getJSONObject(i);

						planDataSource.savePlansTodataBase(jsonObject.toString());
					}
				}
			} catch (Exception ee) {

			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			try {
				stopSelf();
			} catch (Exception exception) {
				Log.e("Error:", "Error    ");
			}

		}

	}

	private void savePlansTodataBase(String planJson) {
		try {

			String sql = "INSERT INTO " + Pay1SQLiteHelper.TABLE_PLAN
					+ " VALUES (?,?,?,?,?,?,?,?,?,?);";
			SQLiteStatement statement = database.compileStatement(sql);
			database.beginTransaction();
			/*
			 * planDataSource.open(); circleDataSource.open();
			 */
			long time = System.currentTimeMillis();
			JSONObject planJsonObject = new JSONObject(planJson);
			Iterator<String> operatorKey = planJsonObject.keys();
			while (operatorKey.hasNext()) {
				String operKey = operatorKey.next().toString();
				JSONObject jsonObject2 = planJsonObject.getJSONObject(operKey);
				String opr_name = jsonObject2.getString("opr_name");
				Log.w("Oper_id", "oper   " + operKey + "   opr_name" + opr_name);
				String prod_code_pay1 = jsonObject2.getString("prod_code_pay1");
				JSONObject circleJsonObject = jsonObject2
						.getJSONObject("circles");
				Iterator<String> circleKey = circleJsonObject.keys();
				while (circleKey.hasNext()) {
					JSONObject circleJsonObject2 = circleJsonObject
							.getJSONObject(circleKey.next().toString());
					String circleName = circleJsonObject2
							.getString("circle_name");
					String circle_id = circleJsonObject2.getString("circle_id");
					JSONObject plansJsonObject = circleJsonObject2
							.getJSONObject("plans");
					Iterator<String> planKey = plansJsonObject.keys();
					while (planKey.hasNext()) {
						String planType = planKey.next().toString();
						JSONArray planArray = plansJsonObject
								.getJSONArray(planType);

						for (int i = 0; i < planArray.length(); i++) {
							JSONObject jsonObject = planArray.getJSONObject(i);
							String plan_desc = jsonObject
									.getString("plan_desc");
							// contentValues.put("plan_desc", plan_desc);
							String plan_amt = jsonObject.getString("plan_amt");
							// contentValues.put("plan_amt", plan_amt);
							String plan_validity = jsonObject
									.getString("plan_validity");
							InsertHelper ih = new InsertHelper(this.database,
									Pay1SQLiteHelper.TABLE_PLAN);
							final int planAmount = ih
									.getColumnIndex(Pay1SQLiteHelper.PLAN_AMOUNT);
							final int planDesc = ih
									.getColumnIndex(Pay1SQLiteHelper.PLAN_DESCRIPTION);
							final int planValidity = ih
									.getColumnIndex(Pay1SQLiteHelper.PLAN_VALIDITY);
							final int prodCode = ih
									.getColumnIndex(Pay1SQLiteHelper.PLAN_OPERATOR_ID);
							final int operName = ih
									.getColumnIndex(Pay1SQLiteHelper.PLAN_OPERATOR_NAME);
							final int circleId = ih
									.getColumnIndex(Pay1SQLiteHelper.PLAN_CIRCLE_ID);
							final int circleN = ih
									.getColumnIndex(Pay1SQLiteHelper.PLAN_CIRCLE_NAME);
							final int updateTime = ih
									.getColumnIndex(Pay1SQLiteHelper.PLAN_UPDATE_TIME);
							final int planTypeInt = ih
									.getColumnIndex(Pay1SQLiteHelper.PLAN_TYPE);
							final int planId = ih
									.getColumnIndex(Pay1SQLiteHelper.PLAN_ID);

							statement.clearBindings();

							/*
							 * statement.bindString(planId, prod_code_pay1+1+i);
							 * statement.bindString(prodCode, prod_code_pay1);
							 * statement.bindString(operName, opr_name);
							 * statement.bindString(circleId, circle_id);
							 * statement.bindString(circleN, circleName);
							 * statement.bindString(planTypeInt, planType);
							 * statement.bindString(planAmount, plan_amt);
							 * statement.bindString(planValidity,
							 * plan_validity); statement.bindString(planDesc,
							 * plan_desc); statement.bindString(updateTime,
							 * String.valueOf(time)); statement.execute();
							 */

							// statement.bindString(1, prod_code_pay1);
							statement.bindString(2, prod_code_pay1);
							statement.bindString(3, opr_name);
							statement.bindString(4, circle_id);
							statement.bindString(5, circleName);
							statement.bindString(6, planType);
							statement.bindString(7, plan_amt);
							statement.bindString(8, plan_validity);
							statement.bindString(9, plan_desc);
							statement.bindString(10, String.valueOf(time));
							statement.execute();

							/*
							 * Log.e("PLAN SYNC", "row inserted  circle  " +
							 * circleName + "  plantype  " + planType);
							 */

							// values=values+
							// "('"+prod_code_pay1+"','"+opr_name+"','"+circle_id+"','"+circleName+"','"+planType+"','"+plan_amt+"','"+plan_amtplan_amtplan_amt+"','"+plan_desc+"','"+time+"'),";
							// String
							// values2="('2','AIRTEL','MP','MADHYA PRADESH','FULL','20','3 Days','GOOD PLAN','2014-20-20')";
							/*
							 * planDataSource.createPlan(prod_code_pay1,
							 * opr_name, circle_id, circleName, planType,
							 * plan_amt, plan_validity, plan_desc, "" + time);
							 */
							Utility.setSharedPreference(SyncPlanData.this,
									Constants.LAST_PLANS_UPDATED,
									String.valueOf(time));
						}

					}

				}
			}
			database.setTransactionSuccessful();
			database.endTransaction();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date1 = new Date();

			String timeStamp1 = dateFormat.format(date1);
			Log.d("End Time", "Start Time End  " + timeStamp1);
			int listCount = planDataSource.getAllPlan().size();
			Log.e("Rows inser", "Rows inser   " + listCount);

		} catch (JSONException exception) {
			exception.printStackTrace();
			Log.d("text", "planTest  ");
		} catch (SQLException exception) {
			// TODO: handle exception
			Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Er ", e.getMessage());
		} finally {
			planDataSource.close();
			// stopSelf();
		}
	}
}
