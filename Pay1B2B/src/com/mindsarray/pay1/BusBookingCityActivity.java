package com.mindsarray.pay1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.mindsarray.pay1.adapterhandler.BusBookingCityAdapter;
import com.mindsarray.pay1.constant.Constants;

public class BusBookingCityActivity extends Activity {

	private EditText editText_SelectCity;
	private ListView listView_Cities;
	private ImageView imageView_Clear;

	private BusBookingCityAdapter adapter;
	ArrayList<HashMap<String, String>> city_list = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> city_list_temp = new ArrayList<HashMap<String, String>>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.busbookingcity_activity);

		try {

			city_list.addAll((ArrayList<HashMap<String, String>>) getIntent()
					.getSerializableExtra("CITY_LIST"));

			imageView_Clear = (ImageView) findViewById(R.id.imageView_Clear);
			imageView_Clear.setVisibility(View.GONE);
			imageView_Clear.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					editText_SelectCity.setText("");
					editText_SelectCity.requestFocus();
				}
			});

			editText_SelectCity = (EditText) findViewById(R.id.editText_SelectCity);
			editText_SelectCity.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (editText_SelectCity.getText().toString().trim()
							.equalsIgnoreCase(""))
						imageView_Clear.setVisibility(View.GONE);
					else
						imageView_Clear.setVisibility(View.VISIBLE);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					try {
						String keyToSearch = editText_SelectCity.getText()
								.toString().trim();

						if (!keyToSearch.equalsIgnoreCase("")) {
							city_list_temp.clear();
							for (Map<String, String> map : city_list) {

								if (map.get(Constants.BUS_CITY_NAME)
										.toLowerCase()
										.startsWith(keyToSearch.toLowerCase())) {
									// // Log.e("VAl ", "Found : " + key
									// + " / value : " + map.values());

									// HashMap
									HashMap<String, String> map1 = new HashMap<String, String>();

									map1.put(Constants.BUS_CITY_NAME,
											map.get(Constants.BUS_CITY_NAME));
									map1.put(Constants.BUS_CITY_ID,
											map.get(Constants.BUS_CITY_ID));
									city_list_temp.add(map1);
								}
							}
							BusBookingCityActivity.this
									.runOnUiThread(new Runnable() {

										public void run() {
											// TODO Auto-generated method
											// stub
											adapter.notifyDataSetChanged();
										}
									});
						} else {
							city_list_temp.clear();
							city_list_temp.addAll(city_list);
							adapter.notifyDataSetChanged();
						}
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			});

			listView_Cities = (ListView) findViewById(R.id.listView_Cities);
			// Create adapter
			city_list_temp.addAll(city_list);
			adapter = new BusBookingCityAdapter(BusBookingCityActivity.this,
					city_list_temp);

			// Set adapter to AutoCompleteTextView
			listView_Cities.setAdapter(adapter);
			listView_Cities
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							Intent returnIntent = new Intent();
							HashMap<String, String> map = new HashMap<String, String>();
							map = city_list_temp.get(position);
							returnIntent.putExtra(Constants.BUS_CITY_NAME, ""
									+ map.get(Constants.BUS_CITY_NAME));
							returnIntent.putExtra(Constants.BUS_CITY_ID, ""
									+ map.get(Constants.BUS_CITY_ID));
							setResult(RESULT_OK, returnIntent);
							finish();
						}
					});
		} catch (Exception exception) {

		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

}
