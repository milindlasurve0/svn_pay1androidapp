package com.mindsarray.pay1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;

public class BusBookingSeatActivity extends Activity {

	private String res = null, source = null, source_id = null,
			destination = null, destination_id = null, date = null,
			bus_id = null, bus_name = null, bus_time = null, bus_type = null,
			bus_fare = null, ac = null, nonac = null, sleeper = null,
			nonsleeper = null, arrival_time = null, departure_time = null,
			boarding_point_name = null, boarding_point_id = null,
			boarding_point_time = null, cancellation_policy = null;
	int seat_count = 0;
	private LinearLayout linearLayout_Boarding, linearLayout_Seat;
	private TextView textView_Route, textView_Date, textView_Boarding,
			textView_SelectedSeats, textView_BusName, textView_Time,
			textView_BusType, textView_Fare, textView_AC, textView_NonAC,
			textView_Sleeper, textView_NonSleeper, textView_ArrivalTime,
			textView_DepartureTime;
	private Button button_Book;

	String months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
			"Sep", "Oct", "Nov", "Dec" };

	ArrayList<HashMap<String, String>> boardingpoints_list = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> seat_list = new ArrayList<HashMap<String, String>>();

	static final int BOARDING_POINT_PICKER_ID = 1;
	static final int SEAT_PICKER_ID = 2;
	private static final String TAG = "Bus Booking";

	private GetSeatTask getSeatTask;
	private GetBoardingPoints getBoardingPoints;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.busbookingseat_activity);

		try {
			Bundle extras = getIntent().getExtras();
			if (extras != null) {
				res = extras.getString("RESULT");
				source = extras.getString("SOURCE");
				source_id = extras.getString("SOURCE_ID");
				destination = extras.getString("DESTINATION");
				destination_id = extras.getString("DESTINATION_ID");
				date = extras.getString("DATE");
				// seats = extras.getString("SEATS");
				bus_id = extras.getString(Constants.BUS_ID);
				bus_name = extras.getString(Constants.BUS_NAME);
				bus_time = extras.getString(Constants.BUS_TIME);
				bus_type = extras.getString(Constants.BUS_TYPE);
				bus_fare = extras.getString(Constants.BUS_FARE);
				ac = extras.getString(Constants.BUS_AC);
				nonac = extras.getString(Constants.BUS_NONAC);
				sleeper = extras.getString(Constants.BUS_SLEEPER);
				nonsleeper = extras.getString(Constants.BUS_NONSLEEPER);
				arrival_time = extras.getString(Constants.BUS_ARRIVAL_TIME);
				departure_time = extras.getString(Constants.BUS_DEPARTURE_TIME);
				cancellation_policy = extras
						.getString(Constants.BUS_CANCELLATION_POLICY);
			}

			textView_Route = (TextView) findViewById(R.id.textView_Route);
			textView_Route.setText(source + " to " + destination);
			// textView_Seats = (TextView) findViewById(R.id.textView_Seats);
			// textView_Seats.setText("Number of Travelers : " + seats);
			textView_Date = (TextView) findViewById(R.id.textView_Date);
			textView_Date.setText("Date of Travel : " + date);
			textView_BusName = (TextView) findViewById(R.id.textView_BusName);
			textView_BusName.setText(bus_name);
			textView_Time = (TextView) findViewById(R.id.textView_Time);
			textView_Time.setText(bus_time);
			textView_BusType = (TextView) findViewById(R.id.textView_BusType);
			textView_BusType.setText(bus_type);
			textView_Fare = (TextView) findViewById(R.id.textView_Fare);
			textView_Fare.setText(bus_fare);
			textView_AC = (TextView) findViewById(R.id.textView_AC);
			textView_AC.setText(ac);
			textView_NonAC = (TextView) findViewById(R.id.textView_NonAC);
			textView_NonAC.setText(nonac);
			textView_Sleeper = (TextView) findViewById(R.id.textView_Sleeper);
			textView_Sleeper.setText(sleeper);
			textView_NonSleeper = (TextView) findViewById(R.id.textView_NonSleeper);
			textView_NonSleeper.setText(nonsleeper);
			textView_ArrivalTime = (TextView) findViewById(R.id.textView_ArrivalTime);
			textView_ArrivalTime.setText(arrival_time);
			textView_DepartureTime = (TextView) findViewById(R.id.textView_DepartureTime);
			textView_DepartureTime.setText(departure_time);
			textView_Boarding = (TextView) findViewById(R.id.textView_Boarding);
			textView_SelectedSeats = (TextView) findViewById(R.id.textView_SelectedSeats);
			linearLayout_Boarding = (LinearLayout) findViewById(R.id.linearLayout_Boarding);
			linearLayout_Boarding
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(
									BusBookingSeatActivity.this,
									BusBookingBoardingPointActivity.class);
							intent.putExtra("BOARDING_POINT_LIST",
									boardingpoints_list);
							startActivityForResult(intent,
									BOARDING_POINT_PICKER_ID);
						}
					});

			linearLayout_Seat = (LinearLayout) findViewById(R.id.linearLayout_Seat);
			linearLayout_Seat.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(BusBookingSeatActivity.this,
							BusBookingSeatSelectorActivity.class);
					intent.putExtra("SEAT_LIST", seat_list);
					// intent.putExtra("SEATS", seats);
					intent.putExtra(Constants.BUS_SLEEPER, sleeper);
					startActivityForResult(intent, SEAT_PICKER_ID);
				}
			});

			button_Book = (Button) findViewById(R.id.button_Book);
			button_Book.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (textView_Boarding.getText().toString().trim()
							.equalsIgnoreCase("")) {
						Constants.showOneButtonFailureDialog(
								BusBookingSeatActivity.this,
								"Please select boarding point.", TAG,
								Constants.OTHER_ERROR);
					} else if (textView_SelectedSeats.getText().toString()
							.trim().equalsIgnoreCase("")) {
						Constants.showOneButtonFailureDialog(
								BusBookingSeatActivity.this,
								"Please select seats.", TAG,
								Constants.OTHER_ERROR);
					} else {
						Intent intent = new Intent(BusBookingSeatActivity.this,
								BusBookingContactInformationActivity.class);
						intent.putExtra("SOURCE", source);
						intent.putExtra("SOURCE_ID", source_id);
						intent.putExtra("DESTINATION", destination);
						intent.putExtra("DESTINATION_ID", destination_id);
						intent.putExtra("DATE", date);
						intent.putExtra("SEATS",
								textView_SelectedSeats.getText());
						intent.putExtra("SEATS_COUNT", seat_count);
						intent.putExtra(Constants.BUS_ID, bus_id);
						intent.putExtra(Constants.BUS_NAME, bus_name);
						intent.putExtra(Constants.BUS_TIME, bus_time);
						intent.putExtra(Constants.BUS_FARE, bus_fare);
						intent.putExtra(Constants.BUS_TYPE, bus_type);
						intent.putExtra(Constants.BUS_BOARDING_POINT_ID,
								boarding_point_id);
						intent.putExtra(Constants.BUS_BOARDING_POINT_NAME,
								boarding_point_name);
						intent.putExtra(Constants.BUS_BOARDING_POINT_TIME,
								boarding_point_time);
						intent.putExtra(Constants.BUS_CANCELLATION_POLICY,
								cancellation_policy);
						startActivity(intent);
					}
				}
			});

			// getBoardingPoints = new GetBoardingPoints();
			// getBoardingPoints.execute();
			Log.w("Bus ID", bus_id);
			getSeatTask = new GetSeatTask();
			getSeatTask.execute(new String[] { bus_id });
		} catch (Exception exception) {

		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == BOARDING_POINT_PICKER_ID) {
			if (resultCode == RESULT_OK) {
				boarding_point_id = data
						.getStringExtra(Constants.BUS_BOARDING_POINT_ID);
				boarding_point_name = data
						.getStringExtra(Constants.BUS_BOARDING_POINT_NAME);
				boarding_point_time = data
						.getStringExtra(Constants.BUS_BOARDING_POINT_TIME);
				textView_Boarding.setText(boarding_point_name + " ("
						+ boarding_point_time + ")");

			}
		}
		if (requestCode == SEAT_PICKER_ID) {
			if (resultCode == RESULT_OK) {
				textView_SelectedSeats.setText(data.getStringExtra("SEATS"));
				seat_count = data.getIntExtra("SEATS_COUNT", 0);
			}
		}

		// if (resultCode == RESULT_CANCELED) {
		// mBtnLogin.setVisibility(View.GONE);
		// mBtnLogOut.setVisibility(View.VISIBLE);
		//
		// }
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants.showNavigationBar(BusBookingSeatActivity.this);
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	public class GetSeatTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			try {
				// Date date = new Date(params[3]);
				//
				// Calendar c = Calendar.getInstance();
				// c.setTime(date);

				// String response = RequestClass
				// .getInstance()
				// .readPay1Request(
				// BusBookingSeatActivity.this,
				// Constants.BUS_API_URL
				// +
				// "method=busBooking&action=getBoardingDropping&bord=B&busid="
				// + params[0]
				// + "&source="
				// + params[1]
				// + "&destination="
				// + params[2]
				// + "&doj="
				// + new StringBuilder()
				// .append(c.get(Calendar.YEAR))
				// .append("-")
				// .append((c.get(Calendar.MONTH) + 1))
				// .append("-")
				// .append(c
				// .get(Calendar.DAY_OF_MONTH)));

				// ArrayList<NameValuePair> listValuePair = new
				// ArrayList<NameValuePair>();
				//
				// listValuePair
				// .add(new BasicNameValuePair("method", "busBooking"));
				// listValuePair.add(new BasicNameValuePair("action",
				// "getBoardingDropping"));
				// listValuePair.add(new BasicNameValuePair("bord", "B"));
				// listValuePair.add(new BasicNameValuePair("busid",
				// params[0]));
				// listValuePair.add(new BasicNameValuePair("source",
				// params[1]));
				// listValuePair.add(new BasicNameValuePair("destination",
				// params[2]));
				// listValuePair.add(new BasicNameValuePair("doj",
				// new StringBuilder().append(c.get(Calendar.YEAR))
				// .append("-")
				// .append((c.get(Calendar.MONTH) + 1))
				// .append("-")
				// .append(c.get(Calendar.DAY_OF_MONTH))
				// .toString()));

				ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

				listValuePair
						.add(new BasicNameValuePair("method", "busBooking"));
				listValuePair.add(new BasicNameValuePair("action",
						"getAvailableSeats"));
				listValuePair.add(new BasicNameValuePair("busid", params[0]));

				String response = RequestClass.getInstance().readPay1Request(
						BusBookingSeatActivity.this, Constants.API_URL,
						listValuePair);
				return response;
			} catch (Exception exception) {
				return null;
			}

		}

		@Override
		protected void onPostExecute(String result) {
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			super.onPostExecute(result);
			try {
				if (!result.startsWith("Error")) {
					seat_list.clear();
					JSONObject jsonObjectResult = new JSONObject(result);
					String status = jsonObjectResult.getString("status");
					if (status.equalsIgnoreCase("SUCCESS")) {

						// JSONArray jsonArrayBoardingTime = jsonObjectResult
						// .getJSONArray("boardingTimes");
						//
						// for (int j = 0; j < jsonArrayBoardingTime.length();
						// j++) {
						// JSONArray jsonArray = jsonArrayBoardingTime
						// .getJSONArray(j);
						// for (int k = 0; k < jsonArray.length(); k++) {
						// HashMap<String, String> map = new HashMap<String,
						// String>();
						//
						// map.put(Constants.BUS_BOARDING_POINT_ID,
						// jsonArray.getJSONObject(k)
						// .getString("bpId").toString()
						// .trim());
						// map.put(Constants.BUS_BOARDING_POINT_NAME,
						// jsonArray.getJSONObject(k)
						// .getString("location")
						// .toString().trim());
						//
						// map.put(Constants.BUS_BOARDING_POINT_TIME,
						// jsonArray.getJSONObject(k)
						// .getString("time").toString()
						// .trim());
						//
						// boardingpoints_list.add(map);
						// }
						// }

						JSONObject jsonObjecttripDetails = jsonObjectResult
								.getJSONObject("tripDetails");

						// for (int j = 0; j < jsonObjecttripDetails.length();
						// j++)
						// {
						JSONArray jsonArray = jsonObjecttripDetails
								.getJSONArray("seats");
						for (int k = 0; k < jsonArray.length(); k++) {
							HashMap<String, String> map = new HashMap<String, String>();

							map.put(Constants.BUS_SEAT_ROW_LENGTH, jsonArray
									.getJSONObject(k).getString("width")
									.toString().trim());
							map.put(Constants.BUS_SEAT_COLUMN_LENGTH, jsonArray
									.getJSONObject(k).getString("length")
									.toString().trim());
							map.put(Constants.BUS_SEAT_ROW, jsonArray
									.getJSONObject(k).getString("row")
									.toString().trim());
							map.put(Constants.BUS_SEAT_COLUMN, jsonArray
									.getJSONObject(k).getString("column")
									.toString().trim());
							map.put(Constants.BUS_SEAT_AVAILABLE, jsonArray
									.getJSONObject(k).getString("available")
									.toString().trim());
							map.put(Constants.BUS_SEAT_LADIES, jsonArray
									.getJSONObject(k).getString("ladiesSeat")
									.toString().trim());
							map.put(Constants.BUS_SEAT_NAME, jsonArray
									.getJSONObject(k).getString("name")
									.toString().trim());
							map.put(Constants.BUS_SEAT_ZINDEX, jsonArray
									.getJSONObject(k).getString("zIndex")
									.toString().trim());

							seat_list.add(map);
						}
						// }
					} else {
						Constants.showOneButtonFailureDialog(
								BusBookingSeatActivity.this,
								Constants.checkCode(result), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(
							BusBookingSeatActivity.this,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(
						BusBookingSeatActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(
						BusBookingSeatActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(BusBookingSeatActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							GetSeatTask.this.cancel(true);
							finish();
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			GetSeatTask.this.cancel(true);
			dialog.cancel();
			finish();
		}

	}

	public class GetBoardingPoints extends AsyncTask<String, String, String>
			implements OnDismissListener {
		ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			return res;
		}

		@Override
		protected void onPostExecute(String result) {
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			super.onPostExecute(result);
			try {
				if (!result.startsWith("Error")) {
					boardingpoints_list.clear();
					JSONObject jsonObjectResult = new JSONObject(result);
					String status = jsonObjectResult.getString("status");
					if (status.equalsIgnoreCase("SUCCESS")) {

						JSONArray jsonArrayAvailableTrips = jsonObjectResult
								.getJSONArray("availableTrips");

						for (int i = 0; i < jsonArrayAvailableTrips.length(); i++) {

							if (jsonArrayAvailableTrips.getJSONObject(i)
									.getString("id").toString().trim()
									.equalsIgnoreCase(bus_id)) {
								JSONArray jsonArrayBoardingTime = jsonArrayAvailableTrips
										.getJSONObject(i).getJSONArray(
												"boardingTimes");

								for (int j = 0; j < jsonArrayBoardingTime
										.length(); j++) {

									HashMap<String, String> map = new HashMap<String, String>();

									map.put(Constants.BUS_BOARDING_POINT_ID,
											jsonArrayBoardingTime
													.getJSONObject(j)
													.getString("bpId")
													.toString().trim());
									map.put(Constants.BUS_BOARDING_POINT_NAME,
											jsonArrayBoardingTime
													.getJSONObject(j)
													.getString("location")
													.toString().trim());

									map.put(Constants.BUS_BOARDING_POINT_TIME,
											jsonArrayBoardingTime
													.getJSONObject(j)
													.getString("time")
													.toString().trim());
									boardingpoints_list.add(map);
								}
							}
						}

					} else {
						Constants.showOneButtonFailureDialog(
								BusBookingSeatActivity.this,
								Constants.checkCode(result), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(
							BusBookingSeatActivity.this,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(
						BusBookingSeatActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(
						BusBookingSeatActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(BusBookingSeatActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							GetBoardingPoints.this.cancel(true);
							finish();
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			GetBoardingPoints.this.cancel(true);
			dialog.cancel();
			finish();
		}

	}
}
