package com.mindsarray.pay1;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

public class SettingsUpdateMobileActivity extends Activity {
	EditText editExistMobile;
	Button btnUpdatePin;
	String newMobile;
	private static final String TAG = "Update mobile Activity";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.settingsupdatemobile_activity);
		findViewById();
		 tracker = Constants.setAnalyticsTracker(this, tracker);
		btnUpdatePin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				newMobile = editExistMobile.getText().toString();
				if (newMobile.length() != 10) {
					// Constants.showCustomToast(
					// SettingsUpdateMobileActivity.this,
					// R.string.enter_correct_mobile_number_ + "");
				} else {
					new UpdatMobileTask().execute();
				}
			}
		});
	}
	public static GoogleAnalytics analytics; public static Tracker tracker;	
	@Override
		public void onStart() {
			super.onStart();
			// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
			// EasyTracker.getInstance(this).activityStart(this);
			//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
		}
	public class UpdatMobileTask extends AsyncTask<String, String, String>
			implements OnDismissListener {

		private ProgressDialog dialog;

		/*
		 * method updateMobile mobileNumber 8819079381
		 */
		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair.add(new BasicNameValuePair("method", "updateMobile"));
			listValuePair.add(new BasicNameValuePair("mobileNumber",
					editExistMobile.getText().toString()));
			listValuePair.add(new BasicNameValuePair("device_type", "android"));
			String response = RequestClass.getInstance().readPay1Request(
					SettingsUpdateMobileActivity.this, Constants.API_URL,
					listValuePair);

			listValuePair.clear();
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			if (!result.startsWith("Error")) {
				String replaced = result.replace("(", "").replace(")", "")
						.replace(";", "");
				try {
					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = array.getJSONObject(0);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						// Constants.showCustomToast(
						// SettingsUpdateMobileActivity.this,
						// "Request Sent Successfully");
					} else {
						Constants.showOneButtonFailureDialog(
								SettingsUpdateMobileActivity.this,
								Constants.checkCode(result), "Login",
								Constants.RECHARGE_SETTINGS);
					}

				} catch (JSONException e) {
					// e.printStackTrace();
					// Constants.showCustomToast(
					// SettingsUpdateMobileActivity.this, result);
				} catch (Exception e) {
					// TODO: handle exception
					// Constants.showCustomToast(
					// SettingsUpdateMobileActivity.this, result);
				}
			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			dialog = new ProgressDialog(SettingsUpdateMobileActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							UpdatMobileTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			UpdatMobileTask.this.cancel(true);
			dialog.cancel();
		}

	}

	private void findViewById() {
		editExistMobile = (EditText) findViewById(R.id.editExistMobile);
		btnUpdatePin = (Button) findViewById(R.id.btnUpdatePin);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		mBtnLogin.setVisibility(View.GONE);
		mBtnLogOut.setVisibility(View.VISIBLE);
		textBalance.setText("Balance: Rs."
				+ Utility.getBalance(SettingsUpdateMobileActivity.this,
						Constants.SHAREDPREFERENCE_BALANCE));
	}

	TextView textBalance;
	Button mBtnLogin, mBtnLogOut, mBtnHome, mBtnHelp;

	@Override
	protected void onResume() {
		Constants.showNavigationBar(SettingsUpdateMobileActivity.this);
		super.onResume();
	}

}
