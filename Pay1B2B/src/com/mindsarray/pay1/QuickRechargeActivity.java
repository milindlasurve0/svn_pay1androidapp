package com.mindsarray.pay1;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Selection;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.QuickRechargeAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.NotificationDataSource;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.GridMenuItem;
import com.mindsarray.pay1.utilities.Utility;

public class QuickRechargeActivity extends Activity {
	TextView textExampleCode, textOperator;
	// Declare

	ArrayList<NameValuePair> listValuePair;
	ArrayList<GridMenuItem> gridArray = new ArrayList<GridMenuItem>();
	QuickRechargeAdapter mSideBarAdapter;
	private LinearLayout slidingPanel;
	private boolean isExpanded;
	private DisplayMetrics metrics;
	private ListView /* listView, */listViewSide;
	private RelativeLayout headerPanel;
	private RelativeLayout menuPanel;
	private int panelWidth;
	private Button menuViewButton;

	FrameLayout.LayoutParams menuPanelParameters;
	FrameLayout.LayoutParams slidingPanelParameters;
	LinearLayout.LayoutParams headerPanelParameters;
	LinearLayout.LayoutParams listViewParameters;
	Button btnRechargeCode;
	EditText editRechargeCode;
	String[] split;
	String stv;
	String method;
	int positionList;
	boolean bNetOrSms;
	private SharedPreferences mSettingsData;// Shared preferences for setting
	private static final String TAG = "Quick Recharge Activity";								// data

	String product_id = "", sub_id = "", mobile_no = "", amount = "";
	long timestamp;
	String uuid;
	NotificationDataSource notificationDataSource;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.quickrecharge_activity);
		 tracker = Constants.setAnalyticsTracker(this, tracker);
		 //easyTracker.set(Fields.SCREEN_NAME, TAG);
		notificationDataSource = new NotificationDataSource(
				QuickRechargeActivity.this);

		try {
			TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
			uuid = tManager.getDeviceId();
			if (uuid == null) {
				uuid = Secure.getString(
						QuickRechargeActivity.this.getContentResolver(),
						Secure.ANDROID_ID);
			}
			if (Utility.getUUID(QuickRechargeActivity.this,
					Constants.SHAREDPREFERENCE_UUID) == null)
				Utility.setUUID(QuickRechargeActivity.this,
						Constants.SHAREDPREFERENCE_UUID, uuid);
		} catch (Exception exception) {
		}

		// Initialize
		metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		panelWidth = (int) ((metrics.widthPixels) * 0.22);
		listViewSide = (ListView) findViewById(R.id.listViewSide);
		headerPanel = (RelativeLayout) findViewById(R.id.header);
		headerPanelParameters = (LinearLayout.LayoutParams) headerPanel
				.getLayoutParams();
		headerPanelParameters.width = metrics.widthPixels;
		headerPanel.setLayoutParams(headerPanelParameters);
		listValuePair = new ArrayList<NameValuePair>();
		menuPanel = (RelativeLayout) findViewById(R.id.menuPanel);
		menuPanelParameters = (FrameLayout.LayoutParams) menuPanel
				.getLayoutParams();
		menuPanelParameters.width = panelWidth;
		menuPanel.setLayoutParams(menuPanelParameters);

		slidingPanel = (LinearLayout) findViewById(R.id.slidingPanel);
		slidingPanelParameters = (FrameLayout.LayoutParams) slidingPanel
				.getLayoutParams();
		slidingPanelParameters.width = metrics.widthPixels;
		slidingPanel.setLayoutParams(slidingPanelParameters);

		textExampleCode = (TextView) findViewById(R.id.textExampleCode);
		textOperator = (TextView) findViewById(R.id.textOperator);

		mSettingsData = getSharedPreferences(SettingsSmsActivity.SETTINGS_NAME,
				0);
		bNetOrSms = mSettingsData.getBoolean(SettingsSmsActivity.KEY_NETWORK,
				true);

		/*** Bitmap and Name for operators ***/

		int mAirtel = R.drawable.m_airtel;
		int mVodafone = R.drawable.m_vodafone;
		int mBsnl = R.drawable.m_bsnl;
		int mIdea = R.drawable.m_idea;
		int mAircel = R.drawable.m_aircel;
		int mRelainceCdma = R.drawable.m_reliance_cdma;
		int mRelainceGsm = R.drawable.m_reliance;
		//int mLoop = R.drawable.m_loop;
		int mUninor = R.drawable.m_uninor;
		int mTataDocomo = R.drawable.m_docomo;
		int mTataIndicom = R.drawable.m_indicom;
		int mMTS = R.drawable.m_mts;
		int mVideocon = R.drawable.m_videocon;
		int mMTNL = R.drawable.m_mtnl;

		int mDishDth = R.drawable.d_dishtv;
		int mTataDth = R.drawable.d_tatasky;
		int mSunDth = R.drawable.d_sundirect;
		int mBigDth = R.drawable.d_bigtv;
		int mAirtelDth = R.drawable.m_airtel;
		int mVideoconDth = R.drawable.d_videocon;

		gridArray.add(new GridMenuItem(mAirtel, "Airtel",
				Constants.OPERATOR_AIRTEL, Constants.RECHARGE_MOBILE));
		gridArray.add(new GridMenuItem(mVodafone, "Vodafone",
				Constants.OPERATOR_VODAFONE, Constants.RECHARGE_MOBILE));
		gridArray.add(new GridMenuItem(mBsnl, "BSNL", Constants.OPERATOR_BSNL,
				Constants.RECHARGE_MOBILE));
		gridArray.add(new GridMenuItem(mIdea, "Idea", Constants.OPERATOR_IDEA,
				Constants.RECHARGE_MOBILE));
		gridArray.add(new GridMenuItem(mAircel, "Aircel",
				Constants.OPERATOR_AIRCEL, Constants.RECHARGE_MOBILE));
		gridArray.add(new GridMenuItem(mRelainceCdma, "Reliance CDMA",
				Constants.OPERATOR_RELIANCE_CDMA, Constants.RECHARGE_MOBILE));
		gridArray.add(new GridMenuItem(mRelainceGsm, "Reliance GSM",
				Constants.OPERATOR_RELIANCE_GSM, Constants.RECHARGE_MOBILE));
		/*gridArray.add(new GridMenuItem(mLoop, "Loop", Constants.OPERATOR_LOOP,
				Constants.RECHARGE_MOBILE));*/
		gridArray.add(new GridMenuItem(mUninor, "Uninor",
				Constants.OPERATOR_UNINOR, Constants.RECHARGE_MOBILE));
		gridArray.add(new GridMenuItem(mTataDocomo, "Tata Docomo",
				Constants.OPERATOR_DOCOMO, Constants.RECHARGE_MOBILE));
		gridArray.add(new GridMenuItem(mTataIndicom, "Tata Indicom",
				Constants.OPERATOR_TATA_INDICOM, Constants.RECHARGE_MOBILE));
		gridArray.add(new GridMenuItem(mMTS, "MTS", Constants.OPERATOR_MTS,
				Constants.RECHARGE_MOBILE));
		gridArray.add(new GridMenuItem(mVideocon, "Videocon",
				Constants.OPERATOR_VIDEOCON, Constants.RECHARGE_MOBILE));
		gridArray.add(new GridMenuItem(mMTNL, "MTNL", Constants.OPERATOR_MTNL,
				Constants.RECHARGE_MOBILE));

		gridArray.add(new GridMenuItem(mDishDth, "Dish TV",
				Constants.OPERATOR_DTH_DISHTV, Constants.RECHARGE_DTH));
		gridArray.add(new GridMenuItem(mTataDth, "Tata Sky",
				Constants.OPERATOR_DTH_TATA_SKY, Constants.RECHARGE_DTH));
		gridArray.add(new GridMenuItem(mSunDth, "Sun Direct",
				Constants.OPERATOR_DTH_SUN, Constants.RECHARGE_DTH));
		gridArray.add(new GridMenuItem(mBigDth, "Big TV",
				Constants.OPERATOR_DTH_BIG, Constants.RECHARGE_DTH));
		gridArray.add(new GridMenuItem(mAirtelDth, "AirTel Dth",
				Constants.OPERATOR_DTH_AIRTEL, Constants.RECHARGE_DTH));
		gridArray.add(new GridMenuItem(mVideoconDth, "Videocon D2h",
				Constants.OPERATOR_DTH_VIDEOCON, Constants.RECHARGE_DTH));

		editRechargeCode = (EditText) findViewById(R.id.editRechargeCode);
		btnRechargeCode = (Button) findViewById(R.id.btnRechargeCode);

		new GetVASListTask().execute();

		mSideBarAdapter = new QuickRechargeAdapter(this,
				R.layout.quickrecharge_adapter, gridArray);

		listViewSide.setAdapter(mSideBarAdapter);
		listViewSide.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {

				positionList = arg2;
				if (gridArray.get(arg2).getmRechargeType() == Constants.RECHARGE_MOBILE) {
					editRechargeCode.setText("*" + gridArray.get(arg2).getId()
							+ "*");
					textExampleCode
							.setText("*"
									+ gridArray.get(arg2).getId()
									+ "*mobile*amount  put after amount # for STV recharges ");

				} else if (gridArray.get(arg2).getmRechargeType() == Constants.RECHARGE_DTH) {
					editRechargeCode.setText("*" + gridArray.get(arg2).getId()
							+ "*");
					textExampleCode.setText("E.g.   " + "*"
							+ gridArray.get(arg2).getId()
							+ "*sub-id*mobile*amount");

				} else if (gridArray.get(arg2).getmRechargeType() == Constants.RECHARGE_ENTERTAINMENT) {
					editRechargeCode.setText("*" + gridArray.get(arg2).getId()
							+ "*mob-no");

					method = "vasRecharge";
				}
				int position = editRechargeCode.length();

				textOperator.setText(gridArray.get(arg2).getTitle());
				Editable etext = editRechargeCode.getText();
				Selection.setSelection(etext, position);
				isExpanded = false;

				// Collapse
				// new CollapseAnimation(slidingPanel, panelWidth,
				// TranslateAnimation.RELATIVE_TO_SELF, 0.22f,
				// TranslateAnimation.RELATIVE_TO_SELF, 0.0f, 0, 0.0f, 0,
				// 0.0f);
			}
		});

		btnRechargeCode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					if (!Utility.getLoginFlag(QuickRechargeActivity.this,
							Constants.SHAREDPREFERENCE_IS_LOGIN)) {
						Intent intent = new Intent(QuickRechargeActivity.this,
								LoginActivity.class);
						startActivity(intent);
						return;
					}
					String foo = editRechargeCode.getText().toString();
					if (foo.length() == 0) {
						Constants.showOneButtonFailureDialog(
								QuickRechargeActivity.this,
								"Please select operator.", "Quick Recharge",
								Constants.OTHER_ERROR);
						return;
					}
					split = foo.split(Pattern.quote("*"));
					timestamp = System.currentTimeMillis();
					/*
					 * StringBuilder sb = new StringBuilder(); for (int i = 0; i
					 * < split.length; i++) { sb.append(split[i+1]); if (i !=
					 * split.length - 1) { sb.append(" "); } }
					 */
					if (bNetOrSms) {
						if (RequestClass.getInstance() == null) {
							Intent intent = new Intent(
									QuickRechargeActivity.this,
									LoginActivity.class);
							startActivity(intent);
						} else {

							int size = split.length;
							// String rechargeString =
							// editRechargeCode.getText()
							// .toString();
							// for (int i = 0; i < rechargeString.length(); i++)
							// if (rechargeString.charAt(i) == '*')
							// count++;

							if (size < 4) {
								Constants
										.showOneButtonFailureDialog(
												QuickRechargeActivity.this,
												"Please enter correct recharge format.",
												"Quick Recharge",
												Constants.OTHER_ERROR);
								return;
							}
							product_id = split[1].length() != 0 ? split[1] : "";

							if (product_id.equals("")) {
								Constants
										.showOneButtonFailureDialog(
												QuickRechargeActivity.this,
												"Please select operator",
												"Quick Recharge",
												Constants.OTHER_ERROR);
								return;
							}
							if (gridArray.get(positionList).getmRechargeType() == Constants.RECHARGE_MOBILE) {

								mobile_no = split[2].length() == 10 ? split[2]
										: "";
								amount = split[3].indexOf("#") != -1 ? split[3]
										.substring(0, split[3].indexOf("#"))
										: split[3];
								amount = amount.length() != 0 ? amount : "";

							} else if (gridArray.get(positionList)
									.getmRechargeType() == Constants.RECHARGE_DTH) {

								if (size < 5) {
									Constants
											.showOneButtonFailureDialog(
													QuickRechargeActivity.this,
													"Please enter correct recharge format.",
													"Quick Recharge",
													Constants.OTHER_ERROR);
									return;
								}
								sub_id = split[2].length() != 0 ? split[2] : "";
								mobile_no = split[3].length() == 10 ? split[3]
										: "";
								amount = split[4].indexOf("#") != -1 ? split[4]
										.substring(0, split[4].indexOf("#"))
										: split[4];
								amount = amount.length() != 0 ? amount : "";

								if (sub_id.equals("")) {
									Constants.showOneButtonFailureDialog(
											QuickRechargeActivity.this,
											"Please enter subscriber id",
											"Quick Recharge",
											Constants.OTHER_ERROR);
									return;
								}

							} else if (gridArray.get(positionList)
									.getmRechargeType() == Constants.RECHARGE_ENTERTAINMENT) {

								mobile_no = split[2].length() == 10 ? split[2]
										: "";
								amount = split[3].indexOf("#") != -1 ? split[3]
										.substring(0, split[3].indexOf("#"))
										: split[3];
								amount = amount.length() != 0 ? amount : "";

								method = "vasRecharge";
							}

							if (mobile_no.equals("")) {
								Constants
										.showOneButtonFailureDialog(
												QuickRechargeActivity.this,
												"Please enter 10 digit mobile number",
												"Quick Recharge",
												Constants.OTHER_ERROR);
								return;
							} else if (amount.equals("")) {
								Constants
										.showOneButtonFailureDialog(
												QuickRechargeActivity.this,
												"Please enter proper amount",
												"Quick Recharge",
												Constants.OTHER_ERROR);
								return;
							} else {

								if (gridArray.get(positionList)
										.getmRechargeType() == Constants.RECHARGE_MOBILE) {

									listValuePair.add(new BasicNameValuePair(
											"method", "mobRecharge"));
									listValuePair.add(new BasicNameValuePair(
											"operator", product_id));
									listValuePair.add(new BasicNameValuePair(
											"mobileNumber", mobile_no));
									listValuePair.add(new BasicNameValuePair(
											"subId", mobile_no));
									listValuePair.add(new BasicNameValuePair(
											"amount", amount));

								} else if (gridArray.get(positionList)
										.getmRechargeType() == Constants.RECHARGE_DTH) {

									listValuePair.add(new BasicNameValuePair(
											"method", "dthRecharge"));
									listValuePair.add(new BasicNameValuePair(
											"operator",
											""
													+ (Integer
															.parseInt(product_id) - 15)));
									listValuePair.add(new BasicNameValuePair(
											"mobileNumber", mobile_no));
									listValuePair.add(new BasicNameValuePair(
											"subId", sub_id));
									listValuePair.add(new BasicNameValuePair(
											"amount", amount));

								} else if (gridArray.get(positionList)
										.getmRechargeType() == Constants.RECHARGE_ENTERTAINMENT) {

									// method = "vasRecharge";

									listValuePair.add(new BasicNameValuePair(
											"method", "vasRecharge"));
									listValuePair.add(new BasicNameValuePair(
											"product", product_id));
									listValuePair.add(new BasicNameValuePair(
											"Mobile", mobile_no));

								}

								if (foo.contains("#")) {
									listValuePair.add(new BasicNameValuePair(
											"special", "2"));
								} else {
									listValuePair.add(new BasicNameValuePair(
											"special", "1"));
								}

								new RechargeTask().execute();
							}
						}
					} else {
						String rechargeString = editRechargeCode.getText()
								.toString().trim();
						Constants.senDirectSms(QuickRechargeActivity.this,
								rechargeString);

					}

				} catch (Exception e) {
				}
			}
		});

		textOperator.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (!isExpanded) {
					isExpanded = true;

					// Expand
					// new ExpandAnimation(slidingPanel, panelWidth,
					// Animation.RELATIVE_TO_SELF, 0.0f,
					// Animation.RELATIVE_TO_SELF, 0.22f, 0, 0.0f, 0, 0.0f);
				} else {
					isExpanded = false;

					// Collapse
					// new CollapseAnimation(slidingPanel, panelWidth,
					// TranslateAnimation.RELATIVE_TO_SELF, 0.22f,
					// TranslateAnimation.RELATIVE_TO_SELF, 0.0f, 0, 0.0f,
					// 0, 0.0f);

				}
			}
		});
		// Slide the Panel
		menuViewButton = (Button) findViewById(R.id.menuViewButton);
		menuViewButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (!isExpanded) {
					isExpanded = true;

					// Expand
					// new ExpandAnimation(slidingPanel, panelWidth,
					// Animation.RELATIVE_TO_SELF, 0.0f,
					// Animation.RELATIVE_TO_SELF, 0.22f, 0, 0.0f, 0, 0.0f);
				} else {
					isExpanded = false;

					// Collapse
					// new CollapseAnimation(slidingPanel, panelWidth,
					// TranslateAnimation.RELATIVE_TO_SELF, 0.22f,
					// TranslateAnimation.RELATIVE_TO_SELF, 0.0f, 0, 0.0f,
					// 0, 0.0f);

				}
			}
		});

	}
	public static GoogleAnalytics analytics; public static Tracker tracker;	
	@Override
		public void onStart() {
			super.onStart();
			// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
			// EasyTracker.getInstance(this).activityStart(this);
			//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
		}
	public class GetVASListTask extends AsyncTask<String, String, String> {

		private ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair
					.add(new BasicNameValuePair("method", "getVASProducts"));
			listValuePair.add(new BasicNameValuePair("device_type", "android"));
			String response = RequestClass.getInstance().readPay1Request(
					QuickRechargeActivity.this, Constants.API_URL,
					listValuePair);

			listValuePair.clear();
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			if (!result.startsWith("Error")) {
				String replaced = result.replace("(", "").replace(")", "")
						.replace(";", "");

				try {
					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = array.getJSONObject(0);
					// Bitmap bitmap = null;
					String productName = null;
					String productId = null;
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONArray descArray = jsonObject
								.getJSONArray("description");
						for (int i = 0; i < descArray.length(); i++) {
							JSONObject json_data = descArray.getJSONObject(i);

							String imageUrl = json_data.getJSONObject("prods")
									.getString("image");
							productName = json_data.getJSONObject("prods")
									.getString("name");
							productId = json_data.getJSONObject("prods")
									.getString("product_id");
							if (imageUrl.length() <= 4) {

							} else {
								// bitmap = Utility.getBitmap(imageUrl);

							}
							// gridArray.add(new GridMenuItem(bitmap,
							// productName,
							// Integer.parseInt(productId),
							// Constants.RECHARGE_ENTERTAINMENT));
						}

					} else {
						Constants.showOneButtonFailureDialog(
								QuickRechargeActivity.this,
								Constants.checkCode(replaced), "Login",
								Constants.RECHARGE_SETTINGS);
					}
					mSideBarAdapter.notifyDataSetChanged();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
				}
			} else {
				// Constants.showCustomToast(QuickRechargeActivity.this,
				// "Check Your Connection");
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(QuickRechargeActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.show();
			super.onPreExecute();
		}

	}

	public static boolean saveRegion(Context context, GridMenuItem item) {
		try {
			FileOutputStream fos = context.openFileOutput("temp",
					Context.MODE_PRIVATE);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(item);
			oos.close();
		} catch (IOException e) {
			// e.printStackTrace();
			return false;
		}

		return true;
	}

	public static GridMenuItem getRegion(Context context) {
		try {
			FileInputStream fis = context.openFileInput("temp");
			ObjectInputStream is = new ObjectInputStream(fis);
			Object readObject = is.readObject();
			is.close();

			if (readObject != null && readObject instanceof GridMenuItem) {
				return (GridMenuItem) readObject;
			}
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// e.printStackTrace();
		}

		return null;
	}

	public class RechargeTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			listValuePair.add(new BasicNameValuePair("type", "flexi"));
			listValuePair.add(new BasicNameValuePair("circle", ""));
			listValuePair.add(new BasicNameValuePair("timestamp", String
					.valueOf(timestamp)));
			listValuePair.add(new BasicNameValuePair("profile_id", Utility
					.getProfileID(QuickRechargeActivity.this,
							Constants.SHAREDPREFERENCE_PROFILE_ID)));
			if (gridArray.get(positionList).getmRechargeType() == Constants.RECHARGE_DTH) {
				listValuePair.add(new BasicNameValuePair("hash_code", Constants
						.encryptPassword(Utility.getUUID(
								QuickRechargeActivity.this,
								Constants.SHAREDPREFERENCE_UUID)
								+ sub_id + amount + timestamp)));
			} else {
				listValuePair.add(new BasicNameValuePair("hash_code", Constants
						.encryptPassword(Utility.getUUID(
								QuickRechargeActivity.this,
								Constants.SHAREDPREFERENCE_UUID)
								+ mobile_no + amount + timestamp)));
			}

			listValuePair.add(new BasicNameValuePair("device_type", "android"));

			String response = RequestClass.getInstance().readPay1Request(
					QuickRechargeActivity.this, Constants.API_URL,
					listValuePair);
			String replaced = response.replace("(", "").replace(")", "")
					.replace(";", "");
			return replaced;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}

			try {
				JSONArray array = new JSONArray(result);
				JSONObject jsonObject = array.getJSONObject(0);
				String rechargeString = null;

				if (gridArray.get(positionList).getmRechargeType() == Constants.RECHARGE_DTH) {
					rechargeString = Constants.getRechargeString(
							QuickRechargeActivity.this, Constants.RECHARGE_DTH,
							Integer.parseInt(product_id), sub_id, mobile_no,
							amount, "0");
				} else {
					rechargeString = Constants.getRechargeString(
							QuickRechargeActivity.this,
							Constants.RECHARGE_MOBILE,
							Integer.parseInt(product_id), mobile_no, mobile_no,
							amount, "0");
				}

				try {
					notificationDataSource.open();
					notificationDataSource.createNotification(rechargeString,
							0, "" + System.currentTimeMillis());
				} catch (SQLException exception) {
					// TODO: handle exception
					// // Log.e("Er ", exception.getMessage());
				} catch (Exception e) {
					// TODO: handle exception
					// // Log.e("Er ", e.getMessage());
				} finally {
					notificationDataSource.close();
				}
				String status = jsonObject.getString("status");
				if (status.equalsIgnoreCase("success")) {
					// Constants.showCustomToast(QuickRechargeActivity.this,
					// "Recharge request sent successfully.");
					Constants.showOneButtonSuccessDialog(
							QuickRechargeActivity.this,
							"Recharge request sent successfully.\nTransaction id : "
									+ jsonObject.getString("description"), 0);
					String balance = jsonObject.getString("balance");
					Utility.setBalance(QuickRechargeActivity.this,
							Constants.SHAREDPREFERENCE_BALANCE, balance);
					// finish();
				} else {
					// Constants.showCustomToast(QuickRechargeActivity.this,
					// Constants.checkCode(result));
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				// Constants.showCustomToast(QuickRechargeActivity.this,
				// result);
			} catch (Exception e) {
				// TODO: handle exception
				// Constants.showCustomToast(QuickRechargeActivity.this,
				// result);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(QuickRechargeActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							RechargeTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			RechargeTask.this.cancel(true);
			dialog.cancel();
		}

	}

}
