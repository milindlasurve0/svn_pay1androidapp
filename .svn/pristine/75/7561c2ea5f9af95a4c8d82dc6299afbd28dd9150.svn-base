package com.mindsarray.pay1.databasehandler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.mindsarray.pay1.contenthandler.Post;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class PostDBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "postManager";
    private static final String TABLE_C2D_POSTS = "c2d_posts";
    private static final String KEY_POST_ID = "mId";
    private static final String KEY_POST_IMAGES = "mPostImages";
    private static final String KEY_WHOLESALER_LOGO = "mWholesalerLogo";
    private static final String KEY_WHOLESALER_NAME = "mWholesalerName";

    private static final String KEY_WHOLESALER_TITLE = "mPostTitle";
    private static final String KEY_WHOLESALER_DESC = "mPostDescription";
    private static final String KEY_WHOLESALER_ID = "mClientId";
    private static final String KEY_WHOLESALER_CATEGORY_ID = "mCategoryId";
    private static final String KEY_WHOLESALER_CATEGORY_NAME = "mCategoryName";
    private static final String KEY_POST_TIME = "mTime";
    private static final String KEY_WHOLESALER_CONTACT = "mContactNo";
    private static final String KEY_WHOLESALER_INTERESTED = "mIsInterested";

    private static final String TABLE_C2D_STATISTICS = "c2d_statistics";
    private static final String KEY_STATS_ID = "mStatsId";
    private static final String KEY_STATS_TIME = "viewTime";
    private static final String KEY_STATS_POST_ID = "postId";
    private static final String KEY_STATS_RETAILERS_ID = "retailerID";

    // private static final String KEY_WHOLESALER_LOGO = "recharge_datetime";

    public PostDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

        String CREATE_STATS_TABLE = "CREATE TABLE " + TABLE_C2D_STATISTICS
                + "(" + KEY_STATS_RETAILERS_ID + " TEXT," + KEY_STATS_POST_ID
                + " INTEGER," + KEY_STATS_TIME + " DATETIME" + ")";

        String CREATE_POSTS_TABLE = "CREATE TABLE " + TABLE_C2D_POSTS + "("
                + KEY_POST_ID + " INTEGER PRIMARY KEY ,"
                + KEY_WHOLESALER_CATEGORY_ID + " TEXT,"
                + KEY_WHOLESALER_INTERESTED + " TEXT,"
                + KEY_WHOLESALER_CATEGORY_NAME + " TEXT,"
                + KEY_WHOLESALER_CONTACT + " TEXT," + KEY_POST_TIME
                + " DATETIME," + KEY_WHOLESALER_ID + " TEXT,"
                + KEY_WHOLESALER_DESC + " TEXT," + KEY_WHOLESALER_LOGO
                + " TEXT," + KEY_WHOLESALER_NAME + " TEXT,"
                + KEY_WHOLESALER_TITLE + " TEXT," + KEY_POST_IMAGES + " TEXT"
                + ")";

        db.execSQL(CREATE_STATS_TABLE);
        db.execSQL(CREATE_POSTS_TABLE);
    }

    public void addStatsToTable(int mPostId, String mRetailersId) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_STATS_POST_ID, mPostId);

        values.put(KEY_STATS_RETAILERS_ID, mRetailersId);
        // getmIsInterested
        values.put(KEY_STATS_TIME, getDateTime());

        db.insert(TABLE_C2D_STATISTICS, null, values);

        db.close();
    }

    public void deleteAllStats() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_C2D_STATISTICS);
    }

    // public String getC2DStats(){
    public JSONArray getC2DStats() {

        SQLiteDatabase db = this.getWritableDatabase();
        String searchQuery = "SELECT  * FROM " + TABLE_C2D_STATISTICS;
        Cursor cursor = db.rawQuery(searchQuery, null);

        JSONArray resultSet = new JSONArray();

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        if (cursor.getString(i) != null) {
                            Log.d("TAG_NAME", cursor.getString(i));
                            rowObject.put(cursor.getColumnName(i),
                                    cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                        Log.d("TAG_NAME", e.getMessage());
                    }
                }
            }
            resultSet.put(rowObject);
            cursor.moveToNext();
        }
        cursor.close();
        Log.d("TAG_NAME", resultSet.toString());
        return resultSet;
    }

    public void addPostsToTable(Post post) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_POST_ID, post.getmId());
        values.put(KEY_WHOLESALER_CATEGORY_ID, post.getmCategoryId());
        values.put(KEY_WHOLESALER_CATEGORY_NAME, "");
        values.put(KEY_WHOLESALER_CONTACT, post.getmContactNo());
        values.put(KEY_WHOLESALER_DESC, post.getmPostDescription());
        values.put(KEY_WHOLESALER_ID, post.getmClientId());
        values.put(KEY_WHOLESALER_LOGO, post.getmWholesalerLogo());
        values.put(KEY_POST_IMAGES, post.getmPostImages());
        values.put(KEY_WHOLESALER_NAME, post.getmWholesalerName());
        values.put(KEY_WHOLESALER_TITLE, post.getmPostTitle());
        values.put(KEY_WHOLESALER_INTERESTED, post.getmIsInterested());
        // getmIsInterested
        //values.put(KEY_POST_TIME,getDateTime());
        values.put(KEY_POST_TIME, post.getmTime());

        db.insert(TABLE_C2D_POSTS, null, values);

        db.close();
    }

    public void UpdatePost(int postId) {
        String updateQuery = "UPDATE " + TABLE_C2D_POSTS + " SET "
                + KEY_WHOLESALER_INTERESTED + " = '1' WHERE mId = " + postId;
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(updateQuery);
    }

    public Post getWholesalerData(String wholesalerId) {
        Post post = new Post();
        String updateQuery = "SELECT * FROM " + TABLE_C2D_POSTS
                + " WHERE mClientId = '" + wholesalerId + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(updateQuery, null);
        cursor.moveToFirst();
        post = cursorToPost(cursor);
        return post;

    }

    public void deletePosts(String... postIds) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL(String.format("DELETE FROM " + TABLE_C2D_POSTS + " WHERE mId IN (%s);",
                    postIds));
        } catch (SQLException sqe) {
            Log.d("asdF", "asdf");
        } catch (Exception exc) {
            Log.d("asdF", "asdf");
        }

    }

    public ArrayList<Post> getPostsFromDB() {
        ArrayList<Post> posts = new ArrayList<Post>();
        Cursor cursor = null;
        SQLiteDatabase db = this.getWritableDatabase();
        // Cursor cursor = db.rawQuery(selectQuery, null);

		/*
         * Cursor cursor = db.query(TABLE_C2D_POSTS, null, null, null, null,
		 * null, KEY_POST_TIME + " desc");
		 */
        int viewUpdate = 0;
        if (viewUpdate == 1) {
            cursor = db.query(TABLE_C2D_POSTS, null, null, null, null, null,
                    null);
        } else {
            cursor = db.query(TABLE_C2D_POSTS, null, null, null, null, null,
                    KEY_POST_TIME + " desc");
        }
        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Post post = cursorToPost(cursor);
                posts.add(post);
                cursor.moveToNext();
            }
        }


        // make sure to close the cursor
        cursor.close();
        return posts;
    }

    private Post cursorToPost(Cursor cursor) {
        Post post = new Post();

        post.setmCategoryId(cursor.getString(cursor
                .getColumnIndex(KEY_WHOLESALER_CATEGORY_ID)));
        post.setmClientId(cursor.getString(cursor
                .getColumnIndex(KEY_WHOLESALER_ID)));
        post.setmContactNo(cursor.getString(cursor
                .getColumnIndex(KEY_WHOLESALER_CONTACT)));
        post.setmPostDescription(cursor.getString(cursor
                .getColumnIndex(KEY_WHOLESALER_DESC)));
        post.setmPostTitle(cursor.getString(cursor
                .getColumnIndex(KEY_WHOLESALER_TITLE)));
        post.setmTime(cursor.getString(cursor.getColumnIndex(KEY_POST_TIME)));
        post.setmWholesalerLogo(cursor.getString(cursor
                .getColumnIndex(KEY_WHOLESALER_LOGO)));
        post.setmWholesalerName(cursor.getString(cursor
                .getColumnIndex(KEY_WHOLESALER_NAME)));
        post.setmId(cursor.getInt(cursor.getColumnIndex(KEY_POST_ID)));
        post.setmPostImages(cursor.getString(cursor
                .getColumnIndex(KEY_POST_IMAGES)));
        post.setmIsInterested(cursor.getString(cursor
                .getColumnIndex(KEY_WHOLESALER_INTERESTED)));

        return post;
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_C2D_POSTS);

    }

}
