# view res/layout/retailer_kyc_step_1.xml #generated:87
-keep class .RetailerKYCActivity { <init>(...); }

# view res/layout/design_navigation_item.xml #generated:17
-keep class android.support.design.internal.NavigationMenuItemView { <init>(...); }

# view res/layout/design_navigation_menu.xml #generated:17
-keep class android.support.design.internal.NavigationMenuView { <init>(...); }

# view res/layout/design_layout_snackbar.xml #generated:18
# view sw600dp-v13\res/layout-sw600dp-v13/design_layout_snackbar.xml #generated:18
-keep class android.support.design.widget.Snackbar$SnackbarLayout { <init>(...); }

# view res/layout/main_tab_activity.xml #generated:70
# view res/layout/retailer_kyc.xml #generated:46
-keep class android.support.v4.view.PagerTabStrip { <init>(...); }

# view res/layout/earning_fragement_layout.xml #generated:50
# view res/layout/image_slide_activity.xml #generated:7
# view res/layout/main_activity.xml #generated:108
# view res/layout/main_layout.xml #generated:23
# view res/layout/main_tab_activity.xml #generated:64
# view res/layout/new_main_activity.xml #generated:115
# view res/layout/plan_fragment_activity.xml #generated:90
# view res/layout/recharge_fragment_activity.xml #generated:55
# view res/layout/retailer_kyc.xml #generated:42
# view res/layout/reversal_fragment.xml #generated:44
# view res/layout/reversal_status_fragment.xml #generated:45
# view sw600dp-v13\res/layout-sw600dp-v13/main_activity.xml #generated:105
# view sw600dp-v13\res/layout-sw600dp-v13/new_main_activity.xml #generated:99
# view sw720dp-v13\res/layout-sw720dp-v13/main_activity.xml #generated:105
# view sw720dp-v13\res/layout-sw720dp-v13/new_main_activity.xml #generated:97
-keep class android.support.v4.view.ViewPager { <init>(...); }

# view res/layout/abc_alert_dialog_material.xml #generated:130
-keep class android.support.v4.widget.Space { <init>(...); }

# view res/layout/main_tab_wholesale_fragment.xml #generated:2
-keep class android.support.v4.widget.SwipeRefreshLayout { <init>(...); }

# view res/layout/abc_action_menu_item_layout.xml #generated:17
-keep class android.support.v7.internal.view.menu.ActionMenuItemView { <init>(...); }

# view res/layout/abc_expanded_menu_layout.xml #generated:17
-keep class android.support.v7.internal.view.menu.ExpandedMenuView { <init>(...); }

# view res/layout/abc_list_menu_item_layout.xml #generated:17
# view res/layout/abc_popup_menu_item_layout.xml #generated:17
-keep class android.support.v7.internal.view.menu.ListMenuItemView { <init>(...); }

# view res/layout/abc_screen_toolbar.xml #generated:27
-keep class android.support.v7.internal.widget.ActionBarContainer { <init>(...); }

# view res/layout/abc_action_mode_bar.xml #generated:19
# view res/layout/abc_screen_toolbar.xml #generated:43
-keep class android.support.v7.internal.widget.ActionBarContextView { <init>(...); }

# view res/layout/abc_screen_toolbar.xml #generated:17
-keep class android.support.v7.internal.widget.ActionBarOverlayLayout { <init>(...); }

# view res/layout/abc_activity_chooser_view.xml #generated:19
-keep class android.support.v7.internal.widget.ActivityChooserView$InnerLayout { <init>(...); }

# view res/layout/abc_screen_content_include.xml #generated:19
-keep class android.support.v7.internal.widget.ContentFrameLayout { <init>(...); }

# view res/layout/abc_alert_dialog_material.xml #generated:48
-keep class android.support.v7.internal.widget.DialogTitle { <init>(...); }

# view res/layout/abc_screen_simple_overlay_action_mode.xml #generated:23
-keep class android.support.v7.internal.widget.FitWindowsFrameLayout { <init>(...); }

# view res/layout/abc_dialog_title_material.xml #generated:22
# view res/layout/abc_screen_simple.xml #generated:17
-keep class android.support.v7.internal.widget.FitWindowsLinearLayout { <init>(...); }

# view res/layout/abc_action_mode_close_item_material.xml #generated:17
# view res/layout/abc_search_dropdown_item_icons_2line.xml #generated:27
# view res/layout/abc_search_dropdown_item_icons_2line.xml #generated:37
# view res/layout/abc_search_dropdown_item_icons_2line.xml #generated:48
# view res/layout/abc_search_view.xml #generated:116
# view res/layout/abc_search_view.xml #generated:128
# view res/layout/abc_search_view.xml #generated:38
# view res/layout/abc_search_view.xml #generated:60
# view res/layout/abc_search_view.xml #generated:97
-keep class android.support.v7.internal.widget.TintImageView { <init>(...); }

# view res/layout/abc_screen_simple.xml #generated:25
# view res/layout/abc_screen_simple_overlay_action_mode.xml #generated:32
-keep class android.support.v7.internal.widget.ViewStubCompat { <init>(...); }

# view res/layout/abc_action_menu_layout.xml #generated:17
-keep class android.support.v7.widget.ActionMenuView { <init>(...); }

# view res/layout/abc_search_view.xml #generated:78
-keep class android.support.v7.widget.SearchView$SearchAutoComplete { <init>(...); }

# view res/layout/abc_screen_toolbar.xml #generated:36
-keep class android.support.v7.widget.Toolbar { <init>(...); }

# view AndroidManifest.xml #generated:202
-keep class com.eze.api.EzeAPIActivity { <init>(...); }

# view AndroidManifest.xml #generated:817
-keep class com.google.android.gms.ads.AdActivity { <init>(...); }

# view AndroidManifest.xml #generated:821
-keep class com.google.android.gms.ads.purchase.InAppPurchaseActivity { <init>(...); }

# view AndroidManifest.xml #generated:828
-keep class com.google.android.gms.appinvite.PreviewActivity { <init>(...); }

# view AndroidManifest.xml #generated:847
-keep class com.google.android.gms.auth.api.signin.RevocationBoundService { <init>(...); }

# view AndroidManifest.xml #generated:838
-keep class com.google.android.gms.auth.api.signin.internal.SignInHubActivity { <init>(...); }

# view AndroidManifest.xml #generated:855
-keep class com.google.android.gms.cast.framework.ReconnectionService { <init>(...); }

# view res/layout/cast_help_text.xml #generated:2
-keep class com.google.android.gms.cast.framework.internal.featurehighlight.HelpTextView { <init>(...); }

# view AndroidManifest.xml #generated:852
-keep class com.google.android.gms.cast.framework.media.MediaIntentReceiver { <init>(...); }

# view AndroidManifest.xml #generated:854
-keep class com.google.android.gms.cast.framework.media.MediaNotificationService { <init>(...); }

# view AndroidManifest.xml #generated:824
-keep class com.google.android.gms.common.api.GoogleApiActivity { <init>(...); }

# view AndroidManifest.xml #generated:744
-keep class com.google.android.gms.gcm.GcmReceiver { <init>(...); }

# view res/layout/activity_maps.xml #generated:5
# view res/layout/retailer_kyc_step_1.xml #generated:87
# view res/layout/settingsmap_activity.xml #generated:6
# view res/layout/settingsupdateaddress_activity.xml #generated:35
-keep class com.google.android.gms.maps.SupportMapFragment { <init>(...); }

# view AndroidManifest.xml #generated:900
-keep class com.google.android.gms.measurement.AppMeasurementReceiver { <init>(...); }

# view AndroidManifest.xml #generated:908
-keep class com.google.android.gms.measurement.AppMeasurementService { <init>(...); }

# view AndroidManifest.xml #generated:928
-keep class com.google.android.gms.tagmanager.TagManagerPreviewActivity { <init>(...); }

# view AndroidManifest.xml #generated:923
-keep class com.google.android.gms.tagmanager.TagManagerService { <init>(...); }

# view AndroidManifest.xml #generated:893
-keep class com.google.firebase.crash.internal.service.FirebaseCrashReceiverService { <init>(...); }

# view AndroidManifest.xml #generated:896
-keep class com.google.firebase.crash.internal.service.FirebaseCrashSenderService { <init>(...); }

# view AndroidManifest.xml #generated:872
-keep class com.google.firebase.iid.FirebaseInstanceIdInternalReceiver { <init>(...); }

# view AndroidManifest.xml #generated:857
-keep class com.google.firebase.iid.FirebaseInstanceIdReceiver { <init>(...); }

# view AndroidManifest.xml #generated:879
-keep class com.google.firebase.iid.FirebaseInstanceIdService { <init>(...); }

# view AndroidManifest.xml #generated:916
-keep class com.google.firebase.messaging.FirebaseMessagingService { <init>(...); }

# view AndroidManifest.xml #generated:887
-keep class com.google.firebase.provider.FirebaseInitProvider { <init>(...); }

# view AndroidManifest.xml #generated:167
-keep class com.mindsarray.pay1.AboutActivity { <init>(...); }

# view AndroidManifest.xml #generated:502
-keep class com.mindsarray.pay1.AccountHistoryActivity { <init>(...); }

# view AndroidManifest.xml #generated:512
-keep class com.mindsarray.pay1.AllQuickRechargeActivity { <init>(...); }

# view AndroidManifest.xml #generated:60
-keep class com.mindsarray.pay1.AnalyticsApplication { <init>(...); }

# view AndroidManifest.xml #generated:655
-keep class com.mindsarray.pay1.BankCashDepositActivity { <init>(...); }

# view AndroidManifest.xml #generated:261
-keep class com.mindsarray.pay1.CashPaymentActivity { <init>(...); }

# view AndroidManifest.xml #generated:256
-keep class com.mindsarray.pay1.CashPaymentListActivity { <init>(...); }

# view AndroidManifest.xml #generated:146
-keep class com.mindsarray.pay1.ChangeMobileNumberActivity { <init>(...); }

# view AndroidManifest.xml #generated:405
-keep class com.mindsarray.pay1.ChatActivity { <init>(...); }

# view AndroidManifest.xml #generated:635
-keep class com.mindsarray.pay1.CongratulateRetailerActivity { <init>(...); }

# view AndroidManifest.xml #generated:625
-keep class com.mindsarray.pay1.ConsumerAppActivity { <init>(...); }

# view AndroidManifest.xml #generated:570
-keep class com.mindsarray.pay1.DataCardActivity { <init>(...); }

# view AndroidManifest.xml #generated:565
-keep class com.mindsarray.pay1.DataCardMainActivity { <init>(...); }

# view AndroidManifest.xml #generated:604
-keep class com.mindsarray.pay1.DataCardTabFragment { <init>(...); }

# view AndroidManifest.xml #generated:615
-keep class com.mindsarray.pay1.DiscountStructureTabFragment { <init>(...); }

# view AndroidManifest.xml #generated:665
-keep class com.mindsarray.pay1.DistributorInterestActivity { <init>(...); }

# view AndroidManifest.xml #generated:341
-keep class com.mindsarray.pay1.DistributorRegistrationActivity { <init>(...); }

# view AndroidManifest.xml #generated:387
-keep class com.mindsarray.pay1.DthRechargeActivity { <init>(...); }

# view AndroidManifest.xml #generated:382
-keep class com.mindsarray.pay1.DthRechargeMainActivity { <init>(...); }

# view AndroidManifest.xml #generated:592
-keep class com.mindsarray.pay1.DthRechargeTabFragment { <init>(...); }

# view AndroidManifest.xml #generated:434
-keep class com.mindsarray.pay1.EarnigFragementActivity { <init>(...); }

# view AndroidManifest.xml #generated:466
-keep class com.mindsarray.pay1.EarningActivity { <init>(...); }

# view AndroidManifest.xml #generated:476
-keep class com.mindsarray.pay1.EarningDetailsActivity { <init>(...); }

# view AndroidManifest.xml #generated:416
-keep class com.mindsarray.pay1.EntertainmentRechargeActivity { <init>(...); }

# view AndroidManifest.xml #generated:412
-keep class com.mindsarray.pay1.EntertainmentRechargeMainActivty { <init>(...); }

# view AndroidManifest.xml #generated:400
-keep class com.mindsarray.pay1.EntertainmentRechargeTabActivity { <init>(...); }

# view AndroidManifest.xml #generated:598
-keep class com.mindsarray.pay1.EntertainmentRechargeTabFragment { <init>(...); }

# view AndroidManifest.xml #generated:188
-keep class com.mindsarray.pay1.EzeTapTransactionsHistoryActivity { <init>(...); }

# view AndroidManifest.xml #generated:98
-keep class com.mindsarray.pay1.EzetapDashBoardActivity { <init>(...); }

# view AndroidManifest.xml #generated:195
-keep class com.mindsarray.pay1.EzetapLeadActivity { <init>(...); }

# view AndroidManifest.xml #generated:181
-keep class com.mindsarray.pay1.EzetapPosAtmActivity { <init>(...); }

# view AndroidManifest.xml #generated:246
-keep class com.mindsarray.pay1.ForgotPassword { <init>(...); }

# view AndroidManifest.xml #generated:519
-keep class com.mindsarray.pay1.HelpActivity { <init>(...); }

# view AndroidManifest.xml #generated:133
-keep class com.mindsarray.pay1.ImagesSlideActivity { <init>(...); }

# view AndroidManifest.xml #generated:139
-keep class com.mindsarray.pay1.KitRequestActivity { <init>(...); }

# view AndroidManifest.xml #generated:497
-keep class com.mindsarray.pay1.KycShopImageSampleActivity { <init>(...); }

# view AndroidManifest.xml #generated:160
-keep class com.mindsarray.pay1.LanguageSelectionActivity { <init>(...); }

# view AndroidManifest.xml #generated:362
-keep class com.mindsarray.pay1.LeadGenerationOtpActivity { <init>(...); }

# view AndroidManifest.xml #generated:112
-keep class com.mindsarray.pay1.LimitOptionActivity { <init>(...); }

# view AndroidManifest.xml #generated:315
-keep class com.mindsarray.pay1.LogOutActivity { <init>(...); }

# view AndroidManifest.xml #generated:320
-keep class com.mindsarray.pay1.LoginActivity { <init>(...); }

# view AndroidManifest.xml #generated:222
-keep class com.mindsarray.pay1.LoginDialogActivity { <init>(...); }

# view AndroidManifest.xml #generated:237
-keep class com.mindsarray.pay1.MainActivity { <init>(...); }

# view AndroidManifest.xml #generated:675
-keep class com.mindsarray.pay1.MapsActivity { <init>(...); }

# view AndroidManifest.xml #generated:547
-keep class com.mindsarray.pay1.MobileBillActivity { <init>(...); }

# view AndroidManifest.xml #generated:541
-keep class com.mindsarray.pay1.MobileBillMainActivity { <init>(...); }

# view AndroidManifest.xml #generated:529
-keep class com.mindsarray.pay1.MobileBillTabActivity { <init>(...); }

# view AndroidManifest.xml #generated:609
-keep class com.mindsarray.pay1.MobileBillTabFragment { <init>(...); }

# view AndroidManifest.xml #generated:297
-keep class com.mindsarray.pay1.MobileRechargeActivity { <init>(...); }

# view AndroidManifest.xml #generated:271
-keep class com.mindsarray.pay1.MobileRechargeMainActivity { <init>(...); }

# view AndroidManifest.xml #generated:276
-keep class com.mindsarray.pay1.MobileRechargeTabActivity { <init>(...); }

# view AndroidManifest.xml #generated:586
-keep class com.mindsarray.pay1.MobileRechargeTabFragment { <init>(...); }

# view AndroidManifest.xml #generated:660
-keep class com.mindsarray.pay1.NetbankingActivity { <init>(...); }

# view AndroidManifest.xml #generated:242
-keep class com.mindsarray.pay1.NewMainActivity { <init>(...); }

# view AndroidManifest.xml #generated:309
-keep class com.mindsarray.pay1.NotificationActivity { <init>(...); }

# view AndroidManifest.xml #generated:355
-keep class com.mindsarray.pay1.OTPVerificationActivity { <init>(...); }

# view AndroidManifest.xml #generated:153
-keep class com.mindsarray.pay1.OTPVerificationNumber { <init>(...); }

# view AndroidManifest.xml #generated:535
-keep class com.mindsarray.pay1.PGActivity { <init>(...); }

# view AndroidManifest.xml #generated:553
-keep class com.mindsarray.pay1.PaymentActivity { <init>(...); }

# view AndroidManifest.xml #generated:620
-keep class com.mindsarray.pay1.PlanFragmentActivity { <init>(...); }

# view AndroidManifest.xml #generated:524
-keep class com.mindsarray.pay1.PlansActivity { <init>(...); }

# view AndroidManifest.xml #generated:251
-keep class com.mindsarray.pay1.ProceedCashPaymentActivity { <init>(...); }

# view AndroidManifest.xml #generated:507
-keep class com.mindsarray.pay1.QuickRechargeActivity { <init>(...); }

# view AndroidManifest.xml #generated:327
-keep class com.mindsarray.pay1.RegistrationActivity { <init>(...); }

# view AndroidManifest.xml #generated:214
-keep class com.mindsarray.pay1.RegistrationLayout { <init>(...); }

# view AndroidManifest.xml #generated:645
-keep class com.mindsarray.pay1.RegistrationPaymentOptionActivity { <init>(...); }

# view AndroidManifest.xml #generated:460
-keep class com.mindsarray.pay1.ReportMainActivity { <init>(...); }

# view AndroidManifest.xml #generated:376
-keep class com.mindsarray.pay1.RequestActivity { <init>(...); }

# view AndroidManifest.xml #generated:105
-keep class com.mindsarray.pay1.RequestTopupActivity { <init>(...); }

# view AndroidManifest.xml #generated:691
-keep class com.mindsarray.pay1.RetailerKYCActivity { <init>(...); }

# view AndroidManifest.xml #generated:640
-keep class com.mindsarray.pay1.RetailerRequestMissedCallActivity { <init>(...); }

# view AndroidManifest.xml #generated:650
-keep class com.mindsarray.pay1.RetailerVerificationActivity { <init>(...); }

# view AndroidManifest.xml #generated:445
-keep class com.mindsarray.pay1.ReversalRequestActivity { <init>(...); }

# view AndroidManifest.xml #generated:281
-keep class com.mindsarray.pay1.ReversalRequestTabFragment { <init>(...); }

# view AndroidManifest.xml #generated:455
-keep class com.mindsarray.pay1.ReversalStatusActivity { <init>(...); }

# view AndroidManifest.xml #generated:450
-keep class com.mindsarray.pay1.ReversalStatusTabFragment { <init>(...); }

# view AndroidManifest.xml #generated:439
-keep class com.mindsarray.pay1.ReversalTabActivity { <init>(...); }

# view AndroidManifest.xml #generated:471
-keep class com.mindsarray.pay1.SalesDeatilsActivity { <init>(...); }

# view AndroidManifest.xml #generated:700
-keep class com.mindsarray.pay1.SearchActivity { <init>(...); }

# view AndroidManifest.xml #generated:334
-keep class com.mindsarray.pay1.SelectLeadActivity { <init>(...); }

# view AndroidManifest.xml #generated:86
-keep class com.mindsarray.pay1.SendStatsToC2DService { <init>(...); }

# view AndroidManifest.xml #generated:422
-keep class com.mindsarray.pay1.SettingsActivity { <init>(...); }

# view AndroidManifest.xml #generated:126
-keep class com.mindsarray.pay1.SettingsDebugActivity { <init>(...); }

# view AndroidManifest.xml #generated:174
-keep class com.mindsarray.pay1.SettingsMainActivity { <init>(...); }

# view AndroidManifest.xml #generated:581
-keep class com.mindsarray.pay1.SettingsMapActivity { <init>(...); }

# view AndroidManifest.xml #generated:230
-keep class com.mindsarray.pay1.SettingsSmsActivity { <init>(...); }

# view AndroidManifest.xml #generated:575
-keep class com.mindsarray.pay1.SettingsUpdateAddressActivity { <init>(...); }

# view AndroidManifest.xml #generated:486
-keep class com.mindsarray.pay1.SettingsUpdateMobileActivity { <init>(...); }

# view AndroidManifest.xml #generated:427
-keep class com.mindsarray.pay1.SettingsUpdatePinActivity { <init>(...); }

# view AndroidManifest.xml #generated:369
-keep class com.mindsarray.pay1.SignUpActivity { <init>(...); }

# view res/layout/distributor_registration_activity.xml #generated:88
# view res/layout/netbanking_activity.xml #generated:54
# view res/layout/registration_activity.xml #generated:88
-keep class com.mindsarray.pay1.SpinnerTextView { <init>(...); }

# view AndroidManifest.xml #generated:88
-keep class com.mindsarray.pay1.SplashScreenActivity { <init>(...); }

# view AndroidManifest.xml #generated:292
-keep class com.mindsarray.pay1.StatusActvity { <init>(...); }

# view AndroidManifest.xml #generated:670
-keep class com.mindsarray.pay1.SupportActivity { <init>(...); }

# view res/layout/earning_fragement_layout.xml #generated:33
# view res/layout/plan_fragment_activity.xml #generated:64
# view res/layout/recharge_fragment_activity.xml #generated:33
# view res/layout/reversal_fragment.xml #generated:27
# view res/layout/reversal_status_fragment.xml #generated:27
-keep class com.mindsarray.pay1.TabPageIndicator { <init>(...); }

# view res/layout/main_activity.xml #generated:33
# view res/layout/new_main_activity.xml #generated:34
# view sw600dp-v13\res/layout-sw600dp-v13/main_activity.xml #generated:33
# view sw600dp-v13\res/layout-sw600dp-v13/new_main_activity.xml #generated:33
# view sw720dp-v13\res/layout-sw720dp-v13/main_activity.xml #generated:33
# view sw720dp-v13\res/layout-sw720dp-v13/new_main_activity.xml #generated:33
-keep class com.mindsarray.pay1.TabPageIndicatorNew { <init>(...); }

# view AndroidManifest.xml #generated:492
-keep class com.mindsarray.pay1.TopUpActivity { <init>(...); }

# view AndroidManifest.xml #generated:481
-keep class com.mindsarray.pay1.TransactionReportsActivity { <init>(...); }

# view AndroidManifest.xml #generated:266
-keep class com.mindsarray.pay1.TransactionTabFragmentActivity { <init>(...); }

# view AndroidManifest.xml #generated:303
-keep class com.mindsarray.pay1.UtilityBillActivity { <init>(...); }

# view AndroidManifest.xml #generated:287
-keep class com.mindsarray.pay1.UtilityBillTabFragment { <init>(...); }

# view AndroidManifest.xml #generated:207
-keep class com.mindsarray.pay1.VerificationActivity { <init>(...); }

# view AndroidManifest.xml #generated:684
-keep class com.mindsarray.pay1.WalletTabFragment { <init>(...); }

# view AndroidManifest.xml #generated:630
-keep class com.mindsarray.pay1.WalletTopupActivity { <init>(...); }

# view AndroidManifest.xml #generated:348
-keep class com.mindsarray.pay1.WholesaleNotificationActivity { <init>(...); }

# view AndroidManifest.xml #generated:119
-keep class com.mindsarray.pay1.WholesalerProfileActivity { <init>(...); }

# view AndroidManifest.xml #generated:811
-keep class com.mindsarray.pay1.contenthandler.StubProvider { <init>(...); }

# view res/layout/slide_image_activity.xml #generated:8
-keep class com.mindsarray.pay1.customviews.CustomImageView { <init>(...); }

# view res/layout/posts_adapter.xml #generated:87
-keep class com.mindsarray.pay1.customviews.ExpandableTextView { <init>(...); }

# view res/layout/wholesaler_profile_activity.xml #generated:10
-keep class com.mindsarray.pay1.customviews.InteractiveScrollView { <init>(...); }

# view AndroidManifest.xml #generated:716
-keep class com.mindsarray.pay1.receiverhandler.SMSBroadcastReceiver { <init>(...); }

# view AndroidManifest.xml #generated:769
-keep class com.mindsarray.pay1.servicehandler.AuthenticatorService { <init>(...); }

# view AndroidManifest.xml #generated:85
-keep class com.mindsarray.pay1.servicehandler.BroadcastService { <init>(...); }

# view AndroidManifest.xml #generated:768
-keep class com.mindsarray.pay1.servicehandler.ChatIntentService { <init>(...); }

# view AndroidManifest.xml #generated:765
-keep class com.mindsarray.pay1.servicehandler.ChatService { <init>(...); }

# view AndroidManifest.xml #generated:790
-keep class com.mindsarray.pay1.servicehandler.FetchAddressIntentService { <init>(...); }

# view AndroidManifest.xml #generated:766
-keep class com.mindsarray.pay1.servicehandler.NotificationService { <init>(...); }

# view AndroidManifest.xml #generated:807
-keep class com.mindsarray.pay1.servicehandler.RegistrationIntentService { <init>(...); }

# view AndroidManifest.xml #generated:778
-keep class com.mindsarray.pay1.servicehandler.SyncService { <init>(...); }

# view AndroidManifest.xml #generated:793
-keep class com.mindsarray.pay1.servicehandler.gcmListenerService { <init>(...); }

# view AndroidManifest.xml #generated:800
-keep class com.mindsarray.pay1.servicehandler.instanceIDListenerService { <init>(...); }

# view res/layout/retailer_kyc_step_2_0.xml #generated:56
# view res/layout/retailer_kyc_step_2_0.xml #generated:64
# view res/layout/retailer_kyc_step_2_0.xml #generated:72
-keep class com.mindsarray.pay1.utilities.DocumentButton { <init>(...); }

# view res/layout/retailer_kyc_step_2.xml #generated:113
# view res/layout/retailer_kyc_step_2.xml #generated:42
# view res/layout/retailer_kyc_step_2.xml #generated:77
# view res/layout/upload_dialog.xml #generated:50
-keep class com.mindsarray.pay1.utilities.ExpandableHeightGridView { <init>(...); }

