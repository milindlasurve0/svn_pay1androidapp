package com.mindsarray.pay1;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.StatusAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class StatusActvity extends Activity {
    private TextView mDateDisplay;
    private Button btnCalender;
    private int mYear;
    private int mMonth;
    private int mDay;
    StringBuilder builder;
    static final int DATE_DIALOG_ID = 0;
    ListView mStatusListView;
    ArrayList<String> statusList;
    StatusAdapter adapter;
    Bundle bundle;
    private static final String TAG = "Status";

    // boolean isLogin;
    /*
	 * TextView textBalance; Button mBtnLogin, mBtnLogOut, mBtnHome, mBtnHelp;
	 */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        // WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.status_activity);
        tracker = Constants.setAnalyticsTracker(this, tracker);
        // isLogin=Utility.getSharedPreferencesBoolean(StatusActvity.this,
        // Constants.SHAREDPREFERENCE_IS_LOGIN);
        bundle = getIntent().getExtras();
        if (Utility.getLoginFlag(StatusActvity.this, Constants.SHAREDPREFERENCE_IS_LOGIN)) {
            mInsideonCreate();

        } else {
            Intent intent = new Intent(StatusActvity.this, LoginActivity.class);
            intent.putExtra(Constants.REQUEST_FOR, bundle);
            startActivityForResult(intent, Constants.LOGIN);

        }
    }

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        //tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.LOGIN) {
                bundle = data.getExtras().getBundle(Constants.REQUEST_FOR);
                bundle.getInt(Constants.REQUEST_FOR);

                mInsideonCreate();
            } else {

            }
        } else {

        }
    }

    private void mInsideonCreate() {
        // TODO Auto-generated method stub
        findViewById();

        btnCalender.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        updateDisplay();

        statusList = new ArrayList<String>();
        if (bundle.getInt(Constants.REQUEST_FOR) == Constants.MOBILE_TAB) {
            adapter = new StatusAdapter(this, statusList, 1);
        } else if (bundle.getInt(Constants.REQUEST_FOR) == Constants.DTH_TAB) {
            adapter = new StatusAdapter(this, statusList, 2);
        } else if (bundle.getInt(Constants.REQUEST_FOR) == Constants.ENTERTAINMENT_TAB) {
            adapter = new StatusAdapter(this, statusList, 1);
        } else if (bundle.getInt(Constants.REQUEST_FOR) == Constants.BILL_TAB) {
            adapter = new StatusAdapter(this, statusList, 1);
        }
        mStatusListView.setAdapter(adapter);
    }

    private void updateDisplay() {
        builder = new StringBuilder().append(mYear).append("-")
                .append(mMonth + 1).append("-").append(mDay);
        mDateDisplay.setText(builder.toString());
        new GetStatus().execute();
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            mYear = year;
            mMonth = monthOfYear;
            mDay = dayOfMonth;
            updateDisplay();
        }
    };

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                DatePickerDialog dialog = new DatePickerDialog(this,
                        mDateSetListener, mYear, mMonth, mDay);

			/*
			 * if (Build.VERSION.SDK_INT >= 11) { // (picker is a DatePicker)
			 * dialog.getDatePicker().setMaxDate(new Date().getTime()); } else {
			 * 
			 * }
			 */

                return dialog;
			/*
			 * return new DatePickerDialog(this, mDateSetListener, mYear,
			 * mMonth, mDay);
			 */
        }
        return null;
    }

    private void findViewById() {
        btnCalender = (Button) findViewById(R.id.btnCalender);
        mDateDisplay = (TextView) findViewById(R.id.mDateDisplay);
        mStatusListView = (ListView) findViewById(R.id.mStatusListView);
    }

    public class GetStatus extends AsyncTask<String, String, String> implements
            OnDismissListener {

        ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method",
                    "reversalTransactions"));
            listValuePair
                    .add(new BasicNameValuePair("date", builder.toString()));
            if (bundle.getInt(Constants.REQUEST_FOR) == Constants.MOBILE_TAB) {
                listValuePair.add(new BasicNameValuePair("service", "1"));
            } else if (bundle.getInt(Constants.REQUEST_FOR) == Constants.DTH_TAB) {
                listValuePair.add(new BasicNameValuePair("service", "2"));
            } else if (bundle.getInt(Constants.REQUEST_FOR) == Constants.ENTERTAINMENT_TAB) {
                listValuePair.add(new BasicNameValuePair("service", "3"));
            } else if (bundle.getInt(Constants.REQUEST_FOR) == Constants.BILL_TAB) {
                listValuePair.add(new BasicNameValuePair("service", "4"));
            }
            listValuePair.add(new BasicNameValuePair("device_type", "android"));
            String response = RequestClass.getInstance().readPay1Request(
                    StatusActvity.this, Constants.API_URL, listValuePair);
            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            super.onPostExecute(result);
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    statusList.clear();
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray jsonDescription = jsonObject
                                .getJSONArray("description");
                        JSONArray array2 = jsonDescription.getJSONArray(0);
                        for (int i = 0; i < array2.length(); i++) {
                            JSONObject json_data = array2.getJSONObject(i);
                            statusList.add(json_data.toString());

                        }
                        // Log.d("Arra", "Arra");
                        adapter.notifyDataSetChanged();
                    } else {
                        Constants.showOneButtonFailureDialog(
                                StatusActvity.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(StatusActvity.this,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(StatusActvity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(StatusActvity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(StatusActvity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            GetStatus.this.cancel(true);
                            dialog.cancel();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            GetStatus.this.cancel(true);
            dialog.cancel();
        }
    }
}
