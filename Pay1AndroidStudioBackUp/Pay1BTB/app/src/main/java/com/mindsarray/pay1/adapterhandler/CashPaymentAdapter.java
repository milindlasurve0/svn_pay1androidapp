package com.mindsarray.pay1.adapterhandler;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindsarray.pay1.R;
import com.mindsarray.pay1.utilities.Utility;
import com.squareup.picasso.Picasso;

import org.jivesoftware.smack.util.Base64;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class CashPaymentAdapter extends ArrayAdapter<JSONObject> {
    Context mContext;
    List<JSONObject> jsonObject;
    LayoutInflater inflater;
    TextView textViewRupee, textViewRefId, textViewProvider,
            textViewExpiryDate;
    Button btnPayment;
    ImageView imageLogo;
    OnClickListener clickListener;

    public CashPaymentAdapter(Context context, int textViewResourceId,
                              List<JSONObject> objects, OnClickListener clickListener) {
        super(context, textViewResourceId, objects);
        // TODO Auto-generated constructor stub
        this.mContext = context;
        this.jsonObject = objects;
        this.clickListener = clickListener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View view = convertView;
        if (convertView == null) {
            inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.cash_payment_adapter, null);
        }

        imageLogo = (ImageView) view.findViewById(R.id.imageLogo);
        textViewRupee = (TextView) view.findViewById(R.id.textViewRupee);
        textViewRefId = (TextView) view.findViewById(R.id.textViewRefId);
        textViewProvider = (TextView) view.findViewById(R.id.textViewProvider);
        textViewExpiryDate = (TextView) view
                .findViewById(R.id.textViewExpiryDate);
        btnPayment = (Button) view.findViewById(R.id.btnPayment);
        btnPayment.setTag(position);
        btnPayment.setOnClickListener(clickListener);


        try {
            try {
                Picasso.with(mContext)
                        .load(jsonObject.get(position).getString("img_url"))
                        .placeholder(R.drawable.ic_deafult_c2d).fit().into(imageLogo);
                BitmapDrawable drawable = (BitmapDrawable) imageLogo.getDrawable();
                Bitmap bitmap = drawable.getBitmap();

                Utility.setImageSharedPreference(mContext, jsonObject.get(position).getString("img_url"), jsonObject.get(position).getString(
                        "operator"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            textViewRupee.setText("Rs. "
                    + jsonObject.get(position).getString("amount"));

            textViewExpiryDate.setText("Expiry: "
                    + jsonObject.get(position).getString("expiry_time"));

            textViewRefId.setText("Order ID: "
                    + jsonObject.get(position).getString("id"));

            textViewProvider.setText(jsonObject.get(position).getString(
                    "company_name"));
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            // e.printStackTrace();
        }

        return view;
    }

    public String encode(Bitmap icon) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        icon.compress(Bitmap.CompressFormat.PNG, 50, baos);
        byte[] data = baos.toByteArray();
        String test = Base64.encodeBytes(data);
        return test;
    }

}
