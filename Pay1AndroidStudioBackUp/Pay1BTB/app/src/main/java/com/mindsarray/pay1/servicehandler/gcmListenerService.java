package com.mindsarray.pay1.servicehandler;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.media.MediaScannerConnection;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.mindsarray.pay1.MainActivity;
import com.mindsarray.pay1.NotificationActivity;
import com.mindsarray.pay1.R;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.contenthandler.Post;
import com.mindsarray.pay1.databasehandler.NotificationDataSource;
import com.mindsarray.pay1.databasehandler.PostDBHelper;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.NotificationUtil;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;

public class gcmListenerService extends GcmListenerService {

    String TAG = "gcmListenerService";
    Context mContext = gcmListenerService.this;
    Intent intent;
    //TextToSpeech t1;
    PostDBHelper postDBHelper;
    public static int C2D_NOTIFICATION_ID = 1234;
    public static final String BROADCAST_ACTION_FEED = "com.mindsarray.pay1.servicehandler.displayfeedevent";
    public static final String BROADCAST_ACTION = "com.mindsarray.pay1.servicehandler.displayevent";

    @Override
    public void onMessageReceived(String from, Bundle data) {
        try {
            Log.e(TAG, "From: " + from);
            Log.e(TAG, "Data: " + data.toString());

            String msg = data.getString("data");
            String description = "", title = "", type = "", newMsg = "";
            JSONObject jsonObject = new JSONObject(msg);
            Log.d("c2dfeed", "c2dfeed");
            type = jsonObject.getString("type");

            if (type.equalsIgnoreCase("C2dfeed")) {
              /*  t1 = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if (status != TextToSpeech.ERROR) {
                            t1.setLanguage(Locale.UK);
                        }
                    }
                });*/

                newMsg = jsonObject.getString("newMsg");
                if (!newMsg.isEmpty()) {
                    try {
                        Uri notification1 = RingtoneManager
                                .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        Ringtone r = RingtoneManager.getRingtone(
                                getApplicationContext(), notification1);
                        //r.play();
                        //  t1.speak(newMsg, TextToSpeech.QUEUE_FLUSH, null);
                        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

                        v.vibrate(1000);
                    } catch (Exception e) {
                    }

                }
                postDBHelper = new PostDBHelper(mContext);
                if (jsonObject.has("delete_posts")) {
                    int countNoti = Utility.getC2DNotificationCount(mContext);
                    // cancelC2DNotification(mContext,C2D_NOTIFICATION_ID);
                    //showC2DNotification(countNoti, 0);
                    postDBHelper.deletePosts(jsonObject.getString("delete_posts"));
                } else {

                    if (jsonObject.has("msg") && jsonObject.get("msg") instanceof JSONArray) {
                        JSONArray jsonData = jsonObject.getJSONArray("msg");
                        writeToFile(mContext, jsonData.toString());
                        writeStringAsFile(mContext, jsonData.toString(), "testPay1");
                        for (int i = 0; i < jsonData.length(); i++) {
                            JSONObject responsePost = jsonData.getJSONObject(i);
                            Post post = new Post();
                            post.setmCategoryId(responsePost.getString("category_id"));

                            post.setmClientId(responsePost.getString("client_id"));
                            post.setmContactNo(responsePost.getString("contact_no"));
                            post.setmId(Integer.parseInt(responsePost.getString("id")));

                            post.setmPostDescription(responsePost
                                    .getString("description"));
                            post.setmPostTitle(responsePost.getString("title"));

                            post.setmWholesalerLogo(responsePost.getString("logo_url")
                                    .replace("\\/", "/"));
                            post.setmWholesalerName(responsePost
                                    .getString("company_name"));
                            post.setmCategoryName("");
                            post.setmPostImages(responsePost.getJSONArray("images")
                                    .toString());
                            post.setmIsInterested("0");
                            post.setmTime(responsePost
                                    .getString("post_time"));

                            postDBHelper.addPostsToTable(post);
                            writeStringAsFile(mContext, "data inserted " + post.getmId() + " " + post.getmPostTitle(),
                                    "testPay1");
                            writeToFile(mContext, "dataInserted");
                        }

                        int countNoti = Utility.getC2DNotificationCount(mContext);
                        cancelC2DNotification(mContext, C2D_NOTIFICATION_ID);
                        showC2DNotification(countNoti, 1);
                        Utility.setC2DNotificationCount(mContext, countNoti + 1);

                    }

                }
                int countNoti = Utility.getC2DNotificationCount(mContext);
                Intent intent = new Intent();
                intent.setAction("c2d_message");

                intent.putExtra("c2d_count", "" + countNoti);
                mContext.sendOrderedBroadcast(intent, null);
            } else if (type.equalsIgnoreCase("ServiceFeed")) {
                /*Intent i = new Intent("android.intent.action.MAIN").putExtra("some_msg", "I will be sent!");
				this.sendBroadcast(i);*/
                JSONObject jsonObject1 = new JSONObject(jsonObject.getString("msg").toString());

                Notify(jsonObject1.getString("title"), jsonObject1.getString("description"));
                Intent intent = new Intent();
                intent.setAction("com.mindsarray.pay1.servicealert");
                sendBroadcast(intent);
                Utility.setServiceMsgSharedPreference(mContext, jsonObject.getString("msg"));
            } else if (type.equalsIgnoreCase("MarketingFeed")) {
				/*Intent i = new Intent("android.intent.action.MAIN").putExtra("some_msg", "I will be sent!");
				this.sendBroadcast(i);*/
                Intent intent = new Intent();
                intent.setAction("com.mindsarray.pay1.servicealert");
                sendBroadcast(intent);
                Utility.setMarketingMsgSharedPreference(mContext, jsonObject.getString("msg"));
            } else {
                new CheckBalanceTask().execute();
                description = jsonObject.getString("msg");
                title = jsonObject.getString("title");

                callNotification(type, description, title);
                Notify(title, description);

                Utility.setSharedPreference(mContext,
                        Constants.SHAREDPREFERENCE_LAST_NOTIFICATION, ""
                                + description);

            }

			/*
			 * if (type.equalsIgnoreCase("notification")) {
			 * callNotification(type, description, title); Notify(title,
			 * description); } else if (type.equalsIgnoreCase("banner")) {
			 * showBanner(type, description, msg); }else
			 * if(type.equalsIgnoreCase("upload")){ callNotification(type,
			 * description, title); Notify(title, description); }
			 */

            // updateMainActivity(mContext);
            // Constants.showNavigationBar(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    static void updateMainActivity(Context context) {
        Intent intent = new Intent("last_notification");
        context.sendBroadcast(intent);
    }


    private void callNotification(String type, String description, String msg) {

        NotificationDataSource notificationDataSource = new NotificationDataSource(
                mContext);
        try {
            notificationDataSource.open();
            notificationDataSource.createNotification(description, 1, ""
                    + System.currentTimeMillis());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            notificationDataSource.close();
        }

    }

    private void Notify(String notificationTitle, String notificationMessage) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Notification notification = new Notification(R.drawable.ic_b2b_notif,
                "Pay1 ", System.currentTimeMillis());

        notification.number = Constants.COUNT_NOTIFY++;
        intent = new Intent(BROADCAST_ACTION);

        intent.putExtra("time", new Date().toLocaleString());
        intent.putExtra("counter", String.valueOf(Constants.COUNT_NOTIFY));
        sendBroadcast(intent);
        Utility.setNotificationCount(mContext, "NotiCount",
                Constants.COUNT_NOTIFY);
        Intent notificationIntent = new Intent(this, NotificationActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        try {
            Uri notification1 = RingtoneManager
                    .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(),
                    notification1);
            r.play();

            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

            v.vibrate(1000);
        } catch (Exception e) {
        }


        //  Notification notification = null;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            //notification = new Notification();
            notification.icon = R.drawable.ic_launcher;
            //notification.
            try {
                Method deprecatedMethod = notification.getClass().getMethod("setLatestEventInfo", Context.class, CharSequence.class, CharSequence.class, PendingIntent.class);
                deprecatedMethod.invoke(notification, mContext, notificationTitle, notificationMessage,
                        pendingIntent);
            } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException
                    | InvocationTargetException e) {
                Log.w(TAG, "Method not found", e);
            }
        } else {
            // Use new API
            Notification.Builder builder = new Notification.Builder(mContext)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentText(notificationMessage)
                    .setContentIntent(pendingIntent)
                    .setContentTitle(notificationTitle);
            notification = builder.build();
        }

		/*notification.setLatestEventInfo(mContext, notificationTitle,
				notificationMessage, pendingIntent);*/
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(9999, notification);
    }

    private void showBanner(String type, String description, String msg) {
        NotificationDataSource notificationDataSource = new NotificationDataSource(
                mContext);
        try {
            notificationDataSource.open();
            notificationDataSource.createNotification(description, 1, ""
                    + System.currentTimeMillis());
        } catch (SQLException exception) {

        } catch (Exception e) {

        } finally {
            notificationDataSource.close();
        }
        Intent notifyIntent = new Intent(this, NotificationActivity.class);
        notifyIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        notifyIntent.putExtra("pushBundle", msg);

        mContext.startActivity(notifyIntent);
    }

    public class CheckBalanceTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method", "updateBal"));
            String response = RequestClass.getInstance().readPay1Request(
                    gcmListenerService.this, Constants.API_URL, listValuePair);
            listValuePair.clear();

            return response;

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");

                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");

                    if (status.equalsIgnoreCase("success")) {
                        if (jsonObject.getString("login").equalsIgnoreCase("1")) {
                            Utility.setBalance(gcmListenerService.this,
                                    Constants.SHAREDPREFERENCE_BALANCE,
                                    jsonObject.getString("description"));
                        } else {

                        }
                    }
                }

            } catch (JSONException e) {

            } catch (Exception e) {

            }
        }
    }

    public void showC2DNotification(int countNoti, int delFlag) {

        String wsNotificationPreference = Utility
                .getWholesaleNotificationSharedPreferences(mContext);
        if (wsNotificationPreference.equalsIgnoreCase("0")) {

        } else {


            NotificationUtil notificationUtil = new NotificationUtil();
            Intent notificationIntent = new Intent(mContext, MainActivity.class);
            notificationIntent.putExtra("requestFrom", "fromNotification");
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                    notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            // if (delFlag == 1) {
            countNoti = countNoti + 1;
            try {
                Uri notification1 = RingtoneManager
                        .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(),
                        notification1);
                r.play();

                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

                v.vibrate(500);
            } catch (Exception e) {
            }

            String notiMsg = (countNoti) + " unseen wholesale post(s).";
            Notification c2dNotification = notificationUtil.createNotification(mContext,
                    pendingIntent, "Pay1 Wholesale",
                    notiMsg, R.drawable.ic_b2b_notif);

            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            // if (countNoti <= 0) {
            notificationManager.notify(C2D_NOTIFICATION_ID, c2dNotification);

        }

        // }
    }

    private void writeToFile(Context context, String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput
                    ("testPay1.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public static void writeStringAsFile(Context mContext, final String fileContents, String
            fileName) {
        try {
            // Creates a trace file in the primary external storage space of the
            // current application.
            // If the file does not exists, it is created.
            File traceFile = new File((mContext).getExternalFilesDir(null), "TraceFile.txt");
            if (!traceFile.exists())
                traceFile.createNewFile();
            // Adds a line to the trace file
            BufferedWriter writer = new BufferedWriter(new FileWriter(traceFile, true /*append*/));
            writer.write(fileContents);
            writer.close();
            // Refresh the data so it can seen when the device is plugged in a
            // computer. You may have to unplug and replug the device to see the
            // latest changes. This is not necessary if the user should not modify
            // the files.
            MediaScannerConnection.scanFile((Context) (mContext),
                    new String[]{traceFile.toString()},
                    null,
                    null);

        } catch (IOException e) {
            //Log.e("com.cindypotvin.FileTest", "Unable to write to the TraceFile.txt file.");
        }
    }

    public static void cancelC2DNotification(Context ctx, int notifyId) {

        try {
            String ns = Context.NOTIFICATION_SERVICE;
            NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);

            nMgr.cancel(notifyId);
        } catch (Exception exc) {
            //nMgr.cancelAll();
        }
    }
}
