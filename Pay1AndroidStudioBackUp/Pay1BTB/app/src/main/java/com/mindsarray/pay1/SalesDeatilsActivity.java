package com.mindsarray.pay1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.SalesDetailsAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

public class SalesDeatilsActivity extends Activity {
    LinearLayout layoutMobile;
    private static final String TAG = "Sales details";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.salesdetails_activity);
        layoutMobile = (LinearLayout) findViewById(R.id.layoutMobile);
        tracker = Constants.setAnalyticsTracker(this, tracker);
        new EarningTask().execute();
    }

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        // tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
    }

    public class EarningTask extends AsyncTask<String, String, String>
            implements OnDismissListener {
        ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method", "saleReport"));
            listValuePair.add(new BasicNameValuePair("device_type", "android"));
            listValuePair.add(new BasicNameValuePair("date", getIntent()
                    .getExtras().getString(Constants.DATE)));
            String response = RequestClass.getInstance().readPay1Request(
                    SalesDeatilsActivity.this, Constants.API_URL,
                    listValuePair);
            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (!result.startsWith("Error")) {

                try {
                    if (!result.startsWith("Error")) {
                        String replaced = result.replace("(", "")
                                .replace(")", "").replace(";", "");
                        JSONArray array = new JSONArray(replaced);
                        JSONObject jsonObject = array.getJSONObject(0);
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("success")) {
                            JSONArray descArray = jsonObject
                                    .getJSONArray("description");
                            JSONObject jsonObject2 = descArray.getJSONObject(0);
                            Iterator keyss = jsonObject2.keys();
                            while (keyss.hasNext()) {
                                double amount = 0, counts = 0, income = 0;

                                String type = (String) keyss.next().toString();
                                Log.d("1", type);
                                ArrayList<JSONObject> mMobArrayList = new ArrayList<JSONObject>();

                                layoutMobile.setVisibility(View.VISIBLE);
                                JSONObject jsonObject3 = jsonObject2
                                        .getJSONObject(type);
                                JSONArray dataArray = jsonObject3
                                        .getJSONArray("data");
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject jsonObject4 = dataArray
                                            .getJSONObject(i);

                                    JSONObject jsonObject5 = jsonObject4
                                            .getJSONObject("0");
                                    mMobArrayList.add(jsonObject4);

                                    amount = amount
                                            + Double.parseDouble(jsonObject5
                                            .getString("amount"));
                                    counts = counts
                                            + Double.parseDouble(jsonObject5
                                            .getString("counts"));
                                    income = income
                                            + Double.parseDouble(jsonObject5
                                            .getString("income"));

                                }
                                View view;
                                LayoutInflater mInflater = (LayoutInflater) SalesDeatilsActivity.this
                                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                                view = mInflater.inflate(
                                        R.layout.earning_details_layout,
                                        layoutMobile, false);

                                TextView textTotalSale = (TextView) view
                                        .findViewById(R.id.textTotalSale);
                                TextView textTotalIncome1 = (TextView) view
                                        .findViewById(R.id.textTotalIncome1);
                                TextView textTotalTransaction = (TextView) view
                                        .findViewById(R.id.textTotalTransaction);

                                ListView mMobileList = (ListView) view
                                        .findViewById(R.id.mMobileList);
                                SalesDetailsAdapter mMobSaleAdapter = new SalesDetailsAdapter(
                                        SalesDeatilsActivity.this, 1,
                                        mMobArrayList);

                                mMobileList.setAdapter(mMobSaleAdapter);

                                textTotalSale.setText("" + amount);
                                textTotalIncome1.setText(""
                                        + new DecimalFormat("##.##")
                                        .format(income));
                                textTotalTransaction.setText("" + counts);
                                layoutMobile.addView(view);
                                mMobSaleAdapter.notifyDataSetChanged();

                            }
                        } else {
                            Constants.showOneButtonFailureDialog(
                                    SalesDeatilsActivity.this,
                                    Constants.checkCode(replaced), TAG,
                                    Constants.OTHER_ERROR);
                        }

                    } else {
                        Constants.showOneButtonFailureDialog(
                                SalesDeatilsActivity.this,
                                Constants.ERROR_INTERNET, TAG,
                                Constants.OTHER_ERROR);

                    }
                } catch (JSONException e) {
                    // e.printStackTrace();
                    Constants.showOneButtonFailureDialog(
                            SalesDeatilsActivity.this,
                            Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                            Constants.OTHER_ERROR);
                } catch (Exception e) {
                    // TODO: handle exception
                    Constants.showOneButtonFailureDialog(
                            SalesDeatilsActivity.this,
                            Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                            Constants.OTHER_ERROR);
                }
            }
            if (this.dialog.isShowing())
                this.dialog.dismiss();
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(SalesDeatilsActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            EarningTask.this.cancel(true);
                            dialog.cancel();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            EarningTask.this.cancel(true);
            dialog.cancel();
        }

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Constants.showNavigationBar(SalesDeatilsActivity.this);
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (Constants.isExpanded) {
            Constants.collapsView();
        } else {
            super.onBackPressed();
        }
    }
}






/*package com.mindsarray.pay1;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.JsonObject;
import com.mindsarray.pay1.adapterhandler.SalesDetailsAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;

public class SalesDeatilsActivity extends Activity {
	ArrayList<JSONObject> mMobArrayList, mDthArrayList, mEntArrayList;
	ListView mMobileList, mEntList, mDthList;
	LinearLayout layoutMobile, layoutDth, layoutEnt;
	SalesDetailsAdapter mMobSaleAdapter, mDthAdapter, mEntAdapter;
	TextView textDateToday, textSaleToday, textEarningToday,
			textTotalTransaction, textTotalIncome1, textTotalSale,
			textTotalTransaction1, textTotalIncome2, textTotalSale1,
			textTotalTransaction2, textTotalIncome3, textTotalSale2;

	int amount = 0, counts = 0;
	double income = 0, income1 = 0, income2 = 0;
	int amount1 = 0, counts1 = 0;
	int amount2 = 0, counts2 = 0;
	private static final String TAG = "Sales details";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.salesdetails_activity);
		findViewById();
		 tracker = Constants.setAnalyticsTracker(this, tracker);
		mMobArrayList = new ArrayList<JSONObject>();
		mDthArrayList = new ArrayList<JSONObject>();
		mEntArrayList = new ArrayList<JSONObject>();

		mMobSaleAdapter = new SalesDetailsAdapter(SalesDeatilsActivity.this, 1,
				mMobArrayList);
		mDthAdapter = new SalesDetailsAdapter(SalesDeatilsActivity.this, 1,
				mDthArrayList);
		mEntAdapter = new SalesDetailsAdapter(SalesDeatilsActivity.this, 1,
				mEntArrayList);

		mMobileList.setAdapter(mMobSaleAdapter);
		mDthList.setAdapter(mDthAdapter);
		mEntList.setAdapter(mEntAdapter);

		new EarningTask().execute();

	}
	public static GoogleAnalytics analytics; public static Tracker tracker;	
	@Override
		public void onStart() {
			super.onStart();
			// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
			// EasyTracker.getInstance(this).activityStart(this);
			//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
		}
	public class EarningTask extends AsyncTask<String, String, String>
			implements OnDismissListener {

		ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair.add(new BasicNameValuePair("method", "saleReport"));
			listValuePair.add(new BasicNameValuePair("device_type", "android"));
			listValuePair.add(new BasicNameValuePair("date", getIntent()
					.getExtras().getString(Constants.DATE)));
			String response = RequestClass.getInstance()
					.readPay1Request(SalesDeatilsActivity.this,
							Constants.API_URL, listValuePair);
			listValuePair.clear();
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (!result.startsWith("Error")) {

				try {
					if (!result.startsWith("Error")) {
						String replaced = result.replace("(", "")
								.replace(")", "").replace(";", "");
						JSONArray array = new JSONArray(replaced);
						JSONObject jsonObject = array.getJSONObject(0);
						String status = jsonObject.getString("status");
						if (status.equalsIgnoreCase("success")) {
							JSONArray descArray = jsonObject
									.getJSONArray("description");
							JSONObject jsonObject2 = descArray.getJSONObject(0);
							Iterator keyss=	jsonObject2.keys();
							while (keyss.hasNext()) {
								String type = (String) keyss.next().toString();
								Log.d("1", type);
							}

							if (jsonObject2.has("1")) {
								layoutMobile.setVisibility(View.VISIBLE);
								JSONObject jsonObject3 = jsonObject2
										.getJSONObject("1");
								JSONArray dataArray = jsonObject3
										.getJSONArray("data");
								for (int i = 0; i < dataArray.length(); i++) {
									JSONObject jsonObject4 = dataArray
											.getJSONObject(i);

									JSONObject jsonObject5 = jsonObject4
											.getJSONObject("0");
									mMobArrayList.add(jsonObject4);

									amount = amount
											+ Integer.parseInt(jsonObject5
													.getString("amount"));
									counts = counts
											+ Integer.parseInt(jsonObject5
													.getString("counts"));
									income = income
											+ Double.parseDouble(jsonObject5
													.getString("income"));

								}

								textTotalSale.setText("" + amount);
								textTotalIncome1.setText(""
										+ new DecimalFormat("##.##")
												.format(income));
								textTotalTransaction.setText("" + counts);
								mMobSaleAdapter.notifyDataSetChanged();
							}

							if (jsonObject2.has("3")||jsonObject2.has("5")||jsonObject2.has("6")||jsonObject2.has("7")) {
							//Iterator keyss=	jsonObject2.keys();
							String typeKeys = keyss.next().toString();
								JSONObject jsonObj = jsonObject2
										.getJSONObject(typeKeys);
								JSONArray array3 = jsonObj.getJSONArray("data");
								layoutEnt.setVisibility(View.VISIBLE);
								for (int i = 0; i < array3.length(); i++) {
									JSONObject jsonObject4 = array3
											.getJSONObject(i);

									JSONObject jsonObject5 = jsonObject4
											.getJSONObject("0");
									amount2 = amount2
											+ Integer.parseInt(jsonObject5
													.getString("amount"));
									counts2 = counts2
											+ Integer.parseInt(jsonObject5
													.getString("counts"));
									income2 = income2
											+ Double.parseDouble(jsonObject5
													.getString("income"));

									mEntArrayList.add(jsonObject4);
								}

								textTotalSale2.setText("" + amount2);
								textTotalIncome3.setText(""
										+ new DecimalFormat("##.##")
												.format(income2));
								textTotalTransaction2.setText("" + counts2);
								mEntAdapter.notifyDataSetChanged();
							}

							if (jsonObject2.has("2")) {
								JSONObject json = jsonObject2
										.getJSONObject("2");
								JSONArray array2 = json.getJSONArray("data");
								layoutDth.setVisibility(View.VISIBLE);
								for (int i = 0; i < array2.length(); i++) {
									JSONObject jsonObject4 = array2
											.getJSONObject(i);

									JSONObject jsonObject5 = jsonObject4
											.getJSONObject("0");
									amount1 = amount1
											+ Integer.parseInt(jsonObject5
													.getString("amount"));
									counts1 = counts1
											+ Integer.parseInt(jsonObject5
													.getString("counts"));
									income1 = income1
											+ Integer.parseInt(jsonObject5
													.getString("income"));

									mDthArrayList.add(jsonObject4);
								}

								textTotalSale1.setText("" + amount1);
								textTotalIncome2.setText(""
										+ new DecimalFormat("##.##")
												.format(income1));
								textTotalTransaction1.setText("" + counts1);
								mDthAdapter.notifyDataSetChanged();
							}

						} else {
							Constants.showOneButtonFailureDialog(
									SalesDeatilsActivity.this,
									Constants.checkCode(replaced), TAG,
									Constants.OTHER_ERROR);
						}

					} else {
						Constants.showOneButtonFailureDialog(
								SalesDeatilsActivity.this,
								Constants.ERROR_INTERNET, TAG,
								Constants.OTHER_ERROR);

					}
				} catch (JSONException e) {
					// e.printStackTrace();
					Constants.showOneButtonFailureDialog(
							SalesDeatilsActivity.this,
							Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
							Constants.OTHER_ERROR);
				} catch (Exception e) {
					// TODO: handle exception
					Constants.showOneButtonFailureDialog(
							SalesDeatilsActivity.this,
							Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
							Constants.OTHER_ERROR);
				}
			}
			if (this.dialog.isShowing())
				this.dialog.dismiss();
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(SalesDeatilsActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							EarningTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			EarningTask.this.cancel(true);
			dialog.cancel();
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants.showNavigationBar(SalesDeatilsActivity.this);
		super.onResume();
	}

	private void findViewById() {
		// TODO Auto-generated method stub
		textTotalTransaction = (TextView) findViewById(R.id.textTotalTransaction);
		textTotalIncome1 = (TextView) findViewById(R.id.textTotalIncome1);
		textTotalSale = (TextView) findViewById(R.id.textTotalSale);

		textTotalTransaction1 = (TextView) findViewById(R.id.textTotalTransaction1);
		textTotalIncome2 = (TextView) findViewById(R.id.textTotalIncome2);
		textTotalSale1 = (TextView) findViewById(R.id.textTotalSale1);

		textTotalTransaction2 = (TextView) findViewById(R.id.textTotalTransaction2);
		textTotalIncome3 = (TextView) findViewById(R.id.textTotalIncome3);
		textTotalSale2 = (TextView) findViewById(R.id.textTotalSale2);

		layoutMobile = (LinearLayout) findViewById(R.id.layoutMobile);
		layoutDth = (LinearLayout) findViewById(R.id.layoutDth);
		layoutEnt = (LinearLayout) findViewById(R.id.layoutEnt);

		mEntList = (ListView) findViewById(R.id.mEntList);
		mDthList = (ListView) findViewById(R.id.mDthList);
		mMobileList = (ListView) findViewById(R.id.mMobileList);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}
}
*/