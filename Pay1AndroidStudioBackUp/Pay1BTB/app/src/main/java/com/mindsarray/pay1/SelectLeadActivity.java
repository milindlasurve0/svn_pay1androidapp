package com.mindsarray.pay1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.mindsarray.pay1.utilities.Utility;

/**
 * Created by Administrator on 6/21/2016.
 */
public class SelectLeadActivity extends Activity {
    Button buttonRetailerProceed, buttonDistributorProceed, buttonPersonalProceed;
    EditText editOtp;
    ImageView imageViewClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_lead_activity);
        imageViewClose = (ImageView) findViewById(R.id.imageViewClose);
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        buttonRetailerProceed = (Button) findViewById(R.id.buttonRetailerProceed);

        buttonDistributorProceed = (Button) findViewById(R.id.buttonDistributorProceed);
        buttonPersonalProceed = (Button) findViewById(R.id.buttonPersonalProceed);

        editOtp = (EditText) findViewById(R.id.editOtp);
        editOtp.setText(Utility.getUserMobileNumber(SelectLeadActivity.this));
        /*if (getIntent().getExtras() != null) {
            editOtp.setText(getIntent().getStringExtra("MobileNumber"));
        }*/
        Utility.setSharedPreference(SelectLeadActivity.this, "Distributor_interest", "");


        buttonPersonalProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = "com.pay1"; // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        buttonRetailerProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    if (editOtp.getText().toString().matches("[7-9][0-9]{9}$")) {

                    } else {
                        setErrorToEditText(SelectLeadActivity.this, "Please enter correct mobile",
                                editOtp);
                        return;
                    }
                    Utility.setSharedPreference(SelectLeadActivity.this,
                            "Registration_interest", "Retailer");
                    if (editOtp.getText().toString().trim().length() == 10) {
                        Intent intent = new Intent(SelectLeadActivity.this, RegistrationActivity.class);
                        intent.putExtra("MobileNumber", editOtp.getText().toString().trim());
                        startActivity(intent);
                    } else {
                        int ecolor = R.color.black;
                        ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
                        String errorMessage = "Eneter correct mobile number";
                        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
                                errorMessage);
                        ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
                        editOtp.setError(ssbuilder);
                        editOtp.setBackgroundDrawable(SelectLeadActivity.this.getResources().getDrawable(
                                R.drawable.edittext_error_background_selector));
                        editOtp.setPadding(10, 10, 10, 10);
                        editOtp.setTextSize(14);
                        editOtp.requestFocus();
                    }
                } catch (android.content.ActivityNotFoundException anfe) {

                }
            }
        });

        buttonDistributorProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    if (editOtp.getText().toString().matches("[7-9][0-9]{9}$")) {

                    } else {
                        setErrorToEditText(SelectLeadActivity.this, "Please enter correct mobile",
                                editOtp);
                        return;
                    }


                    Utility.setSharedPreference(SelectLeadActivity.this,
                            "Registration_interest", "Distributor");

                    if (editOtp.getText().toString().length() == 10) {

                        Intent intent = new Intent(SelectLeadActivity.this, DistributorRegistrationActivity.class);
                        intent.putExtra("MobileNumber", editOtp.getText().toString().trim());
                        startActivity(intent);
                    } else {
                        int ecolor = R.color.black;
                        ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
                        String errorMessage = "Eneter correct mobile number";
                        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
                                errorMessage);
                        ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
                        editOtp.setError(ssbuilder);
                        editOtp.setBackgroundDrawable(SelectLeadActivity.this.getResources().getDrawable(
                                R.drawable.edittext_error_background_selector));
                        editOtp.setPadding(10, 10, 10, 10);
                        editOtp.setTextSize(14);
                        editOtp.requestFocus();
                    }
                } catch (android.content.ActivityNotFoundException anfe) {

                }
            }
        });

    }

    private void setErrorToEditText(Context context, String errorMessage, EditText editText) {
        int ecolor = R.color.black;
        ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
                errorMessage);
        ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
        editText.setError(ssbuilder);
        editText.setBackgroundDrawable(context.getResources().getDrawable(
                R.drawable.edittext_error_background_selector));
        editText.setPadding(10, 10, 10, 10);
        editText.setTextSize(14);
        editText.requestFocus();
    }
}
