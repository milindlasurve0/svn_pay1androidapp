package com.mindsarray.pay1.receiverhandler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.mindsarray.pay1.databasehandler.NotificationDataSource;

public class SMSBroadcastReceiver extends BroadcastReceiver {

    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

    // private static final String TAG = "SMSBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        // Log.i(TAG, "Intent received: " + intent.getAction());
        try {
            if (intent.getAction().equals(SMS_RECEIVED)) {
                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    final SmsMessage[] messages = new SmsMessage[pdus.length];
                    for (int i = 0; i < pdus.length; i++) {
                        messages[i] = SmsMessage
                                .createFromPdu((byte[]) pdus[i]);
                        if (messages.length > -1) {

                            String FROM = messages[i].getOriginatingAddress();

                            // ContentValues contentValues = new
                            // ContentValues();
                            // contentValues.put("notification_msg",
                            // messages[0].getMessageBody());
                            // contentValues.put("timestamp ", "1234567890123");
                            // contentValues.put("netOrSms ", 1);
                            // if (FROM.contains("PAYONE")) {
                            // controller.insertNotifications(contentValues);
                            // }

                            if (FROM.contains("PAYONE")) {
                                NotificationDataSource notificationDataSource = new NotificationDataSource(
                                        context);
                                try {
                                    Log.e("SMS Broadcast Receiver",
                                            "SMS Received (Notification Created) ");
                                    notificationDataSource.open();
                                    notificationDataSource.createNotification(
                                            messages[0].getMessageBody(), 1,
                                            "" + System.currentTimeMillis());
                                } catch (SQLException exception) {
                                    // TODO: handle exception
                                    // // Log.e("Er ", exception.getMessage());
                                } catch (Exception e) {
                                    // TODO: handle exception
                                    // // Log.e("Er ", e.getMessage());
                                } finally {
                                    notificationDataSource.close();
                                }
                            }
                            // Constants.showCustomToast(
                            // context,
                            // "Umesh Message recieved: "
                            // + messages[0].getMessageBody());
                        }
                    }

                }
            }
        } catch (Exception exc) {

        }
    }
}