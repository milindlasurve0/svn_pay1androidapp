package com.mindsarray.pay1.databasehandler;

public class VMN {

    private int vmnID;
    private String vmnNumber;

    public int getVmnID() {
        return vmnID;
    }

    public void setVmnID(int vmnID) {
        this.vmnID = vmnID;
    }

    public String getVmnNumber() {
        return vmnNumber;
    }

    public void setVmnNumber(String vmnNumber) {
        this.vmnNumber = vmnNumber;
    }


}
