package com.mindsarray.pay1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.TopUpAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TopUpActivity extends Activity {

    private TextView mDateDisplay;
    private Button btnPreWeek, btnNextWeek;
    // static final int DATE_DIALOG_ID = 0;
    ListView mTopUpListView;
    ArrayList<JSONObject> mTopUpList;
    TopUpAdapter adapter;
    String date = "";
    String preWeek, nextWeek, currentWeek;
    private static final String TAG = "Topup";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
            // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            // WindowManager.LayoutParams.FLAG_FULLSCREEN);

            setContentView(R.layout.topup_activity);
            tracker = Constants.setAnalyticsTracker(this, tracker);
            //easyTracker.set(Fields.SCREEN_NAME, TAG);
            findViewById();
            TextView textView_NoData = (TextView) findViewById(R.id.textView_NoData);
            mTopUpListView.setEmptyView(textView_NoData);
            mTopUpList = new ArrayList<JSONObject>();
            adapter = new TopUpAdapter(TopUpActivity.this, 1, mTopUpList);
            mTopUpListView.setAdapter(adapter);

            btnNextWeek.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    date = nextWeek;
                    // if (date.length() == 0) {
                    // Utility.alertBoxShow(TopUpActivity.this,
                    // "Can not Proceed");
                    // } else {
                    new GetTopUpTask().execute();
                    // }

                }
            });

            btnPreWeek.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    date = preWeek;
                    new GetTopUpTask().execute();
                }
            });
        } catch (Exception exception) {
        }
    }

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        //tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.LOGIN) {
                mInsideonCreate();
            } else {

            }
        } else {
            finish();
        }
    }

    private void mInsideonCreate() {

        new GetTopUpTask().execute();

    }

    private void findViewById() {
        mDateDisplay = (TextView) findViewById(R.id.mDateDisplay);
        mTopUpListView = (ListView) findViewById(R.id.mTopUpListView);
        btnPreWeek = (Button) findViewById(R.id.btnPreWeek);
        btnNextWeek = (Button) findViewById(R.id.btnNextWeek);
    }

    public class GetTopUpTask extends AsyncTask<String, String, String>
            implements OnDismissListener {

        ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method", "topups"));
            listValuePair.add(new BasicNameValuePair("date", date));
            listValuePair.add(new BasicNameValuePair("device_type", "android"));
            String response = RequestClass.getInstance().readPay1Request(
                    TopUpActivity.this, Constants.API_URL, listValuePair);
            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            super.onPostExecute(result);
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    mTopUpList.clear();
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray jsonDescription = jsonObject
                                .getJSONArray("description");
                        JSONArray array2 = jsonDescription.getJSONArray(0);
                        for (int i = 0; i < array2.length(); i++) {
                            JSONArray json_data = array2.getJSONArray(i);
                            JSONObject jsonObject2 = json_data.getJSONObject(0);

                            mTopUpList.add(jsonObject2);

                        }
                        JSONArray preArray = jsonObject
                                .getJSONArray("prevWeek");
                        JSONArray nextArray = jsonObject
                                .getJSONArray("nextWeek");
                        JSONArray curArray = jsonObject
                                .getJSONArray("currWeek");
                        preWeek = preArray.getString(0);
                        nextWeek = nextArray.getString(0);
                        currentWeek = curArray.getString(0);
                        mDateDisplay.setText(currentWeek);
                        // Log.d("Arra", "Arra");
                        adapter.notifyDataSetChanged();

                        if (nextWeek.length() == 0)
                            btnNextWeek.setVisibility(View.GONE);
                        else
                            btnNextWeek.setVisibility(View.VISIBLE);
                    } else {
                        Constants.showOneButtonFailureDialog(
                                TopUpActivity.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(TopUpActivity.this,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(TopUpActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(TopUpActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(TopUpActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            GetTopUpTask.this.cancel(true);
                            dialog.cancel();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            GetTopUpTask.this.cancel(true);
            dialog.cancel();
        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Constants.showNavigationBar(TopUpActivity.this);
        if (Utility.getLoginFlag(TopUpActivity.this,
                Constants.SHAREDPREFERENCE_IS_LOGIN)) {
            mInsideonCreate();

        } else {
            Intent intent = new Intent(TopUpActivity.this, LoginActivity.class);

            startActivityForResult(intent, Constants.LOGIN);

        }
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (Constants.isExpanded) {
            Constants.collapsView();
        } else {
            super.onBackPressed();
        }
    }
}
