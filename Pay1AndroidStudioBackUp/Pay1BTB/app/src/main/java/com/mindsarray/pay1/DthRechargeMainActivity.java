package com.mindsarray.pay1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.RechargeMainAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.utilities.GridMenuItem;

import java.util.ArrayList;

public class DthRechargeMainActivity extends Activity {
    GridView gridView;
    ArrayList<GridMenuItem> gridArray = new ArrayList<GridMenuItem>();
    RechargeMainAdapter customGridAdapter;
    private static final String TAG = "Dth Recharge Activity";
    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        // WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.rechargemain_activity);

//		tracker = Constants.setAnalyticsTracker(this, tracker);
        //analytics = GoogleAnalytics.getInstance(this);
        tracker = Constants.setAnalyticsTracker(this, tracker);
        int mAirtel = R.drawable.d_dishtv;
        int mVodafone = R.drawable.d_tatasky;
        int mBsnl = R.drawable.d_sundirect;
        int mIdea = R.drawable.d_bigtv;
        int mAircel = R.drawable.m_airtel;
        int mRelainceCdma = R.drawable.d_videocon;

        gridArray.add(new GridMenuItem(mAirtel, "Dish TV",
                Constants.OPERATOR_DTH_DISHTV, Constants.RECHARGE_DTH));
        gridArray.add(new GridMenuItem(mVodafone, "Tata Sky",
                Constants.OPERATOR_DTH_TATA_SKY, Constants.RECHARGE_DTH));
        gridArray.add(new GridMenuItem(mBsnl, "Sun Direct",
                Constants.OPERATOR_DTH_SUN, Constants.RECHARGE_DTH));
        gridArray.add(new GridMenuItem(mIdea, "Big TV",
                Constants.OPERATOR_DTH_BIG, Constants.RECHARGE_DTH));
        gridArray.add(new GridMenuItem(mAircel, "Airtel",
                Constants.OPERATOR_DTH_AIRTEL, Constants.RECHARGE_DTH));
        gridArray.add(new GridMenuItem(mRelainceCdma, "Videocon",
                Constants.OPERATOR_DTH_VIDEOCON, Constants.RECHARGE_DTH));

        gridView = (GridView) findViewById(R.id.gridView1);
        customGridAdapter = new RechargeMainAdapter(this,
                R.layout.rechargemain_adapter, gridArray, false);
        gridView.setAdapter(customGridAdapter);

        gridView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {
                Intent intent = new Intent(DthRechargeMainActivity.this,
                        DthRechargeActivity.class);
                intent.putExtra(Constants.OPERATOR_ID, gridArray.get(position)
                        .getId());
                intent.putExtra(Constants.PLAN_JSON,
                        String.valueOf(gridArray.get(position).getId()));
                intent.putExtra(Constants.OPERATOR_LOGO, gridArray
                        .get(position).getImage());
                intent.putExtra(Constants.OPERATOR_NAME, gridArray
                        .get(position).getTitle());
                startActivity(intent);

            }

        });

    }

    //	public static GoogleAnalytics analytics; public static Tracker tracker;
    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
//			//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
        //tracker.setScreenName(TAG);
        Constants.trackOnStart(tracker, TAG);
        //.build());
    }
}
