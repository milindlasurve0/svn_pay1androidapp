package com.mindsarray.pay1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.utilities.Utility;

import java.util.Locale;

public class LanguageSelectionActivity extends Activity {
    CheckBox chkEnglish, chkHindi;
    Locale myLocale;
    Button buttonSave;
    Context mContext = LanguageSelectionActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.language_selection_activity);
        String languagePreference = Utility
                .getLanguageSharedPreferences(LanguageSelectionActivity.this);
        buttonSave = (Button) findViewById(R.id.buttonSave);
        chkEnglish = (CheckBox) findViewById(R.id.chkEnglish);
        chkHindi = (CheckBox) findViewById(R.id.chkHindi);

        if (languagePreference.equalsIgnoreCase("hi")) {
            chkHindi.setChecked(true);
            chkEnglish.setChecked(false);
        } else {
            chkHindi.setChecked(false);
            chkEnglish.setChecked(true);
        }

        chkEnglish.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                chkHindi.setChecked(!isChecked);
                chkEnglish.setChecked(isChecked);
            }
        });

        chkHindi.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                chkHindi.setChecked(isChecked);
                chkEnglish.setChecked(!isChecked);
            }
        });

        buttonSave.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String message = "Are you sure you want to change language? The app will restart now.";
                Runnable buttonNow = new Runnable() {
                    @Override
                    public void run() {
                        if (chkHindi.isChecked()) {
                            Utility.setLanguageSharedPreference(
                                    LanguageSelectionActivity.this, "hi");
                            setLocale("hi");

                        } else if (chkEnglish.isChecked()) {

                            Utility.setLanguageSharedPreference(
                                    LanguageSelectionActivity.this, "en");
                            setLocale("en");
                        }

                        // Intent intent = new Intent(Intent.ACTION_MAIN);
                        Intent intent = new Intent(LanguageSelectionActivity.this,
                                SplashScreenActivity.class);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                };
                Constants.showTwoButtonDialog(mContext, message, "Change Language", "Confirm", buttonNow, "Cancel");

            }
        });

    }

    public void setLocale(String lang) {

        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, LanguageSelectionActivity.class);
        startActivity(refresh);
        finish();

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Constants.showNavigationBar(LanguageSelectionActivity.this);
        super.onResume();
    }

}
