package com.mindsarray.pay1;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

public class SpinnerTextView extends TextView implements View.OnClickListener {
    private String mPrompt;
    public CharSequence[] mEntries;
    private int mSelection;
    public ArrayAdapter<String> itemsAdapter;
    private ListView dialogListView;
    private Context mContext;
    public AlertDialog mDialog;
    public View spinnerDialogView;

    public SpinnerTextView(Context context) {
        super(context);
        mContext = context;
        init(null);
    }

    public SpinnerTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init(attrs);
    }

    public SpinnerTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.SpinnerTextView);

            mPrompt = typedArray.getString(R.styleable.SpinnerTextView_android_prompt);
            mEntries = typedArray.getTextArray(R.styleable.SpinnerTextView_android_entries);

            typedArray.recycle();
        }

        mSelection = -1;
        mPrompt = (mPrompt == null) ? "" : mPrompt;

        setText(mPrompt);
        setOnClickListener(this);

        buildDialog();
    }

    public void buildDialog() {
        try {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            spinnerDialogView = inflater.inflate(R.layout.spinner_dialog, null);
            ArrayList<String> entries = new ArrayList(Arrays.asList(mEntries));
            itemsAdapter = new ArrayAdapter<String>(mContext, R.layout.spinner_list_item, entries);
            dialogListView = (ListView) spinnerDialogView.findViewById(R.id.spinner_dialog_list_view);
            dialogListView.setAdapter(itemsAdapter);

            TextView spinnerDialogTitle = (TextView) spinnerDialogView.findViewById(R.id.spinner_dialog_title);
            spinnerDialogTitle.setText(mPrompt);

            final EditText spinnerDialogSearchBox = (EditText) spinnerDialogView.findViewById(R.id.spinner_dialog_search_box);
            spinnerDialogSearchBox.setHintTextColor(R.color.gray);

            final LinearLayout spinnerDialogHead = (LinearLayout) spinnerDialogView.findViewById(R.id.spinner_dialog_head);
            mDialog = new AlertDialog.Builder(mContext)
                    .setView(spinnerDialogView)
                    .create();
            dialogListView.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    try {
                        mSelection = position;
                        setText(mEntries[mSelection]);
                        if (mDialog.isShowing())
                            mDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            mDialog.setOnDismissListener(new OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    spinnerDialogSearchBox.setVisibility(View.GONE);
                    spinnerDialogHead.setVisibility(View.VISIBLE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getSelectedItem() {
        if (mSelection < 0 || mSelection >= mEntries.length) {
            return null;
        } else {
            return mEntries[mSelection].toString();
        }
    }

    public int getSelectedItemPosition() {
        return mSelection;
    }

    public void setSelection(int selection) {
        mSelection = selection;

        if (selection < 0) {
            setText(mPrompt);
        } else if (selection < mEntries.length) {
            setText(mEntries[mSelection]);
        }
    }

    @Override
    public void onClick(View v) {
        mDialog.show();
    }

    public void update(ArrayList<String> entries) {
        final CharSequence[] charSequenceItems = entries.toArray(new CharSequence[entries.size()]);

        mEntries = charSequenceItems;
        itemsAdapter.clear();
        itemsAdapter.addAll(entries);
        itemsAdapter.notifyDataSetChanged();
    }
}