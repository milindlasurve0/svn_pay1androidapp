package com.mindsarray.pay1;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.constant.Constants;

public class KycShopImageSampleActivity extends Activity {
    public static final String TAG = "KYC Shop Image Tutorial";

    Button button_Ok;
    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kyc_shop_image_sample_activity);
        button_Ok = (Button) findViewById(R.id.buttonOk);

        tracker = Constants.setAnalyticsTracker(this, tracker);
        button_Ok.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        Constants.trackOnStart(tracker, TAG);
    }

    @Override
    protected void onResume() {

        Constants.showNavigationBar(KycShopImageSampleActivity.this);
        super.onResume();
    }

    @Override
    public void onBackPressed() {

        if (Constants.isExpanded) {
            Constants.collapsView();
        } else {
            super.onBackPressed();
        }
    }

}
