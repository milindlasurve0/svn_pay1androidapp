/**
 *
 */
package com.mindsarray.pay1;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.EditTextValidator;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * @author Umesh
 */
public class PGActivity extends Activity {
    Button btnTopUp;
    EditText editTopUP;
    long timestamp;
    String amount, mobileNumber;
    private static final String TAG = "Payment Gateway";
    int serviceCharge;
    String OTA_Fee;
    String OTA_FeeText = "";

    private static final int REQUEST_CODE_PAYMENT = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pg_activity);
        findViewById();
        tracker = Constants.setAnalyticsTracker(this, tracker);
        //easyTracker.set(Fields.SCREEN_NAME, TAG);
        TextView textInfo = (TextView) findViewById(R.id.textInfo);

        serviceCharge = Utility.getServiceChargeForPG(PGActivity.this);
        /*if(serviceCharge!=0){
		textInfo.setText("*Rs. " + serviceCharge
				+ " service charge inclusive of all taxes.");
		}*/

        textInfo.setText("");
        TextView textOTA_fee = (TextView) findViewById(R.id.textOTA_fee);

        OTA_Fee = Utility.getSharedPreferences(PGActivity.this, "OTA_Fee");
        if (OTA_Fee != null && OTA_Fee != "" && OTA_Fee != "0") {
            textOTA_fee.setVisibility(View.INVISIBLE);
            textOTA_fee.setText("*Rs. " + OTA_Fee
                    + " One Time Activation Fee");
            //OTA_FeeText = "+ Rs. " + OTA_Fee;
        }
        editTopUP.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
                try {
                    EditTextValidator.hasText(PGActivity.this, editTopUP,
                            Constants.ERROR_AMOUNT_BLANK_FIELD);

                } catch (Exception exception) {
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        btnTopUp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                timestamp = System.currentTimeMillis();
                mobileNumber = Utility.getUserMobileNumber(PGActivity.this);// editMobileNumber.getText().toString();
                amount = editTopUP.getText().toString();

                if (!checkValidation(PGActivity.this)) {

                } else {
                    String msg = "";
                    if (serviceCharge != 0) {
                        msg = "Amount to be debited from your account is Rs. "
                                + (Integer.parseInt(amount) + serviceCharge)
                                + "\nRs. "
                                + Integer.parseInt(amount) + " + "
                                + serviceCharge
                                + " service charge inclusive of all taxes"
                                + OTA_FeeText;
                    } else {
                        msg = "Amount to be debited from your account is Rs. "
                                + (Integer.parseInt(amount))
                                + OTA_FeeText
                                + "\n\nPress Ok to continue.";

                    }
                    new AlertDialog.Builder(PGActivity.this)
                            .setTitle("Online Balance")
                            .setMessage(msg)
                            .setPositiveButton("Yes",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog,
                                                int which) {
                                            dialog.dismiss();
                                            new PaymentGatewayTask().execute();

                                        }
                                    })
                            .setNegativeButton("No",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog,
                                                int which) {
                                            dialog.dismiss();
                                        }
                                    }).show();

                }
            }
        });
    }

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        //tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
    }

    private void findViewById() {
        // TODO Auto-generated method stub
        btnTopUp = (Button) findViewById(R.id.mBtnTopUp);
        editTopUP = (EditText) findViewById(R.id.editTopUp);
    }

    private boolean checkValidation(Context context) {
        boolean ret = true;
        if (!EditTextValidator.hasText(context, editTopUP,
                Constants.ERROR_AMOUNT_BLANK_FIELD)) {
            ret = false;
            editTopUP.requestFocus();
            return ret;
        } else
            return ret;
    }

    public class PaymentGatewayTask extends AsyncTask<String, String, String> {

        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method", "pg"));

            int totalAmount = serviceCharge
                    + Integer.parseInt(editTopUP.getText().toString().trim());
            listValuePair.add(new BasicNameValuePair("amount", String
                    .valueOf(totalAmount)));
            listValuePair.add(new BasicNameValuePair("deviceType", "android"));

            String response = RequestClass.getInstance().readPay1Request(
                    PGActivity.this, Constants.API_URL, listValuePair);
            return response;
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

            try {
                if (!result.startsWith("Error")) {

                    String replaced = result.substring(1, result.length() - 1);
                    replaced = replaced.substring(1, replaced.length() - 1);
                    replaced.substring(0, replaced.length() - 1);
                    JSONObject jsonObject = new JSONObject(replaced);

                    String status = jsonObject.getString("status");

                    if (status.equalsIgnoreCase("success")) {
                        String description = jsonObject.getString("description");
                        Intent intent = new Intent(PGActivity.this,
                                PaymentActivity.class);
                        intent.putExtra(Constants.PG_DATA, description);
                        startActivityForResult(intent, REQUEST_CODE_PAYMENT);

                    } else {
                        String replacedFail = result.replace("(", "").replace(")", "")
                                .replace(";", "");
                        Constants.showOneButtonFailureDialog(PGActivity.this,
                                Constants.checkCode(replacedFail), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(PGActivity.this,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(PGActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(PGActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(PGActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            PaymentGatewayTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();
        }

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Constants.showNavigationBar(PGActivity.this);
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == RESULT_OK) {
                Constants
                        .showOneButtonFailureDialog(
                                PGActivity.this,
                                "Transaction failed. Please try again.",
                                "Failure", Constants.PG_SETTINGS);
            }
        }
    }

}
