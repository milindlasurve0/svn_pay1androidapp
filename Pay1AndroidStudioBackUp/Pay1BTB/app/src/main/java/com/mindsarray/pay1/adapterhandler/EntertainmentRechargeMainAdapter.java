package com.mindsarray.pay1.adapterhandler;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindsarray.pay1.R;
import com.mindsarray.pay1.constant.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class EntertainmentRechargeMainAdapter extends ArrayAdapter<String> {
    Context appContext;
    ArrayList<String> productList;
    LayoutInflater inflater;
    JSONObject json_product;

    @SuppressLint("NewApi")
    public EntertainmentRechargeMainAdapter(Context context,
                                            int textViewResourceId, ArrayList<String> objects) {
        super(context, textViewResourceId, objects);
        this.appContext = context;
        this.productList = objects;
        inflater = (LayoutInflater) appContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // StrictMode.ThreadPolicy policy = new
        // StrictMode.ThreadPolicy.Builder()
        // .permitAll().build();
        //
        // StrictMode.setThreadPolicy(policy);
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        View holder = convertView;
        if (holder == null) {
            holder = inflater.inflate(
                    R.layout.entertainmentrechargemain_adapter, null);
        }
        if (position % 2 == 1) {
            holder.setBackgroundColor(Color.WHITE);
        } else {
            // v.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.border_blue));/*Color(Color.WHITE);*/
            holder.setBackgroundColor(appContext.getResources().getColor(
                    R.color.app_faint_theme_secondry));

        }
        holder.setBackgroundColor(Color.WHITE);
        JSONObject json = null;

        try {
            json = new JSONObject(productList.get(position));
            json_product = json.getJSONObject("prods");

        } catch (JSONException e1) {
            // e1.printStackTrace();
        }
        if (json != null) {
            TextView textProductName = (TextView) holder
                    .findViewById(R.id.textProductName);

            try {
                String name = json_product.getString("name");
                textProductName.setText(name);
            } catch (Exception e) {
                // e.printStackTrace();
            }
        }

        Button button = (Button) holder.findViewById(R.id.btnProduct);

        button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    final Dialog dialog = new Dialog(appContext);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                    dialog.setContentView(R.layout.entertainment_dialog_layout);

                    JSONObject jsonObject = new JSONObject(productList
                            .get(position));

                    TextView textDesc = (TextView) dialog
                            .findViewById(R.id.textDesc);
                    // set the custom dialog components - text, image and button
                    TextView text = (TextView) dialog.findViewById(R.id.text);
                    text.setText(jsonObject.getJSONObject("prods").getString(
                            "shortDesc"));
                    ImageView image = (ImageView) dialog
                            .findViewById(R.id.image);
                    int id = Integer.parseInt(jsonObject.getJSONObject("prods")
                            .getString("product_id").toString().trim());
                    image.setImageResource(Constants
                            .getBitmapById(id, appContext));
                    textDesc.setText(jsonObject.getJSONObject("prods")
                            .getString("longDesc"));
                    textDesc.setMovementMethod(new ScrollingMovementMethod());
                    ImageView dialogButton = (ImageView) dialog
                            .findViewById(R.id.imageView_Close);

                    dialogButton.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    // String imageUrl = jsonObject.getJSONObject("prods")
                    // .getString("image");
                    //
                    // if (imageUrl.length() <= 4) {
                    //
                    // } else {
                    // Drawable drawable = Utility.getImageFromURL(imageUrl);
                    // image.setImageDrawable(drawable);
                    //
                    // }

					/*
                     * final Dialog dialog = new Dialog(appContext);
					 * dialog.setContentView
					 * (R.layout.entertainment_dialog_layout); TextView
					 * textEntTitle = (TextView)
					 * dialog.findViewById(R.id.textEntTitle);
					 * textEntTitle.setText("Android custom dialog example!");
					 * ImageView imageEnt = (ImageView)
					 * dialog.findViewById(R.id.imageEnt);
					 * imageEnt.setImageResource(R.drawable.ic_launcher);
					 * TextView
					 * textEntDesc=(TextView)dialog.findViewById(R.id.textEntDesc
					 * ); textEntDesc.setText("sfgfdg");
					 * 
					 * /*try{ AlertDialog.Builder builder = new
					 * AlertDialog.Builder( appContext); JSONObject
					 * jsonObject=new JSONObject(productList.get(position));
					 * builder.setTitle(jsonObject.getJSONObject("prods")
					 * .getString("shortDesc"));
					 * builder.setMessage(jsonObject.getJSONObject("prods")
					 * .getString("longDesc"));
					 * 
					 * 
					 * 
					 * String imageUrl = jsonObject.getJSONObject("prods")
					 * .getString("image");
					 * 
					 * if (imageUrl.length() <= 4) {
					 * 
					 * } else { Drawable drawable =
					 * Utility.getImageFromURL(imageUrl);
					 * builder.setIcon(drawable);
					 * 
					 * }
					 * 
					 * builder.setPositiveButton("OK", new
					 * DialogInterface.OnClickListener() { public void
					 * onClick(DialogInterface dialog, int id) { // actions }
					 * }); AlertDialog dialog = builder.create(); dialog.show();
					 * } catch (Exception exception) { //
					 * exception.printStackTrace();
					 * 
					 * }
					 */
                    dialog.show();
                } catch (Exception e) {
                    // Log.w("FFFF", "" + e.getMessage());
                }
            }
        });

        return holder;

    }
}
