package com.mindsarray.pay1.adapterhandler;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.mindsarray.pay1.TransactionCashPGFragment;
import com.mindsarray.pay1.TransactionDthFragment;
import com.mindsarray.pay1.TransactionEntertainmentFragment;
import com.mindsarray.pay1.TransactionMobileBillFragment;
import com.mindsarray.pay1.TransactionMobileFragment;
import com.mindsarray.pay1.TransactionUtilityBillFragment;
import com.mindsarray.pay1.TransactionWalletFragment;

public class TransactionTabFragmentAdapter extends FragmentStatePagerAdapter {
    protected static final String[] CONTENT = new String[]{"Mobile", "Dth",
            "Mobile Bill", "Entertainment", "Wallet Topup", "Utility Bill", "Online Payment"};

    private int mCount = CONTENT.length;

    public TransactionTabFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        // return TestFragment.newInstance(CONTENT[position % CONTENT.length]);
        switch (position) {

            case 0:
                return TransactionMobileFragment
                        .newInstance("FirstFragment, Instance 1");
            case 1:
                return TransactionDthFragment
                        .newInstance("SecondFragment, Instance 2");
            case 2:
                return TransactionMobileBillFragment
                        .newInstance("ThirdFragment, Instance 4");
            case 3:
                return TransactionEntertainmentFragment
                        .newInstance("FourthFragment, Instance 3");
            case 4:
                return TransactionWalletFragment
                        .newInstance("FourthFragment, Instance 3");
            case 5:
                return TransactionUtilityBillFragment
                        .newInstance("FourthFragment, Instance 3");
            case 6:
                return TransactionCashPGFragment
                        .newInstance("FourthFragment, Instance 4");
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TransactionTabFragmentAdapter.CONTENT[position % CONTENT.length];
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }
}