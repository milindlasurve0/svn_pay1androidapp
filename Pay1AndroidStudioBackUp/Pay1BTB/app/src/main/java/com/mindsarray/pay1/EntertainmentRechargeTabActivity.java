package com.mindsarray.pay1;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;

import com.mindsarray.pay1.constant.Constants;

public class EntertainmentRechargeTabActivity extends TabActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        // WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.recharge_activity);
        Resources res = getResources(); // Resource object to get Drawables
        TabHost tabHost = getTabHost(); // The activity TabHost
        TabHost.TabSpec spec; // Resusable TabSpec for each tab
        Intent intent; // Reusable Intent for each tab
        View tabView = createTabView(this, "Entertainment");
        intent = new Intent().setClass(this,
                EntertainmentRechargeMainActivty.class);
        spec = tabHost.newTabSpec("First Tab").setIndicator(tabView)
                .setContent(intent);
        tabHost.addTab(spec);
        /*
		 * if (LoginActivity.mDefaultHttpClient != null) { intent = new
		 * Intent().setClass(this, StatusActvity.class);
		 * intent.putExtra(Constants.REQUEST_FOR, Constants.ENTERTAINMENT_TAB);
		 * } else {
		 * 
		 * intent = new Intent(EntertainmentTabActivity.this,
		 * LoginActivity.class); intent.putExtra(Constants.IS_STATUS_TAB,
		 * Constants.STATUS_TAB); intent.putExtra(Constants.REQUEST_FOR,
		 * Constants.ENTERTAINMENT_TAB);
		 * 
		 * }
		 */

        intent = new Intent().setClass(this, StatusActvity.class);
        intent.putExtra(Constants.REQUEST_FOR, Constants.ENTERTAINMENT_TAB);
        tabView = createTabView(this, "Status");
        spec = tabHost.newTabSpec("Second Tab").setIndicator(tabView)
                .setContent(intent);
        tabHost.addTab(spec);
		/*
		 * if (LoginActivity.mDefaultHttpClient != null) { intent = new
		 * Intent().setClass(this, RequestActivity.class);
		 * intent.putExtra(Constants.REQUEST_FOR, Constants.ENTERTAINMENT_TAB);
		 * } else { intent = new Intent(EntertainmentTabActivity.this,
		 * LoginActivity.class); intent.putExtra(Constants.IS_REQUEST_TAB,
		 * Constants.REQUEST_TAB); intent.putExtra(Constants.REQUEST_FOR,
		 * Constants.ENTERTAINMENT_TAB); }
		 */

        intent = new Intent().setClass(this, RequestActivity.class);
        intent.putExtra(Constants.REQUEST_FOR, Constants.ENTERTAINMENT_TAB);
        tabView = createTabView(this, "Request");
        spec = tabHost.newTabSpec("Third Tab").setIndicator(tabView)
                .setContent(intent);
        tabHost.addTab(spec);

        if (getIntent().getExtras() == null) {
            tabHost.setCurrentTab(0);
        } else {
            tabHost.setCurrentTab(getIntent().getExtras().getInt(
                    Constants.REQUEST_FOR_TAB));
        }

    }

    @Override
    protected void onResume() {
        Constants.showNavigationBar(EntertainmentRechargeTabActivity.this);
        super.onResume();
    }

    private static View createTabView(Context context, String tabText) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_tabs,
                null, false);
        TextView tv = (TextView) view.findViewById(R.id.tabTitleText);
        tv.setText(tabText);
        return view;
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (Constants.isExpanded) {
            Constants.collapsView();
        } else {
            super.onBackPressed();
        }
    }
}