package com.mindsarray.pay1.adapterhandler;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mindsarray.pay1.R;

import org.json.JSONObject;

import java.util.List;

public class DiscountAdapter extends ArrayAdapter<JSONObject> {
    Context mContext;
    List<JSONObject> jsonObjects;
    LayoutInflater inflater;
    TextView textProduct, textPercent;

    public DiscountAdapter(Context context, int textViewResourceId,
                           List<JSONObject> objects) {
        super(context, textViewResourceId, objects);
        // TODO Auto-generated constructor stub
        this.mContext = context;
        this.jsonObjects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View v = convertView;

        if (convertView == null) {
            inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.discountstructure_adapter, null);
        }

        if (position % 2 == 1) {
            //v.setBackgroundColor(Color.WHITE);
            v.setBackgroundDrawable(mContext.getResources().getDrawable(R.color.app_faint_theme_secondry));
        } else {
            // v.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.border_blue));/*Color(Color.WHITE);*/
        /*	v.setBackgroundColor(mContext.getResources().getColor(
					R.color.app_faint_theme_secondry));*/
            v.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.border));
        }
        v.setBackgroundColor(Color.WHITE);
        TextView textProduct = (TextView) v.findViewById(R.id.textProduct);
        TextView textPercent = (TextView) v.findViewById(R.id.textPercent);

        try {
            String prodPercent = jsonObjects.get(position).getString(
                    "prodPercent");
            String prodName = jsonObjects.get(position).getString("prodName");
            textProduct.setText(prodName);
            textPercent.setText(prodPercent + "%");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            // e.printStackTrace();
        }
        return v;
    }

}
