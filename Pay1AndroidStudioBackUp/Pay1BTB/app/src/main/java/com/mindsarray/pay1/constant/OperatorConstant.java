package com.mindsarray.pay1.constant;

import android.content.Context;

import com.mindsarray.pay1.R;
import com.mindsarray.pay1.utilities.GridMenuItem;

public class OperatorConstant {

    public static GridMenuItem getGridItem(Context mContext, int op_code) {

        switch (op_code) {

            case Constants.OPERATOR_AIRCEL:
                int mAircel = R.drawable.m_aircel;
                return new GridMenuItem(mAircel, "Aircel",
                        Constants.OPERATOR_AIRCEL, Constants.RECHARGE_MOBILE);

            case Constants.OPERATOR_AIRTEL:
                int mAirtel = R.drawable.m_airtel;
                return new GridMenuItem(mAirtel, "Airtel",
                        Constants.OPERATOR_AIRTEL, Constants.RECHARGE_MOBILE);

            case Constants.OPERATOR_BSNL:
                int mBsnl = R.drawable.m_bsnl;
                return new GridMenuItem(mBsnl, "BSNL", Constants.OPERATOR_BSNL,
                        Constants.RECHARGE_MOBILE);

            case Constants.OPERATOR_IDEA:
                int mIdea = R.drawable.m_idea;
                return new GridMenuItem(mIdea, "Idea", Constants.OPERATOR_IDEA,
                        Constants.RECHARGE_MOBILE);

            case Constants.OPERATOR_LOOP:
                int mLoop = R.drawable.m_loop;
                return new GridMenuItem(mLoop, "Loop", Constants.OPERATOR_LOOP,
                        Constants.RECHARGE_MOBILE);

            case Constants.OPERATOR_MTS:
                int mMTS = R.drawable.m_mts;
                return new GridMenuItem(mMTS, "MTS", Constants.OPERATOR_MTS,
                        Constants.RECHARGE_MOBILE);

            case Constants.OPERATOR_RELIANCE_CDMA:
                int mRelainceCdma = R.drawable.m_reliance_cdma;
                return new GridMenuItem(mRelainceCdma, "Reliance CDMA",
                        Constants.OPERATOR_RELIANCE_CDMA, Constants.RECHARGE_MOBILE);

            case Constants.OPERATOR_RELIANCE_GSM:
                int mRelainceGsm = R.drawable.m_reliance;
                return new GridMenuItem(mRelainceGsm, "Reliance GSM",
                        Constants.OPERATOR_RELIANCE_GSM, Constants.RECHARGE_MOBILE);

            case Constants.OPERATOR_DOCOMO:
                int mTataDocomo = R.drawable.m_docomo;
                return new GridMenuItem(mTataDocomo, "Tata Docomo",
                        Constants.OPERATOR_DOCOMO, Constants.RECHARGE_MOBILE);

            case Constants.OPERATOR_TATA_INDICOM:
                int mTataIndicom = R.drawable.m_indicom;
                return new GridMenuItem(mTataIndicom, "Tata Indicom",
                        Constants.OPERATOR_TATA_INDICOM, Constants.RECHARGE_MOBILE);

            case Constants.OPERATOR_UNINOR:
                int mUninor = R.drawable.m_uninor;
                return new GridMenuItem(mUninor, "Uninor",
                        Constants.OPERATOR_UNINOR, Constants.RECHARGE_MOBILE);

            case Constants.OPERATOR_VIDEOCON:
                int mVideocon = R.drawable.m_videocon;
                return new GridMenuItem(mVideocon, "Videocon",
                        Constants.OPERATOR_VIDEOCON, Constants.RECHARGE_MOBILE);

            case Constants.OPERATOR_VODAFONE:
                int mVodafone = R.drawable.m_vodafone;
                return new GridMenuItem(mVodafone, "Vodafone",
                        Constants.OPERATOR_VODAFONE, Constants.RECHARGE_MOBILE);

            case Constants.OPERATOR_MTNL:
                int mMTNL = R.drawable.m_mtnl;
                return new GridMenuItem(mMTNL, "MTNL", Constants.OPERATOR_MTNL,
                        Constants.RECHARGE_MOBILE);

            case Constants.OPERATOR_DTH_AIRTEL:
                int m_airtel = R.drawable.m_airtel;
                return new GridMenuItem(m_airtel, "AirTel",
                        Constants.OPERATOR_DTH_AIRTEL, Constants.RECHARGE_DTH);

            case Constants.OPERATOR_DTH_BIG:
                int d_bigtv = R.drawable.d_bigtv;
                return new GridMenuItem(d_bigtv, "Big TV",
                        Constants.OPERATOR_DTH_BIG, Constants.RECHARGE_DTH);

            case Constants.OPERATOR_DTH_DISHTV:
                int d_dishtv = R.drawable.d_dishtv;
                return new GridMenuItem(d_dishtv, "Dish TV",
                        Constants.OPERATOR_DTH_DISHTV, Constants.RECHARGE_DTH);

            case Constants.OPERATOR_DTH_SUN:

                int d_sundirect = R.drawable.d_sundirect;
                return new GridMenuItem(d_sundirect, "Sun Direct",
                        Constants.OPERATOR_DTH_SUN, Constants.RECHARGE_DTH);
            case Constants.OPERATOR_DTH_TATA_SKY:
                int d_tatasky = R.drawable.d_tatasky;
                return new GridMenuItem(d_tatasky, "Tata Sky",
                        Constants.OPERATOR_DTH_TATA_SKY, Constants.RECHARGE_DTH);

            case Constants.OPERATOR_DTH_VIDEOCON:
                int d_videocon = R.drawable.d_videocon;

                return new GridMenuItem(d_videocon, "Videocon",
                        Constants.OPERATOR_DTH_VIDEOCON, Constants.RECHARGE_DTH);

            case Constants.OPERATOR_BILL_DOCOMO:
                mTataDocomo = R.drawable.m_docomo;
                return new GridMenuItem(mTataDocomo, "Tata Docomo",
                        Constants.OPERATOR_BILL_DOCOMO, Constants.BILL_PAYMENT);
            case Constants.OPERATOR_BILL_LOOP:
                mLoop = R.drawable.m_loop;
                return new GridMenuItem(mLoop, "Loop",
                        Constants.OPERATOR_BILL_LOOP, Constants.BILL_PAYMENT);
            case Constants.OPERATOR_BILL_CELLONE:
                mBsnl = R.drawable.m_bsnl;
                return new GridMenuItem(mBsnl, "BSNL",
                        Constants.OPERATOR_BILL_CELLONE, Constants.BILL_PAYMENT);
            case Constants.OPERATOR_BILL_IDEA:
                mIdea = R.drawable.m_idea;
                return new GridMenuItem(mIdea, "Idea",
                        Constants.OPERATOR_BILL_IDEA, Constants.BILL_PAYMENT);
            case Constants.OPERATOR_BILL_TATATELESERVICE:
                mTataIndicom = R.drawable.m_indicom;
                return new GridMenuItem(mTataIndicom, "Tata Teleservices",
                        Constants.OPERATOR_BILL_TATATELESERVICE,
                        Constants.BILL_PAYMENT);
            case Constants.OPERATOR_BILL_VODAFONE:
                mVodafone = R.drawable.m_vodafone;
                return new GridMenuItem(mVodafone, "Vodafone",
                        Constants.OPERATOR_BILL_VODAFONE, Constants.BILL_PAYMENT);
            case Constants.OPERATOR_BILL_AIRTEL:
                mAirtel = R.drawable.m_airtel;
                return new GridMenuItem(mAirtel, "Airtel",
                        Constants.OPERATOR_BILL_AIRTEL, Constants.BILL_PAYMENT);
            case Constants.OPERATOR_BILL_RELIANCE:
                mRelainceGsm = R.drawable.m_reliance;
                return new GridMenuItem(mRelainceGsm, "Reliance ",
                        Constants.OPERATOR_BILL_RELIANCE, Constants.BILL_PAYMENT);
            default:

        }
        return null;

    }

}
