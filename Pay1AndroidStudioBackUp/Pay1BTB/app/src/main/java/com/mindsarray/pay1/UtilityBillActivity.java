package com.mindsarray.pay1;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.EditTextValidator;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class UtilityBillActivity extends Activity {
    ImageView mImageOprLogo;
    TextView mTextOperatorName, mLastRecharge;
    Button mBtnRchNow;

    EditText editMobileNumber, editAccountNumber, editParam,
            editRechargeAmount;
    Bundle bundle;
    int mOperatorId;
    String mobileNumber, account_no, parameters, amount, planJson;
    String amountExtras, amountInWords;
    Context mContext;
    String uuid;
    boolean bNetOrSms;
    private SharedPreferences mSettingsData;// Shared preferences for setting
    TextView cycle_hint, account_hint; // data
    long timestamp;
    private static final String TAG = "Utility bill";
    int subIdLen;
    String prefix;

    // NotificationDataSource notificationDataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        // WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.utilitybill_activity);
        findViewById();
        tracker = Constants.setAnalyticsTracker(this, tracker);
        // easyTracker.set(Fields.SCREEN_NAME, TAG);
        try {
            TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            uuid = tManager.getDeviceId();
            if (uuid == null) {
                uuid = Secure.getString(
                        UtilityBillActivity.this.getContentResolver(),
                        Secure.ANDROID_ID);
            }
            if (Utility.getUUID(UtilityBillActivity.this,
                    Constants.SHAREDPREFERENCE_UUID) == null)
                Utility.setUUID(UtilityBillActivity.this,
                        Constants.SHAREDPREFERENCE_UUID, uuid);
        } catch (Exception exception) {
        }
        /*
		 * notificationDataSource = new NotificationDataSource(
		 * UtilityBillAcitivity.this);
		 */

        LinearLayout adLayout = (LinearLayout) findViewById(R.id.adLayout);
        String notificationText = Utility.getSharedPreferences(
                UtilityBillActivity.this, "notification");
        // String notificationText="";
        if (notificationText.equals("") || notificationText.length() == 0) {
            adLayout.setVisibility(View.GONE);
        } else {
            adLayout.setVisibility(View.VISIBLE);
            String html = "<html><body height:50px  bgcolor='#26a9e0'><marquee><div style='color:#1b1b1b; padding: 10px'>"
                    + notificationText + "</div></marquee></body></html>";

            WebView myWebView = (WebView) this.findViewById(R.id.myWebView);
            myWebView.getSettings().setJavaScriptEnabled(true);

            myWebView.loadData(html, "text/html", null);
        }

        mLastRecharge = (TextView) findViewById(R.id.lastRecharge);

        mContext = this;
        mSettingsData = getSharedPreferences(SettingsSmsActivity.SETTINGS_NAME,
                0);
        bNetOrSms = mSettingsData.getBoolean(SettingsSmsActivity.KEY_NETWORK,
                true);
        bundle = getIntent().getExtras();
        mOperatorId = bundle.getInt(Constants.OPERATOR_ID);
        planJson = bundle.getString(Constants.PLAN_JSON);
        cycle_hint = (TextView) findViewById(R.id.cycle_hint);
        account_hint = (TextView) findViewById(R.id.account_hint);
        switch (mOperatorId) {
            case 45:
                editAccountNumber.setVisibility(View.VISIBLE);
                account_hint.setText("e.g. 102252117");
                editParam.setVisibility(View.VISIBLE);
                editParam.setHint(getResources().getString(R.string.cycle_number));
                // cycle_hint.setText(getResources().getString(R.string.cycle_number));
                cycle_hint
                        .setText("e.g. 05");
                cycle_hint.setVisibility(View.VISIBLE);
                break;
            case 46:
                editAccountNumber.setVisibility(View.VISIBLE);
                account_hint.setText("e.g. 102252117");
                editParam.setVisibility(View.GONE);
                cycle_hint.setVisibility(View.GONE);
                break;
            case 47:
                editAccountNumber.setVisibility(View.VISIBLE);
                account_hint.setText("e.g. 102252117");
                editParam.setVisibility(View.GONE);
                cycle_hint.setVisibility(View.GONE);
                break;
            case 48:
                editAccountNumber.setVisibility(View.VISIBLE);
                account_hint.setText("e.g. 61100131066");
                editParam.setVisibility(View.GONE);
                cycle_hint.setVisibility(View.GONE);
                break;
            case 49:
                editAccountNumber.setVisibility(View.VISIBLE);
                editAccountNumber.setHint(getResources().getString(
                        R.string.phone_label));
                account_hint.setText("e.g. 02242932233");
                editParam.setVisibility(View.GONE);
                cycle_hint.setVisibility(View.GONE);
                break;
            case 50:
                editAccountNumber.setVisibility(View.VISIBLE);
                account_hint.setText("e.g. 21458796");
                editAccountNumber.setHint(getResources().getString(
                        R.string.phone_label));
			/*account_hint
					.setText(getResources().getString(R.string.phone_label));*/
                editParam.setVisibility(View.VISIBLE);
                cycle_hint.setVisibility(View.GONE);
                editParam
                        .setHint(getResources().getString(R.string.account_number));
                cycle_hint.setText(getResources()
                        .getString(R.string.account_number));

                break;
            case 51:
                editAccountNumber.setVisibility(View.VISIBLE);
                account_hint.setText("e.g. 210000253340");
                editParam.setVisibility(View.GONE);
                cycle_hint.setVisibility(View.GONE);
                break;
        }
        editMobileNumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
                EditTextValidator.hasText(UtilityBillActivity.this,
                        editMobileNumber, Constants.ERROR_MOBILE_BLANK_FIELD);
                if (editMobileNumber.length() == 10) {
                    editAccountNumber.requestFocus();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        editAccountNumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
                EditTextValidator.hasText(UtilityBillActivity.this,
                        editAccountNumber, Constants.ERROR_ACCOUNT_BLANK_FIELD);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        editParam.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
                EditTextValidator.hasText(UtilityBillActivity.this, editParam,
                        Constants.ERROR_CYCLE_BLANK_FIELD);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        editRechargeAmount.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                EditTextValidator.hasText(UtilityBillActivity.this,
                        editRechargeAmount, Constants.ERROR_AMOUNT_BLANK_FIELD);
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });

        mImageOprLogo.setImageResource(getIntent().getIntExtra(
                Constants.OPERATOR_LOGO, 0));
        mTextOperatorName.setText(bundle.getString(Constants.OPERATOR_NAME));

        mBtnRchNow.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                bNetOrSms = mSettingsData.getBoolean(
                        SettingsSmsActivity.KEY_NETWORK, true);
//				checkSubIdValidation(mOperatorId);
                timestamp = System.currentTimeMillis();


                if (!checkValidation(UtilityBillActivity.this)) {

                } else {

                    mobileNumber = editMobileNumber.getText().toString();
                    account_no = editAccountNumber.getText().toString().trim();
                    parameters = editParam.getText().toString().trim();
                    amount = editRechargeAmount.getText().toString();

                    DecimalFormat f = new DecimalFormat("##.00");
                    double serviceCharge = 0.005 * Integer.parseInt(amount);

                    if (serviceCharge < 5) {
                        serviceCharge = 5;
                    }
                    double serviceTaxPer = serviceCharge * (14.5 / 100);
                    amountExtras = " + " + f.format(serviceCharge + serviceTaxPer);
                    if (amount.equals("")) {
                        amountInWords = "";
                    } else
                        amountInWords = "0.5 %  ";


                    if (mOperatorId == 45
                            && !EditTextValidator.hasText(
                            UtilityBillActivity.this, editParam,
                            Constants.ERROR_CYCLE_BLANK_FIELD)) {

                    } else if (mOperatorId == 50
                            && !EditTextValidator.hasText(
                            UtilityBillActivity.this, editParam,
                            Constants.ERROR_ACCOUNT_BLANK_FIELD)) {

                    } else if (bNetOrSms) {

                        if (Utility.getLoginFlag(UtilityBillActivity.this,
                                Constants.SHAREDPREFERENCE_IS_LOGIN)) {

                            String msg = "";
                            if (mOperatorId == 45) {
                                msg = "Operator: "
                                        + mTextOperatorName.getText()
                                        + "\nMobile Number: "
                                        + mobileNumber
                                        + "\nAccount Number: "
                                        + account_no
                                        + "\nCycle Number: "
                                        + parameters
                                        + "\nAmount: "
                                        + amount
                                        + amountExtras
                                        + "("
                                        + amountInWords
                                        + " + service charge )"
                                        + "\nAre you sure you want to Pay Bill?";
                            } else if (mOperatorId == 50) {
                                msg = "Operator: "
                                        + mTextOperatorName.getText()
                                        + "\nMobile Number: "
                                        + mobileNumber
                                        + "\nPhone Number: "
                                        + account_no
                                        + "\nAccount Number: "
                                        + parameters
                                        + "\nAmount: "
                                        + amount
                                        + amountExtras
                                        + "("
                                        + amountInWords
                                        + " service charge )"
                                        + "\nAre you sure you want to Pay Bill?";
                            } else {

                                msg = "Operator: "
                                        + mTextOperatorName.getText()
                                        + "\nMobile Number: "
                                        + mobileNumber
                                        + "\nAccount Number: "
                                        + account_no
                                        + "\nAmount: "
                                        + amount
                                        + amountExtras
                                        + "("
                                        + amountInWords
                                        + " service charge )"
                                        + "\nAre you sure you want to Pay Bill?";
                            }
                            new AlertDialog.Builder(UtilityBillActivity.this)
                                    .setTitle("Utility Bill")
                                    .setMessage(msg)
                                    .setPositiveButton(
                                            "Yes",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(
                                                        DialogInterface dialog,
                                                        int which) {
                                                    dialog.dismiss();
                                                    new BillPaymentTask()
                                                            .execute();
                                                }
                                            })
                                    .setNegativeButton(
                                            "No",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(
                                                        DialogInterface dialog,
                                                        int which) {
                                                    dialog.dismiss();
                                                }
                                            }).show();

                        } else {
                            Intent intent = new Intent(
                                    UtilityBillActivity.this,
                                    LoginActivity.class);
                            startActivity(intent);
                        }
                    } else {
                        new AlertDialog.Builder(UtilityBillActivity.this)
                                .setTitle("Utility Bill (SMS)")
                                .setMessage(
                                        "This service is not active on SMS.")
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int which) {
                                                dialog.dismiss();
                                            }
                                        }).show();

                    }

                }

            }
        });

    }

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        // tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
    }

    private boolean checkValidation(Context context) {
        boolean ret = true;

        if (!EditTextValidator.hasText(context, editMobileNumber,
                Constants.ERROR_MOBILE_BLANK_FIELD)) {
            ret = false;
            editMobileNumber.requestFocus();
            return ret;
//		} else if (!EditTextValidator.isUtilityEntryValid(
//				getApplicationContext(), editAccountNumber, prefix, subIdLen,
//				mOperatorId)) {
//			ret = false;
//			editAccountNumber.requestFocus();
//			return ret;
        } else if (!EditTextValidator.isValidMobileNumber(context,
                editMobileNumber, Constants.ERROR_MOBILE_LENGTH_FIELD)) {
            ret = false;
            editMobileNumber.requestFocus();
            return ret;
        } else if (!EditTextValidator.hasText(context, editAccountNumber,
                Constants.ERROR_ACCOUNT_BLANK_FIELD)) {
            ret = false;
            editAccountNumber.requestFocus();
            return ret;
        } else if (!EditTextValidator.hasText(context, editRechargeAmount,
                Constants.ERROR_AMOUNT_BLANK_FIELD)) {
            ret = false;
            editRechargeAmount.requestFocus();
            return ret;
        } else
            return ret;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.PLAN_REQUEST) {
                editRechargeAmount
                        .setText(data.getExtras().getString("result"));
            }
        }
    }

    private void findViewById() {
        mImageOprLogo = (ImageView) findViewById(R.id.operator_logo);
        mTextOperatorName = (TextView) findViewById(R.id.operator_name);
        mBtnRchNow = (Button) findViewById(R.id.mBtnRchNow);
        editMobileNumber = (EditText) findViewById(R.id.editMobileNumber);
        editAccountNumber = (EditText) findViewById(R.id.editAccountNumber);
        editParam = (EditText) findViewById(R.id.editParam);
        editRechargeAmount = (EditText) findViewById(R.id.editRechargeAmount);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Constants.showNavigationBar(UtilityBillActivity.this);
        super.onResume();
    }

    public class BillPaymentTask extends AsyncTask<String, String, String>
            implements OnDismissListener {
        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method",
                    "utilityBillPayment"));
            listValuePair.add(new BasicNameValuePair("mobileNumber",
                    mobileNumber));
            listValuePair.add(new BasicNameValuePair("accountNumber",
                    account_no));
            listValuePair.add(new BasicNameValuePair("param", parameters));
            listValuePair.add(new BasicNameValuePair("operator",
                    (mOperatorId - 44) + ""));
            listValuePair.add(new BasicNameValuePair("subId", mobileNumber));
            listValuePair.add(new BasicNameValuePair("amount", amount));
            listValuePair.add(new BasicNameValuePair("type", "flexi"));
            listValuePair.add(new BasicNameValuePair("circle", ""));
            // listValuePair.add(new BasicNameValuePair("special", radioSTV
            // .getTag().toString()));
            // listValuePair.add(new BasicNameValuePair("uuid", uuid));
            listValuePair.add(new BasicNameValuePair("timestamp", String
                    .valueOf(timestamp)));
            listValuePair.add(new BasicNameValuePair("profile_id", Utility
                    .getProfileID(UtilityBillActivity.this,
                            Constants.SHAREDPREFERENCE_PROFILE_ID)));
            listValuePair.add(new BasicNameValuePair("hash_code", Constants
                    .encryptPassword(Utility.getUUID(UtilityBillActivity.this,
                            Constants.SHAREDPREFERENCE_UUID)
                            + mobileNumber
                            + amount + timestamp)));
            listValuePair.add(new BasicNameValuePair("device_type", "android"));
            // String
            // url="http://panel.activestores.in/apis/receiveWeb/mindsarray/mindsarray/json?method=mobRecharge&Mobile=9819032643&operator=4&subId=9819032643&amount=10&type=flexi&circle=&special=0&timestamp=1389870880878&hash_code=92da34555032fda9ff31d7fbdfe2c6482bc094ca&device_type=android";

            String response = RequestClass.getInstance().readPay1Request(
                    UtilityBillActivity.this, Constants.API_URL, listValuePair);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    // String rechargeString = Constants.getRechargeString(
                    // mContext, Constants.RECHARGE_MOBILE, mOperatorId,
                    // mobileNumber, mobileNumber, amount, "0");
					/*
					 * try { notificationDataSource.open();
					 * notificationDataSource.createNotification(rechargeString,
					 * 0, "" + System.currentTimeMillis()); } catch
					 * (SQLException exception) { // TODO: handle exception //
					 * // Log.e("Er ", exception.getMessage()); } catch
					 * (Exception e) { // TODO: handle exception // //
					 * Log.e("Er ", e.getMessage()); } finally {
					 * notificationDataSource.close(); }
					 */

                    // ContentValues contentValues = new ContentValues();
                    // contentValues.put("notification_msg", rechargeString);
                    // contentValues.put("timestamp ", "12345678902");
                    // contentValues.put("netOrSms ", 0);
                    // controller.insertNotifications(contentValues);
                    if (status.equalsIgnoreCase("success")) {
                        // Constants.showCustomToast(UtilityBillAcitivity.this,
                        // "Bill payment request sent successfully.");
                        Constants.showOneButtonSuccessDialog(
                                UtilityBillActivity.this,
                                "Utility Bill payment request sent successfully.\nTransaction id : "
                                        + jsonObject.getString("description"),
                                Constants.RECHARGE_SETTINGS);
                        String balance = jsonObject.getString("balance");
                        Utility.setBalance(UtilityBillActivity.this,
                                Constants.SHAREDPREFERENCE_BALANCE, balance);

                        // finish();
                    } else {
                        Constants.showOneButtonFailureDialog(
                                UtilityBillActivity.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(
                            UtilityBillActivity.this, Constants.ERROR_INTERNET,
                            TAG, Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(UtilityBillActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(UtilityBillActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(UtilityBillActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            BillPaymentTask.this.cancel(true);
                            dialog.cancel();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            BillPaymentTask.this.cancel(true);
            dialog.cancel();
        }

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (Constants.isExpanded) {
            Constants.collapsView();
        } else {
            super.onBackPressed();
        }
    }

    public class GetLastRechargeTask extends AsyncTask<String, String, String>
            implements OnDismissListener {
        ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            String response = Constants.fetchLastRecharge(mContext, params[0],
                    (mOperatorId - 44) + "");
            Log.e("Last transaction:", "" + response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            super.onPostExecute(result);
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONObject jsonDescription = jsonObject
                                .getJSONObject("description");
                        String time_stamp = jsonDescription
                                .getString("time_stamp");
                        String amount = jsonDescription.getString("amount");

                        mLastRecharge.setText("Last "
                                + bundle.getString(Constants.OPERATOR_NAME)
                                + " recharge was done on " + time_stamp
                                + " Amount Rs. " + amount);
                        mLastRecharge.setVisibility(View.VISIBLE);
                    } else {
                        mLastRecharge.setVisibility(View.GONE);
                    }

                }
            } catch (JSONException e) {
                // e.printStackTrace();

            } catch (Exception e) {

            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(mContext);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            GetLastRechargeTask.this.cancel(true);
                            dialog.cancel();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            GetLastRechargeTask.this.cancel(true);
            dialog.cancel();
        }
    }

    public void checkSubIdValidation(int operatorId) {

        switch (operatorId) {
            case 45:

                prefix = "";
                subIdLen = 9;
                break;
            case 46:
                prefix = "";
                subIdLen = 9;
                break;
            case 47:

                prefix = "";
                subIdLen = 9;
                break;
            case 48:

                prefix = "6";
                subIdLen = 11;
                break;
            case 49:

                prefix = "0";
                subIdLen = 11;
                break;
            case 50:
                prefix = "2";
                subIdLen = 8;

                break;

            case 51:
                prefix = "21";
                subIdLen = 12;

                break;

            default:

                break;
        }

    }

}
