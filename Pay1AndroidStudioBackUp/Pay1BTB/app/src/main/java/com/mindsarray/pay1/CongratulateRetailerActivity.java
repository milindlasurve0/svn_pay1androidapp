package com.mindsarray.pay1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CongratulateRetailerActivity extends Activity {

    private String TAG = "Congratulate Retailer Activity";
    private SpinnerTextView spinnerTextView;
    private GetDistributorsByLocationTask getDistributorsByLocationTask;
    private Context mContext = CongratulateRetailerActivity.this;
    private String congratulateViewFlag = "1";
    private String retailer_pin;
    private String retailer_mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.congratulateretailer_activity);

        retailer_pin = Utility.getSharedPreferences(mContext, "Retailer_Pin");
        retailer_mobile = Utility.getSharedPreferences(mContext, "Retailer_Mobile");

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            congratulateViewFlag = extras.getString("congratulateView");
        }
        final String registrationPreference = Utility.getSharedPreferences(mContext, "Retailer_Registration_Preference");
        final String registrationInterest = Utility.getSharedPreferences(mContext, "Registration_interest");
        final String retailerUnderDistributor = Utility.getSharedPreferences(mContext, "Retailer_Under_Distributor");

        TextView text1 = (TextView) findViewById(R.id.text1);
        TextView text2 = (TextView) findViewById(R.id.text2);
        TextView text3 = (TextView) findViewById(R.id.text3);
        TextView text4 = (TextView) findViewById(R.id.text4);
        View view1 = findViewById(R.id.view1);
        View view2 = findViewById(R.id.view2);
        Button congratulateSubmit = (Button) findViewById(R.id.congratulate_submit);

        if (registrationInterest.equals("Distributor")) {
            if (registrationPreference.equals("Netbanking")) {
//				if(congratulateViewFlag.equals("2")){
//					text1.setText("Our team will contact you soon for your distributorship request.");
//					text2.setVisibility(View.GONE);
//					view2.setVisibility(View.GONE);
//					text3.setText("Your 7 days trial PAY1 Business Partner (Retailer) account is active. "
//							+ "Please email your KYC documents ( PAN/Aadhar Card copy, shop picture and address) "
//							+ "on info@pay1.in or call us on 022 12345678 for more information");					
//					congratulateSubmit.setText("START NOW");
//					text4.setText("We have sent your login details by sms & e-mail.");
//				}
//				else {
                text1.setText("Your request for distributorship has been forwarded to respective team and "
                        + "they will contact you soon.");
                text2.setText("We have sent your login details by sms.");
                text2.setVisibility(View.GONE);
                view2.setVisibility(View.GONE);
                text3.setText("Your 7 days trial PAY1 Channel Partner (Distributor) account is active. "
                        + "Please email your documents at info@pay1.in or call us on " + Constants.PAY1_CUSTOMER_CARE + " for more information");
                congratulateSubmit.setText("START RECHARGING");
                text4.setVisibility(View.GONE);
//				}	
            } else if (registrationPreference.equals("Deposit")) {
//				if(congratulateViewFlag.equals("2")){
//					text1.setText("Your request for distributoship has been forwarded to respective team and "
//							+ "they will contact you soon.");
//					text2.setText("Your 7 days trial PAY1 Channel Partner (Distributor) account is active. "
//							+ "Please email your documents at info@pay1.in or call us on 022 12345678 for more information");
//					text3.setText("The minimum deposit amount for limit balance is Rs. 5000/-");					
//					congratulateSubmit.setText("START RECHARGING");
//					text4.setVisibility(View.GONE);
//				}
//				else {
                text1.setText("Our team will contact you soon for your distributorship request.");
                text2.setVisibility(View.GONE);
                view1.setVisibility(View.GONE);
                text3.setText("Your 7 days trial PAY1 Business Partner (Retailer) account is active. "
                        + "Please email your KYC documents ( PAN/Aadhar Card copy, shop picture and address) "
                        + "on info@pay1.in or call us on " + Constants.PAY1_CUSTOMER_CARE + " for more information");
                congratulateSubmit.setText("START NOW");
                text4.setVisibility(View.GONE);
//				}	
            }
        } else if (registrationInterest.equals("Retailer")) {
            text1.setText("YOU HAVE BEEN REGISTERED AS BUSINESS PARTNER (RETAILER)!");
            congratulateSubmit.setText("START NOW");
            if (retailerUnderDistributor.equals("true")) {
                text2.setText("We have sent your login details by sms.");
                text3.setText("*Kindly note - The Channel Partner (Distributor) or company representatives "
                        + "will contact you soon or mail us on info@pay1.in");
                text4.setVisibility(View.GONE);
                text2.setVisibility(View.GONE);
                view2.setVisibility(View.GONE);
            } else if (registrationPreference.equals("Netbanking")) {
                text2.setText("We have sent your login details by sms.");
                text3.setVisibility(View.GONE);
                text4.setVisibility(View.GONE);
                view2.setVisibility(View.GONE);
                text2.setVisibility(View.GONE);
                view2.setVisibility(View.GONE);
            } else if (registrationPreference.equals("Deposit")) {
                text2.setVisibility(View.GONE);
                text3.setText("Your 7 days trial PAY1 Channel Partner (Retailer) account is active. "
                        + "Please email your documents at info@pay1.in or call us on " + Constants.PAY1_CUSTOMER_CARE + " for more information");
                text4.setText("We have sent your login details by sms & e-mail.");
                view1.setVisibility(View.GONE);
            }
        }

        congratulateSubmit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (registrationInterest.equals("Distributor")) {
                    if (registrationPreference.equals("Netbanking")) {
//						if(congratulateViewFlag.equals("2")){
//							startActivity(new Intent(mContext, PGActivity.class));
//						}
//						else {
//							Intent intent = new Intent(mContext, CongratulateRetailerActivity.class);
//							intent.putExtra("congratulateView", "2");
//							startActivity(intent);
//						}	
                        setLoginForNetbanking();
//						startActivity(new Intent(mContext, PGActivity.class));
                    } else if (registrationPreference.equals("Deposit")) {
//						if(congratulateViewFlag.equals("2")){
//							startActivity(new Intent(mContext, BankCashDepositActivity.class));
//						}
//						else {
//							Intent intent = new Intent(mContext, CongratulateRetailerActivity.class);
//							intent.putExtra("congratulateView", "2");
//							startActivity(intent);
//						}
                        Intent intent = new Intent(mContext, BankCashDepositActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        //startActivity(new Intent(mContext, BankCashDepositActivity.class));
                    }
                } else if (registrationInterest.equals("Retailer")) {
                    if (registrationPreference.equals("Netbanking")) {
//						startActivity(new Intent(mContext, PGActivity.class));
                        setLoginForNetbanking();
                    } else if (registrationPreference.equals("Deposit")) {
                        Intent intent = new Intent(mContext, BankCashDepositActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    }
                }
            }
        });
    }

    void setLoginForNetbanking() {
        Intent intent = new Intent(mContext, LoginActivity.class);
        intent.putExtra("class", "PGActivity");
        if (!retailer_mobile.equals("") && !retailer_pin.equals("")) {
            intent.putExtra("retailer_mobile", retailer_mobile);
            intent.putExtra("retailer_pin", retailer_pin);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public class GetDistributorsByLocationTask extends AsyncTask<String, String, String>
            implements OnDismissListener {
        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
            listValuePair.add(new BasicNameValuePair("method", "distributorByLocation"));
            listValuePair.add(new BasicNameValuePair("city", Utility.getSharedPreferences(CongratulateRetailerActivity.this, Constants.SHAREDPREFERENCE_RETAILER_FORM_CITY)));
            listValuePair.add(new BasicNameValuePair("state", Utility.getSharedPreferences(CongratulateRetailerActivity.this, Constants.SHAREDPREFERENCE_RETAILER_FORM_STATE)));
            listValuePair.add(new BasicNameValuePair("req_by", "android"));

            String response = RequestClass.getInstance()
                    .readPay1Request(CongratulateRetailerActivity.this,
                            Constants.API_URL, listValuePair);
            Log.e("background", response);

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("post execute", result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            try {
                if (!result.startsWith("Error")) {
                    result = result.substring(result.indexOf("([{"), result.lastIndexOf("}])") + 3);
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");

                    JSONArray jsonArray = new JSONArray(replaced);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray distributors = (JSONArray) jsonObject.get("distributors");
                        final ArrayList<String> distributorsArray = new ArrayList();
                        for (int i = 0; i < distributors.length(); i++) {
                            distributorsArray.add(distributors.getJSONObject(i).getJSONObject("distributors").getString("name"));
                        }
                        spinnerTextView.update(distributorsArray);

                        View view = (View) spinnerTextView.spinnerDialogView;
                        ImageView searchDialogSpinner = (ImageView) view.findViewById(R.id.spinner_dialog_search);
                        searchDialogSpinner.setVisibility(View.VISIBLE);

                        final EditText spinnerDialogSearchBox = (EditText) view.findViewById(R.id.spinner_dialog_search_box);
                        final LinearLayout spinnerDialogHead = (LinearLayout) view.findViewById(R.id.spinner_dialog_head);
                        searchDialogSpinner.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                spinnerDialogHead.setVisibility(View.GONE);
                                spinnerDialogSearchBox.setVisibility(View.VISIBLE);
                                spinnerDialogSearchBox.requestFocus();
                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.showSoftInput(spinnerDialogSearchBox, InputMethodManager.SHOW_IMPLICIT);
                            }
                        });

                        spinnerDialogSearchBox.addTextChangedListener(new TextWatcher() {

                            @Override
                            public void onTextChanged(CharSequence s, int start,
                                                      int before, int count) {
                                ArrayList<String> filteredList = new ArrayList<String>();
                                for (String distributor : distributorsArray) {
                                    if (distributor.toLowerCase().contains(s.toString().toLowerCase())) {
                                        filteredList.add(distributor);
                                    }
                                }
                                spinnerTextView.update(filteredList);
                            }

                            @Override
                            public void beforeTextChanged(CharSequence s,
                                                          int start, int count, int after) {
                                // TODO Auto-generated method stub

                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                // TODO Auto-generated method stub

                            }
                        });

                        Log.e("distributors", String.valueOf(distributorsArray));
                    } else {
                    }

                } else {
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Constants.showOneButtonFailureDialog(CongratulateRetailerActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                e.printStackTrace();
                Constants.showOneButtonFailureDialog(CongratulateRetailerActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(CongratulateRetailerActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            GetDistributorsByLocationTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            GetDistributorsByLocationTask.this.cancel(true);
            dialog.cancel();
        }

    }
}	
