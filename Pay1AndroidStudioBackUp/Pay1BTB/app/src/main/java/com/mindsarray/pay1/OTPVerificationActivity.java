package com.mindsarray.pay1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.Address;
import com.mindsarray.pay1.databasehandler.AddressDataSource;
import com.mindsarray.pay1.databasehandler.VMNDataSource;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OTPVerificationActivity extends Activity {
    EditText editText_OTP;
    Context mContext;
    private static final String TAG = "OTP verification";
    VMNDataSource vmnDataSource;

    AddressDataSource addressDataSource;
    String intentClass;
    Button button_Cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_verification_activity);
        mContext = OTPVerificationActivity.this;
        editText_OTP = (EditText) findViewById(R.id.editText_OTP);
        TextView textView_Mobile = (TextView) findViewById(R.id.textView_Mobile);
        textView_Mobile.setText(Utility.getUserMobileNumber(mContext));
        button_Cancel = (Button) findViewById(R.id.button_Cancel);
        ImageView imageView_Edit = (ImageView) findViewById(R.id.imageView_Edit);
        vmnDataSource = new VMNDataSource(OTPVerificationActivity.this);
        addressDataSource = new AddressDataSource(OTPVerificationActivity.this);
        imageView_Edit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });


        button_Cancel.setVisibility(View.INVISIBLE);
        button_Cancel.postDelayed(new Runnable() {
            public void run() {
                button_Cancel.setVisibility(View.VISIBLE);
            }
        }, 10000);
        button_Cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // finish();
                new LoginTask().execute();
            }
        });

        Button button_Submit = (Button) findViewById(R.id.button_Submit);
        button_Submit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (editText_OTP.getText().toString().trim().length() != 0) {
                    if (Utility.getUserMobileNumber(OTPVerificationActivity.this).toString()
                            .trim().length() == 10) {
                        new OTPVerificationNumberTask().execute(editText_OTP
                                .getText().toString().trim());
                    } else {
                        finish();
                    }
                } else {
                    editText_OTP.setBackgroundDrawable(mContext.getResources().getDrawable(
                            R.drawable.edittext_valid_background_selector));
                    editText_OTP.setPadding(10, 10, 10, 10);
                    editText_OTP.setTextSize(14);
                    editText_OTP.setError("Please enter correct OTP");
                    editText_OTP.setTextColor(mContext.getResources()
                            .getColor(R.color.Black));

                }
            }
        });
    }


    public class LoginTask extends AsyncTask<String, String, String> implements
            OnDismissListener {
        ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            PackageInfo pinfo;
            int versionNumber = 0;
            String versionName = "";
            try {
                pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);

                versionNumber = pinfo.versionCode;
                versionName = pinfo.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
            listValuePair.add(new BasicNameValuePair("method", "authenticate_new"));
            Utility.getSharedPreferences(OTPVerificationActivity.this, "UserPin");
            listValuePair
                    .add(new BasicNameValuePair("mobile", Utility.getUserMobileNumber(OTPVerificationActivity.this)));
            listValuePair.add(new BasicNameValuePair("password", Utility.getSharedPreferences(OTPVerificationActivity.this, "UserPin")));
            /* listValuePair.add(new BasicNameValuePair("uuid","")); */
            listValuePair.add(new BasicNameValuePair("mode", "test"));
            //listValuePair.add(new BasicNameValuePair("type", "1"));
            listValuePair.add(new BasicNameValuePair("device_type", "android"));
            if (Utility
                    .getUUID(OTPVerificationActivity.this,
                            Constants.SHAREDPREFERENCE_UUID) == null) {
                listValuePair.add(new BasicNameValuePair("uuid", Utility
                        .getUUID(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_UUID)));
                listValuePair.add(new BasicNameValuePair("device_id", Utility
                        .getUUID(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_UUID)));
            } else {
                listValuePair.add(new BasicNameValuePair("uuid", Utility
                        .getUUID(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_UUID)));
                listValuePair.add(new BasicNameValuePair("device_id", Utility
                        .getUUID(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_UUID)));
            }
            listValuePair.add(new BasicNameValuePair("longitude", Utility
                    .getLongitude(OTPVerificationActivity.this,
                            Constants.SHAREDPREFERENCE_LONGITUDE)));
            listValuePair.add(new BasicNameValuePair("latitude", Utility
                    .getLatitude(OTPVerificationActivity.this,
                            Constants.SHAREDPREFERENCE_LATITUDE)));
			/*
			 * listValuePair.add(new BasicNameValuePair("gcm_reg_id", Utility
			 * .getGCMID(OTPVerificationActivity.this,
			 * Constants.SHAREDPREFERENCE_GCMID)));
			 */
            listValuePair.add(new BasicNameValuePair("gcm_reg_id", Utility
                    .getSharedPreferences(OTPVerificationActivity.this,
                            Constants.SHAREDPREFERENCE_GCMID)));
            listValuePair.add(new BasicNameValuePair("version", Utility
                    .getOSVersion(OTPVerificationActivity.this,
                            Constants.SHAREDPREFERENCE_OS_VERSION)
                    + "_" + versionName));

            listValuePair.add(new BasicNameValuePair("version_code", String.valueOf(versionNumber)));
            listValuePair.add(new BasicNameValuePair("manufacturer", Utility
                    .getOSManufacturer(OTPVerificationActivity.this,
                            Constants.SHAREDPREFERENCE_OS_MANUFACTURER)));
            listValuePair.add(new BasicNameValuePair("location_src", "test"));

            String response = RequestClass.getInstance()
                    .readPay1RequestForPlan(OTPVerificationActivity.this,
                            Constants.API_URL, listValuePair);

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d("Login  ", "Plan    " + result);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            try {
                Log.e("Login Response", result);
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");

                    JSONArray jsonArray = new JSONArray(replaced);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        Utility.setUserMobileNumber(OTPVerificationActivity.this, Utility.getUserMobileNumber(OTPVerificationActivity.this));
                        Utility.setIsAppUsed(OTPVerificationActivity.this, true);
                        Utility.setLoginFlag(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_IS_LOGIN, true);
                        JSONObject jsonObject2 = jsonObject
                                .getJSONObject("description");
                        String balance = jsonObject2.getString("balance")
                                .trim();
                        String passFlag = jsonObject.getString("passFlag");
                        String userName = jsonObject2.getString("shopname")
                                .trim();

						/*
						 * String trialFlag =
						 * jsonObject2.getString("trial_flag") .trim();
						 */
                        String createdOn = jsonObject2.getString("created")
                                .trim();
                        String distributorId = jsonObject2
                                .getString("parent_id");
                        if (jsonObject.has("OTA_Fee")) {
                            String OTA_Fee = jsonObject.getString("OTA_Fee");
                            Utility.setSharedPreference(OTPVerificationActivity.this,
                                    "OTA_Fee", OTA_Fee);
                        }
                        if (jsonObject.has("trial_period")) {
                            String trial_period = jsonObject
                                    .getString("trial_period");
                            Utility.setSharedPreference(OTPVerificationActivity.this,
                                    "trial_period", trial_period);
                        }
                        String retailer_id = jsonObject2.getString("id");
                        Utility.setRentalFlag(OTPVerificationActivity.this,
                                jsonObject2.getString("rental_flag"));
                        Utility.setBalance(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_BALANCE, balance);
                        Utility.setUserName(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_USER_NAME, userName);
                        Utility.setProfileID(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_PROFILE_ID,
                                jsonObject.getString("profile_id"));
                        Utility.setUUID(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_UUID,
                                jsonObject.getString("uuid"));
                        Utility.setMobileNumber(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_MOBILE_NUMBER,
                                jsonObject2.getString("mobile"));
                        Utility.setLatitude(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_LATITUDE,
                                jsonObject2.getString("latitude"));
                        Utility.setLongitude(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_LONGITUDE,
                                jsonObject2.getString("longitude"));
                        Utility.setSharedPreference(OTPVerificationActivity.this,
                                "Distributor Id", distributorId);
                        Utility.setSharedPreference(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_RETAILER_ID,
                                retailer_id);

                        Utility.setSharedPreference(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_DEVICE_SERIAL_NO,
                                jsonObject2.getString("device_serial_no"));

						/*
						 * Utility.setSharedPreference(OTPVerificationActivity.this,
						 * Constants.SHAREDPREFERENCE_TRIAL_FLAG, trialFlag);
						 */
                        Utility.setSharedPreference(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_CREATED_ON,
                                createdOn);

                        JSONArray vmns = jsonObject.getJSONArray("vmnList");

                        if (vmns.length() != 0) {
                            try {
                                vmnDataSource.open();
                                vmnDataSource.deleteVMN();
                                for (int i = 0, j = 1; i < vmns.length(); i++, j++) {
                                    vmnDataSource.createVMN(j,
                                            String.valueOf(vmns.get(i)));
                                }
                            } catch (SQLException exception) {
                                // TODO: handle exception
                                // //// Log.e("Er ", exception.getMessage());
                            } catch (Exception e) {
                                e.printStackTrace();
                                // //// Log.e("Er ", e.getMessage());
                            } finally {
                                vmnDataSource.close();
                            }
                        }

                        String pg_flag = jsonObject.getString("pg_flag");
                        Utility.setCookieName(
                                OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_COOKIE_NAME_RESPONSE,
                                "CAKEPHP");
                        Utility.setCookieValue(
                                OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_COOKIE_VALUE_RESPONSE,
                                jsonObject.getString("CAKEPHP"));
                        String service_charge = jsonObject
                                .getString("service_charge");
                        if (pg_flag.equalsIgnoreCase("1")) {
                            Utility.setServiceChargeForPG(OTPVerificationActivity.this,
                                    Integer.parseInt(service_charge));
                            Utility.setIsAbleForPG(OTPVerificationActivity.this, true);
                        } else {
                            Utility.setIsAbleForPG(OTPVerificationActivity.this, false);
                            Utility.setServiceChargeForPG(OTPVerificationActivity.this,
                                    Integer.parseInt(service_charge));
                        }

                        try {
                            Utility.setMaximumAmount(OTPVerificationActivity.this,
                                    jsonObject
                                            .getString("min_amount_for_prompt"));
                        } catch (Exception exception) {
                            Utility.setMaximumAmount(OTPVerificationActivity.this, "500");
                        }

                        try {
                            addressDataSource.open();

                            String add = jsonObject2.getString("address");
                            String area = jsonObject2.getString("area_name");
                            String city = jsonObject2.getString("city_name");
                            String state = jsonObject2.getString("state_name");
                            String lat = jsonObject2.getString("latitude");
                            String lng = jsonObject2.getString("longitude");
                            String zip = jsonObject2.getString("pin");
                            List<com.mindsarray.pay1.databasehandler.Address> addresses = addressDataSource
                                    .getAllAddress();
                            if (addresses.size() == 0) {
                                addressDataSource.createAddress(add, area,
                                        city, state, zip, lat, lng);
                            } else {
                                addressDataSource.updateAddress(add, area,
                                        city, state, zip, lat, lng, addresses
                                                .get(0).getAddressID());
                            }
                        } catch (SQLException exception) {
                            // TODO: handle exception
                            // //// Log.e("Er ", exception.getMessage());
                        } catch (Exception e) {
                            e.printStackTrace();
                            // //// Log.e("Er ", e.getMessage());
                        } finally {
                            addressDataSource.close();
                        }

                        if (intentClass != null
                                && intentClass.equals("PGActivity")) {
                            Intent intent = new Intent();
                            intent.setClass(OTPVerificationActivity.this,
                                    PGActivity.class);
                            startActivity(intent);
                            finish();
                        } else if (passFlag.equalsIgnoreCase("1")) {

                            Intent intent = new Intent();
                            if (getIntent().getExtras() == null) {

                            } else {
                                intent.putExtra(
                                        Constants.REQUEST_FOR,
                                        getIntent().getExtras().getBundle(
                                                Constants.REQUEST_FOR));
                                if (getIntent().getExtras().getString(
                                        "is_login") == null) {
                                    setResult(RESULT_OK, intent);
                                } else {

                                    if (getIntent().getExtras()
                                            .getString("is_login")
                                            .equals("login")) {
                                        intent.setClass(OTPVerificationActivity.this,
                                                MainActivity.class);
                                        startActivity(intent);
                                    } else {
                                        setResult(RESULT_OK, intent);
                                    }
                                }
                            }
                            intent.setClass(OTPVerificationActivity.this,
                                    MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(OTPVerificationActivity.this,
                                    SettingsUpdatePinActivity.class);
                            intent.putExtra(Constants.IS_RESET, true);
                            startActivity(intent);
                            finish();
                        }

                        // String LastUpdateTimeCircle = Utility
                        // .getSharedPreferences(OTPVerificationActivity.this,
                        // Constants.LAST_CIRCLE_UPDATED);
                        // if (LastUpdateTimeCircle.length() > 0) {
                        // int days = (int) ((System.currentTimeMillis() - Long
                        // .parseLong(LastUpdateTimeCircle)) / (1000 * 60 * 60 *
                        // 24));
                        //
                        // if (days >= 1) {
                        // Intent circleServiceIntent = new Intent(
                        // OTPVerificationActivity.this,
                        // CircleIntentService.class);
                        // startService(circleServiceIntent);
                        // }
                        // } else {
                        // Intent circleServiceIntent = new Intent(
                        // OTPVerificationActivity.this,
                        // CircleIntentService.class);
                        // startService(circleServiceIntent);
                        // }
                    } else if (status.equalsIgnoreCase("failure")) {
                        // if
                        // (jsonObject.getString("code").equalsIgnoreCase("47"))
                        // {
                        if (jsonObject.has("forced_upgrade_flag")) {
                            if (jsonObject.getString("forced_upgrade_flag")
                                    .equalsIgnoreCase("1")) {
                                Constants.showOneButtonFailureDialog(
                                        OTPVerificationActivity.this,
                                        jsonObject.getString("description"),
                                        "Update Available",
                                        Constants.LOGIN_APP_UPDATE_SETTINGS);
                            } else {
                                Constants.showOneButtonFailureDialog(
                                        OTPVerificationActivity.this,
                                        Constants.checkCode(replaced), TAG,
                                        Constants.OTHER_ERROR);
                            }
                        } else {
                            Constants.showOneButtonFailureDialog(
                                    OTPVerificationActivity.this,
                                    Constants.checkCode(replaced), TAG,
                                    Constants.OTHER_ERROR);
                        }
                    } else if (status.equalsIgnoreCase("successOTP")) {
                        //Utility.setUserMobileNumber(OTPVerificationActivity.this,
                        // strMobileNumber);
                        JSONObject jsonObject1 = jsonObject.getJSONObject("description");
                        Constants.showOneButtonSuccessDialog(OTPVerificationActivity.this,
                                jsonObject1.getString("description"),
                                Constants.PG_SETTINGS);
                    } else {
                        Constants.showOneButtonFailureDialog(
                                OTPVerificationActivity.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(OTPVerificationActivity.this,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                e.printStackTrace();
                Constants.showOneButtonFailureDialog(OTPVerificationActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                e.printStackTrace();
                Constants.showOneButtonFailureDialog(OTPVerificationActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(OTPVerificationActivity.this);
            dialog.setMessage("Please wait.....");
            dialog.setCancelable(true);
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

                @Override
                public void onCancel(DialogInterface dialog) {
                    // TODO Auto-generated method stub
                    LoginTask.this.cancel(true);
                }
            });
            dialog.show();
            super.onPreExecute();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            LoginTask.this.cancel(true);
            dialog.cancel();
        }

    }


    public class OTPVerificationNumberTask extends
            AsyncTask<String, String, String> implements OnDismissListener {

        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
            listValuePair.add(new BasicNameValuePair("method",
                    "verifyOTPOfUserDeviceMapping"));
            listValuePair.add(new BasicNameValuePair("otp", params[0]));


            listValuePair.add(new BasicNameValuePair("device_type", "android"));
            listValuePair.add(new BasicNameValuePair("device_id", Utility
                    .getUUID(OTPVerificationActivity.this,
                            Constants.SHAREDPREFERENCE_UUID)));
            listValuePair.add(new BasicNameValuePair("longitude", Utility
                    .getLongitude(OTPVerificationActivity.this,
                            Constants.SHAREDPREFERENCE_LONGITUDE)));
            listValuePair.add(new BasicNameValuePair("latitude", Utility
                    .getLatitude(OTPVerificationActivity.this,
                            Constants.SHAREDPREFERENCE_LATITUDE)));
            listValuePair.add(new BasicNameValuePair("mobile", Utility.getUserMobileNumber(OTPVerificationActivity.this)));
            listValuePair.add(new BasicNameValuePair("gcm_reg_id", Utility
                    .getSharedPreferences(OTPVerificationActivity.this,
                            Constants.SHAREDPREFERENCE_GCMID)));
            listValuePair.add(new BasicNameValuePair("uuid", Utility
                    .getUUID(OTPVerificationActivity.this,
                            Constants.SHAREDPREFERENCE_UUID)));

            String response = RequestClass.getInstance().readPay1RequestForPlan(
                    mContext, Constants.API_URL, listValuePair);


            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("post execute", result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");

                    JSONArray jsonArray = new JSONArray(replaced);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        Utility.setIsAppUsed(OTPVerificationActivity.this, true);
                        Utility.setLoginFlag(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_IS_LOGIN, true);
                        JSONObject jsonObject2 = jsonObject
                                .getJSONObject("description");
                        String balance = jsonObject2.getString("balance")
                                .trim();
                        String passFlag = jsonObject.getString("passFlag");
                        String userName = jsonObject2.getString("shopname")
                                .trim();

						/*
						 * String trialFlag =
						 * jsonObject2.getString("trial_flag") .trim();
						 */
                        String createdOn = jsonObject2.getString("created")
                                .trim();
                        String distributorId = jsonObject2
                                .getString("parent_id");
                        if (jsonObject.has("OTA_Fee")) {
                            String OTA_Fee = jsonObject.getString("OTA_Fee");
                            Utility.setSharedPreference(OTPVerificationActivity.this,
                                    "OTA_Fee", OTA_Fee);
                        }
                        if (jsonObject.has("trial_period")) {
                            String trial_period = jsonObject
                                    .getString("trial_period");
                            Utility.setSharedPreference(OTPVerificationActivity.this,
                                    "trial_period", trial_period);
                        }
                        String retailer_id = jsonObject2.getString("id");
                        Utility.setRentalFlag(OTPVerificationActivity.this,
                                jsonObject2.getString("rental_flag"));
                        Utility.setBalance(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_BALANCE, balance);
                        Utility.setUserName(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_USER_NAME, userName);
                        Utility.setProfileID(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_PROFILE_ID,
                                jsonObject.getString("profile_id"));
                        Utility.setUUID(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_UUID,
                                jsonObject.getString("uuid"));
                        Utility.setMobileNumber(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_MOBILE_NUMBER,
                                jsonObject2.getString("mobile"));
                        Utility.setLatitude(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_LATITUDE,
                                jsonObject2.getString("latitude"));
                        Utility.setLongitude(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_LONGITUDE,
                                jsonObject2.getString("longitude"));
                        Utility.setSharedPreference(OTPVerificationActivity.this,
                                "Distributor Id", distributorId);
                        Utility.setSharedPreference(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_RETAILER_ID,
                                retailer_id);

                        Utility.setSharedPreference(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_DEVICE_SERIAL_NO,
                                jsonObject2.getString("device_serial_no"));

						/*
						 * Utility.setSharedPreference(OTPVerificationActivity.this,
						 * Constants.SHAREDPREFERENCE_TRIAL_FLAG, trialFlag);
						 */
                        Utility.setSharedPreference(OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_CREATED_ON,
                                createdOn);

                        JSONArray vmns = jsonObject.getJSONArray("vmnList");

                        if (vmns.length() != 0) {
                            try {
                                vmnDataSource.open();
                                vmnDataSource.deleteVMN();
                                for (int i = 0, j = 1; i < vmns.length(); i++, j++) {
                                    vmnDataSource.createVMN(j,
                                            String.valueOf(vmns.get(i)));
                                }
                            } catch (SQLException exception) {
                                // TODO: handle exception
                                // //// Log.e("Er ", exception.getMessage());
                            } catch (Exception e) {
                                e.printStackTrace();
                                // //// Log.e("Er ", e.getMessage());
                            } finally {
                                vmnDataSource.close();
                            }
                        }

                        String pg_flag = jsonObject.getString("pg_flag");
                        Utility.setCookieName(
                                OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_COOKIE_NAME_RESPONSE,
                                "CAKEPHP");
                        Utility.setCookieValue(
                                OTPVerificationActivity.this,
                                Constants.SHAREDPREFERENCE_COOKIE_VALUE_RESPONSE,
                                jsonObject.getString("CAKEPHP"));
                        String service_charge = jsonObject
                                .getString("service_charge");
                        if (pg_flag.equalsIgnoreCase("1")) {
                            Utility.setServiceChargeForPG(OTPVerificationActivity.this,
                                    Integer.parseInt(service_charge));
                            Utility.setIsAbleForPG(OTPVerificationActivity.this, true);
                        } else {
                            Utility.setIsAbleForPG(OTPVerificationActivity.this, false);
                            Utility.setServiceChargeForPG(OTPVerificationActivity.this,
                                    Integer.parseInt(service_charge));
                        }

                        try {
                            Utility.setMaximumAmount(OTPVerificationActivity.this,
                                    jsonObject
                                            .getString("min_amount_for_prompt"));
                        } catch (Exception exception) {
                            Utility.setMaximumAmount(OTPVerificationActivity.this, "500");
                        }

                        try {
                            addressDataSource.open();

                            String add = jsonObject2.getString("address");
                            String area = jsonObject2.getString("area_name");
                            String city = jsonObject2.getString("city_name");
                            String state = jsonObject2.getString("state_name");
                            String lat = jsonObject2.getString("latitude");
                            String lng = jsonObject2.getString("longitude");
                            String zip = jsonObject2.getString("pin");
                            List<Address> addresses = addressDataSource
                                    .getAllAddress();
                            if (addresses.size() == 0) {
                                addressDataSource.createAddress(add, area,
                                        city, state, zip, lat, lng);
                            } else {
                                addressDataSource.updateAddress(add, area,
                                        city, state, zip, lat, lng, addresses
                                                .get(0).getAddressID());
                            }
                        } catch (SQLException exception) {
                            // TODO: handle exception
                            // //// Log.e("Er ", exception.getMessage());
                        } catch (Exception e) {
                            e.printStackTrace();
                            // //// Log.e("Er ", e.getMessage());
                        } finally {
                            addressDataSource.close();
                        }

                        if (intentClass != null
                                && intentClass.equals("PGActivity")) {
                            Intent intent = new Intent();
                            intent.setClass(OTPVerificationActivity.this,
                                    PGActivity.class);
                            startActivity(intent);
                            finish();
                        } else if (passFlag.equalsIgnoreCase("1")) {

                            Intent intent = new Intent();
                            if (getIntent().getExtras() == null) {

                            } else {
                                intent.putExtra(
                                        Constants.REQUEST_FOR,
                                        getIntent().getExtras().getBundle(
                                                Constants.REQUEST_FOR));
                                if (getIntent().getExtras().getString(
                                        "is_login") == null) {
                                    setResult(RESULT_OK, intent);
                                } else {

                                    if (getIntent().getExtras()
                                            .getString("is_login")
                                            .equals("login")) {
                                        intent.setClass(OTPVerificationActivity.this,
                                                MainActivity.class);
                                        startActivity(intent);
                                    } else {
                                        setResult(RESULT_OK, intent);
                                    }
                                }
                            }
                            intent.setClass(OTPVerificationActivity.this,
                                    MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(OTPVerificationActivity.this,
                                    SettingsUpdatePinActivity.class);
                            intent.putExtra(Constants.IS_RESET, true);
                            startActivity(intent);
                            finish();
                        }

                        // String LastUpdateTimeCircle = Utility
                        // .getSharedPreferences(OTPVerificationActivity.this,
                        // Constants.LAST_CIRCLE_UPDATED);
                        // if (LastUpdateTimeCircle.length() > 0) {
                        // int days = (int) ((System.currentTimeMillis() - Long
                        // .parseLong(LastUpdateTimeCircle)) / (1000 * 60 * 60 *
                        // 24));
                        //
                        // if (days >= 1) {
                        // Intent circleServiceIntent = new Intent(
                        // OTPVerificationActivity.this,
                        // CircleIntentService.class);
                        // startService(circleServiceIntent);
                        // }
                        // } else {
                        // Intent circleServiceIntent = new Intent(
                        // OTPVerificationActivity.this,
                        // CircleIntentService.class);
                        // startService(circleServiceIntent);
                        // }
                    } else if (status.equalsIgnoreCase("failure")) {
                        // if
                        // (jsonObject.getString("code").equalsIgnoreCase("47"))
                        // {
                        if (jsonObject.has("forced_upgrade_flag")) {
                            if (jsonObject.getString("forced_upgrade_flag")
                                    .equalsIgnoreCase("1")) {
                                Constants.showOneButtonFailureDialog(
                                        OTPVerificationActivity.this,
                                        jsonObject.getString("description"),
                                        "Update Available",
                                        Constants.LOGIN_APP_UPDATE_SETTINGS);
                            } else {
                                Constants.showOneButtonFailureDialog(
                                        OTPVerificationActivity.this,
                                        Constants.checkCode(replaced), TAG,
                                        Constants.OTHER_ERROR);
                            }
                        } else {
                            Constants.showOneButtonFailureDialog(
                                    OTPVerificationActivity.this,
                                    Constants.checkCode(replaced), TAG,
                                    Constants.OTHER_ERROR);
                        }
                    } else if (status.equalsIgnoreCase("successOTP")) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("description");
                        Constants.showOneButtonSuccessDialog(OTPVerificationActivity.this,
                                jsonObject1.getString("description"),
                                Constants.OTP_SETTINGS_LOGIIN);
                    } else {
                        Constants.showOneButtonFailureDialog(
                                OTPVerificationActivity.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(mContext,
                            Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                            Constants.OTHER_ERROR);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Constants.showOneButtonFailureDialog(mContext,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                e.printStackTrace();
                Constants.showOneButtonFailureDialog(mContext,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(mContext);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            OTPVerificationNumberTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            OTPVerificationNumberTask.this.cancel(true);
            dialog.cancel();
        }

    }
}
