package com.mindsarray.pay1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.StatusAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RequestActivity extends Activity {
    private Button btnSearch;
    StringBuilder builder;
    ListView mStatusListView;
    static final int DATE_DIALOG_ID = 0;
    ArrayList<String> statusList;
    StatusAdapter adapter;
    EditText editMobileNum;
    private static final String TAG = "Request";

    /*
     * TextView textBalance; Button mBtnLogin, mBtnLogOut, mBtnHome, mBtnHelp;
     */
    Bundle bundle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        // WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.request_activity);
        tracker = Constants.setAnalyticsTracker(this, tracker);
        bundle = getIntent().getExtras();
        if (Utility.getLoginFlag(RequestActivity.this,
                Constants.SHAREDPREFERENCE_IS_LOGIN)) {
            mInsideonCreate();

        } else {
            Intent intent = new Intent(RequestActivity.this,
                    LoginActivity.class);
            intent.putExtra(Constants.REQUEST_FOR, bundle);
            startActivityForResult(intent, Constants.LOGIN);

        }

    }

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        //tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.LOGIN) {
                bundle = data.getExtras().getBundle(Constants.REQUEST_FOR);

                mInsideonCreate();
            } else {

            }
        } else {

        }
    }

    private void mInsideonCreate() {
        // TODO Auto-generated method stub

        findViewById();
        statusList = new ArrayList<String>();
        new GetRequestTask().execute();
        btnSearch.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (editMobileNum.getText().length() != 10) {
                    // Constants.showCustomToast(RequestActivity.this,
                    // "Please Enter a valid mobile number");
                } else {
                    new GetSearchTask().execute();
                }
            }
        });

        if (bundle.getInt(Constants.REQUEST_FOR) == Constants.MOBILE_TAB) {
            adapter = new StatusAdapter(this, statusList, 1);
        } else if (bundle.getInt(Constants.REQUEST_FOR) == Constants.DTH_TAB) {
            adapter = new StatusAdapter(this, statusList, 2);
        } else if (bundle.getInt(Constants.REQUEST_FOR) == Constants.ENTERTAINMENT_TAB) {
            adapter = new StatusAdapter(this, statusList, 1);
        } else if (bundle.getInt(Constants.REQUEST_FOR) == Constants.BILL_TAB) {
            adapter = new StatusAdapter(this, statusList, 1);
        }

        mStatusListView.setAdapter(adapter);
    }

    private void findViewById() {
        btnSearch = (Button) findViewById(R.id.btnSearch);

        mStatusListView = (ListView) findViewById(R.id.mStatusListView);
        editMobileNum = (EditText) findViewById(R.id.editMobileNum);
    }

    public class GetRequestTask extends AsyncTask<String, String, String>
            implements OnDismissListener {
        ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method", "lastten"));

            if (bundle.getInt(Constants.REQUEST_FOR) == Constants.MOBILE_TAB) {
                listValuePair.add(new BasicNameValuePair("service", "1"));
            } else if (bundle.getInt(Constants.REQUEST_FOR) == Constants.DTH_TAB) {
                listValuePair.add(new BasicNameValuePair("service", "2"));
            } else if (bundle.getInt(Constants.REQUEST_FOR) == Constants.ENTERTAINMENT_TAB) {
                listValuePair.add(new BasicNameValuePair("service", "3"));
            } else if (bundle.getInt(Constants.REQUEST_FOR) == Constants.BILL_TAB) {
                listValuePair.add(new BasicNameValuePair("service", "4"));
            }
            listValuePair.add(new BasicNameValuePair("device_type", "android"));

            String response = RequestClass.getInstance().readPay1Request(
                    RequestActivity.this, Constants.API_URL, listValuePair);
            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            super.onPostExecute(result);
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    statusList.clear();
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray jsonDescription = jsonObject
                                .getJSONArray("description");
                        JSONArray array2 = jsonDescription.getJSONArray(0);
                        for (int i = 0; i < array2.length(); i++) {
                            JSONObject json_data = array2.getJSONObject(i);
                            statusList.add(json_data.toString());

                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        Constants.showOneButtonFailureDialog(
                                RequestActivity.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(RequestActivity.this,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(RequestActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(RequestActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(RequestActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            GetRequestTask.this.cancel(true);
                            dialog.cancel();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            GetRequestTask.this.cancel(true);
            dialog.cancel();
        }
    }

    public class GetSearchTask extends AsyncTask<String, String, String>
            implements OnDismissListener {

        ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method",
                    "mobileTransactions"));

            listValuePair.add(new BasicNameValuePair("mobile", editMobileNum
                    .getText().toString()));
            listValuePair.add(new BasicNameValuePair("service", "1"));

            String response = RequestClass.getInstance().readPay1Request(
                    RequestActivity.this, Constants.API_URL, listValuePair);
            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            super.onPostExecute(result);
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    statusList.clear();
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray jsonDescription = jsonObject
                                .getJSONArray("description");
                        JSONArray array2 = jsonDescription.getJSONArray(0);
                        for (int i = 0; i < array2.length(); i++) {
                            JSONObject json_data = array2.getJSONObject(i);
                            statusList.add(json_data.toString());

                        }
                        // Log.d("Arra", "Arra");
                        adapter.notifyDataSetChanged();
                    } else {
                        Constants.showOneButtonFailureDialog(
                                RequestActivity.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(RequestActivity.this,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(RequestActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(RequestActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(RequestActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            GetSearchTask.this.cancel(true);
                            dialog.cancel();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            GetSearchTask.this.cancel(true);
            dialog.cancel();
        }
    }

}
