package com.mindsarray.pay1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.EntertainmentRechargeMainAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class EntertainmentRechargeMainActivty extends Activity {

    ArrayList<String> productList;
    ListView listProduct;
    EntertainmentRechargeMainAdapter adapter;
    private static final String TAG = "Entertainment";
    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        // WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.entertainmentrechargemain_activity);
        productList = new ArrayList<String>();
        adapter = new EntertainmentRechargeMainAdapter(
                EntertainmentRechargeMainActivty.this, 1, productList);
        listProduct = (ListView) findViewById(R.id.listProduct);
        listProduct.setAdapter(adapter);
//		 tracker = Constants.setAnalyticsTracker(this, tracker);
        //analytics = GoogleAnalytics.getInstance(this);
        tracker = Constants.setAnalyticsTracker(this, tracker);

        new GetVASListTask().execute();

        listProduct.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {
                Intent intent = new Intent(
                        EntertainmentRechargeMainActivty.this,
                        EntertainmentRechargeActivity.class);
                try {
                    JSONObject jsonObject = new JSONObject(productList
                            .get(position));

                    intent.putExtra(Constants.PRODUCT_NAME, jsonObject
                            .getJSONObject("prods").getString("name"));
                    intent.putExtra(Constants.PRODUCT_ID, jsonObject
                            .getJSONObject("prods").getString("product_id"));
                    intent.putExtra(Constants.PRODUCT_DETAILS, jsonObject
                            .getJSONObject("prods").toString());

                } catch (JSONException e1) {
                    // e1.printStackTrace();
                }

                startActivity(intent);

            }
        });
    }

    //	public static GoogleAnalytics analytics; public static Tracker tracker;
    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
//			//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
        //tracker.setScreenName(TAG);
        Constants.trackOnStart(tracker, TAG);
        //.build());
    }

    public class GetVASListTask extends AsyncTask<String, String, String>
            implements OnDismissListener {

        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair
                    .add(new BasicNameValuePair("method", "getVASProducts"));
            listValuePair.add(new BasicNameValuePair("device_type", "android"));
            String response = RequestClass.getInstance().readPay1Request(
                    EntertainmentRechargeMainActivty.this, Constants.API_URL,
                    listValuePair);

            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            if (!result.startsWith("Error")) {
                String replaced = result.replace("(", "").replace(")", "")
                        .replace(";", "");
                Utility.setSharedPreference(
                        EntertainmentRechargeMainActivty.this,
                        Constants.ENTERTAINMENT, replaced);

                try {
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);

                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray descArray = jsonObject
                                .getJSONArray("description");
                        for (int i = 0; i < descArray.length(); i++) {
                            JSONObject json_data = descArray.getJSONObject(i);
                            productList.add(json_data.toString());
                        }

                    } else {
                        Constants.showOneButtonFailureDialog(
                                EntertainmentRechargeMainActivty.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    // e.printStackTrace();
                    Constants.showOneButtonFailureDialog(
                            EntertainmentRechargeMainActivty.this,
                            Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                            Constants.OTHER_ERROR);
                } catch (Exception e) {
                    // TODO: handle exception
                    Constants.showOneButtonFailureDialog(
                            EntertainmentRechargeMainActivty.this,
                            Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                            Constants.OTHER_ERROR);
                }
            } else {
                String replaced = Utility.getSharedPreferences(
                        EntertainmentRechargeMainActivty.this,
                        Constants.ENTERTAINMENT);
                try {

                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);

                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray descArray = jsonObject
                                .getJSONArray("description");
                        for (int i = 0; i < descArray.length(); i++) {
                            JSONObject json_data = descArray.getJSONObject(i);
                            productList.add(json_data.toString());
                        }

                    } else {
                        Constants.showOneButtonFailureDialog(
                                EntertainmentRechargeMainActivty.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    // e.printStackTrace();
                    Constants.showOneButtonFailureDialog(
                            EntertainmentRechargeMainActivty.this,
                            Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                            Constants.OTHER_ERROR);
                } catch (Exception e) {
                    // TODO: handle exception
                    Constants.showOneButtonFailureDialog(
                            EntertainmentRechargeMainActivty.this,
                            Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                            Constants.OTHER_ERROR);
                }
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(EntertainmentRechargeMainActivty.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            GetVASListTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            GetVASListTask.this.cancel(true);
            dialog.cancel();
        }

    }

}
