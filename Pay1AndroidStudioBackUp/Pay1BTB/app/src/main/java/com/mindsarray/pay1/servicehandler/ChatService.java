package com.mindsarray.pay1.servicehandler;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class ChatService extends Service {

    public static final String TAG = "Chat service";
    private Timer myTimer = null;
    int counter = 0;

    /**
     * Called when the service is first created.
     */
    @Override
    public void onCreate() {

    }

    @SuppressWarnings("static-access")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        try {
            // counter = 0;
            checkCounter(startId);
        } catch (Exception exception) {
        }
        // Log.w("CHAT", intent.getStringExtra("RECIPIENT"));
        // Log.w("CHAT", intent.getStringExtra("USER"));

        return super.START_NOT_STICKY;

    }

    /**
     * Called when the service is started.
     */
    @Override
    public void onStart(Intent intent, int startid) {

    }

    /**
     * Called when the service is onBind.
     */
    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Called when the service is destroyed.
     */
    @Override
    public void onDestroy() {
        myTimer.cancel();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            // Toast.makeText(getApplicationContext(), "" + msg,
            // Toast.LENGTH_LONG)
            // .show();
            // Log.w("Message ", msg + "");

            new SendMailTask().execute();
            stopSelf(msg.arg1);
        }
    };

    /**
     * Alerts insert.
     */
    public void checkCounter(final int startId) {

        myTimer = new Timer();
        myTimer.schedule(new TimerTask() {

            @Override
            public void run() { // do your work

                try {
                    // Log.w("Counter", counter + "");
                    if (counter >= 300) {
                        Message myMessage = new Message();
                        Bundle resBundle = new Bundle();
                        resBundle.putString("status", "Mail sent");
                        myMessage.obj = resBundle;
                        myMessage.arg1 = startId;
                        handler.sendMessage(myMessage);
                        counter = 0;
                        myTimer.cancel();
                        // stopService(new Intent(ChatService.this,
                        // ChatService.class));

                    } else {
                        counter++;
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                } finally {
                }
            }

        }, 0, 1000);
    }

    public class SendMailTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method",
                    "ccNotResponding"));
            listValuePair.add(new BasicNameValuePair("support_id", Utility
                    .getChatServiceID(ChatService.this,
                            Constants.SHAREDPREFERENCE_CHAT_SERVICE_ID)));
            listValuePair.add(new BasicNameValuePair("user", Utility
                    .getMobileNumber(ChatService.this,
                            Constants.SHAREDPREFERENCE_MOBILE_NUMBER)));
            listValuePair.add(new BasicNameValuePair("time", Utility
                    .getChatServiceTime(ChatService.this,
                            Constants.SHAREDPREFERENCE_CHAT_SERVICE_TIME)));
            listValuePair.add(new BasicNameValuePair("device_type", "android"));
            String response = RequestClass.getInstance().readPay1Request(
                    ChatService.this, Constants.API_URL, listValuePair);
            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");

                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {

                    } else {
                        // Constants.showOneButtonFailureDialog(
                        // ChatService.this,
                        // Constants.checkCode(replaced), TAG,
                        // Constants.OTHER_ERROR);
                    }

                } else {
                    // Constants.showOneButtonFailureDialog(ChatService.this,
                    // Constants.ERROR_INTERNET, TAG,
                    // Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                // Constants.showOneButtonFailureDialog(ChatService.this,
                // Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                // Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                // Constants.showOneButtonFailureDialog(ChatService.this,
                // Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                // Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
    }
}
