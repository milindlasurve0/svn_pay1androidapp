package com.mindsarray.pay1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Administrator on 6/20/2016.
 */

public class LeadGenerationOtpActivity extends Activity {
    private static final String TAG = "Lead Generation ";
    TextView textViewResendOtp, textViewChangeMobile;
    ImageView imageViewClose;
    Button buttonProceed;
    EditText editOTP;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lead_generation_otp_layout);
        textViewResendOtp = (TextView) findViewById(R.id.textViewResendOtp);
        textViewChangeMobile = (TextView) findViewById(R.id.textViewChangeMobile);
        imageViewClose = (ImageView) findViewById(R.id.imageViewClose);
        buttonProceed = (Button) findViewById(R.id.buttonProceed);
        editOTP = (EditText) findViewById(R.id.editOTP);
        mContext = LeadGenerationOtpActivity.this;
        buttonProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editOTP.getText().toString().trim().length() != 0) {
                    new OTPVerificationNumberTask().execute(editOTP.getText().toString().trim()
                            , getIntent().getStringExtra("MobileNumber"));
                }
            }
        });
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        textViewChangeMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        textViewResendOtp.setVisibility(View.INVISIBLE);
        textViewResendOtp.postDelayed(new Runnable() {
            public void run() {
                textViewResendOtp.setVisibility(View.VISIBLE);
            }
        }, 10000);

        textViewResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ResendOTPNumberTask().execute();
            }
        });

    }

    public class OTPVerificationNumberTask extends
            AsyncTask<String, String, String> implements DialogInterface.OnDismissListener

    {

        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
            listValuePair.add(new BasicNameValuePair("method",
                    "verifyRetDistLeads"));
            listValuePair.add(new BasicNameValuePair("otp", params[0]));
            listValuePair.add(new BasicNameValuePair("r_m", params[1]));


            String response = RequestClass.getInstance().readPay1Request(
                    LeadGenerationOtpActivity.this, Constants.API_URL, listValuePair);


            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("post execute", result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            try {
                if (!result.startsWith("Error")) {

                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");

                    JSONArray jsonArray = new JSONArray(replaced);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {


                        Constants.showOneButtonSuccessDialog(mContext,
                                jsonObject.getString("description"),
                                Constants.OTP_SETTINGS_LEAD);


                    } else {
                        Constants.showOneButtonFailureDialog(
                                LeadGenerationOtpActivity.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(mContext,
                            Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                            Constants.OTHER_ERROR);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Constants.showOneButtonFailureDialog(mContext,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                e.printStackTrace();
                Constants.showOneButtonFailureDialog(mContext,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(mContext);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            OTPVerificationNumberTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            OTPVerificationNumberTask.this.cancel(true);
            dialog.cancel();
        }

    }


    public class ResendOTPNumberTask extends
            AsyncTask<String, String, String> implements DialogInterface.OnDismissListener

    {

        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
            listValuePair.add(new BasicNameValuePair("method",
                    "sendOTPToRetDistLeads"));
            listValuePair.add(new BasicNameValuePair("interest", Utility.getSharedPreferences
                    (LeadGenerationOtpActivity.this, "Registration_interest")));
            listValuePair.add(new BasicNameValuePair("mobile", getIntent().getStringExtra("MobileNumber")));


            String response = RequestClass.getInstance().readPay1Request(
                    LeadGenerationOtpActivity.this, Constants.API_URL, listValuePair);


            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("post execute", result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            try {
                if (!result.startsWith("Error")) {

                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");

                    JSONArray jsonArray = new JSONArray(replaced);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {


                        Constants.showOneButtonSuccessDialog(mContext,
                                jsonObject.getString("description"),
                                Constants.PG_SETTINGS);


                    } else {
                        Constants.showOneButtonFailureDialog(
                                LeadGenerationOtpActivity.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(mContext,
                            Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                            Constants.OTHER_ERROR);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Constants.showOneButtonFailureDialog(mContext,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                e.printStackTrace();
                Constants.showOneButtonFailureDialog(mContext,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(mContext);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            ResendOTPNumberTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            ResendOTPNumberTask.this.cancel(true);
            dialog.cancel();
        }

    }
}
