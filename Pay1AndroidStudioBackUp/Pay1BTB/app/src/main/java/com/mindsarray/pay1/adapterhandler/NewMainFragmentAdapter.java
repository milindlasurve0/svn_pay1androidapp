package com.mindsarray.pay1.adapterhandler;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.mindsarray.pay1.MainTabRechargesFragmentNew;
import com.mindsarray.pay1.MainTabWholesaleFragmentNew;

public class NewMainFragmentAdapter extends FragmentStatePagerAdapter {
    protected static final String[] CONTENT = new String[]{"RECHARGE",
            "WHOLESALE"};

    private int mCount = CONTENT.length;

    public NewMainFragmentAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {
        // return TestFragment.newInstance(CONTENT[position % CONTENT.length]);
        switch (position) {

            case 0:
                return MainTabRechargesFragmentNew
                        .newInstance("FirstFragment, Instance 1");
            case 1:
                return MainTabWholesaleFragmentNew
                        .newInstance("SecondFragment, Instance 2");

            default:
                return null;
        }
    }


    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return NewMainFragmentAdapter.CONTENT[position % CONTENT.length];
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }
}
