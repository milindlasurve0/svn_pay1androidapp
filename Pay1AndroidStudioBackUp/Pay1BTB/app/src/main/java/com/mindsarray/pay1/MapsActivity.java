package com.mindsarray.pay1;

/**
 * Created by rohan on 21/7/15.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mindsarray.pay1.constant.Constants;

public class MapsActivity extends FragmentActivity implements
        OnMapReadyCallback {

    private static final String TAG = "MapsActivity";
    private String latitude;
    private String longitude;
    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            latitude = extras.getString("latitude");
            longitude = extras.getString("longitude");
        }
        tracker = Constants.setAnalyticsTracker(this, tracker);
        /*AnalyticsApplication application = (AnalyticsApplication) getApplication();
		tracker = application.getDefaultTracker();*/


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Button setLocationButton = (Button) findViewById(R.id.button_setLocation);
        setLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(
                        Activity.RESULT_OK,
                        new Intent().putExtra("latitude", latitude).putExtra(
                                "longitude", longitude));
                finish();
            }
        });
    }


    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();

        Constants.sendGoogleAnalytics(MapsActivity.this, "Map Layout");
        Log.i(TAG, "Setting screen name: " + TAG);
        tracker.setScreenName("Image~" + TAG);
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    @Override
    public void onMapReady(final GoogleMap map) {

        try {

            if (!latitude.equalsIgnoreCase("") || !latitude.isEmpty()) {
                LatLng currentLocation = new LatLng(
                        Double.parseDouble(latitude),
                        Double.parseDouble(longitude));
                map.addMarker(new MarkerOptions().position(currentLocation)
                        .title("Shop Location"));
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        currentLocation, 15));
                // map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000,
                // null);

                map.setMyLocationEnabled(true);
                map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng point) {
                        map.clear();
                        latitude = point.latitude + "";
                        longitude = point.longitude + "";
                        map.addMarker(markerOptions(point));
                    }
                });

                map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {

                    @Override
                    public void onMarkerDragStart(Marker marker) {

                    }

                    @Override
                    public void onMarkerDragEnd(Marker marker) {
                        map.clear();
                        LatLng point = marker.getPosition();
                        latitude = point.latitude + "";
                        longitude = point.longitude + "";
                        map.addMarker(markerOptions(point));
                    }

                    @Override
                    public void onMarkerDrag(Marker marker) {

                    }
                });
            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }

    }

    MarkerOptions markerOptions(LatLng point) {
        MarkerOptions options = new MarkerOptions();
        options.position(point);
        options.title("Shop Location");
        options.draggable(true);
        options.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        return options;
    }
}
