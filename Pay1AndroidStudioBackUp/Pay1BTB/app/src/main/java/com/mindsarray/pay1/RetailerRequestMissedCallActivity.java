package com.mindsarray.pay1;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class RetailerRequestMissedCallActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.retailerrequestmissedcall_activity);

        Button callNow = (Button) findViewById(R.id.call_phone_retailer);
        callNow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "02212345678"));
                startActivity(intent);
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(RetailerRequestMissedCallActivity.this,
                        CongratulateRetailerActivity.class);
                startActivity(i);
            }
        }, 3000);
    }
}