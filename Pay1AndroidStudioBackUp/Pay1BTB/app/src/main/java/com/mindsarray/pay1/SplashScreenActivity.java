package com.mindsarray.pay1;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.DisplayMetrics;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.Pay1SQLiteHelper;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.servicehandler.RegistrationIntentService;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class SplashScreenActivity extends Activity {

    private static int SPLASH_TIME_OUT = 4000;
    private static final String TAG = "Splash Activity";

    public static final String AUTHORITY = "com.mindsarray.pay1.provider";
    public static final String ACCOUNT_TYPE = "Pay1 Merchant";
    public static final String ACCOUNT = "Pay1 Merchant";
    public static final long SECONDS_PER_HOUR = 3600L;
    public static final long SYNC_INTERVAL_IN_HOURS = 12L;
    public static final long SYNC_INTERVAL = SYNC_INTERVAL_IN_HOURS * SECONDS_PER_HOUR;

    Account syncAccount;
    ContentResolver syncResolver;

    public static GoogleAnalytics analytics;
    //public static Tracker tracker;

    Activity mActivity = SplashScreenActivity.this;
    Context mContext = SplashScreenActivity.this;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //tracker = Constants.setAnalyticsTracker(this, tracker);
        /*AnalyticsApplication application = (AnalyticsApplication) getApplication();
		tracker = application.getDefaultTracker();*/

        syncAccount = CreateSyncAccount(this);
        syncResolver = getContentResolver();

        setContentView(R.layout.splashscreen_activity);
        try {
            Locale myLocale = new Locale(
                    Utility.getLanguageSharedPreferences(SplashScreenActivity.this));
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);
        } catch (Exception ed) {

        } finally {

        }

        if (Constants.checkPlayServices(mActivity)) {
            mRegistrationBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (intent.getExtras() != null) {
                        String regId = intent.getExtras().getString("registration_id");
                        Log.e("BroadcastReceiver", "" + regId);
                    }
                }
            };

            Intent intent = new Intent(mContext, RegistrationIntentService.class);
            startService(intent);

            Pay1SQLiteHelper pay1Db = new Pay1SQLiteHelper(SplashScreenActivity.this);
            pay1Db.getWritableDatabase();
            pay1Db.close();

            String lastSyncTime = Utility.getLastSyncTime(mContext);
            if (lastSyncTime.equals("")) {
                sync(syncAccount);
            }
            ContentResolver.setSyncAutomatically(syncAccount, AUTHORITY, true);
            ContentResolver.addPeriodicSync(
                    syncAccount,
                    AUTHORITY,
                    Bundle.EMPTY,
                    SYNC_INTERVAL);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(SplashScreenActivity.this,
                            MainActivity.class);
                    startActivity(i);

                    finish();
                }
            }, SPLASH_TIME_OUT);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.checkPlayServices(mActivity);
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constants.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    public static Account CreateSyncAccount(Context context) {
        Account newAccount = new Account(ACCOUNT, ACCOUNT_TYPE);

        AccountManager accountManager =
                (AccountManager) context.getSystemService(
                        ACCOUNT_SERVICE);

        if (accountManager.addAccountExplicitly(newAccount, null, null)) {

        } else {
            /*
             * The account exists or some other error occurred. Log this, report it,
             * or handle it internally.
             */
        }
        return newAccount;
    }

    public static void sync(Account sAccount) {
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_EXPEDITED, true);

        ContentResolver.requestSync(sAccount, AUTHORITY, settingsBundle);
    }

    public class CheckBalanceTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method", "updateBal"));
            String response = RequestClass.getInstance()
                    .readPay1Request(SplashScreenActivity.this,
                            Constants.API_URL, listValuePair);
            listValuePair.clear();

            return response;

        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");

                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    // {"status":"success","description":"0","login":0}
                    if (status.equalsIgnoreCase("success")) {
                        if (jsonObject.getString("login").equalsIgnoreCase("1")) {
                            Utility.setBalance(SplashScreenActivity.this,
                                    Constants.SHAREDPREFERENCE_BALANCE,
                                    jsonObject.getString("description"));
                        } else {

                        }
                    }
                }

            } catch (JSONException e) {
                // e.printStackTrace();
				/*
				 * Constants.showOneButtonFailureDialog(SplashScreenActivity.this
				 * , Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
				 * Constants.OTHER_ERROR);
				 */

            } catch (Exception e) {
                // TODO: handle exception
				/*
				 * Constants.showOneButtonFailureDialog(SplashScreenActivity.this
				 * , Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
				 * Constants.OTHER_ERROR);
				 */

            }
        }
    }

    private void checkNotNull(Object reference, String name) {
        if (reference == null) {
            throw new NullPointerException("Errror");
        }
    }

//	public static GoogleAnalytics analytics; public static Tracker tracker;

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
//		//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);

        //	Constants.trackOnStart(tracker, TAG);

//		//tracker.setScreenName(TAG);
//		Constants.trackOnStart(tracker, TAG);
//	       //.build());
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

        /** Comment super method to handle (avoid) backpressed event. */
        // super.onBackPressed();
    }

}
