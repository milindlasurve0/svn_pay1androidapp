package com.mindsarray.pay1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LogOutActivity extends Activity {

    private static final String TAG = "Logout";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.logout_activity);

        Button btnDone = (Button) findViewById(R.id.btnDone);
        Button btnCancel = (Button) findViewById(R.id.btnCancel);
//		tracker = Constants.setAnalyticsTracker(this, tracker);
//		
//		//easyTracker.set(Fields.SCREEN_NAME, TAG);
        tracker = Constants.setAnalyticsTracker(this, tracker);
        btnDone.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LogoutTask().execute();
//				if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
//					new LogoutTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//				else
//					new LogoutTask().execute();
            }
        });

        btnCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });
    }

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        //tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
    }

    public class LogoutTask extends AsyncTask<String, String, String> implements
            OnDismissListener {

        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method", "logout"));
            listValuePair.add(new BasicNameValuePair("device_type", "android"));
            String response = RequestClass.getInstance().readPay1Request(
                    LogOutActivity.this, Constants.API_URL, listValuePair);
            /*
			 * String replaced = response.replace("(", "").replace(")", "")
			 * .replace(";", "");
			 */
            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");

                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        Utility.setLoginFlag(LogOutActivity.this,
                                Constants.SHAREDPREFERENCE_IS_LOGIN, false);
                        Utility.setBalance(LogOutActivity.this,
                                Constants.SHAREDPREFERENCE_BALANCE, null);
                        Utility.setUserName(LogOutActivity.this,
                                Constants.SHAREDPREFERENCE_USER_NAME, null);
                        Utility.setProfileID(LogOutActivity.this,
                                Constants.SHAREDPREFERENCE_PROFILE_ID, null);
                        Utility.setMobileNumber(LogOutActivity.this,
                                Constants.SHAREDPREFERENCE_MOBILE_NUMBER, null);
                        Utility.setCookieVersion(LogOutActivity.this,
                                Constants.SHAREDPREFERENCE_COOKIE_VERSION, 0);
                        Utility.setCookieName(LogOutActivity.this,
                                Constants.SHAREDPREFERENCE_COOKIE_NAME_RESPONSE, null);
                        Utility.setCookieValue(LogOutActivity.this,
                                Constants.SHAREDPREFERENCE_COOKIE_VALUE_RESPONSE, null);
                        Utility.setCookieDomain(LogOutActivity.this,
                                Constants.SHAREDPREFERENCE_COOKIE_DOMAIN, null);
                        Utility.setCookiePath(LogOutActivity.this,
                                Constants.SHAREDPREFERENCE_COOKIE_PATH, null);
                        Utility.setCookieExpiry(LogOutActivity.this,
                                Constants.SHAREDPREFERENCE_COOKIE_EXPIRY, null);
                        Utility.setCookieObject(LogOutActivity.this, null);
                        Utility.setSharedPreference(LogOutActivity.this,
                                Constants.SHAREDPREFERENCE_DEVICE_SERIAL_NO,
                                "");

                        Utility.setSharedPreference(LogOutActivity.this,
                                Constants.SHAREDPREFERENCE_RETAILER_ID,
                                "");
                        Utility.setSharedPreference(LogOutActivity.this, Constants.SHAREDPREFERENCE_LAST_5_TRANSACTIONS, "");
                        Utility.setSharedPreference(LogOutActivity.this, Constants.SHAREDPREFERENCE_COMPLAINT_STATS, "");
                        Intent intent = new Intent(LogOutActivity.this,
                                MainActivity.class);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        finish();
                    } else {
                        Constants.showOneButtonFailureDialog(
                                LogOutActivity.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(LogOutActivity.this,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(LogOutActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(LogOutActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(LogOutActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            LogoutTask.this.cancel(true);
                            finish();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            LogoutTask.this.cancel(true);
            dialog.cancel();
            finish();
        }

    }

}
