package com.mindsarray.pay1;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;

import com.mindsarray.pay1.constant.Constants;

public class ReversalTabActivity extends TabActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//				WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.recharge_activity);
        Resources res = getResources(); // Resource object to get Drawables
        TabHost tabHost = getTabHost(); // The activity TabHost
        TabHost.TabSpec spec; // Resusable TabSpec for each tab
        Intent intent; // Reusable Intent for each tab

        intent = new Intent().setClass(this, ReversalStatusTabFragment.class);
        View tabView = createTabView(this, "COMPLAINT STATUS");
        spec = tabHost.newTabSpec("First Tab").setIndicator(tabView)
                .setContent(intent);
        tabHost.addTab(spec);
        //EasyTracker tracker = Constants.setAnalyticsTracker(this, tracker);
        intent = new Intent().setClass(this, ReversalRequestTabFragment.class);
        tabView = createTabView(this, "COMPLAINT REQUEST");
        spec = tabHost.newTabSpec("Second Tab").setIndicator(tabView)
                .setContent(intent);
        tabHost.addTab(spec);
        if (getIntent().getExtras() == null) {
            tabHost.setCurrentTab(0);
        } else {
            tabHost.setCurrentTab(getIntent().getExtras().getInt(
                    Constants.REQUEST_FOR_TAB));
        }

    }


    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Constants.showNavigationBar(ReversalTabActivity.this);
        super.onResume();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    private static View createTabView(Context context, String tabText) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_tabs,
                null, false);
        TextView tv = (TextView) view.findViewById(R.id.tabTitleText);
        tv.setText(tabText);
        return view;
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (Constants.isExpanded) {
            Constants.collapsView();
        } else {
            super.onBackPressed();
        }
    }
}