package com.mindsarray.pay1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.EarningAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class EarningActivity extends Activity {
    ArrayList<JSONObject> earningArrayList;
    ListView earningList;
    EarningAdapter earningAdapter;
    private Button btnPreWeek, btnNextWeek;
    // TextView textDateToday textSaleToday, textEarningToday,
    TextView mDateDisplay;
    String date = "";
    String preWeek, nextWeek, currentWeek;
    private static final String TAG = "Earning Main ";

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        try {
            // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
            // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            // WindowManager.LayoutParams.FLAG_FULLSCREEN);
            setContentView(R.layout.earning_activity);

//			tracker = Constants.setAnalyticsTracker(this, tracker);
//			;
//			//easyTracker.set(Fields.SCREEN_NAME, TAG);

            //analytics = GoogleAnalytics.getInstance(this);
            tracker = Constants.setAnalyticsTracker(this, tracker);

            findViewById();
            TextView textView_NoData = (TextView) findViewById(R.id.textView_NoData);
            earningList.setEmptyView(textView_NoData);
            earningArrayList = new ArrayList<JSONObject>();
            earningAdapter = new EarningAdapter(EarningActivity.this, 1,
                    earningArrayList);

            earningList.setAdapter(earningAdapter);

            btnNextWeek.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    date = nextWeek;
                    // if (date.length() == 0) {
                    // Utility.alertBoxShow(EarningActivity.this,
                    // "Can not Proceed");
                    // } else {
                    new EarningTask().execute();
                    // }

                }
            });

            btnPreWeek.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    date = preWeek;
                    // if (date.length() == 0) {
                    // Utility.alertBoxShow(EarningActivity.this,
                    // "Can not Proceed");
                    // } else {
                    new EarningTask().execute();
                    // }

                }
            });

            earningList.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1,
                                        int arg2, long arg3) {
                    // TODO Auto-generated method stub
                    String joined = null;
                    Intent intent = new Intent(EarningActivity.this,
                            EarningDetailsActivity.class);
                    try {

                        String foo = earningArrayList.get(arg2).getString(
                                "date");
                        String[] split = foo.split("-");
                        StringBuilder sb = new StringBuilder();
                        for (int i = split.length; i > 0; i--) {
                            sb.append(split[i - 1]);

                        }
                        joined = sb.toString();
                    } catch (JSONException exception) {
                        // exception.printStackTrace();
                    }

                    intent.putExtra(Constants.DATE, joined + "-" + joined);
                    startActivity(intent);
                    // finish();
                }
            });
        } catch (Exception exception) {
        }
    }

//	public static GoogleAnalytics analytics; public static Tracker tracker;

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
//		//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
        //tracker.setScreenName(TAG);
        Constants.trackOnStart(tracker, TAG);
        //.build());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.LOGIN) {
                mInsideonCreate();
            } else {

            }
        } else {
            finish();
        }
    }

    private void mInsideonCreate() {
        // TODO Auto-generated method stub

        new EarningTask().execute();

    }

    private void findViewById() {
        // TODO Auto-generated method stub
        // textDateToday = (TextView) findViewById(R.id.textDateToday);
        // textEarningToday = (TextView) findViewById(R.id.textSaleToday);
        // textSaleToday = (TextView) findViewById(R.id.textEarningToday);
        mDateDisplay = (TextView) findViewById(R.id.mDateDisplay);
        btnPreWeek = (Button) findViewById(R.id.btnPreWeek);
        btnNextWeek = (Button) findViewById(R.id.btnNextWeek);
        earningList = (ListView) findViewById(R.id.earningList);
    }

    public class EarningTask extends AsyncTask<String, String, String>
            implements OnDismissListener {
        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method", "earnings"));
            listValuePair.add(new BasicNameValuePair("date", date));
            listValuePair.add(new BasicNameValuePair("device_type", "android"));
            String response = RequestClass.getInstance().readPay1Request(
                    EarningActivity.this, Constants.API_URL, listValuePair);

            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            super.onPostExecute(result);
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    earningArrayList.clear();

                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray descArray = jsonObject
                                .getJSONArray("description");
                        JSONArray array2 = descArray.getJSONArray(0);
                        for (int i = 0; i < array2.length(); i++) {
                            JSONArray array3 = array2.getJSONArray(i);
                            JSONObject jsonObject2 = array3.getJSONObject(0);
                            earningArrayList.add(jsonObject2);
                        }

                        JSONArray preArray = jsonObject
                                .getJSONArray("prevWeek");
                        JSONArray nextArray = jsonObject
                                .getJSONArray("nextWeek");
                        JSONArray curArray = jsonObject
                                .getJSONArray("currWeek");
                        preWeek = preArray.getString(0);
                        nextWeek = nextArray.getString(0);
                        currentWeek = curArray.getString(0);
                        mDateDisplay.setText(currentWeek);

                        // JSONArray todayArray =
                        // jsonObject.getJSONArray("today");
                        // JSONArray array4 = todayArray.getJSONArray(0);
                        /* JSONArray array5 = array4.getJSONArray(0); */
                        // JSONObject jsonObject2 = array4.getJSONObject(0);
                        // textDateToday.setText(jsonObject2.getString("date"));
                        // textSaleToday.setText(jsonObject2.getString("amount"));
                        // textEarningToday.setText(jsonObject2
                        // .getString("income"));
                        earningAdapter.notifyDataSetChanged();

                        if (nextWeek.length() == 0)
                            btnNextWeek.setVisibility(View.GONE);
                        else
                            btnNextWeek.setVisibility(View.VISIBLE);
                    } else {
                        Constants.showOneButtonFailureDialog(
                                EarningActivity.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(EarningActivity.this,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(EarningActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(EarningActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(EarningActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            EarningTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            EarningTask.this.cancel(true);
            dialog.cancel();
        }

    }

    @Override
    protected void onResume() {
        Constants.showNavigationBar(EarningActivity.this);

        if (Utility.getLoginFlag(EarningActivity.this,
                Constants.SHAREDPREFERENCE_IS_LOGIN)) {
            mInsideonCreate();

        } else {
            Intent intent = new Intent(EarningActivity.this,
                    LoginActivity.class);
            // intent.putExtra(Constants.REQUEST_FOR, bundle);
            startActivityForResult(intent, Constants.LOGIN);

        }
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (Constants.isExpanded) {
            Constants.collapsView();
        } else {
            super.onBackPressed();
        }
    }
}
