package com.mindsarray.pay1;

import android.graphics.Bitmap;

/**
 * Created by Administrator on 6/14/2016.
 */
public interface AsyncResponse {
    void processFinish(Bitmap output);
}
