package com.mindsarray.pay1.servicehandler;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

public class NotificationService extends Service {
    private static Timer timer;//= new Timer();
    private Context ctx;

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        ctx = this;
        timer = new Timer();
        startService();
    }

    private void startService() {
        timer.scheduleAtFixedRate(new mainTask(), 0, 1800000);
    }

    private class mainTask extends TimerTask {
        public void run() {
            toastHandler.sendEmptyMessage(0);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        timer.cancel();

        Log.d("TEST", "TEST DEstroyed");
    }

    private final Handler toastHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            ActivityManager am = (ActivityManager) NotificationService.this
                    .getSystemService(Activity.ACTIVITY_SERVICE);

            String className = am
                    .getRunningTasks(1).get(0).topActivity
                    .getPackageName();

            if (!className.equalsIgnoreCase("com.mindsarray.pay1")) {
                Log.d("Check", "TEST OUT");
                stopService(new Intent(NotificationService.this, NotificationService.class));
                timer.cancel();
            } else {
                new GetNotificationTask().execute();
            }

        }
    };

    public class GetNotificationTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String result = Constants.getPlanData(NotificationService.this,
                    Constants.API_URL + "method=sendNotification");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            //Log.e("Check", "TEST IN "+result);
            String replaced = result.replace("(", "").replace(")", "")
                    .replace(";", "");
            try {
                JSONArray array = new JSONArray(replaced);
                JSONObject jsonObject = array.getJSONObject(0);
                String notification = jsonObject.getString("notification");
                Utility.setSharedPreference(NotificationService.this, "notification",
                        notification);
            } catch (JSONException jee) {

            } catch (Exception e) {
                // TODO: handle exception
            }
        }

    }

}