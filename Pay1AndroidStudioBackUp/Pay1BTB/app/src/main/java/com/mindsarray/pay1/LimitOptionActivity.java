package com.mindsarray.pay1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.mindsarray.pay1.adapterhandler.SettingsAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.utilities.Utility;

import java.util.ArrayList;

public class LimitOptionActivity extends Activity {
    ArrayList<String> limitOptions;
    ListView listViewSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_main_actvity);
        limitOptions = new ArrayList<String>();
        limitOptions.add("Online Limit");

        if (Utility.getSharedPreferences(LimitOptionActivity.this, "Distributor Id")
                .equalsIgnoreCase("1")) {
            limitOptions.add("Request Topup");
            limitOptions.add("Bank Details");
        }


        listViewSettings = (ListView) findViewById(R.id.listViewSettings);
        SettingsAdapter settingsAdapter = new SettingsAdapter(
                LimitOptionActivity.this, 0, limitOptions);
        listViewSettings.setAdapter(settingsAdapter);
        listViewSettings.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {

                switch (arg2) {
                    case 0:
                        Intent intent = new Intent(LimitOptionActivity.this,
                                PGActivity.class);
                        startActivity(intent);
                        break;
                    case 1:
                        Intent intent1 = new Intent(LimitOptionActivity.this,
                                RequestTopupActivity.class);
                        startActivity(intent1);
                        break;
                    case 2:
                        Intent intent2 = new Intent(LimitOptionActivity.this,
                                BankCashDepositActivity.class);
                        startActivity(intent2);
                        break;


                    default:
                        break;
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (Constants.isExpanded) {
            Constants.collapsView();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Constants.showNavigationBar(LimitOptionActivity.this);

        super.onResume();
    }

}
