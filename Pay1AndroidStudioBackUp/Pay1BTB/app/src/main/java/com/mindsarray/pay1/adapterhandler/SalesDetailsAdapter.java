package com.mindsarray.pay1.adapterhandler;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mindsarray.pay1.R;

import org.json.JSONObject;

import java.util.List;

public class SalesDetailsAdapter extends ArrayAdapter<JSONObject> {
    List<JSONObject> list;
    Context mContext;
    LayoutInflater inflater;

    public SalesDetailsAdapter(Context context, int textViewResourceId,
                               List<JSONObject> objects) {
        super(context, textViewResourceId, objects);
        // TODO Auto-generated constructor stub
        this.list = objects;
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View view = convertView;
        if (convertView == null) {
            inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.salesdetails_adapter, null);
        }

        if (position % 2 == 1) {
            view.setBackgroundColor(Color.WHITE);
        } else {
            //view.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.border_blue));/*Color(Color.WHITE);*/
            view.setBackgroundColor(mContext.getResources().getColor(
                    R.color.discount_div));
        }

        TextView textTotalIncome = (TextView) view.findViewById(R.id.textTotalIncome);
        TextView textTotal = (TextView) view.findViewById(R.id.textTotal);
        TextView textTransaction = (TextView) view.findViewById(R.id.textTransaction);
        TextView textProduct = (TextView) view.findViewById(R.id.textProduct);

        try {
            JSONObject jsonObject5 = list.get(position).getJSONObject("0");
            JSONObject jsonObject = list.get(position).getJSONObject("products");

            textTotalIncome.setText(jsonObject5.getString("amount"));
            textTotal.setText(jsonObject5.getString("income"));
            textTransaction.setText(jsonObject5.getString("counts"));
            textProduct.setText(jsonObject.getString("name"));


        } catch (Exception exception) {
            //exception.printStackTrace();
        }
        return view;

    }

}
