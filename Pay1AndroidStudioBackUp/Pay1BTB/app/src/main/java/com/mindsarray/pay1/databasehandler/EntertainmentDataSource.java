package com.mindsarray.pay1.databasehandler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class EntertainmentDataSource {
    // Database fields
    private SQLiteDatabase database;
    private Pay1SQLiteHelper dbHelper;

    public EntertainmentDataSource(Context context) {
        dbHelper = new Pay1SQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void createEntertainmet(String product_id, String product_code,
                                   String product_name, String product_price, String product_validity,
                                   String short_description, String long_description,
                                   String product_image, String data_field, String data_type,
                                   String data_length, String time) {
        ContentValues values = new ContentValues();
        values.put(Pay1SQLiteHelper.ENTERTAINMENT_PRODUCT_ID, product_id);
        values.put(Pay1SQLiteHelper.ENTERTAINMENT_PRODUCT_CODE, product_code);
        values.put(Pay1SQLiteHelper.ENTERTAINMENT_PRODUCT_NAME, product_name);
        values.put(Pay1SQLiteHelper.ENTERTAINMENT_PRODUCT_PRICE, product_price);
        values.put(Pay1SQLiteHelper.ENTERTAINMENT_PRODUCT_VALIDITY,
                product_validity);
        values.put(Pay1SQLiteHelper.ENTERTAINMENT_PRODUCT_SHORT_DESCRIPTION,
                short_description);
        values.put(Pay1SQLiteHelper.ENTERTAINMENT_PRODUCT_LONG_DESCRIPTION,
                long_description);
        values.put(Pay1SQLiteHelper.ENTERTAINMENT_PRODUCT_IMAGE, product_image);
        values.put(Pay1SQLiteHelper.ENTERTAINMENT_PRODUCT_DATA_FIELD,
                data_field);
        values.put(Pay1SQLiteHelper.ENTERTAINMENT_PRODUCT_DATA_TYPE, data_type);
        values.put(Pay1SQLiteHelper.ENTERTAINMENT_PRODUCT_DATA_LENGTH,
                data_length);
        values.put(Pay1SQLiteHelper.ENTERTAINMENT_PRODUCT_UPDATE_TIME, time);

        database.insert(Pay1SQLiteHelper.TABLE_ENTERTAINMENT, null, values);
        // Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_ENTERTAINMENT,
        // null, Pay1SQLiteHelper.ENTERTAINMENT_ID + " = " + insertId, null,
        // null, null, null);
        // cursor.moveToFirst();
        // Entertainment neEntertainment = cursorToEntertainment(cursor);
        // cursor.close();
        // return newEntertainment;
    }

    public void deleteEntertainment() {
        database.delete(Pay1SQLiteHelper.TABLE_ENTERTAINMENT, null, null);
    }

    public void deleteEntertainment(Entertainment entertainment) {
        long id = entertainment.getEntertainmentID();
        database.delete(Pay1SQLiteHelper.TABLE_ENTERTAINMENT,
                Pay1SQLiteHelper.ENTERTAINMENT_ID + " = " + id, null);
    }

    public List<Entertainment> getAllEntertainment() {
        List<Entertainment> entertainments = new ArrayList<Entertainment>();

        Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_ENTERTAINMENT,
                null, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Entertainment entertainment = cursorToEntertainment(cursor);
            entertainments.add(entertainment);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return entertainments;
    }

    private Entertainment cursorToEntertainment(Cursor cursor) {
        Entertainment entertainment = new Entertainment();
        entertainment.setEntertainmentID(cursor.getInt(0));
        entertainment.setEntertainmentProductID(cursor.getString(1));
        entertainment.setEntertainmentProductCode(cursor.getString(2));
        entertainment.setEntertainmentProductName(cursor.getString(3));
        entertainment.setEntertainmentProductPrice(cursor.getString(4));
        entertainment.setEntertainmentProductValidity(cursor.getString(5));
        entertainment.setEntertainmentProductShortDescription(cursor
                .getString(6));
        entertainment.setEntertainmentProductLongDescription(cursor
                .getString(7));
        entertainment.setEntertainmentProductImage(cursor.getString(8));
        entertainment.setEntertainmentProductDataField(cursor.getString(9));
        entertainment.setEntertainmentProductDataType(cursor.getString(10));
        entertainment.setEntertainmentProductDataLength(cursor.getString(11));
        entertainment.setEntertainmentProductUpdateTime(cursor.getString(12));

        return entertainment;
    }
}
