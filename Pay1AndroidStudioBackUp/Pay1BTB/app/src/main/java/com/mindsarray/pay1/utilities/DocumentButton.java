package com.mindsarray.pay1.utilities;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;

import com.mindsarray.pay1.R;

/**
 * Created by rohan on 17/11/15.
 */
public class DocumentButton extends ImageButton {

    private static final int[] STATE_PRESENT = {R.attr.state_present};
    private static final int[] STATE_VERIFIED = {R.attr.state_verified};

    private boolean mIsPresent = false;
    private boolean mIsVerified = false;

    public DocumentButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setPresent(boolean isPresent) {
        mIsPresent = isPresent;
        refreshDrawableState();
    }


    public boolean getPresent() {
        return mIsPresent;
    }

    public void setVerified(boolean isVerified) {
        mIsVerified = isVerified;
        refreshDrawableState();
    }

    @Override
    public int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 2);
        if (mIsPresent) {
            mergeDrawableStates(drawableState, STATE_PRESENT);
        }
        if (mIsVerified) {
            mergeDrawableStates(drawableState, STATE_VERIFIED);
        }
        return drawableState;
    }
}
