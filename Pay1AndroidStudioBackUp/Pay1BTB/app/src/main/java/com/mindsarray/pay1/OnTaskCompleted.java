package com.mindsarray.pay1;

import android.widget.ImageView;

/**
 * Created by Administrator on 6/14/2016.
 */
public interface OnTaskCompleted {
    void onTaskCompleted(ImageView bitmap);
}