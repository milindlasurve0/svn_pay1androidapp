package com.mindsarray.pay1.adapterhandler;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindsarray.pay1.R;
import com.mindsarray.pay1.utilities.GridMenuItem;

import java.util.ArrayList;

public class QuickRechargeAdapter extends ArrayAdapter<GridMenuItem> {
    Context context;
    int layoutResourceId;
    ArrayList<GridMenuItem> data = new ArrayList<GridMenuItem>();

    public QuickRechargeAdapter(Context context, int layoutResourceId,
                                ArrayList<GridMenuItem> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        RecordHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new RecordHolder();
            holder.txtTitle = (TextView) row.findViewById(R.id.item_text);
            holder.imageItem = (ImageView) row.findViewById(R.id.item_image);
            row.setTag(holder);
        } else {
            holder = (RecordHolder) row.getTag();
        }

        GridMenuItem item = data.get(position);
        holder.txtTitle.setText(item.getTitle());
        holder.imageItem.setImageResource(item.getImage());
        return row;

    }

    static class RecordHolder {
        TextView txtTitle;
        ImageView imageItem;

    }
}