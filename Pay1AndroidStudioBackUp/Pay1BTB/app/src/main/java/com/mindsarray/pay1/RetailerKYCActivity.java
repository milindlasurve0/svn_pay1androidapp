package com.mindsarray.pay1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.mindsarray.pay1.adapterhandler.RetailerKYCPagerAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.contenthandler.Document;
import com.mindsarray.pay1.contenthandler.Retailer;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.DocumentButton;
import com.mindsarray.pay1.utilities.GooglePlayServicesApiClient;
import com.mindsarray.pay1.utilities.Utility;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class RetailerKYCActivity extends FragmentActivity
        implements RetailerKYCStepOneFragment.OnFragmentInteractionListener,
        RetailerKYCStepTwoFragment.OnFragmentInteractionListener {

    private GoogleApiClient mGoogleApiClient;
    RetailerKYCPagerAdapter mRetailerKYCPagerAdapter;
    ViewPager mViewPager;
    public Context mContext = RetailerKYCActivity.this;
    public GooglePlayServicesApiClient mGooglePlayServicesApiClient;

    private EditText mNameView;
    private EditText mMobileView;
    private EditText mShopView;
    private Spinner mShopTypeView;
    private Spinner mLocationTypeView;
    private Spinner mShopStructureView;
    private EditText mAddressView;
    private EditText mAreaView;
    private EditText mCityView;
    private EditText mStateView;
    private EditText mPinCodeView;
    private EditText mOtherEditText;
    private TextView mSelectedNatureOfBusiness;
    private TextView mSelectedAreaOfBusiness;
    private RadioGroup mGroupNatureOfBusinessView;
    private RadioGroup mGroupAreaOfBusinessView;
    private RadioGroup mGroupRentalTypeView;
    private RadioButton mNatureOfBusinessView;
    private RadioButton mAreaOfBusinessView;
    private RadioButton mRentalTypeView;

    public ArrayList<String> addressProof = new ArrayList<String>();
    public ArrayList<String> idProof = new ArrayList<String>();
    public ArrayList<String> shopPhotos = new ArrayList<String>();
    public ArrayList<String> mImageURIRemovalList = new ArrayList<String>();

    private String retailer_id = null;
    Retailer retailer;

    private RetailerKYCStepOneFragment mRetailerKYCStepOneFragment;
    private RetailerKYCStepTwoFragment mRetailerKYCStepTwoFragment;

    ProgressDialog dialog;
    Boolean upload = false;

    private Boolean new_trial = false;

    String natureOfBusiness = "", areaOfBusiness = "";
    String kyc_action = "";

    DocumentButton mUploadIDProofdocumentButton, mUploadAddressProofdocumentButton, mUploadShopPhotosdocumentButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.retailer_kyc);

        retailer_id = Utility.getSharedPreferences(mContext, Constants.SHAREDPREFERENCE_RETAILER_ID);
        retailer = new Retailer(new JSONObject());

        Constants.promptForGPS((Activity) mContext);
        mGooglePlayServicesApiClient = new GooglePlayServicesApiClient(mContext, LocationServices.API, false);
        mGoogleApiClient = mGooglePlayServicesApiClient.mGoogleApiClient;
        Log.e("Google API Client: ", String.valueOf(mGoogleApiClient));

        mRetailerKYCPagerAdapter =
                new RetailerKYCPagerAdapter(
                        getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mRetailerKYCPagerAdapter);

        new GetRetailer().execute();
    }

    @Override
    public void onResume() {
        Constants.showNavigationBar(mContext);

        super.onResume();
        Constants.promptForGPS((Activity) mContext);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("Activity: ", "Inside");
    }

    public void onFragmentInteraction(Uri Uri) {

    }

    public Fragment findFragmentByPosition(int position) {
        return getSupportFragmentManager().findFragmentByTag(
                "android:switcher:" + mViewPager.getId() + ":"
                        + mRetailerKYCPagerAdapter.getItemId(position));
    }

    public void editRetailer() {
        mNameView = (EditText) findViewById(R.id.retailer_name);
        mMobileView = (EditText) findViewById(R.id.retailer_mobile);
        mShopView = (EditText) findViewById(R.id.retailer_shop_name);
//        mRentalTypeView = (Spinner) findViewById(R.id.rental_type_spinner);
        mAddressView = (EditText) findViewById(R.id.retailer_address);
        mAreaView = (EditText) findViewById(R.id.retailer_area);
        mCityView = (EditText) findViewById(R.id.retailer_city);
        mStateView = (EditText) findViewById(R.id.retailer_state);
        mPinCodeView = (EditText) findViewById(R.id.retailer_pin_code);

        mGroupRentalTypeView = (RadioGroup) findViewById(R.id.radio_group_rental_type);
        if (mGroupRentalTypeView.getCheckedRadioButtonId() != -1)
            mRentalTypeView = (RadioButton) mGroupRentalTypeView.findViewById(mGroupRentalTypeView.getCheckedRadioButtonId());

        RelativeLayout natureOfBusinessButtons = (RelativeLayout) findViewById(R.id.natureOfBusinessButtons);
        mOtherEditText = (EditText) natureOfBusinessButtons.findViewById(R.id.other_text);
        for (int i = 0; i < natureOfBusinessButtons.getChildCount() - 1; i++) {
            Button child = (Button) natureOfBusinessButtons.getChildAt(i);
            if (child.isSelected()) {
                natureOfBusiness = child.getText().toString();
                if (i == natureOfBusinessButtons.getChildCount() - 2) {
                    natureOfBusiness = mOtherEditText.getText().toString();
                }
            }
        }

        RelativeLayout areaOfBusinessButtons = (RelativeLayout) findViewById(R.id.areaOfBusinessButtons);
        for (int i = 0; i < areaOfBusinessButtons.getChildCount(); i++) {
            Button child = (Button) areaOfBusinessButtons.getChildAt(i);
            if (child.isSelected()) {
                areaOfBusiness = child.getText().toString();
            }
        }

        if (validateForm()) {
            mRetailerKYCStepOneFragment = (RetailerKYCStepOneFragment) findFragmentByPosition(0);
            mRetailerKYCStepTwoFragment = (RetailerKYCStepTwoFragment) findFragmentByPosition(1);

            final Runnable executeBlock = new Runnable() {
                @Override
                public void run() {
                    new UpdateRetailer().execute();
                }
            };

            if (!mUploadIDProofdocumentButton.getPresent() || !mUploadAddressProofdocumentButton.getPresent() || !mUploadShopPhotosdocumentButton.getPresent()) {
                Constants.showTwoButtonDialogForUpload(mContext, "Upload KYC documents and enjoy Toll-Free service.", "PAY1", "NOW", "LATER", null, executeBlock);
            } else
                executeBlock.run();
        }
    }

    private boolean validateForm() {
        String DIALOG_TITLE_INVALIDATE = "Information improper";
        mRetailerKYCStepOneFragment = (RetailerKYCStepOneFragment) findFragmentByPosition(0);
        mRetailerKYCStepTwoFragment = (RetailerKYCStepTwoFragment) findFragmentByPosition(1);

        if (mNameView.getText().toString().length() < 2) {
            Constants.showOneButtonDialog(mContext, DIALOG_TITLE_INVALIDATE, "Enter a proper name", "OK", 0);
            mNameView.requestFocus();
            return false;
        } else if (!Constants.isMobileValid(mMobileView.getText().toString())) {
            Constants.showOneButtonDialog(mContext, DIALOG_TITLE_INVALIDATE, "Enter a proper mobile number", "OK", 0);
            mMobileView.requestFocus();
            return false;
        } else if (mShopView.getText().toString().length() < 2) {
            Constants.showOneButtonDialog(mContext, DIALOG_TITLE_INVALIDATE, "Enter a proper shop name", "OK", 0);
            mShopView.requestFocus();
            return false;
        } else if (mAddressView.getText().toString().length() < 2) {
            Constants.showOneButtonDialog(mContext, DIALOG_TITLE_INVALIDATE, "Enter a proper address", "OK", 0);
            mAddressView.requestFocus();
            return false;
        } else if (mAreaView.getText().toString().length() < 2) {
            Constants.showOneButtonDialog(mContext, DIALOG_TITLE_INVALIDATE, "Enter a proper Area", "OK", 0);
            mAreaView.requestFocus();
            return false;
        } else if (mCityView.getText().toString().length() < 2) {
            Constants.showOneButtonDialog(mContext, DIALOG_TITLE_INVALIDATE, "Enter a proper City", "OK", 0);
            mCityView.requestFocus();
            return false;
        } else if (mStateView.getText().toString().length() < 2) {
            Constants.showOneButtonDialog(mContext, DIALOG_TITLE_INVALIDATE, "Enter a proper State", "OK", 0);
            mStateView.requestFocus();
            return false;
        } else if (mPinCodeView.getText().toString().length() != 6) {
            Constants.showOneButtonDialog(mContext, DIALOG_TITLE_INVALIDATE, "Pin Code should be 6 digits long", "OK", 0);
            mPinCodeView.requestFocus();
            return false;
        } else if (natureOfBusiness.equals("")) {
            Constants.showOneButtonDialog(mContext, DIALOG_TITLE_INVALIDATE, "Select the nature of your business", "OK", 0);
            return false;
        } else if (areaOfBusiness.equals("")) {
            Constants.showOneButtonDialog(mContext, DIALOG_TITLE_INVALIDATE, "Select the area of your business", "OK", 0);
            return false;
        } else if (Constants.SHAREDPREFERENCE_MOBILE_NUMBER.equals("")) {
            startActivity(new Intent(mContext,
                    LoginActivity.class));
            return false;
        } else if (mRetailerKYCStepOneFragment.latitude == null || mRetailerKYCStepOneFragment.longitude == null || mRetailerKYCStepOneFragment.latitude.equals("null") || mRetailerKYCStepOneFragment.longitude.equals("null") || mRetailerKYCStepOneFragment.latitude.equals("0") || mRetailerKYCStepOneFragment.longitude.equals("0") || mRetailerKYCStepOneFragment.latitude.equals("") || mRetailerKYCStepOneFragment.longitude.equals("")) {
            Constants.showOneButtonDialog(mContext, DIALOG_TITLE_INVALIDATE, "Select shop location on map", "OK", 0);
            return false;
        }

        return true;
    }

    private Map editRetailerParams() {
        final Map editRetailerParams = new HashMap();

        editRetailerParams.put("method", "editRetailer");
        editRetailerParams.put("r_id", retailer_id);
        editRetailerParams.put("interest", "Retailer");
        editRetailerParams.put("r_m", mMobileView.getText().toString());
        editRetailerParams.put("r_n", mNameView.getText().toString());
        editRetailerParams.put("s_n", mShopView.getText().toString());
//        editRetailerParams.put("d_uid", .getPersistentString(mContext, Storage.SHAREDPREFERENCE_USER_ID));
        editRetailerParams.put("s_t", natureOfBusiness + "");
        editRetailerParams.put("l_t", areaOfBusiness + "");
//        if(mRentalTypeView.getText().toString().equals("Free"))
//        	editRetailerParams.put("r_t", "1");
//        else
//        	editRetailerParams.put("r_t", "0");
        editRetailerParams.put("r_add", mAddressView.getText().toString());
        editRetailerParams.put("r_a", mAreaView.getText().toString());
        editRetailerParams.put("r_c", mCityView.getText().toString());
        editRetailerParams.put("r_s", mStateView.getText().toString());
        editRetailerParams.put("r_pc", mPinCodeView.getText().toString());
        editRetailerParams.put("r_la", "" + mRetailerKYCStepOneFragment.latitude);
        editRetailerParams.put("r_lo", "" + mRetailerKYCStepOneFragment.longitude);

        addressProof.addAll(mRetailerKYCStepTwoFragment.getAddressProofURIs());
        idProof.addAll(mRetailerKYCStepTwoFragment.getIdProofURIs());
        shopPhotos.addAll(mRetailerKYCStepTwoFragment.getShopPhotosURIs());
        mImageURIRemovalList.addAll(mRetailerKYCStepTwoFragment.getPhotoRemovalURIs());

        return editRetailerParams;
    }

    public void uploadRetailerDocuments() {
        final Map uploadDocumentsParams = new HashMap();

        uploadDocumentsParams.put("method", "uploadKYCDocuments");
        uploadDocumentsParams.put("r_id", Utility.getSharedPreferences(mContext, Constants.SHAREDPREFERENCE_RETAILER_ID));
        uploadDocumentsParams.put("ADDRESS_PROOF[]", addressProof);
        uploadDocumentsParams.put("PAN_CARD[]", idProof);
        uploadDocumentsParams.put("SHOP_PHOTO[]", shopPhotos);
        uploadDocumentsParams.put("remove[]", mImageURIRemovalList);

        if (!addressProof.isEmpty() || !idProof.isEmpty() || !shopPhotos.isEmpty() || !mImageURIRemovalList.isEmpty()) {
            RequestClass.getInstance().executeMultiPartRequest(mContext, uploadDocumentsParams);
            if (!addressProof.isEmpty() || !idProof.isEmpty() || !shopPhotos.isEmpty()) {
                upload = true;
            } else
                upload = false;
        } else
            upload = false;
    }

    public void getRetailer() {
        final Map getRetailerParams = new HashMap();

        getRetailerParams.put("method", "getRetailer");
        getRetailerParams.put("r_id", retailer_id);

        String response = RequestClass.getInstance().executeMultiPartRequest(mContext, getRetailerParams);
        Log.e("Multipart Response: ", " " + response);

        try {
            JSONObject jsonObjectResponse = new JSONObject(response);
            String status = jsonObjectResponse.getString("status");

            Log.e("Status: ", status);
            if (status.equalsIgnoreCase("success")) {
                JSONObject jsonRetailer = jsonObjectResponse.getJSONObject("description");

                retailer = new Retailer(jsonRetailer);

                Utility.setLatitude(mContext, Constants.SHAREDPREFERENCE_LATITUDE, retailer.latitude);
                Utility.setLongitude(mContext, Constants.SHAREDPREFERENCE_LONGITUDE, retailer.longitude);
            } else {
                Constants.showOneButtonFailureDialog(
                        mContext,
                        Constants.checkCode("[" + response + "]"), "Retailer KYC",
                        Constants.OTHER_ERROR);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void showRetailerDetails() {
        if (retailer.retailer_id != null) {
            mNameView = (EditText) findViewById(R.id.retailer_name);
            mMobileView = (EditText) findViewById(R.id.retailer_mobile);
            mShopView = (EditText) findViewById(R.id.retailer_shop_name);
//          mRentalTypeView = (Spinner) findViewById(R.id.rental_type_spinner);
            mAddressView = (EditText) findViewById(R.id.retailer_address);
            mAreaView = (EditText) findViewById(R.id.retailer_area);
            mCityView = (EditText) findViewById(R.id.retailer_city);
            mStateView = (EditText) findViewById(R.id.retailer_state);
            mPinCodeView = (EditText) findViewById(R.id.retailer_pin_code);

            mMobileView.setEnabled(false);
            mMobileView.setFocusable(false);

            String[] shopTypeArray = getResources().getStringArray(R.array.shop_type_array);
            String[] locationTypeArray = getResources().getStringArray(R.array.location_type_array);

            RelativeLayout natureOfBusinessButtons = (RelativeLayout) findViewById(R.id.natureOfBusinessButtons);
            RelativeLayout areaOfBusinessButtons = (RelativeLayout) findViewById(R.id.areaOfBusinessButtons);

            mOtherEditText = (EditText) natureOfBusinessButtons.findViewById(R.id.other_text);

            mNameView.setText(retailer.name);
            mMobileView.setText(retailer.mobile);
            mShopView.setText(retailer.shop_name);

            mAddressView.setText(retailer.address);
            mAreaView.setText(retailer.area);
            mCityView.setText(retailer.city);
            mStateView.setText(retailer.state);
            mPinCodeView.setText(retailer.pin_code);

            if (!retailer.area.equals("")) {
                mAreaView.setEnabled(false);
            }
            if (!retailer.city.equals("")) {
                mCityView.setEnabled(false);
            }
            if (!retailer.state.equals("")) {
                mStateView.setEnabled(false);
            }
            if (!retailer.pin_code.equals("")) {
                mPinCodeView.setEnabled(false);
            }

            if (Arrays.asList(shopTypeArray).indexOf(retailer.shop_type) != -1) {
                (natureOfBusinessButtons.getChildAt(Arrays.asList(shopTypeArray).indexOf(retailer.shop_type))).setSelected(true);
                if (retailer.shop_type.equals("Others")) {
                    mOtherEditText.setVisibility(View.VISIBLE);
                }
            } else {
                if (!retailer.shop_type.equals("0"))
                    mOtherEditText.setText(retailer.shop_type + "");
                mOtherEditText.setVisibility(View.VISIBLE);
                if (!retailer.shop_type.equals("") && !retailer.shop_type.equals("0")) {
                    (natureOfBusinessButtons.getChildAt(natureOfBusinessButtons.getChildCount() - 2)).setSelected(true);
                }
            }

            if (Arrays.asList(locationTypeArray).indexOf(retailer.location_type) != -1) {
                (areaOfBusinessButtons.getChildAt(Arrays.asList(locationTypeArray).indexOf(retailer.location_type))).setSelected(true);
            }

//            mGroupRentalTypeView = (RadioGroup) findViewById(R.id.radio_group_rental_type);
//            if(!retailer.rental_flag.equals("0"))
//                ((RadioButton) mGroupRentalTypeView.getChildAt(0)).setChecked(true);
//            else
//                ((RadioButton) mGroupRentalTypeView.getChildAt(1)).setChecked(true);

            mRetailerKYCStepOneFragment = (RetailerKYCStepOneFragment) findFragmentByPosition(0);
            mRetailerKYCStepTwoFragment = (RetailerKYCStepTwoFragment) findFragmentByPosition(1);

            mRetailerKYCStepTwoFragment.loadImages();

            mRetailerKYCStepOneFragment.setLatLng();

            Map documents = retailer.documents;
            mUploadIDProofdocumentButton = (DocumentButton) findViewById(R.id.uploadIDProofDialogButton);
            mUploadAddressProofdocumentButton = (DocumentButton) findViewById(R.id.uploadAddressProofDialogButton);
            mUploadShopPhotosdocumentButton = (DocumentButton) findViewById(R.id.uploadShopPhotosDialogButton);

            if (((ArrayList<Document>) documents.get("idProof")).size() > 0) {
                mUploadIDProofdocumentButton.setPresent(true);
                if (((ArrayList<Document>) documents.get("idProof")).get(0).verify_flag.equals("1")) {
                    mUploadIDProofdocumentButton.setVerified(true);
                } else if (((ArrayList<Document>) documents.get("idProof")).get(0).verify_flag.equals("-1")) {
                    mUploadIDProofdocumentButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_reject_pan));

                }
            }
            if (((ArrayList<Document>) documents.get("addressProof")).size() > 0) {
                mUploadAddressProofdocumentButton.setPresent(true);
                if (((ArrayList<Document>) documents.get("addressProof")).get(0).verify_flag.equals("1")) {
                    mUploadAddressProofdocumentButton.setVerified(true);
                } else if (((ArrayList<Document>) documents.get("addressProof")).get(0).verify_flag.equals("-1")) {
                    mUploadAddressProofdocumentButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_reject_addproof));

                }
            }
            if (((ArrayList<Document>) documents.get("shopPhotos")).size() > 0) {
                mUploadShopPhotosdocumentButton.setPresent(true);
                if (((ArrayList<Document>) documents.get("shopPhotos")).get(0).verify_flag.equals("1")) {
                    mUploadShopPhotosdocumentButton.setVerified(true);
                } else if (((ArrayList<Document>) documents.get("shopPhotos")).get(0).verify_flag.equals("-1")) {
                    mUploadShopPhotosdocumentButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_reject_shoppic));

                }
            }
        }
    }

    class GetRetailer extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Please wait.....");
            dialog.setCancelable(true);
            dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            GetRetailer.this.cancel(true);
                        }
                    });
            dialog.show();
            super.onPreExecute();
        }

        public void onDismiss(DialogInterface dialog) {
            GetRetailer.this.cancel(true);
            dialog.cancel();
        }

        protected String doInBackground(String... urls) {
            try {
                getRetailer();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            return "";
        }

        protected void onPostExecute(String response) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            showRetailerDetails();
        }
    }

    class UpdateRetailer extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Please wait.....");
            dialog.setCancelable(true);
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

                @Override
                public void onCancel(DialogInterface dialog) {
                    UpdateRetailer.this.cancel(true);
                }
            });
            dialog.show();
            super.onPreExecute();
        }

        public void onDismiss(DialogInterface dialog) {
            UpdateRetailer.this.cancel(true);
            dialog.cancel();
        }

        protected String doInBackground(String... urls) {
            String response = RequestClass.getInstance().executeMultiPartRequest(mContext, editRetailerParams());

            try {
                JSONObject jsonObjectResponse = new JSONObject(response);
                String status = jsonObjectResponse.getString("status");

                Log.e("Status: ", status);
                if (status.equalsIgnoreCase("success")) {
                    JSONObject description = jsonObjectResponse.getJSONObject("description");
                    JSONObject jsonRetailer = description.getJSONObject("retailer");

                    retailer = new Retailer(jsonRetailer);

                    /*if(!addressProof.isEmpty() || !idProof.isEmpty() || !shopPhotos.isEmpty() || !mImageURIRemovalList.isEmpty()) {
                        String message = "Thank you!";
                        String message2 = "This account is pending verification.";
                        Runnable confirmBlock = new Runnable() {
                            @Override
                            public void run() {
                                uploadRetailerDocuments();
                            }
                        };

                        Constants.showOneButtonBigDialog(mContext, message, message2, "SUCCESS", "OK", R.drawable.ic_success, confirmBlock);
                    }
                    else {
                        uploadRetailerDocuments();
                    }*/
                    uploadRetailerDocuments();
                } else {
                    Constants.showOneButtonFailureDialog(
                            mContext,
                            Constants.checkCode("[" + response + "]"), "Retailer KYC",
                            Constants.OTHER_ERROR);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        protected void onPostExecute(String response) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            if (upload)
                Toast.makeText(mContext, "Uploading documents",
                        Toast.LENGTH_LONG).show();
            else {
                Toast.makeText(mContext, "Retailer details updated",
                        Toast.LENGTH_LONG).show();
            }
            Intent dashboardIntent = new Intent(mContext,
                    MainActivity.class);
            startActivity(dashboardIntent);
        }
    }
}