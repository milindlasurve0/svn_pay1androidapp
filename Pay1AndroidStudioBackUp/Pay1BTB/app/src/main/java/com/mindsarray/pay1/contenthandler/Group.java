package com.mindsarray.pay1.contenthandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 6/16/2016.
 */
public class Group {

    public String string;
    public final List<String> children = new ArrayList<String>();

    public Group(String string) {
        this.string = string;
    }

}
