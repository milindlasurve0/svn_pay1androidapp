package com.mindsarray.pay1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.EditTextValidator;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SettingsUpdatePinActivity extends Activity {
    EditText editExistPin, editNewPin, editConfirmPin;
    Button btnUpdatePin;
    String newPin, confirmPin, oldPin;
    private static final String TAG = "Update pin";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        // WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.settingsupdatepin_activity);
        findViewById();
        tracker = Constants.setAnalyticsTracker(this, tracker);
        btnUpdatePin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                newPin = editNewPin.getText().toString().trim();
                confirmPin = editConfirmPin.getText().toString().trim();
                oldPin = editExistPin.getText().toString().trim();

                if (!checkValidation(SettingsUpdatePinActivity.this)) {
                    // Constants.showCustomToast(LoginActivity.this,
                    // "Please Enter a valid Number");
                } else {
                    // String string =
                    // Constants.encryptPassword(strPin);
                    if (!newPin.equalsIgnoreCase(oldPin)) {
                        if (newPin.equalsIgnoreCase(confirmPin)) {
                            new UpdatPinTask().execute();
                        } else {
                            Constants.showOneButtonFailureDialog(
                                    SettingsUpdatePinActivity.this,
                                    "Pin does not match.", "Wrong Pin", 8);
                        }
                    } else {
                        /*
						 * Constants .showCustomToast(
						 * SettingsUpdatePinActivity.this, getString(R.string.
						 * old_and_new_pin_both_are_same_please_choose_new_pin
						 * ));
						 */
                        Constants.showOneButtonFailureDialog(
                                SettingsUpdatePinActivity.this,
                                "Old and New Pins are same.", "Wrong Pin", 8);
                    }

					/*
					 * if (newPin.length() != 4 && confirmPin.length() != 4) {
					 * Constants.showCustomToast(
					 * SettingsUpdatePinActivity.this,
					 * R.string.pin_should_be_4_digit_ + ""); } else { if
					 * (newPin.equalsIgnoreCase(confirmPin)) { if
					 * (Utility.getLoginFlag(context,
					 * Constants.SHAREDPREFERENCE_IS_LOGIN)) { new
					 * UpdatPinTask().execute(); } else {
					 * Constants.showCustomToast(
					 * SettingsUpdatePinActivity.this,
					 * R.string.please_login_first + ""); } } else {
					 * Constants.showCustomToast(
					 * SettingsUpdatePinActivity.this,
					 * R.string.enter_correct_pin_ + ""); } }
					 */
                }
            }

        });
    }

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        //tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
    }

    private boolean checkValidation(Context context) {
        boolean ret = true;

        if (!EditTextValidator.hasText(context, editExistPin,
                Constants.ERROR_PIN_BLANK_FIELD)) {
            ret = false;
            editExistPin.requestFocus();
            return ret;
        } else if (!EditTextValidator.hasText(context, editNewPin,
                Constants.ERROR_PIN_BLANK_FIELD)) {
            ret = false;
            editNewPin.requestFocus();
            return ret;
        } else if (!EditTextValidator.hasText(context, editConfirmPin,
                Constants.ERROR_PIN_BLANK_FIELD)) {
            ret = false;
            editConfirmPin.requestFocus();
            return ret;
        } else if (!EditTextValidator.isValidPin(context, editExistPin,
                Constants.ERROR_LOGIN_PIN_VALID_FIELD)) {
            ret = false;
            editExistPin.requestFocus();
            return ret;
        } else if (!EditTextValidator.isValidPin(context, editNewPin,
                Constants.ERROR_LOGIN_PIN_VALID_FIELD)) {
            ret = false;
            editNewPin.requestFocus();
            return ret;
        } else if (!EditTextValidator.isValidPin(context, editConfirmPin,
                Constants.ERROR_LOGIN_PIN_VALID_FIELD)) {
            ret = false;
            editConfirmPin.requestFocus();
            return ret;
        } else
            return ret;
    }

    public class UpdatPinTask extends AsyncTask<String, String, String>
            implements OnDismissListener {

        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method", "updatePin"));
            listValuePair.add(new BasicNameValuePair("newPin", newPin));
            listValuePair.add(new BasicNameValuePair("oldPin", oldPin));
            listValuePair.add(new BasicNameValuePair("device_type", "android"));
            String response = RequestClass.getInstance().readPay1Request(
                    SettingsUpdatePinActivity.this, Constants.API_URL,
                    listValuePair);
            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        editConfirmPin.setText("");
                        editExistPin.setText("");
                        editNewPin.setText("");
                        Constants.showOneButtonSuccessDialog(
                                SettingsUpdatePinActivity.this,
                                "Pin Changed Succesfully. Please Login Again.",
                                Constants.CHANGE_PIN_SETTINGS);
                        // finish();
                    } else {
                        Constants.showOneButtonFailureDialog(
                                SettingsUpdatePinActivity.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(
                            SettingsUpdatePinActivity.this,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(
                        SettingsUpdatePinActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(
                        SettingsUpdatePinActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            dialog = new ProgressDialog(SettingsUpdatePinActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            UpdatPinTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            UpdatPinTask.this.cancel(true);
            dialog.cancel();
        }

    }

    private void findViewById() {
        // TODO Auto-generated method stub
        editConfirmPin = (EditText) findViewById(R.id.editConfirmPin);
        editExistPin = (EditText) findViewById(R.id.editExistPin);
        editNewPin = (EditText) findViewById(R.id.editNewPin);
        btnUpdatePin = (Button) findViewById(R.id.btnUpdatePin);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Constants.showNavigationBar(SettingsUpdatePinActivity.this);
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (Constants.isExpanded) {
            Constants.collapsView();
        } else {
            super.onBackPressed();
        }
    }

}
