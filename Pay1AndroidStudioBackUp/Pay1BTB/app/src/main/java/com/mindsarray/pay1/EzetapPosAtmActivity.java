package com.mindsarray.pay1;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eze.api.EzeAPI;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.EnglishNumberToWords;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class EzetapPosAtmActivity extends Activity implements OnClickListener {
    private EditText txtRefNum;
    private EditText txtAmount;
    private EditText txtAmountCashback;
    private EditText txtName;
    private EditText txtMobile;
    private EditText txtEmail;

    private Button btnInit;
    private Button btnPrep;
    private Button btnCardSale;
    private Button btnCardCashback;
    private Button btnCardCashAtPOS;
    private Button btnCash;
    private Button btnSearchTxn;
    private Button btnVoidTxn;
    private Button btnAttachSign;
    private Button btnClose;

    private String strTxnId = null;
    private String mandatoryErrMsg = "Please fill up mandatory params.";
    private final int REQUESTCODE_INIT = 10001;
    private final int REQUESTCODE_PREP = 10002;
    private final int REQUESTCODE_CLOSE = 10003;
    private final int REQUESTCODE_VOID = 10004;
    private final int REQUESTCODE_SEARCH = 10005;
    private final int REQUESTCODE_CASH = 10006;
    private final int REQUESTCODE_SALE = 10007;
    private final int REQUESTCODE_CASHBACK = 10008;
    private final int REQUESTCODE_CASHATPOS = 10009;
    private final int REQUESTCODE_ATTACHSIGN = 10010;
    private static String TAG = "Mini ATM";

    // private String pay1EzetapProductId = "";
    private String shop_transaction_id = "";
    private String SaleOrCash = "", title = "";
    public static GoogleAnalytics analytics;
    public static Tracker tracker;
    Button btnProceed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nativesample);

        SaleOrCash = getIntent().getExtras().getString("SaleOrCash");
        title = getIntent().getExtras().getString("title");
        btnProceed = (Button) findViewById(R.id.btnProceed);
        ImageView imageView_prepare = (ImageView) findViewById(R.id.imageView_prepare);
        TextView textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        textViewTitle.setText(title);
        TAG = title;
        imageView_prepare.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                initEzetap();
            }
        });

        LinearLayout header_view = (LinearLayout) findViewById(R.id.header_view);
        header_view.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });

        btnProceed.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                String amountStr = txtAmount.getText().toString();

                if (!amountStr.isEmpty()) {
                    int amountInt = Integer.parseInt(amountStr);
                    if (SaleOrCash.equalsIgnoreCase("57")) {
                        // doCashTxn();
                        // doCashAtPosTxn();

                        if (amountInt % 100 == 0 && amountInt > 0) {
                            showTransactionDialog();
                        } else {
                            Constants
                                    .showOneButtonDialog(
                                            EzetapPosAtmActivity.this,
                                            "Mini ATM",
                                            "Invalid Amount.\nAmount should be a multiple of 100.0",
                                            "OK", 1);
                        }
                    } else if (SaleOrCash.equalsIgnoreCase("56")) {
                        // doSaleTxn();
                        showTransactionDialog();
                    } else {

                    }
                } else {
                    Constants
                            .showOneButtonDialog(
                                    EzetapPosAtmActivity.this,
                                    "Mini ATM",
                                    "Please enter amount.",
                                    "OK", 1);
                }
            }
        });

        btnInit = ((Button) findViewById(R.id.btnInitialize));
        btnInit.setOnClickListener(this);

        btnPrep = ((Button) findViewById(R.id.btnPrepare));
        btnPrep.setOnClickListener(this);

        btnCardSale = ((Button) findViewById(R.id.btnSale));
        btnCardSale.setOnClickListener(this);

        btnCardCashback = ((Button) findViewById(R.id.btnCashback));
        btnCardCashback.setOnClickListener(this);

        btnCardCashAtPOS = ((Button) findViewById(R.id.btnCashAtPOS));
        btnCardCashAtPOS.setOnClickListener(this);

        btnCash = ((Button) findViewById(R.id.btnCashTxn));
        btnCash.setOnClickListener(this);

        // btnSearchTxn = ((Button) findViewById(R.id.btnTransaction));
        // btnSearchTxn.setOnClickListener(this);

        btnVoidTxn = ((Button) findViewById(R.id.btnVoidTxn));
        btnVoidTxn.setOnClickListener(this);

        btnAttachSign = ((Button) findViewById(R.id.btnAttachSignature));
        btnAttachSign.setOnClickListener(this);

        btnClose = ((Button) findViewById(R.id.btnClose));
        btnClose.setOnClickListener(this);

        txtRefNum = (EditText) findViewById(R.id.txtRefNum);
        txtAmount = (EditText) findViewById(R.id.txtAmount);
        txtAmountCashback = (EditText) findViewById(R.id.txtCashbackAmount);
        txtName = (EditText) findViewById(R.id.txtName);
        txtMobile = (EditText) findViewById(R.id.txtMobile);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        /*AnalyticsApplication application = (AnalyticsApplication) getApplication();
		tracker = application.getDefaultTracker();*/
        tracker = Constants.setAnalyticsTracker(this, tracker);
        initEzetap();
    }

    protected void showTransactionDialog() {
        // TODO Auto-generated method stub

        new AlertDialog.Builder(EzetapPosAtmActivity.this)
                .setTitle("Mini ATM")
                .setMessage(
                        "Mode: "
                                + title
                                + "\nAmount: "
                                + txtAmount.getText().toString()
                                + " ( "
                                + EnglishNumberToWords.convert(Long
                                .parseLong(txtAmount.getText()
                                        .toString())) + ") "
                                + "\nAre you sure you want to Proceed?")
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                                if (isMandatoryParamsValid()) {
                                    // pay1EzetapProductId = "57";

                                    new CreatemPosTransactionTask()
                                            .execute(SaleOrCash);
                                }

                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (intent != null && intent.hasExtra("response"))
			/*
			 * Toast.makeText(this, intent.getStringExtra("response"),
			 * Toast.LENGTH_LONG).show();
			 */
            switch (requestCode) {
                case REQUESTCODE_CASH:
                case REQUESTCODE_CASHBACK:
                case REQUESTCODE_CASHATPOS:
                case REQUESTCODE_SALE:
                    if (resultCode == RESULT_OK) {
                        if (SaleOrCash.equalsIgnoreCase("57")) {

                        } else if (SaleOrCash.equalsIgnoreCase("56")) {

                        } else {

                        }

                        try {
                            JSONObject response = new JSONObject(
                                    intent.getStringExtra("response"));
                            String status = response.getString("status");
                            String error = response.getString("error");

                            JSONObject responseResult = response
                                    .getJSONObject("result");
                            JSONObject txnResponse = responseResult
                                    .getJSONObject("txn");
                            strTxnId = txnResponse.getString("txnId");
                            new mPosTransactionResponseTask().execute(
                                    shop_transaction_id, response.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (resultCode == RESULT_CANCELED) {
                        try {
                            JSONObject response = new JSONObject(
                                    intent.getStringExtra("response"));
                            JSONObject error = response.getJSONObject("error");
                            // if (error.length() != 0) {
                            String message = error.getString("message");
                            // }

                            new mPosTransactionResponseTask().execute(
                                    shop_transaction_id, response.toString());
                            // response = response.getJSONObject("result");
                        } catch (Exception e) {
                            // e.printStackTrace();
                        }
                    }
                    break;
                case REQUESTCODE_INIT:
                    Log.d("test", "text");
                    if (resultCode == RESULT_OK) {
                        try {
                            JSONObject response = new JSONObject(
                                    intent.getStringExtra("response"));
                            JSONObject resultObject = response
                                    .getJSONObject("result");
                            String message = resultObject.getString("message");

                            String status = response.getJSONObject("status")
                                    .toString();
                            if (status.equals("success")) {
                                prepareEzetap();
                            }
                            // response = response.getJSONObject("txn");
                            // strTxnId = response.getString("txnId");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                default:
                    break;
            }

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        // Constants.showNavigationBar(EzetapPosAtmActivity.this);

        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        // tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
        /**
         * Sanjeev please do the same for all activities and fragment
         *
         *
         *
         */
        Constants.sendGoogleAnalytics(EzetapPosAtmActivity.this, "Mini ATM");
        Log.i(TAG, "Setting screen name: " + TAG);
        tracker.setScreenName("Image~" + TAG);
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAttachSignature:
                attachSignature();
                break;
            case R.id.btnCashAtPOS:
                // doCashAtPosTxn();
                if (isMandatoryParamsValid()) {
                    // pay1EzetapProductId = "57";
                    new CreatemPosTransactionTask().execute(SaleOrCash);
                }
                break;
            case R.id.btnCashback:
                // doCashbackTxn();
                break;
            case R.id.btnCashTxn:
                // doCashTxn();
			/*
			 * if (isMandatoryParamsValid()) { pay1EzetapProductId = "57"; new
			 * CreatemPosTransactionTask().execute(pay1EzetapProductId); }
			 */
                break;
            case R.id.btnClose:
                closeEzetap();
                break;
            case R.id.btnInitialize:
                initEzetap();
                break;
            case R.id.btnPrepare:
                // prepareEzetap();
                break;
            case R.id.btnSale:
                if (isMandatoryParamsValid()) {
                    // SaleOrCash = "56";
                    new CreatemPosTransactionTask().execute("56");
                }

                // doSaleTxn();
                break;
            case R.id.btnTransaction:
                // searchTxn();
                Intent intent = new Intent(EzetapPosAtmActivity.this,
                        EzeTapTransactionsHistoryActivity.class);
                startActivity(intent);
                break;
            case R.id.btnVoidTxn:
                voidTxn();
                break;
            default:
                break;
        }
    }

    private boolean isMandatoryParamsValid() {
        if (txtAmount.getText().toString().equalsIgnoreCase(""))
            return false;
        else
            return true;
    }

    private boolean isTransactionIdValid() {
        if (strTxnId == null)
            return false;
        else
            return true;
    }

    private void displayToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private void voidTxn() {
        if (isTransactionIdValid()) {
            EzeAPI.voidTransaction(this, REQUESTCODE_VOID, strTxnId);
        } else
            displayToast("Inorrect txn Id, please make a Txn.");
    }

    private void searchTxn() {
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("agentName", "Demo User");
            jsonRequest.put("startDate", "1/1/2016");
            jsonRequest.put("endDate", "12/31/2016");
            jsonRequest.put("txnType", "cash");
            jsonRequest.put("txnStatus", "void");
            EzeAPI.searchTransaction(this, REQUESTCODE_SEARCH, jsonRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void doSaleTxn() {
        if (isMandatoryParamsValid()) {
            JSONObject jsonRequest = new JSONObject();
            JSONObject jsonOptionalParams = new JSONObject();
            JSONObject jsonReferences = new JSONObject();
            JSONObject jsonCustomer = new JSONObject();
            try {
                // Building Customer Object
                jsonCustomer.put("name", txtName.getText().toString().trim());
                jsonCustomer.put("mobileNo", txtMobile.getText().toString()
                        .trim());
                jsonCustomer.put("email", txtEmail.getText().toString().trim());

                // Building References Object
                jsonReferences.put("reference1", shop_transaction_id);

                // Building Optional params Object
                jsonOptionalParams.put("amountCashback", 0.00);// Cannot have
                // amount
                // cashback in
                // SALE
                // transaction.
                jsonOptionalParams.put("amountTip", 0.00);
                jsonOptionalParams.put("references", jsonReferences);
                jsonOptionalParams.put("customer", jsonCustomer);

                // Building final request object
                jsonRequest
                        .put("amount", txtAmount.getText().toString().trim());
                jsonRequest.put("mode", "SALE");// This attributes determines
                // the type of transaction
                jsonRequest.put("options", jsonOptionalParams);

                EzeAPI.cardTransaction(this, REQUESTCODE_SALE, jsonRequest);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else
            displayToast(mandatoryErrMsg);
    }

    private void prepareEzetap() {
        EzeAPI.prepareDevice(this, REQUESTCODE_PREP);
    }

    private void initEzetap() {
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("demoAppKey",
                    "d9160852-3b64-4792-836f-6f4e526ac9bf");
            jsonRequest.put("prodAppKey",
                    "d9160852-3b64-4792-836f-6f4e526ac9bf");
            jsonRequest.put("merchantName", "Mindsarray Technologies Pvt Ltd");
            jsonRequest.put("userName", "Mindsarray");
            jsonRequest.put("currencyCode", "INR");
            jsonRequest.put("appMode", "PROD");
            jsonRequest.put("captureSignature", "true");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        EzeAPI.initialize(this, REQUESTCODE_INIT, jsonRequest);
    }

    private void closeEzetap() {
        EzeAPI.close(this, REQUESTCODE_CLOSE);
    }

    private void doCashTxn() {
        if (isMandatoryParamsValid()) {
            JSONObject jsonRequest = new JSONObject();
            JSONObject jsonOptionalParams = new JSONObject();
            JSONObject jsonReferences = new JSONObject();
            JSONObject jsonCustomer = new JSONObject();
            try {
                // Building Customer Object
                jsonCustomer.put("name", txtName.getText().toString().trim());
                jsonCustomer.put("mobileNo", txtMobile.getText().toString()
                        .trim());
                jsonCustomer.put("email", txtEmail.getText().toString().trim());

                // Building References Object
                jsonReferences.put("reference1", txtRefNum.getText().toString()
                        .trim());

                // Building Optional params Object
                jsonOptionalParams.put("amountCashback", 0.00);// Cannot have
                // amount
                // cashback in
                // cash
                // transaction.
                jsonOptionalParams.put("amountTip", 0.00);
                jsonOptionalParams.put("references", jsonReferences);
                jsonOptionalParams.put("customer", jsonCustomer);

                // Building final request object
                jsonRequest
                        .put("amount", txtAmount.getText().toString().trim());
                jsonRequest.put("options", jsonOptionalParams);

                EzeAPI.cashTransaction(this, REQUESTCODE_CASH, jsonRequest);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else
            displayToast(mandatoryErrMsg);
    }

    /*
     * private void doCashbackTxn() { if (isMandatoryParamsValid()) { JSONObject
     * jsonRequest = new JSONObject(); JSONObject jsonOptionalParams = new
     * JSONObject(); JSONObject jsonReferences = new JSONObject(); JSONObject
     * jsonCustomer = new JSONObject(); try { // Building Customer Object
     * jsonCustomer.put("name", txtName.getText().toString().trim());
     * jsonCustomer.put("mobileNo", txtMobile.getText().toString() .trim());
     * jsonCustomer.put("email", txtEmail.getText().toString().trim());
     *
     * // Building References Object jsonReferences.put("reference1",
     * txtRefNum.getText().toString() .trim());
     *
     * // Building Optional params Object
     * jsonOptionalParams.put("amountCashback", txtAmountCashback
     * .getText().toString().trim()); jsonOptionalParams.put("amountTip", 0.00);
     * jsonOptionalParams.put("references", jsonReferences);
     * jsonOptionalParams.put("customer", jsonCustomer);
     *
     * // Building final request object jsonRequest .put("amount",
     * txtAmount.getText().toString().trim()); jsonRequest.put("mode",
     * "CASHBACK");// This attributes // determines the type of // transaction
     * jsonRequest.put("options", jsonOptionalParams);
     *
     * EzeAPI.cardTransaction(this, REQUESTCODE_CASHBACK, jsonRequest); } catch
     * (JSONException e) { e.printStackTrace(); } } else
     * displayToast(mandatoryErrMsg); }
     */
    private void doCashAtPosTxn() {
        if (isMandatoryParamsValid()) {
            JSONObject jsonRequest = new JSONObject();
            JSONObject jsonOptionalParams = new JSONObject();
            JSONObject jsonReferences = new JSONObject();
            JSONObject jsonCustomer = new JSONObject();
            try {
                // Building Customer Object
                jsonCustomer.put("name", txtName.getText().toString().trim());
                jsonCustomer.put("mobileNo", txtMobile.getText().toString()
                        .trim());
                jsonCustomer.put("email", txtEmail.getText().toString().trim());

                // Building References Object
                jsonReferences.put("reference1", shop_transaction_id);

                // Building Optional params Object
                jsonOptionalParams.put("amountCashback", txtAmount.getText()
                        .toString().trim());
                jsonOptionalParams.put("amountTip", 0.00);
                jsonOptionalParams.put("references", jsonReferences);
                jsonOptionalParams.put("customer", jsonCustomer);

                // Building final request object
                jsonRequest.put("amount", 0.00);// Cannot have amount for
                // CASH@POS transaction.
                jsonRequest.put("mode", "CASH@POS");// This attributes
                // determines the type of
                // transaction
                jsonRequest.put("options", jsonOptionalParams);

                EzeAPI.cardTransaction(this, REQUESTCODE_CASHATPOS, jsonRequest);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else
            displayToast(mandatoryErrMsg);
    }

    private void attachSignature() {
        if (isTransactionIdValid()) {
            JSONObject jsonRequest = new JSONObject();
            JSONObject jsonImageObj = new JSONObject();
            try {
                // Building Image Object
                jsonImageObj
                        .put("imageData",
                                "The Base64 Image bitmap string of your siganture goes here");// Cannot
                // have
                // amount
                // for
                // CASH@POS
                // transaction.
                jsonImageObj.put("imageType", "JPEG");
                jsonImageObj.put("height", "");// optional
                jsonImageObj.put("weight", "");// optional

                // Building final request object
                jsonRequest.put("emiID", "");// pass this field if you have an
                // email Id associated with the
                // transaction
                jsonRequest.put("tipAmount", 0.00);// optional
                // jsonRequest.put("image", jsonImageObj); // Pass this
                // attribute when you have a valid captured signature
                jsonRequest.put("txnId", strTxnId);

                EzeAPI.attachSignature(this, REQUESTCODE_ATTACHSIGN,
                        jsonRequest);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else
            displayToast("Inorrect txn Id, please make a Txn.");
    }

    public class mPosTransactionResponseTask extends
            AsyncTask<String, String, String> implements OnDismissListener {
        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(EzetapPosAtmActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            mPosTransactionResponseTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            mPosTransactionResponseTask.this.cancel(true);
            dialog.cancel();
        }

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method",
                    "mposTransactionResponse"));
            listValuePair.add(new BasicNameValuePair("shop_transaction_id",
                    shop_transaction_id));

            listValuePair.add(new BasicNameValuePair(
                    "card_transaction_response", params[1]));

            String response = RequestClass.getInstance().readPay1Request(
                    EzetapPosAtmActivity.this, Constants.MPOS_URL_FULL,
                    listValuePair);
            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        Constants.showOneButtonSuccessDialog(
                                EzetapPosAtmActivity.this,
                                jsonObject.getString("description"),
                                Constants.RECHARGE_SETTINGS);
                    } else {
                        Constants.showOneButtonFailureDialog(
                                EzetapPosAtmActivity.this,
                                jsonObject.getString("description"), TAG,
                                Constants.OTHER_ERROR);

                    }
                } else {
                    Constants.showOneButtonFailureDialog(
                            EzetapPosAtmActivity.this,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(EzetapPosAtmActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(EzetapPosAtmActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }

        }
    }

    public class CreatemPosTransactionTask extends
            AsyncTask<String, String, String> implements OnDismissListener {
        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(EzetapPosAtmActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            CreatemPosTransactionTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            CreatemPosTransactionTask.this.cancel(true);
            dialog.cancel();
        }

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method",
                    "createMposTransaction"));
            listValuePair.add(new BasicNameValuePair("product_id", params[0]));

            listValuePair.add(new BasicNameValuePair("amount", txtAmount
                    .getText().toString()));

            // listValuePair.add(new BasicNameValuePair("device_type",
            // "android"));
            String response = RequestClass.getInstance().readPay1Request(
                    EzetapPosAtmActivity.this, Constants.MPOS_URL_FULL,
                    listValuePair);
            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONObject description = jsonObject
                                .getJSONObject("description");

                        shop_transaction_id = description
                                .getString("shop_transaction_id");
                        if (SaleOrCash.equalsIgnoreCase("57")) {
                            // doCashTxn();
                            doCashAtPosTxn();
                        } else if (SaleOrCash.equalsIgnoreCase("56")) {
                            doSaleTxn();
                        } else {

                        }
                    } else {
                        Constants.showOneButtonFailureDialog(
                                EzetapPosAtmActivity.this,
                                jsonObject.getString("description"), TAG,
                                Constants.OTHER_ERROR);

                    }
                } else {
                    Constants.showOneButtonFailureDialog(
                            EzetapPosAtmActivity.this,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(EzetapPosAtmActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(EzetapPosAtmActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }

        }
    }

}
