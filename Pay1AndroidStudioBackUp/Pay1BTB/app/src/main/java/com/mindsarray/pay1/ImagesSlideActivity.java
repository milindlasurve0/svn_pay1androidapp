package com.mindsarray.pay1;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import java.util.ArrayList;

public class ImagesSlideActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_slide_activity);
        Bundle b = getIntent().getExtras();
        int position = b.getInt("position");
        int requestFrom = b.getInt("requestFrom");
        ArrayList<String> imageArray = b.getStringArrayList("Array");
        try {
            // JSONArray jsonArray = new JSONArray(Array);
            ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
            ShopImageAdapter adapter = new ShopImageAdapter(this, imageArray, requestFrom);

            viewPager.setAdapter(adapter);

            viewPager.setCurrentItem(position, true);
        } catch (Exception exc) {

        }

        ImageView imageViewClose = (ImageView) findViewById(R.id.imageViewClose);


        imageViewClose.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });

    }

}

