package com.mindsarray.pay1;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.database.DataSetObserver;
import android.database.SQLException;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.ChatAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.ChatHistory;
import com.mindsarray.pay1.databasehandler.ChatHistoryDataSource;
import com.mindsarray.pay1.servicehandler.ChatIntentService;
import com.mindsarray.pay1.utilities.Utility;

import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ChatActivity extends Activity {

    private final static String SERVER_HOST = "dev.pay1.in";
    private final static int SERVER_PORT = 5222;
    private final static String SERVICE_NAME = "dev.pay1.in";
    private static String LOGIN = "demo";
    private static String PASSWORD = "demo";
    int chatId = 0;
    int prevChatId = 0;
    // private List<String> m_discussionThread;
    private ArrayList<HashMap<String, String>> adapterList = new ArrayList<HashMap<String, String>>();
    private XMPPConnection m_connection;
    private Handler m_handler;

    private EditText message;
    private ListView listView_Chat;
    static String recipient = "adminsupport@dev.pay1.in";
    private ChatHistoryDataSource chatHistoryDataSource;
    private ChatAdapter chatAdapter;
    private static final String TAG = "Chat Support";
    public static final String CHAT_TEXT = "text";
    public static final String CHAT_POSITION = "position";
    public static final String CHAT_TIME = "time";
    //	public static GoogleAnalytics analytics; public static Tracker tracker;
    public static GoogleAnalytics analytics;
    public static Tracker tracker;
    boolean isResponded = false;
    private Timer myTimer = null;
    int counter = 0;
    boolean FromStatusBar;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity);
        try {

            if (Utility.getChatTime(ChatActivity.this,
                    Constants.SHAREDPREFERENCE_CHAT_TIME) == 0) {
                Utility.setChatTime(ChatActivity.this,
                        Constants.SHAREDPREFERENCE_CHAT_TIME, 1);
            }
            Utility.setLastRecieveTime(ChatActivity.this,
                    System.currentTimeMillis());
//			tracker = Constants.setAnalyticsTracker(this, tracker);
//
//			//easyTracker.set(Fields.SCREEN_NAME, TAG);
            //analytics = GoogleAnalytics.getInstance(this);
            tracker = Constants.setAnalyticsTracker(this, tracker);
            /*AnalyticsApplication application = (AnalyticsApplication) getApplication();
			tracker = application.getDefaultTracker();*/
            // if (Utility.getChatTime(ChatActivity.this,
            // Constants.SHAREDPREFERENCE_CHAT_TIME) == 0) {
            // Utility.setChatTime(ChatActivity.this,
            // Constants.SHAREDPREFERENCE_CHAT_TIME,
            // System.currentTimeMillis());
            // Utility.setChatFlag(ChatActivity.this,
            // Constants.SHAREDPREFERENCE_CHAT_FLAG, false);
            // }

            // long time = Utility.getChatTime(ChatActivity.this,
            // Constants.SHAREDPREFERENCE_CHAT_TIME);
            // SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
            //
            // String cd = dateFormat.format(new Date(time));
            // String d = dateFormat.format(new Date());
            // Date chat_date = dateFormat.parse(cd);
            // Date current_date = dateFormat.parse(d);
            //
            // if (current_date.compareTo(chat_date) > 0) {
            // if (Utility.getChatFlag(ChatActivity.this,
            // Constants.SHAREDPREFERENCE_CHAT_FLAG) == true) {
            // Utility.setChatFlag(ChatActivity.this,
            // Constants.SHAREDPREFERENCE_CHAT_FLAG, false);
            // }
            // }

            chatHistoryDataSource = new ChatHistoryDataSource(ChatActivity.this);

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                FromStatusBar = bundle.getBoolean("FromStatusBar");
                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancelAll();
            }
            deleteOldMessages();

            Utility.setChatCount(ChatActivity.this, "ChatCount", 0);

            LOGIN = Utility.getMobileNumber(ChatActivity.this,
                    Constants.SHAREDPREFERENCE_MOBILE_NUMBER);
            PASSWORD = Utility.getMobileNumber(ChatActivity.this,
                    Constants.SHAREDPREFERENCE_MOBILE_NUMBER);

            m_handler = new Handler();
            // recipient = (EditText) this.findViewById(R.id.recipient);
            // recipient.setText("admin@192.168.0.38");
            message = (EditText) this.findViewById(R.id.message);
            listView_Chat = (ListView) this.findViewById(R.id.listView_Chat);
            final Button button_LoadMore = new Button(ChatActivity.this);
            button_LoadMore.setHeight(50);
            button_LoadMore.setBackgroundResource(R.drawable.button_background_unselected);
            button_LoadMore.setText("Load more messages..");
            button_LoadMore.setTextColor(getResources().getColor(R.color.white));
            button_LoadMore.setVisibility(View.VISIBLE);
            button_LoadMore.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    // // Log.e("Last post ID ", "" + lastPostID);
                    button_LoadMore.setText("Loading.....");
                    try {

                        chatHistoryDataSource.open();
                        adapterList.clear();
                        List<ChatHistory> chatHistories = chatHistoryDataSource
                                .getAllChatHistory();
                        for (int i = 0; i < chatHistories.size(); i++) {
                            HashMap<String, String> map = new HashMap<String, String>();
                            map.put(CHAT_TEXT, chatHistories.get(i)
                                    .getHistoryText());
                            map.put(CHAT_POSITION, chatHistories.get(i)
                                    .getHistoryPosition());
                            map.put(CHAT_TIME, ""
                                    + chatHistories.get(i).getHistoryTime());

                            adapterList.add(map);

                        }

                        chatAdapter.notifyDataSetChanged();
                    } catch (SQLException exception) {
                        exception.printStackTrace();
                        // // Log.e("Er ", exception.getMessage());
                    } catch (Exception e) {
                        e.printStackTrace();
                        // // Log.e("Er ", e.getMessage());
                    } finally {
                        chatHistoryDataSource.close();

                        button_LoadMore.setText("Load more messages");
                    }
                }
            });

            listView_Chat.addHeaderView(button_LoadMore);
            // Register new user
			/*
			 * ConnectionConfiguration config = new ConnectionConfiguration(
			 * SERVER_HOST, SERVER_PORT, SERVICE_NAME); m_connection = new
			 * XMPPConnection(config); m_connection.connect(); AccountManager
			 * accountManager=new AccountManager(m_connection);
			 * accountManager.createAccount("12345678", "987456321");
			 */

            // m_discussionThread = new ArrayList<String>();
            // m_discussionThreadAdapter = new ArrayAdapter<String>(this,
            // R.layout.multi_line_list_item, m_discussionThread);

            chatAdapter = new ChatAdapter(ChatActivity.this, adapterList);
            listView_Chat.setAdapter(chatAdapter);

            chatAdapter.registerDataSetObserver(new DataSetObserver() {
                @Override
                public void onChanged() {
                    super.onChanged();
                    listView_Chat.setSelection(chatAdapter.getCount() - 1);
                }
            });

            showLast5Messages();

            ImageView send = (ImageView) this.findViewById(R.id.send);
            send.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    isResponded = true;
                    // String to = recipient.getText().toString();
                    String text = message.getText().toString();
                    // // Log.w("Sending to ", "" + recipient);
                    if (text.length() != 0) {
                        try {
                            // myTimer.cancel();
                            // if (Utility.getChatFlag(ChatActivity.this,
                            // Constants.SHAREDPREFERENCE_CHAT_FLAG) == false) {
                            // Toast.makeText(ChatActivity.this,
                            // "Service started", Toast.LENGTH_LONG)
                            // .show();
                            // Utility.setChatFlag(ChatActivity.this,
                            // Constants.SHAREDPREFERENCE_CHAT_FLAG,
                            // true);
                            //
                            // Intent intent = new Intent(ChatActivity.this,
                            // ChatService.class);
                            // intent.putExtra("RECIPIENT", recipient);
                            // intent.putExtra("USER", LOGIN);
                            // Date date = new Date(System.currentTimeMillis());
                            // SimpleDateFormat dateFormat = new
                            // SimpleDateFormat(
                            // "dd-MMMM-yyyy HH:mm:ss");
                            // intent.putExtra("TIME", dateFormat.format(date));
                            // startService(intent);
                            //
                            // }

                            chatHistoryDataSource.open();

                            Message msg = new Message(recipient,
                                    Message.Type.chat);
                            msg.setBody(text);
                            final Long timestamp = System.currentTimeMillis();
                            m_connection.sendPacket(msg);
                            HashMap<String, String> map = new HashMap<String, String>();
                            map.put(CHAT_TEXT, "Me : " + text);
                            map.put(CHAT_POSITION, "1");
                            map.put(CHAT_TIME, "" + timestamp);

                            message.setText("");
                            adapterList.add(map);

                            chatHistoryDataSource.createChatHistory("Me : "
                                    + text, "" + timestamp, "1");
                            chatAdapter.notifyDataSetChanged();

                            InputMethodManager imm = (InputMethodManager) ChatActivity.this
                                    .getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(),
                                    0);

                            if (Utility.getChatTime(ChatActivity.this,
                                    Constants.SHAREDPREFERENCE_CHAT_TIME) == 1) {
                                Utility.setChatTime(ChatActivity.this,
                                        Constants.SHAREDPREFERENCE_CHAT_TIME, 2);
                                // Toast.makeText(ChatActivity.this,
                                // "Service started", Toast.LENGTH_LONG)
                                // .show();
                                Utility.setChatFlag(ChatActivity.this,
                                        Constants.SHAREDPREFERENCE_CHAT_FLAG,
                                        true);
                                Utility.setChatServiceID(
                                        ChatActivity.this,
                                        Constants.SHAREDPREFERENCE_CHAT_SERVICE_ID,
                                        recipient);
                                Date date = new Date(System.currentTimeMillis());
                                SimpleDateFormat dateFormat = new SimpleDateFormat(
                                        "dd-MMMM-yyyy HH:mm:ss");
                                Utility.setChatServiceTime(
                                        ChatActivity.this,
                                        Constants.SHAREDPREFERENCE_CHAT_SERVICE_TIME,
                                        dateFormat.format(date));

                            }

                            Utility.setLastSendingTime(ChatActivity.this,
                                    timestamp);
                            counter = 0;

                            Thread.sleep(1000);
                            isResponded = false;
                            myTimer = null;
                            myTimer = new Timer();

                            chatId = chatId + 1;

                            myTimer.schedule(new TimerTask() {

                                @Override
                                public void run() { // do your work

                                    try {
                                        if (!isResponded) {
                                            if (counter >= 300) {

                                                Intent intent = new Intent(
                                                        ChatActivity.this,
                                                        ChatIntentService.class);
                                                startService(intent);
                                                prevChatId = chatId;
                                                myTimer.cancel();
                                                counter = 0;
                                                // isResponded=true;
                                            } else {
                                                counter++;
                                                Log.d("Counter", "Counter    "
                                                        + counter);
                                            }
                                        } else {
                                            myTimer.cancel();
                                            counter = 0;
                                        }
                                    } catch (Exception e) {
//										Log.e("Send Chat", "" + e.getMessage());
                                        e.printStackTrace();
                                    } finally {
                                        myTimer.cancel();
                                        counter = 0;
                                    }
                                }

                            }, 0, 1000);
                            try {

                                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

                                // Vibrate for 1000 milliseconds
                                v.vibrate(1000);
                            } catch (Exception e) {
                            }

                        } catch (SQLException exception) {
                            // TODO: handle exception
//							Log.e("Er ", " " + exception.getMessage());
                        } catch (Exception e) {
                            // TODO: handle exception
//							Log.e("Er ", " " + e.getMessage());
                        } finally {
                            chatHistoryDataSource.close();
                        }
                    }
                }
            });

            // initConnection();

        } catch (SQLException exception) {
            // TODO: handle exception
            // // Log.e("Er ", exception.getMessage());
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
//			 Log.e("Chat Activity ", " " + e.getMessage());
        } finally {
            // chatHistoryDataSource.close();
        }
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
//		//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
        //tracker.setScreenName(TAG);
        Constants.trackOnStart(tracker, TAG);
        //.build());
    }

    private void deleteOldMessages() {
        try {

            chatHistoryDataSource.open();

            chatHistoryDataSource.deleteChatHistory();

        } catch (SQLException exception) {
            // TODO: handle exception
            // // Log.e("Er ", exception.getMessage());
        } catch (Exception e) {
            // TODO: handle exception
            // // Log.e("Er ", e.getMessage());
        } finally {
            chatHistoryDataSource.close();
        }

    }

    private void showLast5Messages() {
        try {

            chatHistoryDataSource.open();
            adapterList.clear();
            List<ChatHistory> chatHistories = chatHistoryDataSource
                    .getLast5ChatHistory();
            for (int i = chatHistories.size() - 1; i >= 0; i--) {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(CHAT_TEXT, chatHistories.get(i).getHistoryText());
                map.put(CHAT_POSITION, chatHistories.get(i)
                        .getHistoryPosition());
                map.put(CHAT_TIME, "" + chatHistories.get(i).getHistoryTime());

                adapterList.add(map);

            }

            chatAdapter.notifyDataSetChanged();
        } catch (SQLException exception) {
            // TODO: handle exception
            // // Log.e("Er ", exception.getMessage());
        } catch (Exception e) {
            // TODO: handle exception
            // // Log.e("Er ", e.getMessage());
        } finally {
            chatHistoryDataSource.close();
        }

    }

    private class CreateAccountTask extends AsyncTask<Void, Void, String>
            implements OnDismissListener {
        ProgressDialog dialog;
        String response = "";
        boolean isNew = false;
        boolean isConnected = false;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            dialog = new ProgressDialog(ChatActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            CreateAccountTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                if (Constants.isOnline(ChatActivity.this)) {
                    ConnectionConfiguration config = new ConnectionConfiguration(
                            SERVER_HOST, SERVER_PORT, SERVICE_NAME);
                    m_connection = new XMPPConnection(config);

                    if (!m_connection.isConnected()) {
                        m_connection.connect();
                        m_connection.login(LOGIN, PASSWORD);

                        Presence presence = new Presence(
                                Presence.Type.available);
                        m_connection.sendPacket(presence);

                        Message msg = new Message("adminsupport@dev.pay1.in",
                                Message.Type.chat);
//						Message msg = new Message("umesh@dev.pay1.in",
//								Message.Type.chat);
                        msg.setBody("user is now connected");
                        m_connection.sendPacket(msg);

                        isConnected = true;
                    }
                }
            } catch (XMPPException e) {
                response = "Error";
                String str = e.getMessage().toString().trim();
                if (str.equalsIgnoreCase("SASL authentication failed")) {
                    try {
                        AccountManager manager = m_connection
                                .getAccountManager();
                        Map<String, String> attributes = new HashMap<String, String>();

                        attributes.put("username", LOGIN);
                        attributes.put("password", LOGIN);
                        attributes.put("email", LOGIN);
                        attributes.put("name", LOGIN);
                        manager.createAccount(LOGIN, LOGIN, attributes);
                        isNew = true;
                    } catch (XMPPException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                }

            } catch (Exception e) {
                // TODO: handle exception
                response = "Error";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

            try {
                if (Constants.isOnline(ChatActivity.this) && isConnected
                        && !result.equalsIgnoreCase("Error")) {

                    PacketFilter filter = new MessageTypeFilter(
                            Message.Type.chat);

                    m_connection.addPacketListener(new PacketListener() {
                        public void processPacket(Packet packet) {
                            Message message = (Message) packet;
                            if (message.getBody() != null) {
//								Log.e("Chat1", message.getBody());
                                // String fromName =
                                // StringUtils.parseBareAddress(message
                                // .getFrom());
                                // Log.w("Body", "" + message.getBody());

                                try {

                                    chatHistoryDataSource.open();

                                    Long timestamp = System.currentTimeMillis();

                                    int index = message.getBody().indexOf("|");

                                    if (index != -1) {
                                        String data1 = message.getBody()
                                                .substring(0, index);
                                        String data2 = message.getBody()
                                                .substring(index + 1);
                                        // Log.w("Body", "" + data1);
                                        // Log.w("Body", "" + data2);
                                        if (data1.equalsIgnoreCase("0")) {

                                            Utility.setChatTime(
                                                    ChatActivity.this,
                                                    Constants.SHAREDPREFERENCE_CHAT_TIME,
                                                    0);
                                            try {
                                                myTimer.cancel();
                                                isResponded = true;
                                                Utility.setLastRecieveTime(
                                                        ChatActivity.this,
                                                        timestamp);
                                                stopService(new Intent(
                                                        ChatActivity.this,
                                                        ChatIntentService.class));

                                            } catch (Exception e) {
                                            }
                                            HashMap<String, String> map = new HashMap<String, String>();
                                            map.put(CHAT_TEXT, "Pay1 : "
                                                    + data2);
                                            map.put(CHAT_POSITION, "0");
                                            map.put(CHAT_TIME, "" + timestamp);
                                            adapterList.add(map);

                                            chatHistoryDataSource
                                                    .createChatHistory(
                                                            "Pay1 : " + data2,
                                                            "" + timestamp, "0");

                                            ActivityManager am = (ActivityManager) ChatActivity.this
                                                    .getSystemService(Activity.ACTIVITY_SERVICE);
                                            // String packageName = am
                                            // .getRunningTasks(1).get(0).topActivity
                                            // .getPackageName();
                                            String className = am
                                                    .getRunningTasks(1).get(0).topActivity
                                                    .getClassName();

                                            if (!className
                                                    .equalsIgnoreCase("com.mindsarray.pay1.ChatActivity"))
                                                Notify(ChatActivity.this,
                                                        "Message from Pay1",
                                                        data2);
                                            else {
                                                try {
                                                    Uri notification1 = RingtoneManager
                                                            .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                                    Ringtone r = RingtoneManager
                                                            .getRingtone(
                                                                    getApplicationContext(),
                                                                    notification1);
                                                    r.play();

                                                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

                                                    // Vibrate for 1000
                                                    // milliseconds
                                                    v.vibrate(1000);
                                                } catch (Exception e) {
                                                }
                                            }

                                            // m_discussionThread.add(message.getBody());
                                        } else {
                                            // m_discussionThread.add("Pay1 : "
                                            // +
                                            // data2);
                                            recipient = data2;
//											if (!FromStatusBar) {
//												HashMap<String, String> map = new HashMap<String, String>();
//												map.put(CHAT_TEXT,
//														"Pay1 : How may I help you sir?");
//												map.put(CHAT_POSITION, "0");
//												map.put(CHAT_TIME, ""
//														+ timestamp);
//
//												adapterList.add(map);
//											}
                                        }
                                    } else {

                                        Utility.setChatTime(
                                                ChatActivity.this,
                                                Constants.SHAREDPREFERENCE_CHAT_TIME,
                                                0);
                                        try {
                                            myTimer.cancel();
                                            isResponded = true;
                                            stopService(new Intent(
                                                    ChatActivity.this,
                                                    ChatIntentService.class));
                                            Utility.setLastRecieveTime(
                                                    ChatActivity.this,
                                                    timestamp);
                                        } catch (Exception e) {
                                        }
                                        String data1 = message.getBody();
                                        // Log.w("Body", "" + data1);
                                        // if (data1.equalsIgnoreCase("0"))
                                        // {
                                        HashMap<String, String> map = new HashMap<String, String>();
                                        map.put(CHAT_TEXT, "Pay1 : " + data1);
                                        map.put(CHAT_POSITION, "0");
                                        map.put(CHAT_TIME, "" + timestamp);

                                        adapterList.add(map);

                                        chatHistoryDataSource
                                                .createChatHistory("Pay1 : "
                                                                + data1,
                                                        "" + timestamp, "0");

                                        ActivityManager am = (ActivityManager) ChatActivity.this
                                                .getSystemService(Activity.ACTIVITY_SERVICE);
                                        // String packageName = am
                                        // .getRunningTasks(1).get(0).topActivity
                                        // .getPackageName();
                                        String className = am
                                                .getRunningTasks(1).get(0).topActivity
                                                .getClassName();

                                        if (!className
                                                .equalsIgnoreCase("com.mindsarray.pay1.ChatActivity"))
                                            Notify(ChatActivity.this,
                                                    "Message from Pay1", data1);
                                        else {
                                            try {
                                                Uri notification1 = RingtoneManager
                                                        .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                                Ringtone r = RingtoneManager
                                                        .getRingtone(
                                                                getApplicationContext(),
                                                                notification1);
                                                r.play();

                                                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

                                                // Vibrate for 1000 milliseconds
                                                v.vibrate(1000);
                                            } catch (Exception e) {
                                            }
                                        }

                                        // m_discussionThread.add(message.getBody());
                                        // } else {
                                        // m_discussionThread.add("Pay1 : "
                                        // +
                                        // data1);
                                        // }
                                    }

                                    m_handler.post(new Runnable() {
                                        public void run() {
                                            chatAdapter.notifyDataSetChanged();
                                        }
                                    });

                                } catch (SQLException exception) {
                                    // TODO: handle exception
                                    // // Log.e("Er ", exception.getMessage());
                                } catch (Exception e) {
                                    // TODO: handle exception
                                    // // Log.e("Er ", e.getMessage());
                                } finally {
                                    chatHistoryDataSource.close();
                                }
                            }
                        }
                    }, filter);
                } else {
                    Constants.showOneButtonFailureDialog(ChatActivity.this,
                            Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
                            "Chat Support", Constants.CHAT_SETTINGS);
                    if (isNew) {
                        isNew = false;
                        new CreateConnectionTask().execute();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                // TODO: handle exception
                // Constants.showCustomToast(ChatActivity.this,
                // Constants.ERROR_SERVER);
            }
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            CreateAccountTask.this.cancel(true);
            this.dialog.dismiss();
        }

    }

    private class CreateConnectionTask extends AsyncTask<Void, Void, String>
            implements OnDismissListener {
        ProgressDialog dialog;
        String response = "";
        boolean isConnected = false;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            dialog = new ProgressDialog(ChatActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            CreateConnectionTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                if (Constants.isOnline(ChatActivity.this)) {
                    ConnectionConfiguration config = new ConnectionConfiguration(
                            SERVER_HOST, SERVER_PORT, SERVICE_NAME);
                    m_connection = new XMPPConnection(config);

                    if (!m_connection.isConnected()) {
                        m_connection.connect();
                        m_connection.login(LOGIN, PASSWORD);
                        Presence presence = new Presence(
                                Presence.Type.available);
                        m_connection.sendPacket(presence);

                        Message msg = new Message("adminsupport@dev.pay1.in",
                                Message.Type.chat);
//						Message msg = new Message("umesh@dev.pay1.in",
//								Message.Type.chat);
                        msg.setBody("user is now connected");
                        m_connection.sendPacket(msg);

                        isConnected = true;
                    }
                }
            } catch (XMPPException e) {
                response = "Error";
            } catch (Exception e) {
                // TODO: handle exception
                response = "Error";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            try {
                if (Constants.isOnline(ChatActivity.this) && isConnected
                        && !result.equalsIgnoreCase("Error")) {

                    PacketFilter filter = new MessageTypeFilter(
                            Message.Type.chat);

                    m_connection.addPacketListener(new PacketListener() {
                        public void processPacket(Packet packet) {
                            Message message = (Message) packet;
                            if (message.getBody() != null) {
//								Log.e("Chat2", message.getBody());
                                // String fromName =
                                // StringUtils.parseBareAddress(message
                                // .getFrom());
                                // Log.w("Body", "" + message.getBody());

                                try {

                                    chatHistoryDataSource.open();

                                    Long timestamp = System.currentTimeMillis();

                                    int index = message.getBody().indexOf("|");

                                    if (index != -1) {
                                        String data1 = message.getBody()
                                                .substring(0, index);
                                        String data2 = message.getBody()
                                                .substring(index + 1);
                                        // Log.w("Body", "" + data1);
                                        // Log.w("Body", "" + data2);
                                        if (data1.equalsIgnoreCase("0")) {

                                            Utility.setChatTime(
                                                    ChatActivity.this,
                                                    Constants.SHAREDPREFERENCE_CHAT_TIME,
                                                    0);
                                            try {
                                                myTimer.cancel();
                                                isResponded = true;
                                                Utility.setLastRecieveTime(
                                                        ChatActivity.this,
                                                        timestamp);
                                                stopService(new Intent(
                                                        ChatActivity.this,
                                                        ChatIntentService.class));
                                            } catch (Exception e) {
                                            }

                                            HashMap<String, String> map = new HashMap<String, String>();
                                            map.put(CHAT_TEXT, "Pay1 : "
                                                    + data2);
                                            map.put(CHAT_POSITION, "0");
                                            map.put(CHAT_TIME, "" + timestamp);
                                            adapterList.add(map);

                                            chatHistoryDataSource
                                                    .createChatHistory(
                                                            "Pay1 : " + data2,
                                                            "" + timestamp, "0");
                                            ActivityManager am = (ActivityManager) ChatActivity.this
                                                    .getSystemService(Activity.ACTIVITY_SERVICE);
                                            // String packageName = am
                                            // .getRunningTasks(1).get(0).topActivity
                                            // .getPackageName();
                                            String className = am
                                                    .getRunningTasks(1).get(0).topActivity
                                                    .getClassName();

                                            if (!className
                                                    .equalsIgnoreCase("com.mindsarray.pay1.ChatActivity"))
                                                Notify(ChatActivity.this,
                                                        "Pay1 Chat Support",
                                                        data2);
                                            else {
                                                try {
                                                    Uri notification1 = RingtoneManager
                                                            .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                                    Ringtone r = RingtoneManager
                                                            .getRingtone(
                                                                    getApplicationContext(),
                                                                    notification1);
                                                    r.play();

                                                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

                                                    // Vibrate for 1000
                                                    // milliseconds
                                                    v.vibrate(1000);
                                                } catch (Exception e) {
                                                }
                                            }

                                            // m_discussionThread.add(message.getBody());
                                        } else {
                                            // m_discussionThread.add("Pay1 : "
                                            // +
                                            // data2);
                                            recipient = data2;
//											if (!FromStatusBar) {
//												HashMap<String, String> map = new HashMap<String, String>();
//												map.put(CHAT_TEXT,
//														"Pay1 : How may I help you sir?");
//												map.put(CHAT_POSITION, "0");
//												map.put(CHAT_TIME, ""
//														+ timestamp);
//
//												adapterList.add(map);
//											}
                                        }
                                    } else {
                                        Utility.setChatTime(
                                                ChatActivity.this,
                                                Constants.SHAREDPREFERENCE_CHAT_TIME,
                                                0);
                                        try {
                                            myTimer.cancel();
                                            isResponded = true;
                                            Utility.setLastRecieveTime(
                                                    ChatActivity.this,
                                                    timestamp);
                                            stopService(new Intent(
                                                    ChatActivity.this,
                                                    ChatIntentService.class));
                                        } catch (Exception e) {
                                        }

                                        String data1 = message.getBody();
                                        // Log.w("Body", "" + data1);
                                        // if (data1.equalsIgnoreCase("0"))
                                        // {
                                        HashMap<String, String> map = new HashMap<String, String>();
                                        map.put(CHAT_TEXT, "Pay1 : " + data1);
                                        map.put(CHAT_POSITION, "0");
                                        map.put(CHAT_TIME, "" + timestamp);

                                        adapterList.add(map);

                                        chatHistoryDataSource
                                                .createChatHistory("Pay1 : "
                                                                + data1,
                                                        "" + timestamp, "0");
                                        ActivityManager am = (ActivityManager) ChatActivity.this
                                                .getSystemService(Activity.ACTIVITY_SERVICE);
                                        // String packageName = am
                                        // .getRunningTasks(1).get(0).topActivity
                                        // .getPackageName();
                                        String className = am
                                                .getRunningTasks(1).get(0).topActivity
                                                .getClassName();

                                        if (!className
                                                .equalsIgnoreCase("com.mindsarray.pay1.ChatActivity"))
                                            Notify(ChatActivity.this,
                                                    "Pay1 Chat Support", data1);
                                        else {
                                            try {
                                                Uri notification1 = RingtoneManager
                                                        .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                                Ringtone r = RingtoneManager
                                                        .getRingtone(
                                                                getApplicationContext(),
                                                                notification1);
                                                r.play();

                                                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

                                                // Vibrate for 1000 milliseconds
                                                v.vibrate(1000);
                                            } catch (Exception e) {
                                            }
                                        }

                                        // m_discussionThread.add(message.getBody());
                                        // } else {
                                        // m_discussionThread.add("Pay1 : "
                                        // +
                                        // data1);
                                        // }
                                    }

                                    m_handler.post(new Runnable() {
                                        public void run() {
                                            chatAdapter.notifyDataSetChanged();
                                        }
                                    });

                                } catch (SQLException exception) {
                                    // TODO: handle exception
                                    // // Log.e("Er ", exception.getMessage());
                                } catch (Exception e) {
                                    // TODO: handle exception
                                    // // Log.e("Er ", e.getMessage());
                                } finally {
                                    chatHistoryDataSource.close();
                                }
                            }
                        }
                    }, filter);
                } else {
                    Constants.showOneButtonFailureDialog(ChatActivity.this,
                            Constants.ERROR_MESSAGE_FOR_ALL_REQUEST,
                            "Chat Support", Constants.CHAT_SETTINGS);
                }

            } catch (Exception e) {
                // TODO: handle exception
                // Constants.showCustomToast(ChatActivity.this,
                // Constants.ERROR_SERVER);
            }
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            CreateConnectionTask.this.cancel(true);
            this.dialog.dismiss();
        }

    }

    // private void initConnection() throws XMPPException {
    // // Initialization connection
    // ConnectionConfiguration config = new ConnectionConfiguration(
    // SERVER_HOST, SERVER_PORT, SERVICE_NAME);
    // m_connection = new XMPPConnection(config);
    // if (!m_connection.isConnected()) {
    // m_connection.connect();
    // m_connection.login(LOGIN, PASSWORD);
    //
    // Presence presence = new Presence(Presence.Type.available);
    // m_connection.sendPacket(presence);
    // Message msg = new Message(recipient, Message.Type.chat);
    // msg.setBody("hi");
    // m_connection.sendPacket(msg);
    // }
    //
    // // enregistrement de l'�couteur de messages
    // PacketFilter filter = new MessageTypeFilter(Message.Type.chat);
    // m_connection.addPacketListener(new PacketListener() {
    // public void processPacket(Packet packet) {
    // Message message = (Message) packet;
    // if (message.getBody() != null) {
    // // String fromName = StringUtils.parseBareAddress(message
    // // .getFrom());
    // // Log.w("Body", "" + message.getBody());
    //
    // try {
    //
    // chatHistoryDataSource.open();
    //
    // Long timestamp = System.currentTimeMillis();
    //
    // int index = message.getBody().indexOf("|");
    //
    // if (index != -1) {
    // String data1 = message.getBody()
    // .substring(0, index);
    // String data2 = message.getBody().substring(
    // index + 1);
    // // Log.w("Body", "" + data1);
    // // Log.w("Body", "" + data2);
    // if (data1.equalsIgnoreCase("0")) {
    // HashMap<String, String> map = new HashMap<String, String>();
    // map.put(CHAT_TEXT, "Pay1 : " + data2);
    // map.put(CHAT_POSITION, "0");
    // map.put(CHAT_TIME, "" + timestamp);
    // adapterList.add(map);
    //
    // chatHistoryDataSource.createChatHistory(
    // "Pay1 : " + data2, "" + timestamp, "0");
    //
    // // m_discussionThread.add(message.getBody());
    // } else {
    // // m_discussionThread.add("Pay1 : " + data2);
    // recipient = data2;
    // }
    // } else {
    // String data1 = message.getBody();
    // // Log.w("Body", "" + data1);
    // // if (data1.equalsIgnoreCase("0")) {
    // HashMap<String, String> map = new HashMap<String, String>();
    // map.put(CHAT_TEXT, "Pay1 : " + data1);
    // map.put(CHAT_POSITION, "0");
    // map.put(CHAT_TIME, "" + timestamp);
    //
    // adapterList.add(map);
    //
    // chatHistoryDataSource.createChatHistory("Pay1 : "
    // + data1, "" + timestamp, "0");
    // // m_discussionThread.add(message.getBody());
    // // } else {
    // // m_discussionThread.add("Pay1 : " + data1);
    // // }
    // }
    //
    // m_handler.post(new Runnable() {
    // public void run() {
    // chatAdapter.notifyDataSetChanged();
    // }
    // });
    //
    // } catch (SQLException exception) {
    // // TODO: handle exception
    // // // Log.e("Er ", exception.getMessage());
    // } catch (Exception e) {
    // // TODO: handle exception
    // // // Log.e("Er ", e.getMessage());
    // } finally {
    // chatHistoryDataSource.close();
    // }
    // }
    // }
    // }, filter);
    // }

    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (Constants.isExpanded) {
            Constants.collapsView();
        } else {
            super.onBackPressed();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Constants.showNavigationBar(ChatActivity.this);
        if (Utility.getLoginFlag(ChatActivity.this,
                Constants.SHAREDPREFERENCE_IS_LOGIN)) {
            new CreateAccountTask().execute();
        } else {
            startActivity(new Intent(ChatActivity.this, LoginActivity.class));
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        Utility.setChatTime(ChatActivity.this,
                Constants.SHAREDPREFERENCE_CHAT_TIME, 0);
        super.onDestroy();
    }

    private void Notify(Context mContext, String notificationTitle,
                        String notificationMessage) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Notification notification = new Notification(
                R.drawable.ic_b2b_notif, "Pay1 Chat Support",
                System.currentTimeMillis());
        // Constants.notificationList.add(notificationMessage);
        notification.number = Constants.COUNT_CHAT++;
        Utility.setChatCount(mContext, "ChatCount", Constants.COUNT_CHAT);
        Intent notificationIntent = new Intent(ChatActivity.this,
                ChatActivity.class);
        notificationIntent.putExtra("FromStatusBar", true);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        try {
            Uri notification1 = RingtoneManager
                    .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(),
                    notification1);
            r.play();

            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

            // Vibrate for 1000 milliseconds
            v.vibrate(1000);
        } catch (Exception e) {
        }
        // notification.number=count++;
		/*notification.setLatestEventInfo(mContext, notificationTitle,
				notificationMessage, pendingIntent);*/
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(9999, notification);
        isResponded = true;
        myTimer.cancel();
        Utility.setLastRecieveTime(ChatActivity.this,
                System.currentTimeMillis());
        stopService(new Intent(ChatActivity.this, ChatIntentService.class));
    }

}