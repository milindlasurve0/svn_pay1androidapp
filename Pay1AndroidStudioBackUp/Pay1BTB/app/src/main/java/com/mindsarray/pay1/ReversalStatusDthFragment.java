package com.mindsarray.pay1;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.StatusAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ReversalStatusDthFragment extends Fragment {
    View vi = null;
    private TextView mDateDisplay;
    private Button btnCalender, btnPreWeek, btnNextWeek;
    private int mYear;
    private int mMonth;
    private int mDay;
    boolean bNetOrSms;
    private SharedPreferences mSettingsData;
    StringBuilder builder;
    static final int DATE_DIALOG_ID = 0;
    ListView mStatusListView;
    ArrayList<String> statusList;
    StatusAdapter adapter;
    Bundle bundle;
    private static final String TAG = "Status";

    String months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
            "Sep", "Oct", "Nov", "Dec"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//		analytics = GoogleAnalytics.getInstance(getActivity()); 
        tracker = Constants.setAnalyticsTracker(getActivity(), tracker);//easyTracker.set(Fields.SCREEN_NAME, TAG);
        vi = inflater.inflate(R.layout.status_activity, container, false);

        btnCalender = (Button) vi.findViewById(R.id.btnCalender);
        btnPreWeek = (Button) vi.findViewById(R.id.btnPreWeek);
        btnNextWeek = (Button) vi.findViewById(R.id.btnNextWeek);
        mDateDisplay = (TextView) vi.findViewById(R.id.mDateDisplay);
        mStatusListView = (ListView) vi.findViewById(R.id.mStatusListView);
        TextView textView_NoData = (TextView) vi
                .findViewById(R.id.textView_NoData);
        mStatusListView.setEmptyView(textView_NoData);
        bundle = getActivity().getIntent().getExtras();
        mSettingsData = getActivity().getSharedPreferences(
                SettingsSmsActivity.SETTINGS_NAME, 0);
        bNetOrSms = mSettingsData.getBoolean(SettingsSmsActivity.KEY_NETWORK,
                true);

        if (bNetOrSms) {
            if (Utility.getLoginFlag(getActivity(),
                    Constants.SHAREDPREFERENCE_IS_LOGIN)) {
                mInsideonCreate();

            } else {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.putExtra(Constants.REQUEST_FOR, bundle);
                startActivityForResult(intent, Constants.LOGIN);

            }
        } else {
            // mInsideonCreate();
            Constants
                    .showTwoButtonFailureDialog(
                            getActivity(),
                            "Internet setting is not enabled.\nWould you like to change settings?",
                            "Settings", Constants.SMS_SETTINGS);
        }
        return vi;

    }

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        //tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
    }

    public static ReversalStatusDthFragment newInstance(String content) {
        ReversalStatusDthFragment fragment = new ReversalStatusDthFragment();

        Bundle b = new Bundle();
        b.putString("msg", content);

        fragment.setArguments(b);

        return fragment;
    }

    // private String mContent = "???";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // if ((savedInstanceState != null)
        // && savedInstanceState.containsKey(KEY_CONTENT)) {
        // mContent = savedInstanceState.getString(KEY_CONTENT);
        // }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // outState.putString(KEY_CONTENT, mContent);
    }

    private void mInsideonCreate() {
        // TODO Auto-generated method stub
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        btnCalender.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Toast.makeText(getActivity(), "Hi I am Working Fine",
                // Toast.LENGTH_LONG).show();
                // getActivity().showDialog(DATE_DIALOG_ID);
                // Constants.showSuccessDialog(getActivity(),
                // "Hi I am Working");

                DatePickerDialog dialog = new DatePickerDialog(getActivity(),
                        datePickerListener, mYear, mMonth, mDay);
                dialog.show();
            }
        });

        btnPreWeek.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    Date selected_date = new Date(mDateDisplay.getText()
                            .toString().trim());

                    Calendar c = Calendar.getInstance();
                    c.set((1900 + selected_date.getYear()),
                            selected_date.getMonth(), selected_date.getDate());
                    c.add(Calendar.DATE, -1);
                    // dateFormat.format(c.getTime());

                    // Show current date

                    mDay = c.get(Calendar.DAY_OF_MONTH);
                    mMonth = c.get(Calendar.MONTH);
                    mYear = c.get(Calendar.YEAR);

                    updateDisplay();
                } catch (Exception e) {

                }
            }
        });

        btnNextWeek.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    Date selected_date = new Date(mDateDisplay.getText()
                            .toString().trim());

                    Calendar c = Calendar.getInstance();
                    c.set((1900 + selected_date.getYear()),
                            selected_date.getMonth(), selected_date.getDate());
                    c.add(Calendar.DATE, +1);
                    // dateFormat.format(c.getTime());

                    // Show current date

                    mDay = c.get(Calendar.DAY_OF_MONTH);
                    mMonth = c.get(Calendar.MONTH);
                    mYear = c.get(Calendar.YEAR);

                    updateDisplay();
                } catch (Exception e) {

                }
            }
        });

        updateDisplay();

        statusList = new ArrayList<String>();
        adapter = new StatusAdapter(getActivity(), statusList, 2);
        /*
		 * if (bundle.getInt(Constants.REQUEST_FOR) == Constants.MOBILE_TAB) {
		 * adapter = new StatusAdapter(getActivity(), statusList, 1); } else if
		 * (bundle.getInt(Constants.REQUEST_FOR) == Constants.DTH_TAB) { adapter
		 * = new StatusAdapter(getActivity(), statusList, 2); } else if
		 * (bundle.getInt(Constants.REQUEST_FOR) == Constants.ENTERTAINMENT_TAB)
		 * { adapter = new StatusAdapter(getActivity(), statusList, 1); } else
		 * if (bundle.getInt(Constants.REQUEST_FOR) == Constants.BILL_TAB) {
		 * adapter = new StatusAdapter(getActivity(), statusList, 1); }
		 */
        mStatusListView.setAdapter(adapter);
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            // Do whatever you want
            // updateDisplay();

            mYear = selectedYear;
            mMonth = selectedMonth;
            mDay = selectedDay;

            updateDisplay();
        }
    };

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                DatePickerDialog dialog = new DatePickerDialog(getActivity(),
                        datePickerListener, mYear, mMonth, mDay);

			/*
			 * if (Build.VERSION.SDK_INT >= 11) { // (picker is a DatePicker)
			 * dialog.getDatePicker().setMaxDate(new Date().getTime()); } else {
			 * 
			 * }
			 */

                return dialog;
			/*
			 * return new DatePickerDialog(this, mDateSetListener, mYear,
			 * mMonth, mDay);
			 */
        }
        return null;
    }

    private void updateDisplay() {
        String m = "";
        String dt = "";
        if (mDay < 10)
            dt = "0" + mDay;
        else
            dt = mDay + "";
        if ((mMonth + 1) < 10)
            m = "0" + (mMonth + 1);
        else
            m = (mMonth + 1) + "";
        mDateDisplay.setText(dt + "-" + months[mMonth] + "-" + mYear);
        builder = new StringBuilder().append(mYear).append("-").append(m)
                .append("-").append(dt);
        new GetStatus().execute();
    }

    public class GetStatus extends AsyncTask<String, String, String> implements
            OnDismissListener {

        ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method",
                    "reversalTransactions"));
            listValuePair
                    .add(new BasicNameValuePair("date", builder.toString()));
			/*
			 * if (bundle.getInt(Constants.REQUEST_FOR) == Constants.MOBILE_TAB)
			 * { listValuePair.add(new BasicNameValuePair("service", "1")); }
			 * else if (bundle.getInt(Constants.REQUEST_FOR) ==
			 * Constants.DTH_TAB) { listValuePair.add(new
			 * BasicNameValuePair("service", "2")); } else if
			 * (bundle.getInt(Constants.REQUEST_FOR) ==
			 * Constants.ENTERTAINMENT_TAB) { listValuePair.add(new
			 * BasicNameValuePair("service", "3")); } else if
			 * (bundle.getInt(Constants.REQUEST_FOR) == Constants.BILL_TAB) {
			 * listValuePair.add(new BasicNameValuePair("service", "4")); }
			 */
            listValuePair.add(new BasicNameValuePair("service", "2"));
            listValuePair.add(new BasicNameValuePair("device_type", "android"));
            String response = RequestClass.getInstance().readPay1Request(
                    getActivity(), Constants.API_URL, listValuePair);
            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            super.onPostExecute(result);
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    statusList.clear();
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray jsonDescription = jsonObject
                                .getJSONArray("description");
                        JSONArray array2 = jsonDescription.getJSONArray(0);
                        for (int i = 0; i < array2.length(); i++) {
                            JSONObject json_data = array2.getJSONObject(i);
                            statusList.add(json_data.toString());

                        }
                        // Log.d("Arra", "Arra");
                        adapter.notifyDataSetChanged();
                    } else {
                        Constants.showOneButtonFailureDialog(getActivity(),
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(getActivity(),
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(getActivity(),
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(getActivity(),
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(getActivity());
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            GetStatus.this.cancel(true);
                            dialog.cancel();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            GetStatus.this.cancel(true);
            dialog.cancel();
        }
    }
}
