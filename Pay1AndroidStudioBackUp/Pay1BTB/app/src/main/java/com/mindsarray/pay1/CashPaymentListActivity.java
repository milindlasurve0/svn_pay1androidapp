package com.mindsarray.pay1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.mindsarray.pay1.adapterhandler.CashPaymentAdapter;
import com.mindsarray.pay1.constant.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class CashPaymentListActivity extends Activity implements OnClickListener {
    private static final String TAG = "C2D Payment ";
    ArrayList<JSONObject> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cash_payment_list_activity);
        arrayList = new ArrayList<JSONObject>();
        ListView listViewPayment = (ListView) findViewById(R.id.listViewPayment);
        CashPaymentAdapter adapter = new CashPaymentAdapter(
                CashPaymentListActivity.this, 0, arrayList, this);
        listViewPayment.setAdapter(adapter);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String cashList = bundle.getString("cashList");
            String mobileNumber = bundle.getString("mobileNumber");
            TextView textViewMobNumber = (TextView) findViewById(R.id.textViewMobNumber);
            textViewMobNumber.setText(mobileNumber);
            try {

                JSONArray array = new JSONArray(cashList);
                if (array.length() != 0) {
                    for (int i = 0; i < array.length(); i++) {
                        arrayList.add(array.getJSONObject(i));
                    }
                    adapter.notifyDataSetChanged();
                } else {
                    Constants.showOneButtonFailureDialog(
                            CashPaymentListActivity.this, "No Record Found",
                            TAG, Constants.OTHER_ERROR);
                }
            } catch (Exception exc) {

            }

        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (Constants.isExpanded) {
            Constants.collapsView();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Constants.showNavigationBar(CashPaymentListActivity.this);

        super.onResume();
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        int pos = (Integer) v.getTag();
        Intent intent = new Intent(CashPaymentListActivity.this, ProceedCashPaymentActivity.class);
        intent.putExtra("details", arrayList.get(pos).toString());

        startActivity(intent);
    }

}
