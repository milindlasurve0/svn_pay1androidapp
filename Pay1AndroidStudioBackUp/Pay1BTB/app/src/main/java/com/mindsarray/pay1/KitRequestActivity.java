package com.mindsarray.pay1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class KitRequestActivity extends Activity {
    public static final String TAG = "Kit Enquiry";
    // TextView textView_Message;
    Button button_Ok, button_Cancel;
    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kit_request_activity);
        button_Ok = (Button) findViewById(R.id.button_Ok);
        button_Cancel = (Button) findViewById(R.id.button_Cancel);
        Bundle bundle = getIntent().getExtras();
        String service_type = bundle.getString("service_type");
        TextView textTitle = (TextView) findViewById(R.id.textTitle);
        TextView textMsg = (TextView) findViewById(R.id.textMsg);
        ImageView sample_image = (ImageView) findViewById(R.id.sample_image);

        if (service_type.equalsIgnoreCase("4")) {
            textTitle.setText("MOBILE POSTPAID BILL PAYMENT SERVICE");
            textMsg.setText("To activate Mobile Postpaid bill payment\nservice please click on Send Enquiry Button.");
            sample_image.setImageDrawable(getResources().getDrawable(R.drawable.ic_mob_bill_info));
            //ic_mob_bill_info
        } else if (service_type.equalsIgnoreCase("6")) {
            textTitle.setText("UTILITY BILL PAYMENT SERVICE");
            textMsg.setText("To activate Electricity and Gas bill payment\nservice please click on Send Enquiry Button.");
            sample_image.setImageDrawable(getResources().getDrawable(R.drawable.ic_utility_bill_info));

        }

        tracker = Constants.setAnalyticsTracker(this, tracker);
        button_Ok.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new mPosLeadTask().execute();
            }
        });

        button_Cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });

    }

    public class mPosLeadTask extends AsyncTask<String, String, String>
            implements OnDismissListener {

        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(KitRequestActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            mPosLeadTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            mPosLeadTask.this.cancel(true);
            dialog.cancel();
        }

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method",
                    "kitActivationRequest"));
            //listValuePair.add(new BasicNameValuePair("service_id", "8"));

            String response = RequestClass.getInstance().readPay1Request(
                    KitRequestActivity.this, Constants.API_URL,
                    listValuePair);
            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {

                        Constants.showOneButtonDialog(KitRequestActivity.this,
                                "THANK YOU",
                                jsonObject.getString("description"), "OK", 1);
                    } else {

                        Constants.showOneButtonFailureDialog(
                                KitRequestActivity.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);

                    }
                } else {
                    Constants.showOneButtonFailureDialog(
                            KitRequestActivity.this, Constants.ERROR_INTERNET,
                            TAG, Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(KitRequestActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(KitRequestActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        // //tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);

        // tracker.setScreenName(TAG);
        Constants.trackOnStart(tracker, TAG);
        // .build());
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub

        Constants.showNavigationBar(KitRequestActivity.this);
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (Constants.isExpanded) {
            Constants.collapsView();
        } else {
            super.onBackPressed();
        }
    }

}
