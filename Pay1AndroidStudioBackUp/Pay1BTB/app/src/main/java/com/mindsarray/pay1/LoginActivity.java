package com.mindsarray.pay1;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.SQLException;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.AddressDataSource;
import com.mindsarray.pay1.databasehandler.VMNDataSource;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.servicehandler.RegistrationIntentService;
import com.mindsarray.pay1.utilities.EditTextValidator;
import com.mindsarray.pay1.utilities.MyLocation;
import com.mindsarray.pay1.utilities.MyLocation.LocationResult;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends Activity {
    EditText editMobileNumber, editPin;
    Button mBtnLogin, mBtnRegister;
    String strMobileNumber, strPin;
    String uuid = "";
    String provider;
    VMNDataSource vmnDataSource;
    TextView textForgetPin;
    AddressDataSource addressDataSource;
    LoginTask loginTask;
    String forgotMobileNumber;
    ProgressDialog dialog;

    Bundle extras;
    String intentClass;

    private static final String TAG = "Login";
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    String[] permissions= new String[]{
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_SMS,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION};

    private  boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p:permissions) {
            result = ContextCompat.checkSelfPermission(LoginActivity.this,p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new
                    String[listPermissionsNeeded.size()]),100);
            return false;
        }
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        // WindowManager.LayoutParams.FLAG_FULLSCREEN);
        try {

            //if (Utility.getIsAppUsed(LoginActivity.this)) {

            if (android.os.Build.VERSION.SDK_INT >= 23) {




                /*if (checkPermissions()){

                //  permissions  granted.
            }*/
                // only for gingerbread and newer versions
                //String permission = Manifest.permission.CAMERA;
                ArrayList<String> arrayList = new ArrayList<String>();

                boolean isGranted = true;
                String permissionCall = Manifest.permission.CALL_PHONE;
                if (checkSelfPermission(permissionCall) != PackageManager.PERMISSION_GRANTED) {
                    arrayList.add(permissionCall);
                    isGranted = false;
                        /*ActivityCompat.requestPermissions(LoginActivity.this, new
								String[]{permissionCall}, 100);*/
                }


                String permissionLocation = android.Manifest.permission.ACCESS_FINE_LOCATION;
                if (checkSelfPermission(permissionLocation) != PackageManager.PERMISSION_GRANTED) {
                    arrayList.add(permissionLocation);
                    isGranted = false;
						/*ActivityCompat.requestPermissions(LoginActivity.this, new
								String[]{permissionLocation}, 100);*/
                }


                String permissionStorage = android.Manifest.permission
                        .WRITE_EXTERNAL_STORAGE;
                if (checkSelfPermission(permissionStorage) != PackageManager.PERMISSION_GRANTED) {
                    arrayList.add(permissionStorage);
                    isGranted = false;
						/*ActivityCompat.requestPermissions(LoginActivity.this, new
								String[]{permissionStorage}, 100);*/
                }


                String permissionContacts = Manifest.permission.READ_SMS;
                if (checkSelfPermission(permissionContacts) != PackageManager.PERMISSION_GRANTED) {
                    arrayList.add(permissionContacts);
                    isGranted = false;
						/*ActivityCompat.requestPermissions(LoginActivity.this, new
								String[]{permissionContacts}, 100);*/
                }

                String permissionContacts1 = android.Manifest.permission.READ_PHONE_STATE;
                if (checkSelfPermission(permissionContacts1) != PackageManager.PERMISSION_GRANTED) {
                    arrayList.add(permissionContacts1);
                    isGranted = false;
						/*ActivityCompat.requestPermissions(LoginActivity.this, new
								String[]{permissionContacts}, 100);*/
                }

               /* String permissionContacts11 = Manifest.permission.READ_CONTACTS;
                if (checkSelfPermission(permissionContacts11) != PackageManager.PERMISSION_GRANTED) {
                    arrayList.add(permissionContacts11);
                    isGranted = false;
						*//*ActivityCompat.requestPermissions(LoginActivity.this, new
								String[]{permissionContacts}, 100);*//*
                }*/

                if (isGranted) {

                } else {
                    String[] dsf = new String[arrayList.size()];
                    arrayList.toArray(dsf);
                    ActivityCompat.requestPermissions(LoginActivity.this,
                            dsf, 100);
                }

/*


			String permission = "android.permission.CALL_PHONE";
			if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {

				ActivityCompat.requestPermissions(RegistrationLayout.this, new
						String[]{permission,"android.permission.",android.Manifest.permission
						.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION,
						Manifest.permission.READ_SMS, Manifest.permission.READ_CONTACTS}, 100);
			} else {
				// Add your function here which open camera
			}*/
            } else {
                // Add your function here which open camera
            }


			/*} else {
				Intent intent = new Intent(LoginActivity.this,
						RegistrationLayout.class);
				startActivity(intent);
				finish();
			}*/

            setContentView(R.layout.login_activity);
            getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            findViewById();
            // tracker = Constants.setAnalyticsTracker(this, tracker);
            // //easyTracker.set(Fields.SCREEN_NAME, TAG);
            tracker = Constants.setAnalyticsTracker(this, tracker);
            vmnDataSource = new VMNDataSource(LoginActivity.this);
            addressDataSource = new AddressDataSource(LoginActivity.this);

            String mobNo = Utility.getUserMobileNumber(LoginActivity.this);

            editMobileNumber.setText(mobNo);
            editPin.setText("");

			/*editMobileNumber.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					editPin.setText("");
					editMobileNumber.setText("");
					Intent intent = new Intent(LoginActivity.this,
							RegistrationLayout.class);
					startActivity(intent);
					finish();
				}
			});*/
            // editMobileNumber.setKeyListener(null);
           /* editMobileNumber.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    // TODO Auto-generated method stub
                    EditTextValidator.hasText(LoginActivity.this,
                            editMobileNumber,
                            Constants.ERROR_MOBILE_BLANK_FIELD);
                    if (editMobileNumber.length() == 10) {
                        editPin.requestFocus();
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable s) {
                    // TODO Auto-generated method stub

                }
            });*/

            textForgetPin.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    // new ForgetPasswordTask().execute();

                    final Context context = LoginActivity.this;
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_forgot_password);
                    TextView textView_Title = (TextView) dialog
                            .findViewById(R.id.textView_Title);
                    TextView text = (TextView) dialog
                            .findViewById(R.id.textView_Message);
                    final EditText editTextForgot = (EditText) dialog
                            .findViewById(R.id.editTextForgot);
                    text.setText("Please enter your number registered with us.");
                    textView_Title.setText("GENERATE NEW PIN");
                    ImageView closeButton = (ImageView) dialog
                            .findViewById(R.id.imageView_Close);
                    closeButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    Button dialogButtonOk = (Button) dialog
                            .findViewById(R.id.button_Ok);
                    dialogButtonOk
                            .setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    forgotMobileNumber = editTextForgot
                                            .getText().toString();
                                    dialog.dismiss();
                                    new ForgetPasswordTask()
                                            .execute(forgotMobileNumber);

                                }
                            });
                    dialog.setCancelable(false);
                    dialog.show();

                }
            });

           /* editPin.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    // TODO Auto-generated method stub
                    EditTextValidator.hasText(LoginActivity.this, editPin,
                            Constants.ERROR_LOGIN_PIN_BLANK_FIELD);

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable s) {
                    // TODO Auto-generated method stub

                }
            });*/

            editPin.setOnEditorActionListener(new TextView.OnEditorActionListener() {

                @Override
                public boolean onEditorAction(TextView v, int actionId,
                                              KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (actionId == EditorInfo.IME_ACTION_GO) {
                        try {
                            strMobileNumber = editMobileNumber.getText()
                                    .toString();
                            strPin = editPin.getText().toString();
                            if (!checkValidation(LoginActivity.this)) {
                                // Constants.showCustomToast(LoginActivity.this,
                                // "Please Enter a valid Number");
                            } else {
                                // String string =
                                // Constants.encryptPassword(strPin);

                                Constants
                                        .displayPromptForEnablingGPS(LoginActivity.this);
                                loginTask = new LoginTask();
                                loginTask.execute();
                            }
                        } catch (Exception e) {
                            // e.printStackTrace();
                        }
                        return true;
                    }
                    return false;
                }
            });



            try {
                TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                uuid = tManager.getDeviceId();
                if (uuid.length() == 0) {
                    uuid = Settings.System.getString(LoginActivity.this
                                    .getContentResolver(),
                            Secure.ANDROID_ID);

                    if (uuid.length() == 0 || uuid == null) {
                        uuid = Secure.getString(
                                LoginActivity.this.getContentResolver(),
                                Secure.ANDROID_ID);

                    }

                }
                if (Utility.getUUID(LoginActivity.this,
                        Constants.SHAREDPREFERENCE_UUID) == null)
                    Utility.setUUID(LoginActivity.this,
                            Constants.SHAREDPREFERENCE_UUID, uuid);
            } catch (Exception exception) {
                uuid = Secure.getString(
                        LoginActivity.this.getContentResolver(),
                        Secure.ANDROID_ID);
                if (Utility.getUUID(LoginActivity.this,
                        Constants.SHAREDPREFERENCE_UUID) == null)
                    Utility.setUUID(LoginActivity.this,
                            Constants.SHAREDPREFERENCE_UUID, uuid);
            }

            mBtnLogin.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    String serial = null;

                    try {
                        Class<?> c = Class.forName("android.os.SystemProperties");
                        Method get = c.getMethod("get", String.class);
                        serial = (String) get.invoke(c, "ro.serialno");
                    } catch (Exception ignored) {
                    }
                    try {
                        TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                        uuid = tManager.getDeviceId();
                        if (uuid.length() == 0) {
                            uuid = Settings.System.getString(LoginActivity.this
                                    .getContentResolver(),
                                    Secure.ANDROID_ID);

                            if (uuid.length() == 0 || uuid == null) {
                                uuid = Secure.getString(
                                        LoginActivity.this.getContentResolver(),
                                        Secure.ANDROID_ID);

                            }

                        }
                        if (Utility.getUUID(LoginActivity.this,
                                Constants.SHAREDPREFERENCE_UUID) == null)
                            Utility.setUUID(LoginActivity.this,
                                    Constants.SHAREDPREFERENCE_UUID, uuid);
                    } catch (Exception exception) {
                        uuid = Secure.getString(
                                LoginActivity.this.getContentResolver(),
                                Secure.ANDROID_ID);
                        if (Utility.getUUID(LoginActivity.this,
                                Constants.SHAREDPREFERENCE_UUID) == null)
                            Utility.setUUID(LoginActivity.this,
                                    Constants.SHAREDPREFERENCE_UUID, uuid);
                    }


                    strMobileNumber = editMobileNumber.getText().toString();
                    strPin = editPin.getText().toString();


                    if (!checkValidation(LoginActivity.this)) {
                        // Constants.showCustomToast(LoginActivity.this,
                        // "Please Enter a valid Number");
                    } else {
                        // String string = Constants.encryptPassword(strPin);
                        // Utility.setHTTPObject(LoginActivity.this,
                        // Constants.SHAREDPREFERENCE_HTTP, null);
                        // Constants.displayPromptForEnablingGPS(LoginActivity.this);
						/*
						 * loginTask.execute(); new LoginTask().execute();
						 */

                        final AlertDialog.Builder dialog = new AlertDialog.Builder(
                                LoginActivity.this);
                        LocationManager lm = null;
                        boolean gps_enabled = false, network_enabled = false;
                        if (lm == null)
                            lm = (LocationManager) LoginActivity.this
                                    .getSystemService(Context.LOCATION_SERVICE);
                        try {
                            gps_enabled = lm
                                    .isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
                        } catch (Exception ex) {
                        }
                        try {
                            network_enabled = lm
                                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                        } catch (Exception ex) {
                        }

                        if (!network_enabled) {
                            // dialog = new
                            // AlertDialog.Builder(LoginActivity.this);
                            dialog.setMessage("To get your Shop Location:\n\n1.Turn on GPS and mobile network location\n2.Turn on Wi-Fi");
                            dialog.setPositiveButton("On",
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(
                                                DialogInterface paramDialogInterface,
                                                int paramInt) {
                                            // TODO Auto-generated method stub
                                            Intent myIntent = new Intent(
                                                    Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                            LoginActivity.this
                                                    .startActivity(myIntent);
                                            // get gps
                                        }
                                    });
                            dialog.setNegativeButton("Cancel",
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(
                                                DialogInterface paramDialogInterface,
                                                int paramInt) {
                                            // TODO Auto-generated method stub
                                            loginTask = new LoginTask();
                                            loginTask.execute();
                                            // new LoginTask().execute();
                                        }
                                    });
                            dialog.show();

                        } else {

                            loginTask = new LoginTask();
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                                loginTask
                                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            else
                                loginTask.execute();
                            // loginTask.execute();
                            // new LoginTask().execute();
                        }

                    }
                }
            });

            mBtnRegister.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    startActivity(new Intent(LoginActivity.this,
                            SelectLeadActivity.class));
                    //finish();
                }
            });

            try {
                addressDataSource.open();

                if (addressDataSource.getAllAddress().size() != 0) {
                    List<com.mindsarray.pay1.databasehandler.Address> addresses = addressDataSource
                            .getAllAddress();

                    if (addresses.get(0).getAddressLatitude() != null
                            && addresses.get(0).getAddressLongitude() != null) {
                        Location loc = new Location("test");
                        loc.setLatitude(Double.parseDouble(addresses.get(0)
                                .getAddressLatitude()));
                        loc.setLongitude(Double.parseDouble(addresses.get(0)
                                .getAddressLongitude()));

                        Utility.setLatitude(LoginActivity.this,
                                Constants.SHAREDPREFERENCE_LATITUDE, addresses
                                        .get(0).getAddressLatitude());
                        Utility.setLongitude(LoginActivity.this,
                                Constants.SHAREDPREFERENCE_LONGITUDE, addresses
                                        .get(0).getAddressLongitude());

                        provider = loc.getProvider();
                    }

                } else {

                    LocationResult locationResult = new LocationResult() {
                        @Override
                        public void gotLocation(Location location) {
                            // Got the location!
                            try {
                                // //// Log.w("Location", location + "");

                                if (location != null) {
                                    Utility.setLatitude(
                                            LoginActivity.this,
                                            Constants.SHAREDPREFERENCE_LATITUDE,
                                            String.valueOf(location
                                                    .getLatitude()));
                                    Utility.setLongitude(
                                            LoginActivity.this,
                                            Constants.SHAREDPREFERENCE_LONGITUDE,
                                            String.valueOf(location
                                                    .getLongitude()));

                                    provider = location.getProvider();
                                }

                            } catch (Exception exception) {
                            }
                        }
                    };
                    MyLocation myLocation = new MyLocation();
                    myLocation.getLocation(this, locationResult);
                }
            } catch (SQLException exception) {
                // TODO: handle exception
                // //// Log.e("Er ", exception.getMessage());
            } catch (Exception e) {
                // TODO: handle exception
                Log.e("Er ", e.getMessage());
            } finally {
                addressDataSource.close();
            }

            if (Utility.getOSVersion(LoginActivity.this,
                    Constants.SHAREDPREFERENCE_OS_VERSION) == null)
                Utility.setOSVersion(LoginActivity.this,
                        Constants.SHAREDPREFERENCE_OS_VERSION,
                        android.os.Build.VERSION.RELEASE);

            if (Utility.getOSManufacturer(LoginActivity.this,
                    Constants.SHAREDPREFERENCE_OS_MANUFACTURER) == null)
                Utility.setOSManufacturer(LoginActivity.this,
                        Constants.SHAREDPREFERENCE_OS_MANUFACTURER,
                        android.os.Build.MANUFACTURER);



            mRegistrationBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (intent.getExtras() != null) {
                        String regId = intent.getExtras().getString(
                                "registration_id");
                        Log.e("BroadcastReceiver", "" + regId);

                        Utility.setGCMID(LoginActivity.this,
                                Constants.SHAREDPREFERENCE_GCMID, regId);
                    }
                }
            };

            Intent intent = new Intent(LoginActivity.this,
                    RegistrationIntentService.class);
            startService(intent);

        } catch (Exception e) {
            // TODO: handle exception
            // Log.e("Er ", e.getMessage());
        }

        View pay1AppDownload = findViewById(R.id.login_download_pay1);
        pay1AppDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,
                        ConsumerAppActivity.class));
            }
        });

        try {
            extras = getIntent().getExtras();
            if (extras != null) {
                intentClass = extras.getString("class");
                if (intentClass != null
                        && (intentClass.equals("PGActivity") || intentClass
                        .equals("AutoLogin"))) {
                    editMobileNumber.setText(extras
                            .getString("retailer_mobile"));
                    // editPin.setText(extras.getString("retailer_pin"));
                    mBtnLogin.performClick();
                } else {
                    String mobNo = Utility
                            .getUserMobileNumber(LoginActivity.this);

                    editMobileNumber.setText(mobNo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 100: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Add your function here which open camera
                } else {
                    Toast.makeText(getApplication(), "Permission required", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        // tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
    }

    public class ForgetPasswordTask extends AsyncTask<String, String, String>
            implements OnDismissListener {

        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair
                    .add(new BasicNameValuePair("method", "forgetPassword"));
            listValuePair.add(new BasicNameValuePair("mobileNo", params[0]));

            String response = RequestClass.getInstance().readPay1Request(
                    LoginActivity.this, Constants.API_URL, listValuePair);
            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {

                        Intent intent = new Intent(LoginActivity.this,
                                ForgotPassword.class);
                        intent.putExtra("forgetPasswordNumber",
                                forgotMobileNumber);
                        startActivity(intent);
                        // finish();
                    } else {
                        Constants.showOneButtonFailureDialog(
                                LoginActivity.this,
                                jsonObject.getString("description"), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(LoginActivity.this,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                e.printStackTrace();
                Constants.showOneButtonFailureDialog(LoginActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                e.printStackTrace();
                Constants.showOneButtonFailureDialog(LoginActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            dialog = new ProgressDialog(LoginActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            ForgetPasswordTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            ForgetPasswordTask.this.cancel(true);
            dialog.cancel();
        }

    }

    private boolean checkValidation(Context context) {
        boolean ret = true;

        if (!EditTextValidator.hasText(context, editMobileNumber,
                Constants.ERROR_MOBILE_BLANK_FIELD)) {
            ret = false;
            editMobileNumber.requestFocus();
            return ret;
        } else if (!EditTextValidator.isValidMobileNumber(context,
                editMobileNumber, Constants.ERROR_MOBILE_LENGTH_FIELD)) {
            ret = false;
            editMobileNumber.requestFocus();
            return ret;
        } else if (!EditTextValidator.hasText(context, editPin,
                Constants.ERROR_LOGIN_PIN_BLANK_FIELD)) {
            ret = false;
            editPin.requestFocus();
            return ret;
        }
        // else if (!EditTextValidator.isValidPin(context, editPin,
        // Constants.ERROR_LOGIN_PIN_VALID_FIELD)) {
        // ret = false;
        // editPin.requestFocus();
        // return ret;
        // }
        else
            return ret;
    }

    private void checkNotNull(Object reference, String name) {
        if (reference == null) {
            throw new NullPointerException("Errror");
        }
    }

    private void findViewById() {
        editMobileNumber = (EditText) findViewById(R.id.editMobileNumber);
        editPin = (EditText) findViewById(R.id.editPin);
        mBtnLogin = (Button) findViewById(R.id.mBtnLogin);
        mBtnRegister = (Button) findViewById(R.id.mBtnRegister);

        textForgetPin = (TextView) findViewById(R.id.textForgetPin);
    }

    public class LoginTask extends AsyncTask<String, String, String> implements
            OnDismissListener {

        @Override
        protected String doInBackground(String... params) {
            PackageInfo pinfo;
            int versionNumber = 0;
            String versionName = "";
            try {
                pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);

                versionNumber = pinfo.versionCode;
                versionName = pinfo.versionName;
            } catch (NameNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
            listValuePair.add(new BasicNameValuePair("method", "authenticate_new"));
            listValuePair
                    .add(new BasicNameValuePair("mobile", strMobileNumber));
            listValuePair.add(new BasicNameValuePair("password", strPin));
			/* listValuePair.add(new BasicNameValuePair("uuid","")); */
            listValuePair.add(new BasicNameValuePair("mode", "test"));
            //listValuePair.add(new BasicNameValuePair("type", "1"));
            listValuePair.add(new BasicNameValuePair("device_type", "android"));

                listValuePair.add(new BasicNameValuePair("uuid",Utility.getUUID(LoginActivity.this,
                        Constants.SHAREDPREFERENCE_UUID)));
                listValuePair.add(new BasicNameValuePair("device_id", Utility.getUUID(LoginActivity.this,
                        Constants.SHAREDPREFERENCE_UUID)));

            listValuePair.add(new BasicNameValuePair("longitude", Utility
                    .getLongitude(LoginActivity.this,
                            Constants.SHAREDPREFERENCE_LONGITUDE)));
            listValuePair.add(new BasicNameValuePair("latitude", Utility
                    .getLatitude(LoginActivity.this,
                            Constants.SHAREDPREFERENCE_LATITUDE)));
			/*
			 * listValuePair.add(new BasicNameValuePair("gcm_reg_id", Utility
			 * .getGCMID(LoginActivity.this,
			 * Constants.SHAREDPREFERENCE_GCMID)));
			 */
            listValuePair.add(new BasicNameValuePair("gcm_reg_id", Utility
                    .getSharedPreferences(LoginActivity.this,
                            Constants.SHAREDPREFERENCE_GCMID)));
            listValuePair.add(new BasicNameValuePair("version", Utility
                    .getOSVersion(LoginActivity.this,
                            Constants.SHAREDPREFERENCE_OS_VERSION)
                    + "_" + versionName));

            listValuePair.add(new BasicNameValuePair("version_code", String.valueOf(versionNumber)));
            listValuePair.add(new BasicNameValuePair("manufacturer", Utility
                    .getOSManufacturer(LoginActivity.this,
                            Constants.SHAREDPREFERENCE_OS_MANUFACTURER)));
            listValuePair.add(new BasicNameValuePair("location_src", provider));

            String response = RequestClass.getInstance()
                    .readPay1RequestForPlan(LoginActivity.this,
                            Constants.API_URL, listValuePair);

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d("Login  ", "Plan    " + result);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            try {
                Log.e("Login Response", result);
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");

                    JSONArray jsonArray = new JSONArray(replaced);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        Utility.setUserMobileNumber(LoginActivity.this, strMobileNumber);
                        Utility.setIsAppUsed(LoginActivity.this, true);
                        Utility.setLoginFlag(LoginActivity.this,
                                Constants.SHAREDPREFERENCE_IS_LOGIN, true);
                        JSONObject jsonObject2 = jsonObject
                                .getJSONObject("description");
                        String balance = jsonObject2.getString("balance")
                                .trim();
                        String passFlag = jsonObject.getString("passFlag");
                        String userName = jsonObject2.getString("shopname")
                                .trim();

						/*
						 * String trialFlag =
						 * jsonObject2.getString("trial_flag") .trim();
						 */
                        String createdOn = jsonObject2.getString("created")
                                .trim();
                        String distributorId = jsonObject2
                                .getString("parent_id");
                        if (jsonObject.has("OTA_Fee")) {
                            String OTA_Fee = jsonObject.getString("OTA_Fee");
                            Utility.setSharedPreference(LoginActivity.this,
                                    "OTA_Fee", OTA_Fee);
                        }
                        if (jsonObject.has("trial_period")) {
                            String trial_period = jsonObject
                                    .getString("trial_period");
                            Utility.setSharedPreference(LoginActivity.this,
                                    "trial_period", trial_period);
                        }
                        String retailer_id = jsonObject2.getString("id");
                        Utility.setRentalFlag(LoginActivity.this,
                                jsonObject2.getString("rental_flag"));
                        Utility.setBalance(LoginActivity.this,
                                Constants.SHAREDPREFERENCE_BALANCE, balance);
                        Utility.setUserName(LoginActivity.this,
                                Constants.SHAREDPREFERENCE_USER_NAME, userName);
                        Utility.setProfileID(LoginActivity.this,
                                Constants.SHAREDPREFERENCE_PROFILE_ID,
                                jsonObject.getString("profile_id"));
                        Utility.setUUID(LoginActivity.this,
                                Constants.SHAREDPREFERENCE_UUID,
                                jsonObject.getString("uuid"));
                        Utility.setMobileNumber(LoginActivity.this,
                                Constants.SHAREDPREFERENCE_MOBILE_NUMBER,
                                jsonObject2.getString("mobile"));
                        Utility.setLatitude(LoginActivity.this,
                                Constants.SHAREDPREFERENCE_LATITUDE,
                                jsonObject2.getString("latitude"));
                        Utility.setLongitude(LoginActivity.this,
                                Constants.SHAREDPREFERENCE_LONGITUDE,
                                jsonObject2.getString("longitude"));
                        Utility.setSharedPreference(LoginActivity.this,
                                "Distributor Id", distributorId);
                        Utility.setSharedPreference(LoginActivity.this,
                                Constants.SHAREDPREFERENCE_RETAILER_ID,
                                retailer_id);

                        Utility.setSharedPreference(LoginActivity.this,
                                Constants.SHAREDPREFERENCE_DEVICE_SERIAL_NO,
                                jsonObject2.getString("device_serial_no"));

						/*
						 * Utility.setSharedPreference(LoginActivity.this,
						 * Constants.SHAREDPREFERENCE_TRIAL_FLAG, trialFlag);
						 */
                        Utility.setSharedPreference(LoginActivity.this,
                                Constants.SHAREDPREFERENCE_CREATED_ON,
                                createdOn);

                        JSONArray vmns = jsonObject.getJSONArray("vmnList");

                        if (vmns.length() != 0) {
                            try {
                                vmnDataSource.open();
                                vmnDataSource.deleteVMN();
                                for (int i = 0, j = 1; i < vmns.length(); i++, j++) {
                                    vmnDataSource.createVMN(j,
                                            String.valueOf(vmns.get(i)));
                                }
                            } catch (SQLException exception) {
                                // TODO: handle exception
                                // //// Log.e("Er ", exception.getMessage());
                            } catch (Exception e) {
                                e.printStackTrace();
                                // //// Log.e("Er ", e.getMessage());
                            } finally {
                                vmnDataSource.close();
                            }
                        }

                        String pg_flag = jsonObject.getString("pg_flag");
                        Utility.setCookieName(
                                LoginActivity.this,
                                Constants.SHAREDPREFERENCE_COOKIE_NAME_RESPONSE,
                                "CAKEPHP");
                        Utility.setCookieValue(
                                LoginActivity.this,
                                Constants.SHAREDPREFERENCE_COOKIE_VALUE_RESPONSE,
                                jsonObject.getString("CAKEPHP"));
                        String service_charge = jsonObject
                                .getString("service_charge");
                        if (pg_flag.equalsIgnoreCase("1")) {
                            Utility.setServiceChargeForPG(LoginActivity.this,
                                    Integer.parseInt(service_charge));
                            Utility.setIsAbleForPG(LoginActivity.this, true);
                        } else {
                            Utility.setIsAbleForPG(LoginActivity.this, false);
                            Utility.setServiceChargeForPG(LoginActivity.this,
                                    Integer.parseInt(service_charge));
                        }

                        try {
                            Utility.setMaximumAmount(LoginActivity.this,
                                    jsonObject
                                            .getString("min_amount_for_prompt"));
                        } catch (Exception exception) {
                            Utility.setMaximumAmount(LoginActivity.this, "500");
                        }

                        try {
                            addressDataSource.open();

                            String add = jsonObject2.getString("address");
                            String area = jsonObject2.getString("area_name");
                            String city = jsonObject2.getString("city_name");
                            String state = jsonObject2.getString("state_name");
                            String lat = jsonObject2.getString("latitude");
                            String lng = jsonObject2.getString("longitude");
                            String zip = jsonObject2.getString("pin");
                            List<com.mindsarray.pay1.databasehandler.Address> addresses = addressDataSource
                                    .getAllAddress();
                            if (addresses.size() == 0) {
                                addressDataSource.createAddress(add, area,
                                        city, state, zip, lat, lng);
                            } else {
                                addressDataSource.updateAddress(add, area,
                                        city, state, zip, lat, lng, addresses
                                                .get(0).getAddressID());
                            }
                        } catch (SQLException exception) {
                            // TODO: handle exception
                            // //// Log.e("Er ", exception.getMessage());
                        } catch (Exception e) {
                            e.printStackTrace();
                            // //// Log.e("Er ", e.getMessage());
                        } finally {
                            addressDataSource.close();
                        }

                        if (intentClass != null
                                && intentClass.equals("PGActivity")) {
                            Intent intent = new Intent();
                            intent.setClass(LoginActivity.this,
                                    PGActivity.class);
                            startActivity(intent);
                            finish();
                        } else if (passFlag.equalsIgnoreCase("1")) {

                            Intent intent = new Intent();
                            if (getIntent().getExtras() == null) {

                            } else {
                                intent.putExtra(
                                        Constants.REQUEST_FOR,
                                        getIntent().getExtras().getBundle(
                                                Constants.REQUEST_FOR));
                                if (getIntent().getExtras().getString(
                                        "is_login") == null) {
                                    setResult(RESULT_OK, intent);
                                } else {

                                    if (getIntent().getExtras()
                                            .getString("is_login")
                                            .equals("login")) {
                                        intent.setClass(LoginActivity.this,
                                                MainActivity.class);
                                        startActivity(intent);
                                    } else {
                                        setResult(RESULT_OK, intent);
                                    }
                                }
                            }
                            intent.setClass(LoginActivity.this,
                                    MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(LoginActivity.this,
                                    SettingsUpdatePinActivity.class);
                            intent.putExtra(Constants.IS_RESET, true);
                            startActivity(intent);
                            finish();
                        }

                        // String LastUpdateTimeCircle = Utility
                        // .getSharedPreferences(LoginActivity.this,
                        // Constants.LAST_CIRCLE_UPDATED);
                        // if (LastUpdateTimeCircle.length() > 0) {
                        // int days = (int) ((System.currentTimeMillis() - Long
                        // .parseLong(LastUpdateTimeCircle)) / (1000 * 60 * 60 *
                        // 24));
                        //
                        // if (days >= 1) {
                        // Intent circleServiceIntent = new Intent(
                        // LoginActivity.this,
                        // CircleIntentService.class);
                        // startService(circleServiceIntent);
                        // }
                        // } else {
                        // Intent circleServiceIntent = new Intent(
                        // LoginActivity.this,
                        // CircleIntentService.class);
                        // startService(circleServiceIntent);
                        // }
                    } else if (status.equalsIgnoreCase("failure")) {
                        // if
                        // (jsonObject.getString("code").equalsIgnoreCase("47"))
                        // {
                        if (jsonObject.has("forced_upgrade_flag")) {
                            if (jsonObject.getString("forced_upgrade_flag")
                                    .equalsIgnoreCase("1")) {
                                Constants.showOneButtonFailureDialog(
                                        LoginActivity.this,
                                        jsonObject.getString("description"),
                                        "Update Available",
                                        Constants.LOGIN_APP_UPDATE_SETTINGS);
                            } else {
                                Constants.showOneButtonFailureDialog(
                                        LoginActivity.this,
                                        Constants.checkCode(replaced), TAG,
                                        Constants.OTHER_ERROR);
                            }
                        } else {
                            Constants.showOneButtonFailureDialog(
                                    LoginActivity.this,
                                    Constants.checkCode(replaced), TAG,
                                    Constants.OTHER_ERROR);
                        }
                    } else if (status.equalsIgnoreCase("successOTP")) {
                        Utility.setUserMobileNumber(LoginActivity.this, strMobileNumber);
                        Utility.setSharedPreference(LoginActivity.this, "UserPin", strPin);

                        JSONObject jsonObject1 = jsonObject.getJSONObject("description");
                        Constants.showOneButtonSuccessDialog(LoginActivity.this,
                                jsonObject1.getString("description"),
                                Constants.OTP_SETTINGS_LOGIIN);
                    } else {
                        Constants.showOneButtonFailureDialog(
                                LoginActivity.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(LoginActivity.this,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                e.printStackTrace();
                Constants.showOneButtonFailureDialog(LoginActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                e.printStackTrace();
                Constants.showOneButtonFailureDialog(LoginActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(LoginActivity.this);
            dialog.setMessage("Please wait.....");
            dialog.setCancelable(true);
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

                @Override
                public void onCancel(DialogInterface dialog) {
                    // TODO Auto-generated method stub
                    LoginTask.this.cancel(true);
                }
            });
            dialog.show();
            super.onPreExecute();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            LoginTask.this.cancel(true);
            dialog.cancel();
        }

    }

    private void writeToFile(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
                    openFileOutput("logFile.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e(TAG, "File write failed: " + e.toString());
        }

    }

    @Override
    protected void onDestroy() {
        if (dialog != null) {
            dialog.dismiss();
        }
        super.onDestroy();
        // Log.e("Login Activity", "Activity Destroyed");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.checkPlayServices(LoginActivity.this);
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mRegistrationBroadcastReceiver,
                new IntentFilter(Constants.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
