package com.mindsarray.pay1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class MoneyTransferSearchUserActivity extends Activity {
    Button buttonSearch, buttonAddNewUser;
    ListView listSearch;
    EditText editSearchUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.money_transfer_search_user);
        findViewById();

        buttonSearch.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (editSearchUser.getText().toString().length() > 0) {
                    new GetUsersTask().execute();
                }
            }
        });
    }

    public class GetUsersTask extends AsyncTask<String, String, String> {
        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(MoneyTransferSearchUserActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            GetUsersTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();
        }
    }

    private void findViewById() {
        // TODO Auto-generated method stub
        buttonSearch = (Button) findViewById(R.id.button_Search);
        buttonAddNewUser = (Button) findViewById(R.id.buttonAddNewUser);
        listSearch = (ListView) findViewById(R.id.listSearch);
        editSearchUser = (EditText) findViewById(R.id.editSearchUser);
    }

}
