package com.mindsarray.pay1;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PaymentActivity extends Activity {

    private static final String SCREEN_LABEL = "PG Screen";
    //	private EasyTracker easyTracker = null;
    public static GoogleAnalytics analytics;
    public static Tracker tracker;
    private WebView webView_PG;
    private String html = "";
    int rechargeType;
    String operatorName, mobileNumber, rechargeAmount, payOpid,
            specialRecharge, subsId, tax = "";

    @SuppressLint("NewApi")
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_layout);
        try {
//			easyTracker = EasyTracker.getInstance(PaymentActivity.this);
//			easyTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);
            tracker = Constants.setAnalyticsTracker(this, tracker);
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                html = bundle.getString(Constants.PG_DATA);

				/*
                 * mobileNumber = bundle.getString(Constants.MOBILE_NUMBER);
				 * rechargeAmount = bundle.getString(Constants.RECHARGE_AMOUNT);
				 * rechargeType = bundle.getInt(Constants.RECHARGE_FOR);
				 * operatorName = bundle.getString(Constants.OPERATOR_NAME); tax
				 * = bundle.getString("TAX"); if (rechargeType ==
				 * Constants.RECHARGE_DTH) subsId =
				 * bundle.getString(Constants.SUBSCRIBER_ID);
				 */
            }

            webView_PG = (WebView) findViewById(R.id.webView_PG);
            webView_PG.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    // TODO Auto-generated method stub
                    return true;
                }
            });

			/*
			 * html=Constants.loadJSONFromAsset(getApplicationContext(),
			 * "test.txt");
			 */
            webView_PG.setLongClickable(false);
            webView_PG.setBackgroundColor(0x00000000);
            if (Build.VERSION.SDK_INT >= 11)
                webView_PG.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

            webView_PG.getSettings().setJavaScriptEnabled(true);
            webView_PG.getSettings().setDomStorageEnabled(true);
            webView_PG.getSettings().setDatabaseEnabled(true);
            webView_PG.setWebViewClient(new MyWebClient());
            webView_PG.loadDataWithBaseURL(null, html, "text/html", "UTF-8",
                    null);

        } catch (Exception e) {
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
//		tracker.setScreenName(SCREEN_LABEL); 
        Constants.trackOnStart(tracker, SCREEN_LABEL);

    }

    @Override
    public void onStop() {
        super.onStop();
        // EasyTracker.getInstance(this).activityStop(this);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Constants.showNavigationBar(PaymentActivity.this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }

    class MyWebClient extends WebViewClient implements OnDismissListener {
        private ProgressDialog dialog;// = new ProgressDialog(PaymentActivity.this);
        Boolean loadingFinished = true;
        Boolean redirect = false;
        int count = 0;

        /*@Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler,
                                       SslError error) {
            handler.proceed(); // Ignore SSL certificate errors
        }*/


        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentActivity.this);
            builder.setMessage(R.string.notification_error_ssl_cert_invalid);
            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();
                }
            });
            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                }
            });
            final AlertDialog dialog = builder.create();
            dialog.show();
        }

        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            Log.e("Load Signup page", description);
            Toast.makeText(
                    PaymentActivity.this,
                    "Problem loading. Make sure internet connection is available.",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);

            if (this.dialog == null) {
                this.dialog = new ProgressDialog(PaymentActivity.this);
                this.dialog.setCancelable(false);
                this.dialog
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {

                            @Override
                            public void onCancel(DialogInterface dialog) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();
                                finish();
                            }
                        });
            }
            if (!this.dialog.isShowing()) {
                this.dialog.show();
            }
            count++;

            Log.e("Count", count + "");
            Log.e("loadingFinished", "" + loadingFinished);
            Log.e("redirect", "" + redirect);

            if (!redirect && !loadingFinished && this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

            loadingFinished = false;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (!loadingFinished) {
                redirect = true;
            }

            loadingFinished = false;
            view.loadUrl(url);

            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // String str=url.substring(url.lastIndexOf("/"));
            super.onPageFinished(view, url);

            Log.w("PG", url);
            view.loadUrl("javascript:console.log(document.body.getElementsByTagName('pre')[0].innerHTML);");
            if (url.contains("payu_status")) {
                String str = url.substring(url.lastIndexOf("/") + 1);
                Log.w("OUT", str);

                if (str.equalsIgnoreCase("success")) {

                    new CheckBalanceTask().execute();
                    Utility.setSharedPreference(PaymentActivity.this, "OTA_Fee", "");
                    Intent intent = new Intent(PaymentActivity.this, MainActivity.class);
                    intent.putExtra(Constants.PG_DATA, Constants.PG_SUCCESS);
                    startActivity(intent);
					/*Intent intent=new Intent(PaymentActivity.this,MainActivity.class);
					intent.putExtra(Constants.PG_DATA, Constants.PG_SUCCESS);
					startActivity(intent);*/
                } else if (str.equalsIgnoreCase("failure")) {
                    Intent i = getIntent();
                    setResult(RESULT_OK, i);

                    finish();
                } else if (str.equalsIgnoreCase("cancel")) {
					/*Constants
					.showOneButtonFailureDialog(
							PaymentActivity.this,
							"Transaction failed. Please try again.",
							"Failure", Constants.PG_SETTINGS);*/
                    finish();
                } else if (str.equalsIgnoreCase("timeout")) {
					/*Constants
					.showOneButtonFailureDialog(
							PaymentActivity.this,
							"Transaction failed. Please try again.",
							"Failure", Constants.PG_SETTINGS);*/
                    finish();
                }

                finish();
            }
            if (this.dialog.isShowing())
                this.dialog.dismiss();

            if (!redirect) {
                loadingFinished = true;
            }

            if (loadingFinished && !redirect) {

            } else {
                redirect = false;
            }
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            this.dialog.dismiss();
            finish();
        }
    }

    public class CheckBalanceTask extends AsyncTask<String, String, String> {
        //private ProgressDialog dialog = new ProgressDialog(PaymentActivity.this);
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method", "updateBal"));
            String response = RequestClass.getInstance().readPay1Request(
                    PaymentActivity.this, Constants.API_URL, listValuePair);
            listValuePair.clear();

            return response;

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");

                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    // {"status":"success","description":"0","login":0}
                    if (status.equalsIgnoreCase("success")) {
                        if (jsonObject.getString("login").equalsIgnoreCase("1")) {
                            Utility.setBalance(PaymentActivity.this,
                                    Constants.SHAREDPREFERENCE_BALANCE,
                                    jsonObject.getString("description"));
							/*Constants
									.showOneButtonSuccessDialog(
											PaymentActivity.this,
											"Transaction Successfull. Please Check your balance.",
											Constants.PG_SETTINGS);*/
                        } else {

                        }
                    }
                }

            } catch (JSONException e) {
                // e.printStackTrace();
				/*
				 * Constants.showOneButtonFailureDialog(SplashScreenActivity.this
				 * , Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
				 * Constants.OTHER_ERROR);
				 */

            } catch (Exception e) {
                // TODO: handle exception
				/*
				 * Constants.showOneButtonFailureDialog(SplashScreenActivity.this
				 * , Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
				 * Constants.OTHER_ERROR);
				 */

            }
        }

    }
}
