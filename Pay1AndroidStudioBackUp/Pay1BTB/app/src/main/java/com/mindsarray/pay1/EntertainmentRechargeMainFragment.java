package com.mindsarray.pay1;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.EntertainmentRechargeMainAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class EntertainmentRechargeMainFragment extends Fragment {

    View vi = null;
    ArrayList<String> productList = new ArrayList<String>();
    ListView listProduct;
    EntertainmentRechargeMainAdapter adapter;
    private static final String TAG = "Entertainment";
    public static GoogleAnalytics analytics;
    public static Tracker tracker;
    // String planJson;
    // JSONObject jsonObject2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


//		analytics = GoogleAnalytics.getInstance(getActivity()); tracker = Constants.setAnalyticsTracker(this, tracker);
//		//easyTracker.set(Fields.SCREEN_NAME, TAG);
        analytics = GoogleAnalytics.getInstance(getActivity());
        tracker = Constants.setAnalyticsTracker(getActivity(), tracker);
        vi = inflater.inflate(R.layout.entertainmentrechargemain_activity,
                container, false);

        adapter = new EntertainmentRechargeMainAdapter(getActivity(), 3,
                productList);
        listProduct = (ListView) vi.findViewById(R.id.listProduct);
        listProduct.setAdapter(adapter);

        if (productList.size() == 0) {
            new GetVASListTask().execute();
        }

        listProduct.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {
                Intent intent = new Intent(getActivity(),
                        EntertainmentRechargeActivity.class);
                try {
                    JSONObject jsonObject = new JSONObject(productList
                            .get(position));

                    intent.putExtra(Constants.PRODUCT_NAME, jsonObject
                            .getJSONObject("prods").getString("name"));
                    intent.putExtra(Constants.PRODUCT_ID, jsonObject
                            .getJSONObject("prods").getString("product_id"));
                    intent.putExtra(Constants.PRODUCT_DETAILS, jsonObject
                            .getJSONObject("prods").toString());

                } catch (JSONException e1) {
                    // e1.printStackTrace();
                }

                startActivity(intent);

            }
        });

        return vi;

    }

    //	public static GoogleAnalytics analytics; public static Tracker tracker;
    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
//			//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
        //tracker.setScreenName(TAG);
        Constants.trackOnStart(tracker, TAG);
        //.build());
    }

    class GetVASListTask extends AsyncTask<String, String, String> implements
            OnDismissListener {

        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair
                    .add(new BasicNameValuePair("method", "getVASProducts"));
            listValuePair.add(new BasicNameValuePair("device_type", "android"));
            String response = RequestClass.getInstance().readPay1Request(
                    getActivity(), Constants.API_URL, listValuePair);

            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            if (!result.startsWith("Error")) {
                String replaced = result.replace("(", "").replace(")", "")
                        .replace(";", "");
                Utility.setSharedPreference(getActivity(),
                        Constants.ENTERTAINMENT, replaced);

                try {
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);

                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray descArray = jsonObject
                                .getJSONArray("description");
                        for (int i = 0; i < descArray.length(); i++) {
                            JSONObject json_data = descArray.getJSONObject(i);
                            productList.add(json_data.toString());
                        }

                    } else {
                        Constants.showOneButtonFailureDialog(getActivity(),
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    // e.printStackTrace();
                    Constants.showOneButtonFailureDialog(getActivity(),
                            Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                            Constants.OTHER_ERROR);
                } catch (Exception e) {
                    // TODO: handle exception
                    Constants.showOneButtonFailureDialog(getActivity(),
                            Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                            Constants.OTHER_ERROR);
                }
            } else {
                String replaced = Utility.getSharedPreferences(getActivity(),
                        Constants.ENTERTAINMENT);
                try {

                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);

                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray descArray = jsonObject
                                .getJSONArray("description");
                        for (int i = 0; i < descArray.length(); i++) {
                            JSONObject json_data = descArray.getJSONObject(i);
                            productList.add(json_data.toString());
                        }

                    } else {
                        Constants.showOneButtonFailureDialog(getActivity(),
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    // e.printStackTrace();
                    Constants.showOneButtonFailureDialog(getActivity(),
                            Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                            Constants.OTHER_ERROR);
                } catch (Exception e) {
                    // TODO: handle exception
                    Constants.showOneButtonFailureDialog(getActivity(),
                            Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                            Constants.OTHER_ERROR);
                }
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(getActivity());
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            GetVASListTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            GetVASListTask.this.cancel(true);
            dialog.cancel();
        }

    }

    public static EntertainmentRechargeMainFragment newInstance(String content) {
        EntertainmentRechargeMainFragment fragment = new EntertainmentRechargeMainFragment();

        Bundle b = new Bundle();
        b.putString("msg", content);

        fragment.setArguments(b);

        return fragment;
    }

    // private String mContent = "???";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // if ((savedInstanceState != null)
        // && savedInstanceState.containsKey(KEY_CONTENT)) {
        // mContent = savedInstanceState.getString(KEY_CONTENT);
        // }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // outState.putString(KEY_CONTENT, mContent);
    }

}
