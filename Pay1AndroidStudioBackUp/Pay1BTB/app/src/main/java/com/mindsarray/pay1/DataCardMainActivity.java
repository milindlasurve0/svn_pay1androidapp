package com.mindsarray.pay1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.RechargeMainAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.utilities.GridMenuItem;

import java.util.ArrayList;

public class DataCardMainActivity extends Activity {
    GridView gridView;
    //	public static GoogleAnalytics analytics; public static Tracker tracker;
    public static GoogleAnalytics analytics;
    public static Tracker tracker;
    ArrayList<GridMenuItem> gridArray = new ArrayList<GridMenuItem>();
    RechargeMainAdapter customGridAdapter;

    // String planJson;
    // JSONObject jsonObject2;
    private static final String TAG = "Data Card Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        // WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.rechargemain_activity);

        int mAirtel = R.drawable.m_airtel;
        int mVodafone = R.drawable.m_vodafone;
        int mBsnl = R.drawable.m_bsnl;
        int mIdea = R.drawable.m_idea;
        int mAircel = R.drawable.m_aircel;
        int mRelainceCdma = R.drawable.m_reliance_cdma;
        int mRelainceGsm = R.drawable.m_reliance;
        int mTataDocomo = R.drawable.m_docomo;
        int mTataIndicom = R.drawable.m_indicom;
        int mMTS = R.drawable.m_mts;
        int mMTNL = R.drawable.m_mtnl;

        gridArray.add(new GridMenuItem(mAirtel, "Airtel",
                Constants.OPERATOR_AIRTEL, Constants.RECHARGE_MOBILE));
        gridArray.add(new GridMenuItem(mVodafone, "Vodafone",
                Constants.OPERATOR_VODAFONE, Constants.RECHARGE_MOBILE));
        gridArray.add(new GridMenuItem(mBsnl, "BSNL", Constants.OPERATOR_BSNL,
                Constants.RECHARGE_MOBILE));
        gridArray.add(new GridMenuItem(mIdea, "Idea", Constants.OPERATOR_IDEA,
                Constants.RECHARGE_MOBILE));
        gridArray.add(new GridMenuItem(mAircel, "Aircel",
                Constants.OPERATOR_AIRCEL, Constants.RECHARGE_MOBILE));
        gridArray.add(new GridMenuItem(mRelainceCdma, "Reliance Netconnect+",
                Constants.OPERATOR_RELIANCE_CDMA, Constants.RECHARGE_MOBILE));
        gridArray.add(new GridMenuItem(mRelainceGsm, "Reliance GSM",
                Constants.OPERATOR_RELIANCE_GSM, Constants.RECHARGE_MOBILE));
        gridArray.add(new GridMenuItem(mTataDocomo, "Tata Docomo",
                Constants.OPERATOR_DOCOMO, Constants.RECHARGE_MOBILE));
        gridArray.add(new GridMenuItem(mTataIndicom, "Tata Photon",
                Constants.OPERATOR_TATA_INDICOM, Constants.RECHARGE_MOBILE));
        gridArray.add(new GridMenuItem(mMTS, "MTS", Constants.OPERATOR_MTS,
                Constants.RECHARGE_MOBILE));
        gridArray.add(new GridMenuItem(mMTNL, "MTNL", Constants.OPERATOR_MTNL,
                Constants.RECHARGE_MOBILE));

        gridView = (GridView) findViewById(R.id.gridView1);
        customGridAdapter = new RechargeMainAdapter(this,
                R.layout.rechargemain_adapter, gridArray, false);
//		tracker = Constants.setAnalyticsTracker(this, tracker);
//		//easyTracker.set(Fields.SCREEN_NAME, TAG);
        //analytics = GoogleAnalytics.getInstance(this);
        tracker = Constants.setAnalyticsTracker(this, tracker);
        // planJson = Constants.loadJSONFromAsset(getApplicationContext(),
        // "plans.json");
        gridView.setAdapter(customGridAdapter);

        gridView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {
                // Constants.showCustomToast(MobileRechargeActivity.this,
                // gridArray.get(position).getId()+"",
                // Toast.LENGTH_LONG).show();
                try {
                    /*
					 * JSONObject jsonObject=new JSONObject(planJson);
					 * jsonObject2
					 * =jsonObject.getJSONObject(String.valueOf(gridArray
					 * .get(position).getId()));
					 */
                    Intent intent = new Intent(DataCardMainActivity.this,
                            DataCardActivity.class);
                    intent.putExtra(Constants.OPERATOR_ID,
                            gridArray.get(position).getId());
					/*
					 * intent.putExtra(Constants.PLAN_JSON,
					 * jsonObject2.toString());
					 */
                    intent.putExtra(Constants.PLAN_JSON,
                            String.valueOf(gridArray.get(position).getId()));
                    intent.putExtra(Constants.OPERATOR_LOGO,
                            gridArray.get(position).getImage());
                    intent.putExtra(Constants.OPERATOR_NAME,
                            gridArray.get(position).getTitle());
                    startActivity(intent);

                } catch (Exception e) {
                    // e.printStackTrace();
                }

            }

        });

    }

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
//		//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
        //tracker.setScreenName(TAG);
        Constants.trackOnStart(tracker, TAG);
        //.build());
    }
}
