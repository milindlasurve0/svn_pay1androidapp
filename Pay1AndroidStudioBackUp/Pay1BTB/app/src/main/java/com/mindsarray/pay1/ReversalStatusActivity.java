package com.mindsarray.pay1;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.StatusAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class ReversalStatusActivity extends Activity {
    RadioGroup radioTypeGroup;
    RadioButton radioButton;
    String mRadioTag;
    private TextView mDateDisplay;
    private Button btnCalender;
    private int mYear;
    private int mMonth;
    private int mDay;
    StringBuilder builder;
    ListView listReversalStatus;
    static final int DATE_DIALOG_ID = 0;
    ArrayList<String> statusList;
    StatusAdapter adapter;
    String serviceType = "1";
    int requestFor = 1;
    TextView textMobile, textDTH, textEntertainment, textMobileBill;
    private static final String TAG = "Reversal status";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reversalstatus_activity);
        tracker = Constants.setAnalyticsTracker(this, tracker);
        if (Utility.getLoginFlag(ReversalStatusActivity.this, Constants.SHAREDPREFERENCE_IS_LOGIN)) {
            inSideOnCreate();

        } else {
            Intent intent = new Intent(ReversalStatusActivity.this,
                    LoginActivity.class);
            // intent.putExtra(Constants.REQUEST_FOR, bundle);
            startActivityForResult(intent, Constants.LOGIN);

        }

    }

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        //tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.LOGIN) {
                /*
				 * bundle=data.getExtras().getBundle(Constants.REQUEST_FOR);
				 * bundle.getInt(Constants.REQUEST_FOR);
				 */
                inSideOnCreate();
            } else {

            }
        } else {

        }
    }

    private void inSideOnCreate() {
        // TODO Auto-generated method stub
        findViewById();
        textMobile.setTextColor(getResources().getColor(R.color.white));
        btnCalender.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        updateDisplay();

        textMobile = (TextView) findViewById(R.id.textMobile);
        textDTH = (TextView) findViewById(R.id.textDTH);
        textEntertainment = (TextView) findViewById(R.id.textEntertainment);
        textMobileBill = (TextView) findViewById(R.id.textMobileBill);

        textMobile.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                statusList.clear();
                textMobile.setTextColor(getResources().getColor(R.color.white));
                textDTH.setTextColor(getResources().getColor(R.color.black));
                textEntertainment.setTextColor(getResources().getColor(
                        R.color.black));
                textMobileBill.setTextColor(getResources().getColor(
                        R.color.black));
                serviceType = "1";
                requestFor = 1;
                new ReversalStatusTask().execute();
            }
        });

        textDTH.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                statusList.clear();
                // TODO Auto-generated method stub
                textMobile.setTextColor(getResources().getColor(R.color.black));
                textDTH.setTextColor(getResources().getColor(R.color.white));
                textEntertainment.setTextColor(getResources().getColor(
                        R.color.black));
                textMobileBill.setTextColor(getResources().getColor(
                        R.color.black));
                serviceType = "2";
                requestFor = 2;
                new ReversalStatusTask().execute();

            }
        });

        textEntertainment.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                statusList.clear();
                // TODO Auto-generated method stub
                textMobile.setTextColor(getResources().getColor(R.color.black));
                textDTH.setTextColor(getResources().getColor(R.color.black));
                textEntertainment.setTextColor(getResources().getColor(
                        R.color.white));
                textMobileBill.setTextColor(getResources().getColor(
                        R.color.black));
                serviceType = "3";
                requestFor = 3;
                new ReversalStatusTask().execute();
            }
        });
        textMobileBill.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                statusList.clear();
                // TODO Auto-generated method stub
                textMobile.setTextColor(getResources().getColor(R.color.black));
                textDTH.setTextColor(getResources().getColor(R.color.black));
                textEntertainment.setTextColor(getResources().getColor(
                        R.color.black));
                textMobileBill.setTextColor(getResources().getColor(
                        R.color.white));
                serviceType = "4";
                requestFor = 4;
                new ReversalStatusTask().execute();
            }
        });

        statusList = new ArrayList<String>();
        adapter = new StatusAdapter(this, statusList, requestFor);
        listReversalStatus.setAdapter(adapter);
    }

    private void updateDisplay() {
        builder = new StringBuilder().append(mYear).append("-")
                .append(mMonth + 1).append("-").append(mDay);
        mDateDisplay.setText(builder.toString());
        new ReversalStatusTask().execute();
    }

    private void findViewById() {
        // TODO Auto-generated method stub
        btnCalender = (Button) findViewById(R.id.btnCalender);
        mDateDisplay = (TextView) findViewById(R.id.mDateDisplay);
        listReversalStatus = (ListView) findViewById(R.id.listReversalStatus);
        textMobile = (TextView) findViewById(R.id.textMobile);
        textDTH = (TextView) findViewById(R.id.textDTH);
        textEntertainment = (TextView) findViewById(R.id.textEntertainment);
    }

    public class ReversalStatusTask extends AsyncTask<String, String, String>
            implements OnDismissListener {

        ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method",
                    "reversalTransactions"));
            listValuePair
                    .add(new BasicNameValuePair("date", builder.toString()));
            listValuePair.add(new BasicNameValuePair("device_type", "android"));
            listValuePair.add(new BasicNameValuePair("service", serviceType));
            String response = RequestClass.getInstance().readPay1Request(
                    ReversalStatusActivity.this, Constants.API_URL,
                    listValuePair);
            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            super.onPostExecute(result);
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    statusList.clear();
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray jsonDescription = jsonObject
                                .getJSONArray("description");
                        JSONArray array2 = jsonDescription.getJSONArray(0);
                        for (int i = 0; i < array2.length(); i++) {
                            JSONObject json_data = array2.getJSONObject(i);
                            statusList.add(json_data.toString());

                        }
                        // Log.d("Arra", "Arra");
                        adapter.notifyDataSetChanged();
                    } else {
                        Constants.showOneButtonFailureDialog(
                                ReversalStatusActivity.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(
                            ReversalStatusActivity.this,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(
                        ReversalStatusActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(
                        ReversalStatusActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(ReversalStatusActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            ReversalStatusTask.this.cancel(true);
                            dialog.cancel();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            ReversalStatusTask.this.cancel(true);
            dialog.cancel();
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                DatePickerDialog dialog = new DatePickerDialog(this,
                        mDateSetListener, mYear, mMonth, mDay);

			/*
			 * if (Build.VERSION.SDK_INT >= 11) { // (picker is a DatePicker)
			 * dialog.getDatePicker().setMaxDate(new Date().getTime()); } else {
			 * 
			 * }
			 */

                return dialog;
			/*
			 * return new DatePickerDialog(this, mDateSetListener, mYear,
			 * mMonth, mDay);
			 */
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            mYear = year;
            mMonth = monthOfYear;
            mDay = dayOfMonth;
            updateDisplay();
        }
    };

}
