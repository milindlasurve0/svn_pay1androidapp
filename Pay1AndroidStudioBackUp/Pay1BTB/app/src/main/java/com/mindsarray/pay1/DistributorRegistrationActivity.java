package com.mindsarray.pay1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.contenthandler.Group;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.EditTextValidator;
import com.mindsarray.pay1.utilities.GooglePlayServicesApiClient;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Administrator on 6/21/2016.
 */
public class DistributorRegistrationActivity extends Activity {
    private static final String TAG = "Registration";
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    public static GoogleAnalytics analytics;
    public static Tracker tracker;
    EditText editName, editShopName, editEmail, editMobile, editArea, editCity, editState,
            editPincode, editCurrentDistribution, editEmpSize;
    Button mBtnRegister;

    ImageView imageViewClose;
    SparseArray<Group> mGroups = new SparseArray<Group>();
    Spinner areaOfBusinessSpinner;
    ExpandableListView mListView;
    CreateRetailerTask mCreateRetailerTask;

    GooglePlayServicesApiClient mGooglePlayServicesApiClient;
    private GoogleApiClient mGoogleApiClient;
    private SpinnerTextView spinnerTextView;
    private boolean mResolvingError = false;


    private Context mContext = DistributorRegistrationActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.distributor_registration_activity);


        Constants.promptForGPS((Activity) mContext);
        mGooglePlayServicesApiClient = new GooglePlayServicesApiClient(mContext, LocationServices.API, true);
        mGoogleApiClient = mGooglePlayServicesApiClient.mGoogleApiClient;


        findViewById();
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getIntent().getExtras() != null) {
            editMobile.setText(getIntent().getStringExtra("MobileNumber").toString());
        }
        tracker = Constants.setAnalyticsTracker(this, tracker);

        String[] natureOfBusinessArray = new String[]{"--Select nature of business--", "Mobile" +
                " Store", "Stationary Shop",
                "Medical Store",
                "Grocery" +
                        " Store", "Photocopy Shop", "Hardware Store", "Travel Agency", "Other"};


        String[] areaOfBusinessArray = new String[]{"--Select area of business--",
                "Residential Area", "Industrial Area",
                "Commercial Area"};

        ArrayAdapter<String> areaOfBusinessAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item_layout, areaOfBusinessArray);

        areaOfBusinessSpinner.setAdapter(areaOfBusinessAdapter);

        areaOfBusinessSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Log.v("item", (String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });


        /*editName.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                // TODO Auto-generated method stub
                EditTextValidator.hasText(DistributorRegistrationActivity.this,
                        editName, Constants.ERROR_NAME_BLANK_FIELD);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        editShopName.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                // TODO Auto-generated method stub
                EditTextValidator.hasText(DistributorRegistrationActivity.this,
                        editShopName, Constants.ERROR_NAME_BLANK_FIELD);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        editEmail.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                // TODO Auto-generated method stub
                EditTextValidator.isEmailAddress(DistributorRegistrationActivity.this,
                        editEmail, Constants.ERROR_EMAIL_BLANK_FIELD);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        editMobile.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                // TODO Auto-generated method stub
                EditTextValidator.hasText(DistributorRegistrationActivity.this,
                        editMobile, Constants.ERROR_MOBILE_BLANK_FIELD);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        editCity.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                // TODO Auto-generated method stub
                EditTextValidator.hasText(DistributorRegistrationActivity.this,
                        editCity, Constants.ERROR_CITY_BLANK_FIELD);

//					String query = editCity.getText().toString().trim();
//					mPlaceArrayAdapter.getFilter().filter(query);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });*/


        mBtnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editName.getText().toString().trim().length() != 0) {

                } else {
                    setErrorToEditText(mContext, "Please enter your name", editName);
                    return;
                }

                if (editMobile.getText().toString().trim().length() != 0) {

                } else {
                    setErrorToEditText(mContext, "Please enter mobile", editMobile);
                    return;
                }

                if (editMobile.getText().toString().matches("[7-9][0-9]{9}$")) {

                } else {
                    setErrorToEditText(mContext, "Please enter correct mobile", editMobile);
                    return;
                }

                if (editEmail.getText().toString().trim().length() != 0) {

                } else {
                    setErrorToEditText(mContext, "Please enter email", editEmail);
                    return;
                }

                if (EditTextValidator.isEmailAddress(DistributorRegistrationActivity.this,
                        editEmail, Constants.ERROR_EMAIL_BLANK_FIELD)) {

                } else {
                    setErrorToEditText(mContext, "Please enter correct email", editEmail);
                    return;
                }


                if (editCity.getText().toString().trim().length() != 0) {

                } else {
                    setErrorToEditText(mContext, "Please enter address", editCity);
                    return;
                }

                if (editArea.getText().toString().trim().length() != 0) {

                } else {
                    setErrorToEditText(mContext, "Please enter area", editArea);
                    return;
                }

                if (editPincode.getText().toString().trim().length() != 0) {

                } else {
                    setErrorToEditText(mContext, "Please enter pincode", editPincode);
                    return;
                }

                if (editPincode.getText().toString().trim().length() == 6) {

                } else {
                    setErrorToEditText(mContext, "Please enter correct pincode", editPincode);
                    return;
                }

                if (editShopName.getText().toString().trim().length() != 0) {

                } else {
                    setErrorToEditText(mContext, "Please enter shopName", editShopName);
                    return;
                }

                if (editCurrentDistribution.getText().toString().trim().length() != 0) {

                } else {
                    setErrorToEditText(mContext, "Please enter current distribution",
                            editCurrentDistribution);
                    return;
                }

                if (editEmpSize.getText().toString().trim().length() != 0) {

                } else {
                    setErrorToEditText(mContext, "Please enter employee number", editEmpSize);
                    return;
                }

                //if(areaOfBusinessSpinner.getSelectedItem())
                if (areaOfBusinessSpinner.getSelectedItemPosition() > 0) {

                } else {
                    Toast.makeText(mContext, "Please select Type of Business", Toast.LENGTH_LONG)
                            .show();
                    return;
                }
                mCreateRetailerTask = new CreateRetailerTask();
                mCreateRetailerTask.execute();
            }
        });
    }

    private void setErrorToEditText(Context context, String errorMessage, EditText editText) {
        int ecolor = R.color.black;
        ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
                errorMessage);
        ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
        editText.setError(ssbuilder);
        editText.setBackgroundDrawable(context.getResources().getDrawable(
                R.drawable.edittext_error_background_selector));
        editText.setPadding(10, 10, 10, 10);
        editText.setTextSize(14);
        editText.requestFocus();
    }

    private void findViewById() {
        editName = (EditText) findViewById(R.id.editName);
        editShopName = (EditText) findViewById(R.id.editShopName);
        editEmail = (EditText) findViewById(R.id.editEmail);
        editMobile = (EditText) findViewById(R.id.editMobile);

        editArea = (EditText) findViewById(R.id.editArea);
        editCity = (EditText) findViewById(R.id.editCity);
        editState = (EditText) findViewById(R.id.editState);
        editPincode = (EditText) findViewById(R.id.editPincode);


        editCurrentDistribution = (EditText) findViewById(R.id.editCurrentDistribution);
        editEmpSize = (EditText) findViewById(R.id.editEmpSize);
        mBtnRegister = (Button) findViewById(R.id.mBtnRegister);
        areaOfBusinessSpinner = (Spinner) findViewById(R.id.areaOfBusinessSpinner);

        imageViewClose = (ImageView) findViewById(R.id.imageViewClose);
        //mBtnCancel = (Button) findViewById(R.id.mBtnCancel);
    }


    public class CreateRetailerTask extends AsyncTask<String, String, String>
            implements DialogInterface.OnDismissListener {
        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
            listValuePair.add(new BasicNameValuePair("method", "createRetDistLeads"));
            listValuePair.add(new BasicNameValuePair("r_n", editName
                    .getText().toString().trim()));
            listValuePair.add(new BasicNameValuePair("r_s_n", editShopName
                    .getText().toString().trim()));
            listValuePair.add(new BasicNameValuePair("r_e", editEmail
                    .getText().toString().trim()));
            listValuePair.add(new BasicNameValuePair("r_m", editMobile
                    .getText().toString().trim()));
            listValuePair.add(new BasicNameValuePair("r_a_r", editArea.getText()
                    .toString().trim()));
            listValuePair.add(new BasicNameValuePair("r_a_d", editCity.getText()
                    .toString().trim()));

            listValuePair.add(new BasicNameValuePair("r_p", editPincode.getText()
                    .toString().trim()));

            listValuePair.add(new BasicNameValuePair("d_e_l", editEmpSize.getText()
                    .toString().trim()));

            listValuePair.add(new BasicNameValuePair("d_c_d", editCurrentDistribution.getText()
                    .toString().trim()));
//bbusiness nature
            listValuePair.add(new BasicNameValuePair("r_b_n", areaOfBusinessSpinner.getSelectedItem()
                    .toString()));


            listValuePair.add(new BasicNameValuePair("req_by", "android"));
            listValuePair.add(new BasicNameValuePair("reg_i", "Distributor"));

            String response = RequestClass.getInstance()
                    .readPay1Request(DistributorRegistrationActivity.this,
                            Constants.API_URL, listValuePair);

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");

                    JSONArray jsonArray = new JSONArray(replaced);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        Intent intent = new Intent(DistributorRegistrationActivity.this,
                                LeadGenerationOtpActivity.class);
                        intent.putExtra("MobileNumber", editMobile.getText().toString().trim());
                        startActivity(intent);

                        /*{
                        if (Utility.getSharedPreferences(RegistrationActivity.this, "Registration_interest") == "Retailer") {
                            startActivity(new Intent(RegistrationActivity.this, RegistrationPaymentOptionActivity.class));
                        } else if (Utility.getSharedPreferences(RegistrationActivity.this, "Distributor_interest") == "Retailer") {
                            startActivity(new Intent(RegistrationActivity.this, RegistrationPaymentOptionActivity.class));
                        } else if (Utility.getSharedPreferences(RegistrationActivity.this, "Distributor_interest") == "Distributor") {
                            startActivity(new Intent(RegistrationActivity.this, DistributorInterestActivity.class));
                        }
                    }*/
                    } else {
                        Constants.showOneButtonFailureDialog(
                                DistributorRegistrationActivity.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(
                            DistributorRegistrationActivity.this,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(DistributorRegistrationActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(DistributorRegistrationActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(DistributorRegistrationActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            CreateRetailerTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            CreateRetailerTask.this.cancel(true);
            dialog.cancel();
        }

    }


}
