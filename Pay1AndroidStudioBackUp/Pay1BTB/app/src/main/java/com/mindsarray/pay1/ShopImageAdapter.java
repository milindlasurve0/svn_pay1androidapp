package com.mindsarray.pay1;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mindsarray.pay1.customviews.CustomImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;

public class ShopImageAdapter extends PagerAdapter {
    Context context;

    ArrayList<String> data;
    LayoutInflater inflater;
    int requestFrom;

    public ShopImageAdapter(Context context, ArrayList<String> drawables,
                            int requestFrom) {
        this.context = context;
        this.data = drawables;
        this.requestFrom = requestFrom;
    }

    @Override
    public int getCount() {
        return data.size();
    }

	/*
     * @Override public boolean isViewFromObject(View view, Object object) {
	 * return view == ((LinearLayout) object); }
	 */

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.slide_image_activity,
                container, false);

        CustomImageView imageView = (CustomImageView) itemView.findViewById(R.id.imageView);
        TextView textViewCaption = (TextView) itemView
                .findViewById(R.id.textViewCaption);
        // imageView.setImageDrawable(context.getResources().getDrawable(drawables.get(position)));
        if (requestFrom == 0) {
            try {

                //JSONObject jsonObject = new JSONObject(data.get(position));
                //textViewCaption.setText(jsonObject.getString("caption"));
                Picasso.with(context).load(data.get(position))

                        .placeholder(R.drawable.default_cover_wholesaler).into(imageView);

            } catch (Exception e) {
            }
        } else {
            try {

                JSONObject jsonObject = new JSONObject(data.get(position));
                textViewCaption.setText(jsonObject.getString("caption"));
                Picasso.with(context).load(jsonObject.getString("filename"))

                        .placeholder(R.drawable.default_cover_wholesaler).into(imageView);

            } catch (Exception e) {
            }
        }
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
    }

}
