package com.mindsarray.pay1;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.EditTextValidator;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DthRechargeActivity extends Activity {

    ImageView mImageOprLogo;
    TextView mTextOperatorName, textBalance, mLastRecharge, textViewHintSubId;
    Button mBtnRchNow, btnPlans;
    int mOperatorId;
    EditText editMobileNumber, editRechargeAmount, editSubscriberNumber;
    Bundle bundle;
    boolean bNetOrSms;
    Context mContext;
    String mobileNumber, amount, subId, planJson;
    String uuid;
    private SharedPreferences mSettingsData;// Shared preferences for setting
    String messagePrefix = ""; // data
    long timestamp;
    private static final String TAG = "DTH recharge";
    int subIdLen;
    String prefix;
    // NotificationDataSource notificationDataSource;
    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        // WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.dthrecharge_activity);
        findViewById();
        /*
		 * notificationDataSource = new NotificationDataSource(
		 * DthRechargeActivity.this);
		 */
        mContext = DthRechargeActivity.this;
        timestamp = System.currentTimeMillis();
        // tracker = Constants.setAnalyticsTracker(this, tracker);

        // analytics = GoogleAnalytics.getInstance(this);
        tracker = Constants.setAnalyticsTracker(this, tracker);
		/*AnalyticsApplication application = (AnalyticsApplication) getApplication();
		tracker = application.getDefaultTracker();*/
        try {
            TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            uuid = tManager.getDeviceId();
            if (uuid == null) {
                uuid = Secure.getString(
                        DthRechargeActivity.this.getContentResolver(),
                        Secure.ANDROID_ID);
            }
            if (Utility.getUUID(DthRechargeActivity.this,
                    Constants.SHAREDPREFERENCE_UUID) == null)
                Utility.setUUID(DthRechargeActivity.this,
                        Constants.SHAREDPREFERENCE_UUID, uuid);
        } catch (Exception exception) {
        }

        LinearLayout adLayout = (LinearLayout) findViewById(R.id.adLayout);
        String notificationText = Utility.getSharedPreferences(
                DthRechargeActivity.this, "notification");
        // String notificationText="";
        if (notificationText.equals("") || notificationText.length() == 0) {
            adLayout.setVisibility(View.GONE);
        } else {
            adLayout.setVisibility(View.VISIBLE);
            String html = "<html><body height:50px  bgcolor='#26a9e0'><marquee><div style='color:#1b1b1b; padding: 10px'>"
                    + notificationText + "</div></marquee></body></html>";

            WebView myWebView = (WebView) this.findViewById(R.id.myWebView);
            myWebView.getSettings().setJavaScriptEnabled(true);

            myWebView.loadData(html, "text/html", null);
        }

        bundle = getIntent().getExtras();
        mOperatorId = bundle.getInt(Constants.OPERATOR_ID);
        //setHintForSubId(mOperatorId);
        planJson = bundle.getString(Constants.PLAN_JSON);
        mSettingsData = getSharedPreferences(SettingsSmsActivity.SETTINGS_NAME,
                0);
        bNetOrSms = mSettingsData.getBoolean(SettingsSmsActivity.KEY_NETWORK,
                true);

        mImageOprLogo.setImageResource(getIntent().getIntExtra(
                Constants.OPERATOR_LOGO, 0));
        mTextOperatorName.setText(bundle.getString(Constants.OPERATOR_NAME));

        switch (mOperatorId) {
            case 16:
                editSubscriberNumber.setHint("Customer ID");
                break;
            case 17:
                editSubscriberNumber.setHint("Smart Card No.");
                break;
            case 18:
                editSubscriberNumber.setHint("Viewing Card No.");
                break;
            case 19:
                editSubscriberNumber.setHint("Smart Card No.");
                break;
            case 20:
                editSubscriberNumber.setHint("Subscriber ID");
                break;
            case 21:
                editSubscriberNumber.setHint("Customer ID");
                break;
        }

        btnPlans.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(DthRechargeActivity.this,
                        PlanFragmentActivity.class);
                intent.putExtra(Constants.PLAN_JSON, planJson);
                intent.putExtra("CIRCLE_CODE", "all");
                intent.putExtra("DATA_CARD_PLAN", 0);
                startActivityForResult(intent, Constants.PLAN_REQUEST);
                // Constants.overridePendingTransitionActivity(
                // RechargeActivity.this, 4);
            }
        });

        mBtnRchNow.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                bNetOrSms = mSettingsData.getBoolean(
                        SettingsSmsActivity.KEY_NETWORK, true);

                mobileNumber = editMobileNumber.getText().toString();
                amount = editRechargeAmount.getText().toString();
                subId = editSubscriberNumber.getText().toString();
                checkSubIdValidation(subId, mOperatorId);
                if (!checkValidation(DthRechargeActivity.this)) {

                } else {

                    if (bNetOrSms) {

                        if (Utility.getLoginFlag(DthRechargeActivity.this,
                                Constants.SHAREDPREFERENCE_IS_LOGIN)) {

                            new AlertDialog.Builder(DthRechargeActivity.this)
                                    .setTitle("Recharge")
                                    .setMessage(
                                            "Operator: "
                                                    + mTextOperatorName
                                                    .getText()
                                                    + "\nSubscriberId: "
                                                    + subId
                                                    + "\nMobile Number: "
                                                    + mobileNumber
                                                    + "\nAmount: "
                                                    + amount
                                                    + "\nAre you sure you want to Recharge?")
                                    .setPositiveButton(
                                            "Yes",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(
                                                        DialogInterface dialog,
                                                        int which) {
                                                    dialog.dismiss();
                                                    new RechargeTask()
                                                            .execute();
                                                }
                                            })
                                    .setNegativeButton(
                                            "No",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(
                                                        DialogInterface dialog,
                                                        int which) {
                                                    dialog.dismiss();
                                                }
                                            }).show();
                        } else {
                            Intent intent = new Intent(
                                    DthRechargeActivity.this,
                                    LoginActivity.class);
                            startActivity(intent);
                        }
                    } else {

                        new AlertDialog.Builder(DthRechargeActivity.this)
                                .setTitle("Recharge (SMS)")
                                .setMessage(
                                        "Operator: "
                                                + mTextOperatorName.getText()
                                                + "\nSubscriberId: "
                                                + subId
                                                + "\nMobile Number: "
                                                + mobileNumber
                                                + "\nAmount: "
                                                + amount
                                                + "\nAre you sure you want to Recharge?")
                                .setPositiveButton("Yes",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int which) {
                                                dialog.dismiss();
                                                Constants.sendSMSMessage(
                                                        mContext,
                                                        Constants.RECHARGE_DTH,
                                                        mOperatorId,
                                                        subId,
                                                        mobileNumber,
                                                        amount,
                                                        "0",
                                                        bundle.getString(Constants.OPERATOR_NAME));
                                            }
                                        })
                                .setNegativeButton("No",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int which) {
                                                dialog.dismiss();
                                            }
                                        }).show();

                    }

                }

            }

        });

        editMobileNumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
                EditTextValidator.hasText(DthRechargeActivity.this,
                        editMobileNumber, Constants.ERROR_MOBILE_BLANK_FIELD);
                if (editMobileNumber.length() == 10) {
                    editRechargeAmount.requestFocus();
                    // new
                    // GetLastRechargeTask().execute(editMobileNumber.getText().toString());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        editRechargeAmount.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                EditTextValidator.hasText(DthRechargeActivity.this,
                        editRechargeAmount, Constants.ERROR_AMOUNT_BLANK_FIELD);
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });

        editSubscriberNumber.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                EditTextValidator.hasText(DthRechargeActivity.this,
                        editSubscriberNumber,
                        Constants.ERROR_SUB_ID_BLANK_FIELD);
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });

    }

    private void setHintForSubId(int mOperatorId2) {
        // TODO Auto-generated method stub
        switch (mOperatorId2) {
            case 16:
                textViewHintSubId.setText("Sub Id should start with 30 & Sub Id should should be 10 digit long");
                break;
            case 17:
                textViewHintSubId.setText("Sub Id should start with 20 & Sub Id should should be 12 digit long");
                break;
            case 18:
                textViewHintSubId.setText("Sub Id should start with 015 & Sub Id should should be 11 digit long");
                break;
            case 19:
                textViewHintSubId.setText("Sub Id should start with 4 & Sub Id should should be 11 digit long");
                break;
            case 20:
                textViewHintSubId.setText("Sub Id should start with 10 & Sub Id should should be 10 digit long");
                break;
            case 21:
                textViewHintSubId.setText("Sub Id should be Minmum 6 & Maximum 9 Digit");
                break;

            default:

                break;
        }
    }

    // public static GoogleAnalytics analytics; public static Tracker tracker;
    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        // //tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
        // tracker.setScreenName(TAG);
        Constants.trackOnStart(tracker, TAG);
        // .build());
    }

    private boolean checkValidation(Context context) {
        boolean ret = true;

        if (!EditTextValidator.hasText(context, editSubscriberNumber,
                Constants.ERROR_SUB_ID_BLANK_FIELD)) {
            ret = false;
            editSubscriberNumber.requestFocus();
            return ret;
//		} else if (!EditTextValidator.isSubIdValid(context,
//				editSubscriberNumber, prefix, subIdLen,mOperatorId)) {
//			ret = false;
//			editSubscriberNumber.requestFocus();
//			return ret;
        } else if (!EditTextValidator.hasText(context, editMobileNumber,
                Constants.ERROR_MOBILE_BLANK_FIELD)) {
            ret = false;
            editMobileNumber.requestFocus();
            return ret;
        } else if (!EditTextValidator.isValidMobileNumber(context,
                editMobileNumber, Constants.ERROR_MOBILE_LENGTH_FIELD)) {
            ret = false;
            editMobileNumber.requestFocus();
            return ret;
        } else if (!EditTextValidator.hasText(context, editRechargeAmount,
                Constants.ERROR_AMOUNT_BLANK_FIELD)) {
            ret = false;
            editRechargeAmount.requestFocus();
            return ret;
        } else
            return ret;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.PLAN_REQUEST) {
                editRechargeAmount
                        .setText(data.getExtras().getString("result"));
            }
        }
    }

    private void findViewById() {
        mImageOprLogo = (ImageView) findViewById(R.id.operator_logo);
        mTextOperatorName = (TextView) findViewById(R.id.operator_name);
        mBtnRchNow = (Button) findViewById(R.id.mBtnRchNow);
        editMobileNumber = (EditText) findViewById(R.id.editMobileNumber);
        editRechargeAmount = (EditText) findViewById(R.id.editRechargeAmount);
        editSubscriberNumber = (EditText) findViewById(R.id.editSubscriberNumber);
        btnPlans = (Button) findViewById(R.id.btnPlans);
        mLastRecharge = (TextView) findViewById(R.id.lastRecharge);
        textViewHintSubId = (TextView) findViewById(R.id.textViewHintSubId);
    }

    public class RechargeTask extends AsyncTask<String, String, String>
            implements OnDismissListener {
        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method", "dthRecharge"));
            listValuePair.add(new BasicNameValuePair("mobileNumber",
                    editMobileNumber.getText().toString().trim()));
            listValuePair.add(new BasicNameValuePair("operator",
                    (mOperatorId - 15) + ""));
            listValuePair.add(new BasicNameValuePair("subId",
                    editSubscriberNumber.getText().toString().trim()));
            listValuePair.add(new BasicNameValuePair("amount",
                    editRechargeAmount.getText().toString()));
            listValuePair.add(new BasicNameValuePair("type", "flexi"));
            // listValuePair.add(new BasicNameValuePair("uuid", uuid));
            listValuePair.add(new BasicNameValuePair("timestamp", String
                    .valueOf(timestamp)));
            listValuePair.add(new BasicNameValuePair("profile_id", Utility
                    .getProfileID(DthRechargeActivity.this,
                            Constants.SHAREDPREFERENCE_PROFILE_ID)));
            listValuePair.add(new BasicNameValuePair("hash_code", Constants
                    .encryptPassword(Utility.getUUID(DthRechargeActivity.this,
                            Constants.SHAREDPREFERENCE_UUID)
                            + editSubscriberNumber.getText().toString().trim()
                            + amount + timestamp)));
            listValuePair.add(new BasicNameValuePair("device_type", "android"));
            String response = RequestClass.getInstance().readPay1Request(
                    DthRechargeActivity.this, Constants.API_URL, listValuePair);
            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {

                        // String rechargeString = Constants.getRechargeString(
                        // mContext, Constants.RECHARGE_MOBILE,
                        // mOperatorId, subId, mobileNumber, amount, "0");

						/*
						 * try { notificationDataSource.open();
						 * notificationDataSource.createNotification(
						 * rechargeString, 0, "" + System.currentTimeMillis());
						 * } catch (SQLException exception) { // TODO: handle
						 * exception // // Log.e("Er ", exception.getMessage());
						 * } catch (Exception e) { // TODO: handle exception //
						 * // Log.e("Er ", e.getMessage()); } finally {
						 * notificationDataSource.close(); }
						 */
                        // ContentValues contentValues = new ContentValues();
                        // contentValues.put("notification_msg",
                        // rechargeString);
                        // contentValues.put("timestamp ", "12345678902");
                        // contentValues.put("netOrSms ", 0);
                        // controller.insertNotifications(contentValues);

                        // Constants.showCustomToast(DthRechargeActivity.this,
                        // "Recharge request sent successfully.");
                        Constants.showOneButtonSuccessDialog(
                                DthRechargeActivity.this,
                                "Recharge request sent successfully.\nTransaction id : "
                                        + jsonObject.getString("description"),
                                Constants.RECHARGE_SETTINGS);
                        String balance = jsonObject.getString("balance");
                        Utility.setBalance(DthRechargeActivity.this,
                                Constants.SHAREDPREFERENCE_BALANCE, balance);
                        // finish();
                    } else {
                        Constants.showOneButtonFailureDialog(
                                DthRechargeActivity.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(
                            DthRechargeActivity.this, Constants.ERROR_INTERNET,
                            TAG, Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(DthRechargeActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(DthRechargeActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(DthRechargeActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            RechargeTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            RechargeTask.this.cancel(true);
            dialog.cancel();
        }

    }

    @Override
    protected void onResume() {
        Constants.showNavigationBar(DthRechargeActivity.this);
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (Constants.isExpanded) {
            Constants.collapsView();
        } else {
            super.onBackPressed();
        }
    }

    public class GetLastRechargeTask extends AsyncTask<String, String, String>
            implements OnDismissListener {
        ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            String response = Constants.fetchLastRecharge(mContext, params[0],
                    (mOperatorId - 15) + "");
            Log.e("Last transaction:", "" + response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            super.onPostExecute(result);
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONObject jsonDescription = jsonObject
                                .getJSONObject("description");
                        String time_stamp = jsonDescription
                                .getString("time_stamp");
                        String amount = jsonDescription.getString("amount");

                        mLastRecharge.setText("Last "
                                + bundle.getString(Constants.OPERATOR_NAME)
                                + " recharge was done on " + time_stamp
                                + " Amount Rs. " + amount);
                        mLastRecharge.setVisibility(View.VISIBLE);
                    } else {
                        mLastRecharge.setVisibility(View.GONE);
                    }

                }
            } catch (JSONException e) {
                // e.printStackTrace();

            } catch (Exception e) {

            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(mContext);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            GetLastRechargeTask.this.cancel(true);
                            dialog.cancel();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            GetLastRechargeTask.this.cancel(true);
            dialog.cancel();
        }
    }

    public void checkSubIdValidation(String subId, int operatorId) {
        boolean isValid;

		/*
		 * public static final int OPERATOR_DTH_AIRTEL = 16; public static final
		 * int OPERATOR_DTH_BIG = 17; public static final int
		 * OPERATOR_DTH_DISHTV = 18; public static final int OPERATOR_DTH_SUN =
		 * 19; public static final int OPERATOR_DTH_TATA_SKY = 20; public static
		 * final int OPERATOR_DTH_VIDEOCON = 21;
		 */

        switch (operatorId) {
            case 16:
                // isValid = checkLengthNPrefix("30", 10);
                prefix = "30";
                subIdLen = 10;
                break;
            case 17:
                prefix = "20";
                subIdLen = 12;
                break;
            case 18:

                prefix = "015";
                subIdLen = 12;
                break;
            case 19:

                prefix = "4";
                subIdLen = 11;
                break;
            case 20:
                isValid = checkLengthNPrefix("10", 10);
                prefix = "10";
                subIdLen = 10;
                break;
            case 21:
                prefix = "";
                subIdLen = 9;
                isValid = checkLengthNPrefix("30", 10);
                if (subId.length() >= 6 && subId.length() <= 9) {
                    isValid = true;
                } else {
                    isValid = false;
                    messagePrefix = messagePrefix
                            + "Sub Id should should be 6 to 9 digit long";
                }
                break;

            default:
                isValid = true;
                break;
        }

    }

    public boolean checkLengthNPrefix(String prefix, int len) {
        boolean isValidLength;

        if (subId.startsWith(prefix)) {
            isValidLength = true;
            messagePrefix = "" + messagePrefix;
        } else {
            isValidLength = false;
            messagePrefix = messagePrefix + "Sub Id should start with "
                    + prefix + "\n";
        }

        if (subId.length() == len) {
            isValidLength = true;
            messagePrefix = "" + messagePrefix;
        } else {
            isValidLength = false;
            messagePrefix = messagePrefix + "Sub Id should should be " + len
                    + " digit long";
        }

        return isValidLength;
    }

}
