package com.mindsarray.pay1.requesthandler;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.utilities.CookieObject;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class RequestClass {

    HttpClient httpClient;
    HttpPost httpPost;
    HttpResponse httpResponse;
    StatusLine statusLine;
    HttpEntity entity;
    InputStream content;
    String result = null;
    List<Cookie> cookies = null;
    CookieStore cookieStore;
    List<CookieObject> cookieObjects;
    private static RequestClass instance = null;

    public RequestClass() {
        try {
            // HttpParams httpParameters = new BasicHttpParams();
            // // Set the timeout in milliseconds until a connection is
            // // established.
            // int timeoutConnection = 30000;
            // HttpConnectionParams.setConnectionTimeout(httpParameters,
            // timeoutConnection);
            // // Set the default socket timeout (SO_TIMEOUT)
            // // in milliseconds which is the timeout for waiting for data.
            // int timeoutSocket = 30000;
            // HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            //
            // httpClient = new DefaultHttpClient(httpParameters);

            HttpParams httpParameters = new BasicHttpParams();
            // Set the timeout in milliseconds until a connection is
            // established.
            int timeoutConnection = 30000;
            HttpConnectionParams.setConnectionTimeout(httpParameters,
                    timeoutConnection);
            // Set the default socket timeout (SO_TIMEOUT)
            // in milliseconds which is the timeout for waiting for data.
            int timeoutSocket = 30000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            KeyStore trustStore = KeyStore.getInstance(KeyStore
                    .getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory
                    .getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            DefaultHttpClient client = new DefaultHttpClient(httpParameters);
            // ClientConnectionManager mgr = client.getConnectionManager();
            HttpParams params = client.getParams();
            httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(
                    params, registry), params);

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public static RequestClass getInstance() {
        if (instance == null) {
            instance = new RequestClass();
        }
        return instance;
    }

    public synchronized String readPay1Request(Context context, String url,
                                               List<NameValuePair> nameValuePairs) {

        if (Constants.isOnline(context)) {

            BufferedReader reader = null;

            try {
                getCookies(context);
                String nameValue = "Parameters: ";
                for (NameValuePair valPair : nameValuePairs) {
                    nameValue += valPair.getName() + " : " + valPair.getValue() + " ";
                }
                Constants.log(context, "Params Sent:" + nameValue);
                Log.e("Paramters:", nameValue);
                HttpContext ctx = new BasicHttpContext();
//				ctx.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
                StringBuilder builder = new StringBuilder();

                httpPost = new HttpPost(url);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                setCookiesToRequest(context);
                httpResponse = httpClient.execute(httpPost, ctx);
                getCookiesFromResponse(context);
                statusLine = httpResponse.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                Log.e("Status Code", statusCode + "");
                if (statusCode == 200) {

//					cookies = cookieStore.getCookies();

				/*	Gson gson = new Gson();
					String json = gson.toJson(cookieStore);
					Utility.setCookieObject(context,json);*/
                    cookieObjects = new ArrayList<CookieObject>();
//					for (Cookie a : cookies) {
//						CookieObject cookieObject=new CookieObject();
//						cookieObject.setDomain(a.getDomain());
//						cookieObject.setExpiry(a.getExpiryDate());
//						cookieObject.setName(a.getName());
//						cookieObject.setPath(a.getPath());
//						cookieObject.setValue(a.getValue());
//						cookieObject.setVersion(a.getVersion());
//						cookieObjects.add(cookieObject);
//					}
                    //Gson gson = new Gson();
                    //String json = gson.toJson(cookieObjects);
//					Log.e("Cookies", "Cookies Received:" + json);
                    //Utility.setCookieObject(context,json);
                    // }
                    entity = httpResponse.getEntity();
                    content = entity.getContent();
                    reader = new BufferedReader(new InputStreamReader(content));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line);
                    }
//					Constants.writeToSDCard(context, "\n"+url+"\n"+nameValuePairs.toString()+"\n"+builder+"\n");
                    result = (builder.toString().trim() != null || builder
                            .toString().trim() != "") ? builder.toString()
                            .trim() : Constants.ERROR_SERVER;
                } else {

                    entity = httpResponse.getEntity();
                    content = entity.getContent();
                    reader = new BufferedReader(new InputStreamReader(content));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line);
                    }
                    String res = builder.toString();
                    // // Log.e(RequestClass.class.toString(),
                    // "Failed to connect");
                    result = Constants.ERROR_SERVER;
                }
            } catch (UnknownHostException e) {
                // e.printStackTrace();
                // // Log.i("fail ", e.getMessage());
                result = Constants.ERROR_SERVER;
            } catch (ClientProtocolException e) {
                // e.printStackTrace();
                // // Log.i("fail ", e.getMessage());
                result = Constants.ERROR_SERVER;
            } catch (IOException e) {
                // e.printStackTrace();
                // // Log.i("fail ", e.getMessage());
                result = Constants.ERROR_SERVER;
            } catch (Exception e) {
                // e.printStackTrace();
                // // Log.i("fail ", e.getMessage());
                result = Constants.ERROR_SERVER;
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        // e.printStackTrace();
                        result = Constants.ERROR_SERVER;
                    }
                }

            }
        } else {
            result = Constants.ERROR_INTERNET;
        }
        Log.e("Result ", result);
//		 Constants.log(context, "Response:" + result);
        return result;
    }


    public String readPay1RequestForPlan(Context context, String url,
                                         List<NameValuePair> nameValuePairs) {

        if (Constants.isOnline(context)) {

            BufferedReader reader = null;

            try {
//				
                getCookies(context);

                String nameValue = "Parameters: ";
                for (NameValuePair valPair : nameValuePairs) {
                    nameValue += valPair.getName() + " : " + valPair.getValue();
                }

                HttpContext ctx = new BasicHttpContext();
//				ctx.setAttribute(ClientContext.COOKIE_STORE, cookieStore);

                StringBuilder builder = new StringBuilder();

                httpPost = new HttpPost(url);
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                setCookiesToRequest(context);
                httpResponse = httpClient.execute(httpPost, ctx);
                getCookiesFromResponse(context);
                statusLine = httpResponse.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                Log.e("Status Code", statusCode + "");
                if (statusCode == 200) {

//					cookies = cookieStore.getCookies();

                    cookieObjects = new ArrayList<CookieObject>();
//					for (Cookie a : cookies) {
//						CookieObject cookieObject=new CookieObject();
//						cookieObject.setDomain(a.getDomain());
//						cookieObject.setExpiry(a.getExpiryDate());
//						cookieObject.setName(a.getName());
//						cookieObject.setPath(a.getPath());
//						cookieObject.setValue(a.getValue());
//						cookieObject.setVersion(a.getVersion());
//						cookieObjects.add(cookieObject);
//					}


                    //Gson gson = new Gson();
                    //String json = gson.toJson(cookieObjects);
                    //Utility.setCookieObject(context,json);

                    entity = httpResponse.getEntity();
                    content = entity.getContent();
                    try {
                        reader = new BufferedReader(new InputStreamReader(content));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line);
                    }

//					Constants.writeToSDCard(context, "\n"+url+"\n"+nameValuePairs.toString()+"\n"+builder+"\n");
                    result = (builder.toString().trim() != null || builder
                            .toString().trim() != "") ? builder.toString()
                            .trim() : Constants.ERROR_SERVER;
                } else {
                    // // Log.e(RequestClass.class.toString(),
                    // "Failed to connect");
                    result = Constants.ERROR_SERVER;
                }
            } catch (UnknownHostException e) {
                // e.printStackTrace();
                // // Log.i("fail ", e.getMessage());
                result = Constants.ERROR_SERVER;
            } catch (ClientProtocolException e) {
                // e.printStackTrace();
                // // Log.i("fail ", e.getMessage());
                result = Constants.ERROR_SERVER;
            } catch (IOException e) {
                // e.printStackTrace();
                // // Log.i("fail ", e.getMessage());
                result = Constants.ERROR_SERVER;
            } catch (Exception e) {
                e.printStackTrace();
//				Log.e("fail ", e.getMessage());
                result = Constants.ERROR_SERVER;
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        // e.printStackTrace();
                        result = Constants.ERROR_SERVER;
                    }
                }

            }
        } else {
            result = Constants.ERROR_INTERNET;
        }
        // Log.w("Result ", result);
//		Constants.log(context, "Response:" + result);
        return result;

    }


    public synchronized String readPay1Request(Context context, String url) {

        if (Constants.isOnline(context)) {

            BufferedReader reader = null;

            try {
                getCookies(context);

                HttpContext ctx = new BasicHttpContext();
//				ctx.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
                StringBuilder builder = new StringBuilder();

                httpPost = new HttpPost(url);
                setCookiesToRequest(context);
                httpResponse = httpClient.execute(httpPost, ctx);
                getCookiesFromResponse(context);
                statusLine = httpResponse.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                Log.e("Status Code", statusCode + "");
                if (statusCode == 200) {

//					cookies = cookieStore.getCookies();
                    cookieObjects = new ArrayList<CookieObject>();
//					for (Cookie a : cookies) {
//						CookieObject cookieObject=new CookieObject();
//						cookieObject.setDomain(a.getDomain());
//						cookieObject.setExpiry(a.getExpiryDate());
//						cookieObject.setName(a.getName());
//						cookieObject.setPath(a.getPath());
//						cookieObject.setValue(a.getValue());
//						cookieObject.setVersion(a.getVersion());
//						cookieObjects.add(cookieObject);
//					}

                    entity = httpResponse.getEntity();
                    content = entity.getContent();
                    reader = new BufferedReader(new InputStreamReader(content));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line);
                    }

//					Constants.writeToSDCard(context, "\n"+url+"\n"+"\n"+builder+"\n");
                    result = (builder.toString().trim() != null || builder
                            .toString().trim() != "") ? builder.toString()
                            .trim() : Constants.ERROR_SERVER;
                } else {
                    // // Log.e(RequestClass.class.toString(),
                    // "Failed to connect");
                    result = Constants.ERROR_SERVER;
                }
            } catch (UnknownHostException e) {
                // e.printStackTrace();
                // // Log.i("fail ", e.getMessage());
                result = Constants.ERROR_SERVER;
            } catch (ClientProtocolException e) {
                // e.printStackTrace();
                // // Log.i("fail ", e.getMessage());
                result = Constants.ERROR_SERVER;
            } catch (IOException e) {
                // e.printStackTrace();
                // // Log.i("fail ", e.getMessage());
                result = Constants.ERROR_SERVER;
            } catch (Exception e) {
                // e.printStackTrace();
                // // Log.i("fail ", e.getMessage());
                result = Constants.ERROR_SERVER;
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        // e.printStackTrace();
                        result = Constants.ERROR_SERVER;
                    }
                }

            }
        } else {
            result = Constants.ERROR_INTERNET;
        }
//		Constants.log(context, "Response:" + result);
        return result;
    }

    public synchronized String readPay1AppUpdateRequest(Context context,
                                                        String url) {

        if (Constants.isOnline(context)) {

            BufferedReader reader = null;

            try {

                StringBuilder builder = new StringBuilder();
                // Log.w("URL", url);
                httpPost = new HttpPost(url);
                // httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                httpResponse = httpClient.execute(httpPost);
                statusLine = httpResponse.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                if (statusCode == 200) {
                    entity = httpResponse.getEntity();
                    content = entity.getContent();
                    reader = new BufferedReader(new InputStreamReader(content));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line);
                    }
                    result = (builder.toString().trim() != null || builder
                            .toString().trim() != "") ? builder.toString()
                            .trim() : Constants.ERROR_SERVER;
                } else {
                    // // Log.e(RequestClass.class.toString(),
                    // "Failed to connect");
                    result = Constants.ERROR_SERVER;
                }
            } catch (UnknownHostException e) {
                // e.printStackTrace();
                // // Log.i("fail ", e.getMessage());
                result = Constants.ERROR_SERVER;
            } catch (ClientProtocolException e) {
                // e.printStackTrace();
                // // Log.i("fail ", e.getMessage());
                result = Constants.ERROR_SERVER;
            } catch (IOException e) {
                // e.printStackTrace();
                // // Log.i("fail ", e.getMessage());
                result = Constants.ERROR_SERVER;
            } catch (Exception e) {
                // e.printStackTrace();
                // // Log.i("fail ", e.getMessage());
                result = Constants.ERROR_SERVER;
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        // e.printStackTrace();
                        result = Constants.ERROR_SERVER;
                    }
                }

            }
        } else {
            result = Constants.ERROR_INTERNET;
        }
        return result;
    }

    void getCookies(Context context) {
//		cookieStore = new BasicCookieStore();
//		Gson gsonObect = new Gson();
//		String jsonObect = Utility.getCookieObject(context);
//		if(jsonObect!=null){
//			Type collectionType = new TypeToken<Collection<CookieObject>>(){}.getType();
//			Collection<CookieObject> enums = gsonObect.fromJson(jsonObect, collectionType);
//			for(CookieObject ee:enums){
//				BasicClientCookie cookie = new BasicClientCookie(
//						ee.getName(),
//						ee.getValue());
//				cookie.setVersion(ee.getVersion());
//				cookie.setDomain(ee.getDomain());
//				cookie.setPath(ee.getPath());
//				cookie.setExpiryDate(ee.getExpiry());
//				
//				cookieStore.addCookie(cookie);
//			}
//		}else{
//			if(Utility.getCookieName(context,
//					Constants.SHAREDPREFERENCE_COOKIE_NAME_RESPONSE) != null && Utility.getCookieName(context,
//							Constants.SHAREDPREFERENCE_COOKIE_NAME_RESPONSE) != ""){
//				BasicClientCookie cookie_response = new BasicClientCookie(
//						Utility.getCookieName(context,
//								Constants.SHAREDPREFERENCE_COOKIE_NAME_RESPONSE),
//						Utility.getCookieValue(context,
//								Constants.SHAREDPREFERENCE_COOKIE_VALUE_RESPONSE));
//				cookie_response.setVersion(Utility.getCookieVersion(context,
//						Constants.SHAREDPREFERENCE_COOKIE_VERSION));
//				cookie_response.setDomain(Utility.getCookieDomain(context,
//						Constants.SHAREDPREFERENCE_COOKIE_DOMAIN));
//				cookie_response.setPath(Utility.getCookiePath(context,
//						Constants.SHAREDPREFERENCE_COOKIE_PATH));
//				cookieStore.addCookie(cookie_response);
//			}	
//		}
    }

    public void setCookiesToRequest(Context context) {
        String cookies = Utility.getHeaderCookieString(context);
        Log.e("Request Cookie", cookies + " ");
        httpPost.setHeader("Cookie", cookies);
        Header[] headers = httpPost.getAllHeaders();
        for (Header header : headers) {
            Log.e("Request Header", "Key : " + header.getName()
                    + ", Value : " + header.getValue());
        }
    }

    public void getCookiesFromResponse(Context context) {
        Header[] headers = httpResponse.getAllHeaders();
        for (Header header : headers) {
            if (header.getName().equals("Set-Cookie")) {
                Utility.setHeaderCookieString(context, header.getValue());
                Log.e("Response Cookie", "Key : " + header.getName()
                        + ", Value : " + header.getValue());
            }
        }
        Log.e("Response Cookie", Utility.getHeaderCookieString(context));
    }

//	private String executeRequest(Context context, HttpRequestBase requestBase){
//        String responseString = "" ;
//
//        InputStream responseStream = null ;
//        HttpClient client = new DefaultHttpClient () ;
//        try{
//            httpResponse = client.execute(requestBase) ;
//            getCookiesFromResponse(context);
//            if (httpResponse != null){
//                HttpEntity responseEntity = httpResponse.getEntity() ;
//
//                if (responseEntity != null){
//                    responseStream = responseEntity.getContent() ;
//                    if (responseStream != null){
//                        BufferedReader br = new BufferedReader (new InputStreamReader (responseStream)) ;
//                        String responseLine = br.readLine() ;
//                        String tempResponseString = "" ;
//                        while (responseLine != null){
//                            tempResponseString = tempResponseString + responseLine + System.getProperty("line.separator") ;
//                            responseLine = br.readLine() ;
//                        }
//                        br.close() ;
//                        if (tempResponseString.length() > 0){
//                            responseString = tempResponseString ;
//                        }
//                    }
//                }
//            }
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        } catch (ClientProtocolException e) {
//            e.printStackTrace();
//        } catch (IllegalStateException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }finally{
//            if (responseStream != null){
//                try {
//                    responseStream.close() ;
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        client.getConnectionManager().shutdown() ;
//
//        return responseString ; 
//    }
//
//    public String executeMultiPartRequest(Context context, Map params){
//    	String url = Constants.URL + "distributors/api?";
//    	httpPost = new HttpPost (url) ;
//    	 
//    	try{
//    		MultipartEntity multiPartEntity = new MultipartEntity () ;
//    		Iterator it = params.entrySet().iterator();
//            while (it.hasNext()) {
//                Map.Entry pair = (Map.Entry) it.next();
//                if(pair.getValue() instanceof ArrayList<?>){
//                    if(!((ArrayList<?>) pair.getValue()).isEmpty()) {
//                        if(pair.getKey().equals("remove[]")){
//                            for (String fileURI : (ArrayList<String>) pair.getValue()) {
//                            	multiPartEntity.addPart((String) pair.getKey(), new StringBody(fileURI)) ;
//                            }
//                        }
//                        else {
//                        	for (String fileURI : (ArrayList<String>) pair.getValue()) {
//	                        	File file = new File(fileURI);
//	                        	FileBody fileBody = new FileBody(file, "application/octet-stream") ;
//	                            multiPartEntity.addPart((String) pair.getKey(), fileBody) ;
//                        	}    
//                        }
//                    }
//                }
//                else
//                	multiPartEntity.addPart((String) pair.getKey(), new StringBody((String) pair.getValue())) ;
//            }
//            setCookiesToRequest(context);
//            httpPost.setEntity(multiPartEntity) ;
//            
//    	}
//    	catch(Exception e){
//    		e.printStackTrace();
//    	}
//    	
//    	return executeRequest (context, httpPost) ;
//    }

    public synchronized String executeMultiPartRequest(Context context, Map params) {
        String url = Constants.KYC_URL + "distributors/api?";
        if (Constants.isOnline(context)) {

            BufferedReader reader = null;

            try {
                getCookies(context);

                MultipartEntity multiPartEntity = new MultipartEntity();
                Iterator it = params.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    if (pair.getValue() instanceof ArrayList<?>) {
                        if (!((ArrayList<?>) pair.getValue()).isEmpty()) {
                            if (pair.getKey().equals("remove[]")) {
                                for (String fileURI : (ArrayList<String>) pair.getValue()) {
                                    multiPartEntity.addPart((String) pair.getKey(), new StringBody(fileURI));
                                }
                            } else {
                                for (String fileURI : (ArrayList<String>) pair.getValue()) {
                                    Log.e("Image: " + pair.getKey(), pair.getValue() + "");
                                    File file = new File(fileURI);
                                    FileBody fileBody = new FileBody(file);
                                    multiPartEntity.addPart((String) pair.getKey(), fileBody);
                                }
                            }
                        }
                    } else {
                        multiPartEntity.addPart((String) pair.getKey(), new StringBody((String) pair.getValue()));
                        Log.e("Request " + pair.getKey(), pair.getValue() + "");
                    }
                }
                HttpContext ctx = new BasicHttpContext();
//				ctx.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
                StringBuilder builder = new StringBuilder();

                httpPost = new HttpPost(url);
                httpPost.setEntity(multiPartEntity);
                setCookiesToRequest(context);
                httpResponse = httpClient.execute(httpPost, ctx);
                getCookiesFromResponse(context);
                statusLine = httpResponse.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                Log.e("Status Code", statusCode + "");
                if (statusCode == 200) {

//					cookies = cookieStore.getCookies();
					
				/*	Gson gson = new Gson();
					String json = gson.toJson(cookieStore);
					Utility.setCookieObject(context,json);*/
                    cookieObjects = new ArrayList<CookieObject>();
//					for (Cookie a : cookies) {
//						CookieObject cookieObject=new CookieObject();
//						cookieObject.setDomain(a.getDomain());
//						cookieObject.setExpiry(a.getExpiryDate());
//						cookieObject.setName(a.getName());
//						cookieObject.setPath(a.getPath());
//						cookieObject.setValue(a.getValue());
//						cookieObject.setVersion(a.getVersion());
//						cookieObjects.add(cookieObject);
//					}
                    Gson gson = new Gson();
                    String json = gson.toJson(cookieObjects);
//					Log.e("Cookies", "Cookies Received:" + json);
                    Utility.setCookieObject(context, json);
                    // }
                    entity = httpResponse.getEntity();
                    content = entity.getContent();
                    reader = new BufferedReader(new InputStreamReader(content));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line);
                    }
//					Constants.writeToSDCard(context, "\n"+url+"\n"+nameValuePairs.toString()+"\n"+builder+"\n");
                    result = (builder.toString().trim() != null || builder
                            .toString().trim() != "") ? builder.toString()
                            .trim() : Constants.ERROR_SERVER;
                } else {
                    // // Log.e(RequestClass.class.toString(),
                    // "Failed to connect");
                    result = Constants.ERROR_SERVER;
                }
            } catch (UnknownHostException e) {
                // e.printStackTrace();
                // // Log.i("fail ", e.getMessage());
                result = Constants.ERROR_SERVER;
            } catch (ClientProtocolException e) {
                // e.printStackTrace();
                // // Log.i("fail ", e.getMessage());
                result = Constants.ERROR_SERVER;
            } catch (IOException e) {
                // e.printStackTrace();
                // // Log.i("fail ", e.getMessage());
                result = Constants.ERROR_SERVER;
            } catch (Exception e) {
                // e.printStackTrace();
                // // Log.i("fail ", e.getMessage());
                result = Constants.ERROR_SERVER;
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        // e.printStackTrace();
                        result = Constants.ERROR_SERVER;
                    }
                }

            }
        } else {
            result = Constants.ERROR_INTERNET;
        }
        Log.e("Result ", result);
//		 Constants.log(context, "Response:" + result);
        return result;
    }
}