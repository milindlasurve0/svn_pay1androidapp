package com.mindsarray.pay1;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mindsarray.pay1.constant.Constants;

public class SettingsMapActivity extends FragmentActivity {

    private GoogleMap googleMap;
    private Location location = null;
    private Button button_setLocation;
    private static final String TAG = "Setting Map Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settingsmap_activity);
        tracker = Constants.setAnalyticsTracker(this, tracker);
        try {
            button_setLocation = (Button) findViewById(R.id.button_setLocation);
            button_setLocation.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("LOCATION", location);
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
            });

            location = getIntent().getExtras().getParcelable("LOCATION");
            if (googleMap == null) {
                // Try to obtain the map from the SupportMapFragment.
                /*googleMap = ((SupportMapFragment) getSupportFragmentManager()
						.findFragmentById(R.id.map_full)).getMap();*/
                ((SupportMapFragment) getSupportFragmentManager().findFragmentById
                        (R.id.map_full)).getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap1) {
                        googleMap = googleMap1;
                    }
                });
                googleMap
                        .setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {

                            @Override
                            public void onMarkerDragStart(Marker marker) {
                                // TODO Auto-generated method stub

                            }

                            @Override
                            public void onMarkerDragEnd(Marker marker) {
                                // TODO Auto-generated method stub
                                googleMap.clear();
                                LatLng latLng = marker.getPosition();
                                MarkerOptions options = new MarkerOptions();
                                options.position(latLng);
                                options.title("You are here");
                                options.draggable(true);
                                options.icon(BitmapDescriptorFactory
                                        .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                                // options.icon(BitmapDescriptorFactory.fromResource(R.drawable.close));
                                location = new Location("Test");
                                location.setLatitude(latLng.latitude);
                                location.setLongitude(latLng.longitude);
                                googleMap.addMarker(options);
                            }

                            @Override
                            public void onMarkerDrag(Marker marker) {
                                // TODO Auto-generated method stub

                            }
                        });
                googleMap
                        .setOnMapClickListener(new GoogleMap.OnMapClickListener() {

                            @Override
                            public void onMapClick(LatLng latLng) {
                                // TODO Auto-generated method stub
                                googleMap.clear();
                                MarkerOptions options = new MarkerOptions();
                                options.position(latLng);
                                options.title("You are here");
                                options.draggable(true);
                                options.icon(BitmapDescriptorFactory
                                        .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                                // options.icon(BitmapDescriptorFactory.fromResource(R.drawable.close));
                                location = new Location("Test");
                                location.setLatitude(latLng.latitude);
                                location.setLongitude(latLng.longitude);
                                googleMap.addMarker(options);
                            }
                        });
                // Check if we were successful in obtaining the map.
                if (googleMap != null) {
                    setUpMap(location);
                }
            }
        } catch (Exception exception) {
        }
    }

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        //tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setUpMap(Location location) {
        try {
            double lat = location.getLatitude(), lng = location.getLongitude();
            LatLng latLng = new LatLng(lat, lng);
            MarkerOptions options = new MarkerOptions();
            options.position(latLng);
            options.title("You are here");
            options.draggable(true);
            options.icon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
            // options.icon(BitmapDescriptorFactory.fromResource(R.drawable.close));
            googleMap.addMarker(options);

            // CameraPosition camPos = new CameraPosition.Builder()
            // .target(new LatLng(latitude, longitude)).zoom(18)
            // .bearing(location.getBearing()).tilt(70).build();

            CameraPosition camPos = new CameraPosition.Builder().target(latLng)
                    .zoom(15).build();

            CameraUpdate camUpd3 = CameraUpdateFactory
                    .newCameraPosition(camPos);

            googleMap.animateCamera(camUpd3);
        } catch (Exception exception) {
        }

    }
}
