package com.mindsarray.pay1;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;

import com.mindsarray.pay1.adapterhandler.EzetopTransactionAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class EzeTapTransactionsHistoryActivity extends Activity {

    public static final String TAG = "EzeTap Transactions History";
    ListView listViewmPosAtm;
    EzetopTransactionAdapter transactionAdapter;

    ArrayList<JSONObject> objects;

    private TextView mDateDisplay;
    private Button btnCalender, btnPreWeek, btnNextWeek;
    private int mYear;
    private int mMonth;
    private int mDay;

    StringBuilder builder;
    static final int DATE_DIALOG_ID = 0;
    String months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
            "Sep", "Oct", "Nov", "Dec"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.ezetap_transaction_activity);

        objects = new ArrayList<JSONObject>();
        listViewmPosAtm = (ListView) findViewById(R.id.listViewmPosAtm);
        btnCalender = (Button) findViewById(R.id.btnCalender);
        btnPreWeek = (Button) findViewById(R.id.btnPreWeek);
        btnNextWeek = (Button) findViewById(R.id.btnNextWeek);
        mDateDisplay = (TextView) findViewById(R.id.mDateDisplay);
        transactionAdapter = new EzetopTransactionAdapter(
                EzeTapTransactionsHistoryActivity.this, objects);
        listViewmPosAtm.setAdapter(transactionAdapter);

        //new GetEzeTapTransactionsHistoryTask().execute();


        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        btnCalender.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Toast.makeText(getActivity(), "Hi I am Working Fine",
                // Toast.LENGTH_LONG).show();
                // getActivity().showDialog(DATE_DIALOG_ID);
                // Constants.showSuccessDialog(getActivity(),
                // "Hi I am Working");

                DatePickerDialog dialog = new DatePickerDialog(EzeTapTransactionsHistoryActivity.this,
                        datePickerListener, mYear, mMonth, mDay);
                dialog.show();
            }
        });

        btnPreWeek.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    Date selected_date = new Date(mDateDisplay.getText()
                            .toString().trim());

                    Calendar c = Calendar.getInstance();
                    c.set((1900 + selected_date.getYear()),
                            selected_date.getMonth(), selected_date.getDate());
                    c.add(Calendar.DATE, -1);
                    // dateFormat.format(c.getTime());

                    // Show current date

                    mDay = c.get(Calendar.DAY_OF_MONTH);
                    mMonth = c.get(Calendar.MONTH);
                    mYear = c.get(Calendar.YEAR);

                    updateDisplay();
                } catch (Exception e) {

                }
            }
        });

        btnNextWeek.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    Date selected_date = new Date(mDateDisplay.getText()
                            .toString().trim());

                    Calendar c = Calendar.getInstance();
                    c.set((1900 + selected_date.getYear()),
                            selected_date.getMonth(), selected_date.getDate());
                    c.add(Calendar.DATE, +1);
                    // dateFormat.format(c.getTime());

                    // Show current date

                    mDay = c.get(Calendar.DAY_OF_MONTH);
                    mMonth = c.get(Calendar.MONTH);
                    mYear = c.get(Calendar.YEAR);

                    updateDisplay();
                } catch (Exception e) {

                }
            }
        });
        updateDisplay();
    }


    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            // Do whatever you want
            // updateDisplay();

            mYear = selectedYear;
            mMonth = selectedMonth;
            mDay = selectedDay;

            updateDisplay();
        }
    };

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                DatePickerDialog dialog = new DatePickerDialog(EzeTapTransactionsHistoryActivity.this,
                        datePickerListener, mYear, mMonth, mDay);

			/*
             * if (Build.VERSION.SDK_INT >= 11) { // (picker is a DatePicker)
			 * dialog.getDatePicker().setMaxDate(new Date().getTime()); } else {
			 * 
			 * }
			 */

                return dialog;
			/*
			 * return new DatePickerDialog(this, mDateSetListener, mYear,
			 * mMonth, mDay);
			 */
        }
        return null;
    }

    private void updateDisplay() {
        String m = "";
        String dt = "";
        if (mDay < 10)
            dt = "0" + mDay;
        else
            dt = mDay + "";
        if ((mMonth + 1) < 10)
            m = "0" + (mMonth + 1);
        else
            m = (mMonth + 1) + "";
        mDateDisplay.setText(dt + "-" + months[mMonth] + "-" + mYear);
        builder = new StringBuilder().append(mYear).append("-").append(m)
                .append("-").append(dt);
        objects.clear();
        new GetEzeTapTransactionsHistoryTask().execute();
    }


    public class GetEzeTapTransactionsHistoryTask extends
            AsyncTask<String, String, String> implements OnDismissListener {

        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(EzeTapTransactionsHistoryActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            GetEzeTapTransactionsHistoryTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            GetEzeTapTransactionsHistoryTask.this.cancel(true);
            dialog.cancel();
        }

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method",
                    "mposTransactionsHistory"));
            listValuePair.add(new BasicNameValuePair("date", builder
                    .toString()));

            String response = RequestClass.getInstance().readPay1Request(
                    EzeTapTransactionsHistoryActivity.this,
                    Constants.MPOS_URL_FULL, listValuePair);
            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {

                        JSONArray transactionArray = jsonObject
                                .getJSONArray("description");

                        for (int i = 0; i < transactionArray.length(); i++) {
                            objects.add(transactionArray.getJSONObject(i));
                        }

                        transactionAdapter.notifyDataSetChanged();
                    } else {
                        Constants.showOneButtonFailureDialog(
                                EzeTapTransactionsHistoryActivity.this,
                                jsonObject.getString("description"), TAG,
                                Constants.OTHER_ERROR);

                    }
                } else {
                    Constants.showOneButtonFailureDialog(
                            EzeTapTransactionsHistoryActivity.this,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(
                        EzeTapTransactionsHistoryActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(
                        EzeTapTransactionsHistoryActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }

        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Constants.showNavigationBar(EzeTapTransactionsHistoryActivity.this);

        super.onResume();
    }

}
