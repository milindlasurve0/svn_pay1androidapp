package com.mindsarray.pay1.servicehandler;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;

import com.mindsarray.pay1.R;
import com.mindsarray.pay1.constant.Constants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class FetchAddressIntentService extends IntentService {

    private static String TAG = "FetchAddressIntentService";
    protected ResultReceiver mReceiver;

    public FetchAddressIntentService() {
        super("FetchAddressIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());

        String errorMessage = "";

        // Get the location passed to this service through an extra.
        Location location = intent.getParcelableExtra(
                Constants.LOCATION_DATA_EXTRA);
        mReceiver = intent.getParcelableExtra(Constants.RECEIVER);
        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            errorMessage = getString(R.string.service_not_available);
            Log.e(TAG, errorMessage, ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            errorMessage = getString(R.string.invalid_lat_long_used);
            Log.e(TAG, errorMessage + ". " +
                    "Latitude = " + location.getLatitude() +
                    ", Longitude = " +
                    location.getLongitude(), illegalArgumentException);
        } catch (Exception e) {
            e.printStackTrace();
            errorMessage = "Some error occurred. Try Again.";
        }

        if (addresses == null || addresses.size() == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = getString(R.string.no_address_found);
                Log.e(TAG, errorMessage);
            }
            deliverResultToReceiver(Constants.FAILURE_RESULT, "" + errorMessage);
        } else {
            Address address = addresses.get(0);
            ArrayList<String> addressFragments = new ArrayList<String>();
            Log.e("API Address", addresses.get(0).toString() + "");
            Log.e("API Address SubLocality", addresses.get(0).getSubLocality() + "");
            Log.e("API Address Locality", addresses.get(0).getLocality() + "");
            Log.e("API Address AdminArea", addresses.get(0).getAdminArea() + "");
            Log.e("API Address SubAdmin", addresses.get(0).getSubAdminArea() + "");
            Log.e("API Address PostalCode", addresses.get(0).getPostalCode() + "");

            String area = "", city = "", state = "", postal_code = "", extra = "";

            area = (addresses.get(0).getSubLocality() != null) ? addresses.get(0).getSubLocality() : "";
            city = (addresses.get(0).getLocality() != null) ? addresses.get(0).getLocality() : "";
            state = (addresses.get(0).getAdminArea() != null) ? addresses.get(0).getAdminArea() : "";
            extra = (addresses.get(0).getSubAdminArea() != null) ? addresses.get(0).getSubAdminArea() : "";
            postal_code = (addresses.get(0).getPostalCode() != null) ? addresses.get(0).getPostalCode() : "";

            if (postal_code.equals("")) {
                String addressLine2 = addresses.get(0).getAddressLine(2);
                if (addressLine2 != null && !addressLine2.equals("")) {
                    if (Constants.isInteger(TextUtils.split(addressLine2, " ")[2]) && TextUtils.split(addressLine2, " ")[2].length() == 6) {
                        postal_code = TextUtils.split(addressLine2, " ")[2];
                    }
                }
            }

            if (area.equals("")) {
                area = city;
                if (!extra.equals("")) {
                    city = extra;
                }
            }
//            for(int i = 0; i < address.getMaxAddressLineIndex(); i++) {
//                addressFragments.add(address.getAddressLine(i));
//            }
            Log.i(TAG, getString(R.string.address_found));

            String joinedAddressLine = area + System.getProperty("line.separator") + city + System.getProperty("line.separator")
                    + state + System.getProperty("line.separator") + postal_code;
            deliverResultToReceiver(Constants.SUCCESS_RESULT,
                    joinedAddressLine);
        }
    }

    private void deliverResultToReceiver(int resultCode, String message) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.RESULT_DATA_KEY, message);
        mReceiver.send(resultCode, bundle);
    }
}
