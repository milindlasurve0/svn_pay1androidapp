package com.mindsarray.pay1.databasehandler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class ChatHistoryDataSource {
    // Database fields
    private SQLiteDatabase database;
    private Pay1SQLiteHelper dbHelper;

    public ChatHistoryDataSource(Context context) {
        dbHelper = new Pay1SQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void createChatHistory(String historyText, String historyTime,
                                  String historyPosition) {
        ContentValues values = new ContentValues();
        values.put(Pay1SQLiteHelper.CHATHISTORY_TEXT, historyText);
        values.put(Pay1SQLiteHelper.CHATHISTORY_TIME, historyTime);
        values.put(Pay1SQLiteHelper.CHATHISTORY_POSITION, historyPosition);

        database.insert(Pay1SQLiteHelper.TABLE_CHATHISTORY, null, values);
        // Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_ChatHistory,
        // null, Pay1SQLiteHelper.ChatHistory_ID + " = " + insertId, null,
        // null, null, null);
        // cursor.moveToFirst();
        // ChatHistory neChatHistory = cursorToChatHistory(cursor);
        // cursor.close();
        // return newChatHistory;
    }

    public void deleteChatHistory() {
        // database.delete(Pay1SQLiteHelper.TABLE_CHATHISTORY, null, null);

        database.delete(Pay1SQLiteHelper.TABLE_CHATHISTORY,
                Pay1SQLiteHelper.CHATHISTORY_ID + " NOT IN ( select "
                        + Pay1SQLiteHelper.CHATHISTORY_ID + " from "
                        + Pay1SQLiteHelper.TABLE_CHATHISTORY + " order by "
                        + Pay1SQLiteHelper.CHATHISTORY_TIME + " desc limit 100"
                        + " )", null);
    }

    public void deleteChatHistory(ChatHistory ChatHistory) {
        long id = ChatHistory.getHistoryID();
        database.delete(Pay1SQLiteHelper.TABLE_CHATHISTORY,
                Pay1SQLiteHelper.CHATHISTORY_ID + " = " + id, null);
    }

    public List<ChatHistory> getAllChatHistory() {
        List<ChatHistory> histories = new ArrayList<ChatHistory>();

        Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_CHATHISTORY,
                null, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ChatHistory history = cursorToChatHistory(cursor);
            histories.add(history);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return histories;
    }

    public List<ChatHistory> getLast5ChatHistory() {
        List<ChatHistory> histories = new ArrayList<ChatHistory>();

        Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_CHATHISTORY,
                null, null, null, null, null, Pay1SQLiteHelper.CHATHISTORY_TIME
                        + " desc limit 5");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ChatHistory history = cursorToChatHistory(cursor);
            histories.add(history);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return histories;
    }

    private ChatHistory cursorToChatHistory(Cursor cursor) {
        ChatHistory history = new ChatHistory();
        history.setHistoryID(cursor.getInt(0));
        history.setHistoryText(cursor.getString(1));
        history.setHistoryTime(cursor.getString(2));
        history.setHistoryPosition(cursor.getString(3));
        return history;
    }
}
