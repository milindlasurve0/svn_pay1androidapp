package com.mindsarray.pay1;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;

import com.mindsarray.pay1.adapterhandler.PostsAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.contenthandler.Post;
import com.mindsarray.pay1.databasehandler.PostDBHelper;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.GoogleAnalyticsHelper;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainTabWholesaleFragmentNew extends Fragment implements
        OnRefreshListener, OnClickListener {

    SwipeRefreshLayout swipeLayout;
    private Context context;
    String TAG = "Wholesale";
    public PostsAdapter mPostsAdapter;
    private ArrayList<Post> mPosts;
    // private ArrayList<ArrayList<Post>> tenPostsArray;
    private ListView mPostListView;
    PostDBHelper postDBHelper;
    GoogleAnalyticsHelper mGoogleHelper;
    int PostIdUpdate;

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            // ...
            MainActivity.currentItemIndex = 1;
            // Utility.setC2DNotificationCount(getActivity(), 0);
            //showTutorialDialog();
            if (getActivity() != null) {
                if (Utility.getLoginFlag(getActivity(),
                        Constants.SHAREDPREFERENCE_IS_LOGIN)) {
                    if (getActivity() != null) {
                        if (!Utility.getSharedPreferences(getActivity(),
                                "wholesale_banner_time").equals("")) {
                            int count = Integer.parseInt(Utility.getSharedPreferences(getActivity(),
                                    "wholesale_banner_count"));
                            double lastBannerShowTime = Double.parseDouble(Utility.getSharedPreferences(getActivity(),
                                    "wholesale_banner_time"));
                            if (System.currentTimeMillis() - lastBannerShowTime > 24 * 60 * 60 * 1000 && count <= 5) {
                                showWholesaleBanner();
                                Utility.setSharedPreference(getActivity(), "wholesale_banner_time",
                                        "" + System.currentTimeMillis());
                                Utility.setSharedPreference(getActivity(), "wholesale_banner_count",
                                        "" + (count + 1));
                            }
                        } else {
                            showWholesaleBanner();
                            Utility.setSharedPreference(getActivity(), "wholesale_banner_count",
                                    "1");
                            Utility.setSharedPreference(getActivity(), "wholesale_banner_time",
                                    "" + System.currentTimeMillis());
                        }
                    }
                } else {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivityForResult(intent, Constants.LOGIN);
                }
            }
            //gcmListenerService.cancelC2DNotification(getActivity(),gcmListenerService
            //	.C2D_NOTIFICATION_ID);

        }
    }

    private void showTutorialDialog() {
        // TODO Auto-generated method stub
        if (Utility.getSharedPreferencesBoolean(getActivity(), "show_dialog") != true) {
            TutorialDialog();
        }
    }

    private void TutorialDialog() {/*
        final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_tutorial);
		
		TextView textView_Title = (TextView) dialog
				.findViewById(R.id.textView_Title);
		TextView text = (TextView) dialog
				.findViewById(R.id.textView_Message);
		text.setText(message);
		textView_Title.setText(title);
		ImageView closeButton = (ImageView) dialog
				.findViewById(R.id.imageView_Close);
		closeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(id==Constants.LOGIN_APP_UPDATE_SETTINGS){
					
				}else{
					dialog.dismiss();
				}

			}
		});
		Button dialogButtonOk = (Button) dialog
				.findViewById(R.id.button_Ok);
		if(Constants.LOGIN_APP_UPDATE_SETTINGS==id){
			dialogButtonOk.setText("Install Update");
			dialog.setCancelable(false);
		}
		
		
		dialogButtonOk.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
			}
		});
		dialog.show();
	*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewPager mPager = (ViewPager) getActivity().findViewById(R.id.pager);
		/*RelativeLayout msgRelativeLayout = (RelativeLayout)getActivity().findViewById(R.id
				.msgRelativeLayout);
		if(mPager.getCurrentItem()==0) {
			msgRelativeLayout.setVisibility(View.VISIBLE);
		}*/
        InitGoogleAnalytics();
        mGoogleHelper.SendScreenNameGoogleAnalytics("Wholesaler List Layout",
                getActivity());

    }

    private void showWholesaleBanner() {
        final Dialog fDialog = new Dialog(getActivity());
        fDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        fDialog.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);

        fDialog.setContentView(R.layout.dialog_wholesale_banner);
        fDialog.setCancelable(false);

        Button closeButton = (Button) fDialog.findViewById(R.id.closeButton);
        closeButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                fDialog.dismiss();

            }
        });
        fDialog.show();

        fDialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    fDialog.dismiss();
                }
                return true;
            }
        });

    }

    private void InitGoogleAnalytics() {
        mGoogleHelper = new GoogleAnalyticsHelper();
        mGoogleHelper.init(getActivity());
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        if (Utility.getLoginFlag(context, Constants.SHAREDPREFERENCE_IS_LOGIN)) {
            // new GetPosts().execute();
            if (postDBHelper.getPostsFromDB().size() <= 0) {
                new GetPosts().execute();
            } else {
                refreshList();

            }
        } else {
            //context.startActivity(new Intent(context, LoginDialogActivity.class));
        }
        //refreshList();
		/*
		 * registerReceiver(broadcastReceiver, new IntentFilter(
		 * gcmListenerService.BROADCAST_ACTION_FEED));
		 */

    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_tab_wholesale_fragment,
                container, false);
        context = getActivity();
        postDBHelper = new PostDBHelper(context);

        mPosts = new ArrayList<Post>();
        // tenPostsArray = new ArrayList<>();

        mPostListView = (ListView) view.findViewById(R.id.posts);
        mPostsAdapter = new PostsAdapter(context, mPosts, this);
        mPostListView.setAdapter(mPostsAdapter);

        swipeLayout = (SwipeRefreshLayout) view
                .findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        swipeLayout.setRefreshing(true);

		/*if (Utility.getLoginFlag(context, Constants.SHAREDPREFERENCE_IS_LOGIN)) {
			// new GetPosts().execute();
			if (postDBHelper.getPostsFromDB().size() <= 0) {
				new GetPosts().execute();
			} else {
				refreshList();

			}
		} else {
			//context.startActivity(new Intent(context, LoginDialogActivity.class));
		}*/

        return view;
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // updateDate(intent);
        }
    };

    public class GetPosts extends AsyncTask<String, String, String> implements
            OnDismissListener {
        ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("retailerID", Utility
                    .getSharedPreferences(context,
                            Constants.SHAREDPREFERENCE_RETAILER_ID)));

            String response = RequestClass.getInstance().readPay1Request(
                    getActivity(), Constants.C2D_URL + "posts/find",
                    listValuePair);
            listValuePair.clear();

            Log.e("C2D Posts:", "" + response);

            return response;
        }

        @Override
        protected void onPostExecute(String result) {

            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

            swipeLayout.setRefreshing(false);
            super.onPostExecute(result);
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    JSONObject responseObject = new JSONObject(replaced);

                    String status = responseObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        if (responseObject.getBoolean("type")) {
                            JSONArray jsonData = responseObject
                                    .getJSONArray("data");

                            mPosts.clear();
                            for (int i = 0; i < jsonData.length(); i++) {
                                JSONObject responsePost = jsonData
                                        .getJSONObject(i);
                                ArrayList<String> wholesalerImages = Post
                                        .jsonArrayToImagesStringArray(responsePost
                                                .getJSONArray("images"));
                                Post post = new Post();
                                post.setmCategoryId(responsePost
                                        .getString("category_id"));

                                post.setmClientId(responsePost
                                        .getString("client_id"));
                                post.setmContactNo(responsePost
                                        .getString("contact_no"));
                                post.setmId(Integer.parseInt(responsePost
                                        .getString("id")));

                                post.setmPostDescription(responsePost
                                        .getString("description"));
                                post.setmPostTitle(responsePost
                                        .getString("title"));

                                post.setmWholesalerLogo(responsePost.getString(
                                        "logo_url").replace("\\/", "/"));
                                post.setmWholesalerName(responsePost
                                        .getString("company_name"));
                                post.setmCategoryName("");
                                post.setmPostImages(responsePost.getJSONArray(
                                        "images").toString());
                                post.setmIsInterested(responsePost
                                        .getString("is_still_interested"));
                                post.setmTime(responsePost
                                        .getString("post_time"));
                                postDBHelper.addPostsToTable(post);

                                mPosts.add(post);
								/*
								 * Post post = new Post(wholesalerImages,
								 * responsePost.getString("logo_url")
								 * .replace("\\/", "/"),
								 * responsePost.getString("company_name"),
								 * responsePost.getString("description"),
								 * responsePost.getString("title"),
								 * responsePost.getString("id"),
								 * responsePost.getString("client_id"),
								 * responsePost.getString("category_id"),
								 * responsePost.getString("post_time"),
								 * responsePost.getString("contact_no"));
								 */

								/*
								 * if(i != 0 && i % 10 == 0){
								 * //tenPostsArray.add(tenPosts); tenPosts = new
								 * ArrayList<>(); }
								 */
                            }

                            // mPosts.add(post);
                            // mPosts= postDBHelper.getPostsFromDB();
                            // tenPostsArray.add(tenPosts);
                            // mPosts = tenPostsArray.get(0);

                            // mPostListView.setAdapter(mPostsAdapter);
                            // mPostsAdapter.addAll(mPosts);
                            mPostsAdapter.notifyDataSetChanged();
                        }
                    } else {
                        Constants.showOneButtonFailureDialog(context,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(context,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(context,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                Constants.showOneButtonFailureDialog(context,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(context);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            GetPosts.this.cancel(true);
                            dialog.cancel();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            GetPosts.this.cancel(true);
            dialog.cancel();
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // swipeLayout.setRefreshing(false);
                new GetPosts().execute();
            }
        }, 1000);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        // tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
    }

    public static MainTabWholesaleFragmentNew newInstance(String content) {
        MainTabWholesaleFragmentNew fragment = new MainTabWholesaleFragmentNew();
        MainActivity.currentItemIndex = 1;
        Bundle b = new Bundle();
        b.putString("msg", content);

        fragment.setArguments(b);

        return fragment;
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        int tag = (int) v.getTag();
        Post post = mPosts.get(tag);
        ImageButton imageView = (ImageButton) v;
        mGoogleHelper.SendEventGoogleAnalytics(getActivity(),
                String.valueOf(post.getmId()) + " " + post.getmPostTitle(),
                "Interest Button Clicked", "ButtonEvent");
        ShowInterestDialog(post, imageView);
    }

    private void ShowInterestDialog(Post post, final ImageButton view) {

        final String postId = String.valueOf(post.getmId());
        final Dialog mDialog = new Dialog(getActivity());
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        mDialog.setContentView(R.layout.show_interested_dialog);
        Button buttonInterest = (Button) mDialog
                .findViewById(R.id.buttonInterest);
        ImageView imageViewClose = (ImageView) mDialog
                .findViewById(R.id.imageViewClose);
        final EditText editTextMsg = (EditText) mDialog
                .findViewById(R.id.editTextMsg);
        editTextMsg.getHint();
        PostIdUpdate = post.getmId();
        buttonInterest.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mDialog.dismiss();
                if (editTextMsg.getText().toString().length() != 0) {

                    new ShowInterestTask(view).execute(postId, editTextMsg
                            .getText().toString());
                } else {
                    new ShowInterestTask(view).execute(postId, editTextMsg
                            .getHint().toString());
                }
            }
        });

        imageViewClose.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    public class ShowInterestTask extends AsyncTask<String, String, String>
            implements OnDismissListener {
        private static final String TAG = "C2D";
        ProgressDialog dialog;
        ImageButton view;

        public ShowInterestTask(ImageButton view) {
            super();
            this.view = view;

        }

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("retailerID", Utility
                    .getSharedPreferences(context,
                            Constants.SHAREDPREFERENCE_RETAILER_ID)));
            listValuePair.add(new BasicNameValuePair("postId", params[0]));
            listValuePair.add(new BasicNameValuePair("message", params[1]));

            String response = RequestClass.getInstance().readPay1Request(
                    context, Constants.C2D_URL + "posts/interests",
                    listValuePair);
            listValuePair.clear();

            Log.e("C2D Posts:", "" + response);

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            super.onPostExecute(result);
            try {
                if (!result.startsWith("Error")) {

                    JSONObject responseObject = new JSONObject(result);

                    String status = responseObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        if (responseObject.getBoolean("type")) {
                            mPostListView.invalidateViews();
                            Constants.showOneButtonSuccessDialog(context,
                                    "Request sent", Constants.C2D_SETTINGS);

                            PostDBHelper postDBHelper = new PostDBHelper(
                                    context);
                            postDBHelper.UpdatePost(PostIdUpdate);
                            // refreshList();
                            // mPosts = postDBHelper.getPostsFromDB();
                            // mPostsAdapter.notifyDataSetInvalidated();
                            // mPostsAdapter.notifyDataSetChanged();
							/*
							 * this.view.setBackgroundColor(getActivity()
							 * .getResources().getColor(R.color.pay1_red));
							 */
							/*
							 * this.view.setBackgroundResource(0);
							 * this.view.setBackgroundResource
							 * (R.drawable.selected_heart);
							 */
                            mPosts.get((int) this.view.getTag()).setmIsInterested("1");
                            mPostsAdapter.notifyDataSetChanged();
                            //Picasso.with(context).load(R.drawable.selected_heart).into(this.view);
						/*	this.view
							.setBackgroundResource(android.R.color.transparent);
							this.view
									.setImageResource(R.drawable.selected_heart);*/


                        } else {
                            Constants.showOneButtonFailureDialog(context,
                                    responseObject.getString("errorMsg"), TAG,
                                    Constants.OTHER_ERROR);
                        }
                    } else {
                        Constants.showOneButtonFailureDialog(context,
                                Constants.checkCode(result), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(context,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(context,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                Constants.showOneButtonFailureDialog(context,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(context);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            ShowInterestTask.this.cancel(true);
                            dialog.cancel();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            ShowInterestTask.this.cancel(true);
            dialog.cancel();
        }
    }

    public void refreshList() {
        // TODO Auto-generated method stub
        // swipeLayout.setRefreshing(false);
        mPosts.clear();
        mPosts = postDBHelper.getPostsFromDB();
        Log.d("noOfPost", "" + mPosts.size());
        // mPostsAdapter.notifyDataSetChanged();
        mPostsAdapter = new PostsAdapter(context, mPosts, this);
        mPostListView.setAdapter(mPostsAdapter);

    }

}
