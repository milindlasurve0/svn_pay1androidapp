package com.mindsarray.pay1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.utilities.Utility;

public class SettingsActivity extends Activity {
    Button btnUpdateAddress, btnUpdatePin, btnUpdateMobile, btnSMSRecharge;
    Button btnUnregister;
    private static final String TAG = "Setting Activity";
    boolean isFirst = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        // WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.settings_activity);
        findViewById();
        tracker = Constants.setAnalyticsTracker(this, tracker);

        //easyTracker.set(Fields.SCREEN_NAME, TAG);
        btnUpdateAddress.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(SettingsActivity.this,
                        SettingsUpdateAddressActivity.class);
                startActivity(intent);
            }
        });

        btnUpdateMobile.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(SettingsActivity.this,
                        SettingsUpdateMobileActivity.class);
                startActivity(intent);
            }
        });
        btnSMSRecharge.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(SettingsActivity.this,
                        SettingsSmsActivity.class);
                startActivity(intent);
            }
        });

        btnUpdatePin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(SettingsActivity.this,
                        SettingsUpdatePinActivity.class);
                intent.putExtra(Constants.IS_RESET, false);
                startActivity(intent);
            }
        });
    }

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        //tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
    }

    private void findViewById() {
        // TODO Auto-generated method stub
        btnUpdateAddress = (Button) findViewById(R.id.btnUpdateAddress);
        btnUpdatePin = (Button) findViewById(R.id.btnUpdatePin);
        btnUpdateMobile = (Button) findViewById(R.id.btnUpdateMobile);
        btnSMSRecharge = (Button) findViewById(R.id.btnSMSRecharge);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mBtnLogin.setVisibility(View.GONE);
        mBtnLogOut.setVisibility(View.VISIBLE);
        textBalance.setText("Balance: Rs."
                + Utility.getBalance(SettingsActivity.this,
                Constants.SHAREDPREFERENCE_BALANCE));
    }

    TextView textBalance;
    Button mBtnLogin, mBtnLogOut, mBtnHome, mBtnHelp;

    @Override
    protected void onResume() {
        Constants.showNavigationBar(SettingsActivity.this);
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (Constants.isExpanded) {
            Constants.collapsView();
        } else {
            super.onBackPressed();
        }
    }
}
