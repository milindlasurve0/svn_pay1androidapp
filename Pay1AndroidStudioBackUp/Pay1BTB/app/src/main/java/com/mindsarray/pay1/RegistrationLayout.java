package com.mindsarray.pay1;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RegistrationLayout extends Activity {
    private static final String TAG = "Registration";
    EditText editText_mobileNumber;
    TextView textView_TC, textView_PP;
    Button button_Submit;
    String userMobile;
    String[] permissions = new String[4];
    ;
    private static final int REQUEST_APP_SETTINGS = 168;

    private static final String[] requiredPermissions = new String[]{"android.permission.CALL_PHONE",
            "android.permission.CALL_PHONE"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_layout);

        if (android.os.Build.VERSION.SDK_INT >= 23) {
            // only for gingerbread and newer versions
            //String permission = Manifest.permission.CAMERA;


            checkAndRequestPermissions();


		/*	String permissionCall = "android.permission.CALL_PHONE";
            if (checkSelfPermission(permissionCall) != PackageManager.PERMISSION_GRANTED) {
				ActivityCompat.requestPermissions(RegistrationLayout.this, new
						String[]{permissionCall}, 100);
			}


			String permissionLocation = Manifest.permission.ACCESS_FINE_LOCATION;
			if (checkSelfPermission(permissionLocation) != PackageManager.PERMISSION_GRANTED) {
				ActivityCompat.requestPermissions(RegistrationLayout.this, new
						String[]{permissionLocation}, 101);
			}


			String permissionStorage = android.Manifest.permission
					.WRITE_EXTERNAL_STORAGE;
			if (checkSelfPermission(permissionStorage) != PackageManager.PERMISSION_GRANTED) {
				ActivityCompat.requestPermissions(RegistrationLayout.this, new
						String[]{permissionStorage}, 102);
			}


			String permissionContacts = Manifest.permission.READ_SMS;
			if (checkSelfPermission(permissionContacts) != PackageManager.PERMISSION_GRANTED) {
				ActivityCompat.requestPermissions(RegistrationLayout.this, new
						String[]{permissionContacts}, 103);
			}*/

/*


			String permission = "android.permission.CALL_PHONE";
			if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {

				ActivityCompat.requestPermissions(RegistrationLayout.this, new
						String[]{permission,"android.permission.",android.Manifest.permission
						.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION,
						Manifest.permission.READ_SMS, Manifest.permission.READ_CONTACTS}, 100);
			} else {
				// Add your function here which open camera
			}*/
        } else {
            // Add your function here which open camera
        }


        editText_mobileNumber = (EditText) findViewById(R.id.editText_mobileNumber);
        textView_TC = (TextView) findViewById(R.id.textView_TC);
        textView_PP = (TextView) findViewById(R.id.textView_PP);
        button_Submit = (Button) findViewById(R.id.button_Submit);

        button_Submit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                userMobile = editText_mobileNumber.getText().toString();
                if (userMobile.length() == 10
                        && (userMobile.startsWith("7")
                        || userMobile.startsWith("8") || userMobile
                        .startsWith("9"))) {
                    new CheckUserLevel().execute();

                } else {

                    int ecolor = R.color.black;
                    ForegroundColorSpan fgcspan = new ForegroundColorSpan(getResources().getColor
                            (ecolor));
                    SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
                            "Enter correct number");
                    ssbuilder.setSpan(fgcspan, 0, "Enter correct number".length(), 0);

                    editText_mobileNumber.setError(ssbuilder);
                }
            }
        });


        editText_mobileNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                // TODO Auto-generated method stub
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    try {
                        // TODO Auto-generated method stub
                        userMobile = editText_mobileNumber.getText().toString();
                        if (userMobile.length() == 10
                                && (userMobile.startsWith("7")
                                || userMobile.startsWith("8") || userMobile
                                .startsWith("9"))) {
                            new CheckUserLevel().execute();

                        } else {

                            int ecolor = R.color.black;
                            ForegroundColorSpan fgcspan = new ForegroundColorSpan(getResources().getColor
                                    (ecolor));
                            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
                                    "Enter correct number");
                            ssbuilder.setSpan(fgcspan, 0, "Enter correct number".length(), 0);

                            editText_mobileNumber.setError(ssbuilder);
                        }
                    } catch (Exception e) {
                        // e.printStackTrace();
                    }
                    return true;
                }
                return false;
            }
        });

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 100: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Add your function here which open camera
                } else {
                    //Toast.makeText(getApplication(), "Permission required", Toast.LENGTH_LONG)
                    //	.show();
                }
                return;
            }

            case 101: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Add your function here which open camera
                } else {
                    //Toast.makeText(getApplication(), "Permission required", Toast.LENGTH_LONG)
                    //	.show();
                }
                return;
            }

            case 102: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Add your function here which open camera
                } else {
                    //Toast.makeText(getApplication(), "Permission required", Toast.LENGTH_LONG)
                    //	.show();
                }
                return;
            }

            case 103: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Add your function here which open camera
                } else {
                    //Toast.makeText(getApplication(), "Permission required", Toast.LENGTH_LONG)
                    //	.show();
                }
                return;
            }


        }
    }


    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS);
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        int permissionCall = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE);
        int permissionStorage = ContextCompat.checkSelfPermission(this, Manifest.permission
                .WRITE_EXTERNAL_STORAGE);


        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (permissionCall != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CALL_PHONE);
        }
        if (permissionStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }


        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new
                    String[listPermissionsNeeded.size()]), 100);
            return false;
        }
        return true;
    }

    private void goToSettings() {
        Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivityForResult(myAppSettings, REQUEST_APP_SETTINGS);
    }

    public boolean hasPermissions(@NonNull String... permissions) {
        for (String permission : permissions)
            if (PackageManager.PERMISSION_GRANTED != checkSelfPermission(permission))
                return false;
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_APP_SETTINGS) {
            if (hasPermissions(requiredPermissions)) {
                Toast.makeText(this, "All permissions granted!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Permissions not granted.", Toast.LENGTH_LONG).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public class CheckUserLevel extends AsyncTask<String, String, String> {


        ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair
                    .add(new BasicNameValuePair("method", "checkRetailerExist"));
            listValuePair.add(new BasicNameValuePair("mobileNo", userMobile));

            String response = RequestClass.getInstance().readPay1Request(
                    RegistrationLayout.this, Constants.API_URL, listValuePair);
            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            dialog = new ProgressDialog(RegistrationLayout.this);
            dialog.setMessage("Please wait");
            dialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        Utility.setUserMobileNumber(RegistrationLayout.this, userMobile);
                        String code = jsonObject.getString("code");

                        if (code.equalsIgnoreCase("0")) {
                            Utility.setUserMobileNumber(RegistrationLayout.this, userMobile);
                            Constants.showTwoButtonDialog(RegistrationLayout.this, "Your number is not registered with us. To signup with us click on Yes to continue.", "Registration", "YES", "NO", 1);
							
							
							/*Intent intent=new Intent(RegistrationLayout.this,RegistrationActivity.class);
							startActivity(intent);*/
                        } else if (code.equalsIgnoreCase("2")) {
                            Utility.setUserMobileNumber(RegistrationLayout.this, userMobile);
                            ShowDialogForOtp(RegistrationLayout.this, "OTP sent to your number", TAG);
                        } else {
                            Utility.setUserMobileNumber(RegistrationLayout.this, userMobile);
                            Utility.setIsAppUsed(RegistrationLayout.this, true);
                            Intent intent = new Intent(RegistrationLayout.this, LoginActivity.class);
                            intent.putExtra("retailer_mobile", userMobile);
                            startActivity(intent);
                            finish();
                        }


                    } else {
                        Constants.showOneButtonFailureDialog(
                                RegistrationLayout.this,
                                jsonObject.getString("description"), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(
                            RegistrationLayout.this, Constants.ERROR_INTERNET,
                            TAG, Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                e.printStackTrace();
                Constants.showOneButtonFailureDialog(RegistrationLayout.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                e.printStackTrace();
                Constants.showOneButtonFailureDialog(RegistrationLayout.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }


        }
    }


    public void ShowDialogForOtp(Context mContext, String message, String title) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_failure_one_button);
        TextView textView_Title = (TextView) dialog
                .findViewById(R.id.textView_Title);
        TextView text = (TextView) dialog
                .findViewById(R.id.textView_Message);
        text.setText(message);
        textView_Title.setText(title);
        ImageView closeButton = (ImageView) dialog
                .findViewById(R.id.imageView_Close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Button dialogButtonOk = (Button) dialog
                .findViewById(R.id.button_Ok);
        dialogButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                Intent intent = new Intent(RegistrationLayout.this, VerificationActivity.class);
                intent.putExtra("userMobile", userMobile);
                startActivity(intent);
            }
        });
        dialog.show();
    }


}
