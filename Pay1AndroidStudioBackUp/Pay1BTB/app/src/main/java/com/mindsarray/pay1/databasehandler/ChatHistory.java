package com.mindsarray.pay1.databasehandler;

public class ChatHistory {
    private int historyID;
    private String historyText;
    private String historyTime;
    private String historyPosition;

    public int getHistoryID() {
        return historyID;
    }

    public void setHistoryID(int historyID) {
        this.historyID = historyID;
    }

    public String getHistoryText() {
        return historyText;
    }

    public void setHistoryText(String historyText) {
        this.historyText = historyText;
    }

    public String getHistoryTime() {
        return historyTime;
    }

    public void setHistoryTime(String historyTime) {
        this.historyTime = historyTime;
    }

    public String getHistoryPosition() {
        return historyPosition;
    }

    public void setHistoryPosition(String historyPosition) {
        this.historyPosition = historyPosition;
    }

}
