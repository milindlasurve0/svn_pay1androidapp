package com.mindsarray.pay1.utilities;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.widget.EditText;

import com.mindsarray.pay1.R;

import java.util.regex.Pattern;

public class EditTextValidator {

    // Regular Expression
    // you can change the expression based on your need
    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String PHONE_REGEX = "\\d{3}-\\d{7}";

    // Error Messages
    private static final String PHONE_MSG = "###-#######";

    // call this method when you need to check email validation
    public static boolean isEmailAddress(Context context, EditText editText,
                                         String errorMessage) {

        if (isValid(editText, EMAIL_REGEX, errorMessage, true)) {
            editText.setBackgroundDrawable(context.getResources().getDrawable(
                    R.drawable.edittext_valid_background_selector));
            editText.setPadding(10, 10, 10, 10);
            editText.setTextSize(14);
            editText.setError(null);
            editText.setTextColor(context.getResources()
                    .getColor(R.color.Black));
            return true;

        } else {
            int ecolor = R.color.black;
            ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
                    errorMessage);
            ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
            editText.setError(ssbuilder);
            editText.setBackgroundDrawable(context.getResources().getDrawable(
                    R.drawable.edittext_error_background_selector));
            editText.setPadding(10, 10, 10, 10);
            editText.setTextSize(14);
            return false;
        }
    }

    // call this method when you need to check phone number validation
    public static boolean isPhoneNumber(EditText editText, boolean required) {
        return isValid(editText, PHONE_REGEX, PHONE_MSG, required);
    }

    // return true if the input field is valid, based on the parameter passed
    public static boolean isValid(EditText editText, String regex,
                                  String errorMessage, boolean required) {

        String text = editText.getText().toString().trim();
        // pattern doesn't match so returning false
        if (!Pattern.matches(regex, text)) {
            return false;
        }

        return true;
    }

    // check the input field has any text or not
    // return true if it contains text otherwise false
    public static boolean hasText(Context context, EditText editText,
                                  String errorMessage) {
        String text = editText.getText().toString().trim();

        if (text.length() == 0) {

            int ecolor = R.color.black;
            ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
                    errorMessage);
            ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
            editText.setError(ssbuilder);
            editText.setBackgroundDrawable(context.getResources().getDrawable(
                    R.drawable.edittext_error_background_selector));
            editText.setPadding(10, 10, 10, 10);
            editText.setTextSize(14);
            return false;
        } else {
            editText.setBackgroundDrawable(context.getResources().getDrawable(
                    R.drawable.edittext_valid_background_selector));
            editText.setPadding(10, 10, 10, 10);
            editText.setTextSize(14);
            editText.setError(null);
            editText.setTextColor(context.getResources()
                    .getColor(R.color.Black));
            return true;
        }

    }

    // check the input mobile has length 10 character text or not
    // return true if it contains text otherwise false
    public static boolean isValidMobileNumber(Context context,
                                              EditText editText, String errorMessage) {
        String text = editText.getText().toString().trim();

        if (text.length() != 10) {
            int ecolor = R.color.black;
            ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
                    errorMessage);
            ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
            editText.setError(ssbuilder);
            editText.setBackgroundDrawable(context.getResources().getDrawable(
                    R.drawable.edittext_error_background_selector));
            editText.setPadding(10, 10, 10, 10);
            editText.setTextSize(14);
            return false;
        } else {
            editText.setBackgroundDrawable(context.getResources().getDrawable(
                    R.drawable.edittext_valid_background_selector));
            editText.setPadding(10, 10, 10, 10);
            editText.setTextSize(14);
            editText.setError(null);
            editText.setTextColor(context.getResources()
                    .getColor(R.color.Black));
            return true;
        }

    }

    public static boolean isNumberValid(Context context, EditText editText,
                                        String errorMessage) {
        String text = editText.getText().toString().trim();
        if (!text.toString().startsWith("7")
                && !text.toString().startsWith("8")
                && !text.toString().startsWith("9")) {
            int ecolor = R.color.black;
            ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
                    errorMessage);
            ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
            editText.setError(ssbuilder);
            editText.setBackgroundDrawable(context.getResources().getDrawable(
                    R.drawable.edittext_error_background_selector));
            editText.setPadding(10, 10, 10, 10);
            editText.setTextSize(14);
            return false;
        } else {
            editText.setBackgroundDrawable(context.getResources().getDrawable(
                    R.drawable.edittext_valid_background_selector));
            editText.setPadding(10, 10, 10, 10);
            editText.setTextSize(14);
            editText.setError(null);
            editText.setTextColor(context.getResources()
                    .getColor(R.color.Black));
            return true;
        }
    }

    public static boolean isSubIdValid(Context context, EditText editText,
                                       String prefix, int len, int mOperatorId) {
        String subId = editText.getText().toString().trim();
        if (mOperatorId != 21) {
            if (mOperatorId == 20 || mOperatorId == 19) {
                if (prefix.indexOf(" or ") != -1) {
                    String[] prefixes = prefix.split("|");
                    if ((subId.startsWith(prefixes[0]) || subId.startsWith(prefixes[1])) && subId.length() == len) {
                        return true;
                    }
                }
            }
            if (subId.startsWith(prefix) && subId.length() == len) {
                return true;
            } else {

                String messagePrefix = "Sub Id should start with " + prefix
                        + " & Sub Id should should be " + len + " digit long";
                int ecolor = R.color.black;
                ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
                SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
                        messagePrefix);
                ssbuilder.setSpan(fgcspan, 0, messagePrefix.length(), 0);
                editText.setError(ssbuilder);
                editText.setBackgroundDrawable(context.getResources()
                        .getDrawable(
                                R.drawable.edittext_error_background_selector));
                editText.setPadding(10, 10, 10, 10);
                editText.setTextSize(14);
                return false;
            }
        } else if (mOperatorId == 21) {
            if (subId.length() >= 6 && subId.length() <= 9) {
                return true;
            } else {

                String messagePrefix = "Sub Id should be 6 to 9 digit long";
                int ecolor = R.color.black;
                ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
                SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
                        messagePrefix);
                ssbuilder.setSpan(fgcspan, 0, messagePrefix.length(), 0);
                editText.setError(ssbuilder);
                editText.setBackgroundDrawable(context.getResources()
                        .getDrawable(
                                R.drawable.edittext_error_background_selector));
                editText.setPadding(10, 10, 10, 10);
                editText.setTextSize(14);
                return false;
            }
        }
        return false;

    }

    public static boolean isUtilityEntryValid(Context context,
                                              EditText editText, String prefix, int len, int mOperatorId) {
        String subId = editText.getText().toString().trim();
        if (mOperatorId != 45 && mOperatorId != 46 && mOperatorId != 47) {

            if (subId.startsWith(prefix) && subId.length() == len) {
                return true;
            } else {
                if (mOperatorId == 48) {

                    String messagePrefix = "Please enter a valid  11 digit Contract Account Number starting with 6(e.g. 61100131066)";
                    int ecolor = R.color.black;
                    ForegroundColorSpan fgcspan = new ForegroundColorSpan(
                            ecolor);
                    SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
                            messagePrefix);
                    ssbuilder.setSpan(fgcspan, 0, messagePrefix.length(), 0);
                    editText.setError(ssbuilder);
                    editText.setBackgroundDrawable(context
                            .getResources()
                            .getDrawable(
                                    R.drawable.edittext_error_background_selector));
                    editText.setPadding(10, 10, 10, 10);
                    editText.setTextSize(14);
                } else if (mOperatorId == 49) {
                    String messagePrefix = "Please enter 11 digit valid telephone number with STD Code along with Prefix 0. (e.g. 08041149427 )";
                    int ecolor = R.color.black;
                    ForegroundColorSpan fgcspan = new ForegroundColorSpan(
                            ecolor);
                    SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
                            messagePrefix);
                    ssbuilder.setSpan(fgcspan, 0, messagePrefix.length(), 0);
                    editText.setError(ssbuilder);
                    editText.setBackgroundDrawable(context
                            .getResources()
                            .getDrawable(
                                    R.drawable.edittext_error_background_selector));
                    editText.setPadding(10, 10, 10, 10);
                    editText.setTextSize(14);

                } else if (mOperatorId == 50) {
                    String messagePrefix = "Please enter a valid 8 digit Telephone Number start with 2 (eg 24183797)";
                    int ecolor = R.color.black;
                    ForegroundColorSpan fgcspan = new ForegroundColorSpan(
                            ecolor);
                    SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
                            messagePrefix);
                    ssbuilder.setSpan(fgcspan, 0, messagePrefix.length(), 0);
                    editText.setError(ssbuilder);
                    editText.setBackgroundDrawable(context
                            .getResources()
                            .getDrawable(
                                    R.drawable.edittext_error_background_selector));
                    editText.setPadding(10, 10, 10, 10);
                    editText.setTextSize(14);
                } else if (mOperatorId == 51) {
                    String messagePrefix = "Please enter a valid 12 digit C.A. Number start with 21 (e.g. 210000253340)";
                    int ecolor = R.color.black;
                    ForegroundColorSpan fgcspan = new ForegroundColorSpan(
                            ecolor);
                    SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
                            messagePrefix);
                    ssbuilder.setSpan(fgcspan, 0, messagePrefix.length(), 0);
                    editText.setError(ssbuilder);
                    editText.setBackgroundDrawable(context
                            .getResources()
                            .getDrawable(
                                    R.drawable.edittext_error_background_selector));
                    editText.setPadding(10, 10, 10, 10);
                    editText.setTextSize(14);
                } else {

                    String messagePrefix = "Account number should start with "
                            + prefix + " & Account number should should be "
                            + len + " digit long";
                    int ecolor = R.color.black;
                    ForegroundColorSpan fgcspan = new ForegroundColorSpan(
                            ecolor);
                    SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
                            messagePrefix);
                    ssbuilder.setSpan(fgcspan, 0, messagePrefix.length(), 0);
                    editText.setError(ssbuilder);
                    editText.setBackgroundDrawable(context
                            .getResources()
                            .getDrawable(
                                    R.drawable.edittext_error_background_selector));
                    editText.setPadding(10, 10, 10, 10);
                    editText.setTextSize(14);
                }
                return false;
            }
        } else if (mOperatorId == 45 || mOperatorId == 46 || mOperatorId == 47) {
            if (subId.length() == 9) {
                return true;
            } else {

                String messagePrefix = "Please enter fixed 9 digit long Account No  (eg 212345678)";
                int ecolor = R.color.black;
                ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
                SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
                        messagePrefix);
                ssbuilder.setSpan(fgcspan, 0, messagePrefix.length(), 0);
                editText.setError(ssbuilder);
                editText.setBackgroundDrawable(context.getResources()
                        .getDrawable(
                                R.drawable.edittext_error_background_selector));
                editText.setPadding(10, 10, 10, 10);
                editText.setTextSize(14);
                return false;
            }
        }
        return false;

    }

    public static boolean isValidPin(Context context, EditText editText,
                                     String errorMessage) {
        String text = editText.getText().toString().trim();

        if (text.length() < 4) {
            int ecolor = R.color.black;
            ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
                    errorMessage);
            ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
            editText.setError(ssbuilder);
            editText.setBackgroundDrawable(context.getResources().getDrawable(
                    R.drawable.edittext_error_background_selector));
            editText.setPadding(10, 10, 10, 10);
            editText.setTextSize(14);
            return false;
        } else {
            editText.setBackgroundDrawable(context.getResources().getDrawable(
                    R.drawable.edittext_valid_background_selector));
            editText.setPadding(10, 10, 10, 10);
            editText.setTextSize(14);
            editText.setError(null);
            editText.setTextColor(context.getResources()
                    .getColor(R.color.Black));
            return true;
        }

    }

}