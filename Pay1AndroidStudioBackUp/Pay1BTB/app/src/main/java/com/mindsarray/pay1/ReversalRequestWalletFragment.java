package com.mindsarray.pay1;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.StatusAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.EditTextValidator;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ReversalRequestWalletFragment extends Fragment {

    View vi = null;
    private TextView mDateDisplay;
    // private Button btnCalender;
    private int mYear;
    private int mMonth;
    private int mDay;
    boolean bNetOrSms;
    private SharedPreferences mSettingsData;
    Bundle bundle;
    private Button btnSearch;
    StringBuilder builder;
    ListView mStatusListView;
    static final int DATE_DIALOG_ID = 0;
    ArrayList<String> statusList;
    StatusAdapter adapter;
    EditText editMobileNum;
    private static final String TAG = "Reversal request";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//		analytics = GoogleAnalytics.getInstance(getActivity());
        tracker = Constants.setAnalyticsTracker(getActivity(), tracker);//easyTracker.set(Fields.SCREEN_NAME, TAG);
        vi = inflater.inflate(R.layout.request_activity, container, false);

        // btnCalender = (Button) vi.findViewById(R.id.btnCalender);
        mDateDisplay = (TextView) vi.findViewById(R.id.mDateDisplay);
        mStatusListView = (ListView) vi.findViewById(R.id.mStatusListView);
        TextView textView_NoData = (TextView) vi
                .findViewById(R.id.textView_NoData);
        mStatusListView.setEmptyView(textView_NoData);

		/*
         * bundle = getActivity().getIntent().getExtras(); if
		 * (Utility.getLoginFlag(context, Constants.SHAREDPREFERENCE_IS_LOGIN))
		 * { mInsideonCreate();
		 * 
		 * } else { Intent intent = new Intent(getActivity(),
		 * LoginActivity.class); intent.putExtra(Constants.REQUEST_FOR, bundle);
		 * startActivityForResult(intent, Constants.LOGIN);
		 * 
		 * }
		 */

        bundle = getActivity().getIntent().getExtras();
        mSettingsData = getActivity().getSharedPreferences(
                SettingsSmsActivity.SETTINGS_NAME, 0);
        bNetOrSms = mSettingsData.getBoolean(SettingsSmsActivity.KEY_NETWORK,
                true);

        if (bNetOrSms) {
            if (Utility.getLoginFlag(getActivity(),
                    Constants.SHAREDPREFERENCE_IS_LOGIN)) {
                mInsideonCreate();

            } else {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.putExtra(Constants.REQUEST_FOR, bundle);
                startActivityForResult(intent, Constants.LOGIN);

            }
        } else {
            // mInsideonCreate();
            Constants
                    .showTwoButtonFailureDialog(
                            getActivity(),
                            "Internet setting is not enabled.\nWould you like to change settings?",
                            "Settings", Constants.SMS_SETTINGS);
        }

        return vi;

    }

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        //tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
    }

    public static ReversalRequestWalletFragment newInstance(
            String content) {
        ReversalRequestWalletFragment fragment = new ReversalRequestWalletFragment();

        Bundle b = new Bundle();
        b.putString("msg", content);

        fragment.setArguments(b);

        return fragment;
    }

    // private String mContent = "???";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // if ((savedInstanceState != null)
        // && savedInstanceState.containsKey(KEY_CONTENT)) {
        // mContent = savedInstanceState.getString(KEY_CONTENT);
        // }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // outState.putString(KEY_CONTENT, mContent);
    }

    private void mInsideonCreate() {

        // TODO Auto-generated method stub

        findViewById();
        statusList = new ArrayList<String>();
        new GetRequestTask().execute();
        btnSearch.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!EditTextValidator.hasText(getActivity(), editMobileNum,
                        Constants.ERROR_MOBILE_BLANK_FIELD)
                        || !EditTextValidator.isValidMobileNumber(
                        getActivity(), editMobileNum,
                        Constants.ERROR_MOBILE_LENGTH_FIELD)) {
                } else {
                    new GetSearchTask().execute();
                }
            }
        });
        adapter = new StatusAdapter(getActivity(), statusList, 1);
		/*
		 * if (bundle.getInt(Constants.REQUEST_FOR) == Constants.MOBILE_TAB) {
		 * adapter = new StatusAdapter(this, statusList, 1); } else if
		 * (bundle.getInt(Constants.REQUEST_FOR) == Constants.DTH_TAB) { adapter
		 * = new StatusAdapter(this, statusList, 2); } else if
		 * (bundle.getInt(Constants.REQUEST_FOR) == Constants.ENTERTAINMENT_TAB)
		 * { adapter = new StatusAdapter(this, statusList, 1); } else if
		 * (bundle.getInt(Constants.REQUEST_FOR) == Constants.BILL_TAB) {
		 * adapter = new StatusAdapter(this, statusList, 1); }
		 */

        mStatusListView.setAdapter(adapter);

    }

    private void findViewById() {
        btnSearch = (Button) vi.findViewById(R.id.btnSearch);

        mStatusListView = (ListView) vi.findViewById(R.id.mStatusListView);
        editMobileNum = (EditText) vi.findViewById(R.id.editMobileNum);
    }

    private void updateDisplay() {
        builder = new StringBuilder().append(mYear).append("-")
                .append(mMonth + 1).append("-").append(mDay);
        mDateDisplay.setText(builder.toString());
        new GetStatus().execute();
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            mYear = year;
            mMonth = monthOfYear;
            mDay = dayOfMonth;
            updateDisplay();
        }
    };

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                DatePickerDialog dialog = new DatePickerDialog(getActivity(),
                        mDateSetListener, mYear, mMonth, mDay);

			/*
			 * if (Build.VERSION.SDK_INT >= 11) { // (picker is a DatePicker)
			 * dialog.getDatePicker().setMaxDate(new Date().getTime()); } else {
			 * 
			 * }
			 */

                return dialog;
			/*
			 * return new DatePickerDialog(this, mDateSetListener, mYear,
			 * mMonth, mDay);
			 */
        }
        return null;
    }

    public class GetStatus extends AsyncTask<String, String, String> implements
            OnDismissListener {

        ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method",
                    "reversalTransactions"));
            listValuePair.add(new BasicNameValuePair("date", builder.toString()
                    .trim()));
			/*
			 * if (bundle.getInt(Constants.REQUEST_FOR) == Constants.MOBILE_TAB)
			 * { listValuePair.add(new BasicNameValuePair("service", "1")); }
			 * else if (bundle.getInt(Constants.REQUEST_FOR) ==
			 * Constants.DTH_TAB) { listValuePair.add(new
			 * BasicNameValuePair("service", "2")); } else if
			 * (bundle.getInt(Constants.REQUEST_FOR) ==
			 * Constants.ENTERTAINMENT_TAB) { listValuePair.add(new
			 * BasicNameValuePair("service", "3")); } else if
			 * (bundle.getInt(Constants.REQUEST_FOR) == Constants.BILL_TAB) {
			 * listValuePair.add(new BasicNameValuePair("service", "4")); }
			 */
            listValuePair.add(new BasicNameValuePair("service", "5"));
            listValuePair.add(new BasicNameValuePair("device_type", "android"));
            String response = RequestClass.getInstance().readPay1Request(
                    getActivity(), Constants.API_URL, listValuePair);
            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            super.onPostExecute(result);
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    statusList.clear();
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray jsonDescription = jsonObject
                                .getJSONArray("description");
                        JSONArray array2 = jsonDescription.getJSONArray(0);
                        for (int i = 0; i < array2.length(); i++) {
                            JSONObject json_data = array2.getJSONObject(i);
                            statusList.add(json_data.toString());

                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        Constants.showOneButtonFailureDialog(getActivity(),
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(getActivity(),
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(getActivity(),
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(getActivity(),
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(getActivity());
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            GetStatus.this.cancel(true);
                            dialog.cancel();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            GetStatus.this.cancel(true);
            dialog.cancel();
        }
    }

    public class GetRequestTask extends AsyncTask<String, String, String>
            implements OnDismissListener {
        ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method", "lastten"));

			/*
			 * if (bundle.getInt(Constants.REQUEST_FOR) == Constants.MOBILE_TAB)
			 * { listValuePair.add(new BasicNameValuePair("service", "1")); }
			 * else if (bundle.getInt(Constants.REQUEST_FOR) ==
			 * Constants.DTH_TAB) { listValuePair.add(new
			 * BasicNameValuePair("service", "2")); } else if
			 * (bundle.getInt(Constants.REQUEST_FOR) ==
			 * Constants.ENTERTAINMENT_TAB) { listValuePair.add(new
			 * BasicNameValuePair("service", "3")); } else if
			 * (bundle.getInt(Constants.REQUEST_FOR) == Constants.BILL_TAB) {
			 * listValuePair.add(new BasicNameValuePair("service", "4")); }
			 */
            listValuePair.add(new BasicNameValuePair("service", "5"));
            listValuePair.add(new BasicNameValuePair("device_type", "android"));

            String response = RequestClass.getInstance().readPay1Request(
                    getActivity(), Constants.API_URL, listValuePair);
            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            super.onPostExecute(result);
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    statusList.clear();
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray jsonDescription = jsonObject
                                .getJSONArray("description");
                        JSONArray array2 = jsonDescription.getJSONArray(0);
                        for (int i = 0; i < array2.length(); i++) {
                            JSONObject json_data = array2.getJSONObject(i);
                            statusList.add(json_data.toString());

                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        Constants.showOneButtonFailureDialog(getActivity(),
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(getActivity(),
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(getActivity(),
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(getActivity(),
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(getActivity());
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            GetRequestTask.this.cancel(true);
                            dialog.cancel();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            GetRequestTask.this.cancel(true);
            dialog.cancel();
        }
    }

    public class GetSearchTask extends AsyncTask<String, String, String>
            implements OnDismissListener {

        ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method",
                    "mobileTransactions"));

            listValuePair.add(new BasicNameValuePair("mobile", editMobileNum
                    .getText().toString()));
            listValuePair.add(new BasicNameValuePair("service", "5"));

            String response = RequestClass.getInstance().readPay1Request(
                    getActivity(), Constants.API_URL, listValuePair);
            listValuePair.clear();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            super.onPostExecute(result);
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    statusList.clear();
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray jsonDescription = jsonObject
                                .getJSONArray("description");
                        JSONArray array2 = jsonDescription.getJSONArray(0);
                        for (int i = 0; i < array2.length(); i++) {
                            JSONObject json_data = array2.getJSONObject(i);
                            statusList.add(json_data.toString());

                        }
                        // Log.d("Arra", "Arra");
                        adapter.notifyDataSetChanged();
                    } else {
                        Constants.showOneButtonFailureDialog(getActivity(),
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(getActivity(),
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(getActivity(),
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(getActivity(),
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(getActivity());
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            GetSearchTask.this.cancel(true);
                            dialog.cancel();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            GetSearchTask.this.cancel(true);
            dialog.cancel();
        }
    }

}
