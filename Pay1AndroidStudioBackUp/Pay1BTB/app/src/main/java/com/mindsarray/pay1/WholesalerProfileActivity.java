package com.mindsarray.pay1;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.contenthandler.Post;
import com.mindsarray.pay1.customviews.GiftImageGalleryLayout;
import com.mindsarray.pay1.customviews.InteractiveScrollView;
import com.mindsarray.pay1.customviews.InteractiveScrollView.OnScrollViewListener;
import com.mindsarray.pay1.databasehandler.PostDBHelper;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.GoogleAnalyticsHelper;
import com.mindsarray.pay1.utilities.Utility;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class WholesalerProfileActivity extends Activity {
    ImageView img_cover_photo, imageView_Back, img_profile_photo;
    TextView textViewWholesalerName, textViewWebAdderess, textView_Title;
    TextView textViewDescription;
    Button buttonMakeACall, buttonCall;
    RelativeLayout back_layout;
    LinearLayout linearLayout_Main, linear_listview, galleryLayout,
            galleryDivider, callButtonLayout, placeHolderLayout;
    Context mContext;

    String company_name, logo_url, contact_no, cover_photo, description;
    String wholeSalerId, phoneNumber;
    InteractiveScrollView scrollview;
    Post mPost;
    float botButton;
    PostDBHelper postDBHelper;
    GoogleAnalyticsHelper mGoogleHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wholesaler_profile_activity);
        mContext = WholesalerProfileActivity.this;
        img_cover_photo = (ImageView) findViewById(R.id.img_cover_photo);
        imageView_Back = (ImageView) findViewById(R.id.imageView_Back);
        img_profile_photo = (ImageView) findViewById(R.id.img_profile_photo);
        textViewWholesalerName = (TextView) findViewById(R.id.textViewWholesalerName);
        textViewWebAdderess = (TextView) findViewById(R.id.textView_Title);
        textViewDescription = (TextView) findViewById(R.id.textViewDescription);
        buttonMakeACall = (Button) findViewById(R.id.buttonMakeACall);
        buttonCall = (Button) findViewById(R.id.buttonCall);
        linearLayout_Main = (LinearLayout) findViewById(R.id.linearLayout_Main);
        linear_listview = (LinearLayout) findViewById(R.id.linear_listview);
        galleryLayout = (LinearLayout) findViewById(R.id.galleryLayout);
        galleryDivider = (LinearLayout) findViewById(R.id.galleryDivider);
        callButtonLayout = (LinearLayout) findViewById(R.id.callButtonLayout);
        placeHolderLayout = (LinearLayout) findViewById(R.id.placeHolderLayout);
        postDBHelper = new PostDBHelper(mContext);
        back_layout = (RelativeLayout) findViewById(R.id.back_layout);
        if (getIntent().getExtras() != null) {
            wholeSalerId = getIntent().getExtras().getString("wholeSalerId");
            mPost = postDBHelper.getWholesalerData(wholeSalerId);
            new GetProfileTask().execute(wholeSalerId);
        } else {
            finish();
        }
        scrollview = (InteractiveScrollView) findViewById(R.id.scrollview);
        back_layout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });
        /*
		 * scrollview.getViewTreeObserver().addOnScrollChangedListener(new
		 * OnScrollChangedListener() {
		 * 
		 * @Override public void onScrollChanged() {
		 * buttonCall.setVisibility(View.INVISIBLE); } });
		 */

        ViewTreeObserver vto = scrollview.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                buttonMakeACall.getViewTreeObserver()
                        .removeGlobalOnLayoutListener(this);
                botButton = buttonMakeACall.getX() + 150;
                Log.d("s", "ddd" + botButton);
                int width = buttonMakeACall.getMeasuredWidth();
                int height = buttonMakeACall.getMeasuredHeight();

            }
        });

        buttonMakeACall.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                callMethod();
            }
        });

        buttonCall.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                callMethod();
            }
        });
        scrollview.setOnScrollViewListener(new OnScrollViewListener() {
            public void onScrollChanged(InteractiveScrollView v, int l, int t,
                                        int oldl, int oldt) {
                Log.d("Scroller", "I changed!");

                if (t > botButton) {
                    callButtonLayout.setVisibility(View.VISIBLE);
                    RelativeLayout.LayoutParams layoutParams = (LayoutParams) scrollview.getLayoutParams();
                    layoutParams.setMargins(0, 0, 0, callButtonLayout.getHeight() + 20);

                } else {
                    callButtonLayout.setVisibility(View.GONE);
                    RelativeLayout.LayoutParams layoutParams = (LayoutParams) scrollview.getLayoutParams();
                    layoutParams.setMargins(0, 0, 0, 0);
                }

            }
        });
        InitGoogleAnalytics();

        mGoogleHelper.SendScreenNameGoogleAnalytics("Wholesaler Profile", mContext);
    }

    private void InitGoogleAnalytics() {
        mGoogleHelper = new GoogleAnalyticsHelper();
        mGoogleHelper.init(WholesalerProfileActivity.this);
    }

    public class GetProfileTask extends AsyncTask<String, String, String>
            implements OnDismissListener {
        private static final String TAG = "C2D";
        ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("retailerID", Utility
                    .getSharedPreferences(mContext,
                            Constants.SHAREDPREFERENCE_RETAILER_ID)));
            listValuePair
                    .add(new BasicNameValuePair("wholeSalerId", params[0]));

            String response = RequestClass.getInstance().readPay1Request(
                    mContext, Constants.C2D_URL + "ws/profile", listValuePair);
            listValuePair.clear();

            Log.e("C2D Posts:", "" + response);

            return response;
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            try {
                if (!result.startsWith("Error")) {

                    JSONObject responseObject = new JSONObject(result);

                    String status = responseObject.getString("status");
                    String type = responseObject.getString("type");
                    if (status.equalsIgnoreCase("success")) {

                        boolean booleanType = Boolean.parseBoolean(type);
                        if (booleanType == true) {

                            JSONObject dataObject = responseObject
                                    .getJSONObject("data");
                            company_name = dataObject.getString("company_name");
                            logo_url = dataObject.getString("logo_url");
                            contact_no = dataObject.getString("contact_no");
                            cover_photo = dataObject.getString("cover_photo");
                            description = dataObject.getString("description");
                            textViewDescription.setText(description);


                            textViewDescription.post(new Runnable() {
                                @Override
                                public void run() {
                                    int lineCount = textViewDescription.getLineCount();
                                    // Use lineCount here

                                    if (lineCount > 3) {
                                        makeTextViewResizable(textViewDescription, 3, "Read More", true);
                                    } else {

                                    }
                                }
                            });
							/*textViewDescription.setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									scrollview.fullScroll(ScrollView.FOCUS_UP);
								}
							});*/
                            textViewWholesalerName.setText(company_name);

                            try {
                                Picasso.with(mContext).load(logo_url)
                                        .placeholder(R.drawable.default_logo)
                                        .into(img_profile_photo);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
                                Picasso.with(mContext)
                                        .load(cover_photo)
                                        .placeholder(
                                                R.drawable.default_cover_wholesaler)
                                        .into(img_cover_photo);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            JSONArray jsonArrayImage = dataObject
                                    .getJSONArray("gallery");
                            if (jsonArrayImage.length() != 0) {
                                galleryLayout.setVisibility(View.VISIBLE);
                                galleryDivider.setVisibility(View.VISIBLE);
                                HorizontalScrollView hs = new HorizontalScrollView(
                                        WholesalerProfileActivity.this);
                                GiftImageGalleryLayout giftCell = new GiftImageGalleryLayout(
                                        WholesalerProfileActivity.this);

                                hs.addView(giftCell);
                                linearLayout_Main.addView(hs);

                                for (int i = 0; i < jsonArrayImage.length(); i++)
                                    giftCell.add(jsonArrayImage.getString(i));

                            } else {
                                galleryDivider.setVisibility(View.GONE);
                                galleryLayout.setVisibility(View.GONE);

                            }

                            final JSONArray postsArray = dataObject
                                    .getJSONArray("posts");

                            for (int i = 0; i < postsArray.length(); i++) {
                                /**
                                 * inflate items/ add items in linear layout
                                 * instead of listview
                                 */
                                LayoutInflater inflater = null;
                                inflater = (LayoutInflater) getApplicationContext()
                                        .getSystemService(
                                                Context.LAYOUT_INFLATER_SERVICE);
                                View mLinearView = inflater.inflate(
                                        R.layout.profile_post_row, null);
                                ImageView post_images = (ImageView) mLinearView
                                        .findViewById(R.id.post_images);
                                final Button btn_interested = (Button) mLinearView
                                        .findViewById(R.id.btn_interested);

                                final TextView post_description = (TextView) mLinearView
                                        .findViewById(R.id.post_description);

                                try {
                                    final JSONObject jsonObject = postsArray
                                            .getJSONObject(i);

                                    if (jsonObject.getString(
                                            "is_still_interested")
                                            .equalsIgnoreCase("1")) {
                                        btn_interested.setText("INTERESTED");
                                        btn_interested
                                                .setTextColor(Color.WHITE);
                                        btn_interested
                                                .setBackgroundColor(mContext
                                                        .getResources()
                                                        .getColor(
                                                                R.color.pay1_red));

                                        Drawable d = mContext
                                                .getResources()
                                                .getDrawable(
                                                        R.drawable.white_heart_icon);
                                        d.setBounds(new Rect(0, 0, 0, 0));
                                        btn_interested
                                                .setCompoundDrawablesWithIntrinsicBounds(
                                                        d, null, null, null);
                                        btn_interested.setEnabled(false);
										/*
										 * viewHolder.btn_interested.
										 * setCompoundDrawables(mContext
										 * .getResources
										 * ().getDrawable(R.drawable
										 * .white_heart_icon), null, null,
										 * null);
										 */
                                    }

                                    post_description.setText(jsonObject
                                            .getString("description"));
                                    post_description.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            int lineCount = post_description.getLineCount();
                                            // Use lineCount here

                                            if (lineCount > 3) {
                                                makeTextViewResizable(post_description, 3, "Read More", true);
                                            } else {

                                            }
                                        }
                                    });
                                    final JSONArray imagesArray = jsonObject
                                            .getJSONArray("images");

                                    post_images
                                            .setOnClickListener(new OnClickListener() {

                                                @Override
                                                public void onClick(View v) {
                                                    // TODO Auto-generated
                                                    // method stub
                                                    try {
                                                        mGoogleHelper.SendEventGoogleAnalytics(
                                                                mContext,
                                                                String.valueOf(mPost.getmId()) + " "
                                                                        + mPost.getmPostTitle(), "Post Image Profile Clicked",
                                                                "ButtonEvent");
                                                        ArrayList<String> imageArrayList = new ArrayList<String>();
                                                        try {

                                                            for (int i = 0; i < imagesArray
                                                                    .length(); i++) {
                                                                imageArrayList
                                                                        .add(imagesArray
                                                                                .getString(i));
                                                            }
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                        postDBHelper.addStatsToTable(mPost.getmId(), Utility
                                                                .getSharedPreferences(mContext,
                                                                        Constants.SHAREDPREFERENCE_RETAILER_ID));
                                                        Intent slideIntent = new Intent(
                                                                mContext,
                                                                ImagesSlideActivity.class);

                                                        Bundle b = new Bundle();
                                                        b.putInt("position", 0);
                                                        b.putStringArrayList(
                                                                "Array",
                                                                imageArrayList);
                                                        b.putInt("requestFrom",
                                                                0);
                                                        slideIntent
                                                                .putExtras(b);
                                                        mContext.startActivity(slideIntent);

                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            });

                                    try {
                                        Picasso.with(mContext)
                                                .load(imagesArray.getString(0)
                                                        .toString())
                                                .placeholder(
                                                        R.drawable.product_loading)
                                                .into(post_images);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    linear_listview.addView(mLinearView);

                                    btn_interested
                                            .setOnClickListener(new OnClickListener() {

                                                @Override
                                                public void onClick(View v) {
                                                    // TODO Auto-generated
                                                    // method
                                                    // stub
                                                    mGoogleHelper.SendEventGoogleAnalytics(
                                                            mContext,
                                                            String.valueOf(mPost.getmId()) + " "
                                                                    + mPost.getmPostTitle(), "Interest Button Profile Clicked",
                                                            "ButtonEvent");
                                                    String postID;
                                                    try {
                                                        postID = jsonObject
                                                                .getString("postID");
                                                        ShowInterestDialog(postID, btn_interested);
                                                    } catch (JSONException e) {
                                                        // TODO Auto-generated
                                                        // catch block
                                                        e.printStackTrace();
                                                    }

                                                }
                                            });

                                } catch (Exception exc) {

                                }

                            }

                        } else {
                            Constants.showOneButtonFailureDialog(mContext,
                                    Constants.checkCode(result), TAG,
                                    Constants.OTHER_ERROR);
                        }

                    } else {
                        Constants.showOneButtonFailureDialog(mContext,
                                Constants.checkCode(result), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(mContext,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(mContext,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                Constants.showOneButtonFailureDialog(mContext,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
            placeHolderLayout.setVisibility(View.GONE);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(mContext);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            GetProfileTask.this.cancel(true);
                            dialog.cancel();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            GetProfileTask.this.cancel(true);
            dialog.cancel();
        }
    }

    public class ShowInterestTask extends AsyncTask<String, String, String>
            implements OnDismissListener {
        private static final String TAG = "C2D";
        ProgressDialog dialog;

        Button btn_interested;


        public ShowInterestTask(Button btn_interested) {
            super();
            this.btn_interested = btn_interested;
        }

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("retailerID", Utility
                    .getSharedPreferences(mContext,
                            Constants.SHAREDPREFERENCE_RETAILER_ID)));
            listValuePair.add(new BasicNameValuePair("postId", params[0]));
            listValuePair.add(new BasicNameValuePair("message", params[1]));

            String response = RequestClass.getInstance().readPay1Request(
                    mContext, Constants.C2D_URL + "posts/interests",
                    listValuePair);
            listValuePair.clear();

            Log.e("C2D Posts:", "" + response);

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            super.onPostExecute(result);
            try {
                if (!result.startsWith("Error")) {

                    JSONObject responseObject = new JSONObject(result);

                    String status = responseObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        if (responseObject.getBoolean("type")) {
                            Constants.showOneButtonSuccessDialog(mContext,
                                    responseObject.getString("msg"), Constants.C2D_SETTINGS);


                            this.btn_interested.setText("INTERESTED");
                            this.btn_interested
                                    .setTextColor(Color.WHITE);
                            this.btn_interested
                                    .setBackgroundColor(mContext
                                            .getResources()
                                            .getColor(
                                                    R.color.pay1_red));

                            Drawable d = mContext
                                    .getResources()
                                    .getDrawable(
                                            R.drawable.white_heart_icon);
                            d.setBounds(new Rect(0, 0, 0, 0));
                            this.btn_interested
                                    .setCompoundDrawablesWithIntrinsicBounds(
                                            d, null, null, null);
                            this.btn_interested.setEnabled(false);
                            postDBHelper.UpdatePost(mPost.getmId());
                        } else {
                            Constants.showOneButtonFailureDialog(mContext,
                                    responseObject.getString("errorMsg"), TAG,
                                    Constants.OTHER_ERROR);
                        }
                    } else {
                        Constants.showOneButtonFailureDialog(mContext,
                                Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(mContext,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(mContext,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                Constants.showOneButtonFailureDialog(mContext,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(mContext);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            ShowInterestTask.this.cancel(true);
                            dialog.cancel();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            ShowInterestTask.this.cancel(true);
            dialog.cancel();
        }
    }

    public void ShowInterestDialog(final String postId, final Button btn_interested) {
        final Dialog mDialog = new Dialog(mContext);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        mDialog.setContentView(R.layout.show_interested_dialog);
        Button buttonInterest = (Button) mDialog
                .findViewById(R.id.buttonInterest);
        ImageView imageViewClose = (ImageView) mDialog
                .findViewById(R.id.imageViewClose);
        final EditText editTextMsg = (EditText) mDialog
                .findViewById(R.id.editTextMsg);
        editTextMsg.getHint();
        buttonInterest.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mDialog.dismiss();
                if (editTextMsg.getText().toString().length() != 0) {

                    new ShowInterestTask(btn_interested).execute(postId, editTextMsg
                            .getText().toString());
                } else {
                    new ShowInterestTask(btn_interested).execute(postId, editTextMsg
                            .getHint().toString());
                }
            }
        });

        imageViewClose.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    public void callMethod() {


        mGoogleHelper.SendEventGoogleAnalytics(
                mContext,
                String.valueOf(mPost.getmId()) + " "
                        + mPost.getmPostTitle(), "Call Button Clicked",
                "ButtonEvent");

        if (android.os.Build.VERSION.SDK_INT >= 23) {
            // only for gingerbread and newer versions
            //String permission = Manifest.permission.CAMERA;
            String permission = "android.permission.CALL_PHONE";
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(WholesalerProfileActivity.this, new
                        String[]{permission}, 100);
            } else {
                // Add your function here which open camera
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + Constants.C2D_SUPPORT_CALL));
                mContext.startActivity(callIntent);
            }
        } else {
            // Add your function here which open camera
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + Constants.C2D_SUPPORT_CALL));
            mContext.startActivity(callIntent);
        }



/*
		String permission = "android.permission.CALL_PHONE";
		int res = mContext.checkCallingOrSelfPermission(permission);
		if (res == PackageManager.PERMISSION_GRANTED) {

			Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.setData(Uri.parse("tel:"+Constants.C2D_SUPPORT_CALL));
			mContext.startActivity(callIntent);
		}*/
		/*
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                WholesalerProfileActivity.this);

        alertDialog.setTitle("Wholesaler");

        alertDialog.setMessage("Do you want to call the wholesaler?");

        //alertDialog.setIcon(R.drawable.ic_launcher);

        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                            int which) {
                    	new ClickToCallWholesalerTask().execute();
                    }
                });
           
            alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                            int which) {
                   
                    }
                });
        alertDialog.show();


		
		showTwoButtonDialog(
				mContext,
				"Do you want the wholesaler to call you on "
						+ Utility
								.getUserMobileNumber(mContext)
						+ " ?", "Confirmation", "Yes",
				"No", 0, mPost.getmId(),
				mPost.getmClientId(), mPost.getmContactNo());
		
		*/


    }


    private void showTwoButtonDialog(final Context context, String message,
                                     String title, String buttonOK, String buttonCancel, final int type,
                                     final int postId, final String wsId, final String wsContactNumber) {
        try {
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_two_button);

            TextView textView_Title = (TextView) dialog
                    .findViewById(R.id.textView_Title);
            TextView text = (TextView) dialog
                    .findViewById(R.id.textView_Message);
            text.setText(message);
            textView_Title.setText(title);

            Button dialogButtonOk = (Button) dialog
                    .findViewById(R.id.button_Ok);
            dialogButtonOk.setText(buttonOK);
            dialogButtonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    new ClickToCallWholesalerTask().execute(
                            String.valueOf(postId), wsId, wsContactNumber);
                }
            });

            Button dialogButtonCancel = (Button) dialog
                    .findViewById(R.id.button_Cancel);
            dialogButtonCancel.setText(buttonCancel);
            dialogButtonCancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        } catch (Exception exception) {
        }
    }


    public class ClickToCallWholesalerTask extends
            AsyncTask<String, String, String> implements OnDismissListener {
        private static final String TAG = "C2D";
        ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("retailerID", Utility
                    .getSharedPreferences(mContext,
                            Constants.SHAREDPREFERENCE_RETAILER_ID)));
            listValuePair.add(new BasicNameValuePair("postId", "0"));
            listValuePair.add(new BasicNameValuePair("retailermobile", Utility
                    .getUserMobileNumber(mContext)));
            listValuePair.add(new BasicNameValuePair("wsmobile", mPost
                    .getmContactNo()));
            listValuePair.add(new BasicNameValuePair("wholesaler_id", mPost
                    .getmClientId()));

            String response = RequestClass.getInstance().readPay1Request(
                    mContext, Constants.C2D_URL + "posts/clickToCall",
                    listValuePair);
            listValuePair.clear();

            Log.e("C2D Posts:", "" + response);

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            super.onPostExecute(result);
            try {
                if (!result.startsWith("Error")) {

                    JSONObject responseObject = new JSONObject(result);

                    String status = responseObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        if (responseObject.getBoolean("type")) {
                            Constants.showOneButtonSuccessDialog(mContext,
                                    responseObject.getString("msg"),
                                    Constants.C2D_SETTINGS);
							/*
							 * PostDBHelper postDBHelper = new PostDBHelper(
							 * mContext); postDBHelper.UpdatePost(PostIdUpdate);
							 * notifyDataSetChanged();
							 */
                        } else {
                            Constants.showOneButtonFailureDialog(mContext,
                                    responseObject.getString("errorMsg"), TAG,
                                    Constants.OTHER_ERROR);
                        }
                    } else {
                        Constants.showOneButtonFailureDialog(mContext,
                                Constants.checkCode(result), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(mContext,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(mContext,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                Constants.showOneButtonFailureDialog(mContext,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(mContext);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            ClickToCallWholesalerTask.this.cancel(true);
                            dialog.cancel();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            ClickToCallWholesalerTask.this.cancel(true);
            dialog.cancel();
        }
    }
	
	/*private void cycleTextViewExpansion(TextView tv){
	    int collapsedMaxLines = 3;
	    ObjectAnimator animation = ObjectAnimator.ofInt(tv, "maxLines", 
	        tv.getMaxLines() == collapsedMaxLines? tv.getLineCount() : collapsedMaxLines);
	    animation.setDuration(200).start();
	}
	
	private void expandTextView(TextView tv){
	    ObjectAnimator animation = ObjectAnimator.ofInt(tv, "maxLines", tv.getLineCount());
	    animation.setDuration(200).start();
	}

	private void collapseTextView(TextView tv, int numLines){
	    ObjectAnimator animation = ObjectAnimator.ofInt(tv, "maxLines", numLines);
	    animation.setDuration(200).start();
	}*/


    public void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), BufferType.SPANNABLE);
                }
            }
        });

    }

    private SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                     final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new ClickableSpan() {

                @Override
                public void onClick(View widget) {

                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, -1, "Read Less", false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, 3, "Read More", true);
                    }

                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }

}
