package com.mindsarray.pay1.servicehandler;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.mindsarray.pay1.adapterhandler.SyncAdapter;

/**
 * https://developer.android.com/training/sync-adapters/creating-sync-adapter.html
 **/

public class SyncService extends Service {

    private static SyncAdapter sSyncAdapter = null;
    private static final Object sSyncAdapterLock = new Object();

    @Override
    public void onCreate() {
        synchronized (sSyncAdapterLock) {
            if (sSyncAdapter == null) {
                sSyncAdapter = new SyncAdapter(getApplicationContext(), true);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return sSyncAdapter.getSyncAdapterBinder();
    }
}