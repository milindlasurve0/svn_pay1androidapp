package com.mindsarray.pay1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.utilities.Utility;

import java.util.Locale;


public class WholesaleNotificationActivity extends Activity {
    CheckBox chkOn, chkOff;
    Locale myLocale;
    Button buttonSave;
    Context mContext = WholesaleNotificationActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wholesale_notification_layout);

        buttonSave = (Button) findViewById(R.id.buttonSave);
        chkOn = (CheckBox) findViewById(R.id.chkOn);
        chkOff = (CheckBox) findViewById(R.id.chkOff);

        String wsNotificationPreference = Utility
                .getWholesaleNotificationSharedPreferences(WholesaleNotificationActivity.this);
        if (wsNotificationPreference.equalsIgnoreCase("0")) {
            chkOff.setChecked(true);
            chkOn.setChecked(false);
        } else {
            chkOff.setChecked(false);
            chkOn.setChecked(true);
        }

        chkOn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                chkOff.setChecked(!isChecked);
                chkOn.setChecked(isChecked);
            }
        });

        chkOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                chkOff.setChecked(isChecked);
                chkOn.setChecked(!isChecked);
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Runnable buttonNow = new Runnable() {
                    @Override
                    public void run() {
                        if (chkOff.isChecked()) {
                            Utility.setWholesaleNotificationSharedPreference(
                                    WholesaleNotificationActivity.this, "0");

                        } else if (chkOn.isChecked()) {

                            Utility.setWholesaleNotificationSharedPreference(
                                    WholesaleNotificationActivity.this, "1");
                        }
                        finish();
                    }
                };

                if (chkOff.isChecked()) {
                    String message = "Are you sure you want to stop receiving wholesale notifications?";
                    Constants.showTwoButtonDialog(mContext, message, "Change Notification", "Confirm",
                            buttonNow, "Cancel");

                } else if (chkOn.isChecked()) {
                    String message = "Are you sure you want to start receiving wholesale " +
                            "notifications?";
                    Constants.showTwoButtonDialog(mContext, message, "Change Notification", "Confirm",
                            buttonNow, "Cancel");
                }

            }
        });

    }

    public void setLocale(String lang) {

        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, WholesaleNotificationActivity.class);
        startActivity(refresh);
        finish();

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Constants.showNavigationBar(WholesaleNotificationActivity.this);
        super.onResume();
    }

}
