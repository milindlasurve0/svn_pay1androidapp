package com.mindsarray.pay1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.utilities.Utility;

public class EzetapDashBoardActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ezetap_dashboard_activity);

        Button button = (Button) findViewById(R.id.btnTransaction);

        Button cashTxnButton = (Button) findViewById(R.id.cashTxnButton);

        Button saleTxnButton = (Button) findViewById(R.id.saleTxnButton);


        final String ezeTapDeviceSerialNumber = Utility.getSharedPreferences(
                EzetapDashBoardActivity.this,
                Constants.SHAREDPREFERENCE_DEVICE_SERIAL_NO);
        Log.d("SHAREDPREFERENCE_DEVICE_SERIAL_NO",
                "SHAREDPREFERENCE_DEVICE_SERIAL_NO" + ezeTapDeviceSerialNumber);

        saleTxnButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (ezeTapDeviceSerialNumber.isEmpty()) {
                    /*
					 * Constants .showOneButtonDialog(
					 * EzetapDashBoardActivity.this, "Mini ATM",
					 * "Please contact your distributor to enable this service."
					 * , "OK", 1);
					 */

                    Intent intent = new Intent(EzetapDashBoardActivity.this,
                            EzetapLeadActivity.class);
                    startActivity(intent);
                } else {

                    Intent intent = new Intent(EzetapDashBoardActivity.this,
                            EzetapPosAtmActivity.class);
                    intent.putExtra("SaleOrCash", "56");
                    intent.putExtra("title", "Card Payment");
                    startActivity(intent);
                }
            }
        });

        cashTxnButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (ezeTapDeviceSerialNumber.isEmpty()) {
					/*
					 * Constants .showOneButtonDialog(
					 * EzetapDashBoardActivity.this, "Mini ATM",
					 * "Please contact your distributor to enable this service."
					 * , "OK", 1);
					 */

                    Intent intent = new Intent(EzetapDashBoardActivity.this,
                            EzetapLeadActivity.class);
                    startActivity(intent);
                } else {

                    Intent intent = new Intent(EzetapDashBoardActivity.this,
                            EzetapPosAtmActivity.class);
                    intent.putExtra("SaleOrCash", "57");
                    intent.putExtra("title", "Cash Withdrawal");
                    startActivity(intent);
                }
            }
        });

        button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (ezeTapDeviceSerialNumber.isEmpty()) {
					/*
					 * Constants .showOneButtonDialog(
					 * EzetapDashBoardActivity.this, "Mini ATM",
					 * "Please contact your distributor to enable this service."
					 * , "OK", 1);
					 */

                    Intent intent = new Intent(EzetapDashBoardActivity.this,
                            EzetapLeadActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(EzetapDashBoardActivity.this,
                            EzeTapTransactionsHistoryActivity.class);
                    startActivity(intent);
                }
            }
        });

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Constants.showNavigationBar(EzetapDashBoardActivity.this);

        super.onResume();
    }

}
