package com.mindsarray.pay1;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.NotificationDataSource;
import com.mindsarray.pay1.databasehandler.Plan;
import com.mindsarray.pay1.databasehandler.PlanDataSource;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DataCardActivity extends Activity {
    // RadioGroup radioRchTypeGroup;
    // RadioButton radioTopUp, radioSTV;
    ImageView mImageOprLogo;
    TextView mTextOperatorName;
    Button mBtnRchNow, btnPlans;

    EditText editMobileNumber, editRechargeAmount;
    Bundle bundle;
    int mOperatorId;
    String circle_code = null;
    String mobileNumber, amount, planJson;

    Context mContext;
    String uuid;
    boolean bNetOrSms;
    private SharedPreferences mSettingsData;// Shared preferences for setting
    // data
    long timestamp;
    //	public static GoogleAnalytics analytics; public static Tracker tracker;
    public static GoogleAnalytics analytics;
    public static Tracker tracker;
    NotificationDataSource notificationDataSource;
    PlanDataSource planDataSource;
    ProgressBar progressBar1;
    String special = "0";
    private static final String TAG = "Data card";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        // WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.datacard_activity);
        findViewById();
//		tracker = Constants.setAnalyticsTracker(this, tracker);
//		//easyTracker.set(Fields.SCREEN_NAME, TAG);
        //analytics = GoogleAnalytics.getInstance(this);
        tracker = Constants.setAnalyticsTracker(this, tracker);
        try {
            TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            uuid = tManager.getDeviceId();
            if (uuid == null) {
                uuid = Secure.getString(
                        DataCardActivity.this.getContentResolver(),
                        Secure.ANDROID_ID);
            }
            if (Utility.getUUID(DataCardActivity.this,
                    Constants.SHAREDPREFERENCE_UUID) == null)
                Utility.setUUID(DataCardActivity.this,
                        Constants.SHAREDPREFERENCE_UUID, uuid);
        } catch (Exception exception) {
        }

        notificationDataSource = new NotificationDataSource(
                DataCardActivity.this);
        planDataSource = new PlanDataSource(DataCardActivity.this);

        btnPlans.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(DataCardActivity.this,
                        PlanFragmentActivity.class);
                if (editMobileNumber.getText().toString().trim().length() != 0) {
                    intent.putExtra(Constants.PLAN_JSON, planJson);
                    intent.putExtra("CIRCLE_CODE", circle_code);
                    intent.putExtra("DATA_CARD_PLAN", 0);
                } else {
                    intent.putExtra(Constants.PLAN_JSON, planJson);
                    intent.putExtra("CIRCLE_CODE", "");
                    intent.putExtra("DATA_CARD_PLAN", 0);
                }
                startActivityForResult(intent, Constants.PLAN_REQUEST);
                // Constants.overridePendingTransitionActivity(
                // RechargeActivity.this, 4);
            }
        });
        mContext = this;
        mSettingsData = getSharedPreferences(SettingsSmsActivity.SETTINGS_NAME,
                0);
        bNetOrSms = mSettingsData.getBoolean(SettingsSmsActivity.KEY_NETWORK,
                true);
        bundle = getIntent().getExtras();
        mOperatorId = bundle.getInt(Constants.OPERATOR_ID);
        planJson = bundle.getString(Constants.PLAN_JSON);
        if (mOperatorId == Constants.OPERATOR_AIRCEL
                || mOperatorId == Constants.OPERATOR_AIRTEL
                || mOperatorId == Constants.OPERATOR_IDEA
                || mOperatorId == Constants.OPERATOR_MTS
                || mOperatorId == Constants.OPERATOR_RELIANCE_CDMA
                || mOperatorId == Constants.OPERATOR_RELIANCE_GSM
                || mOperatorId == Constants.OPERATOR_VODAFONE
                || mOperatorId == Constants.OPERATOR_TATA_INDICOM) {
            special = "0";
        } else {
            special = "1";
        }

        editMobileNumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
                if (editMobileNumber.length() == 10) {
                    editRechargeAmount.requestFocus();
                }
                if (editMobileNumber.length() == 4) {
                    new GetPlansCircletask().execute();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        editRechargeAmount.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
//				textView_Details.setVisibility(View.GONE);
                if (editMobileNumber.length() == 10
                        && editRechargeAmount.getText().toString().trim()
                        .length() != 0) {
                    progressBar1.setVisibility(View.GONE);
                    getPlan(String.valueOf(mOperatorId),
                            circle_code,
                            Integer.parseInt(editRechargeAmount.getText()
                                    .toString().trim()));
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        mImageOprLogo.setImageResource(getIntent().getIntExtra(
                Constants.OPERATOR_LOGO, 0));
        mTextOperatorName.setText(bundle.getString(Constants.OPERATOR_NAME));

        mBtnRchNow.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                bNetOrSms = mSettingsData.getBoolean(
                        SettingsSmsActivity.KEY_NETWORK, true);

                timestamp = System.currentTimeMillis();
                mobileNumber = editMobileNumber.getText().toString();
                amount = editRechargeAmount.getText().toString();
                if (bNetOrSms) {

                    if (Utility.getLoginFlag(DataCardActivity.this,
                            Constants.SHAREDPREFERENCE_IS_LOGIN)) {
                        if (editMobileNumber.getText().toString().length() != 10) {
                            // Constants.showCustomToast(DataCardActivity.this,
                            // "Please enter a valid mobile number");
                        } else {
                            if (editRechargeAmount.getText().toString()
                                    .length() == 0) {
                                // Constants.showCustomToast(
                                // DataCardActivity.this,
                                // "Please enter a valid Amount");
                            } else {
                                // int selectedId = radioRchTypeGroup
                                // .getCheckedRadioButtonId();
                                // radioSTV = (RadioButton)
                                // findViewById(selectedId);

                                new AlertDialog.Builder(DataCardActivity.this)
                                        .setTitle("Recharge")
                                        .setMessage(
                                                "Operator: "
                                                        + mTextOperatorName
                                                        .getText()
                                                        + "\nMobile Number: "
                                                        + mobileNumber
                                                        + "\nAmount: "
                                                        + amount
                                                        + "\nAre you sure you want to Recharge?")
                                        .setPositiveButton(
                                                "Yes",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(
                                                            DialogInterface dialog,
                                                            int which) {
                                                        dialog.dismiss();
                                                        new RechargeTask()
                                                                .execute();

                                                    }
                                                })
                                        .setNegativeButton(
                                                "No",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(
                                                            DialogInterface dialog,
                                                            int which) {
                                                        dialog.dismiss();
                                                    }
                                                }).show();

                            }
                        }

                    } else {
                        Intent intent = new Intent(DataCardActivity.this,
                                LoginActivity.class);
                        startActivity(intent);
                    }
                } else {

                    if (mobileNumber.length() != 10) {
                        // Constants.showCustomToast(DataCardActivity.this,
                        // "Please enter a valid mobile number");
                    } else {
                        if (amount.length() == 0) {
                            // Constants.showCustomToast(DataCardActivity.this,
                            // "Please enter a valid Amount");
                        } else {
                            // int selectedId = radioRchTypeGroup
                            // .getCheckedRadioButtonId();
                            // radioSTV = (RadioButton)
                            // findViewById(selectedId);
                            new AlertDialog.Builder(DataCardActivity.this)
                                    .setTitle("Recharge (SMS)")
                                    .setMessage(
                                            "Operator: "
                                                    + mTextOperatorName
                                                    .getText()
                                                    + "\nMobile Number: "
                                                    + mobileNumber
                                                    + "\nAmount: "
                                                    + amount
                                                    + "\nAre you sure you want to Recharge?")
                                    .setPositiveButton(
                                            "Yes",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(
                                                        DialogInterface dialog,
                                                        int which) {
                                                    dialog.dismiss();
                                                    Constants
                                                            .sendSMSMessage(
                                                                    mContext,
                                                                    Constants.RECHARGE_MOBILE,
                                                                    mOperatorId,
                                                                    mobileNumber,
                                                                    mobileNumber,
                                                                    amount,
                                                                    special,
                                                                    bundle.getString(Constants.OPERATOR_NAME));
                                                    // finish();
                                                }
                                            })
                                    .setNegativeButton(
                                            "No",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(
                                                        DialogInterface dialog,
                                                        int which) {
                                                    dialog.dismiss();
                                                }
                                            }).show();

                        }

                    }

                }

            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
//		//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
        //tracker.setScreenName(TAG);
        Constants.trackOnStart(tracker, TAG);
        //.build());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.PLAN_REQUEST) {
                editRechargeAmount
                        .setText(data.getExtras().getString("result"));
            }
        }
    }

    private void findViewById() {
        // radioRchTypeGroup = (RadioGroup)
        // findViewById(R.id.radioRechargeType);
        // radioSTV = (RadioButton) findViewById(R.id.radioSTV);
        // radioTopUp = (RadioButton) findViewById(R.id.radioTopup);
        mImageOprLogo = (ImageView) findViewById(R.id.operator_logo);
        mTextOperatorName = (TextView) findViewById(R.id.operator_name);
        mBtnRchNow = (Button) findViewById(R.id.mBtnRchNow);
        editMobileNumber = (EditText) findViewById(R.id.editMobileNumber);
        editRechargeAmount = (EditText) findViewById(R.id.editRechargeAmount);
//		textView_Details = (TextView) findViewById(R.id.textView_Details);
        btnPlans = (Button) findViewById(R.id.btnPlans);
        progressBar1 = (ProgressBar) findViewById(R.id.progressBar1);

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Constants.showNavigationBar(DataCardActivity.this);
        super.onResume();
    }

    public class RechargeTask extends AsyncTask<String, String, String>
            implements OnDismissListener {
        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method", "mobRecharge"));
            listValuePair.add(new BasicNameValuePair("mobileNumber",
                    mobileNumber));
            listValuePair.add(new BasicNameValuePair("operator", mOperatorId
                    + ""));
            listValuePair.add(new BasicNameValuePair("subId", mobileNumber));
            listValuePair.add(new BasicNameValuePair("amount", amount));
            listValuePair.add(new BasicNameValuePair("type", "flexi"));
            listValuePair.add(new BasicNameValuePair("circle", ""));
            listValuePair.add(new BasicNameValuePair("special", special));
            // listValuePair.add(new BasicNameValuePair("uuid", uuid));
            listValuePair.add(new BasicNameValuePair("timestamp", String
                    .valueOf(timestamp)));
            listValuePair.add(new BasicNameValuePair("profile_id", Utility
                    .getProfileID(DataCardActivity.this,
                            Constants.SHAREDPREFERENCE_PROFILE_ID)));
            listValuePair.add(new BasicNameValuePair("hash_code", Constants
                    .encryptPassword(Utility.getUUID(DataCardActivity.this,
                            Constants.SHAREDPREFERENCE_UUID)
                            + mobileNumber
                            + amount + timestamp)));
            listValuePair.add(new BasicNameValuePair("device_type", "android"));
            // String
            // url="http://panel.activestores.in/apis/receiveWeb/mindsarray/mindsarray/json?method=mobRecharge&Mobile=9819032643&operator=4&subId=9819032643&amount=10&type=flexi&circle=&special=0&timestamp=1389870880878&hash_code=92da34555032fda9ff31d7fbdfe2c6482bc094ca&device_type=android";

            String response = RequestClass.getInstance().readPay1Request(
                    DataCardActivity.this, Constants.API_URL, listValuePair);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");

                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    String rechargeString = Constants.getRechargeString(
                            mContext, Constants.RECHARGE_MOBILE, mOperatorId,
                            mobileNumber, mobileNumber, amount, special);
                    try {
                        notificationDataSource.open();
                        notificationDataSource.createNotification(
                                rechargeString, 0,
                                "" + System.currentTimeMillis());
                    } catch (SQLException exception) {
                        // TODO: handle exception
                        // // Log.e("Er ", exception.getMessage());
                    } catch (Exception e) {
                        // TODO: handle exception
                        // // Log.e("Er ", e.getMessage());
                    } finally {
                        notificationDataSource.close();
                    }

                    // ContentValues contentValues = new ContentValues();
                    // contentValues.put("notification_msg", rechargeString);
                    // contentValues.put("timestamp ", "12345678902");
                    // contentValues.put("netOrSms ", 0);
                    // controller.insertNotifications(contentValues);
                    if (status.equalsIgnoreCase("success")) {
                        // Constants.showCustomToast(DataCardActivity.this,
                        // "Recharge request sent successfully.");
                        Constants.showOneButtonSuccessDialog(
                                DataCardActivity.this,
                                "Recharge request sent successfully.\nTransaction id : "
                                        + jsonObject.getString("description"),
                                Constants.RECHARGE_SETTINGS);
                        String balance = jsonObject.getString("balance");
                        Utility.setBalance(DataCardActivity.this,
                                Constants.SHAREDPREFERENCE_BALANCE, balance);

                        // finish();
                    } else {
                        Constants.showOneButtonFailureDialog(
                                DataCardActivity.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(DataCardActivity.this,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(DataCardActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(DataCardActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(DataCardActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            RechargeTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            RechargeTask.this.cancel(true);
            dialog.cancel();
        }

    }

    public class GetPlansCircletask extends AsyncTask<String, String, String> {
        ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            // String response = RequestClass.getInstance().readPay1Request(
            // DataCardActivity.this,
            // Constants.URL + "apis/getMobileDetails/"
            // + editMobileNumber.getText().toString().trim());

            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method",
                    "getMobileDetails"));
            listValuePair.add(new BasicNameValuePair("mobile", editMobileNumber
                    .getText().toString().trim()));

            String response = RequestClass.getInstance().readPay1Request(
                    DataCardActivity.this, Constants.API_URL, listValuePair);
            String replaced = response.replace("(", "").replace(")", "")
                    .replace(";", "");

            return replaced;

        }

        @Override
        protected void onPostExecute(String result) {
            // if (this.dialog.isShowing()) {
            // this.dialog.dismiss();
            // }
            super.onPostExecute(result);
            try {
                JSONArray array = new JSONArray(result);
                JSONObject jsonObject = array.getJSONObject(0);
                String status = jsonObject.getString("status");
                if (status.equalsIgnoreCase("SUCCESS")) {

                    JSONObject jsonObjectdetails = jsonObject
                            .getJSONObject("details");
                    circle_code = jsonObjectdetails.getString("area")
                            .toString().trim();
                }
            } catch (JSONException e) {
                // e.printStackTrace();
                // Constants.showCustomToast(DataCardActivity.this, result);
            } catch (Exception e) {
                // TODO: handle exception
                // Constants.showCustomToast(DataCardActivity.this, result);
            }
        }

        @Override
        protected void onPreExecute() {
            // dialog = new ProgressDialog(PlansActivity.this);
            // this.dialog.setMessage("Please wait.....");
            // this.dialog.show();
            super.onPreExecute();

        }

    }

    protected void getPlan(String operator, String circle, int amount) {

        try {
            planDataSource.open();

            List<Plan> plans = planDataSource.getPlanDetails(operator, circle,
                    amount, 1);
            if (plans.size() == 0
                    && planDataSource.getAllPlan(operator, circle).size() == 0) {
                new GetPlanstask().execute();
            } else {
                String LastUpdateTime = plans.get(0).getPlanUptateTime();
                int days = (int) ((System.currentTimeMillis() - Long
                        .parseLong(LastUpdateTime)) / (1000 * 60 * 60 * 24));
                if (days >= 2) {
                    planDataSource.deletePlan();
                    new GetPlanstask().execute();
                } else {
//					textView_Details.setText(plans.get(0).getPlanDescription());
//					textView_Details.setVisibility(View.VISIBLE);
                }
            }

        } catch (SQLException exception) {

            // TODO: handle exception
            // // Log.e("Er ", exception.getMessage());
        } catch (Exception e) {
            // TODO: handle exception
            // // Log.e("Er ", e.getMessage());
        } finally {
            planDataSource.close();
            // progressBar1.setVisibility(View.GONE);
        }

    }

    public class GetPlanstask extends AsyncTask<String, String, String> {
        // ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair
                    .add(new BasicNameValuePair("method", "getPlanDetails"));
            listValuePair.add(new BasicNameValuePair("operator", getIntent()
                    .getExtras().getString(Constants.PLAN_JSON)));
            listValuePair.add(new BasicNameValuePair("circle", circle_code));

            String response = RequestClass.getInstance().readPay1Request(
                    DataCardActivity.this, Constants.API_URL, listValuePair);
            String replaced = response.replace("(", "").replace(")", "")
                    .replace(";", "");
            savePlansTodataBase(replaced);

            return replaced;

        }

        @Override
        protected void onPostExecute(String result) {
            // if (this.dialog.isShowing()) {
            // this.dialog.dismiss();
            // }
            progressBar1.setVisibility(View.GONE);
            super.onPostExecute(result);
            try {
                if (result != null && !result.startsWith("null")
                        && !result.startsWith("Error"))
                    getPlan(String.valueOf(mOperatorId),
                            circle_code,
                            Integer.parseInt(editRechargeAmount.getText()
                                    .toString().trim()));
            } catch (Exception e) {
                // TODO: handle exception
                // Constants.showCustomToast(DataCardActivity.this, result);
            }
        }

        @Override
        protected void onPreExecute() {
            // dialog = new ProgressDialog(RechargeActivity.this);
            // this.dialog.setMessage("Please wait.....");
            // this.dialog.show();
            progressBar1.setVisibility(View.GONE);
            super.onPreExecute();

        }

    }

    private void savePlansTodataBase(String planJson) {
        try {
            planDataSource.open();
            // ContentValues contentValues = new ContentValues();
            JSONArray array = new JSONArray(planJson);
            JSONObject planJsonObject = array.getJSONObject(0);

            Iterator<String> operatorKey = planJsonObject.keys();

            if (operatorKey.hasNext()) {

                long time = System.currentTimeMillis();

                JSONObject jsonObject2 = planJsonObject
                        .getJSONObject(operatorKey.next());

                String opr_name = jsonObject2.getString("opr_name");
                String prod_code_pay1 = jsonObject2.getString("prod_code_pay1");
                // contentValues.put("operator_id", prod_code_pay1);
                // contentValues.put("operator_name", opr_name);
                // contentValues.put("prod_code_pay1", prod_code_pay1);
                JSONObject circleJsonObject = jsonObject2
                        .getJSONObject("circles");
                Iterator<String> circleKey = circleJsonObject.keys();
                if (circleKey.hasNext()) {
                    JSONObject circleJsonObject2 = circleJsonObject
                            .getJSONObject(circleKey.next().toString());
                    String circleName = circleJsonObject2
                            .getString("circle_name");

                    // contentValues.put("circle_name", circleName);

                    JSONObject plansJsonObject = circleJsonObject2
                            .getJSONObject("plans");
                    String circle_id = circleJsonObject2.getString("circle_id");
                    Iterator<String> planKey = plansJsonObject.keys();
                    Iterator<String> planKeyDB = plansJsonObject.keys();
                    // contentValues.put("circle_id", circle_id);

                    while (planKey.hasNext()) {
                        String planType = planKey.next().toString();
                        JSONArray planArray = plansJsonObject
                                .getJSONArray(planType);
                        // contentValues.put("planType", planType);

                        for (int i = 0; i < planArray.length(); i++) {
                            JSONObject jsonObject = planArray.getJSONObject(i);
                            String plan_desc = jsonObject
                                    .getString("plan_desc");
                            // contentValues.put("plan_desc", plan_desc);
                            String plan_amt = jsonObject.getString("plan_amt");
                            // contentValues.put("plan_amt", plan_amt);
                            String plan_validity = jsonObject
                                    .getString("plan_validity");
                            String show_flag = jsonObject
                                    .getString("show_flag");
                            // contentValues.put("plan_validity",
                            // plan_validity);
                            // // Log.e("cORRECT", "cORRECT   " + i + "  "
                            // + jsonObject.toString());
                            // controller.insertPlans(contentValues);
                            planDataSource.createPlan(prod_code_pay1, opr_name,
                                    circle_id, circleName, planType, plan_amt,
                                    plan_validity, plan_desc, "" + time, show_flag);
                        }

                    }

                }

            }
        } catch (JSONException exception) {

        } catch (SQLException exception) {
            // TODO: handle exception
            // // Log.e("Er ", exception.getMessage());
        } catch (Exception e) {
            // TODO: handle exception
            // // Log.e("Er ", e.getMessage());
        } finally {
            planDataSource.close();
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (Constants.isExpanded) {
            Constants.collapsView();
        } else {
            super.onBackPressed();
        }
    }
}
