package com.mindsarray.pay1.adapterhandler;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mindsarray.pay1.R;

import java.util.List;

public class ReportMainAdapter extends ArrayAdapter<String> {
    Context mContext;
    List<String> mArrayList;
    LayoutInflater inflater;

    public ReportMainAdapter(Context context, int textViewResourceId,
                             List<String> objects) {
        super(context, textViewResourceId, objects);
        // TODO Auto-generated constructor stub
        this.mContext = context;
        this.mArrayList = objects;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View v = convertView;

        if (convertView == null) {
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.reportmain_adapter, null);
        }

        TextView textView = (TextView) v.findViewById(R.id.simpleTextView);

        textView.setText(mArrayList.get(position));
        return v;
    }


}
