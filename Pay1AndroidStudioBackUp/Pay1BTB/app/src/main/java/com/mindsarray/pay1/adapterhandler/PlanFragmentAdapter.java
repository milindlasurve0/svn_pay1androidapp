package com.mindsarray.pay1.adapterhandler;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.mindsarray.pay1.PlanListFragment;

import java.util.ArrayList;

public class PlanFragmentAdapter extends FragmentStatePagerAdapter {

    ArrayList<String> planCategoryList = new ArrayList<String>();
    int mCount = 0;
    String operator = "";
    String circle = "";

    public PlanFragmentAdapter(FragmentManager fm,
                               ArrayList<String> planCategoryList, String operator, String circle) {
        super(fm);
        this.planCategoryList = planCategoryList;
        this.mCount = planCategoryList.size();
        this.operator = operator;
        this.circle = circle;
    }

    @Override
    public Fragment getItem(int position) {
        return PlanListFragment.newInstance(operator, circle,
                planCategoryList.get(position));
        // switch (position) {
        //
        // case 0:
        // return MobileBillMainFragment
        // .newInstance("FirstFragment, Instance 1");
        // case 1:
        // return TestFragment.newInstance("SecondFragment, Instance 1");
        // case 2:
        // return TestFragment.newInstance("ThirdFragment, Default");
        // default:
        // return null;
        // }
    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return planCategoryList.get(position);
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }
}