package com.mindsarray.pay1.utilities;

import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout.LayoutParams;
import android.widget.RelativeLayout;

public class CollapseAnimation extends TranslateAnimation implements
        TranslateAnimation.AnimationListener {

    private RelativeLayout slidingLayout;
    int panelWidth;

    public CollapseAnimation(RelativeLayout layout, int width, int fromXType,
                             float fromXValue, int toXType, float toXValue, int fromYType,
                             float fromYValue, int toYType, float toYValue) {

        super(fromXType, fromXValue, toXType, toXValue, fromYType, fromYValue,
                toYType, toYValue);

        // Initialize
        slidingLayout = layout;
        panelWidth = width;
        //setDuration(400);
        setFillAfter(false);
        setInterpolator(new AccelerateDecelerateInterpolator());
        setAnimationListener(this);

        // Clear left and right margins
        LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT,
                LayoutParams.FILL_PARENT);//(LayoutParams) slidingLayout.getLayoutParams();
        params.rightMargin = 0;
        params.leftMargin = 0;
        //params.width = panelWidth;
        slidingLayout.setLayoutParams(params);
        slidingLayout.requestLayout();
        slidingLayout.startAnimation(this);

    }

    public void onAnimationEnd(Animation animation) {

    }

    public void onAnimationRepeat(Animation animation) {

    }

    public void onAnimationStart(Animation animation) {

    }

}
