package com.mindsarray.pay1;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.NewMainFragmentAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.utilities.Utility;

public class NewMainActivity extends FragmentActivity {

    NewMainFragmentAdapter mAdapter;
    ViewPager mPager;
    PageIndicator mIndicator;
    private static final String TAG = "Transaction Tab Fragment Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_main_activity);
        tracker = Constants.setAnalyticsTracker(this, tracker);

    }

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (Constants.isExpanded) {
            Constants.collapsView();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Constants.showNavigationBar(NewMainActivity.this);
        if (Utility.getLoginFlag(NewMainActivity.this,
                Constants.SHAREDPREFERENCE_IS_LOGIN)) {
            mAdapter = new NewMainFragmentAdapter(
                    getSupportFragmentManager());

            mPager = (ViewPager) findViewById(R.id.pager);
            mPager.setAdapter(mAdapter);

            TabPageIndicator indicator = (TabPageIndicator) findViewById(R.id.indicator);
            indicator.setViewPager(mPager);
            //indicator.setFooterIndicatorStyle(IndicatorStyle.Underline);
            mIndicator = indicator;

        } else {
            Intent intent = new Intent(NewMainActivity.this,
                    LoginActivity.class);
            startActivityForResult(intent, Constants.LOGIN);
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

}
