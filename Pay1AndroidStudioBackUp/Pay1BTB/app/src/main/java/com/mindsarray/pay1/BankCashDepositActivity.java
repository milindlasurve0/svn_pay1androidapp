package com.mindsarray.pay1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;

import com.mindsarray.pay1.adapterhandler.BankDetailsAdapter;
import com.mindsarray.pay1.adapterhandler.BankDetailsAdapter.BankDetailsPair;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class BankCashDepositActivity extends Activity {

    private String TAG = "Bank/Cash Deposit";
    private Context mContext = BankCashDepositActivity.this;
    private ExpandableListView mExpandableListView;
    private BankDetailsAdapter mBankDetailsAdapter;
    private ArrayList<BankDetailsPair> listDataHeader;
    private ArrayList<ArrayList<BankDetailsPair>> listChildData;
    private String retailer_pin;
    private String retailer_mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.bankcashdeposit_activity);

        retailer_pin = Utility.getSharedPreferences(mContext, "Retailer_Pin");
        retailer_mobile = Utility.getSharedPreferences(mContext,
                "Retailer_Mobile");
        listDataHeader = new ArrayList<BankDetailsPair>();
        listChildData = new ArrayList<ArrayList<BankDetailsPair>>();
        if (Utility.getLoginFlag(mContext, Constants.SHAREDPREFERENCE_IS_LOGIN)) {
            Button submitButton = (Button) findViewById(R.id.bankcashdeposit_submit);
            submitButton.setVisibility(View.GONE);
        }
        Button bankCashDepositButton = (Button) findViewById(R.id.bankcashdeposit_submit);
        bankCashDepositButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                setLoginForBankCashDeposit();
            }
        });

        mExpandableListView = (ExpandableListView) findViewById(R.id.bankDetailsList);
        // prepareListData();
        mBankDetailsAdapter = new BankDetailsAdapter(this, listDataHeader,
                listChildData);
        mExpandableListView.setAdapter(mBankDetailsAdapter);

        mExpandableListView
                .setOnGroupExpandListener(new OnGroupExpandListener() {

                    @Override
                    public void onGroupExpand(int groupPosition) {
                        adjustGroupSizes();
                    }

                });

        mExpandableListView
                .setOnGroupCollapseListener(new OnGroupCollapseListener() {

                    @Override
                    public void onGroupCollapse(int groupPosition) {
                        adjustGroupSizes();
                    }

                });

        new GetBankDetailsTask().execute();

    }

    public class GetBankDetailsTask extends AsyncTask<String, String, String> {
        ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {

            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method", "bankAccounts"));

            String response = RequestClass.getInstance().readPay1Request(
                    BankCashDepositActivity.this, Constants.API_URL,
                    listValuePair);
            String replaced = response.replace("(", "").replace(")", "")
                    .replace(";", "");

            return replaced;

        }

        @Override
        protected void onPostExecute(String result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            super.onPostExecute(result);
            try {

                if (!result.startsWith("Error")) {
                    JSONArray array = new JSONArray(result);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        JSONArray description = jsonObject
                                .getJSONArray("description");
                        for (int i = 0; i < description.length(); i++) {
                            JSONObject jObj = description.getJSONObject(i);
                            String bankName = jObj.getString("bank");
                            String Account_No = jObj.getString("account_no");
                            String AccountName = jObj.getString("account_name");
                            String BranchName = jObj.getString("branch");
                            String IFSCCode = jObj.getString("ifsc");
                            String ACType = jObj.getString("account_type");

                            listDataHeader.add(new BankDetailsPair(bankName,
                                    "ACCOUNT NO.: " + Account_No));

                            String detailsKey = "BANK DETAILS: ";
                            String detailsValue = "Cash Deposit, NEFT & IMPS";
                            String accountName = "ACCOUNT NAME: ";
                            String accountNo = "ACCOUNT No.: ";
                            String accountType = "A/C TYPE: ";
                            String accountTypeValue = "Current";
                            String ifscCode = "IFSC Code: ";
                            String branchName = "Branch Name: ";

                            ArrayList<BankDetailsPair> listChildDataArray = new ArrayList<BankDetailsPair>();

                            listChildDataArray.add(new BankDetailsPair(
                                    detailsKey, detailsValue));
                            listChildDataArray.add(new BankDetailsPair(
                                    accountName, AccountName));
                            listChildDataArray.add(new BankDetailsPair(
                                    accountNo, Account_No));
                            listChildDataArray.add(new BankDetailsPair(
                                    accountType, ACType));
                            listChildDataArray.add(new BankDetailsPair(
                                    ifscCode, IFSCCode));
                            listChildDataArray.add(new BankDetailsPair(
                                    branchName, BranchName));

                            listChildData.add(listChildDataArray);
                        }

                        mBankDetailsAdapter.notifyDataSetChanged();
                    } else {
                        Constants.showOneButtonFailureDialog(mContext,
                                Constants.checkCode(result), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(
                            mContext,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);
                }

            } catch (Exception e) {
                Constants.showOneButtonFailureDialog(mContext,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(BankCashDepositActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.show();
            super.onPreExecute();

        }

    }

    private void adjustGroupSizes() {
        int GEI1 = 0;
        int GEI2 = 0;
        int GEI3 = 0;
        int GEI4 = 0;
        float scale = mContext.getResources().getDisplayMetrics().density;
        if (mExpandableListView.isGroupExpanded(0))
            GEI1 = 1;
        if (mExpandableListView.isGroupExpanded(1))
            GEI2 = 1;
        if (mExpandableListView.isGroupExpanded(2))
            GEI3 = 1;
        if (mExpandableListView.isGroupExpanded(3))
            GEI4 = 1;
        int TGEI = GEI1 + GEI2 + GEI3 + GEI4;
        int pixels = (int) (200 * scale + 0.5f);
        switch (TGEI) {
            case 0:
                pixels = (int) (200 * scale + 0.5f);
                break;
            case 1:
                pixels = (int) (450 * scale + 0.5f);
                break;
            case 2:
                pixels = (int) (700 * scale + 0.5f);
                break;
            case 3:
                pixels = (int) (900 * scale + 0.5f);
                break;
            case 4:
                pixels = (int) (1250 * scale + 0.5f);
                break;
        }
        mExpandableListView.getLayoutParams().height = pixels + 100;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        mExpandableListView.setIndicatorBounds(mExpandableListView.getRight(),
                mExpandableListView.getWidth());
    }

	/*
     * private void prepareListData() { listDataHeader = new
	 * ArrayList<BankDetailsPair>(); listChildData = new
	 * ArrayList<ArrayList<BankDetailsPair>>();
	 * 
	 * String iciciBankAccountNo = "054405006714"; String bomBankAccountNo =
	 * "60117424079"; String sbiBankAccountNo = "33619466476"; String
	 * ubiBankAccountNo = "582401010050258";
	 * 
	 * String detailsKey = "BANK DETAILS: "; String detailsValue =
	 * "Cash Deposit, NEFT & IMPS"; String accountName = "ACCOUNT NAME: ";
	 * String accountNo = "ACCOUNT No.: "; String accountType = "A/C TYPE: ";
	 * String accountTypeValue = "Current"; String ifscCode = "IFSC Code: ";
	 * String branchName = "Branch Name: ";
	 * 
	 * listDataHeader.add(new BankDetailsPair("ICICI BANK", "ACCOUNT NO.: " +
	 * iciciBankAccountNo)); listDataHeader.add(new
	 * BankDetailsPair("BANK OF MAHARASHTRA", "ACCOUNT NO.: " +
	 * bomBankAccountNo)); listDataHeader.add(new
	 * BankDetailsPair("STATE BANK OF INDIA", "ACCOUNT NO.: " +
	 * sbiBankAccountNo)); listDataHeader.add(new
	 * BankDetailsPair("UNION BANK OF INDIA", "ACCOUNT NO.: " +
	 * ubiBankAccountNo));
	 * 
	 * ArrayList<BankDetailsPair> iciciListChildDataArray = new
	 * ArrayList<BankDetailsPair>(); ArrayList<BankDetailsPair>
	 * bomListChildDataArray = new ArrayList<BankDetailsPair>();
	 * ArrayList<BankDetailsPair> sbiListChildDataArray = new
	 * ArrayList<BankDetailsPair>();
	 * 
	 * ArrayList<BankDetailsPair> ubiListChildDataArray = new
	 * ArrayList<BankDetailsPair>();
	 * 
	 * iciciListChildDataArray.add(new BankDetailsPair(detailsKey,
	 * detailsValue)); iciciListChildDataArray.add(new
	 * BankDetailsPair(accountName, "Mindsarray Technologies Pvt. Ltd."));
	 * iciciListChildDataArray.add(new BankDetailsPair(accountNo,
	 * iciciBankAccountNo)); iciciListChildDataArray.add(new
	 * BankDetailsPair(accountType, accountTypeValue)); iciciListChildDataArray
	 * .add(new BankDetailsPair(ifscCode, "ICIC0000544"));
	 * iciciListChildDataArray.add(new BankDetailsPair(branchName, "MIDC"));
	 * 
	 * bomListChildDataArray .add(new BankDetailsPair(detailsKey,
	 * detailsValue)); bomListChildDataArray.add(new
	 * BankDetailsPair(accountName, "Mindsarray Technologies Pvt. Ltd."));
	 * bomListChildDataArray.add(new BankDetailsPair(accountNo,
	 * bomBankAccountNo)); bomListChildDataArray.add(new
	 * BankDetailsPair(accountType, accountTypeValue));
	 * bomListChildDataArray.add(new BankDetailsPair(ifscCode, "MAHB0000117"));
	 * bomListChildDataArray.add(new BankDetailsPair(branchName, "Malad (W)"));
	 * 
	 * sbiListChildDataArray .add(new BankDetailsPair(detailsKey,
	 * detailsValue)); sbiListChildDataArray.add(new
	 * BankDetailsPair(accountName, "Pay1")); sbiListChildDataArray.add(new
	 * BankDetailsPair(accountNo, sbiBankAccountNo));
	 * sbiListChildDataArray.add(new BankDetailsPair(accountType,
	 * accountTypeValue)); sbiListChildDataArray.add(new
	 * BankDetailsPair(ifscCode, "SBIN0006055")); sbiListChildDataArray.add(new
	 * BankDetailsPair(branchName, "Gokuldham (Goregaon - E)"));
	 * 
	 * ubiListChildDataArray .add(new BankDetailsPair(detailsKey,
	 * detailsValue)); ubiListChildDataArray.add(new
	 * BankDetailsPair(accountName, "Mindsarray Technologies PVT LTD."));
	 * ubiListChildDataArray.add(new BankDetailsPair(accountNo,
	 * ubiBankAccountNo)); ubiListChildDataArray.add(new
	 * BankDetailsPair(accountType, accountTypeValue));
	 * ubiListChildDataArray.add(new BankDetailsPair(ifscCode, "UBIN0558249"));
	 * ubiListChildDataArray.add(new BankDetailsPair(branchName,
	 * " Link Road Malad West"));
	 * 
	 * listChildData.add(iciciListChildDataArray);
	 * listChildData.add(bomListChildDataArray);
	 * listChildData.add(sbiListChildDataArray);
	 * listChildData.add(ubiListChildDataArray); }
	 */

    void setLoginForBankCashDeposit() {
        Intent intent = new Intent(mContext, LoginActivity.class);
        intent.putExtra("class", "AutoLogin");
        if (!retailer_mobile.equals("") && !retailer_pin.equals("")) {
            intent.putExtra("retailer_mobile", retailer_mobile);
            intent.putExtra("retailer_pin", retailer_pin);
            intent.putExtra("is_login", "login");
        }
        startActivity(intent);
    }
}