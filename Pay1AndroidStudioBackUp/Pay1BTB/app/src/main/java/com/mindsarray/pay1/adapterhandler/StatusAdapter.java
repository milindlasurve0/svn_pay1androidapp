package com.mindsarray.pay1.adapterhandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mindsarray.pay1.R;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class StatusAdapter extends BaseAdapter {
    public ArrayList<String> list;
    Activity activity;
    int requestFor;
    String mComplaintId;
    private static final String TAG = "Status";
    ViewHolder holder;
    int pos = 0;
    String complaintTag = null;
    // private ImageLoader imageLoader;

    public StatusAdapter(Activity activity, ArrayList<String> statusList,
                         int requestFor) {
        super();
        this.activity = activity;
        this.list = statusList;
        this.requestFor = requestFor;
        // imageLoader = new ImageLoader(activity);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    private class ViewHolder {
        TextView MobileText;
        TextView AmountText;
        TextView TimeText;
        TextView TransIdText;
        ImageView imageOperator, imageStatus, imageComplaint;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.status_adapter_new, null);
            holder = new ViewHolder();
            holder.imageComplaint = (ImageView) convertView
                    .findViewById(R.id.imageComplaint);
            holder.MobileText = (TextView) convertView
                    .findViewById(R.id.MobileText);
            holder.AmountText = (TextView) convertView
                    .findViewById(R.id.AmountText);
            holder.TimeText = (TextView) convertView
                    .findViewById(R.id.TimeText);
            holder.imageOperator = (ImageView) convertView
                    .findViewById(R.id.imageOperator);
            holder.imageStatus = (ImageView) convertView
                    .findViewById(R.id.imageStatus);
            holder.TransIdText = (TextView) convertView
                    .findViewById(R.id.TransIdText);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//		if (position % 2 == 1) {
//			convertView.setBackgroundColor(Color.WHITE);
//		} else {
//			convertView.setBackgroundColor(activity.getResources().getColor(
//					R.color.row_light_green));
//		}

        try {
            JSONObject jsonObject = new JSONObject(list.get(position));
            JSONObject timeObject = jsonObject.getJSONObject("0");
            String time = timeObject.getString("timestamp");

            final JSONObject descObject = jsonObject
                    .getJSONObject("vendors_activations");
            String complaintResolveString = jsonObject.getJSONObject("0").getString("resolve_flag");
            int complaintResolveFlag = (complaintResolveString.equals("null")) ? -1 : Integer.parseInt(complaintResolveString);
            try {
                String param = descObject.getString("param");
                if (param.contains("*")) {
                    String[] display = param.split("\\*");

                    if (requestFor == 2 || requestFor == 6) {
                        String demo = display[0];
                        for (int i = 0; i < display.length; i++)
                            if (i != 0)
                                demo += " / " + display[i];
                        holder.MobileText.setText(demo);
                    } else {
                        holder.MobileText.setText(descObject
                                .getString("mobile"));
                    }
                } else {
                    if (requestFor == 2 || requestFor == 6) {
                        holder.MobileText.setText(param);
                    } else {
                        holder.MobileText.setText(descObject
                                .getString("mobile"));
                    }
                }
            } catch (Exception e) {
                if (requestFor == 2 || requestFor == 6) {
                    holder.MobileText.setText(descObject.getString("param"));
                } else {
                    holder.MobileText.setText(descObject.getString("mobile"));
                }
            }
            holder.TimeText.setText(time.substring(5, 16));
            if (descObject.has("ref_code")) {
                holder.TransIdText.setText("Txn Id: "
                        + descObject.getString("ref_code"));
            } else {
                holder.TransIdText.setVisibility(View.GONE);
            }
            holder.AmountText.setText("Rs. " + descObject.getString("amount"));
            int id = Integer.parseInt(descObject.getString("product_id"));
            // getDthBitmapById
            // if (requestFor != 3) {
            if (requestFor == 7) {
                try {
                    String url = Utility.getImageSharedPreferences(activity, descObject.getString("product_id"));
                    Picasso.with(activity)
                            .load(url)
                            .placeholder(R.drawable.ic_launcher).fit()
                            .into(holder.imageOperator);

					/*byte[] decodedString = Base64.decode(url, Base64.DEFAULT);
					Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
					holder.imageOperator.setImageBitmap(decodedByte);*/

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                holder.imageOperator.setImageResource(Constants.getBitmapById(id,
                        activity));
            }
            // } else {
            // String url = descObject.getString("image").toString().trim();
            // imageLoader.getEntertainmentImage(url, holder.imageOperator);
            // }

            holder.imageComplaint.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    try {
                        String msg = "";
                        try {
                            String param = descObject.getString("param");
                            if (param.contains("*")) {
                                String[] display = descObject
                                        .getString("param").split("\\*");

                                if (requestFor == 2 || requestFor == 6) {
                                    msg = "Are you sure you want to complaint for "
                                            + display[0] + " ?";
                                } else {
                                    msg = "Are you sure you want to complaint for "
                                            + descObject.getString("mobile")
                                            + " ?";
                                }
                            } else {
                                if (requestFor == 2 || requestFor == 6) {
                                    msg = "Are you sure you want to complaint for "
                                            + param + " ?";
                                } else {
                                    msg = "Are you sure you want to complaint for "
                                            + descObject.getString("mobile")
                                            + " ?";
                                }
                            }
                        } catch (Exception e) {
                            if (requestFor == 2 || requestFor == 6) {
                                msg = "Are you sure you want to complaint for "
                                        + descObject.getString("param") + " ?";
                            } else {
                                msg = "Are you sure you want to complaint for "
                                        + descObject.getString("mobile") + " ?";
                            }
                        }
                        LayoutInflater inflater = (LayoutInflater) activity.getSystemService
                                (Context.LAYOUT_INFLATER_SERVICE);
                        View complaintDialogView = inflater.inflate(R.layout.complaint_confirmation_dialog, null);
                        final AlertDialog complaintDialog = new AlertDialog.Builder(activity)
                                .setView(complaintDialogView)
                                .create();
                        final Spinner complaintSpinner = (Spinner) complaintDialogView.findViewById(R.id.complaint_spinner);
                        ArrayAdapter<CharSequence> complaintSpinnerAdapter = ArrayAdapter.createFromResource(activity,
                                R.array.complaint_tags, android.R.layout.simple_spinner_item);
                        complaintSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        complaintSpinner.setAdapter(complaintSpinnerAdapter);

                        TextView complaintDialogTextView = (TextView) complaintDialogView.findViewById(R.id.textView_Message);
                        complaintDialogTextView.setText(msg);

                        Button okButton = (Button) complaintDialogView.findViewById(R.id.button_Ok);
                        Button cancelButton = (Button) complaintDialogView.findViewById(R.id.button_Cancel);

                        okButton.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                complaintDialog.dismiss();
                                JSONObject jsonObject;
                                try {
                                    jsonObject = new JSONObject(
                                            list.get(position));

                                    JSONObject descObject = jsonObject
                                            .getJSONObject("vendors_activations");
                                    mComplaintId = descObject
                                            .getString("id");
                                    pos = position;
                                    complaintTag = complaintSpinner.getSelectedItem().toString();
                                    new SetComlaintTask()
                                            .execute();
                                } catch (JSONException e) {
                                    // TODO Auto-generated catch
                                    // block
                                    e.printStackTrace();
                                }
                            }
                        });
                        cancelButton.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                complaintDialog.dismiss();
                            }
                        });
                        complaintDialog.show();
//						new AlertDialog.Builder(activity)
//								.setTitle("Reversal complaint")
//								.setMessage(msg)
//								.setPositiveButton("Yes",
//										new DialogInterface.OnClickListener() {
//											public void onClick(
//													DialogInterface dialog,
//													int which) {
//												dialog.dismiss();
//												JSONObject jsonObject;
//												try {
//													jsonObject = new JSONObject(
//															list.get(position));
//
//													JSONObject descObject = jsonObject
//															.getJSONObject("vendors_activations");
//													mComplaintId = descObject
//															.getString("id");
//													pos = position;
//													new SetComlaintTask()
//															.execute();
//												} catch (JSONException e) {
//													// TODO Auto-generated catch
//													// block
//													e.printStackTrace();
//												}
//
//											}
//										})
//								.setNegativeButton("No",
//										new DialogInterface.OnClickListener() {
//											public void onClick(
//													DialogInterface dialog,
//													int which) {
//												dialog.dismiss();
//											}
//										}).show();
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        // e.printStackTrace();
                    }
                }
            });

            int statusId = Integer.parseInt(descObject.getString("status"));
            int complaintStatusResource = Constants
                    .getComplaintStatusBitmap(statusId, complaintResolveFlag);
            int transactionStatusResource = Constants
                    .getTransactionStatusBitmap(statusId);

            holder.imageStatus.setImageResource(transactionStatusResource);

            if (complaintStatusResource != 0) {
                holder.imageComplaint.setImageResource(complaintStatusResource);
                holder.imageComplaint.setVisibility(View.VISIBLE);
            } else {
                holder.imageComplaint.setVisibility(View.INVISIBLE);
            }
        } catch (Exception exception) {
            exception.printStackTrace();

        }

        return convertView;
    }

    public class SetComlaintTask extends AsyncTask<String, String, String> {
        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair.add(new BasicNameValuePair("method", "reversal"));
            listValuePair.add(new BasicNameValuePair("id", mComplaintId));
            listValuePair.add(new BasicNameValuePair("tag", complaintTag));
            String response = RequestClass.getInstance().readPay1Request(
                    activity, Constants.API_URL, listValuePair);

            listValuePair.clear();
            return response;

        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            dialog.dismiss();
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");

                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONObject jObject = new JSONObject(list.get(pos));
                        JSONObject dObject = jObject
                                .getJSONObject("vendors_activations");
                        dObject.put("status", "4");
                        JSONObject cObject = jObject
                                .getJSONObject("0");
                        cObject.put("resolve_flag", "0");
                        list.set(pos, jObject.toString());
                        notifyDataSetChanged();
                        String turnaroundTime = "24";
                        if (jsonObject.has("turnaround_time"))
                            turnaroundTime = jsonObject.getString("turnaround_time");
                        String msg = jsonObject.getString("msg");
						/*Constants
								.showOneButtonSuccessDialog(activity,
										"Complaint Sent. Your complaint should resolve in " + turnaroundTime + " Hrs",
										Constants.COMPLAINT_SETTINGS);*/

                        Constants
                                .showOneButtonSuccessDialog(activity, msg,
                                        Constants.COMPLAINT_SETTINGS);

                    } else {
                        Constants.showOneButtonFailureDialog(activity,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(activity,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(activity,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(activity,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(activity);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            SetComlaintTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

    }

}
