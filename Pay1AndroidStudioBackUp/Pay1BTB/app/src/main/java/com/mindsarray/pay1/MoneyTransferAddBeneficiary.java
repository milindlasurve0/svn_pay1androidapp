package com.mindsarray.pay1;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;

public class MoneyTransferAddBeneficiary extends Activity {
    EditText editBeneficiaryNickName, editBeneficiaryAccountNumber,
            editBeneficiaryIfscCode, editBeneficiaryBankName,
            editBeneficiaryBranchName, editBeneficiaryState,
            editBeneficiaryCity, editBeneficiaryPincode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.money_transfer_add_beneficiary);
        findViewById();
    }

    private void findViewById() {
        // TODO Auto-generated method stub
        editBeneficiaryNickName = (EditText) findViewById(R.id.editBeneficiaryNickName);
        editBeneficiaryAccountNumber = (EditText) findViewById(R.id.editBeneficiaryAccountNumber);
        editBeneficiaryIfscCode = (EditText) findViewById(R.id.editBeneficiaryIfscCode);
        editBeneficiaryBankName = (EditText) findViewById(R.id.editBeneficiaryBankName);
        editBeneficiaryBranchName = (EditText) findViewById(R.id.editBeneficiaryBranchName);
        editBeneficiaryState = (EditText) findViewById(R.id.editBeneficiaryState);
        editBeneficiaryCity = (EditText) findViewById(R.id.editBeneficiaryCity);
        editBeneficiaryPincode = (EditText) findViewById(R.id.editBeneficiaryPincode);
    }

}
