package com.mindsarray.pay1.adapterhandler;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mindsarray.pay1.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class PlansAdapter extends ArrayAdapter<JSONObject> {
    Context mContext;
    List<JSONObject> jsonObject;
    LayoutInflater inflater;
    TextView textPlanAmount, textPlanValidity, textPlanDesc;

    public PlansAdapter(Context context, int textViewResourceId,
                        List<JSONObject> objects) {
        super(context, textViewResourceId, objects);
        // TODO Auto-generated constructor stub
        this.mContext = context;
        this.jsonObject = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View view = convertView;
        if (convertView == null) {
            inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.plans_adapter, null);
        }

        // if (position % 2 == 1) {
        // view.setBackgroundColor(Color.WHITE);
        // } else {
        //
        // view.setBackgroundColor(mContext.getResources().getColor(
        // R.color.row_light_green));
        // }

        textPlanAmount = (TextView) view.findViewById(R.id.textPlanAmount);
        textPlanValidity = (TextView) view.findViewById(R.id.textPlanValidity);
        textPlanDesc = (TextView) view.findViewById(R.id.textPlanDesc);

        try {
            textPlanAmount.setText("Rs. "
                    + jsonObject.get(position).getString("amount"));
            textPlanValidity.setText("validity : "
                    + jsonObject.get(position).getString("validity"));
            textPlanDesc.setText(jsonObject.get(position).getString(
                    "description"));
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            // e.printStackTrace();
        }

        return view;
    }

}
