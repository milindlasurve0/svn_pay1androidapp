package com.mindsarray.pay1;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.PostDBHelper;
import com.mindsarray.pay1.requesthandler.RequestClass;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SendStatsToC2DService extends Service {
    Handler mHandler;
    PostDBHelper dbHelper;
    Runnable mHandlerTask;
    private final static int INTERVAL = 1000 * 60 * 1; // 2 minutes

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Let it continue running until it is stopped.
        // Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();

        Sync sync = new Sync(call, 15 * 60 * 1000);

        return START_NOT_STICKY;
    }

    final private Runnable call = new Runnable() {
        public void run() {
            // This is where my sync code will be, but for testing purposes I
            // only have a Log statement
            // Log.v("test123","test123 this will run every minute");
            doSomething();
            handler.postDelayed(call, 15 * 60 * 1000);
        }
    };
    public final Handler handler = new Handler();

    public class Sync {

        Runnable task;

        public Sync(Runnable task, long time) {
            this.task = task;
            handler.removeCallbacks(task);
            handler.postDelayed(task, time);
        }
    }

    protected void doSomething() {
        // TODO Auto-generated method stub
        // Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
        dbHelper = new PostDBHelper(SendStatsToC2DService.this);
        JSONArray array = dbHelper.getC2DStats();
        if (array != null & array.length() >= 1) {
            new SendStatsToC2DTask().execute(array.toString());
        } else {

        }

    }

    public class SendStatsToC2DTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			/*
             * listValuePair.add(new BasicNameValuePair("retailerID", Utility
			 * .getSharedPreferences(SendStatsToC2DService.this,
			 * Constants.SHAREDPREFERENCE_RETAILER_ID)));
			 */
            // listValuePair.add(new BasicNameValuePair("postId","d"));
            listValuePair.add(new BasicNameValuePair("payload", params[0]));

            String response = RequestClass.getInstance().readPay1Request(
                    SendStatsToC2DService.this,
                    Constants.C2D_URL + "logs/saveTrackingData", listValuePair);
            listValuePair.clear();

            Log.e("C2D Posts:", "" + response);

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    JSONObject responseObject = new JSONObject(replaced);

                    String status = responseObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        if (responseObject.getBoolean("type")) {
                            dbHelper.deleteAllStats();
                        }

                    }

                } else {

                }
            } catch (JSONException e) {

            } catch (Exception e) {

            }

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
    }
}
