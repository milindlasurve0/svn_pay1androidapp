package com.mindsarray.pay1.customviews;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mindsarray.pay1.ImagesSlideActivity;
import com.mindsarray.pay1.R;
import com.mindsarray.pay1.utilities.GoogleAnalyticsHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;

public class GiftImageGalleryLayout extends LinearLayout {
    GoogleAnalyticsHelper mGoogleHelper;
    Context myContext;
    ArrayList<String> data = new ArrayList<String>();
    Typeface Reguler;

    public GiftImageGalleryLayout(Context context) {
        super(context);
        myContext = context;
        Reguler = Typeface.createFromAsset(context.getAssets(),
                "OpenSans-Semibold.ttf");
        InitGoogleAnalytics();
    }

    private void InitGoogleAnalytics() {
        mGoogleHelper = new GoogleAnalyticsHelper();
        mGoogleHelper.init(myContext);
    }

    public GiftImageGalleryLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        myContext = context;
        Reguler = Typeface.createFromAsset(context.getAssets(),
                "OpenSans-Semibold.ttf");
        InitGoogleAnalytics();
    }

    @SuppressLint("NewApi")
    public GiftImageGalleryLayout(Context context, AttributeSet attrs,
                                  int defStyle) {
        super(context, attrs, defStyle);
        myContext = context;
        Reguler = Typeface.createFromAsset(context.getAssets(),
                "OpenSans-Semibold.ttf");
        InitGoogleAnalytics();
    }

    public void add(String item) {
        int newIdx = data.size();
        data.add(item);
        addView(getView(newIdx));
    }

    private View getView(final int position) {
        LayoutInflater inflater = (LayoutInflater) myContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final Context contextThemeWrapper = new ContextThemeWrapper(myContext,
                R.style.StyledIndicators);
        inflater = inflater.cloneInContext(contextThemeWrapper);
        View view = inflater.inflate(R.layout.gift_image_gallery_adapter, null,
                false);
        try {

            ImageView imageView_Photo = (ImageView) view
                    .findViewById(R.id.imageView_Photo);

            try {

                JSONObject jsonObject = new JSONObject(data.get(position));

                Picasso.with(myContext).load(jsonObject.getString("filename"))

                        .placeholder(R.drawable.default_cover_wholesaler).into(imageView_Photo);
            } catch (Exception e) {
            }

            view.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    try {
                        mGoogleHelper.SendEventGoogleAnalytics(
                                myContext,
                                "Gallery Image"
                                , "Gallery Button Clicked",
                                "ButtonEvent");

                        Intent slideIntent = new Intent(myContext,
                                ImagesSlideActivity.class);

                        Bundle b = new Bundle();
                        b.putInt("position", position);
                        b.putStringArrayList("Array", data);
                        // b.putString("Array", data.toString());
                        b.putInt("requestFrom", 1);
                        slideIntent.putExtras(b);
                        myContext.startActivity(slideIntent);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth,
                                             int reqHeight) {
        Bitmap bm = null;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(path, options);

        return bm;
    }

    public int calculateInSampleSize(

            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }

        return inSampleSize;
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

}
