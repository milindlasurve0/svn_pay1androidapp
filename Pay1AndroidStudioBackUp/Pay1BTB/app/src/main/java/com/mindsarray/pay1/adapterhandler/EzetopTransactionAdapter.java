package com.mindsarray.pay1.adapterhandler;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindsarray.pay1.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class EzetopTransactionAdapter extends ArrayAdapter<JSONObject> {
    Context mContext;
    List<JSONObject> jsonObject;
    LayoutInflater inflater;
    TextView TimeText, textTransactionAmount, textTransactionId;
    ImageView imageStatus, imageOperator;

    public EzetopTransactionAdapter(Context context, List<JSONObject> objects) {
        super(context, 0, objects);
        // TODO Auto-generated constructor stub
        this.mContext = context;
        this.jsonObject = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View view = convertView;
        if (convertView == null) {
            inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.ezetap_transaction_row, null);
        }
        TimeText = (TextView) view.findViewById(R.id.TimeText);
        textTransactionAmount = (TextView) view
                .findViewById(R.id.textTransactionAmount);
        textTransactionId = (TextView) view
                .findViewById(R.id.textTransactionId);
        imageStatus = (ImageView) view.findViewById(R.id.imageStatus);
        imageOperator = (ImageView) view.findViewById(R.id.imageOperator);
        try {
            textTransactionAmount.setText("Rs. "
                    + jsonObject.get(position).getString("amount"));
            textTransactionId.setText(jsonObject.get(position).getString(
                    "transaction_id") + "\nTransaction Id");

            TimeText.setText(jsonObject.get(position).getString("datetime"));

            String txnType = jsonObject.get(position).getString("txnType");
            String txnMode = jsonObject.get(position).getString("txnMode");
            String status_flag = jsonObject.get(position).getString("status_flag");

            if (status_flag.equalsIgnoreCase("1")) {//SALE
                imageStatus.setImageDrawable(mContext.getResources()
                        .getDrawable(R.drawable.doubletick));
            } else if (status_flag.equalsIgnoreCase("0")) {//CASH
                imageStatus.setImageDrawable(mContext.getResources()
                        .getDrawable(R.drawable.not_sent));
            } else {
                imageStatus.setImageDrawable(mContext.getResources()
                        .getDrawable(R.drawable.not_sent));
            }


            if (txnMode.equalsIgnoreCase("1")) {//SALE
                imageOperator.setImageDrawable(mContext.getResources()
                        .getDrawable(R.drawable.ic_mpos_purchase_row));
            } else if (txnMode.equalsIgnoreCase("0")) {//CASH
                imageOperator.setImageDrawable(mContext.getResources()
                        .getDrawable(R.drawable.ic_atm_cash_hdpi_row));
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            // e.printStackTrace();
        }


        return view;
    }

}
