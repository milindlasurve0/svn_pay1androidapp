package com.mindsarray.pay1.databasehandler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class VMNDataSource {

    // Database fields
    private SQLiteDatabase database;
    private Pay1SQLiteHelper dbHelper;

    public VMNDataSource(Context context) {
        dbHelper = new Pay1SQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void createVMN(int id, String vmnNumber) {
        ContentValues values = new ContentValues();
        values.put(Pay1SQLiteHelper.VMN_ID, id);
        values.put(Pay1SQLiteHelper.VMN_NUMBER, vmnNumber);

        database.insert(Pay1SQLiteHelper.TABLE_VMN, null, values);
        // Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_VMN,
        // null, Pay1SQLiteHelper.VMN_ID + " = " + insertId, null,
        // null, null, null);
        // cursor.moveToFirst();
        // VMN neVMN = cursorToVMN(cursor);
        // cursor.close();
        // return newVMN;
    }

    public void deleteVMN() {
        database.delete(Pay1SQLiteHelper.TABLE_VMN, null, null);
    }

    public void deleteVMN(VMN VMN) {
        long id = VMN.getVmnID();
        database.delete(Pay1SQLiteHelper.TABLE_VMN, Pay1SQLiteHelper.VMN_ID
                + " = " + id, null);
    }

    public List<VMN> getAllVMN() {
        List<VMN> vmns = new ArrayList<VMN>();

        Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_VMN, null, null,
                null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            VMN vmn = cursorToVMN(cursor);
            vmns.add(vmn);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return vmns;
    }

    public VMN getVMN(int id) {
        VMN vmn = new VMN();

        Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_VMN, null,
                Pay1SQLiteHelper.VMN_ID + " = " + id, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            vmn = cursorToVMN(cursor);

            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return vmn;
    }

    private VMN cursorToVMN(Cursor cursor) {
        VMN vmn = new VMN();
        vmn.setVmnID(cursor.getInt(0));
        vmn.setVmnNumber(cursor.getString(1));

        return vmn;
    }
}
