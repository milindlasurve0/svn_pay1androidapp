package com.mindsarray.pay1;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.EditTextValidator;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class WalletMainFragment extends Fragment {

    View vi = null;
    private EditText editMobileNumber, editRechargeAmount;
    private Button mBtnRchNow;
    private static final String TAG = "Pay1 Topup";
    String mobileNumber, amount, planJson;

    String uuid;
    boolean bNetOrSms;
    private SharedPreferences mSettingsData;// Shared preferences for setting
    public static GoogleAnalytics analytics;
    public static Tracker tracker;                                // data
    long timestamp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


//		 analytics = GoogleAnalytics.getInstance(getActivity()); 
        tracker = Constants.setAnalyticsTracker(getActivity(), tracker);
        //easyTracker.set(Fields.SCREEN_NAME, TAG);
        vi = inflater.inflate(R.layout.walletrecharge_activity, container,
                false);
        try {

            try {
                TelephonyManager tManager = (TelephonyManager) getActivity()
                        .getSystemService(Context.TELEPHONY_SERVICE);
                uuid = tManager.getDeviceId();
                if (uuid == null) {
                    uuid = Secure.getString(getActivity().getContentResolver(),
                            Secure.ANDROID_ID);
                }
                if (Utility.getUUID(getActivity(),
                        Constants.SHAREDPREFERENCE_UUID) == null)
                    Utility.setUUID(getActivity(),
                            Constants.SHAREDPREFERENCE_UUID, uuid);
            } catch (Exception exception) {
            }

            mSettingsData = getActivity().getSharedPreferences(
                    SettingsSmsActivity.SETTINGS_NAME, 0);
            bNetOrSms = mSettingsData.getBoolean(
                    SettingsSmsActivity.KEY_NETWORK, true);

            editMobileNumber = (EditText) vi
                    .findViewById(R.id.editMobileNumber);
            editRechargeAmount = (EditText) vi
                    .findViewById(R.id.editRechargeAmount);

            editMobileNumber.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    // TODO Auto-generated method stub
                    // EditTextValidator.hasText(getActivity(),
                    // editMobileNumber,
                    // Constants.ERROR_MOBILE_BLANK_FIELD);
                    if (editMobileNumber.length() == 10) {
                        editRechargeAmount.requestFocus();
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable s) {
                    // TODO Auto-generated method stub

                }
            });

            // editRechargeAmount.addTextChangedListener(new TextWatcher() {
            // public void afterTextChanged(Editable s) {
            // EditTextValidator.hasText(getActivity(),
            // editRechargeAmount,
            // Constants.ERROR_AMOUNT_BLANK_FIELD);
            // }
            //
            // public void beforeTextChanged(CharSequence s, int start,
            // int count, int after) {
            // }
            //
            // public void onTextChanged(CharSequence s, int start,
            // int before, int count) {
            // }
            // });

            mBtnRchNow = (Button) vi.findViewById(R.id.mBtnRchNow);
            mBtnRchNow.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    bNetOrSms = mSettingsData.getBoolean(
                            SettingsSmsActivity.KEY_NETWORK, true);

                    timestamp = System.currentTimeMillis();
                    mobileNumber = editMobileNumber.getText().toString();
                    amount = editRechargeAmount.getText().toString();

                    if (!checkValidation(getActivity())) {

                    } else {

                        if (bNetOrSms) {

                            if (Utility.getLoginFlag(getActivity(),
                                    Constants.SHAREDPREFERENCE_IS_LOGIN)) {

                                new AlertDialog.Builder(getActivity())
                                        .setTitle("Pay1 wallet topup")
                                        .setMessage(
                                                "Mobile Number: "
                                                        + mobileNumber
                                                        + "\nAmount: "
                                                        + amount
                                                        + "\nAre you sure you want to do topup?")
                                        .setPositiveButton(
                                                "Yes",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(
                                                            DialogInterface dialog,
                                                            int which) {
                                                        dialog.dismiss();
                                                        new WalletPaymentTask()
                                                                .execute();

                                                    }
                                                })
                                        .setNegativeButton(
                                                "No",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(
                                                            DialogInterface dialog,
                                                            int which) {
                                                        dialog.dismiss();
                                                    }
                                                }).show();
                            } else {
                                Intent intent = new Intent(getActivity(),
                                        LoginActivity.class);
                                startActivity(intent);
                            }
                        } else {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Pay1 wallet topup (SMS)")
                                    .setMessage(
                                            "Mobile Number: "
                                                    + mobileNumber
                                                    + "\nAmount: "
                                                    + amount
                                                    + "\nAre you sure you want to do topup?")
                                    .setPositiveButton(
                                            "Yes",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(
                                                        DialogInterface dialog,
                                                        int which) {
                                                    dialog.dismiss();
                                                    Constants
                                                            .sendSMSMessage(
                                                                    getActivity(),
                                                                    Constants.WALLET_PAYMENT,
                                                                    Constants.OPERATOR_PAY1_WALLET,
                                                                    mobileNumber,
                                                                    mobileNumber,
                                                                    amount,
                                                                    "0", "");
                                                    // getActivity().finish();
                                                }
                                            })
                                    .setNegativeButton(
                                            "No",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(
                                                        DialogInterface dialog,
                                                        int which) {
                                                    dialog.dismiss();
                                                }
                                            }).show();

                        }

                    }
                }
            });
        } catch (Exception e) {
        }
        return vi;

    }

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        //tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
    }

    public static WalletMainFragment newInstance(String content) {
        WalletMainFragment fragment = new WalletMainFragment();

        Bundle b = new Bundle();
        b.putString("msg", content);

        fragment.setArguments(b);

        return fragment;
    }

    private boolean checkValidation(Context context) {
        boolean ret = true;

        if (!EditTextValidator.hasText(context, editMobileNumber,
                Constants.ERROR_MOBILE_BLANK_FIELD)) {
            ret = false;
            editMobileNumber.requestFocus();
            return ret;
        } else if (!EditTextValidator.isValidMobileNumber(context,
                editMobileNumber, Constants.ERROR_MOBILE_LENGTH_FIELD)) {
            ret = false;
            editMobileNumber.requestFocus();
            return ret;
        } else if (!EditTextValidator.hasText(context, editRechargeAmount,
                Constants.ERROR_AMOUNT_BLANK_FIELD)) {
            ret = false;
            editRechargeAmount.requestFocus();
            return ret;
        } else
            return ret;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // if ((savedInstanceState != null)
        // && savedInstanceState.containsKey(KEY_CONTENT)) {
        // mContent = savedInstanceState.getString(KEY_CONTENT);
        // }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // outState.putString(KEY_CONTENT, mContent);
    }

    public class WalletPaymentTask extends AsyncTask<String, String, String>
            implements OnDismissListener {
        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
            listValuePair.add(new BasicNameValuePair("method", "pay1Wallet"));
            listValuePair.add(new BasicNameValuePair("mobileNumber",
                    mobileNumber));
            listValuePair.add(new BasicNameValuePair("amount", amount));
            listValuePair.add(new BasicNameValuePair("timestamp", String
                    .valueOf(timestamp)));
            listValuePair.add(new BasicNameValuePair("profile_id", Utility
                    .getProfileID(getActivity(),
                            Constants.SHAREDPREFERENCE_PROFILE_ID)));
            listValuePair.add(new BasicNameValuePair("hash_code", Constants
                    .encryptPassword(Utility.getUUID(getActivity(),
                            Constants.SHAREDPREFERENCE_UUID)
                            + mobileNumber
                            + amount + timestamp)));
            listValuePair.add(new BasicNameValuePair("device_type", "android"));

            String response = RequestClass.getInstance().readPay1Request(
                    getActivity(), Constants.API_URL, listValuePair);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");
                    JSONArray array = new JSONArray(replaced);
                    JSONObject jsonObject = array.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    // String rechargeString = Constants.getRechargeString(
                    // mContext, Constants.RECHARGE_MOBILE, mOperatorId,
                    // mobileNumber, mobileNumber, amount, "0");
                    /*
					 * try { notificationDataSource.open();
					 * notificationDataSource.createNotification(rechargeString,
					 * 0, "" + System.currentTimeMillis()); } catch
					 * (SQLException exception) { // TODO: handle exception //
					 * // Log.e("Er ", exception.getMessage()); } catch
					 * (Exception e) { // TODO: handle exception // //
					 * Log.e("Er ", e.getMessage()); } finally {
					 * notificationDataSource.close(); }
					 */

                    // ContentValues contentValues = new ContentValues();
                    // contentValues.put("notification_msg", rechargeString);
                    // contentValues.put("timestamp ", "12345678902");
                    // contentValues.put("netOrSms ", 0);
                    // controller.insertNotifications(contentValues);
                    if (status.equalsIgnoreCase("success")) {
                        // Constants.showCustomToast(getActivity(),
                        // "Bill payment request sent successfully.");
                        Constants.showOneButtonSuccessDialog(getActivity(),
                                "Pay1 wallet topup done successfully.\nTransaction id : "
                                        + jsonObject.getString("description"),
                                Constants.RECHARGE_SETTINGS);
                        String balance = jsonObject.getString("balance");
                        Utility.setBalance(getActivity(),
                                Constants.SHAREDPREFERENCE_BALANCE, balance);

                        // finish();
                    } else {
                        Constants.showOneButtonFailureDialog(getActivity(),
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(getActivity(),
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(getActivity(),
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(getActivity(),
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(getActivity());
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(false);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            WalletPaymentTask.this.cancel(true);
                            dialog.cancel();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            WalletPaymentTask.this.cancel(true);
            dialog.cancel();
        }

    }

}
