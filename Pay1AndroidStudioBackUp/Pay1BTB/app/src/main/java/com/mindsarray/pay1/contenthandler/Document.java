package com.mindsarray.pay1.contenthandler;


/**
 * Created by rohan on 17/11/15.
 */
public class Document {

    public String id;
    public String type;
    public String uri;
    public String verify_flag;
    public String created_on;
    public String comment;

    public Document(String id, String type, String uri, String verify_flag, String created_on, String comment) {
        this.id = id;
        this.type = type;
        this.uri = uri;
        this.verify_flag = verify_flag;
        this.created_on = created_on;
        this.comment = comment;
    }

    public void set_uri(String uri) {
        this.uri = uri;
    }
}
