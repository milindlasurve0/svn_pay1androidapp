package com.mindsarray.pay1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.contenthandler.Group;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.EditTextValidator;
import com.mindsarray.pay1.utilities.GooglePlayServicesApiClient;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RegistrationActivity extends Activity {

    private static final String TAG = "Registration";
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    public static GoogleAnalytics analytics;
    public static Tracker tracker;
    EditText editName, editShopName, editEmail, editMobile, editArea, editCity, editState,
            editPincode, editComment, editPassword, editConfPassword;
    Button mBtnRegister;
    RegistrationTask registrationTask;
    ImageView imageViewClose;
    SparseArray<Group> mGroups = new SparseArray<Group>();
    Spinner areaOfBusinessSpinner, natureOfBusinessSpinner;
    ExpandableListView mListView;
    CreateRetailerTask mCreateRetailerTask;
    //	private AutoCompleteTextView editCity;
//	private PlaceArrayAdapter mPlaceArrayAdapter;
    GooglePlayServicesApiClient mGooglePlayServicesApiClient;
    private GoogleApiClient mGoogleApiClient;
    private SpinnerTextView spinnerTextView;
    private boolean mResolvingError = false;

    private Context mContext = RegistrationActivity.this;
/*
    private void setListViewHeight(ExpandableListView listView,
								   int group) {
		ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();
		int totalHeight = 0;
		int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
				View.MeasureSpec.EXACTLY);
		for (int i = 0; i < listAdapter.getGroupCount(); i++) {
			View groupItem = listAdapter.getGroupView(i, false, null, listView);
			groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

			totalHeight += groupItem.getMeasuredHeight();

			if (((listView.isGroupExpanded(i)) && (i != group))
					|| ((!listView.isGroupExpanded(i)) && (i == group))) {
				for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
					View listItem = listAdapter.getChildView(i, j, false, null,
							listView);
					listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

					totalHeight += listItem.getMeasuredHeight();

				}
			}
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		int height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
		if (height < 10)
			height = 200;
		params.height = height;
		listView.setLayoutParams(params);
		listView.requestLayout();

	}*/
//    @Override
//    public void onConnectionFailed(ConnectionResult result) {
//    	if (mResolvingError) {
//            // Already attempting to resolve an error.
//            return;
//        } else if (result.hasResolution()) {
//            try {
//                mResolvingError = true;
//                result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
//            } catch (SendIntentException e) {
//                // There was an error with the resolution intent. Try again.
//                mGoogleApiClient.connect();
//            }
//        } else {
//            // Show dialog using GooglePlayServicesUtil.getErrorDialog()
////            showErrorDialog(result.getErrorCode());
//            mResolvingError = true;
//        } 
//    	Log.e("Reg Act: ", "failed " + result.getErrorCode());
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            setContentView(R.layout.registration_activity);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

           /* Utility.setSharedPreference(RegistrationActivity.this, "Registration_interest", "");
            Utility.setSharedPreference(RegistrationActivity.this, "Distributor_interest", "");
            Utility.setSharedPreference(RegistrationActivity.this, "Retailer_Mobile", "");*/


            Constants.promptForGPS((Activity) mContext);
            mGooglePlayServicesApiClient = new GooglePlayServicesApiClient(mContext, LocationServices.API, true);
            mGoogleApiClient = mGooglePlayServicesApiClient.mGoogleApiClient;


            findViewById();
            imageViewClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            if (getIntent().getExtras() != null) {
                editMobile.setText(getIntent().getStringExtra("MobileNumber").toString());
            }
            tracker = Constants.setAnalyticsTracker(this, tracker);


            String[] natureOfBusinessArray = new String[]{"--Select nature of business--", "Mobile" +
                    " Store", "Stationary Shop",
                    "Medical Store",
                    "Grocery" +
                            " Store", "Photocopy Shop", "Hardware Store", "Travel Agency", "Other"};


            String[] areaOfBusinessArray = new String[]{"--Select area of business--",
                    "Residential Area", "Industrial Area",
                    "Commercial Area"};


            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item_layout, natureOfBusinessArray);

            natureOfBusinessSpinner.setAdapter(adapter);

            natureOfBusinessSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    Log.v("item", (String) parent.getItemAtPosition(position));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub
                }
            });

            ArrayAdapter<String> areaOfBusinessAdapter = new ArrayAdapter<String>(this,
                    R.layout.spinner_item_layout, areaOfBusinessArray);

            areaOfBusinessSpinner.setAdapter(areaOfBusinessAdapter);

            areaOfBusinessSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    Log.v("item", (String) parent.getItemAtPosition(position));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub
                }
            });


           /* editName.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    // TODO Auto-generated method stub
                    EditTextValidator.hasText(RegistrationActivity.this,
                            editName, Constants.ERROR_NAME_BLANK_FIELD);
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable s) {
                    // TODO Auto-generated method stub

                }
            });
            editShopName.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    // TODO Auto-generated method stub
                    EditTextValidator.hasText(RegistrationActivity.this,
                            editShopName, Constants.ERROR_NAME_BLANK_FIELD);
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable s) {
                    // TODO Auto-generated method stub

                }
            });
            editEmail.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    // TODO Auto-generated method stub
                    EditTextValidator.isEmailAddress(RegistrationActivity.this,
                            editEmail, Constants.ERROR_EMAIL_BLANK_FIELD);
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable s) {
                    // TODO Auto-generated method stub

                }
            });
            editMobile.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    // TODO Auto-generated method stub
                    EditTextValidator.hasText(RegistrationActivity.this,
                            editMobile, Constants.ERROR_MOBILE_BLANK_FIELD);
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable s) {
                    // TODO Auto-generated method stub

                }
            });
            editCity.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    // TODO Auto-generated method stub
                    EditTextValidator.hasText(RegistrationActivity.this,
                            editCity, Constants.ERROR_CITY_BLANK_FIELD);

//					String query = editCity.getText().toString().trim();
//					mPlaceArrayAdapter.getFilter().filter(query);
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable s) {
                    // TODO Auto-generated method stub

                }
            });*/

//			editCity.setOnItemClickListener(new OnItemClickListener() {
//			    @Override
//			    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
//			                            long arg3) {
//			    	String cityState = arg0.getItemAtPosition(arg2).toString();
//			    	Log.e("city state", cityState);
//			    	String[] locationArray = cityState.split(", ");
//			    	if(locationArray.length > 0){
//				    	editCity.setText(locationArray[0]);
//				    	if(locationArray.length > 1){
//				    		editState.setText(locationArray[1]);
//				    	}
//			    	}
//			    }
//			});

            /*editState.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    // TODO Auto-generated method stub
                    EditTextValidator.hasText(RegistrationActivity.this,
                            editState, Constants.ERROR_STATE_BLANK_FIELD);
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable s) {
                    // TODO Auto-generated method stub

                }
            });*/

            mBtnRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (editName.getText().toString().trim().length() != 0) {

                    } else {
                        setErrorToEditText(mContext, "Please enter your name", editName);
                        return;
                    }

                    if (editMobile.getText().toString().trim().length() != 0) {

                    } else {
                        setErrorToEditText(mContext, "Please enter mobile", editMobile);
                        return;
                    }

                    if (editMobile.getText().toString().matches("[7-9][0-9]{9}$")) {

                    } else {
                        setErrorToEditText(mContext, "Please enter correct mobile", editMobile);
                        return;
                    }

                    if (editEmail.getText().toString().trim().length() != 0) {

                    } else {
                        setErrorToEditText(mContext, "Please enter email", editEmail);
                        return;
                    }

                    if (EditTextValidator.isEmailAddress(RegistrationActivity.this,
                            editEmail, Constants.ERROR_EMAIL_BLANK_FIELD)) {

                    } else {
                        setErrorToEditText(mContext, "Please enter correct email", editEmail);
                        return;
                    }

                    if (editPassword.getText().toString().trim().length() != 0) {

                    } else {
                        setErrorToEditText(mContext, "Please enter password", editPassword);
                        return;
                    }


                    if (editConfPassword.getText().toString().trim().length() != 0) {

                    } else {
                        setErrorToEditText(mContext, "Please enter confirm password", editConfPassword);
                        return;
                    }
                    if (editPassword.getText().toString
                            ().trim().length() < 4) {
                        setErrorToEditText(mContext, "Please enter correct password", editPassword);
                        return;
                    } else {

                    }
                    if (editConfPassword.getText().toString
                            ().trim().length() < 4) {
                        setErrorToEditText(mContext, "Please enter correct confirm password", editConfPassword);
                        return;
                    } else {

                    }

                    if (editCity.getText().toString().trim().length() != 0) {

                    } else {
                        setErrorToEditText(mContext, "Please enter address", editCity);
                        return;
                    }

                    if (editArea.getText().toString().trim().length() != 0) {

                    } else {
                        setErrorToEditText(mContext, "Please enter area", editArea);
                        return;
                    }

                    if (editPincode.getText().toString().trim().length() != 0) {

                    } else {
                        setErrorToEditText(mContext, "Please enter pincode", editPincode);
                        return;
                    }

                    if (editPincode.getText().toString().trim().length() == 6) {

                    } else {
                        setErrorToEditText(mContext, "Please enter correct pincode", editPincode);
                        return;
                    }

                    if (editShopName.getText().toString().trim().length() != 0) {

                    } else {
                        setErrorToEditText(mContext, "Please enter shopName", editShopName);
                        return;
                    }

                    if (editConfPassword.getText().toString().equalsIgnoreCase(editPassword.getText().toString())) {

                    } else {
                        Toast.makeText(mContext, "Password should be same", Toast.LENGTH_LONG).show();
                        setErrorToEditText(mContext, "Password should be same", editConfPassword);
                        return;
                    }


                    if (natureOfBusinessSpinner.getSelectedItemPosition() > 0) {

                    } else {
                        Toast.makeText(mContext, "Please select Nature of Business", Toast.LENGTH_LONG).show();
                        return;
                    }

                    if (areaOfBusinessSpinner.getSelectedItemPosition() > 0) {

                    } else {
                        Toast.makeText(mContext, "Please select Area of Business", Toast.LENGTH_LONG).show();
                        return;
                    }

                    mCreateRetailerTask = new CreateRetailerTask();
                    mCreateRetailerTask.execute();
                }
            });




			/*mBtnCancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					startActivity(new Intent(RegistrationActivity.this, LoginActivity.class));
				}
			});*/


			/*createData();
			mListView = (ExpandableListView) findViewById(R.id.activity_expandable_list_view);
			MyExpandableListAdapter adapter = new MyExpandableListAdapter(RegistrationActivity.this,
					mGroups);
			mListView.setAdapter(adapter);
			mListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

				@Override
				public boolean onGroupClick(ExpandableListView parent, View v,
											int groupPosition, long id) {
					setListViewHeight(parent, groupPosition);
					return false;
				}
			});*/


        } catch (Exception e) {
            // TODO: handle exception
            Log.e("Registration Acti ", e.getMessage());
        } finally {
        }

        editComment.setOnEditorActionListener(new OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                mBtnRegister.requestFocus();
                return false;
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_RESOLVE_ERROR) {
            mResolvingError = false;
            if (resultCode == RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!mGoogleApiClient.isConnecting() &&
                        !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            }
        }
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        //tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
        if (!mResolvingError) {  // more about this later
            mGoogleApiClient.connect();
        }
    }

    private boolean checkValidation(Context context) {
        boolean ret = true;

        if (!EditTextValidator.hasText(context, editName,
                Constants.ERROR_NAME_BLANK_FIELD)) {
            ret = false;
            editName.requestFocus();
            return ret;
        } else if (!EditTextValidator.hasText(context, editMobile,
                Constants.ERROR_MOBILE_BLANK_FIELD)) {
            ret = false;
            editMobile.requestFocus();
            return ret;
        } else if (!EditTextValidator.isValidMobileNumber(context, editMobile,
                Constants.ERROR_MOBILE_LENGTH_FIELD)) {
            ret = false;
            editMobile.requestFocus();
            return ret;
        } else if (!EditTextValidator.isEmailAddress(context, editEmail,
                Constants.ERROR_EMAIL_BLANK_FIELD)) {
            ret = false;
            editEmail.requestFocus();
            return ret;
        } else if (!EditTextValidator.hasText(context, editPassword,
                Constants.ERROR_PASSWORD_BLANK_FIELD))

        {
            ret = false;
            editPassword.requestFocus();
            return ret;
        } else if (!EditTextValidator.hasText(context, editConfPassword,
                Constants.ERROR_PASSWORD_BLANK_FIELD))

        {
            ret = false;
            editConfPassword.requestFocus();
            return ret;
        } else if (!EditTextValidator.hasText(context, editCity,
                Constants.ERROR_CITY_BLANK_FIELD)) {
            ret = false;
            editCity.requestFocus();
            return ret;
        } else if (!EditTextValidator.hasText(context, editArea,
                Constants.ERROR_AREA_BLANK_FIELD)) {
            ret = false;
            editArea.requestFocus();
            return ret;
        } else if (!EditTextValidator.hasText(context, editPincode,
                Constants.ERROR_LOGIN_PIN_BLANK_FIELD)) {
            ret = false;
            editPincode.requestFocus();
            return ret;
        } else if (!editMobile.getText().toString().matches("[7-9][0-9]{9}$")) {
            ret = false;
            editMobile.requestFocus();
            return ret;
        } else if (!EditTextValidator.hasText(context, editShopName,
                Constants.ERROR_NAME_BLANK_FIELD)) {
            ret = false;
            editShopName.requestFocus();
            return ret;
        } else {
            return ret;
        }
    }

    private void setErrorToEditText(Context context, String errorMessage, EditText editText) {
        int ecolor = R.color.black;
        ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
                errorMessage);
        ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
        editText.setError(ssbuilder);
        /*editText.setBackgroundDrawable(context.getResources().getDrawable(
                R.drawable.edittext_error_background_selector));*/
        editText.setPadding(10, 10, 10, 10);
        editText.setTextSize(14);
        editText.requestFocus();
    }

    private void findViewById() {
        editName = (EditText) findViewById(R.id.editName);
        editShopName = (EditText) findViewById(R.id.editShopName);
        editEmail = (EditText) findViewById(R.id.editEmail);
        editMobile = (EditText) findViewById(R.id.editMobile);
        editArea = (EditText) findViewById(R.id.editArea);
        editCity = (EditText) findViewById(R.id.editCity);
        editState = (EditText) findViewById(R.id.editState);
        editPincode = (EditText) findViewById(R.id.editPincode);
        editPassword = (EditText) findViewById(R.id.editPassword);
        editConfPassword = (EditText) findViewById(R.id.editConfPassword);
        editComment = (EditText) findViewById(R.id.editComment);
        mBtnRegister = (Button) findViewById(R.id.mBtnRegister);
        areaOfBusinessSpinner = (Spinner) findViewById(R.id.areaOfBusinessSpinner);
        natureOfBusinessSpinner = (Spinner) findViewById(R.id.natureOfBusinessSpinner);
        spinnerTextView = (SpinnerTextView) findViewById(R.id.registration_interests_spinner);
        imageViewClose = (ImageView) findViewById(R.id.imageViewClose);
        //mBtnCancel = (Button) findViewById(R.id.mBtnCancel);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    public class RegistrationTask extends AsyncTask<String, String, String>
            implements OnDismissListener {
        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
            listValuePair.add(new BasicNameValuePair("method", "addLeads"));
            listValuePair.add(new BasicNameValuePair("full_name", editName
                    .getText().toString().trim()));
            listValuePair.add(new BasicNameValuePair("email", editEmail
                    .getText().toString().trim()));
            listValuePair.add(new BasicNameValuePair("contact_no", editMobile
                    .getText().toString().trim()));
            listValuePair.add(new BasicNameValuePair("area", editArea.getText()
                    .toString().trim()));
            listValuePair.add(new BasicNameValuePair("city", editCity.getText()
                    .toString().trim()));
            listValuePair.add(new BasicNameValuePair("state", editState
                    .getText().toString().trim()));
            listValuePair.add(new BasicNameValuePair("pin_code", editPincode.getText()
                    .toString().trim()));
            listValuePair.add(new BasicNameValuePair("comment", editComment
                    .getText().toString().trim()));
            listValuePair.add(new BasicNameValuePair("req_by", "android"));

            String response = RequestClass.getInstance()
                    .readPay1Request(RegistrationActivity.this,
                            Constants.API_URL, listValuePair);

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");

                    JSONArray jsonArray = new JSONArray(replaced);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
//						Constants.showOneButtonSuccessDialog(RegistrationActivity.this,
//								"Thank you for your interest in \n becoming a PAY1 retail partner.\n"
//								+ "\n We will be in touch with you soon. \n Feel free to call us on 022 0123456."
//										.trim(), Constants.RECHARGE_SETTINGS);
                        if (Utility.getSharedPreferences(RegistrationActivity.this, "Distributor_interest") == "Distributor") {
                            startActivity(new Intent(RegistrationActivity.this, DistributorInterestActivity.class));
                        }
                    } else {
                        Constants.showOneButtonFailureDialog(
                                RegistrationActivity.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(
                            RegistrationActivity.this,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(RegistrationActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(RegistrationActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(RegistrationActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            RegistrationTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            RegistrationTask.this.cancel(true);
            dialog.cancel();
        }

    }

    public class CreateRetailerTask extends AsyncTask<String, String, String>
            implements OnDismissListener {
        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
            listValuePair.add(new BasicNameValuePair("method", "createRetDistLeads"));
            listValuePair.add(new BasicNameValuePair("r_n", editName
                    .getText().toString().trim()));
            listValuePair.add(new BasicNameValuePair("r_s_n", editShopName
                    .getText().toString().trim()));
            listValuePair.add(new BasicNameValuePair("r_e", editEmail
                    .getText().toString().trim()));
            listValuePair.add(new BasicNameValuePair("r_m", editMobile
                    .getText().toString().trim()));
            listValuePair.add(new BasicNameValuePair("r_a_r", editArea.getText()
                    .toString().trim()));
            listValuePair.add(new BasicNameValuePair("r_a_d", editCity.getText()
                    .toString().trim()));
			/*listValuePair.add(new BasicNameValuePair("r_s", editState
					.getText().toString().trim()));*/
            listValuePair.add(new BasicNameValuePair("r_p", editPincode.getText()
                    .toString().trim()));
//bbusiness nature
            listValuePair.add(new BasicNameValuePair("r_b_n", natureOfBusinessSpinner.getSelectedItem().toString()));
//bbusiness area
            listValuePair.add(new BasicNameValuePair("r_b_a", areaOfBusinessSpinner.getSelectedItem()
                    .toString()));
            listValuePair.add(new BasicNameValuePair("r_p_d", editPassword
                    .getText().toString().trim()));
            listValuePair.add(new BasicNameValuePair("req_by", "android"));
            listValuePair.add(new BasicNameValuePair("reg_i", "Retailer"));

            String response = RequestClass.getInstance()
                    .readPay1Request(RegistrationActivity.this,
                            Constants.API_URL, listValuePair);

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            try {
                if (!result.startsWith("Error")) {
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");

                    JSONArray jsonArray = new JSONArray(replaced);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        Intent intent = new Intent(RegistrationActivity.this,
                                LeadGenerationOtpActivity.class);
                        intent.putExtra("MobileNumber", editMobile.getText().toString().trim());
                        startActivity(intent);

                        /*{
                        if (Utility.getSharedPreferences(RegistrationActivity.this, "Registration_interest") == "Retailer") {
                            startActivity(new Intent(RegistrationActivity.this, RegistrationPaymentOptionActivity.class));
                        } else if (Utility.getSharedPreferences(RegistrationActivity.this, "Distributor_interest") == "Retailer") {
                            startActivity(new Intent(RegistrationActivity.this, RegistrationPaymentOptionActivity.class));
                        } else if (Utility.getSharedPreferences(RegistrationActivity.this, "Distributor_interest") == "Distributor") {
                            startActivity(new Intent(RegistrationActivity.this, DistributorInterestActivity.class));
                        }
                    }*/
                    } else {
                        Constants.showOneButtonFailureDialog(
                                RegistrationActivity.this,
                                Constants.checkCode(replaced), TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(
                            RegistrationActivity.this,
                            Constants.ERROR_INTERNET, TAG,
                            Constants.OTHER_ERROR);

                }
            } catch (JSONException e) {
                // e.printStackTrace();
                Constants.showOneButtonFailureDialog(RegistrationActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                // TODO: handle exception
                Constants.showOneButtonFailureDialog(RegistrationActivity.this,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(RegistrationActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            CreateRetailerTask.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            CreateRetailerTask.this.cancel(true);
            dialog.cancel();
        }

    }
}
