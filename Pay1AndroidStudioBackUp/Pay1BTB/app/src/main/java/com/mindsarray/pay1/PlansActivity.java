package com.mindsarray.pay1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.PlansAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.Plan;
import com.mindsarray.pay1.databasehandler.PlanDataSource;
import com.mindsarray.pay1.requesthandler.RequestClass;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PlansActivity extends Activity {
    Spinner spinner_circle, spinner_plantype;
    ArrayList<JSONObject> circleArrayList, operatorArrayList;
    // Button btnGetPlans;
    List<String> circleList, operatorList;
    String circleArray, operatorArray, operatorId, planJson;
    String circleCode;
    JSONArray operatorJArray, jsonArray;
    ListView planList;
    String planType;
    PlansAdapter adapter;
    PlanDataSource planDataSource;
    ArrayList<JSONObject> dataPlanArrayList;
    JSONObject jsonObject2;
    ArrayList<String> planCategoryList;
    ArrayAdapter<String> planCategoryAdapter;
    int isDataCardPlan = 0;
    private static final String TAG = "Plans Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plans_activity);
        tracker = Constants.setAnalyticsTracker(this, tracker);
        try {

            circleArrayList = new ArrayList<JSONObject>();
            operatorArrayList = new ArrayList<JSONObject>();
            spinner_circle = (Spinner) findViewById(R.id.spinner_circle);
            spinner_plantype = (Spinner) findViewById(R.id.spinner_plantype);
            spinner_plantype.setVisibility(View.GONE);
            planList = (ListView) findViewById(R.id.planList);
            circleList = new ArrayList<String>();
            operatorList = new ArrayList<String>();
            planCategoryList = new ArrayList<String>();

            planDataSource = new PlanDataSource(PlansActivity.this);

            dataPlanArrayList = new ArrayList<JSONObject>();

            adapter = new PlansAdapter(PlansActivity.this, 1, dataPlanArrayList);
            planList.setAdapter(adapter);

            circleArray = Constants.loadJSONFromAsset(getApplicationContext(),
                    "circle.json");
            // try {
            // planDataSource.open();
            //
            // // planCategoryList = planDataSource.getAllPlanType();
            // } catch (SQLException exception) {
            // // TODO: handle exception
            // // // Log.e("Er ", exception.getMessage());
            // } catch (Exception e) {
            // // TODO: handle exception
            // // // Log.e("Er ", e.getMessage());
            // } finally {
            // planDataSource.close();
            // }

            jsonArray = new JSONArray(circleArray);
            for (int i = 0; i < jsonArray.length(); i++) {
                circleArrayList.add(jsonArray.getJSONObject(i));
                circleList.add(jsonArray.getJSONObject(i).getString("name"));
            }

            spinner_circle
                    .setOnItemSelectedListener(new OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> arg0,
                                                   View arg1, int position, long arg3) {
                            // TODO Auto-generated method stub
                            if (position != 0) {
                                try {
                                    circleCode = jsonArray.getJSONObject(
                                            position).getString("code");
                                    spinner_plantype.setSelection(0);
                                    planCategoryList.clear();
                                    spinner_plantype.setVisibility(View.GONE);
                                    planCategoryAdapter.notifyDataSetChanged();

                                    getPlan(getIntent().getExtras().getString(
                                            Constants.PLAN_JSON), circleCode);

                                } catch (JSONException e) {
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> arg0) {
                            // TODO Auto-generated method stub

                        }
                    });

            spinner_plantype
                    .setOnItemSelectedListener(new OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> arg0,
                                                   View arg1, int position, long arg3) {
                            // TODO Auto-generated method stub
                            if (position != 0) {
                                planType = spinner_plantype.getSelectedItem()
                                        .toString();
                                getPlanByType(planType);

                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> arg0) {
                            // TODO Auto-generated method stub

                        }
                    });

            planList.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1,
                                        int arg2, long arg3) {
                    // TODO Auto-generated method stub
                    Intent returnIntent = new Intent();
                    try {
                        returnIntent
                                .putExtra("result", dataPlanArrayList.get(arg2)
                                        .getString("amount"));
                    } catch (JSONException e) {
                        // e.printStackTrace();
                        returnIntent.putExtra("result", "0");
                    }
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
            });

			/* SELECT DISTINCT planType FROM plans */

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, circleList);

            planCategoryAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, planCategoryList);
            spinner_plantype.setAdapter(planCategoryAdapter);
            spinner_circle.setAdapter(dataAdapter);

            planCategoryAdapter
                    .setDropDownViewResource(android.R.layout.simple_list_item_1);
            dataAdapter
                    .setDropDownViewResource(android.R.layout.simple_list_item_1);

            circleCode = getIntent().getExtras().getString("CIRCLE_CODE");
            isDataCardPlan = getIntent().getExtras().getInt("DATA_CARD_PLAN");
            if (circleCode != null) {
                // Log.w("Circle Code:", circleCode);
                for (int i = 0; i < jsonArray.length(); i++) {
                    if (jsonArray.getJSONObject(i).getString("code")
                            .equalsIgnoreCase(circleCode)) {
                        spinner_circle.setSelection(i);
                    }
                }
            }
        } catch (Exception e) {
        }

    }

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        //tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
    }

    protected void getPlan(String operator, String circle) {

        dataPlanArrayList.clear();
        planCategoryList.clear();

		/*
         * if(!mSpinnerRechargeType.isSelected()){
		 * Constants.showCustomToast(PlansActivity.this, "Check",
		 * Toast.LENGTH_LONG).show(); planTy="Topup-Plans"; }else{ planTy =
		 * mSpinnerRechargeType.getSelectedItem().toString(); }
		 */

        try {
            planDataSource.open();

            List<Plan> plans = planDataSource.getAllPlan(operator, circle);
            if (plans.size() == 0) {
                new GetPlanstask().execute();
            } else {

                String LastUpdateTime = plans.get(0).getPlanUptateTime();
                int days = (int) ((System.currentTimeMillis() - Long
                        .parseLong(LastUpdateTime)) / (1000 * 60 * 60 * 24));

                if (days >= 2) {
                    //planDataSource.deletePlan();
                    new GetPlanstask().execute();
                } else {

                    JSONArray arrayList;

                    if (spinner_plantype.getVisibility() == View.VISIBLE) {
                        arrayList = planDataSource.getAllPlanDetails(
                                getIntent().getExtras().getString(
                                        Constants.PLAN_JSON), circleCode,
                                spinner_plantype.getSelectedItem().toString(),
                                isDataCardPlan);
                    } else {
                        arrayList = planDataSource.getAllPlanDetails(
                                getIntent().getExtras().getString(
                                        Constants.PLAN_JSON), circleCode, "",
                                isDataCardPlan);
                    }

                    planCategoryList.add("Select type");
                    planCategoryList.addAll(planDataSource.getPlanType(
                            getIntent().getExtras().getString(
                                    Constants.PLAN_JSON), circleCode,
                            isDataCardPlan));

                    for (int i = 0; i < arrayList.length(); i++) {
                        try {

                            JSONObject jsonObject = arrayList.getJSONObject(i);
                            // planCategoryList.add(jsonObject.getString("plantype"));
                            dataPlanArrayList.add(jsonObject);
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            // e.printStackTrace();
                        } finally {
                        }
                    }
                    spinner_plantype.setVisibility(View.VISIBLE);
                    planCategoryAdapter.notifyDataSetChanged();
                }
            }

        } catch (SQLException exception) {
        } catch (Exception e) {
        } finally {
            planDataSource.close();
        }
        adapter.notifyDataSetChanged();

    }

    protected void getPlanByType(String planType) {

        dataPlanArrayList.clear();

        try {
            planDataSource.open();

            JSONArray arrayList;

            arrayList = planDataSource.getAllPlanDetails(getIntent()
                            .getExtras().getString(Constants.PLAN_JSON), circleCode,
                    planType, isDataCardPlan);

            for (int i = 0; i < arrayList.length(); i++) {
                try {

                    JSONObject jsonObject = arrayList.getJSONObject(i);
                    dataPlanArrayList.add(jsonObject);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    // e.printStackTrace();
                } finally {
                }
            }

        } catch (SQLException exception) {
            // TODO: handle exception
            // // Log.e("Er ", exception.getMessage());
        } catch (Exception e) {
            // TODO: handle exception
            // // Log.e("Er ", e.getMessage());
        } finally {
            planDataSource.close();
        }

        adapter.notifyDataSetChanged();

        // // Log.e("map", "" + arrayList + "   " + operator);

    }

    public class GetPlanstask extends AsyncTask<String, String, String>
            implements OnDismissListener {
        ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

            listValuePair
                    .add(new BasicNameValuePair("method", "getPlanDetails"));
            listValuePair.add(new BasicNameValuePair("operator", getIntent()
                    .getExtras().getString(Constants.PLAN_JSON)));
            listValuePair.add(new BasicNameValuePair("circle", circleCode));

            String response = RequestClass.getInstance().readPay1Request(
                    PlansActivity.this, Constants.API_URL, listValuePair);
            String replaced = response.replace("(", "").replace(")", "")
                    .replace(";", "");
            savePlansTodataBase(replaced);

            return replaced;

        }

        @Override
        protected void onPostExecute(String result) {
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            super.onPostExecute(result);
            try {
                if (result != null && !result.startsWith("[null]")
                        && !result.startsWith("Error"))
                    getPlan(getIntent().getExtras().getString(
                            Constants.PLAN_JSON), circleCode);
            } catch (Exception e) {
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(PlansActivity.this);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            GetPlanstask.this.cancel(true);
                            dialog.cancel();
                        }
                    });
            this.dialog.show();
            super.onPreExecute();

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            // TODO Auto-generated method stub
            GetPlanstask.this.cancel(true);
            dialog.cancel();
        }

    }

    private void savePlansTodataBase(String planJson) {
        try {
            planDataSource.open();
            // ContentValues contentValues = new ContentValues();
            JSONArray array = new JSONArray(planJson);
            JSONObject planJsonObject = array.getJSONObject(0);

            Iterator<String> operatorKey = planJsonObject.keys();

            if (operatorKey.hasNext()) {

                long time = System.currentTimeMillis();

                JSONObject jsonObject2 = planJsonObject
                        .getJSONObject(operatorKey.next());

                String opr_name = jsonObject2.getString("opr_name");
                String prod_code_pay1 = jsonObject2.getString("prod_code_pay1");
                // contentValues.put("operator_id", prod_code_pay1);
                // contentValues.put("operator_name", opr_name);
                // contentValues.put("prod_code_pay1", prod_code_pay1);
                JSONObject circleJsonObject = jsonObject2
                        .getJSONObject("circles");
                Iterator<String> circleKey = circleJsonObject.keys();
                if (circleKey.hasNext()) {
                    JSONObject circleJsonObject2 = circleJsonObject
                            .getJSONObject(circleKey.next().toString());
                    String circleName = circleJsonObject2
                            .getString("circle_name");

                    // contentValues.put("circle_name", circleName);

                    JSONObject plansJsonObject = circleJsonObject2
                            .getJSONObject("plans");
                    String circle_id = circleJsonObject2.getString("circle_id");
                    Iterator<String> planKey = plansJsonObject.keys();
                    Iterator<String> planKeyDB = plansJsonObject.keys();
                    // contentValues.put("circle_id", circle_id);

                    while (planKey.hasNext()) {
                        String planType = planKey.next().toString();
                        JSONArray planArray = plansJsonObject
                                .getJSONArray(planType);
                        // contentValues.put("planType", planType);

                        for (int i = 0; i < planArray.length(); i++) {
                            JSONObject jsonObject = planArray.getJSONObject(i);
                            String plan_desc = jsonObject
                                    .getString("plan_desc");
                            // contentValues.put("plan_desc", plan_desc);
                            String plan_amt = jsonObject.getString("plan_amt");
                            // contentValues.put("plan_amt", plan_amt);
                            String plan_validity = jsonObject
                                    .getString("plan_validity");
                            String show_flag = jsonObject
                                    .getString("show_flag");

                            // contentValues.put("plan_validity",
                            // plan_validity);
                            // // Log.e("cORRECT", "cORRECT   " + i + "  "
                            // + jsonObject.toString());
                            // controller.insertPlans(contentValues);
                            planDataSource.createPlan(prod_code_pay1, opr_name,
                                    circle_id, circleName, planType, plan_amt,
                                    plan_validity, plan_desc, "" + time, show_flag);
                        }

                    }

                }

            }
        } catch (JSONException exception) {

        } catch (SQLException exception) {
            // TODO: handle exception
            // // Log.e("Er ", exception.getMessage());
        } catch (Exception e) {
            // TODO: handle exception
            // // Log.e("Er ", e.getMessage());
        } finally {
            planDataSource.close();
        }
    }
}
