package com.mindsarray.pay1.adapterhandler;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mindsarray.pay1.ChatActivity;
import com.mindsarray.pay1.R;
import com.mindsarray.pay1.constant.Constants;

import java.util.ArrayList;
import java.util.HashMap;

public class ChatAdapter extends BaseAdapter {

    private TextView textView_message;
    private TextView textView_time;
    private RelativeLayout wrapper;
    private LinearLayout main;
    ArrayList<HashMap<String, String>> adapterList;
    private Context context;

    public ChatAdapter(Context context,
                       ArrayList<HashMap<String, String>> adapterList) {
        this.context = context;
        this.adapterList = adapterList;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.chat_adapter, parent, false);
        }

        HashMap<String, String> map = new HashMap<String, String>();
        map = adapterList.get(position);
        wrapper = (RelativeLayout) row.findViewById(R.id.wrapper);
        main = (LinearLayout) row.findViewById(R.id.main);

        textView_message = (TextView) row.findViewById(R.id.textView_message);
        textView_time = (TextView) row.findViewById(R.id.textView_time);

        textView_message.setText(map.get(ChatActivity.CHAT_TEXT).toString()
                .trim());
        textView_time.setText(Constants.getMessageTime(map
                .get(ChatActivity.CHAT_TIME).toString().trim()));

        if (map.get(ChatActivity.CHAT_POSITION).equalsIgnoreCase("0")) {

            wrapper.setBackgroundResource(R.drawable.message_receive_background);
            main.setGravity(Gravity.LEFT);
        } else if (map.get(ChatActivity.CHAT_POSITION).equalsIgnoreCase("1")) {

            wrapper.setBackgroundResource(R.drawable.message_send_background);
            main.setGravity(Gravity.RIGHT);
        }

        return row;
    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return adapterList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

}