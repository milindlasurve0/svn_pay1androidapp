package com.mindsarray.pay1;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.UtilityBillTabFragmentAdapter;
import com.mindsarray.pay1.constant.Constants;

public class UtilityBillTabFragment extends FragmentActivity {

    UtilityBillTabFragmentAdapter mAdapter;
    ViewPager mPager;
    PageIndicator mIndicator;

    private static final String TAG = "Utility Bill Tab Fragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recharge_fragment_activity);

        tracker = Constants.setAnalyticsTracker(this, tracker);
        //easyTracker.set(Fields.SCREEN_NAME, TAG);
        mAdapter = new UtilityBillTabFragmentAdapter(
                getSupportFragmentManager());

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

        TabPageIndicator indicator = (TabPageIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mPager);
        //indicator.setFooterIndicatorStyle(IndicatorStyle.Underline);
        mIndicator = indicator;

        // mIndicator
        // .setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
        // @Override
        // public void onPageSelected(int position) {
        // Toast.makeText(Activity_Test.this,
        // "Changed to page " + position,
        // Toast.LENGTH_SHORT).show();
        // // Bundle arguments = new Bundle();
        // // //arguments.putString(ItemDetailFragment.ARG_ITEM_ID,
        // // id);
        // // DetailsFragment fragment = new DetailsFragment();
        // // fragment.setArguments(arguments);
        // // getSupportFragmentManager().beginTransaction()
        // // .replace(R.id.pager, fragment).commit();
        // // startActivity(new Intent(Activity_Test.this,
        // // MobileRechargeMainActivity.class));
        // // mPager.addView(new Activity_Mobile_Recharge(
        // // Activity_Test.this).createView());
        // }
        //
        // @Override
        // public void onPageScrolled(int position,
        // float positionOffset, int positionOffsetPixels) {
        // }
        //
        // @Override
        // public void onPageScrollStateChanged(int state) {
        // }
        // });
    }

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        //tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (Constants.isExpanded) {
            Constants.collapsView();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Constants.showNavigationBar(UtilityBillTabFragment.this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

}