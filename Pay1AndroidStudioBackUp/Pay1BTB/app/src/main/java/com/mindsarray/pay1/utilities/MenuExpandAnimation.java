package com.mindsarray.pay1.utilities;

import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;

public class MenuExpandAnimation extends Animation implements
        Animation.AnimationListener {
    private View view;
    private static int ANIMATION_DURATION;
    private int LastWidth;
    private int FromWidth;
    private int ToWidth;
    private static int STEP_SIZE = 30;

    public MenuExpandAnimation(View v, int FromWidth, int ToWidth, int Duration) {

        this.view = v;
        ANIMATION_DURATION = 1;
        this.FromWidth = FromWidth;
        this.ToWidth = ToWidth;
        setDuration(ANIMATION_DURATION);
        setRepeatCount(20);
        setFillAfter(false);
        setInterpolator(new AccelerateInterpolator());
        setAnimationListener(this);
    }

    @Override
    public void onAnimationStart(Animation animation) {
        // TODO Auto-generated method stub
        LayoutParams lyp = view.getLayoutParams();
        lyp.width = 0;
        view.setLayoutParams(lyp);
        LastWidth = 0;
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        // TODO Auto-generated method stub
        LayoutParams lyp = view.getLayoutParams();
        lyp.width = LastWidth += ToWidth / 20;
        view.setLayoutParams(lyp);
    }

}