package com.mindsarray.pay1;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class SettingsSmsActivity extends Activity implements
        OnCheckedChangeListener {

    Button btnUnregister;
    private SharedPreferences mSettingsData;// Shared preferences for setting
    Locale myLocale; // data
    private SharedPreferences.Editor editor;// Editor for SharedPreferences
    public static final String SETTINGS_NAME = "Pay1Settings";// name for
    // setting
    public static final String KEY_NETWORK = "Network";// Sound setting
    public static final String KEY_SMS = "Sms";// Sound setting
    CheckBox checkSMS, checkInternet;
    LinearLayout layoutSMS, layoutNet;
    public boolean bNetOrSms = false;// Sound Setting is on or not
    boolean isFirst = true;
    Button btnSaveSettings, buttonSendLogs;
    //Spinner spinnerctrl;
    private static final String TAG = "Settings";
    Context mContext = SettingsSmsActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        // WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.settingssms_activity);
        tracker = Constants.setAnalyticsTracker(this, tracker);

        //easyTracker.set(Fields.SCREEN_NAME, TAG);
        findViewById();
        mSettingsData = getSharedPreferences(SettingsSmsActivity.SETTINGS_NAME,
                0);
        bNetOrSms = mSettingsData.getBoolean(SettingsSmsActivity.KEY_NETWORK,
                true);
        //	spinnerctrl = (Spinner) findViewById(R.id.spinner1);
        editor = mSettingsData.edit();
        buttonSendLogs = (Button) findViewById(R.id.sendLogs);

        if (bNetOrSms) {
            checkInternet.setChecked(true);
            checkSMS.setChecked(false);
        } else {
            checkSMS.setChecked(true);
            checkInternet.setChecked(false);
        }

        if (Utility.getLanguageSharedPreferences(SettingsSmsActivity.this)
                .equalsIgnoreCase("hi")) {
            //spinnerctrl.setSelection(1);
        }
        if (Utility.getLanguageSharedPreferences(SettingsSmsActivity.this)
                .equalsIgnoreCase("en")) {
            //spinnerctrl.setSelection(2);
        } else {
            //	spinnerctrl.setSelection(2);
        }

//		final TextView text1 = (TextView) findViewById(R.id.text1);
        final Button changeLang = (Button) findViewById(R.id.changeLang);


        buttonSendLogs.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mSendLogs = new SendLogs();
                mSendLogs.execute();
            }
        });

        changeLang.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                final Dialog dialog = new Dialog(SettingsSmsActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                dialog.setContentView(R.layout.dialog_for_language);

                ListView listView1 = (ListView) dialog
                        .findViewById(R.id.listView1);
                final String languages[] = {"English", "Hindi"};
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                        SettingsSmsActivity.this,
                        android.R.layout.simple_list_item_1, languages);
                listView1.setAdapter(adapter);

                listView1.setOnItemClickListener(new OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1,
                                            int pos, long arg3) {
                        changeLang.setText(languages[pos]);
                        dialog.dismiss();
                        changeLanguageDailog(pos);
                    }

                });

                dialog.show();

            }
        });

        btnSaveSettings.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });
        checkInternet.setOnCheckedChangeListener(this);

        checkSMS.setOnCheckedChangeListener(this);

    }

    private void changeLanguageDailog(final int pos) {
        try {
            final Dialog dialog = new Dialog(SettingsSmsActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialog.setContentView(R.layout.dialog_success_two_button);

            TextView textView_Title = (TextView) dialog
                    .findViewById(R.id.textView_Title);
            textView_Title.setText("Pay1");

            TextView textView_Message = (TextView) dialog
                    .findViewById(R.id.textView_Message);
            textView_Message
                    .setText("App will be restarted.");

            ImageView closeButton = (ImageView) dialog
                    .findViewById(R.id.imageView_Close);

            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            Button button_Ok = (Button) dialog.findViewById(R.id.button_Ok);

            button_Ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (pos == 1) {
                        Utility.setLanguageSharedPreference(
                                SettingsSmsActivity.this, "hi");
                        setLocale("hi");

                    } else if (pos == 0) {

                        Utility.setLanguageSharedPreference(
                                SettingsSmsActivity.this, "en");
                        setLocale("en");
                    }

                    //Intent intent = new Intent(Intent.ACTION_MAIN);
                    Intent intent = new Intent(SettingsSmsActivity.this, SplashScreenActivity.class);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });

            Button button_Cancel = (Button) dialog
                    .findViewById(R.id.button_Cancel);
            // if button is clicked, close the custom dialog
            button_Cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        } catch (Exception ee) {

        }

    }


    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onStart() {
        super.onStart();
        // Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
        // EasyTracker.getInstance(this).activityStart(this);
        //tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
    }

    private void findViewById() {
        // TODO Auto-generated method stub

        checkInternet = (CheckBox) findViewById(R.id.checkInternet);
        checkSMS = (CheckBox) findViewById(R.id.checkSMS);
        layoutNet = (LinearLayout) findViewById(R.id.layoutNet);
        layoutSMS = (LinearLayout) findViewById(R.id.layoutSMS);
        btnSaveSettings = (Button) findViewById(R.id.btnSettings);
    }

    public void setLocale(String lang) {

        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, SettingsSmsActivity.class);
        startActivity(refresh);
        finish();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Constants.showNavigationBar(SettingsSmsActivity.this);
        super.onResume();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if (buttonView.getId() == R.id.checkInternet) {
            if (isChecked) {
                editor.putBoolean(KEY_NETWORK, true);
                editor.commit();
                checkInternet.setChecked(true);
                checkSMS.setChecked(false);
            } else {
                editor.putBoolean(KEY_NETWORK, false);
                editor.commit();
                checkInternet.setChecked(false);
                checkSMS.setChecked(true);
            }
        } else if (buttonView.getId() == R.id.checkSMS) {
            if (isChecked) {
                editor.putBoolean(KEY_NETWORK, false);
                editor.commit();
                checkInternet.setChecked(false);
                checkSMS.setChecked(true);
            } else {
                editor.putBoolean(KEY_NETWORK, true);
                editor.commit();
                checkInternet.setChecked(true);
                checkSMS.setChecked(false);
            }
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (Constants.isExpanded) {
            Constants.collapsView();
        } else {
            super.onBackPressed();
        }
    }

    SendLogs mSendLogs;

    public class SendLogs extends AsyncTask<String, String, String>
            implements OnDismissListener {
        private ProgressDialog dialog;

        @Override
        protected String doInBackground(String... params) {
            ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
            listValuePair = Constants.getLogs(mContext);

            String response = RequestClass.getInstance()
                    .readPay1Request(mContext,
                            Constants.CC_API_URL, listValuePair);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("post execute", result);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            try {
                if (!result.startsWith("Error")) {
//				result = result.substring(result.indexOf("([{"), result.lastIndexOf("}])") + 3);
                    String replaced = result.replace("(", "").replace(")", "")
                            .replace(";", "");

                    JSONArray jsonArray = new JSONArray(replaced);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        Constants.showOneButtonFailureDialog(mContext,
                                "Sent", TAG,
                                Constants.OTHER_ERROR);
                    } else {
                        Constants.showOneButtonFailureDialog(mContext,
                                Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                                Constants.OTHER_ERROR);
                    }

                } else {
                    Constants.showOneButtonFailureDialog(mContext,
                            Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                            Constants.OTHER_ERROR);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Constants.showOneButtonFailureDialog(mContext,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            } catch (Exception e) {
                e.printStackTrace();
                Constants.showOneButtonFailureDialog(mContext,
                        Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
                        Constants.OTHER_ERROR);
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(mContext);
            this.dialog.setMessage("Please wait.....");
            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            SendLogs.this.cancel(true);
                        }
                    });
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            SendLogs.this.cancel(true);
            dialog.cancel();
        }

    }
}
