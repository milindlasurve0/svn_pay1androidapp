package com.mindsarray.pay1;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.NotificationNewAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.NotificationDataSource;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

public class NotificationActivity extends Activity {
	ListView notificationListView;
	NotificationNewAdapter adapter;
	ArrayList<HashMap<String, String>> notificationsList;
	NotificationDataSource notificationDataSource;
	int PAGE_RECORDS = 10;
	int PAGE_NUMBER = 1;
	Button btnFooter;
	String description = "", title = "", type = "";
	Bundle bundle;
	TextView textBanner;
	ImageButton imageBannerClose;
	LinearLayout bannerLayout;
	private static final String TAG = "Notification Activity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.notification_activity);

		// tracker = Constants.setAnalyticsTracker(this, tracker);
		// ;
		// //easyTracker.set(Fields.SCREEN_NAME, TAG);

		try {

			tracker = Constants.setAnalyticsTracker(this, tracker);
			notificationDataSource = new NotificationDataSource(
					NotificationActivity.this);
			if (Constants.isOnline(NotificationActivity.this)) {
				int NotiCount = Utility.getNotificationCount(
						NotificationActivity.this, "NotiCount");
				if (NotiCount >= 1) {
					new CheckBalanceTask().execute();
				}
			}
			NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			notificationManager.cancelAll();
			imageBannerClose = (ImageButton) findViewById(R.id.imageBannerClose);
			bannerLayout = (LinearLayout) findViewById(R.id.bannerLayout);
			bundle = getIntent().getExtras();
			if (bundle != null) {
				String banner = bundle.getString("pushBundle");
				try {
					if (type.equalsIgnoreCase("banner")) {
						bannerLayout.setVisibility(View.VISIBLE);

						JSONObject jsonObject = new JSONObject(banner);
						// JSONObject jsonObject2 =
						// jsonObject.getJSONObject("msg");
						try {
							description = jsonObject.getString("msg");
							title = jsonObject.getString("title");
							type = jsonObject.getString("type");
						} catch (Exception e) {
							e.printStackTrace();
						}
						if (type.equalsIgnoreCase("banner")) {
							textBanner = (TextView) findViewById(R.id.textBanner);
							textBanner.setText(description);
							imageBannerClose
									.setOnClickListener(new OnClickListener() {

										@Override
										public void onClick(View v) {
											// TODO Auto-generated method stub
											bannerLayout
													.setVisibility(View.GONE);
										}
									});
						}
					} else {

					}
				} catch (Exception e) {
					// Log.e("Check", "Check");
				}
			}
			Utility.setNotificationCount(NotificationActivity.this,
					"NotiCount", 0);
			Constants.COUNT_NOTIFY=0;
			try {
				notificationDataSource.open();
				// for (int i = 0; i < 20; i++)
				// notificationDataSource.createNotification(
				// "123456897987wdasdasdasdasd asd46asd4as4d6 ", 1,
				// System.currentTimeMillis() + "");
				notificationsList = notificationDataSource.getAllNotifications(
						PAGE_NUMBER++, 10);

				notificationListView = (ListView) findViewById(R.id.notificationListView);
				notificationListView.setClickable(false);
				// add the footer before adding the adapter, else the footer
				// will not
				// load!
				adapter = new NotificationNewAdapter(this,
						R.layout.notification_adapter, notificationsList);

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				notificationDataSource.close();
			}

			btnFooter = new Button(NotificationActivity.this);
			btnFooter.setHeight(50);
			btnFooter.setText("Load more streams");
			btnFooter.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					try {
						notificationDataSource.open();
						notificationsList.addAll(notificationDataSource
								.getAllNotifications(PAGE_NUMBER++,
										PAGE_RECORDS));

					} catch (SQLException exception) {
						// TODO: handle exception
						// // Log.e("Er ", exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// // Log.e("Er ", e.getMessage());
					} finally {
						notificationDataSource.close();
					}

					adapter.notifyDataSetChanged();

				}
			});
			if (notificationsList == null) {
				btnFooter.setVisibility(View.GONE);
			} else if (notificationsList.size() % 10 == 0) {
				// btnFooter.setVisibility(View.GONE);
				if (notificationsList.size() == 0) {
					btnFooter.setVisibility(View.GONE);
				} else {
					btnFooter.setVisibility(View.VISIBLE);
				}
			} else if (notificationsList.size() % 10 != 0) {

				btnFooter.setVisibility(View.GONE);
			}

			notificationListView.addFooterView(btnFooter);

			notificationListView.setAdapter(adapter);

			Utility.setSharedPreference(NotificationActivity.this,
					Constants.SHAREDPREFERENCE_LAST_NOTIFICATION, "");

		} catch (Exception exc) {

		}
	}

	public static GoogleAnalytics analytics;
	public static Tracker tracker;

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
		// EasyTracker.getInstance(this).activityStart(this);
		// tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
	}

	// private void showDialogAtTop(String msg) {
	// // TODO Auto-generated method stub
	//
	// try {
	//
	// Dialog dialog = new Dialog(NotificationActivity.this);
	// dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	// dialog.setContentView(R.layout.my_banner);
	// TextView textView_Message = (TextView) dialog
	// .findViewById(R.id.text_banner);
	// textView_Message.setText(msg);
	// WindowManager.LayoutParams wmlp = dialog.getWindow()
	// .getAttributes();
	// wmlp.gravity = Gravity.TOP | Gravity.LEFT;
	// wmlp.x = 0; // x position
	// wmlp.y = 0; // y position
	// dialog.getWindow().setAttributes(wmlp);
	// dialog.show();
	// } catch (Exception e) {
	// // Log.e("Check", "Check");
	// }
	// }
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants.showNavigationBar(NotificationActivity.this);
		super.onResume();
	}

	private class CheckBalanceTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair.add(new BasicNameValuePair("method", "updateBal"));
			String response = RequestClass.getInstance()
					.readPay1Request(NotificationActivity.this,
							Constants.API_URL, listValuePair);

			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = array.getJSONObject(0);
					String status = jsonObject.getString("status");
					int login = jsonObject.getInt("login");
					if (status.equalsIgnoreCase("success") && login == 1) {
						String desc = jsonObject.getString("description");
						String balance = desc;
						Utility.setBalance(NotificationActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE, balance);
					}
				}
			} catch (JSONException exception) {

			} catch (Exception e) {
			}
		}
	}
}
