package com.mindsarray.pay1;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

public class RetailerVerificationActivity extends Activity {
	
	private String TAG = "Retailer Verification";
	private Context mContext = RetailerVerificationActivity.this;
	
	EditText otpEditText;
	EditText pinEditText;
	EditText confirmPinEditText;
	
	String otp;
	String pin;
	String confirm_pin;
	
	VerifyRetailerLead mVerifyRetailerLead;
	String OTA_Fee;
	TextView OTA_text;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.retailer_verification_activity);
		
		Utility.setSharedPreference(mContext, "Retailer_Pin", "");
		OTA_Fee = Utility.getSharedPreferences(mContext, "OTA_Fee");
		otpEditText = (EditText) findViewById(R.id.otp);
		pinEditText = (EditText) findViewById(R.id.pin);
		confirmPinEditText = (EditText) findViewById(R.id.confirm_pin);
		OTA_text = (TextView) findViewById(R.id.OTA_text);
		//OTA_text.setText("*Kindly note, Rs. " + OTA_Fee + " will be charged as one time activation fee for the first time load");
		OTA_text.setText("");
		final Button continueButton = (Button) findViewById(R.id.verification_continue);
		continueButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(validateForm()){
//					startActivity(new Intent(mContext, CongratulateRetailerActivity.class));
					Utility.setSharedPreference(mContext, "Retailer_Pin", pin);
					mVerifyRetailerLead = new VerifyRetailerLead();
					mVerifyRetailerLead.execute();
				}	
			}
		});
		
		confirmPinEditText.setOnEditorActionListener(new OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				continueButton.requestFocus();
				return false;
			}
		});
	}
	
	boolean validateForm(){
		otp = otpEditText.getText().toString();
		pin = pinEditText.getText().toString();
		confirm_pin = confirmPinEditText.getText().toString();
		
		String estring;
		int ecolor = R.color.black; 
		
		if(otp.length() < 6){
			estring = "Enter 6 digits";
			ForegroundColorSpan fgcspan1 = new ForegroundColorSpan(ecolor);
	        SpannableStringBuilder ssbuilder1 = new SpannableStringBuilder(estring);
	        ssbuilder1.setSpan(fgcspan1, 0, estring.length(), 0);
	        otpEditText.setError(ssbuilder1);
			return false;
		}
		if(pin.length() > 10){
			estring = "Enter up to 10 digits only";
			ForegroundColorSpan fgcspan2 = new ForegroundColorSpan(ecolor);
	        SpannableStringBuilder ssbuilder2 = new SpannableStringBuilder(estring);
	        ssbuilder2.setSpan(fgcspan2, 0, estring.length(), 0);
	        pinEditText.setError(ssbuilder2);
			return false;
		}
		if(!pin.equals(confirm_pin)){
			estring = "PINs do not match";
			ForegroundColorSpan fgcspan3 = new ForegroundColorSpan(ecolor);
	        SpannableStringBuilder ssbuilder3 = new SpannableStringBuilder(estring);
	        ssbuilder3.setSpan(fgcspan3, 0, estring.length(), 0);
	        confirmPinEditText.setError(ssbuilder3);
			return false;
		}
		return true;
	}
	
	public class VerifyRetailerLead extends AsyncTask<String, String, String>
													implements OnDismissListener {
		private ProgressDialog dialog;
		
		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
			listValuePair.add(new BasicNameValuePair("method", "verifyRetailerLead"));
			listValuePair.add(new BasicNameValuePair("r_m", Utility.getSharedPreferences(mContext, "Retailer_Mobile")));
			listValuePair.add(new BasicNameValuePair("otp", otp));
			listValuePair.add(new BasicNameValuePair("pin", pin));
			listValuePair.add(new BasicNameValuePair("req_by", "android"));
			listValuePair.add(new BasicNameValuePair("r_u_d", Utility.getSharedPreferences(mContext, "Retailer_Under_Distributor")));
			String response = RequestClass.getInstance()
								.readPay1Request(mContext,
								Constants.API_URL, listValuePair);
			listValuePair.clear();
			return response;
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
		
					JSONArray jsonArray = new JSONArray(replaced);
					JSONObject jsonObject = jsonArray.getJSONObject(0);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						Utility.setIsAppUsed(RetailerVerificationActivity.this, true);
						startActivity(new Intent(mContext, CongratulateRetailerActivity.class));
					} else {
						Constants.showOneButtonFailureDialog(
								mContext,
								Constants.checkCode(replaced), TAG,
								Constants.OTHER_ERROR);
					}
		
				} else {
					Constants.showOneButtonFailureDialog(
							mContext,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);
		
				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(mContext,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(mContext,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}
		
		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(mContext);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog.setCanceledOnTouchOutside(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {
		
						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							VerifyRetailerLead.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}
		
		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			VerifyRetailerLead.this.cancel(true);
			dialog.cancel();
		}
			
	}
}	