package com.mindsarray.pay1.databasehandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import android.content.Context;
import android.util.Log;

public class Pay1SQLiteHelper extends SQLiteAssetHelper{ //SQLiteOpenHelper {

	public static final String DATABASE_NAME = "pay1database.db";
	private static final int DATABASE_VERSION = 1;
	public static final String SYNC_DATABASE = "syncdatabase.db";

	public static final String TABLE_ENTERTAINMENT = "EntertainmentTable";
	public static final String TABLE_MOST_VISITED_OPERATOR = "recentlyVisited";

	public static final String ENTERTAINMENT_ID = "e_id";
	public static final String ENTERTAINMENT_PRODUCT_ID = "product_id";
	public static final String ENTERTAINMENT_PRODUCT_CODE = "product_code";
	public static final String ENTERTAINMENT_PRODUCT_NAME = "name";
	public static final String ENTERTAINMENT_PRODUCT_PRICE = "price";
	public static final String ENTERTAINMENT_PRODUCT_VALIDITY = "validity";
	public static final String ENTERTAINMENT_PRODUCT_SHORT_DESCRIPTION = "shortDesc";
	public static final String ENTERTAINMENT_PRODUCT_LONG_DESCRIPTION = "longDesc";
	public static final String ENTERTAINMENT_PRODUCT_IMAGE = "image";
	public static final String ENTERTAINMENT_PRODUCT_DATA_FIELD = "field";
	public static final String ENTERTAINMENT_PRODUCT_DATA_TYPE = "type";
	public static final String ENTERTAINMENT_PRODUCT_DATA_LENGTH = "length";
	public static final String ENTERTAINMENT_PRODUCT_UPDATE_TIME = "time";

	public static final String TABLE_NOTIFICATION = "NotificationTable";

	public static final String NOTIFICATION_ID = "n_id";
	public static final String NOTIFICATION_DATA = "data";
	public static final String NOTIFICATION_TYPE = "type";
	public static final String NOTIFICATION_TIME = "time";

	public static final String TABLE_PLAN = "PlanTable";

	public static final String PLAN_ID = "p_id";
	public static final String PLAN_OPERATOR_ID = "operator_id";
	public static final String PLAN_OPERATOR_NAME = "operator_name";
	public static final String PLAN_CIRCLE_ID = "circle_id";
	public static final String PLAN_CIRCLE_NAME = "circle_name";
	public static final String PLAN_TYPE = "plan_type";
	public static final String PLAN_AMOUNT = "amount";
	public static final String PLAN_VALIDITY = "validity";
	public static final String PLAN_DESCRIPTION = "description";
	public static final String PLAN_UPDATE_TIME = "time";
	public static final String PLAN_SHOW_FLAG = "show_flag";

	public static final String TABLE_VMN = "VMNTable";

	public static final String VMN_ID = "v_id";
	public static final String VMN_NUMBER = "vmn_number";

	public static final String OPERATOR_ID = "op_id";
	public static final String OPERATOR_NAME = "op_name";
	public static final String OPERATOR_CODE = "op_code";
	public static final String RECHARGE_TYPE = "rechargeType";
	public static final String RECHARGE_COUNT = "rechargeCount";

	public static final String TABLE_ADDRESS = "AddressTable";

	public static final String ADDRESS_ID = "address_id";
	public static final String ADDRESS_LINE = "address_line";
	public static final String ADDRESS_AREA = "address_area";
	public static final String ADDRESS_CITY = "address_city";
	public static final String ADDRESS_STATE = "address_state";
	public static final String ADDRESS_ZIP = "address_zip";
	public static final String ADDRESS_LATITUDE = "address_latitude";
	public static final String ADDRESS_LONGITUDE = "address_longitude";
	
	public static final String TABLE_NUMBERANDCIRCLE = "NumberAndCircleTable";

	public static final String NAC_AREA_ID = "nac_area_id";
	public static final String NAC_AREA_NAME = "nac_area_name";
	public static final String NAC_AREA_CODE = "nac_area_code";
	public static final String NAC_OPERATOR_CODE = "nac_operator_code";
	public static final String NAC_OPERATOR_NAME = "nac_operator_name";
	public static final String NAC_OPERATOR_ID = "nac_operator_id";
	public static final String NAC_MOBILE_NUMBER = "nac_mobile_number";
	public static final String NAC_UPDATE_TIME = "nac_update_time";
	

	public static final String TABLE_CHATHISTORY = "ChatHistoryTable";

	public static final String CHATHISTORY_ID = "history_id";
	public static final String CHATHISTORY_TEXT = "history_text";
	public static final String CHATHISTORY_TIME = "history_time";
	public static final String CHATHISTORY_POSITION = "history_position";

	/*private final String CREATE_ENTERTAINMENT = "CREATE TABLE "
			+ TABLE_ENTERTAINMENT + " ( " + ENTERTAINMENT_ID
			+ " integer primary key autoincrement, " + ENTERTAINMENT_PRODUCT_ID
			+ " text, " + ENTERTAINMENT_PRODUCT_CODE + " text, "
			+ ENTERTAINMENT_PRODUCT_NAME + " text, "
			+ ENTERTAINMENT_PRODUCT_PRICE + " text, "
			+ ENTERTAINMENT_PRODUCT_VALIDITY + " text, "
			+ ENTERTAINMENT_PRODUCT_SHORT_DESCRIPTION + " text, "
			+ ENTERTAINMENT_PRODUCT_LONG_DESCRIPTION + " text,"
			+ ENTERTAINMENT_PRODUCT_IMAGE + " text, "
			+ ENTERTAINMENT_PRODUCT_DATA_FIELD + " text, "
			+ ENTERTAINMENT_PRODUCT_DATA_TYPE + " text, "
			+ ENTERTAINMENT_PRODUCT_DATA_LENGTH + " text, "
			+ ENTERTAINMENT_PRODUCT_UPDATE_TIME + " text );";

	private final String CREATE_NOTIFICATION = "CREATE TABLE "
			+ TABLE_NOTIFICATION + " ( " + NOTIFICATION_ID
			+ " integer primary key autoincrement, " + NOTIFICATION_DATA
			+ " text, " + NOTIFICATION_TYPE + " integer, " + NOTIFICATION_TIME
			+ " text );";

	private final String CREATE_PLAN = "CREATE TABLE " + TABLE_PLAN + " ( "
			+ PLAN_ID + " integer primary key autoincrement, "
			+ PLAN_OPERATOR_ID + " text, " + PLAN_OPERATOR_NAME + " text, "
			+ PLAN_CIRCLE_ID + " text, " + PLAN_CIRCLE_NAME + " text, "
			+ PLAN_TYPE + " text, " + PLAN_AMOUNT + " integer, "
			+ PLAN_VALIDITY + " text, " + PLAN_DESCRIPTION + " text, "
			+ PLAN_UPDATE_TIME + " text );";
	
	private final String CREATE_PLAN = "CREATE TABLE " + TABLE_PLAN + " ( "
			+ PLAN_ID + " integer primary key autoincrement, "
			+ PLAN_OPERATOR_ID + " text, " + PLAN_OPERATOR_NAME + " text, "
			+ PLAN_CIRCLE_ID + " text, " + PLAN_CIRCLE_NAME + " text, "
			+ PLAN_TYPE + " text, " + PLAN_AMOUNT + " integer, "
			+ PLAN_VALIDITY + " text, " + PLAN_DESCRIPTION + " text, "
			+ PLAN_UPDATE_TIME + " text,"+PLAN_SHOW_FLAG+" text );";

	private final String CREATE_VMN = "CREATE TABLE " + TABLE_VMN + " ( "
			+ VMN_ID + " integer primary key, " + VMN_NUMBER + " text );";

	private final String MOST_VISITED = "CREATE TABLE "
			+ TABLE_MOST_VISITED_OPERATOR + " ( " + OPERATOR_ID
			+ " integer primary key, " + OPERATOR_NAME + " text, "
			+ OPERATOR_CODE + " text, " + RECHARGE_TYPE + " text, "
			+ RECHARGE_COUNT + " integer );";

	private final String CREATE_ADDRESS = "CREATE TABLE " + TABLE_ADDRESS
			+ " ( " + ADDRESS_ID + " integer primary key autoincrement, "
			+ ADDRESS_LINE + " text, " + ADDRESS_AREA + " text, "
			+ ADDRESS_CITY + " text, " + ADDRESS_STATE + " text, "
			+ ADDRESS_ZIP + " text, " + ADDRESS_LATITUDE + " text, "
			+ ADDRESS_LONGITUDE + " text);";
	
	private final String CREATE_NUMBER_CIRCLE = "CREATE TABLE " + TABLE_NUMBERANDCIRCLE
			+ " ( " + NAC_AREA_ID + " integer primary key autoincrement, "
			+ NAC_AREA_CODE + " text, " + NAC_AREA_NAME + " text, "
			+ NAC_OPERATOR_CODE + " text, " + NAC_OPERATOR_NAME + " text, "
			+ NAC_OPERATOR_ID + " text, " + NAC_MOBILE_NUMBER + " text,"
			+ NAC_UPDATE_TIME + " text "
			+ ");";

	private final String CREATE_CHATHISTORY = "CREATE TABLE "
			+ TABLE_CHATHISTORY + " ( " + CHATHISTORY_ID
			+ " integer primary key autoincrement, " + CHATHISTORY_TEXT
			+ " text, " + CHATHISTORY_TIME + " text, " + CHATHISTORY_POSITION
			+ " text );";*/
	
	private Context dbContext;
//	public static String dbPath = "/mnt/sdcard/Download";
	
	public Pay1SQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		 dbContext = context;
	} 
	
	public Pay1SQLiteHelper(Context context, String databaseName) {
		super(context, databaseName, null, DATABASE_VERSION);
		 dbContext = context;
	} 
	
	public boolean cloneDatabase(String inputDBName, String outputDBName) {
		try{
			File dbFile = dbContext.getDatabasePath(inputDBName);//new File(dbPath + "/" + inputDBName);
			InputStream input = new FileInputStream(dbFile);
			
		    OutputStream output = new FileOutputStream(dbFile.getAbsoluteFile().getParentFile().getAbsolutePath() + "/" + outputDBName);
	
		    byte[] buffer = new byte[1024];
		    int length;
		    while ((length = input.read(buffer)) > 0)
		    {
		        output.write(buffer, 0, length);
		    }
		    
		    output.flush();
		    output.close();
		    input.close();
		    Log.e("cloneDatabase", inputDBName + " cloned to " + outputDBName);
		    return true;
		}
		catch(IOException e){
			e.printStackTrace();
			return false;
		} 
	}

	
//	@Override
//	public void onCreate(SQLiteDatabase db) {
//		// TODO Auto-generated method stub
//		db.execSQL(CREATE_ENTERTAINMENT);
//		db.execSQL(CREATE_NOTIFICATION);
//		db.execSQL(CREATE_PLAN);
//		db.execSQL(CREATE_VMN);
//		db.execSQL(CREATE_ADDRESS);
//		db.execSQL(CREATE_CHATHISTORY);
//		db.execSQL(MOST_VISITED);
//		db.execSQL(CREATE_NUMBER_CIRCLE);
//	}
//
//	@Override
//	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//		// TODO Auto-generated method stub
//		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENTERTAINMENT);
//		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFICATION);
//		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAN);
//		db.execSQL("DROP TABLE IF EXISTS " + TABLE_VMN);
//		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDRESS);
//		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHATHISTORY);
//		db.execSQL("DROP TABLE IF EXISTS " + MOST_VISITED);
//		onCreate(db);
//	}

}
