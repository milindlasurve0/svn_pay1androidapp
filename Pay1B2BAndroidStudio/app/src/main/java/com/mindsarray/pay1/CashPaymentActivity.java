package com.mindsarray.pay1;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.EditTextValidator;

public class CashPaymentActivity extends Activity {
	EditText editMobileNumber;
	String mobileNumber;
	private static final String TAG = "C2D Payment";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cash_payment_activity);

		editMobileNumber = (EditText) findViewById(R.id.editMobileNumber);
		Button btnCashPayment = (Button) findViewById(R.id.btnCashPayment);
		btnCashPayment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*if (editMobileNumber.getText().length() == 0) {
					editMobileNumber.setError("Enter correct number");
				} else {

					if (editMobileNumber.getText().length() == 10) {
						if (editMobileNumber.getText().toString()
								.startsWith("7")
								|| editMobileNumber.getText().toString()
										.startsWith("8")
								|| editMobileNumber.getText().toString()
										.startsWith("9")) {
							// mobileNumber = "9654713658";
							mobileNumber = editMobileNumber.getText()
									.toString();
							new GetCashPaymentList().execute();
						}else{
							editMobileNumber.setError("Enter correct number");
						}
					} else {
						editMobileNumber.setError("Enter correct number");

					}
				}*/
				
				if (!checkValidation(CashPaymentActivity.this)) {
					// Constants.showCustomToast(LoginActivity.this,
					// "Please Enter a valid Number");
				} else {
					mobileNumber = editMobileNumber.getText()
							.toString();
					new GetCashPaymentList().execute();
					
				}
			}
		});
	}

	
	private boolean checkValidation(Context context) {
		boolean ret = true;

		if (!EditTextValidator.hasText(context, editMobileNumber,
				Constants.ERROR_MOBILE_BLANK_FIELD)) {
			ret = false;
			editMobileNumber.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidMobileNumber(context,
				editMobileNumber, Constants.ERROR_MOBILE_LENGTH_FIELD)) {
			ret = false;
			editMobileNumber.requestFocus();
			return ret;
		} else if(!EditTextValidator.isNumberValid(context,
				editMobileNumber, "Number should starts with 7,8,9")) {
			ret = false;
			editMobileNumber.requestFocus();
			return ret;
		} 
		// else if (!EditTextValidator.isValidPin(context, editPin,
		// Constants.ERROR_LOGIN_PIN_VALID_FIELD)) {
		// ret = false;
		// editPin.requestFocus();
		// return ret;
		// }
		else
			return ret;
	}
	
	
	public class GetCashPaymentList extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair
					.add(new BasicNameValuePair("method", "cashpgTxnList"));
			listValuePair.add(new BasicNameValuePair("mobileNumber",
					mobileNumber));

			listValuePair.add(new BasicNameValuePair("action", "get_txn_list"));
			/*
			 * listValuePair.add(new BasicNameValuePair("device_type",
			 * "android")); listValuePair.add(new
			 * BasicNameValuePair("device_type", "android"));
			 */
			listValuePair.add(new BasicNameValuePair("device_type", "android"));
			String response = RequestClass.getInstance().readPay1Request(
					CashPaymentActivity.this, Constants.API_URL, listValuePair);

			listValuePair.clear();
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			super.onPostExecute(result);
			try {
				if (!result.isEmpty()) {
					String replaced = "";
					if (!result.startsWith("Error")) {
						replaced = result.replace("(", "").replace(")", "")
								.replace(";", "");

						JSONArray array = new JSONArray(replaced);
						JSONObject jsonObject = array.getJSONObject(0);
						if (jsonObject.getString("status").equalsIgnoreCase(
								"success")) {
							JSONArray jsonArray = jsonObject
									.getJSONArray("description");
							if (jsonArray.length() != 0) {
								Intent intent = new Intent(
										CashPaymentActivity.this,
										CashPaymentListActivity.class);
								intent.putExtra("cashList",
										jsonArray.toString());
								intent.putExtra("mobileNumber", mobileNumber);
								startActivity(intent);
							} else {
								Constants.showOneButtonFailureDialog(
										CashPaymentActivity.this,
										"No Record Found", TAG,
										Constants.OTHER_ERROR);
							}

						} else {
							Constants.showOneButtonFailureDialog(
									CashPaymentActivity.this,
									Constants.checkCode(replaced), TAG,
									Constants.OTHER_ERROR);
						}

					} else {
						Constants.showOneButtonFailureDialog(
								CashPaymentActivity.this,
								result, TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(
							CashPaymentActivity.this, Constants.ERROR_INTERNET,
							TAG, Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(CashPaymentActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(CashPaymentActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(CashPaymentActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							GetCashPaymentList.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			GetCashPaymentList.this.cancel(true);
			dialog.cancel();
		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants.showNavigationBar(CashPaymentActivity.this);

		super.onResume();
	}

}
