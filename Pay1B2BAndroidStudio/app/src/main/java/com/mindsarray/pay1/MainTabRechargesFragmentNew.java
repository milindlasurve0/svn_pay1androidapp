package com.mindsarray.pay1;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.RechargeMainAdapter;
import com.mindsarray.pay1.adapterhandler.StatusAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.VMNDataSource;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.GridMenuItem;
import com.mindsarray.pay1.utilities.Utility;

public class MainTabRechargesFragmentNew extends Fragment {

	private String title;
	private int page;
	private Context context;

	GridView gridView;
	Location mCurrentLocation;
	ArrayList<GridMenuItem> gridArray = new ArrayList<GridMenuItem>();
	RechargeMainAdapter customGridAdapter;
	SQLiteDatabase db;
	VMNDataSource vmnDataSource;
	LinearLayout adLayout;
	private static final String TAG = "Pay1";
	ImageView bannerClose;
	ArrayList<String> statusList;
	StatusAdapter adapter;
	RelativeLayout mainNotification;
	TextView notiText, unresolved_complaint, resolved_complaint;
	// Intent broadCastIntent;
	TextView textNotiCount;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public void setMenuVisibility(final boolean visible) {
		super.setMenuVisibility(visible);
		if (visible) {
			// ...
			MainActivity.currentItemIndex = 0;
			// Utility.setC2DNotificationCount(getActivity(), 0);
		}
	}

	// Inflate the view for the fragment based on layout XML
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.main_tab_recharges_fragment,
				container, false);
		context = getActivity();

		textNotiCount = (TextView) view.findViewById(R.id.textNotiCount);

		// broadCastIntent = new Intent(this, BroadcastService.class);

		try {
			adLayout = (LinearLayout) view.findViewById(R.id.adLayout);
			String notificationText = Utility.getSharedPreferences(context,
					"notification");

			if (notificationText.equals("") || notificationText.length() == 0) {
				adLayout.setVisibility(View.GONE);
			} else {
				adLayout.setVisibility(View.VISIBLE);
				String html = "<html><body height:50px  bgcolor='#26a9e0'><marquee><div style='color:#1b1b1b; padding: 10px'>"
						+ notificationText + "</div></marquee></body></html>";

				WebView myWebView = (WebView) view.findViewById(R.id.myWebView);
				myWebView.getSettings().setJavaScriptEnabled(true);

				myWebView.loadData(html, "text/html", null);
			}
		} catch (Exception ee) {

		}
		/*
		 *
		 * RechargeDBHelper dbHelper=new RechargeDBHelper(context); Recharges
		 * recharges=new Recharges(); recharges.set_amount("30");
		 *
		 * recharges.set_phone_number("8422057400");
		 * dbHelper.addRecharges(recharges);
		 * dbHelper.getRechargeStatus("8422057400", "30");
		 */
		// tracker = Constants.setAnalyticsTracker(this, tracker);
		// //easyTracker.set(Fields.SCREEN_NAME, TAG);
	//	tracker = Constants.setAnalyticsTracker(context, tracker);

		/*
		 * AnalyticsApplication application = (AnalyticsApplication)
		 * getActivity().getApplication(); tracker =
		 * application.getDefaultTracker();
		 */

		Constants.isExpanded = false;
		if (Utility.getMiliSecForNewLable(context,
				Constants.SHAREDPREFERENCE_MS) == 0)
			Utility.setMiliSecForNewLable(context,
					Constants.SHAREDPREFERENCE_MS, System.currentTimeMillis());
		vmnDataSource = new VMNDataSource(context);
		try {
			vmnDataSource.open();
			if (vmnDataSource.getAllVMN().size() == 0) {
				vmnDataSource.createVMN(1, "9223178889");
				vmnDataSource.createVMN(2, "9821232431");
			}
		} catch (SQLException exception) {
			// TODO: handle exception
			// // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			// // Log.e("Er ", e.getMessage());
		} finally {
			vmnDataSource.close();
		}

		new CheckUpdateTask().execute();

		int mMobileIcon = R.drawable.mobile_recharge;
		int mDthIcon = R.drawable.dth_recharge;
		int mItilityIcon = R.drawable.utility_bill;
		int mBillPaymentIcon = R.drawable.bill_payment;
		int mQuickIcon = R.drawable.quick_recharge;
		int mComplaintIcon = R.drawable.reversal;
		// Bitmap mSettingsIcon = BitmapFactory.decodeResource(
		// this.getResources(), R.drawable.account);
		int mReportsIcon = R.drawable.reports;
		int mChatIcon = R.drawable.chat_support;
		int mWalletIcon = R.drawable.mw_icon;
		int debitcard = R.drawable.debitcard;
		int mPos = R.drawable.ic_mpos;
		int cashPayment = R.drawable.ic_onlinepayment;
		int kycPending = R.drawable.ic_kyc_pending;
		// Bitmap mBusIcon =
		// R.drawable.bus_booking);

		gridArray.add(new GridMenuItem(mMobileIcon, getResources().getString(
				R.string.mobile_recharge), 1, 1));
		gridArray.add(new GridMenuItem(mDthIcon, getResources().getString(
				R.string.dth), 2, 1));
		gridArray.add(new GridMenuItem(mBillPaymentIcon, getResources()
				.getString(R.string.postpaid_mobile), 3, 1));
		gridArray.add(new GridMenuItem(mItilityIcon, getResources().getString(
				R.string.utility_bill), 4, 1));
		gridArray.add(new GridMenuItem(mWalletIcon, getResources().getString(
				R.string.wallet_topup), 5, 1));
		// gridArray.add(new GridMenuItem(mMoneyTransferIcon, "Data Card", 5,
		// 1));
		gridArray.add(new GridMenuItem(mChatIcon, getResources().getString(
				R.string.chat_support), 6, 1));
		// gridArray.add(new GridMenuItem(mSettingsIcon, "Settings", 7, 1));

		gridArray.add(new GridMenuItem(mQuickIcon, getResources().getString(
				R.string.quick_recharge), 7, 1));
		gridArray.add(new GridMenuItem(mComplaintIcon, getResources()
				.getString(R.string.complaint), 8, 1));
		gridArray.add(new GridMenuItem(mReportsIcon, getResources().getString(
				R.string.reports), 9, 1));
		// gridArray.add(new GridMenuItem(1, "", 29, 1));

		gridArray.add(new GridMenuItem(cashPayment, getResources().getString(
				R.string.cash_payment), 11, 1));
		String ezeTapDeviceSerialNumber = Utility.getSharedPreferences(context,
				Constants.SHAREDPREFERENCE_DEVICE_SERIAL_NO);
		if (ezeTapDeviceSerialNumber.isEmpty()) {

		} else {
			gridArray.add(new GridMenuItem(mPos, getResources().getString(
					R.string.mini_atm), 12, 1));

		}

		gridArray.add(new GridMenuItem(debitcard, getResources().getString(
				R.string.online_balance), 10, 1));

		String trial_flag = Utility.getSharedPreferences(context,
				Constants.SHAREDPREFERENCE_TRIAL_FLAG);

		/*
		 * if(!trial_flag.equals("0")) gridArray.add(new
		 * GridMenuItem(kycPending,
		 * getResources().getString(R.string.kyc_pending), 12, 1));
		 */
		// gridArray.add(new GridMenuItem(mBusIcon, "Bus Booking", 9, 1));
		// gridArray.add(new GridMenuItem(mDthIcon, "Merchat Locator", 10, 1));
		// gridArray.add(new GridMenuItem(mReportsIcon, "Notifications", 10,
		// 1));
		// gridArray.add(new GridMenuItem(mReportsIcon, "Live Support", 11, 1));
		// gridArray.add(new GridMenuItem(mBillPaymentIcon, "Bus Booking", 12,
		// 1));

		gridView = (GridView) view.findViewById(R.id.gridView1);
		customGridAdapter = new RechargeMainAdapter(context,
				R.layout.rechargemain_adapter, gridArray, false);
		gridView.setAdapter(customGridAdapter);

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
									long arg3) {
				// TODO Auto-generated method stub

				switch (gridArray.get(pos).getId()) {
					case 1:
						startActivity(new Intent(context,
								MobileRechargeTabFragment.class));
						Constants.SERVICE_TYPE = "1";
						break;
					case 2:
						startActivity(new Intent(context,
								DthRechargeTabFragment.class));
						Constants.SERVICE_TYPE = "2";
						break;
					case 3:
						if (Utility.getRentalFlag(context).equalsIgnoreCase("1")) {
							startActivity(new Intent(context,
									KitRequestActivity.class).putExtra(
									"service_type", "4"));
							Constants.SERVICE_TYPE = "4";
						} else {
							startActivity(new Intent(context,
									MobileBillTabFragment.class));
							Constants.SERVICE_TYPE = "4";
						}

						break;
					case 4:

						if (Utility.getRentalFlag(context).equalsIgnoreCase("1")) {

							startActivity(new Intent(context,
									KitRequestActivity.class).putExtra(
									"service_type", "6"));
							Constants.SERVICE_TYPE = "6";
						} else {
							startActivity(new Intent(context,
									UtilityBillTabFragment.class));
							Constants.SERVICE_TYPE = "6";
						}

						break;
					case 5:
						startActivity(new Intent(context, WalletTabFragment.class));
						Constants.SERVICE_TYPE = "5";
						break;
					case 6:
						Intent chatIntent = new Intent(context, ChatActivity.class);
						chatIntent.putExtra("FromStatusBar", false);
						startActivity(chatIntent);
						break;
					case 7:
						startActivity(new Intent(context,
								AllQuickRechargeActivity.class));
						break;
					case 8:
						startActivity(new Intent(context, ReversalTabActivity.class));
						break;
					case 9:
						startActivity(new Intent(context, ReportMainActivity.class));
						break;

					case 10:
						startActivity(new Intent(context, LimitOptionActivity.class));
					/*
					 * startActivity(new Intent(context,
					 * PaymentActivity.class));
					 */
						break;
					case 11:
						startActivity(new Intent(context, CashPaymentActivity.class));
						break;
					case 12:
						final String ezeTapDeviceSerialNumber = Utility
								.getSharedPreferences(context,
										Constants.SHAREDPREFERENCE_DEVICE_SERIAL_NO);

						if (Utility.getLoginFlag(context,
								Constants.SHAREDPREFERENCE_IS_LOGIN)) {
							if (ezeTapDeviceSerialNumber.isEmpty()) {

								Intent intent = new Intent(context,
										EzetapLeadActivity.class);
								startActivity(intent);
							} else {

								startActivity(new Intent(context,
										EzetapDashBoardActivity.class));
							}

						} else {
							Intent intent = new Intent(context, LoginActivity.class);
							// intent.putExtra(Constants.REQUEST_FOR, bundle);
							// startActivityForResult(intent, Constants.LOGIN);
							startActivity(intent);

						}

						break;
					// case 8:
					// startActivity(new Intent(context,
					// MobileBillTabFragment.class));
					// break;
					// case 9:
					// startActivity(new Intent(context,
					// SettingsActivity.class));
					// break;
					// case 10:
					// startActivity(new Intent(context,
					// NotificationActivity.class));
					// break;
					// case 11:
					// startActivity(new Intent(context,
					// ChatActivity.class));
					// break;
					// case 12:
					// startActivity(new Intent(context,
					// BusBookingActivity.class));
					// break;
					default:
						break;
				}
				// Constants.overridePendingTransitionActivity(context,
				// Constants.randInt(1, 10));

			}
		});

		Runnable confirmBlock = new Runnable() {
			@Override
			public void run() {
				startActivity(new Intent(context, RetailerKYCActivity.class));
			}
		};

		if (trial_flag.equals("2")) {
			if (Utility.getIsAbleForPG(context)) {
				Constants
						.showTwoButtonDialog(
								context,
								"Please submit your KYC documents to continue services",
								"ACCOUNT INACTIVE", "NOW", confirmBlock,
								"LATER");
			} else {
				Constants
						.showOneButtonDialog(
								context,
								"ACCOUNT INACTIVE",
								"Your account is temporarily suspended. Please contact your distributor.",
								"OK", 0);
			}
		}

		Bundle bundle = getActivity().getIntent().getExtras();
		if (bundle != null) {
			String isPg = bundle.getString(Constants.PG_DATA);
			if (isPg != null) {
				if (isPg.equalsIgnoreCase(Constants.PG_SUCCESS)) {
					Constants
							.showOneButtonSuccessDialog(
									context,
									"Transaction Successfull. Your balance will be update soon.",
									Constants.PG_SETTINGS);
				}
			}
		}

		try {
			final View pay1AppDownload = view
					.findViewById(R.id.home_download_pay1);
			if (pay1AppDownload != null) {
				pay1AppDownload.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						startActivity(new Intent(context,
								ConsumerAppActivity.class));
					}
				});
				bannerClose = (ImageView) view
						.findViewById(R.id.pay1ConsumerClose);
				bannerClose.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						pay1AppDownload.setVisibility(View.GONE);

					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
			Constants.logStackTrace(context, e);
		}

		statusList = new ArrayList<String>();
		// ExpandableHeightListView transactionsListView =
		// (ExpandableHeightListView) findViewById(R.id.transactionsListView);
		// adapter = new StatusAdapter(context, statusList, 1);
		// transactionsListView.setAdapter(adapter);
		// transactionsListView.setExpanded(true);
		//
		// ImageView lastFiveTransLoader = (ImageView)
		// findViewById(R.id.lastFiveTransLoader);
		// lastFiveTransLoader.setOnClickListener(new View.OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// // new GetRequestTask().execute();
		// }
		// });

		return view;
	}

	private void loadLast5Transactions(String last5Transactions) {
		try {
			JSONArray jsonArray = new JSONArray(last5Transactions);
			statusList.clear();
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json_data = jsonArray.getJSONObject(i);
				statusList.add(json_data.toString());

			}
			adapter.notifyDataSetChanged();
		} catch (Exception e) {

		}
	}

	private void loadComplaintStats(String complaintStats) {
		try {
			JSONObject jsonDescription = new JSONObject(complaintStats);
			String complaints_count = jsonDescription.getString("complaints");
			unresolved_complaint.setText(complaints_count + " Complaints");
			if (!complaints_count.equals("0"))
				resolved_complaint.setText(complaints_count + " Resolved");
			else
				resolved_complaint.setText("");
		} catch (Exception e) {

		}
	}

	private boolean isMyServiceRunning(Class<?> serviceClass) {
		ActivityManager manager = (ActivityManager) getActivity()
				.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if (serviceClass.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	public class GetNotificationTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			String result = Constants.getPlanData(context, Constants.API_URL
					+ "method=sendNotification");
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			String replaced = result.replace("(", "").replace(")", "")
					.replace(";", "");
			try {
				JSONArray array = new JSONArray(replaced);
				JSONObject jsonObject = array.getJSONObject(0);
				String notification = jsonObject.getString("notification");
				Utility.setSharedPreference(context, "notification",
						notification);
			} catch (JSONException jee) {

			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}

	public void callApi() {

		new Thread(new Runnable() {
			public void run() {
				String result = Constants.getPlanData(context,
						Constants.API_URL + "method=sendNotification");

				String replaced = result.replace("(", "").replace(")", "")
						.replace(";", "");
				try {
					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = array.getJSONObject(0);
					String notification = jsonObject.getString("notification");
					Utility.setSharedPreference(context, "notification",
							notification);
				} catch (JSONException jee) {

				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		});
	}

	public static GoogleAnalytics analytics;
	//public static Tracker tracker;

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
		// EasyTracker.getInstance(this).activityStart(this);
		// tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
		/**
		 * Sanjeev please do the same for all activities and fragment
		 *
		 *
		 *
		 */
		// Constants.sendGoogleAnalytics(context, TAG);
		Log.i(TAG, "Setting screen name: " + TAG);
		//tracker.setScreenName("Imaddge~" + TAG);
		//tracker.send(new HitBuilders.ScreenViewBuilder().build());
	}

	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// updateUI(intent);
			Constants.showNavigationBar(context);
		}
	};

	public class CheckUpdateTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {

			String response = RequestClass
					.getInstance()
					.readPay1AppUpdateRequest(context,
							"https://androidquery.appspot.com/api/market?app=com.mindsarray.pay1");
			// web_update();
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONObject jsonObject = new JSONObject(replaced);
					String live_version = jsonObject.getString("version");
					// int curVersion = getPackageManager().getPackageInfo(
					// "com.mindsarray.pay1", 0).versionCode;
					String current_version = context.getPackageManager()
							.getPackageInfo("com.mindsarray.pay1", 0).versionName;

					// Log.w("SDFSD ", curVersion + "  " + current_version);

					if (!current_version.equalsIgnoreCase(live_version)) {

						try {
							final Dialog dialog = new Dialog(context);
							dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
							// dialog.getWindow().setBackgroundDrawableResource(
							// android.R.color.transparent);
							dialog.setContentView(R.layout.dialog_success_two_button);
							// dialog.setTitle(null);
							TextView textView_Title = (TextView) dialog
									.findViewById(R.id.textView_Title);
							textView_Title.setText("Pay1 Update Available");
							// set the custom dialog components - text, image
							// and button
							TextView textView_Message = (TextView) dialog
									.findViewById(R.id.textView_Message);
							textView_Message
									.setText("There is newer version of Pay1 application available, click OK to upgrade now?");

							ImageView closeButton = (ImageView) dialog
									.findViewById(R.id.imageView_Close);
							// if button is clicked, close the custom dialog
							closeButton
									.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) {
											dialog.dismiss();
										}
									});

							Button button_Ok = (Button) dialog
									.findViewById(R.id.button_Ok);
							// if button is clicked, close the custom dialog
							button_Ok
									.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) {
											dialog.dismiss();
											Intent intent = new Intent(
													Intent.ACTION_VIEW,
													Uri.parse("market://details?id=com.mindsarray.pay1"));
											startActivity(intent);
										}
									});

							Button button_Cancel = (Button) dialog
									.findViewById(R.id.button_Cancel);
							// if button is clicked, close the custom dialog
							button_Cancel
									.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) {
											dialog.dismiss();
										}
									});

							dialog.show();
						} catch (Exception exception) {
						}

					}

				} else {

				}
			} catch (JSONException exception) {

			} catch (Exception e) {
				// TODO: handle exception
			}

		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
		}

	}

	public class GetRequestTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair.add(new BasicNameValuePair("method",
					"lastFiveTransactions"));
			listValuePair.add(new BasicNameValuePair("device_type", "android"));

			String response = RequestClass.getInstance().readPay1Request(
					context, Constants.API_URL, listValuePair);
			listValuePair.clear();
			Log.e("Last 5 transactions:", "" + response);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			super.onPostExecute(result);
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = array.getJSONObject(0);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONArray jsonDescription = jsonObject
								.getJSONArray("description");
						JSONArray array2 = jsonDescription.getJSONArray(0);
						Utility.setSharedPreference(context,
								Constants.SHAREDPREFERENCE_LAST_5_TRANSACTIONS,
								array2.toString());
						statusList.clear();
						for (int i = 0; i < array2.length(); i++) {
							JSONObject json_data = array2.getJSONObject(i);
							statusList.add(json_data.toString());

						}
						adapter.notifyDataSetChanged();
					} else {
						Constants.showOneButtonFailureDialog(context,
								Constants.checkCode(replaced), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(context,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(context,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				Constants.showOneButtonFailureDialog(context,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(context);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							GetRequestTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			GetRequestTask.this.cancel(true);
			dialog.cancel();
		}
	}

	public class GetComplaintStats extends AsyncTask<String, String, String>
			implements OnDismissListener {
		ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair
					.add(new BasicNameValuePair("method", "complaintStats"));
			listValuePair.add(new BasicNameValuePair("device_type", "android"));

			String response = RequestClass.getInstance().readPay1Request(
					context, Constants.API_URL, listValuePair);
			listValuePair.clear();
			Log.e("Complaint stats:", "" + response);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			super.onPostExecute(result);
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = array.getJSONObject(0);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONObject jsonDescription = jsonObject
								.getJSONObject("description");
						Utility.setSharedPreference(context,
								Constants.SHAREDPREFERENCE_COMPLAINT_STATS,
								jsonDescription.toString());
						String complaints_count = jsonDescription
								.getString("complaints");
						unresolved_complaint.setText(complaints_count
								+ " Complaints");
						if (!complaints_count.equals("0"))
							resolved_complaint.setText(complaints_count
									+ " Resolved");
						else
							resolved_complaint.setText("");
					} else {
						Constants.showOneButtonFailureDialog(context,
								Constants.checkCode(replaced), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(context,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(context,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				Constants.showOneButtonFailureDialog(context,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(context);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							GetComplaintStats.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			GetComplaintStats.this.cancel(true);
			dialog.cancel();
		}
	}

	public static MainTabRechargesFragmentNew newInstance(String content) {
		MainTabRechargesFragmentNew fragment = new MainTabRechargesFragmentNew();
		MainActivity.currentItemIndex = 0;
		Bundle b = new Bundle();
		b.putString("msg", content);

		fragment.setArguments(b);

		return fragment;
	}


}
