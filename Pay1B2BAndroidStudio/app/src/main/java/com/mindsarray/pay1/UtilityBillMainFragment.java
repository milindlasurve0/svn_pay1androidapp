package com.mindsarray.pay1;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.RechargeMainAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.utilities.GridMenuItem;

public class UtilityBillMainFragment extends Fragment {

	View vi = null;
	GridView gridView;
	ArrayList<GridMenuItem> gridArray = new ArrayList<GridMenuItem>();
	RechargeMainAdapter customGridAdapter;
	private static final String TAG = "Utility Bill Main Fragment";
	// String planJson;
	// JSONObject jsonObject2;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		vi = inflater.inflate(R.layout.rechargemain_activity, container, false);
//		analytics = GoogleAnalytics.getInstance(getActivity());
		tracker = Constants.setAnalyticsTracker(getActivity(), tracker);
		//easyTracker.set(Fields.SCREEN_NAME, TAG);
		if (gridArray.size() == 0) {
			int mRelianceEnergy = R.drawable.reliance_energy;
			int mBSESRajdhani = R.drawable.bses_rajdhani;
			int mBSESYamuna = R.drawable.bses_yamuna;
			int mNorthDelhiPowerLimited = R.drawable.ndpl;
			int mAirtel = R.drawable.airtel_landline;
			int mMTNLDelhi = R.drawable.mtnl;
			int mMahanagarGasLimited = R.drawable.mahangar_gas;

			gridArray
					.add(new GridMenuItem(mRelianceEnergy, "Reliance Energy",
							Constants.OPERATOR_UITLITY_RELIANCE,
							Constants.BILL_UTILITY));
			gridArray.add(new GridMenuItem(mBSESRajdhani, "BSES Rajdhani",
					Constants.OPERATOR_UITLITY_BSES_RAJDHANI,
					Constants.RECHARGE_MOBILE));
			gridArray.add(new GridMenuItem(mBSESYamuna, "BSES Yamuna",
					Constants.OPERATOR_UITLITY_BSES_YAMUNA,
					Constants.BILL_UTILITY));
			gridArray.add(new GridMenuItem(mNorthDelhiPowerLimited,
					"North Delhi Power Limited",
					Constants.OPERATOR_UITLITY_NDPL, Constants.BILL_UTILITY));
			gridArray.add(new GridMenuItem(mAirtel, "Airtel",
					Constants.OPERATOR_UITLITY_AIRTEL, Constants.BILL_UTILITY));
			gridArray.add(new GridMenuItem(mMTNLDelhi, "MTNL Delhi",
					Constants.OPERATOR_UITLITY_MTNL_DELHI,
					Constants.BILL_UTILITY));
			gridArray.add(new GridMenuItem(mMahanagarGasLimited,
					"Mahanagar Gas Limited",
					Constants.OPERATOR_UITLITY_MAHANAGAR_GAS,
					Constants.BILL_UTILITY));

		}

		gridView = (GridView) vi.findViewById(R.id.gridView1);

		customGridAdapter = new RechargeMainAdapter(getActivity(),
				R.layout.rechargemain_adapter, gridArray,false);

		// planJson = Constants.loadJSONFromAsset(getActivity(), "plans.json");
		gridView.setAdapter(customGridAdapter);

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// Constants.showCustomToast(MobileRechargeActivity.this,
				// gridArray.get(position).getId()+"",
				// Toast.LENGTH_LONG).show();
				try {
					/*
					 * JSONObject jsonObject=new JSONObject(planJson);
					 * jsonObject2
					 * =jsonObject.getJSONObject(String.valueOf(gridArray
					 * .get(position).getId()));
					 */
					Intent intent = new Intent(getActivity(),
							UtilityBillActivity.class);
					intent.putExtra(Constants.OPERATOR_ID,
							gridArray.get(position).getId());
					/*
					 * intent.putExtra(Constants.PLAN_JSON,
					 * jsonObject2.toString());
					 */
					intent.putExtra(Constants.PLAN_JSON,
							String.valueOf(gridArray.get(position).getId()));
					intent.putExtra(Constants.OPERATOR_LOGO,
							gridArray.get(position).getImage());
					intent.putExtra(Constants.OPERATOR_NAME,
							gridArray.get(position).getTitle());
					getActivity().startActivity(intent);

				} catch (Exception e) {
					// e.printStackTrace();
				}

			}

		});
		return vi;

	}
	public static GoogleAnalytics analytics; public static Tracker tracker;
	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
		// EasyTracker.getInstance(this).activityStart(this);
		//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
	}
	public static UtilityBillMainFragment newInstance(String content) {
		UtilityBillMainFragment fragment = new UtilityBillMainFragment();

		Bundle b = new Bundle();
		b.putString("msg", content);

		fragment.setArguments(b);

		return fragment;
	}

	// private String mContent = "???";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// if ((savedInstanceState != null)
		// && savedInstanceState.containsKey(KEY_CONTENT)) {
		// mContent = savedInstanceState.getString(KEY_CONTENT);
		// }
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// outState.putString(KEY_CONTENT, mContent);
	}
}
