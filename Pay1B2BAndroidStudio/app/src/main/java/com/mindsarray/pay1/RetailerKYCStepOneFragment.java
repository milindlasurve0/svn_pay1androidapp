package com.mindsarray.pay1;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.receiverhandler.AddressResultReceiver;
import com.mindsarray.pay1.servicehandler.FetchAddressIntentService;
import com.mindsarray.pay1.utilities.Utility;

public class RetailerKYCStepOneFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private int position;

    private OnFragmentInteractionListener mListener;
    private Fragment mFragment = RetailerKYCStepOneFragment.this;

    private SupportMapFragment mapFragment;
    private GoogleMap map;
    RelativeLayout mapRelativeLayout;

    String latitude;
    String longitude;
    
    private Boolean mapped = false;

    private Spinner mRentalTypeView;

    public static RetailerKYCStepOneFragment newInstance(int position) {
    	RetailerKYCStepOneFragment fragment = new RetailerKYCStepOneFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, position);
        fragment.setArguments(args);
        return fragment;
    }

    public RetailerKYCStepOneFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            position = getArguments().getInt(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.retailer_kyc_step_1, container, false);
        mapRelativeLayout = (RelativeLayout) view.findViewById(R.id.map_container);

//        Motley.setSpinner(getActivity(), view, R.id.rental_type_spinner, R.array.rental_type_array);
        
        latitude = Utility.getLatitude(getActivity(), Constants.SHAREDPREFERENCE_LATITUDE);
        longitude = Utility.getLongitude(getActivity(), Constants.SHAREDPREFERENCE_LONGITUDE);
        
        TextView locateTextView = (TextView) view.findViewById(R.id.locateLocation);
        locateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Constants.displayPromptForEnablingGPS(getActivity())) {
                    if (!mapped)
                        setLatLng();

                    Intent intent = new Intent(getActivity(),
                            MapsActivity.class);
                    intent.putExtra("latitude", latitude);
                    intent.putExtra("longitude", longitude);
                    startActivityForResult(intent, Constants.REQUEST_GET_MAP_LOCATION);
                }
            }
        });
        attachListeners(view);

        Button cancelButton = (Button) view.findViewById(R.id.cancel_button);
        Button continueButton = (Button) view.findViewById(R.id.next_step_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((RetailerKYCActivity) getActivity()).mViewPager.setCurrentItem(1, true);
            }
        });
        
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

           @Override
           public void run() {
	        	FragmentManager fm = getActivity().getSupportFragmentManager();
		        mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map_fragment);
		        if (mapFragment == null) {
		            mapFragment = SupportMapFragment.newInstance(new GoogleMapOptions().liteMode(true));
		            fm.beginTransaction().replace(R.id.map_fragment, mapFragment).commit();
		        }
		        map = mapFragment.getMap();
		        ((SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map_fragment)).getMap();
		        map.setOnMapClickListener(new OnMapClickListener() {
					
					@Override
					public void onMapClick(LatLng arg0) {
						// TODO Auto-generated method stub
						if(Constants.displayPromptForEnablingGPS(getActivity())) {
		                    if (!mapped)
		                        setLatLng();

		                    Intent intent = new Intent(getActivity(),
		                            MapsActivity.class);
		                    intent.putExtra("latitude", latitude);
		                    intent.putExtra("longitude", longitude);
		                    startActivityForResult(intent, Constants.REQUEST_GET_MAP_LOCATION);
		                }
					}
				});
		        
		      
		        if(map != null){
		        	map.getUiSettings().setMapToolbarEnabled(false);
		        }	
               else {
                    handler.postDelayed(this, 500);
               }
           }      
         }, 500);
        
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_GET_MAP_LOCATION && resultCode == Activity.RESULT_OK) {
            latitude = data.getStringExtra("latitude");
            longitude = data.getStringExtra("longitude");
            mapped = true;
            Location location = new Location("");
            location.setLatitude(Double.parseDouble(latitude));
            location.setLongitude(Double.parseDouble(longitude));

            setLiteMap();

            AddressResultReceiver mResultReceiver = new AddressResultReceiver(new Handler(), getActivity());
            Intent intent = new Intent(getActivity(), FetchAddressIntentService.class);
            intent.putExtra(Constants.RECEIVER, mResultReceiver);
            intent.putExtra(Constants.LOCATION_DATA_EXTRA, location);
            getActivity().startService(intent);
        }
    }
    
    public void setLatLng(){
        latitude = Utility.getLatitude(getActivity(), Constants.SHAREDPREFERENCE_LATITUDE);
        longitude = Utility.getLongitude(getActivity(), Constants.SHAREDPREFERENCE_LONGITUDE);

        if(latitude.equals("null") || longitude.equals("null") || latitude.equals("0") || longitude.equals("0") || latitude.equals("") || longitude.equals("")){
            latitude = ((RetailerKYCActivity) getActivity()).mGooglePlayServicesApiClient.latitude;
            longitude = ((RetailerKYCActivity) getActivity()).mGooglePlayServicesApiClient.longitude;
        }
        else
            setLiteMap();
    }

    public void setLiteMap(){
        mapRelativeLayout.setVisibility(View.VISIBLE);

        Location location = new Location("");
        location.setLatitude(Double.parseDouble(latitude));
        location.setLongitude(Double.parseDouble(longitude));
        if (map == null) {
            map = mapFragment.getMap();
        }
        map.clear();
        map.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude))));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)), 10));
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
    }
    
    public void setMapFragment(View view){
    	try{
	    	FragmentManager fm = getActivity().getSupportFragmentManager();
	        mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map_fragment);
	        if (mapFragment == null) {
	            mapFragment = SupportMapFragment.newInstance(new GoogleMapOptions().liteMode(true));
	            fm.beginTransaction().replace(R.id.map_fragment, mapFragment).commit();
	        }
	        view.setClickable(false);
	        map = mapFragment.getMap();
	        if(map != null)
	        	map.getUiSettings().setMapToolbarEnabled(false);
    	}
    	catch(Exception e){
    		e.printStackTrace();
    	}
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if (map == null) {
        	if(mapFragment == null){
        		FragmentManager fm = getActivity().getSupportFragmentManager();
                mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map_fragment);
                if (mapFragment == null) {
                    mapFragment = SupportMapFragment.newInstance(new GoogleMapOptions().liteMode(true));
                    fm.beginTransaction().replace(R.id.map_fragment, mapFragment).commit();
                }
        	}
            map = mapFragment.getMap();
            if(map != null)
            	map.addMarker(new MarkerOptions().position(new LatLng(0, 0)));
        }
    }
    
    private void attachListeners(View view){
        final EditText mNameView = (EditText) view.findViewById(R.id.retailer_name);
        mNameView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus && mNameView.getText().toString().length() < 5)
//                    mNameView.setError("Enter a proper name for the retailer");
//              /*  else
//                    mNameView.setError(null);*/
            }
        });

        final EditText mMobileView = (EditText) view.findViewById(R.id.retailer_mobile);
        mMobileView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus && mMobileView.getText().toString().length() != 10)
//                    mMobileView.setError("Enter 10 digits for mobile number");
//               /* else
//                    mMobileView.setError(null);*/
            }
        });

        final EditText mShopView = (EditText) view.findViewById(R.id.retailer_shop_name);
        mShopView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus && mShopView.getText().toString().length() < 5)
//                    mShopView.setError("Enter a proper shop name");
//           /*     else
//                    mShopView.setError(null);*/
            }
        });
        final EditText mAddressView = (EditText) view.findViewById(R.id.retailer_address);
        mAddressView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus && mAddressView.getText().toString().length() < 5)
//                    mAddressView.setError("Enter 10 digits for mobile number");
//               /* else
//                    mAddressView.setError(null);*/
            }
        });

        final EditText mAreaView = (EditText) view.findViewById(R.id.retailer_area);
        mAreaView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus && mAreaView.getText().toString().length() < 3)
//                    mAreaView.setError("Enter address line 1");
//              /*  else
//                    mAreaView.setError(null);*/
            }
        });

        final EditText mCityView = (EditText) view.findViewById(R.id.retailer_city);
        mCityView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus && mCityView.getText().toString().length() < 2)
//                    mCityView.setError("Enter a proper city");
//              /*  else
//                    mCityView.setError(null);*/
            }
        });

        final EditText mStateView = (EditText) view.findViewById(R.id.retailer_state);
        mStateView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus && mStateView.getText().toString().length() < 5)
//                    mStateView.setError("Enter a proper state");
//              /*  else
//                    mStateView.setError(null);*/
            }
        });
        final EditText mPinCodeView = (EditText) view.findViewById(R.id.retailer_pin_code);
        mPinCodeView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus && mPinCodeView.getText().toString().length() != 6)
//                    mPinCodeView.setError("Enter 6 digits for Pin Code");
//               /* else
//                    mPinCodeView.setError(null);*/
            }
        });
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
