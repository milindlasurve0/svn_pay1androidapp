package com.mindsarray.pay1;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.EditTextValidator;
import com.mindsarray.pay1.utilities.Utility;

public class WalletTopupActivity extends Activity {

	View vi = null;
	private EditText editMobileNumber, editRechargeAmount;
	private Button mBtnRchNow;
	private static final String TAG = "Wallet Topup";
	String mobileNumber, amount, planJson;

	String uuid;
	boolean bNetOrSms;
	private SharedPreferences mSettingsData;// Shared preferences for setting
			public static GoogleAnalytics analytics; public static Tracker tracker;								// data
	long timestamp;
	Context mContext = WalletTopupActivity.this;
	Bundle bundle;
	private String mProductId;
	private String mOperatorName;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		tracker = Constants.setAnalyticsTracker(mContext, tracker);
		setContentView(R.layout.walletrecharge_activity);
		
		try {
			mSettingsData = mContext.getSharedPreferences(
					SettingsSmsActivity.SETTINGS_NAME, 0);
			bNetOrSms = mSettingsData.getBoolean(
					SettingsSmsActivity.KEY_NETWORK, true);

			editMobileNumber = (EditText) findViewById(R.id.editMobileNumber);
			editRechargeAmount = (EditText) findViewById(R.id.editRechargeAmount);
			
			bundle = getIntent().getExtras();
			mProductId = bundle.getInt(Constants.OPERATOR_ID) + "";
			ImageView mImageOprLogo = (ImageView) findViewById(R.id.operator_logo);
			TextView mTextOperatorName = (TextView) findViewById(R.id.operator_name);
			mImageOprLogo.setImageResource(getIntent().getIntExtra(
					Constants.OPERATOR_LOGO, 0));
			mTextOperatorName.setText(bundle.getString(Constants.OPERATOR_NAME));
			mOperatorName = bundle.getString(Constants.OPERATOR_NAME);
			
			editMobileNumber.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					// EditTextValidator.hasText(getActivity(),
					// editMobileNumber,
					// Constants.ERROR_MOBILE_BLANK_FIELD);
					if (editMobileNumber.length() == 10) {
						editRechargeAmount.requestFocus();
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			// editRechargeAmount.addTextChangedListener(new TextWatcher() {
			// public void afterTextChanged(Editable s) {
			// EditTextValidator.hasText(getActivity(),
			// editRechargeAmount,
			// Constants.ERROR_AMOUNT_BLANK_FIELD);
			// }
			//
			// public void beforeTextChanged(CharSequence s, int start,
			// int count, int after) {
			// }
			//
			// public void onTextChanged(CharSequence s, int start,
			// int before, int count) {
			// }
			// });

			mBtnRchNow = (Button) findViewById(R.id.mBtnRchNow);
			mBtnRchNow.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					bNetOrSms = mSettingsData.getBoolean(
							SettingsSmsActivity.KEY_NETWORK, true);

					timestamp = System.currentTimeMillis();
					mobileNumber = editMobileNumber.getText().toString();
					amount = editRechargeAmount.getText().toString();

					if (!checkValidation(mContext)) {

					} else {

						if (bNetOrSms) {

							if (Utility.getLoginFlag(mContext,
									Constants.SHAREDPREFERENCE_IS_LOGIN)) {

								new AlertDialog.Builder(mContext)
										.setTitle(mOperatorName)
										.setMessage(
												"Mobile Number: "
														+ mobileNumber
														+ "\nAmount: "
														+ amount
														+ "\nAre you sure you want to do topup?")
										.setPositiveButton(
												"Yes",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int which) {
														dialog.dismiss();
														new WalletPaymentTask()
																.execute();

													}
												})
										.setNegativeButton(
												"No",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int which) {
														dialog.dismiss();
													}
												}).show();
							} else {
								Intent intent = new Intent(mContext,
										LoginActivity.class);
								startActivity(intent);
							}
						} else {
							new AlertDialog.Builder(mContext)
									.setTitle(mOperatorName + " (SMS)")
									.setMessage(
											"Mobile Number: "
													+ mobileNumber
													+ "\nAmount: "
													+ amount
													+ "\nAre you sure you want to do topup?")
									.setPositiveButton(
											"Yes",
											new DialogInterface.OnClickListener() {
												public void onClick(
														DialogInterface dialog,
														int which) {
													dialog.dismiss();
													Constants
															.sendSMSMessage(
																	mContext,
																	Constants.WALLET_PAYMENT,
																	Constants.OPERATOR_PAY1_WALLET,
																	mobileNumber,
																	mobileNumber,
																	amount,
																	"0", "");
													// getActivity().finish();
												}
											})
									.setNegativeButton(
											"No",
											new DialogInterface.OnClickListener() {
												public void onClick(
														DialogInterface dialog,
														int which) {
													dialog.dismiss();
												}
											}).show();

						}

					}
				}
			});
		} catch (Exception e) {
		}
	}
	
	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
		// EasyTracker.getInstance(this).activityStart(this);
		//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants.showNavigationBar(WalletTopupActivity.this);
		super.onResume();
	}
	
	private boolean checkValidation(Context context) {
		boolean ret = true;

		if (!EditTextValidator.hasText(context, editMobileNumber,
				Constants.ERROR_MOBILE_BLANK_FIELD)) {
			ret = false;
			editMobileNumber.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidMobileNumber(context,
				editMobileNumber, Constants.ERROR_MOBILE_LENGTH_FIELD)) {
			ret = false;
			editMobileNumber.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editRechargeAmount,
				Constants.ERROR_AMOUNT_BLANK_FIELD)) {
			ret = false;
			editRechargeAmount.requestFocus();
			return ret;
		} else
			return ret;
	}

	public class WalletPaymentTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
			listValuePair.add(new BasicNameValuePair("method", "walletTopup"));
			listValuePair.add(new BasicNameValuePair("mobile",
					mobileNumber));
			listValuePair.add(new BasicNameValuePair("amount", amount));
			listValuePair.add(new BasicNameValuePair("timestamp", String
					.valueOf(timestamp)));
			listValuePair.add(new BasicNameValuePair("profile_id", Utility
					.getProfileID(mContext,
							Constants.SHAREDPREFERENCE_PROFILE_ID)));
			listValuePair.add(new BasicNameValuePair("hash_code", Constants
					.encryptPassword(Utility.getUUID(mContext,
							Constants.SHAREDPREFERENCE_UUID)
							+ mobileNumber
							+ amount + timestamp)));
			listValuePair.add(new BasicNameValuePair("device_type", "android"));
			listValuePair.add(new BasicNameValuePair("product_id", mProductId));

			String response = RequestClass.getInstance().readPay1Request(
					mContext, Constants.API_URL, listValuePair);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}

			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = array.getJSONObject(0);
					String status = jsonObject.getString("status");
					String description = jsonObject.getString("description");
					// String rechargeString = Constants.getRechargeString(
					// mContext, Constants.RECHARGE_MOBILE, mOperatorId,
					// mobileNumber, mobileNumber, amount, "0");
					/*
					 * try { notificationDataSource.open();
					 * notificationDataSource.createNotification(rechargeString,
					 * 0, "" + System.currentTimeMillis()); } catch
					 * (SQLException exception) { // TODO: handle exception //
					 * // Log.e("Er ", exception.getMessage()); } catch
					 * (Exception e) { // TODO: handle exception // //
					 * Log.e("Er ", e.getMessage()); } finally {
					 * notificationDataSource.close(); }
					 */

					// ContentValues contentValues = new ContentValues();
					// contentValues.put("notification_msg", rechargeString);
					// contentValues.put("timestamp ", "12345678902");
					// contentValues.put("netOrSms ", 0);
					// controller.insertNotifications(contentValues);
					if (status.equalsIgnoreCase("success")) {
						// Constants.showCustomToast(getActivity(),
						// "Bill payment request sent successfully.");
						Constants.showOneButtonSuccessDialog(mContext,
								mOperatorName + " Topup done successfully.\nTransaction id : "
										+ jsonObject.getString("description"),
								Constants.RECHARGE_SETTINGS);
						String balance = jsonObject.getString("balance");
						Utility.setBalance(mContext,
								Constants.SHAREDPREFERENCE_BALANCE, balance);

						// finish();
					} else {
						Constants.showOneButtonFailureDialog(mContext,
								Constants.checkCode(result), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(mContext,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(mContext,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(mContext,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(mContext);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							WalletPaymentTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			WalletPaymentTask.this.cancel(true);
			dialog.cancel();
		}

	}

}
