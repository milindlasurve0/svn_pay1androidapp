package com.mindsarray.pay1.databasehandler;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class AddressDataSource {
	// Database fields
	private SQLiteDatabase database;
	private Pay1SQLiteHelper dbHelper;

	public AddressDataSource(Context context) {
		dbHelper = new Pay1SQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public void createAddress(String addressLine, String addressArea,
			String addressCity, String addressState, String addressZip,
			String addressLatitude, String addressLongitude) {
		ContentValues values = new ContentValues();
		values.put(Pay1SQLiteHelper.ADDRESS_LINE, addressLine);
		values.put(Pay1SQLiteHelper.ADDRESS_AREA, addressArea);
		values.put(Pay1SQLiteHelper.ADDRESS_CITY, addressCity);
		values.put(Pay1SQLiteHelper.ADDRESS_STATE, addressState);
		values.put(Pay1SQLiteHelper.ADDRESS_ZIP, addressZip);
		values.put(Pay1SQLiteHelper.ADDRESS_LATITUDE, addressLatitude);
		values.put(Pay1SQLiteHelper.ADDRESS_LONGITUDE, addressLongitude);

		database.insert(Pay1SQLiteHelper.TABLE_ADDRESS, null, values);
		// Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_Address,
		// null, Pay1SQLiteHelper.Address_ID + " = " + insertId, null,
		// null, null, null);
		// cursor.moveToFirst();
		// Address neAddress = cursorToAddress(cursor);
		// cursor.close();
		// return newAddress;
	}

	public void updateAddress(String addressLine, String addressArea,
			String addressCity, String addressState, String addressZip,
			String addressLatitude, String addressLongitude, int addressID) {

		ContentValues values = new ContentValues();
		values.put(Pay1SQLiteHelper.ADDRESS_LINE, addressLine);
		values.put(Pay1SQLiteHelper.ADDRESS_AREA, addressArea);
		values.put(Pay1SQLiteHelper.ADDRESS_CITY, addressCity);
		values.put(Pay1SQLiteHelper.ADDRESS_STATE, addressState);
		values.put(Pay1SQLiteHelper.ADDRESS_ZIP, addressZip);
		values.put(Pay1SQLiteHelper.ADDRESS_LATITUDE, addressLatitude);
		values.put(Pay1SQLiteHelper.ADDRESS_LONGITUDE, addressLongitude);
		long updatetId = database.update(Pay1SQLiteHelper.TABLE_ADDRESS,
				values, Pay1SQLiteHelper.ADDRESS_ID + " = " + addressID, null);
		if (updatetId > 0) {
		}
		// // Log.i(Tag, "Monitor updated with id: " + monitor_ID);
		else {
		}
	}

	public void deleteAddress() {
		database.delete(Pay1SQLiteHelper.TABLE_ADDRESS, null, null);
	}

	public void deleteAddress(Address Address) {
		long id = Address.getAddressID();
		database.delete(Pay1SQLiteHelper.TABLE_ADDRESS,
				Pay1SQLiteHelper.ADDRESS_ID + " = " + id, null);
	}

	public List<Address> getAllAddress() {
		List<Address> addresss = new ArrayList<Address>();

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_ADDRESS, null,
				null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Address address = cursorToAddress(cursor);
			addresss.add(address);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return addresss;
	}

	private Address cursorToAddress(Cursor cursor) {
		Address Address = new Address();
		Address.setAddressID(cursor.getInt(0));
		Address.setAddressLine(cursor.getString(1));
		Address.setAddressArea(cursor.getString(2));
		Address.setAddressCity(cursor.getString(3));
		Address.setAddressState(cursor.getString(4));
		Address.setAddressZip(cursor.getString(5));
		Address.setAddressLatitude(cursor.getString(6));
		Address.setAddressLongitude(cursor.getString(7));
		return Address;
	}
}
