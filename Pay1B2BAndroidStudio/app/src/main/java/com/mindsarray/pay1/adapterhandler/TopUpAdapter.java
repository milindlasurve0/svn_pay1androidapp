package com.mindsarray.pay1.adapterhandler;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.net.ParseException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mindsarray.pay1.R;

public class TopUpAdapter extends ArrayAdapter<JSONObject> {
	Context mContext;
	List<JSONObject> jsonObject;
	LayoutInflater inflater;
	TextView textTopUpDate, textTopUpAmount;

	public TopUpAdapter(Context context, int textViewResourceId,
			List<JSONObject> objects) {
		super(context, textViewResourceId, objects);
		this.mContext = context;
		this.jsonObject = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (convertView == null) {
			inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.topup_adapter, null);
		}

		if (position % 2 == 1) {
			view.setBackgroundColor(Color.WHITE);
		} else {

			view.setBackgroundColor(mContext.getResources().getColor(
					R.color.white));
		}

		textTopUpDate = (TextView) view.findViewById(R.id.textTopUpDate);
		textTopUpAmount = (TextView) view.findViewById(R.id.textTopUpAmount);

		try {
			DecimalFormat decimalFormat = new DecimalFormat("00.00");
			double am = Double.parseDouble(jsonObject.get(position)
					.getString("amount").toString().trim());
			textTopUpAmount.setText("Rs. " + decimalFormat.format(am));

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
			String d = jsonObject.get(position).getString("day").toString()
					.trim().replaceAll("-", "/");
			Date date = new Date(d);// new Date(new Date());

			textTopUpDate.setText(dateFormat.format(date));
		} catch (ParseException e) {
			// e.printStackTrace();
		} catch (Exception exception) {
			// exception.printStackTrace();
		}

		return view;
	}

}
