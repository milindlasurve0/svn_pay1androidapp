package com.mindsarray.pay1.utilities;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.mindsarray.pay1.R;
import com.mindsarray.pay1.adapterhandler.ImageAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.contenthandler.Document;

/**
 * Created by rohan on 22/5/15.
 */
public class AttachImage {

	private static final int SELECT_MULTIPLE_PHOTOS = 101;
	private Activity mActivity;
	private Fragment mFragment;
	public static final int THUMBNAIL_WIDTH = 140;
	public static final int THUMBNAIL_HEIGHT = 140;
	private String mCurrentPhotoPath;
	public ExpandableHeightGridView mGridView;
	public ArrayList<Document> mDocuments;
	ImageAdapter imageAdapter;
	public Boolean intentRun = false;
	int mMaxItems;
	ImageButton mButton;
	DocumentButton mDocumentUploadButton;
	public ArrayList<String> mImageURIRemovalList;
	ImageView imgSample;
	private static final File storageDir = Environment
			.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

	public AttachImage(Fragment fragment, Activity activity,
			ExpandableHeightGridView gridView, int maxItems,
			ImageButton button, DocumentButton documentUploadButton,
			ImageView imgSample) {
		mActivity = activity;
		mFragment = fragment;
		mGridView = gridView;
		mDocuments = new ArrayList<>();
		imageAdapter = new ImageAdapter(mActivity, mDocuments, button,
				documentUploadButton);
		mMaxItems = maxItems;
		mButton = button;
		this.imgSample = imgSample;
		mImageURIRemovalList = imageAdapter.mImageURIRemovalList;
		mButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog();
			}
		});
		mDocumentUploadButton = documentUploadButton;
	}

	public void dialog() {
		final CharSequence[] items = { "Capture", "Choose from Gallery",
				"Cancel" };

		AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
		builder.setTitle("Add Photo!");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (items[item].equals("Capture")) {
					dispatchTakePictureIntent();
				} else if (items[item].equals("Choose from Gallery")) {
					selectImageFromGallery();
				} else if (items[item].equals("Cancel")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	private void dispatchTakePictureIntent() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
			File photoFile = null;
			try {
				photoFile = createImageFile();
			} catch (IOException ex) {
				// Error occurred while creating the File
				ex.printStackTrace();
				Log.e("create image file: ", ex.getMessage());
			}
			// Continue only if the File was successfully created
			if (photoFile != null) {
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(photoFile));
				mFragment.startActivityForResult(takePictureIntent,
						Constants.REQUEST_IMAGE_CAPTURE);
				intentRun = true;
			}
		}
	}

	private void selectImageFromGallery() {
		Intent intent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		intent.setType("image/*");
		mFragment.startActivityForResult(
				Intent.createChooser(intent, "Select File"),
				Constants.SELECT_PHOTO);
		intentRun = true;
	}

	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		String imageFileName = "JPEG_" + timeStamp + "_";
		// File storageDir = Environment.getExternalStoragePublicDirectory(
		// Environment.DIRECTORY_PICTURES);

		// File storageDir =
		// mActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
		// File storageDir = Environment.getExternalStoragePublicDirectory(
		// Environment.DIRECTORY_PICTURES);
		File image = File.createTempFile(imageFileName, /* prefix */
				".jpg", /* suffix */
				storageDir /* directory */
		);

		// Save a file: path for use with ACTION_VIEW intents
		mCurrentPhotoPath = image.getAbsolutePath();
		return image;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data,
			int photoType) {
		intentRun = false;
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == Constants.REQUEST_IMAGE_CAPTURE) {
				try {
					renderDocument(new Document("0", "", mCurrentPhotoPath,
							"0", "", ""), photoType);
				} catch (Exception e) {
					e.printStackTrace();
					Log.e("mGridView: ", e.getMessage());
				}
			} else if (requestCode == Constants.SELECT_PHOTO) {
				try {
					String selectedImagePath;
					Uri selectedImageUri = data.getData();
					Cursor cursor = mActivity.getContentResolver().query(
							selectedImageUri, null, null, null, null);
					if (cursor == null) {
						selectedImagePath = selectedImageUri.getPath();
					} else {
						cursor.moveToFirst();
						int idx = cursor
								.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
						selectedImagePath = cursor.getString(idx);
						cursor.close();
					}
					renderDocument(new Document("0", "", selectedImagePath,
							"0", "", ""), photoType);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void renderDocument(Document document, final int photoType) {
		if (mDocuments.size() >= mMaxItems - 1)
			mButton.setVisibility(View.GONE);
		else
			mButton.setVisibility(View.VISIBLE);
		ImageCompressionAsyncTask imageCompression = new ImageCompressionAsyncTask() {
			@Override
			protected void onPostExecute(Document document) {
				mDocuments.add(document);
				
				
				if (photoType == 2) {
					if (mDocuments.size() == 0) {
						imgSample.setVisibility(View.VISIBLE);
					} else {
						imgSample.setVisibility(View.GONE);
					}
				}else{
					imgSample.setVisibility(View.GONE);
				}
				
				
				mGridView.setAdapter(imageAdapter);
				imageAdapter.notifyDataSetChanged();
			}
		};
		if (!document.uri.startsWith("http"))
			imageCompression.execute(document);
		else {
			mDocuments.add(document);
			mGridView.setAdapter(imageAdapter);
			imageAdapter.notifyDataSetChanged();
		}

		/*
		 * mDocumentUploadButton.setPresent(true);
		 * if(document.verify_flag.equals("0"))
		 * mDocumentUploadButton.setVerified(false); else
		 * mDocumentUploadButton.setVerified(true);
		 */

		

		mDocumentUploadButton.setPresent(true);
		if (document.verify_flag.equals("0")) {
			// mDocumentUploadButton.setVerified(false);
			// mDocumentUploadButton.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_verify_pancard));
			if (document.verify_flag.equals("0")) {
				// mDocumentUploadButton.setVerified(false);
				if (photoType == 0) {
					mDocumentUploadButton.setImageDrawable(mActivity
							.getResources().getDrawable(
									R.drawable.ic_verify_pancard));
				} else if (photoType == 1) {
					mDocumentUploadButton.setImageDrawable(mActivity
							.getResources().getDrawable(
									R.drawable.ic_verify_addproof));
				} else if (photoType == 2) {
					mDocumentUploadButton.setImageDrawable(mActivity
							.getResources().getDrawable(
									R.drawable.ic_verify_shoppic));
				} else {
					mDocumentUploadButton.setImageDrawable(mActivity
							.getResources().getDrawable(
									R.drawable.ic_verify_pancard));
				}
			}
		} else if (document.verify_flag.equals("-1")) {
			mDocumentUploadButton.setVerified(false);
		} else {
			mDocumentUploadButton.setVerified(true);
		}
	}

	// private void checkMaxAdd(String filePath) {
	// if (imagesPathList.size() >= mMaxItems - 1)
	// mButton.setVisibility(View.GONE);
	// else
	// mButton.setVisibility(View.VISIBLE);
	// ImageCompressionAsyncTask imageCompression = new
	// ImageCompressionAsyncTask() {
	// @Override
	// protected void onPostExecute(String imagePath) {
	// imagesPathList.add(imagePath);
	// mGridView.setAdapter(imageAdapter);
	// imageAdapter.notifyDataSetChanged();
	// }
	// };
	// if (!filePath.startsWith("http"))
	// imageCompression.execute(filePath);
	// else {
	// imagesPathList.add(filePath);
	// mGridView.setAdapter(imageAdapter);
	// imageAdapter.notifyDataSetChanged();
	// }
	// }

	public abstract class ImageCompressionAsyncTask extends
			AsyncTask<Document, Void, Document> {

		@Override
		protected Document doInBackground(Document... documents) {
			if (documents[0].uri.length() == 0 || documents[0].uri == null)
				return null;
			return ImageUtils.compressImage(documents[0]);
		}

		protected abstract void onPostExecute(Document document);
	}

	private void galleryAddPic() {
		Intent mediaScanIntent = new Intent(
				Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		File f = new File(mCurrentPhotoPath);
		Uri contentUri = Uri.fromFile(f);
		mediaScanIntent.setData(contentUri);
		mActivity.sendBroadcast(mediaScanIntent);
	}

	public ArrayList<String> getImagesPathList() {
		ArrayList<String> stripedImagePathList = new ArrayList<String>();
		for (Document document : mDocuments) {
			if (!document.uri.startsWith("http")) {
				if (document.uri.startsWith("file:"))
					stripedImagePathList.add(document.uri.replace("file:", ""));
				else
					stripedImagePathList.add(document.uri);
			}
		}
		return stripedImagePathList;
	}

	public void loadAndSave(ArrayList<Document> documents, int photoType) {
		mDocuments.clear();
		if (documents.size() > 0) {
			for (Document document : documents) {
				renderDocument(document, photoType);
			}
		}
		// for(String uri : uriList) {
		// Picasso.with(context).load(uri).into(new Target() {
		// @Override
		// public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
		// try {
		// File file;
		// if (!storageDir.exists()) {
		// storageDir.mkdirs();
		// }
		// String name = new Date().toString() + ".jpg";
		// file = new File(storageDir, name);
		// FileOutputStream out = new FileOutputStream(file);
		// bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
		// imagesPathList.add(file.getAbsolutePath());
		// mGridView.setAdapter(imageAdapter);
		// imageAdapter.notifyDataSetChanged();
		// out.flush();
		// out.close();
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		// }
		//
		// @Override
		// public void onBitmapFailed(Drawable errorDrawable) {
		// Log.e("Bitmap Load Failed", " " + errorDrawable.toString());
		// }
		//
		// @Override
		// public void onPrepareLoad(Drawable placeHolderDrawable) {
		// Log.e("onPrepareLoad", " " + placeHolderDrawable.toString());
		// }
		// });
		// }
	}
}
