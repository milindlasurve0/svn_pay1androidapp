package com.mindsarray.pay1;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.RechargeMainAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.constant.OperatorConstant;
import com.mindsarray.pay1.databasehandler.MostVisitedOperator;
import com.mindsarray.pay1.databasehandler.MostVisitedOperatorDataSource;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.GridMenuItem;
import com.mindsarray.pay1.utilities.Utility;

public class AllQuickRechargeActivity extends Activity {
	Context mContext;
	// GridView mobileGridView,dthGridView,mobileBillGridView,mostUsedGridView;
	// ArrayList<GridMenuItem> gridArray = new ArrayList<GridMenuItem>();
	ArrayList<GridMenuItem> gridArray = new ArrayList<GridMenuItem>();
	RechargeMainAdapter customGridAdapter;
	Dialog dialog;
	List<MostVisitedOperator> mostVisitedOperators;
	EditText editRecharge;
	GridView mainGrid;
	TextView textHint;
	ArrayList<NameValuePair> listValuePair;
	TextView textMobile, textDTH, textMobileBill, textMostUsed;
	Button btnRecharge;
	String[] split;
	String stv;
	String method;
	String productName;
	int positionList;

	String rechargeType;
	String product_id = "", sub_id = "", mobile_no = "", amount = "";
	long timestamp;
	String uuid;
	boolean bNetOrSms;
	String special;
	int recType;
	String operatorName;
	private SharedPreferences mSettingsData;
	MostVisitedOperatorDataSource mostVisitedOperatorDataSource;
	private static final String TAG = "Quick Recharge";
//	public static GoogleAnalytics analytics; public static Tracker tracker;
	public static GoogleAnalytics analytics;
	public static Tracker tracker;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.quick_layout);
		mContext = AllQuickRechargeActivity.this;
		
//		tracker = Constants.setAnalyticsTracker(this, tracker);
//		//easyTracker.set(Fields.SCREEN_NAME, TAG);
		//analytics = GoogleAnalytics.getInstance(this);
		tracker = Constants.setAnalyticsTracker(this, tracker);
		
		editRecharge = (EditText) findViewById(R.id.editRecharge);
		btnRecharge = (Button) findViewById(R.id.btnRecharge);
		textHint = (TextView) findViewById(R.id.textHint);
		textMostUsed = (TextView) findViewById(R.id.textMostUsed);
		listValuePair = new ArrayList<NameValuePair>();
		textMobile = (TextView) findViewById(R.id.textMobile);
		textMobileBill = (TextView) findViewById(R.id.textMobileBill);
		mSettingsData = getSharedPreferences(SettingsSmsActivity.SETTINGS_NAME,
				0);

		mostVisitedOperatorDataSource = new MostVisitedOperatorDataSource(
				mContext);

		try {
			TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
			uuid = tManager.getDeviceId();
			if (uuid == null) {
				uuid = Secure.getString(
						AllQuickRechargeActivity.this.getContentResolver(),
						Secure.ANDROID_ID);
			}
			if (Utility.getUUID(AllQuickRechargeActivity.this,
					Constants.SHAREDPREFERENCE_UUID) == null)
				Utility.setUUID(AllQuickRechargeActivity.this,
						Constants.SHAREDPREFERENCE_UUID, uuid);
		} catch (Exception exception) {
		}

		bNetOrSms = mSettingsData.getBoolean(SettingsSmsActivity.KEY_NETWORK,
				true);
		textDTH = (TextView) findViewById(R.id.textDTH);
		textMobileBill.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SetMobileBillGrid(v);
			}

		});

		textDTH.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SetDthGrid(v);
			}

		});

		textMostUsed.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// SetMobileGrid(v);
				SetMostUsedGrid(v);
			}

		});

		textMobile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SetMobileGrid(v);
			}

		});

		btnRecharge.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// TODO Auto-generated method stub
				try {
					bNetOrSms = mSettingsData.getBoolean(
							SettingsSmsActivity.KEY_NETWORK, true);
					if (bNetOrSms) {
						if (!Utility.getLoginFlag(
								AllQuickRechargeActivity.this,
								Constants.SHAREDPREFERENCE_IS_LOGIN)) {
							Intent intent = new Intent(
									AllQuickRechargeActivity.this,
									LoginActivity.class);
							startActivity(intent);
							return;
						}
					}
					final String foo = editRecharge.getText().toString();
					if (foo.length() == 0) {
						Constants.showOneButtonFailureDialog(
								AllQuickRechargeActivity.this,
								"Please select operator.", TAG,
								Constants.OTHER_ERROR);
						return;
					}
					if (foo.contains("#")) {

						special = "1";
					} else {

						special = "0";
					}
					split = foo.split(Pattern.quote("*"));
					timestamp = System.currentTimeMillis();
					/*
					 * StringBuilder sb = new StringBuilder(); for (int i = 0; i
					 * < split.length; i++) { sb.append(split[i+1]); if (i !=
					 * split.length - 1) { sb.append(" "); } }
					 */

					// String rechargeString =
					// editRecharge.getText()
					// .toString();
					// for (int i = 0; i < rechargeString.length(); i++)
					// if (rechargeString.charAt(i) == '*')
					// count++;

					product_id = split[1].length() != 0 ? split[1] : "";
					operatorName = Constants
							.rechargeTypeOperatorName(product_id);
					if (product_id.equals("")) {
						Constants.showOneButtonFailureDialog(
								AllQuickRechargeActivity.this,
								"Please select operator.", TAG,
								Constants.OTHER_ERROR);
						return;
					}
					int size = split.length;
					if (size == 5) {
						recType = Constants.RECHARGE_DTH;
					} else if (size == 4) {
						if (Integer.parseInt(product_id) < 36) {
							recType = Constants.RECHARGE_MOBILE;
						} else {
							recType = Constants.BILL_PAYMENT;
						}
					} else {

					}

					if (size < 4) {
						Constants.showOneButtonFailureDialog(
								AllQuickRechargeActivity.this,
								"Please enter correct recharge format.", TAG,
								Constants.OTHER_ERROR);
						return;
					}

					if (recType == Constants.RECHARGE_MOBILE) {
						rechargeType = String
								.valueOf(Constants.RECHARGE_MOBILE);
						mobile_no = split[2].length() == 10 ? split[2] : "";
						amount = split[3].indexOf("#") != -1 ? split[3]
								.substring(0, split[3].indexOf("#")) : split[3];
						amount = amount.length() != 0 ? amount : "";

					} else if (recType == Constants.RECHARGE_DTH) {
						rechargeType = String.valueOf(Constants.RECHARGE_DTH);
						if (size < 5) {
							Constants.showOneButtonFailureDialog(
									AllQuickRechargeActivity.this,
									"Please enter correct recharge format.",
									"Quick Recharge", Constants.OTHER_ERROR);
							return;
						}
						mobile_no = split[2].length() != 0 ? split[2] : "";
						sub_id = split[3].length() == 10 ? split[3] : "";
						amount = split[4].indexOf("#") != -1 ? split[4]
								.substring(0, split[4].indexOf("#")) : split[4];
						amount = amount.length() != 0 ? amount : "";

						if (sub_id.equals("")) {
							Constants.showOneButtonFailureDialog(
									AllQuickRechargeActivity.this,
									"Please enter sub id.", TAG,
									Constants.OTHER_ERROR);
							return;
						}

					} else if (gridArray.get(positionList).getmRechargeType() == Constants.RECHARGE_ENTERTAINMENT) {
						rechargeType = String
								.valueOf(Constants.RECHARGE_ENTERTAINMENT);
						mobile_no = split[2].length() == 10 ? split[2] : "";
						amount = split[3].indexOf("#") != -1 ? split[3]
								.substring(0, split[3].indexOf("#")) : split[3];
						amount = amount.length() != 0 ? amount : "";

						method = "vasRecharge";
					} else if (recType == Constants.BILL_PAYMENT) {
						rechargeType = String.valueOf(Constants.BILL_PAYMENT);
						mobile_no = split[2].length() == 10 ? split[2] : "";
						amount = split[3].indexOf("#") != -1 ? split[3]
								.substring(0, split[3].indexOf("#")) : split[3];
						amount = amount.length() != 0 ? amount : "";

					}

					if (bNetOrSms) {
						if (RequestClass.getInstance() == null) {
							Intent intent = new Intent(
									AllQuickRechargeActivity.this,
									LoginActivity.class);
							startActivity(intent);
						} else {

							/*
							 * int size = split.length; // String rechargeString
							 * = // editRecharge.getText() // .toString(); //
							 * for (int i = 0; i < rechargeString.length(); i++)
							 * // if (rechargeString.charAt(i) == '*') //
							 * count++;
							 * 
							 * if (size < 4) { Utility.alertBoxShow(
							 * AllQuickRechargeActivity.this,
							 * "Please enter correct recharge format."); return;
							 * } product_id = split[1].length() != 0 ? split[1]
							 * : "";
							 * 
							 * if (product_id.equals("")) {
							 * Utility.alertBoxShow(
							 * AllQuickRechargeActivity.this,
							 * "Please select operator"); return; } if
							 * (gridArray.get(positionList).getmRechargeType()
							 * == Constants.RECHARGE_MOBILE) { rechargeType =
							 * String .valueOf(Constants.RECHARGE_MOBILE);
							 * mobile_no = split[2].length() == 10 ? split[2] :
							 * ""; amount = split[3].indexOf("#") != -1 ?
							 * split[3] .substring(0, split[3].indexOf("#")) :
							 * split[3]; amount = amount.length() != 0 ? amount
							 * : "";
							 * 
							 * } else if (gridArray.get(positionList)
							 * .getmRechargeType() == Constants.RECHARGE_DTH) {
							 * rechargeType = String
							 * .valueOf(Constants.RECHARGE_DTH); if (size < 5) {
							 * Utility.alertBoxShow(
							 * AllQuickRechargeActivity.this,
							 * "Please enter correct recharge format."); return;
							 * } sub_id = split[2].length() != 0 ? split[2] :
							 * ""; mobile_no = split[3].length() == 10 ?
							 * split[3] : ""; amount = split[4].indexOf("#") !=
							 * -1 ? split[4] .substring(0,
							 * split[4].indexOf("#")) : split[4]; amount =
							 * amount.length() != 0 ? amount : "";
							 * 
							 * if (sub_id.equals("")) { Utility.alertBoxShow(
							 * AllQuickRechargeActivity.this,
							 * "Please enter subscriber id"); return; }
							 * 
							 * } else if (gridArray.get(positionList)
							 * .getmRechargeType() ==
							 * Constants.RECHARGE_ENTERTAINMENT) { rechargeType
							 * = String
							 * .valueOf(Constants.RECHARGE_ENTERTAINMENT);
							 * mobile_no = split[2].length() == 10 ? split[2] :
							 * ""; amount = split[3].indexOf("#") != -1 ?
							 * split[3] .substring(0, split[3].indexOf("#")) :
							 * split[3]; amount = amount.length() != 0 ? amount
							 * : "";
							 * 
							 * method = "vasRecharge"; } else if
							 * (gridArray.get(positionList) .getmRechargeType()
							 * == Constants.BILL_PAYMENT) { rechargeType =
							 * String .valueOf(Constants.BILL_PAYMENT);
							 * mobile_no = split[2].length() == 10 ? split[2] :
							 * ""; amount = split[3].indexOf("#") != -1 ?
							 * split[3] .substring(0, split[3].indexOf("#")) :
							 * split[3]; amount = amount.length() != 0 ? amount
							 * : "";
							 * 
							 * }
							 */
							if (mobile_no.equals("")) {
								Constants.showOneButtonFailureDialog(
										AllQuickRechargeActivity.this,
										Constants.ERROR_MOBILE_LENGTH_FIELD,
										TAG, Constants.OTHER_ERROR);
								return;
							} else if (amount.equals("")) {
								Constants.showOneButtonFailureDialog(
										AllQuickRechargeActivity.this,
										Constants.ERROR_AMOUNT_BLANK_FIELD,
										TAG, Constants.OTHER_ERROR);
								return;
							} else {

								if (recType == Constants.RECHARGE_MOBILE) {

									listValuePair.add(new BasicNameValuePair(
											"method", "mobRecharge"));
									listValuePair.add(new BasicNameValuePair(
											"operator", product_id));
									listValuePair.add(new BasicNameValuePair(
											"mobileNumber", mobile_no));
									listValuePair.add(new BasicNameValuePair(
											"subId", mobile_no));
									listValuePair.add(new BasicNameValuePair(
											"amount", amount));

								} else if (recType == Constants.RECHARGE_DTH) {

									listValuePair.add(new BasicNameValuePair(
											"method", "dthRecharge"));
									listValuePair.add(new BasicNameValuePair(
											"operator",
											""
													+ (Integer
															.parseInt(product_id) - 15)));
									listValuePair.add(new BasicNameValuePair(
											"mobileNumber", mobile_no));
									listValuePair.add(new BasicNameValuePair(
											"subId", sub_id));
									listValuePair.add(new BasicNameValuePair(
											"amount", amount));

								} else if (recType == Constants.RECHARGE_ENTERTAINMENT) {

									// method = "vasRecharge";

									listValuePair.add(new BasicNameValuePair(
											"method", "vasRecharge"));
									listValuePair.add(new BasicNameValuePair(
											"product", product_id));
									listValuePair.add(new BasicNameValuePair(
											"Mobile", mobile_no));

								} else if (recType == Constants.BILL_PAYMENT) {

									listValuePair.add(new BasicNameValuePair(
											"method", "mobBillPayment"));
									listValuePair.add(new BasicNameValuePair(
											"operator", product_id));
									listValuePair.add(new BasicNameValuePair(
											"mobileNumber", mobile_no));
									listValuePair.add(new BasicNameValuePair(
											"subId", mobile_no));
									listValuePair.add(new BasicNameValuePair(
											"amount", amount));

								}

								if (foo.contains("#")) {
									listValuePair.add(new BasicNameValuePair(
											"special", "1"));
									special = "1";
								} else {
									listValuePair.add(new BasicNameValuePair(
											"special", "0"));
									special = "0";
								}

								String dialogString = null;

								if (recType == Constants.RECHARGE_MOBILE
										|| recType == Constants.BILL_PAYMENT) {

									dialogString = "Operator: "
											+ operatorName
											+ "\nMobile Number: "
											+ mobile_no
											+ "\nAmount: "
											+ amount
											+ "\nAre you sure you want to Recharge?";
								} else if (recType == Constants.RECHARGE_DTH) {
									dialogString = "Opeartor: "
											+ operatorName
											+ "\nSubscriberId: "
											+ sub_id
											+ "\nMobile Number: "
											+ mobile_no
											+ "Amount: "
											+ amount
											+ "\nAre you sure you want to Recharge?";
								}

								new AlertDialog.Builder(
										AllQuickRechargeActivity.this)
										.setTitle("Recharge")
										.setMessage(dialogString)
										.setPositiveButton(
												"Yes",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int which) {

														new RechargeTask()
																.execute();
													}
												})
										.setNegativeButton(
												"No",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int which) {

													}
												}).show();

							}
						}
					} else {

						String dialogString = null;

						if (recType == Constants.RECHARGE_MOBILE
								|| recType == Constants.BILL_PAYMENT) {

							dialogString = "Operator: " + operatorName
									+ "\nMobile Number: " + mobile_no
									+ "\nAmount: " + amount
									+ "\nAre you sure you want to Recharge?";
						} else if (recType == Constants.RECHARGE_DTH) {
							dialogString = "Opeartor: " + operatorName
									+ "\nSubscriberId: " + sub_id
									+ "\nMobile Number: " + mobile_no
									+ "Amount: " + amount
									+ "\nAre you sure you want to Recharge?";
						}

						new AlertDialog.Builder(AllQuickRechargeActivity.this)
								.setTitle("Recharge (SMS)")
								.setMessage(dialogString)
								.setPositiveButton("Yes",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int which) {

												// String rechargeString =
												// editRecharge
												// .getText().toString()
												// .trim();
												Constants
														.sendSMSMessage(
																mContext,
																recType,
																Integer.parseInt(product_id),
																sub_id,
																mobile_no,
																amount,
																special,
																operatorName);
												editRecharge.setText("");

												// finish();
											}
										})
								.setNegativeButton("No",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int which) {

											}
										}).show();

						/*
						 * String rechargeString = editRecharge.getText()
						 * .toString().trim();
						 * Constants.sendSMSMessage(mContext, recType,
						 * Integer.parseInt(product_id), sub_id, mobile_no, foo,
						 * rechargeString, operatorName);
						 */
					}

				} catch (Exception e) {
					// Log.d("// Log", "// Log");
				}

			}
		});

	}

	protected void SetMostUsedGrid(View v) {
		// TODO Auto-generated method stub
		int[] posXY = new int[2];
		v.getLocationOnScreen(posXY);
		int ax = posXY[0];
		int ay = posXY[1];
		gridArray.clear();

		try {
			mostVisitedOperatorDataSource.open();
			// for (int i = 0; i < 20; i++)
			// notificationDataSource.createNotification(
			// "123456897987wdasdasdasdasd asd46asd4as4d6 ", 1,
			// System.currentTimeMillis() + "");
			mostVisitedOperators = mostVisitedOperatorDataSource.getFirstFive();

			for (int i = 0; i < mostVisitedOperators.size(); i++) {
				int op_code = mostVisitedOperators.get(i).getOperatorId();
				gridArray.add(OperatorConstant.getGridItem(
						AllQuickRechargeActivity.this, op_code));
			}
		} catch (SQLException exception) {
			// TODO: handle exception
			// // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			// // Log.e("Er ", e.getMessage());
		} finally {
			mostVisitedOperatorDataSource.close();
		}

		dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.sample);

		GridView gridView = (GridView) dialog.findViewById(R.id.gridView1);
		WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
		wmlp.gravity = Gravity.TOP | Gravity.LEFT;
		wmlp.x = ax + 50; // x position
		wmlp.y = ay; // y position
		dialog.getWindow().setAttributes(wmlp);
		if (gridArray.size() == 0) {
			Constants.showOneButtonFailureDialog(mContext, "No Records Found",
					"Quick recharge", Constants.QUICK_SETTINGS);
		} else {
			customGridAdapter = new RechargeMainAdapter(
					AllQuickRechargeActivity.this,
					R.layout.quick_dialog_layout, gridArray,false);

			// planJson =
			// Constants.loadJSONFromAsset(AllAllQuickRechargeActivity.this,
			// "plans.json");
			gridView.setAdapter(customGridAdapter);

			gridView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					// Constants.showCustomToast(MobileRechargeActivity.this,
					// gridArray.get(position).getId()+"",
					// Toast.LENGTH_LONG).show();
					try {
						editRecharge.setText("*"
								+ gridArray.get(position).getId() + "*");
						editRecharge.setSelection(editRecharge.getText()
								.length());
						InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
						textHint.setText("*OperatorCode*MobNo*Amount");
						productName = gridArray.get(position).getTitle();
						recType = gridArray.get(position).getmRechargeType();
						dialog.dismiss();
					} catch (Exception e) {
						// e.printStackTrace();
					}

				}

			});

			dialog.show();
		}
	}

	private void SetMobileBillGrid(View v) {

		// TODO Auto-generated method stub
		int[] posXY = new int[2];
		v.getLocationOnScreen(posXY);
		int ax = posXY[0];
		int ay = posXY[1];
		gridArray.clear();

		dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.sample);

		GridView gridView = (GridView) dialog.findViewById(R.id.gridView1);
		WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
		wmlp.gravity = Gravity.TOP | Gravity.LEFT;
		wmlp.x = ax + 50; // x position
		wmlp.y = ay + 50; // y position
		dialog.getWindow().setAttributes(wmlp);
		if (gridArray.size() == 0) {
			int mAirtel = R.drawable.m_airtel;
			int mVodafone = R.drawable.m_vodafone;
			int mIdea = R.drawable.m_idea;
			//int mLoop = R.drawable.m_loop;
			int mTataDocomo = R.drawable.m_docomo;
			int mTataIndicom = R.drawable.m_indicom;
			int mreliance = R.drawable.m_reliance;

			gridArray.add(new GridMenuItem(mTataDocomo, "Tata Docomo",
					Constants.OPERATOR_BILL_DOCOMO, Constants.RECHARGE_MOBILE));
			/*gridArray.add(new GridMenuItem(mLoop, "Loop",
					Constants.OPERATOR_BILL_LOOP, Constants.RECHARGE_MOBILE));*/
			gridArray
					.add(new GridMenuItem(mAirtel, "Cellone",
							Constants.OPERATOR_BILL_CELLONE,
							Constants.RECHARGE_MOBILE));
			gridArray.add(new GridMenuItem(mIdea, "Idea",
					Constants.OPERATOR_BILL_IDEA, Constants.RECHARGE_MOBILE));
			gridArray.add(new GridMenuItem(mTataIndicom, "Tata TeleServices",
					Constants.OPERATOR_BILL_TATATELESERVICE,
					Constants.RECHARGE_MOBILE));
			gridArray.add(new GridMenuItem(mreliance, "Tata TeleServices",
					Constants.OPERATOR_BILL_RELIANCE,
					Constants.RECHARGE_MOBILE));
			
			gridArray
					.add(new GridMenuItem(mVodafone, "Vodafone",
							Constants.OPERATOR_BILL_VODAFONE,
							Constants.RECHARGE_MOBILE));

		}

		customGridAdapter = new RechargeMainAdapter(
				AllQuickRechargeActivity.this, R.layout.quick_dialog_layout,
				gridArray,false);

		// planJson =
		// Constants.loadJSONFromAsset(AllAllQuickRechargeActivity.this,
		// "plans.json");
		gridView.setAdapter(customGridAdapter);

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// Constants.showCustomToast(MobileRechargeActivity.this,
				// gridArray.get(position).getId()+"",
				// Toast.LENGTH_LONG).show();
				try {
					editRecharge.setText("*" + gridArray.get(position).getId()
							+ "*");
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
					editRecharge.setSelection(editRecharge.getText().length());
					textHint.setText("*OperatorCode*MobNo*Amount");
					productName = gridArray.get(position).getTitle();

					recType = gridArray.get(position).getmRechargeType();
					dialog.dismiss();
					// operatorName=gridArray.get(position).getTitle();
				} catch (Exception e) {
					// e.printStackTrace();
				}

			}

		});
		dialog.show();

	}

	private void SetDthGrid(View v) {
		// TODO Auto-generated method stub
		int[] posXY = new int[2];
		v.getLocationOnScreen(posXY);
		int ax = posXY[0];
		int ay = posXY[1];
		gridArray.clear();

		dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.sample);

		GridView gridView = (GridView) dialog.findViewById(R.id.gridView1);
		WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
		wmlp.gravity = Gravity.TOP | Gravity.LEFT;
		wmlp.x = ax + 50; // x position
		wmlp.y = ay; // y position
		if (gridArray.size() == 0) {
			int mAirtel = R.drawable.d_dishtv;
			int mVodafone = R.drawable.d_tatasky;
			int mBsnl = R.drawable.d_sundirect;
			int mIdea = R.drawable.d_bigtv;
			int mAircel = R.drawable.m_airtel;
			int mRelainceCdma = R.drawable.d_videocon;

			gridArray.add(new GridMenuItem(mAirtel, "Dish TV",
					Constants.OPERATOR_DTH_DISHTV, Constants.RECHARGE_DTH));
			gridArray.add(new GridMenuItem(mVodafone, "Tata Sky",
					Constants.OPERATOR_DTH_TATA_SKY, Constants.RECHARGE_DTH));
			gridArray.add(new GridMenuItem(mBsnl, "Sun Direct",
					Constants.OPERATOR_DTH_SUN, Constants.RECHARGE_DTH));
			gridArray.add(new GridMenuItem(mIdea, "Big TV",
					Constants.OPERATOR_DTH_BIG, Constants.RECHARGE_DTH));
			gridArray.add(new GridMenuItem(mAircel, "AirTel",
					Constants.OPERATOR_DTH_AIRTEL, Constants.RECHARGE_DTH));
			gridArray.add(new GridMenuItem(mRelainceCdma, "Videocon",
					Constants.OPERATOR_DTH_VIDEOCON, Constants.RECHARGE_DTH));

		}

		customGridAdapter = new RechargeMainAdapter(
				AllQuickRechargeActivity.this, R.layout.quick_dialog_layout,
				gridArray,false);

		// planJson =
		// Constants.loadJSONFromAsset(AllAllQuickRechargeActivity.this,
		// "plans.json");
		gridView.setAdapter(customGridAdapter);

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// Constants.showCustomToast(MobileRechargeActivity.this,
				// gridArray.get(position).getId()+"",
				// Toast.LENGTH_LONG).show();
				try {
					editRecharge.setText("*" + gridArray.get(position).getId()
							+ "*");
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
					editRecharge.setSelection(editRecharge.getText().length());
					textHint.setText("*OperatorCode*SubId*MobNo*Amount.");
					productName = gridArray.get(position).getTitle();
					recType = gridArray.get(position).getmRechargeType();
					dialog.dismiss();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

		});
		dialog.show();
	}

	public void SetMobileGrid(View v) {

		int[] posXY = new int[2];
		v.getLocationOnScreen(posXY);
		int ax = posXY[0];
		int ay = posXY[1];

		gridArray.clear();
		dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.sample);

		GridView gridView = (GridView) dialog.findViewById(R.id.gridView1);
		WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
		wmlp.gravity = Gravity.TOP | Gravity.LEFT;
		wmlp.x = ax + 50; // x position
		wmlp.y = ay; // y position

		if (gridArray.size() == 0) {
			int mAirtel = R.drawable.m_airtel;
			int mVodafone = R.drawable.m_vodafone;
			int mBsnl = R.drawable.m_bsnl;
			int mIdea = R.drawable.m_idea;
			int mAircel = R.drawable.m_aircel;
			int mRelainceCdma = R.drawable.m_reliance_cdma;
			int mRelainceGsm = R.drawable.m_reliance;
			//int mLoop = R.drawable.m_loop;
			int mUninor = R.drawable.m_uninor;
			int mTataDocomo = R.drawable.m_docomo;
			int mTataIndicom = R.drawable.m_indicom;
			int mMTS = R.drawable.m_mts;
			int mVideocon = R.drawable.m_videocon;
			int mMTNL = R.drawable.m_mtnl;

			gridArray.add(new GridMenuItem(mAirtel, "Airtel",
					Constants.OPERATOR_AIRTEL, Constants.RECHARGE_MOBILE));
			gridArray.add(new GridMenuItem(mVodafone, "Vodafone",
					Constants.OPERATOR_VODAFONE, Constants.RECHARGE_MOBILE));
			gridArray.add(new GridMenuItem(mBsnl, "BSNL",
					Constants.OPERATOR_BSNL, Constants.RECHARGE_MOBILE));
			gridArray.add(new GridMenuItem(mIdea, "Idea",
					Constants.OPERATOR_IDEA, Constants.RECHARGE_MOBILE));
			gridArray.add(new GridMenuItem(mAircel, "Aircel",
					Constants.OPERATOR_AIRCEL, Constants.RECHARGE_MOBILE));
			gridArray
					.add(new GridMenuItem(mRelainceCdma, "Reliance CDMA",
							Constants.OPERATOR_RELIANCE_CDMA,
							Constants.RECHARGE_MOBILE));
			gridArray
					.add(new GridMenuItem(mRelainceGsm, "Reliance GSM",
							Constants.OPERATOR_RELIANCE_GSM,
							Constants.RECHARGE_MOBILE));
			/*gridArray.add(new GridMenuItem(mLoop, "Loop",
					Constants.OPERATOR_LOOP, Constants.RECHARGE_MOBILE));*/
			gridArray.add(new GridMenuItem(mUninor, "Uninor",
					Constants.OPERATOR_UNINOR, Constants.RECHARGE_MOBILE));
			gridArray.add(new GridMenuItem(mTataDocomo, "Tata Docomo",
					Constants.OPERATOR_DOCOMO, Constants.RECHARGE_MOBILE));
			gridArray
					.add(new GridMenuItem(mTataIndicom, "Tata Indicom",
							Constants.OPERATOR_TATA_INDICOM,
							Constants.RECHARGE_MOBILE));
			gridArray.add(new GridMenuItem(mMTS, "MTS", Constants.OPERATOR_MTS,
					Constants.RECHARGE_MOBILE));
			gridArray.add(new GridMenuItem(mVideocon, "Videocon",
					Constants.OPERATOR_VIDEOCON, Constants.RECHARGE_MOBILE));
			gridArray.add(new GridMenuItem(mMTNL, "MTNL",
					Constants.OPERATOR_MTNL, Constants.RECHARGE_MOBILE));

		}

		customGridAdapter = new RechargeMainAdapter(
				AllQuickRechargeActivity.this, R.layout.quick_dialog_layout,
				gridArray,false);

		// planJson =
		// Constants.loadJSONFromAsset(AllAllQuickRechargeActivity.this,
		// "plans.json");
		gridView.setAdapter(customGridAdapter);

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				editRecharge.setText("*" + gridArray.get(position).getId()
						+ "*");
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
				editRecharge.setSelection(editRecharge.getText().length());
				textHint.setText("*OperatorCode*MobNo*Amount.  Put # if STV at the end.");
				productName = gridArray.get(position).getTitle();
				recType = gridArray.get(position).getmRechargeType();
				dialog.dismiss();
			}

		});
		dialog.show();

	}

	public class RechargeTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {

			listValuePair.add(new BasicNameValuePair("type", "flexi"));
			listValuePair.add(new BasicNameValuePair("circle", ""));
			listValuePair.add(new BasicNameValuePair("timestamp", String
					.valueOf(timestamp)));
			listValuePair.add(new BasicNameValuePair("profile_id", Utility
					.getProfileID(AllQuickRechargeActivity.this,
							Constants.SHAREDPREFERENCE_PROFILE_ID)));
			if (recType == Constants.RECHARGE_DTH) {
				listValuePair.add(new BasicNameValuePair("hash_code", Constants
						.encryptPassword(Utility.getUUID(
								AllQuickRechargeActivity.this,
								Constants.SHAREDPREFERENCE_UUID)
								+ sub_id + amount + timestamp)));
			} else {
				listValuePair.add(new BasicNameValuePair("hash_code", Constants
						.encryptPassword(Utility.getUUID(
								AllQuickRechargeActivity.this,
								Constants.SHAREDPREFERENCE_UUID)
								+ mobile_no + amount + timestamp)));
			}

			listValuePair.add(new BasicNameValuePair("device_type", "android"));

			String response = RequestClass.getInstance().readPay1Request(
					AllQuickRechargeActivity.this, Constants.API_URL,
					listValuePair);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (dialog.isShowing()) {
				dialog.dismiss();
			}

			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = array.getJSONObject(0);
					// String rechargeString = null;
					try {
						mostVisitedOperatorDataSource.open();
						// for (int i = 0; i < 20; i++)
						// notificationDataSource.createNotification(
						// "123456897987wdasdasdasdasd asd46asd4as4d6 ", 1,
						// System.currentTimeMillis() + "");
						/*
						 * notificationsList =
						 * mostVisitedOperatorDataSource.getAllNotifications(
						 * PAGE_NUMBER++, 10);
						 */
						if (mostVisitedOperatorDataSource.checkCount(Integer
								.parseInt(product_id)) == 0) {
							mostVisitedOperatorDataSource.createMVO(
									Integer.parseInt(product_id), product_id,
									productName, rechargeType);
						} else {
							mostVisitedOperatorDataSource.updateRow(Integer
									.parseInt(product_id));
						}
					} catch (SQLException exception) {
						// TODO: handle exception
						// Log.e("Er ", exception.getMessage());
					} catch (Exception e) {
						// TODO: handle exception
						// // Log.e("Er ", e.getMessage());
					} finally {
						mostVisitedOperatorDataSource.close();
					}

					// if (recType == Constants.RECHARGE_DTH) {
					// rechargeString = Constants.getRechargeString(
					// AllQuickRechargeActivity.this,
					// Constants.RECHARGE_DTH,
					// Integer.parseInt(product_id), sub_id,
					// mobile_no, amount, "0");
					// } else {
					// rechargeString = Constants.getRechargeString(
					// AllQuickRechargeActivity.this,
					// Constants.RECHARGE_MOBILE,
					// Integer.parseInt(product_id), mobile_no,
					// mobile_no, amount, "0");
					// }

					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						String balance = jsonObject.getString("balance");
						Utility.setBalance(AllQuickRechargeActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE, balance);
						editRecharge.setText("");
						Constants.showOneButtonSuccessDialog(mContext,
								"Recharge request sent successfully.",
								Constants.QUICK_SETTINGS);
						/*
						 * Constants.showCustomToast(AllQuickRechargeActivity.this
						 * , "Recharge request sent successfully.");
						 */
						// mostVisitedOperatorDataSource.createMVO(Integer.parseInt(product_id),
						// product_id, productName, rechargeType);
						// visitedOperatorDataSource.createMVO(mOperatorId,
						// String.valueOf(mOperatorId),
						// bundle.getString(Constants.OPERATOR_NAME),
						// String.valueOf(Constants.RECHARGE_MOBILE));

					} else {
						Constants.showOneButtonFailureDialog(
								AllQuickRechargeActivity.this,
								Constants.checkCode(replaced), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(
							AllQuickRechargeActivity.this,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(
						AllQuickRechargeActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(
						AllQuickRechargeActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(AllQuickRechargeActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							RechargeTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			RechargeTask.this.cancel(true);
			dialog.cancel();
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants.showNavigationBar(AllQuickRechargeActivity.this);
		super.onResume();
	}
	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
		// EasyTracker.getInstance(this).activityStart(this);
//		//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
		//tracker.setScreenName(TAG);
		Constants.trackOnStart(tracker, TAG);
			//.build());
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}
}
