package com.mindsarray.pay1;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;

public class ConsumerAppActivity extends Activity {
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.consumer_app_activity);
		
		RelativeLayout pay1DownloadView = (RelativeLayout) findViewById(R.id.pay1_consumer_download);
		pay1DownloadView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("market://details?id=com.pay1"));
				startActivity(intent);
			}
		});
		
		Button payDownloadButton = (Button) findViewById(R.id.pay1_consumer_download_button);
		payDownloadButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("market://details?id=com.pay1"));
				startActivity(intent);
			}
		});
	}	
}