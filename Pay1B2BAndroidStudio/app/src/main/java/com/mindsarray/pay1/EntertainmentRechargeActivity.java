package com.mindsarray.pay1;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

public class EntertainmentRechargeActivity extends Activity {
	TextView textTitle;
	EditText editMobileNumber;
	Button mBtnRchNow;
	LinearLayout linearRechargeLayout, linearRootLayout;
	EditText editText;
	JSONArray paramArray;
	JSONObject object;
	String field;
	String subUrl = "";
	String smsCode = "*";
	boolean bNetOrSms;
	boolean performTask = true;
	private SharedPreferences mSettingsData;// Shared preferences for setting
											// data
	private static final String TAG = "Entertainment";
	
	public static GoogleAnalytics analytics;
	public static Tracker tracker;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.entertainmentrecharge_activity);
		findViewById();
//		 tracker = Constants.setAnalyticsTracker(this, tracker);
		//analytics = GoogleAnalytics.getInstance(this);
		tracker = Constants.setAnalyticsTracker(this, tracker);
		mSettingsData = getSharedPreferences(SettingsSmsActivity.SETTINGS_NAME,
				0);
		bNetOrSms = mSettingsData.getBoolean(SettingsSmsActivity.KEY_NETWORK,
				true);

		/*
		 * bNetOrSms = Utility.getSharedPreferencesBoolean(
		 * EntRechargeActivity.this, Constants.NETWORK);
		 */
		textTitle.setText(getIntent().getExtras().getString(
				Constants.PRODUCT_NAME));

		String productDetails = getIntent().getExtras().getString(
				Constants.PRODUCT_DETAILS);
		addViews(productDetails);
		mBtnRchNow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				performTask = true;
				for (int i = 0; i < paramArray.length(); i++) {
					try {
						object = paramArray.getJSONObject(i);
						field = object.getString("field");
					} catch (JSONException e) {
						// e.printStackTrace();
					}
					if (field.equalsIgnoreCase("Mobile")) {
						if (((EditText) findViewById(i)).getText().toString()
								.length() != 10) {
							// Constants.showCustomToast(
							// EntertainmentRechargeActivity.this,
							// "Please check your number");
							performTask = false;
						} else {
							subUrl = subUrl
									+ "Mobile="
									+ ((EditText) findViewById(i)).getText()
											.toString();
							// new EntertainmentTask().execute();
						}
					} else {
						if (((EditText) findViewById(i)).getText().toString()
								.length() == 0) {
							// Constants.showCustomToast(
							// EntertainmentRechargeActivity.this,
							// "Please check your " + field);
							performTask = false;
						} else {
							if (bNetOrSms) {
								subUrl = subUrl
										+ "&"
										+ field
										+ "="
										+ ((EditText) findViewById(i))
												.getText().toString();
							} else {
								subUrl = subUrl
										+ "*"
										+ ((EditText) findViewById(i))
												.getText().toString();
								// Log.d("Tag", subUrl);
							}
						}
					}

				}

				if (performTask) {
					if (bNetOrSms) {
						new EntertainmentTask().execute();
					} else {
						Constants.senDirectSms(
								EntertainmentRechargeActivity.this, subUrl);
					}
				} else {
					// Constants.showCustomToast(
					// EntertainmentRechargeActivity.this,
					// "Please check your entries.");
				}
			}
		});
	}
//	public static GoogleAnalytics analytics; public static Tracker tracker;	
	@Override
		public void onStart() {
			super.onStart();
			// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
			// EasyTracker.getInstance(this).activityStart(this);
//			//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
			//tracker.setScreenName(TAG);
			Constants.trackOnStart(tracker, TAG);
				//.build());
		}
	private void addViews(String productDetails) {

		try {
			JSONObject jsonObject = new JSONObject(productDetails);
			JSONObject jparamsObject = jsonObject.getJSONObject("params");
			JSONObject allParamsObject = jparamsObject
					.getJSONObject("allParams");
			paramArray = allParamsObject.getJSONArray("param");
			for (int i = 0; i < paramArray.length(); i++) {
				object = paramArray.getJSONObject(i);
				field = object.getString("field");
				TextView textView = new TextView(
						EntertainmentRechargeActivity.this);
				textView.setLayoutParams(new LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT));
				editText = new EditText(EntertainmentRechargeActivity.this);
				editText.setLayoutParams(new LayoutParams(
						ViewGroup.LayoutParams.MATCH_PARENT,
						ViewGroup.LayoutParams.WRAP_CONTENT));
				editText.setHint(field);
				editText.setId(i);
				if (field.equalsIgnoreCase("Mobile")
						|| field.equalsIgnoreCase("Amount")) {
					editText.setInputType(InputType.TYPE_CLASS_NUMBER);
				}
				textView.setText(field);
				linearRechargeLayout.addView(textView);
				linearRechargeLayout.addView(editText);

			}

		} catch (JSONException e) {
			// e.printStackTrace();
		} catch (Exception e) {
			// e.printStackTrace();
		}
	}

	public class EntertainmentTask extends AsyncTask<String, String, String>
			implements OnDismissListener {

		private ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair.add(new BasicNameValuePair("method", "vasRecharge"));
			listValuePair.add(new BasicNameValuePair("product", getIntent()
					.getExtras().getString(Constants.PRODUCT_ID)));

			String[] pair_split = subUrl.split("&");
			String[] key_split = new String[pair_split.length];
			for (int i = 0; i < pair_split.length; i++) {
				key_split = pair_split[i].split("=");
				listValuePair.add(new BasicNameValuePair(key_split[0],
						key_split[1]));
			}

			String response = RequestClass.getInstance().readPay1Request(
					EntertainmentRechargeActivity.this, Constants.API_URL,
					listValuePair);
			subUrl = "";

			listValuePair.clear();
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					Utility.setSharedPreference(
							EntertainmentRechargeActivity.this,
							Constants.ENTERTAINMENT, replaced);

					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = array.getJSONObject(0);
					String status = jsonObject.getString("status");

					if (status.equalsIgnoreCase("success")) {
						// Constants.showCustomToast(
						// EntertainmentRechargeActivity.this,
						// "Recharge request sent successfully.");
						Constants.showOneButtonSuccessDialog(
								EntertainmentRechargeActivity.this,
								"Recharge request sent successfully.\nTransaction id : "
										+ jsonObject.getString("description"),
								Constants.RECHARGE_SETTINGS);
						String balance = jsonObject.getString("balance");
						Utility.setBalance(EntertainmentRechargeActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE, balance);
						// finish();
					} else {
						Constants.showOneButtonFailureDialog(
								EntertainmentRechargeActivity.this,
								Constants.checkCode(replaced), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(
							EntertainmentRechargeActivity.this,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(
						EntertainmentRechargeActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(
						EntertainmentRechargeActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(EntertainmentRechargeActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							EntertainmentTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			EntertainmentTask.this.cancel(true);
			dialog.cancel();
		}
	}

	private void findViewById() {
		linearRootLayout = (LinearLayout) findViewById(R.id.linearRootLayout);
		linearRechargeLayout = (LinearLayout) findViewById(R.id.linearRechargeLayout);
		textTitle = (TextView) findViewById(R.id.textTitle);
		editMobileNumber = (EditText) findViewById(R.id.editMobileNumber);
		mBtnRchNow = (Button) findViewById(R.id.mBtnRchNow);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

	}

	@Override
	protected void onResume() {
		Constants.showNavigationBar(EntertainmentRechargeActivity.this);
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}
}
