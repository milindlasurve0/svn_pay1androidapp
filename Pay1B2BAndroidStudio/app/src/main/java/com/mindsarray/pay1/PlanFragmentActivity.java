package com.mindsarray.pay1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.PlanFragmentAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.Plan;
import com.mindsarray.pay1.databasehandler.PlanDataSource;
import com.mindsarray.pay1.requesthandler.RequestClass;

public class PlanFragmentActivity extends FragmentActivity {

	PlanFragmentAdapter mAdapter;
	ViewPager mPager;
	PageIndicator mIndicator;
	TabPageIndicator indicator;
	ImageView imageView1;
	TextView textView_NoData;

	Spinner spinner_circle;
	ArrayList<JSONObject> circleArrayList, operatorArrayList;
	// Button btnGetPlans;
	List<String> circleList, operatorList;
	String circleArray, operatorArray, operatorId, planJson;
	String circleCode;
	JSONArray operatorJArray, jsonArray;
	String planType;
	PlanDataSource planDataSource;
	ArrayList<JSONObject> dataPlanArrayList;
	JSONObject jsonObject2;
	ArrayList<String> planCategoryList = new ArrayList<String>();
	RelativeLayout spinner_header;
	private static final String TAG = "Plan Fragement";
	int isDataCardPlan = 0;
	int flag = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.plan_fragment_activity);
		 tracker = Constants.setAnalyticsTracker(this, tracker);
		 //easyTracker.set(Fields.SCREEN_NAME, TAG);
		try {

			mPager = (ViewPager) findViewById(R.id.pager);
			mAdapter = new PlanFragmentAdapter(getSupportFragmentManager(),
					planCategoryList, "", "");
			mPager.setAdapter(mAdapter);

			indicator = (TabPageIndicator) findViewById(R.id.indicator);

			indicator.setViewPager(mPager);
			//indicator.setFooterIndicatorStyle(IndicatorStyle.Underline);
			mIndicator = indicator;

			spinner_header = (RelativeLayout) findViewById(R.id.spinner_header);
			textView_NoData = (TextView) findViewById(R.id.textView_NoData);
			textView_NoData.setVisibility(View.GONE);
			imageView1 = (ImageView) findViewById(R.id.imageView1);
			imageView1.setVisibility(View.GONE);
			mPager.setVisibility(View.GONE);
			indicator.setVisibility(View.GONE);

			circleArrayList = new ArrayList<JSONObject>();
			operatorArrayList = new ArrayList<JSONObject>();
			spinner_circle = (Spinner) findViewById(R.id.spinner_circle);
			circleList = new ArrayList<String>();
			operatorList = new ArrayList<String>();

			planDataSource = new PlanDataSource(PlanFragmentActivity.this);

			dataPlanArrayList = new ArrayList<JSONObject>();

			circleArray = Constants.loadJSONFromAsset(getApplicationContext(),
					"circle.json");
			// try {
			// planDataSource.open();
			//
			// // planCategoryList = planDataSource.getAllPlanType();
			// } catch (SQLException exception) {
			// // TODO: handle exception
			// // // Log.e("Er ", exception.getMessage());
			// } catch (Exception e) {
			// // TODO: handle exception
			// // // Log.e("Er ", e.getMessage());
			// } finally {
			// planDataSource.close();
			// }

			jsonArray = new JSONArray(circleArray);
			for (int i = 0; i < jsonArray.length(); i++) {
				circleArrayList.add(jsonArray.getJSONObject(i));
				circleList.add(jsonArray.getJSONObject(i).getString("name"));
			}

			spinner_circle
					.setOnItemSelectedListener(new OnItemSelectedListener() {

						@Override
						public void onItemSelected(AdapterView<?> arg0,
								View arg1, int position, long arg3) {
							// TODO Auto-generated method stub
							if (position != 0) {
								try {
									flag = 1;
									circleCode = jsonArray.getJSONObject(
											position).getString("code");
									planCategoryList.clear();

									mAdapter.notifyDataSetChanged();

									mPager.setVisibility(View.GONE);
									indicator.setVisibility(View.GONE);
									imageView1.setVisibility(View.GONE);
									textView_NoData.setVisibility(View.GONE);

									getPlan(getIntent().getExtras().getString(
											Constants.PLAN_JSON), circleCode);

								} catch (JSONException e) {
								}
							}
						}

						@Override
						public void onNothingSelected(AdapterView<?> arg0) {
							// TODO Auto-generated method stub

						}
					});

			/* SELECT DISTINCT planType FROM plans */

			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, circleList);

			dataAdapter
					.setDropDownViewResource(android.R.layout.simple_list_item_1);
			spinner_circle.setAdapter(dataAdapter);

			circleCode = getIntent().getExtras().getString("CIRCLE_CODE");
			isDataCardPlan = getIntent().getExtras().getInt("DATA_CARD_PLAN");
			if (circleCode != null) {
				// Log.w("Circle Code:", circleCode);
				for (int i = 0; i < jsonArray.length(); i++) {
					if (jsonArray.getJSONObject(i).getString("code")
							.equalsIgnoreCase(circleCode)) {
						spinner_circle.setSelection(i);
					}
				}
			}
			if (circleCode.equalsIgnoreCase("all")) {
				spinner_header.setVisibility(View.GONE);
				getPlan(getIntent().getExtras().getString(Constants.PLAN_JSON),
						circleCode);
			}
		} catch (Exception e) {
		}

	}
	public static GoogleAnalytics analytics; public static Tracker tracker;	
	@Override
		public void onStart() {
			super.onStart();
			// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
			// EasyTracker.getInstance(this).activityStart(this);
			//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
		}
	protected void getPlan(String operator, String circle) {

		dataPlanArrayList.clear();
		planCategoryList.clear();

		/*
		 * if(!mSpinnerRechargeType.isSelected()){
		 * Constants.showCustomToast(PlanFragmentActivity.this, "Check",
		 * Toast.LENGTH_LONG).show(); planTy="Topup-Plans"; }else{ planTy =
		 * mSpinnerRechargeType.getSelectedItem().toString(); }
		 */

		try {
			planDataSource.open();

			List<Plan> plans = planDataSource.getAllPlan(operator, circle);
			if (plans.size() == 0) {
//				if (Utility.getAvailableMemoryFlag(PlanFragmentActivity.this, "memoryFlag")) {
//
//				} else {
//					
				//planDataSource.deletePlan();
//				new GetPlanstask().execute();
//				}
			} else {

//				String LastUpdateTime = plans.get(0).getPlanUptateTime();
//				int days = (int) ((System.currentTimeMillis() - Long
//						.parseLong(LastUpdateTime)) / (1000 * 60 * 60 * 24));
//				
//				if (days >= 2) {
////					if (Utility.getAvailableMemoryFlag(PlanFragmentActivity.this, "memoryFlag")) {
////
////					} else {
//						
//					//planDataSource.deletePlan();
//					new GetPlanstask().execute();
////					}
//				} else {

					JSONArray arrayList;

					arrayList = planDataSource.getAllPlanDetails(getIntent()
							.getExtras().getString(Constants.PLAN_JSON),
							circleCode, "", isDataCardPlan);

					planCategoryList.addAll(planDataSource.getPlanType(
							getIntent().getExtras().getString(
									Constants.PLAN_JSON), circleCode,
							isDataCardPlan));

					for (int i = 0; i < arrayList.length(); i++) {
						try {

							JSONObject jsonObject = arrayList.getJSONObject(i);
							// planCategoryList.add(jsonObject.getString("plantype"));
							dataPlanArrayList.add(jsonObject);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
							Log.e("Plans", e.getMessage());
						} finally {
						}
//					}

					mAdapter = new PlanFragmentAdapter(
							getSupportFragmentManager(), planCategoryList,
							getIntent().getExtras().getString(
									Constants.PLAN_JSON), circleCode);
					mPager.setAdapter(mAdapter);

					// TabPageIndicator indicator = (TabPageIndicator)
					// findViewById(R.id.indicator);
					//
					// indicator.setViewPager(mPager);
					// //indicator.setFooterIndicatorStyle(IndicatorStyle.Triangle);
					// mIndicator = indicator;

					indicator.setVisibility(View.VISIBLE);
					indicator.notifyDataSetChanged();
					imageView1.setVisibility(View.GONE);
					mPager.setVisibility(View.VISIBLE);
					textView_NoData.setVisibility(View.GONE);
				}
			}

		} catch (SQLException e) {
			Log.e("Plans", e.getMessage());
		} catch (Exception e) {
			Log.e("Plans", e.getMessage());
		} finally {
			planDataSource.close();
		}
	}

	protected void getPlanByType(String planType) {

		dataPlanArrayList.clear();

		try {
			planDataSource.open();

			JSONArray arrayList;

			arrayList = planDataSource.getAllPlanDetails(getIntent()
					.getExtras().getString(Constants.PLAN_JSON), circleCode,
					planType, isDataCardPlan);

			for (int i = 0; i < arrayList.length(); i++) {
				try {

					JSONObject jsonObject = arrayList.getJSONObject(i);
					dataPlanArrayList.add(jsonObject);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
				} finally {
				}
			}

		} catch (SQLException exception) {
			// TODO: handle exception
			// // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			// // Log.e("Er ", e.getMessage());
		} finally {
			planDataSource.close();
		}

		// // Log.e("map", "" + arrayList + "   " + operator);

	}

	public class GetPlanstask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair
					.add(new BasicNameValuePair("method", "getPlanDetails"));
			listValuePair.add(new BasicNameValuePair("operator", getIntent()
					.getExtras().getString(Constants.PLAN_JSON)));
			listValuePair.add(new BasicNameValuePair("circle", circleCode));

			String response = RequestClass.getInstance()
					.readPay1Request(PlanFragmentActivity.this,
							Constants.API_URL, listValuePair);
			String replaced = response.replace("(", "").replace(")", "")
					.replace(";", "");
			planDataSource.open();
			planDataSource.savePlansTodataBase(replaced);
			planDataSource.close();
			return replaced;

		}

		@Override
		protected void onPostExecute(String result) {
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			super.onPostExecute(result);
			try {
				if (result != null && !result.startsWith("Error") && flag != 0) {
					flag--;
					getPlan(getIntent().getExtras().getString(
							Constants.PLAN_JSON), circleCode);
				} else {
					indicator.setVisibility(View.GONE);
					imageView1.setVisibility(View.GONE);
					mPager.setVisibility(View.GONE);
					textView_NoData.setVisibility(View.VISIBLE);
				}
			} catch (Exception e) {
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(PlanFragmentActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							GetPlanstask.this.cancel(true);
							dialog.cancel();
							finish();
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			GetPlanstask.this.cancel(true);
			dialog.cancel();
			finish();
		}

	}

	private void savePlansTodataBase(String planJson) {
		try {
			planDataSource.open();
			// ContentValues contentValues = new ContentValues();
			JSONArray array = new JSONArray(planJson);
			JSONObject planJsonObject = array.getJSONObject(0);

			Iterator<String> operatorKey = planJsonObject.keys();

			if (operatorKey.hasNext()) {

				long time = System.currentTimeMillis();

				JSONObject jsonObject2 = planJsonObject
						.getJSONObject(operatorKey.next());

				String opr_name = jsonObject2.getString("opr_name");
				String prod_code_pay1 = jsonObject2.getString("prod_code_pay1");
				// contentValues.put("operator_id", prod_code_pay1);
				// contentValues.put("operator_name", opr_name);
				// contentValues.put("prod_code_pay1", prod_code_pay1);
				JSONObject circleJsonObject = jsonObject2
						.getJSONObject("circles");
				Iterator<String> circleKey = circleJsonObject.keys();
				if (circleKey.hasNext()) {
					JSONObject circleJsonObject2 = circleJsonObject
							.getJSONObject(circleKey.next().toString());
					String circleName = circleJsonObject2
							.getString("circle_name");

					// contentValues.put("circle_name", circleName);

					JSONObject plansJsonObject = circleJsonObject2
							.getJSONObject("plans");
					String circle_id = circleJsonObject2.getString("circle_id");
					Iterator<String> planKey = plansJsonObject.keys();
					Iterator<String> planKeyDB = plansJsonObject.keys();
					// contentValues.put("circle_id", circle_id);

					while (planKey.hasNext()) {
						String planType = planKey.next().toString();
						JSONArray planArray = plansJsonObject
								.getJSONArray(planType);
						// contentValues.put("planType", planType);

						for (int i = 0; i < planArray.length(); i++) {
							JSONObject jsonObject = planArray.getJSONObject(i);
							String plan_desc = jsonObject
									.getString("plan_desc");
							// contentValues.put("plan_desc", plan_desc);
							String plan_amt = jsonObject.getString("plan_amt");
							// contentValues.put("plan_amt", plan_amt);
							String plan_validity = jsonObject
									.getString("plan_validity");
							String show_flag = jsonObject
									.getString("show_flag");
							// contentValues.put("plan_validity",
							// plan_validity);
							// // Log.e("cORRECT", "cORRECT   " + i + "  "
							// + jsonObject.toString());
							// controller.insertPlans(contentValues);
							planDataSource.createPlan(prod_code_pay1, opr_name,
									circle_id, circleName, planType, plan_amt,
									plan_validity, plan_desc, "" + time,show_flag);
						}

					}

				}

			}
		} catch (JSONException exception) {

		} catch (SQLException exception) {
			// TODO: handle exception
			// // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			// // Log.e("Er ", e.getMessage());
		} finally {
			planDataSource.close();
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants.showNavigationBar(PlanFragmentActivity.this);
		flag = 1;
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

}