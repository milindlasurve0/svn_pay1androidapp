package com.mindsarray.pay1;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.RechargeMainAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.utilities.GridMenuItem;

public class DthRechargeMainFragment extends Fragment {

	View vi = null;
	GridView gridView;
	ArrayList<GridMenuItem> gridArray = new ArrayList<GridMenuItem>();
	RechargeMainAdapter customGridAdapter;
	private static final String TAG = "DthRechargeMainFragment Activity";
	// String planJson;
	// JSONObject jsonObject2;
	public static GoogleAnalytics analytics;
	public static Tracker tracker;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		vi = inflater.inflate(R.layout.rechargemain_activity, container, false);
//		analytics = GoogleAnalytics.getInstance(getActivity()); tracker = Constants.setAnalyticsTracker(this, tracker);
//		//easyTracker.set(Fields.SCREEN_NAME, TAG);
		analytics = GoogleAnalytics.getInstance(getActivity());
		tracker = Constants.setAnalyticsTracker(getActivity(), tracker);
		if (gridArray.size() == 0) {
			int mAirtel = R.drawable.d_dishtv;
			int mVodafone = R.drawable.d_tatasky;
			int mBsnl = R.drawable.d_sundirect;
			int mIdea = R.drawable.d_bigtv;
			int mAircel = R.drawable.m_airtel;
			int mRelainceCdma = R.drawable.d_videocon;

			gridArray.add(new GridMenuItem(mAirtel, "Dish TV",
					Constants.OPERATOR_DTH_DISHTV, Constants.RECHARGE_DTH));
			gridArray.add(new GridMenuItem(mVodafone, "Tata Sky",
					Constants.OPERATOR_DTH_TATA_SKY, Constants.RECHARGE_DTH));
			gridArray.add(new GridMenuItem(mBsnl, "Sun Direct",
					Constants.OPERATOR_DTH_SUN, Constants.RECHARGE_DTH));
			gridArray.add(new GridMenuItem(mIdea, "Big TV",
					Constants.OPERATOR_DTH_BIG, Constants.RECHARGE_DTH));
			gridArray.add(new GridMenuItem(mAircel, "Airtel",
					Constants.OPERATOR_DTH_AIRTEL, Constants.RECHARGE_DTH));
			gridArray.add(new GridMenuItem(mRelainceCdma, "Videocon",
					Constants.OPERATOR_DTH_VIDEOCON, Constants.RECHARGE_DTH));

		}

		gridView = (GridView) vi.findViewById(R.id.gridView1);

		customGridAdapter = new RechargeMainAdapter(getActivity(),
				R.layout.rechargemain_adapter, gridArray,false);

		// planJson = Constants.loadJSONFromAsset(getActivity(), "plans.json");
		gridView.setAdapter(customGridAdapter);

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// Constants.showCustomToast(MobileRechargeActivity.this,
				// gridArray.get(position).getId()+"",
				// Toast.LENGTH_LONG).show();
				try {
					/*
					 * JSONObject jsonObject=new JSONObject(planJson);
					 * jsonObject2
					 * =jsonObject.getJSONObject(String.valueOf(gridArray
					 * .get(position).getId()));
					 */
					Intent intent = new Intent(getActivity(),
							DthRechargeActivity.class);
					intent.putExtra(Constants.OPERATOR_ID,
							gridArray.get(position).getId());
					/*
					 * intent.putExtra(Constants.PLAN_JSON,
					 * jsonObject2.toString());
					 */
					intent.putExtra(Constants.PLAN_JSON,
							String.valueOf(gridArray.get(position).getId()));
					intent.putExtra(Constants.OPERATOR_LOGO,
							gridArray.get(position).getImage());
					intent.putExtra(Constants.OPERATOR_NAME,
							gridArray.get(position).getTitle());
					getActivity().startActivity(intent);

				} catch (Exception e) {
					// e.printStackTrace();
				}

			}

		});
		return vi;

	}
//	public static GoogleAnalytics analytics; public static Tracker tracker;	
	@Override
		public void onStart() {
			super.onStart();
			// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
			// EasyTracker.getInstance(this).activityStart(this);
//			//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
			//tracker.setScreenName(TAG);
			Constants.trackOnStart(tracker, TAG);
				//.build());
		}
	public static DthRechargeMainFragment newInstance(String content) {
		DthRechargeMainFragment fragment = new DthRechargeMainFragment();

		Bundle b = new Bundle();
		b.putString("msg", content);

		fragment.setArguments(b);

		return fragment;
	}

	// private String mContent = "???";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// if ((savedInstanceState != null)
		// && savedInstanceState.containsKey(KEY_CONTENT)) {
		// mContent = savedInstanceState.getString(KEY_CONTENT);
		// }
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// outState.putString(KEY_CONTENT, mContent);
	}
}
