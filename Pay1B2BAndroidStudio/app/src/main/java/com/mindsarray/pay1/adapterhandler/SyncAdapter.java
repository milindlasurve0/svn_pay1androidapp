package com.mindsarray.pay1.adapterhandler;

import java.io.File;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.mindsarray.pay1.SplashScreenActivity;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.Pay1SQLiteHelper;
import com.mindsarray.pay1.databasehandler.SyncDatabase;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

/**https://developer.android.com/training/sync-adapters/creating-sync-adapter.html**/

public class SyncAdapter extends AbstractThreadedSyncAdapter {
	
	private Context syncContext;
	
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        
        syncContext = context;
    }

    public SyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        
        syncContext = context;
    }
    
    @Override
    public void onPerformSync(
            Account account,
            Bundle extras,
            String authority,
            ContentProviderClient provider,
            SyncResult syncResult) {
    	long itime = System.currentTimeMillis();
    	String iDateTime = Utility.convertMillisToDateTime(String.valueOf(itime), "yyyy-MM-dd hh:mm:ss");
    	Log.e("Sync Adapter", "starting sync at " + iDateTime);
    	boolean dbCloned = false;
    	
    	File dbSync = syncContext.getDatabasePath(Pay1SQLiteHelper.SYNC_DATABASE);
		if(!dbSync.exists()){
			SyncDatabase syncDBFirst = new SyncDatabase(syncContext);
			dbCloned = syncDBFirst.cloneDatabase(Pay1SQLiteHelper.DATABASE_NAME, Pay1SQLiteHelper.SYNC_DATABASE);
			syncDBFirst.open();
			syncDBFirst.close();
			Log.e("Sync Adapter", "Sync DB Created");
		}
    	SyncDatabase syncDB = new SyncDatabase(syncContext);
    	
    	boolean arePlansSaved = false;
    	boolean areCirclesSaved = false;
    	
    	String lastSyncTime = Utility.getLastSyncTime(syncContext);
    	String lastSyncDateTime = Utility.convertMillisToDateTime(lastSyncTime, "yyyy-MM-dd hh:mm:ss");
    	Log.e("Sync Adapter", "lastSyncDateTime: " + lastSyncDateTime);
    	
    	ArrayList<NameValuePair> plansUpdateParams = new ArrayList<NameValuePair>();
    	plansUpdateParams.add(new BasicNameValuePair("method", "getPlanDetails"));
    	plansUpdateParams.add(new BasicNameValuePair("operator", "all"));
    	plansUpdateParams.add(new BasicNameValuePair("circle", "all"));
    	plansUpdateParams.add(new BasicNameValuePair("timestamp",lastSyncDateTime));
		
		ArrayList<NameValuePair> circleUpdateParams = new ArrayList<NameValuePair>();
		circleUpdateParams.add(new BasicNameValuePair("method", "getMobileDetails"));
		circleUpdateParams.add(new BasicNameValuePair("mobile", "all"));
		circleUpdateParams.add(new BasicNameValuePair("timestamp", lastSyncDateTime));
		//circleUpdateParams.add(new BasicNameValuePair("mobile_code_digits", "4"));
		Log.e("Sync Adapter: Last Sync Time", lastSyncDateTime);
		try{
			Log.d("Sync Adapter Params Sent", "Params Sent "+(Constants.API_URL+plansUpdateParams));
			String circleResponseString = RequestClass.getInstance().readPay1Request(syncContext, Constants.API_URL, circleUpdateParams);
			String plansResponseString = RequestClass.getInstance().readPay1Request(syncContext, Constants.API_URL, plansUpdateParams);
			
			Log.e("Sync Adapter", "jsons fetched");
			Log.e("Sync Adapter: Plans", plansResponseString);
			Log.e("Sync Adapter: Circle", circleResponseString);
			if (!plansResponseString.startsWith("Error")) {
				String replaced = plansResponseString.replace("(", "").replace(")", "").replace(";", "");
				if(replaced.length() >= 1){
					replaced = replaced.substring(1, replaced.length() - 1);
					if (replaced.contains("No plans found")) {
						
					} 
					else {
						if(!dbCloned)
							dbCloned = syncDB.cloneDatabase(Pay1SQLiteHelper.DATABASE_NAME, Pay1SQLiteHelper.SYNC_DATABASE);
						syncDB.open();
						if(lastSyncDateTime.equals(""))
							arePlansSaved = syncDB.deleteAndSaveAllPlans(replaced);
						else	
							arePlansSaved = syncDB.savePlansTodataBase(replaced);
						syncDB.close();
						Log.d("replaced", "replaced "+replaced);
						
						Log.e("Sync Adapter: Plans Saved", String.valueOf(arePlansSaved));
					}
				}	
			}
			
			if (!circleResponseString.startsWith("Error")) {
				String replaced = circleResponseString.replace("(", "").replace(")", "").replace(";", "");
				replaced = replaced.substring(1, replaced.length() - 1);
				JSONObject jsonObject = new JSONObject(replaced);
				String status = jsonObject.getString("status");
				
				if (status.equalsIgnoreCase("success")) {
					String details = jsonObject.getString("details");
					if(details.length() > 2){
						if(!dbCloned)
							syncDB.cloneDatabase(Pay1SQLiteHelper.DATABASE_NAME, Pay1SQLiteHelper.SYNC_DATABASE);
						areCirclesSaved = syncDB.saveCircleNumberToDatabase(jsonObject);
						Log.e("Sync Adapter: Circles Saved", String.valueOf(areCirclesSaved));
					}	
				}
			}	
			
			if(arePlansSaved){
				Utility.setLastSyncTime(syncContext, String.valueOf(itime));
				syncDB.cloneDatabase(Pay1SQLiteHelper.SYNC_DATABASE, Pay1SQLiteHelper.DATABASE_NAME);
				Log.e("Sync Adapter", "Database cloned back");
			}	
	
			long etime = System.currentTimeMillis();
			String currentDateTime = Utility.convertMillisToDateTime(String.valueOf(etime), "yyyy-MM-dd hh:mm:ss");
			Log.e("Sync Adapter: Sync Time", String.valueOf((etime - itime)/1000) + " secs at " + currentDateTime);
			
//			Constants.writeToSDCard(syncContext, "\n"+"Sync Adapter: Sync Time" + String.valueOf((etime - itime)/1000) + " secs at " + currentDateTime+"\n");
		}
		catch(Exception e){
			e.printStackTrace();
		}	
    }
    
    @Override
    public void onSyncCanceled(){
    	long etime = System.currentTimeMillis();
		String currentDateTime = Utility.convertMillisToDateTime(String.valueOf(etime), "yyyy-MM-dd HH:mm:ss");
		Log.e("sync cancelled at", currentDateTime);
    }
}    