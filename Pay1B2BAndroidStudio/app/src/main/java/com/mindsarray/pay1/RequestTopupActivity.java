package com.mindsarray.pay1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Spinner;

import com.mindsarray.pay1.adapterhandler.BankDetailsAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;

public class RequestTopupActivity extends Activity {

	public static final String TAG = "Request Topup";
	private Context mContext = RequestTopupActivity.this;
	private ExpandableListView mExpandableListView;
	private BankDetailsAdapter mBankDetailsAdapter;
	private ArrayList<BankDetailsAdapter.BankDetailsPair> listDataHeader;
	private ArrayList<ArrayList<BankDetailsAdapter.BankDetailsPair>> listChildData;
	private String topupBank, transferType;
	private Map transferTypeHash = new HashMap();
	private EditText topupAmountEditText, topupTransIdEditText;
	ArrayList<String> banksList, transferTypeList;
	Spinner bankSpinner, transferTypesSpinner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.request_topup_layout);

		bankSpinner = (Spinner) findViewById(R.id.bankSpinner);
		transferTypesSpinner = (Spinner) findViewById(R.id.transferTypes);

		bankSpinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						topupBank = parent.getItemAtPosition(position)
								.toString();
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {

					}
				});

		transferTypesSpinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						transferType = transferTypeHash.get(
								parent.getItemAtPosition(position).toString())
								.toString();
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {

					}
				});

		banksList = new ArrayList<String>();
		transferTypeList = new ArrayList<String>();

		/*
		 * ArrayAdapter<String> banksAdapter = new
		 * ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item,
		 * banksList); ArrayAdapter<String> transferTypesAdapter = new
		 * ArrayAdapter<String>( mContext, android.R.layout.simple_spinner_item,
		 * transferTypeList);
		 * 
		 * 
		 * banksAdapter
		 * .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item
		 * ); transferTypesAdapter
		 * .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item
		 * );
		 * 
		 * // attaching data adapter to spinner
		 * bankSpinner.setAdapter(banksAdapter);
		 * transferTypesSpinner.setAdapter(transferTypesAdapter);
		 * bankSpinner.setSelection(0); transferTypesSpinner.setSelection(0);
		 */

		topupAmountEditText = (EditText) findViewById(R.id.topupAmount);
		topupTransIdEditText = (EditText) findViewById(R.id.topupTransId);
		topupAmountEditText.setText("");
		topupTransIdEditText.setText("");

		Button sendRequestButton = (Button) findViewById(R.id.sendRequestButton);
		sendRequestButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!topupAmountEditText.getText().toString().equals("")) {
					new SendTopUpRequestTask().execute();
				} else {
					Constants.showOneButtonDialog(mContext,
							"Enter an amount to continue", "Invalid Details",
							"OK", 0);
				}
			}
		});

		new GetBanksAndTransferTypes().execute();

	}

	public class GetBanksAndTransferTypes extends
			AsyncTask<String, String, String> implements OnDismissListener {

		ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair.add(new BasicNameValuePair("method",
					"banksAndTransferTypes"));

			String response = RequestClass.getInstance()
					.readPay1Request(RequestTopupActivity.this,
							Constants.API_URL, listValuePair);
			listValuePair.clear();
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			super.onPostExecute(result);
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObjectResponse = array.getJSONObject(0);
					String status = jsonObjectResponse.getString("status");

					Log.e("Status: ", status);
					if (status.equalsIgnoreCase("success")) {
						JSONObject description = jsonObjectResponse
								.getJSONObject("description");
						JSONArray banks = description.getJSONArray("banks");
						JSONArray transferTypes = description
								.getJSONArray("transfer_types");

						for (int i = 0; i < banks.length(); i++) {
							banksList.add(banks.getString(i));
						}

						for (int i = 0; i < transferTypes.length(); i++) {
							String[] ttKeyValue = transferTypes.getString(i)
									.split(":");
							transferTypeList.add(ttKeyValue[1]);
							transferTypeHash.put(ttKeyValue[1], ttKeyValue[0]);
						}
						ArrayAdapter<String> banksAdapter = new ArrayAdapter<String>(
								mContext, android.R.layout.simple_spinner_item,
								banksList);
						ArrayAdapter<String> transferTypesAdapter = new ArrayAdapter<String>(
								mContext, android.R.layout.simple_spinner_item,
								transferTypeList);

						// Drop down layout style - list view with radio button
						banksAdapter
								.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						transferTypesAdapter
								.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

						// attaching data adapter to spinner
						bankSpinner.setAdapter(banksAdapter);
						transferTypesSpinner.setAdapter(transferTypesAdapter);
						bankSpinner.setSelection(0);
						transferTypesSpinner.setSelection(0);

					} else {
						Constants.showOneButtonFailureDialog(
								RequestTopupActivity.this,
								Constants.checkCode(result), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(
							RequestTopupActivity.this,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(RequestTopupActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(RequestTopupActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(RequestTopupActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							GetBanksAndTransferTypes.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			GetBanksAndTransferTypes.this.cancel(true);
			dialog.cancel();
		}
	}

	public class SendTopUpRequestTask extends AsyncTask<String, String, String>
			implements OnDismissListener {

		ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair.add(new BasicNameValuePair("method",
					"sendBalanceTopupRequest"));
			listValuePair.add(new BasicNameValuePair("bank_acc_id", topupBank));
			listValuePair.add(new BasicNameValuePair("trans_type_id",
					transferType));
			listValuePair.add(new BasicNameValuePair("amount",
					topupAmountEditText.getText().toString()));
			listValuePair.add(new BasicNameValuePair("bank_trans_id",
					topupTransIdEditText.getText().toString()));

			String response = RequestClass.getInstance()
					.readPay1Request(RequestTopupActivity.this,
							Constants.API_URL, listValuePair);
			listValuePair.clear();
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			super.onPostExecute(result);
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObjectResponse = array.getJSONObject(0);
					String status = jsonObjectResponse.getString("status");

					Log.e("Status: ", status);
					if (status.equalsIgnoreCase("success")) {
						String description = jsonObjectResponse
								.getString("description");
						Constants.showOneButtonSuccessDialog(mContext,
								description.toString(), Constants.PG_SETTINGS);

					} else {
						Constants.showOneButtonFailureDialog(
								RequestTopupActivity.this,
								Constants.checkCode(result), TAG,
								Constants.OTHER_ERROR);
					}
				} else {
					Constants.showOneButtonFailureDialog(
							RequestTopupActivity.this,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(RequestTopupActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(RequestTopupActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(RequestTopupActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							SendTopUpRequestTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			SendTopUpRequestTask.this.cancel(true);
			dialog.cancel();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Constants.showNavigationBar(mContext);
	}

}
