package com.mindsarray.pay1;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.PlansAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.databasehandler.PlanDataSource;

public class PlanListFragment extends Fragment {
	private static final String KEY_CONTENT = "PlanListFragment:Content";

	ListView planList;
	ArrayList<String> planCategoryList = new ArrayList<String>();
	ArrayList<JSONObject> dataPlanArrayList = new ArrayList<JSONObject>();
	PlanDataSource planDataSource;
	int isDataCardPlan = 0;
	PlansAdapter adapter;
	String operator = "";
	String circle = "";
	private String planType = "";
	private static final String TAG = "PlanList Fragment";
	public static PlanListFragment newInstance(String operator, String circle,
			String planType) {
		PlanListFragment fragment = new PlanListFragment();

		// StringBuilder builder = new StringBuilder();
		// // for (int i = 0; i < 2; i++) {
		// builder.append(content).append(" ");
		// // }
		// builder.deleteCharAt(builder.length() - 1);
		// fragment.planType = builder.toString();

//		fragment.dataPlanArrayList.clear();
//		fragment.planCategoryList.clear();
//		fragment.adapter.notifyDataSetChanged();
		
		fragment.operator = operator;
		fragment.circle = circle;
		fragment.planType = planType;

		return fragment;
	}
	public static GoogleAnalytics analytics; public static Tracker tracker;	
	@Override
		public void onStart() {
			super.onStart();
			// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
			// EasyTracker.getInstance(this).activityStart(this);
			//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
		}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		analytics = GoogleAnalytics.getInstance(getActivity()); 
		tracker = Constants.setAnalyticsTracker(getActivity(), tracker);//easyTracker.set(Fields.SCREEN_NAME, TAG);
		if ((savedInstanceState != null)
				&& savedInstanceState.containsKey(KEY_CONTENT)) {
			planType = savedInstanceState.getString(KEY_CONTENT);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View vi = inflater.inflate(R.layout.plan_listview_fragment_activity,
				null);
		planDataSource = new PlanDataSource(getActivity());
		planList = (ListView) vi.findViewById(R.id.planList);

		adapter = new PlansAdapter(getActivity(), 1, dataPlanArrayList);
		planList.setAdapter(adapter);

		planList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent returnIntent = new Intent();
				try {
					returnIntent.putExtra("result", dataPlanArrayList.get(arg2)
							.getString("amount"));
				} catch (JSONException e) {
					// e.printStackTrace();
					returnIntent.putExtra("result", "0");
				}
				getActivity().setResult(getActivity().RESULT_OK, returnIntent);
				getActivity().finish();
			}
		});

		getPlan(operator, circle);
		// TextView text = new TextView(getActivity());
		// text.setGravity(Gravity.CENTER);
		// text.setText(mContent);
		// text.setTextSize(20 * getResources().getDisplayMetrics().density);
		// text.setPadding(20, 20, 20, 20);
		//
		// LinearLayout layout = new LinearLayout(getActivity());
		// layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
		// LayoutParams.FILL_PARENT));
		// layout.setGravity(Gravity.CENTER);
		// layout.addView(text);

		return vi;
	}

	protected void getPlan(String operator, String circle) {

		dataPlanArrayList.clear();
		planCategoryList.clear();

		/*
		 * if(!mSpinnerRechargeType.isSelected()){
		 * Constants.showCustomToast(PlanFragmentActivity.this, "Check",
		 * Toast.LENGTH_LONG).show(); planTy="Topup-Plans"; }else{ planTy =
		 * mSpinnerRechargeType.getSelectedItem().toString(); }
		 */

		try {
			planDataSource.open();

			//List<Plan> plans = planDataSource.getAllPlan(operator, circle);

			JSONArray arrayList;

			// if (spinner_plantype.getVisibility() == View.VISIBLE) {
			// arrayList = planDataSource.getAllPlanDetails(
			// getIntent().getExtras().getString(
			// Constants.PLAN_JSON), circleCode,
			// spinner_plantype.getSelectedItem().toString(),
			// isDataCardPlan);
			// } else {
			arrayList = planDataSource.getAllPlanDetails(operator, circle, "",
					isDataCardPlan);
			// }

			for (int i = 0; i < arrayList.length(); i++) {
				try {

					JSONObject jsonObject = arrayList.getJSONObject(i);
					// planCategoryList.add(jsonObject.getString("plantype"));
					dataPlanArrayList.add(jsonObject);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
				} finally {
				}
			}

		} catch (SQLException exception) {
		} catch (Exception e) {
		} finally {
			planDataSource.close();
		}
		adapter.notifyDataSetChanged();

		getPlanByType(operator, circle, planType);
	}

	protected void getPlanByType(String operator, String circle, String planType) {

		dataPlanArrayList.clear();

		try {
			planDataSource.open();

			JSONArray arrayList;

			arrayList = planDataSource.getAllPlanDetails(operator, circle,
					planType, isDataCardPlan);

			for (int i = 0; i < arrayList.length(); i++) {
				try {

					JSONObject jsonObject = arrayList.getJSONObject(i);
					dataPlanArrayList.add(jsonObject);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
				} finally {
				}
			}

		} catch (SQLException exception) {
			// TODO: handle exception
			// // Log.e("Er ", exception.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			// // Log.e("Er ", e.getMessage());
		} finally {
			planDataSource.close();
		}
		adapter.notifyDataSetChanged();
		// // Log.e("map", "" + arrayList + "   " + operator);

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(KEY_CONTENT, planType);
	}
}
