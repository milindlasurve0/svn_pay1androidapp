package com.mindsarray.pay1.adapterhandler;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindsarray.pay1.MobileRechargeMainActivity;
import com.mindsarray.pay1.R;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.utilities.GridMenuItem;
import com.mindsarray.pay1.utilities.Utility;

public class RechargeMainAdapter extends ArrayAdapter<GridMenuItem> {
	Context context;
	int layoutResourceId;
	ArrayList<GridMenuItem> data = new ArrayList<GridMenuItem>();
	boolean isSideBar;

	public RechargeMainAdapter(Context context, int layoutResourceId,
			ArrayList<GridMenuItem> data, boolean isSideBar) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.isSideBar = isSideBar;
		this.data = data;
	}



	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		RecordHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new RecordHolder();
			holder.txtTitle = (TextView) row.findViewById(R.id.item_text);
			holder.imageItem = (ImageView) row.findViewById(R.id.item_image);
			holder.textView_New = (TextView) row
					.findViewById(R.id.textView_New);
			holder.textView_New.setVisibility(View.GONE);

			row.setTag(holder);
		} else {
			holder = (RecordHolder) row.getTag();
		}
		// if(position!=9){
		GridMenuItem item = data.get(position);
		holder.txtTitle.setText(item.getTitle());
		if (isSideBar) {
			holder.txtTitle.setTextColor(Color.WHITE);
		} else {
			holder.txtTitle.setTextColor(Color.BLACK);
		}
		holder.imageItem.setImageResource(item.getImage());
		if (item.getTitle().equalsIgnoreCase("Wallet Topup")
				|| item.getTitle().equalsIgnoreCase("Online Payment")||item.getTitle().equalsIgnoreCase("Mini ATM"))
			holder.textView_New.setVisibility(View.VISIBLE);

		if (item.getTitle().equalsIgnoreCase("Limit")) {
			if (Utility.getLoginFlag(context,
					Constants.SHAREDPREFERENCE_IS_LOGIN)) {
				if (Utility.getIsAbleForPG(context)) {
					// gridArray.add(new GridMenuItem(debitcard,
					// "Online Balance", 10, 1));
					row.setVisibility(View.VISIBLE);

				} else {
					row.setVisibility(View.GONE);
				}
			} else {
				row.setVisibility(View.GONE);
			}
		}
		// Animation animation = AnimationUtils.loadAnimation(context,
		// R.anim.fadeout);
		// holder.textView_New.setAnimation(animation);
		// }
		return row;

	}

	static class RecordHolder {
		TextView txtTitle, textView_New;
		ImageView imageItem;

	}
}