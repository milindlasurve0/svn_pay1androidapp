package com.mindsarray.pay1.databasehandler;

public class Plan {

	private int planID;
	private String planOpertorID;
	private String planOperatorName;
	private String planCircleID;
	private String planCircleName;
	private String planType;
	private String planAmount;
	private String planValidity;
	private String planDescription;
	private String planUptateTime;

	public int getPlanID() {
		return planID;
	}

	public void setPlanID(int planID) {
		this.planID = planID;
	}

	public String getPlanOpertorID() {
		return planOpertorID;
	}

	public void setPlanOpertorID(String planOpertorID) {
		this.planOpertorID = planOpertorID;
	}

	public String getPlanOperatorName() {
		return planOperatorName;
	}

	public void setPlanOperatorName(String planOperatorName) {
		this.planOperatorName = planOperatorName;
	}

	public String getPlanCircleID() {
		return planCircleID;
	}

	public void setPlanCircleID(String planCircleID) {
		this.planCircleID = planCircleID;
	}

	public String getPlanCircleName() {
		return planCircleName;
	}

	public void setPlanCircleName(String planCircleName) {
		this.planCircleName = planCircleName;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public String getPlanAmount() {
		return planAmount;
	}

	public void setPlanAmount(String planAmount) {
		this.planAmount = planAmount;
	}

	public String getPlanValidity() {
		return planValidity;
	}

	public void setPlanValidity(String planValidity) {
		this.planValidity = planValidity;
	}

	public String getPlanDescription() {
		return planDescription;
	}

	public void setPlanDescription(String planDescription) {
		this.planDescription = planDescription;
	}

	public String getPlanUptateTime() {
		return planUptateTime;
	}

	public void setPlanUptateTime(String planUptateTime) {
		this.planUptateTime = planUptateTime;
	}

}