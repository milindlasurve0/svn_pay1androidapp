package com.mindsarray.pay1.databasehandler;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class NotificationDataSource {

	// Database fields
	private SQLiteDatabase database;
	private Pay1SQLiteHelper dbHelper;

	public NotificationDataSource(Context context) {
		dbHelper = new Pay1SQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public void createNotification(String Data, int Type, String Time) {
		/*Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_NOTIFICATION,
				null, 
				Pay1SQLiteHelper.NOTIFICATION_DATA + " = '" + Data
				+ "' AND " + Pay1SQLiteHelper.NOTIFICATION_TYPE
				+ " = '1'",
				null, null, null, null);
		 if(cursor.getCount() <= 0){*/
			ContentValues values = new ContentValues();
			values.put(Pay1SQLiteHelper.NOTIFICATION_DATA, Data);
			values.put(Pay1SQLiteHelper.NOTIFICATION_TYPE, Type);
			values.put(Pay1SQLiteHelper.NOTIFICATION_TIME, Time);

			database.insert(Pay1SQLiteHelper.TABLE_NOTIFICATION, null, values);
	    // }
	}

	public void deleteNotification() {
		database.delete(Pay1SQLiteHelper.TABLE_NOTIFICATION, null, null);
	}

	public void deleteNotification(Notification notification) {
		long id = notification.getNotificationID();
		database.delete(Pay1SQLiteHelper.TABLE_NOTIFICATION,
				Pay1SQLiteHelper.NOTIFICATION_ID + " = " + id, null);
	}

	public List<Notification> getAllNotification() {
		List<Notification> notifications = new ArrayList<Notification>();

		Cursor cursor = database.query(Pay1SQLiteHelper.TABLE_NOTIFICATION,
				null, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Notification notification = cursorToNotification(cursor);
			notifications.add(notification);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return notifications;
	}

	public ArrayList<HashMap<String, String>> getAllNotifications(int numpage,
			int numRecords) {
		ArrayList<HashMap<String, String>> wordList = new ArrayList<HashMap<String, String>>();

		/*Cursor cursor = database.query(true,
				Pay1SQLiteHelper.TABLE_NOTIFICATION,
				null,
				null,
				null,
				null,
				null,
				Pay1SQLiteHelper.NOTIFICATION_ID
						+ " DESC ",
						(numpage - 1) * numRecords + ","
						+ numRecords);*/
		
		Cursor cursor = database.query(true,
				Pay1SQLiteHelper.TABLE_NOTIFICATION,
				null,
				null,
				null,
				null,
				null,
				Pay1SQLiteHelper.NOTIFICATION_TIME
						+ " DESC ",null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Notification notification = cursorToNotification(cursor);

			HashMap<String, String> map = new HashMap<String, String>();
			map.put("notification", notification.getNotificationData());
			try {
				Date date = new Date(Long.parseLong(notification
						.getNotificationTime()));
				DateFormat format = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
				map.put("time", format.format(date));
			} catch (Exception e) {
				e.printStackTrace();
			}

			map.put("id", "" + notification.getNotificationID());
			map.put("netOrSms", "" + notification.getNotificationType());
			wordList.add(map);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();

		// return contact list
		return wordList;
	}

	private Notification cursorToNotification(Cursor cursor) {
		Notification notification = new Notification();
		notification.setNotificationID(cursor.getInt(0));
		notification.setNotificationData(cursor.getString(1));
		notification.setNotificationType(cursor.getInt(2));
		notification.setNotificationTime(cursor.getString(3));

		return notification;
	}
}
