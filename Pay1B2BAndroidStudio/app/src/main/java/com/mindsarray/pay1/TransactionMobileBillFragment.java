package com.mindsarray.pay1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.adapterhandler.StatusAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

public class TransactionMobileBillFragment extends Fragment {
	View vi = null;
	private TextView mDateDisplay;
	private Button btnCalender, btnPreWeek, btnNextWeek;
	private int mYear;
	private int mMonth;
	private int mDay;
	StringBuilder builder;
	static final int DATE_DIALOG_ID = 0;
	ListView mStatusListView;
	ArrayList<String> statusList;
	StatusAdapter adapter;
	Bundle bundle;
	boolean bNetOrSms;
	private SharedPreferences mSettingsData;
	private static final String TAG = "Transaction";

	String months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
			"Sep", "Oct", "Nov", "Dec" };
	int page_number = 1;
	Button button_LoadMore;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
//		analytics = GoogleAnalytics.getInstance(getActivity()); 
		tracker = Constants.setAnalyticsTracker(getActivity(), tracker);//easyTracker.set(Fields.SCREEN_NAME, TAG);
		vi = inflater.inflate(R.layout.status_activity, container, false);

		btnCalender = (Button) vi.findViewById(R.id.btnCalender);
		btnPreWeek = (Button) vi.findViewById(R.id.btnPreWeek);
		btnNextWeek = (Button) vi.findViewById(R.id.btnNextWeek);
		mDateDisplay = (TextView) vi.findViewById(R.id.mDateDisplay);
		mStatusListView = (ListView) vi.findViewById(R.id.mStatusListView);
		TextView textView_NoData = (TextView) vi
				.findViewById(R.id.textView_NoData);
		mStatusListView.setEmptyView(textView_NoData);
		button_LoadMore = new Button(getActivity());
		button_LoadMore.setHeight(50);
		button_LoadMore
				.setBackgroundResource(R.drawable.button_background_unselected);
		button_LoadMore.setText("Load more.....");
		button_LoadMore.setVisibility(View.GONE);
		button_LoadMore.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// // Log.e("Last post ID ", "" + lastPostID);
				button_LoadMore.setText("Loading.....");
				page_number++;
				new GetTransactionStatus().execute();
			}
		});

		mStatusListView.addFooterView(button_LoadMore);
		bundle = getActivity().getIntent().getExtras();
		mSettingsData = getActivity().getSharedPreferences(
				SettingsSmsActivity.SETTINGS_NAME, 0);
		bNetOrSms = mSettingsData.getBoolean(SettingsSmsActivity.KEY_NETWORK,
				true);

		

		return vi;

	}
	public static GoogleAnalytics analytics; public static Tracker tracker;	
	
	@Override
	public void setMenuVisibility(final boolean visible) {
		super.setMenuVisibility(visible);
		if (visible) {
			if (bNetOrSms) {
				if (Utility.getLoginFlag(getActivity(),
						Constants.SHAREDPREFERENCE_IS_LOGIN)) {
					mInsideonCreate();

				} else {
					Intent intent = new Intent(getActivity(), LoginActivity.class);
					intent.putExtra(Constants.REQUEST_FOR, bundle);
					startActivityForResult(intent, Constants.LOGIN);

				}
			} else {
				// mInsideonCreate();
				Constants
						.showTwoButtonFailureDialog(
								getActivity(),
								"Internet setting is not enabled.\nWould you like to change settings?",
								"Settings", Constants.SMS_SETTINGS);
			}
		}
	}
	
	@Override
		public void onStart() {
			super.onStart();
			// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
			// EasyTracker.getInstance(this).activityStart(this);
			//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
		}
	public static TransactionMobileBillFragment newInstance(String content) {
		TransactionMobileBillFragment fragment = new TransactionMobileBillFragment();

		Bundle b = new Bundle();
		b.putString("msg", content);

		fragment.setArguments(b);

		return fragment;
	}

	// private String mContent = "???";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// if ((savedInstanceState != null)
		// && savedInstanceState.containsKey(KEY_CONTENT)) {
		// mContent = savedInstanceState.getString(KEY_CONTENT);
		// }
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// outState.putString(KEY_CONTENT, mContent);
	}

	private void mInsideonCreate() {
		// TODO Auto-generated method stub

		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);

		btnCalender.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// getActivity().showDialog(DATE_DIALOG_ID);
				DatePickerDialog dialog = new DatePickerDialog(getActivity(),
						datePickerListener, mYear, mMonth, mDay);
				dialog.show();
			}
		});

		btnPreWeek.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					Date selected_date = new Date(mDateDisplay.getText()
							.toString().trim());

					Calendar c = Calendar.getInstance();
					c.set((1900 + selected_date.getYear()),
							selected_date.getMonth(), selected_date.getDate());
					c.add(Calendar.DATE, -1);
					// dateFormat.format(c.getTime());

					// Show current date

					mDay = c.get(Calendar.DAY_OF_MONTH);
					mMonth = c.get(Calendar.MONTH);
					mYear = c.get(Calendar.YEAR);

					updateDisplay();
				} catch (Exception e) {

				}
			}
		});

		btnNextWeek.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					Date selected_date = new Date(mDateDisplay.getText()
							.toString().trim());

					Calendar c = Calendar.getInstance();
					c.set((1900 + selected_date.getYear()),
							selected_date.getMonth(), selected_date.getDate());
					c.add(Calendar.DATE, +1);
					// dateFormat.format(c.getTime());

					// Show current date

					mDay = c.get(Calendar.DAY_OF_MONTH);
					mMonth = c.get(Calendar.MONTH);
					mYear = c.get(Calendar.YEAR);

					updateDisplay();
				} catch (Exception e) {

				}
			}
		});

		statusList = new ArrayList<String>();
		adapter = new StatusAdapter(getActivity(), statusList, 1);
		/*
		 * if (bundle.getInt(Constants.REQUEST_FOR) == Constants.MOBILE_TAB) {
		 * adapter = new StatusAdapter(getActivity(), statusList, 1); } else if
		 * (bundle.getInt(Constants.REQUEST_FOR) == Constants.DTH_TAB) { adapter
		 * = new StatusAdapter(getActivity(), statusList, 2); } else if
		 * (bundle.getInt(Constants.REQUEST_FOR) == Constants.ENTERTAINMENT_TAB)
		 * { adapter = new StatusAdapter(getActivity(), statusList, 1); } else
		 * if (bundle.getInt(Constants.REQUEST_FOR) == Constants.BILL_TAB) {
		 * adapter = new StatusAdapter(getActivity(), statusList, 1); }
		 */
		mStatusListView.setAdapter(adapter);

		updateDisplay();
	}

	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			// Do whatever you want
			// updateDisplay();

			if (view.isEnabled() && view.isShown()) {
				mYear = selectedYear;
				mMonth = selectedMonth;
				mDay = selectedDay;

				updateDisplay();
			}
		}
	};

	private void updateDisplay() {
		String m = "";
		String dt = "";
		if (mDay < 10)
			dt = "0" + mDay;
		else
			dt = mDay + "";
		if ((mMonth + 1) < 10)
			m = "0" + (mMonth + 1);
		else
			m = (mMonth + 1) + "";
		mDateDisplay.setText(dt + "-" + months[mMonth] + "-" + mYear);
		builder = new StringBuilder().append(dt).append("-").append(m)
				.append("-").append(mYear);
		page_number = 1;
		statusList.clear();
		new GetTransactionStatus().execute();
	}

	public class GetTransactionStatus extends AsyncTask<String, String, String>
			implements OnDismissListener {

		ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair.add(new BasicNameValuePair("method",
					"lastTransactions"));
			listValuePair
					.add(new BasicNameValuePair("date", builder.toString()));
			listValuePair.add(new BasicNameValuePair("date2", builder
					.toString()));
			/*
			 * if (bundle.getInt(Constants.REQUEST_FOR) == Constants.MOBILE_TAB)
			 * { listValuePair.add(new BasicNameValuePair("service", "1")); }
			 * else if (bundle.getInt(Constants.REQUEST_FOR) ==
			 * Constants.DTH_TAB) { listValuePair.add(new
			 * BasicNameValuePair("service", "2")); } else if
			 * (bundle.getInt(Constants.REQUEST_FOR) ==
			 * Constants.ENTERTAINMENT_TAB) { listValuePair.add(new
			 * BasicNameValuePair("service", "3")); } else if
			 * (bundle.getInt(Constants.REQUEST_FOR) == Constants.BILL_TAB) {
			 * listValuePair.add(new BasicNameValuePair("service", "4")); }
			 */
			listValuePair.add(new BasicNameValuePair("service", "4"));
			listValuePair.add(new BasicNameValuePair("page", "" + page_number));
			listValuePair.add(new BasicNameValuePair("items_per_page", "10"));
			listValuePair.add(new BasicNameValuePair("device_type", "android"));
			String response = RequestClass.getInstance().readPay1Request(
					getActivity(), Constants.API_URL, listValuePair);
			listValuePair.clear();
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			super.onPostExecute(result);
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = array.getJSONObject(0);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONArray jsonDescription = jsonObject
								.getJSONArray("description");
						JSONArray array2 = jsonDescription.getJSONArray(0);
						int len = 0;
						for (int i = 0; i < array2.length(); i++) {
							JSONObject json_data = array2.getJSONObject(i);

							if (i + 1 < array2.length()) {
								int t_id = 0, t_nextid = 0;
								try {
									t_id = Integer
											.parseInt(new JSONObject(
													json_data
															.getString("vendors_activations"))
													.getString("id"));

									JSONObject jobj_next = new JSONObject(
											array2.get(i + 1).toString());

									t_nextid = Integer
											.parseInt(new JSONObject(
													jobj_next
															.getString("vendors_activations"))
													.getString("id"));
									if (t_id == t_nextid) {
										i = i + 1;
									}
								} catch (Exception e) {
									// TODO: handle exception
								}
							}

							statusList.add(json_data.toString());
							len++;
						}
						if (len < 10) {
							button_LoadMore.setVisibility(View.GONE);
						} else {
							button_LoadMore.setText("Load more history");
							button_LoadMore.setVisibility(View.VISIBLE);
						}
						adapter.notifyDataSetChanged();
					} else {
						Constants.showOneButtonFailureDialog(getActivity(),
								Constants.checkCode(replaced), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(getActivity(),
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(getActivity(),
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
				button_LoadMore.setVisibility(View.GONE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(getActivity(),
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
				button_LoadMore.setVisibility(View.GONE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(getActivity());
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							GetTransactionStatus.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			GetTransactionStatus.this.cancel(true);
			dialog.cancel();
		}
	}
}
