package com.mindsarray.pay1;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.mindsarray.pay1.adapterhandler.SettingsAdapter;
import com.mindsarray.pay1.constant.Constants;

public class SettingsMainActivity extends Activity {
	ArrayList<String> settingsList;
	ListView listViewSettings;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings_main_actvity);
		settingsList = new ArrayList<String>();
		settingsList.add("Recharge Mode");
		settingsList.add("Language Selection");
		settingsList.add("Change Mobile No.");
		settingsList.add("Debug");
		settingsList.add("About");

		listViewSettings = (ListView) findViewById(R.id.listViewSettings);
		SettingsAdapter settingsAdapter = new SettingsAdapter(
				SettingsMainActivity.this, 0, settingsList);
		listViewSettings.setAdapter(settingsAdapter);
		listViewSettings.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {

				switch (arg2) {
				case 0:
					Intent intent = new Intent(SettingsMainActivity.this,
							SettingsSmsActivity.class);
					startActivity(intent);
					break;
				case 1:
					Intent intent1 = new Intent(SettingsMainActivity.this,
							LanguageSelectionActivity.class);
					startActivity(intent1);
					break;
				case 2:
					Intent intent2 = new Intent(SettingsMainActivity.this,
							ChangeMobileNumberActivity.class);
					startActivity(intent2);
					break;
				case 3:
					Intent intent3 = new Intent(SettingsMainActivity.this,
							SettingsDebugActivity.class);
					startActivity(intent3);
					break;
				case 4:
					Intent intent4 = new Intent(SettingsMainActivity.this,
							AboutActivity.class);
					startActivity(intent4);
					break;

				default:
					break;
				}
			}
		});

	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants.showNavigationBar(SettingsMainActivity.this);

		super.onResume();
	}

}
