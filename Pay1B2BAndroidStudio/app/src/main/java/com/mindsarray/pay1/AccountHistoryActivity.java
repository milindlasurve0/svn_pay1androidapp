package com.mindsarray.pay1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.R.id;
import com.mindsarray.pay1.adapterhandler.AccountHistoryAdapter;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

public class AccountHistoryActivity extends Activity {

	static final int DATE_DIALOG_ID = 0;
	ListView mAccountHistoryListView;
	TextView mDateDisplay;
	Button btnCalender, btnPreWeek, btnNextWeek;
	ArrayList<HashMap<String, String>> mAccountHistoryList;
	AccountHistoryAdapter adapter;
	private int mYear;
	private int mMonth;
	private int mDay;
	StringBuilder builder;
	private static final String TAG = "Account History";
	public static final String PERTICULAR = "particular";
	public static final String REFID = "refid";
	public static final String DEBIT = "debit";
	public static final String CREDIT = "credit";
	public static final String OPENING = "opening";
	public static final String CLOSING = "closing";
	public static final String TIME = "time";
	public static final String CONFIRM_FLAG = "flag";
	public static final String TYPE = "type";

	String months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
			"Sep", "Oct", "Nov", "Dec" };
	int page_number = 1;
	Button button_LoadMore;
//	public static GoogleAnalytics analytics; public static Tracker tracker;
	
	public static GoogleAnalytics analytics;
	public static Tracker tracker;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);

		
		setContentView(R.layout.accounthistory_activity);
//		tracker = Constants.setAnalyticsTracker(this, tracker);
//		//easyTracker.set(Fields.SCREEN_NAME, TAG);
		//analytics = GoogleAnalytics.getInstance(this);
		tracker = Constants.setAnalyticsTracker(this, tracker);
		
		mAccountHistoryListView = (ListView) findViewById(R.id.mAccountHistoryListView);
		TextView textView_NoData = (TextView) findViewById(R.id.textView_NoData);
		mAccountHistoryListView.setEmptyView(textView_NoData);
		button_LoadMore = new Button(AccountHistoryActivity.this);
		button_LoadMore.setHeight(50);
		button_LoadMore
				.setBackgroundResource(R.drawable.button_background_unselected);
		button_LoadMore.setText("Load more history");
		button_LoadMore.setVisibility(View.GONE);
		button_LoadMore.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// // Log.e("Last post ID ", "" + lastPostID);
				button_LoadMore.setText("Loading.....");
				page_number++;
				new GetAccountHistorTask().execute();
			}
		});

		mAccountHistoryListView.addFooterView(button_LoadMore);
		mDateDisplay = (TextView) findViewById(id.mDateDisplay);
		btnCalender = (Button) findViewById(R.id.btnCalender);
		btnPreWeek = (Button) findViewById(R.id.btnPreWeek);
		btnNextWeek = (Button) findViewById(R.id.btnNextWeek);

		btnCalender.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showDialog(DATE_DIALOG_ID);
			}
		});

		btnPreWeek.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					Date selected_date = new Date(mDateDisplay.getText()
							.toString().trim());

					Calendar c = Calendar.getInstance();
					c.set((1900 + selected_date.getYear()),
							selected_date.getMonth(), selected_date.getDate());
					c.add(Calendar.DATE, -1);
					// dateFormat.format(c.getTime());

					// Show current date

					mDay = c.get(Calendar.DAY_OF_MONTH);
					mMonth = c.get(Calendar.MONTH);
					mYear = c.get(Calendar.YEAR);

					updateDisplay();
				} catch (Exception e) {

				}
			}
		});

		btnNextWeek.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					Date selected_date = new Date(mDateDisplay.getText()
							.toString().trim());

					Calendar c = Calendar.getInstance();
					c.set((1900 + selected_date.getYear()),
							selected_date.getMonth(), selected_date.getDate());
					c.add(Calendar.DATE, +1);
					// dateFormat.format(c.getTime());

					// Show current date

					mDay = c.get(Calendar.DAY_OF_MONTH);
					mMonth = c.get(Calendar.MONTH);
					mYear = c.get(Calendar.YEAR);

					updateDisplay();
				} catch (Exception e) {

				}
			}
		});

		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);
		mAccountHistoryList = new ArrayList<HashMap<String, String>>();
		adapter = new AccountHistoryAdapter(AccountHistoryActivity.this,
				mAccountHistoryList);
		mAccountHistoryListView.setAdapter(adapter);

		Constants.showNavigationBar(AccountHistoryActivity.this);
		if (Utility.getLoginFlag(AccountHistoryActivity.this,
				Constants.SHAREDPREFERENCE_IS_LOGIN)) {
			updateDisplay();

		} else {
			Intent intent = new Intent(AccountHistoryActivity.this,
					LoginActivity.class);

			startActivityForResult(intent, Constants.LOGIN);

		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == Constants.LOGIN) {
				updateDisplay();
			} else {

			}
		} else {
			finish();
		}
	}

	private void updateDisplay() {
		String m = "";
		String dt = "";
		if (mDay < 10)
			dt = "0" + mDay;
		else
			dt = mDay + "";
		if ((mMonth + 1) < 10)
			m = "0" + (mMonth + 1);
		else
			m = (mMonth + 1) + "";
		builder = new StringBuilder().append(dt).append(m).append(mYear);
		mDateDisplay.setText(dt + "-" + months[mMonth] + "-" + mYear);
		String d = builder.toString();
		builder = new StringBuilder().append(d).append("-").append(d);
		mAccountHistoryList.clear();
		adapter.notifyDataSetChanged();
		page_number = 1;
		new GetAccountHistorTask().execute();
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {

			if (view.isEnabled() && view.isShown()) {
				mYear = year;
				mMonth = monthOfYear;
				mDay = dayOfMonth;

				updateDisplay();
			}
		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			DatePickerDialog dialog = new DatePickerDialog(this,
					mDateSetListener, mYear, mMonth, mDay);

			/*
			 * if (Build.VERSION.SDK_INT >= 11) { // (picker is a DatePicker)
			 * dialog.getDatePicker().setMaxDate(new Date().getTime()); } else {
			 * 
			 * }
			 */

			return dialog;
			/*
			 * return new DatePickerDialog(this, mDateSetListener, mYear,
			 * mMonth, mDay);
			 */
		}
		return null;
	}

	public class GetAccountHistorTask extends AsyncTask<String, String, String>
			implements OnDismissListener {

		ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair
					.add(new BasicNameValuePair("method", "ledgerBalance"));
			listValuePair
					.add(new BasicNameValuePair("date", builder.toString()));
			listValuePair.add(new BasicNameValuePair("page", "" + page_number));
			listValuePair.add(new BasicNameValuePair("limit", "10"));
			listValuePair.add(new BasicNameValuePair("device_type", "android"));
			String response = RequestClass.getInstance().readPay1Request(
					AccountHistoryActivity.this, Constants.API_URL,
					listValuePair);
			listValuePair.clear();
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			super.onPostExecute(result);
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					// mAccountHistoryList.clear();
					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = array.getJSONObject(0);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						JSONArray jsonDescription = jsonObject
								.getJSONArray("description");
						JSONObject jsonOb = jsonDescription.getJSONObject(0);
						JSONArray arrayTransaction = jsonOb
								.getJSONArray("transactions");
						for (int i = 0; i < arrayTransaction.length(); i++) {
							JSONObject json_data = arrayTransaction
									.getJSONObject(i);
							JSONObject json_trans = json_data
									.getJSONObject("transactions");
							HashMap<String, String> map = new HashMap<String, String>();
							map.put(CONFIRM_FLAG,
									json_trans.getString("confirm_flag"));
							map.put(PERTICULAR, json_trans.getString("name"));
							map.put(REFID, json_trans.getString("refid"));
							map.put(CREDIT, json_trans.getString("credit"));
							map.put(DEBIT, json_trans.getString("debit"));
							map.put(TIME, json_trans.getString("timestamp"));
							map.put(TYPE, json_trans.getString("type"));

							JSONObject json_OC = json_data
									.getJSONObject("opening_closing");
							map.put(OPENING, json_OC.getString("opening"));
							map.put(CLOSING, json_OC.getString("closing"));

							mAccountHistoryList.add(map);

						}
						if (arrayTransaction.length() < 10) {
							button_LoadMore.setVisibility(View.GONE);
						} else {
							button_LoadMore.setText("Load more history");
							button_LoadMore.setVisibility(View.VISIBLE);
						}
						adapter.notifyDataSetChanged();
					} else {
						Constants.showOneButtonFailureDialog(
								AccountHistoryActivity.this,
								Constants.checkCode(replaced), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(
							AccountHistoryActivity.this,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(
						AccountHistoryActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
				button_LoadMore.setVisibility(View.GONE);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(
						AccountHistoryActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
				button_LoadMore.setVisibility(View.GONE);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(AccountHistoryActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							GetAccountHistorTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			GetAccountHistorTask.this.cancel(true);
			dialog.cancel();
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
		// EasyTracker.getInstance(this).activityStart(this);
//		//tracker.setScreenName(TAG); Constants.trackOnStart(tracker, TAG);
		
		//tracker.setScreenName(TAG);
		Constants.trackOnStart(tracker, TAG);
			//.build());
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}
}
