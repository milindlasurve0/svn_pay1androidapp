package com.mindsarray.pay1.adapterhandler;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindsarray.pay1.ImagesSlideActivity;
import com.mindsarray.pay1.R;
import com.mindsarray.pay1.WholesalerProfileActivity;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.contenthandler.Post;
import com.mindsarray.pay1.customviews.ExpandableTextView;
import com.mindsarray.pay1.databasehandler.PostDBHelper;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.GoogleAnalyticsHelper;
import com.mindsarray.pay1.utilities.Utility;
import com.squareup.picasso.Picasso;

public class PostsAdapter extends ArrayAdapter<Post> {
	Context mContext;
	ArrayList<Post> mPosts;
	// LayoutInflater inflater;
	int PostIdUpdate;
	OnClickListener onClickListener;
	GoogleAnalyticsHelper mGoogleHelper;
	// Post post;
	PostDBHelper dbHelper;

	public PostsAdapter(Context mContext, ArrayList<Post> posts,
			OnClickListener onClickListener) {
		super(mContext, 0, posts);
		this.onClickListener = onClickListener;
		this.mContext = mContext;
		this.mPosts = posts;
		InitGoogleAnalytics();
		dbHelper = new PostDBHelper(mContext);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;

		try {
			ViewHolder viewHolder = null;
			if (view == null) {
				viewHolder = new ViewHolder();
				/*
				 * LayoutInflater inflater = (LayoutInflater) mContext
				 * .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				 */
				LayoutInflater inflater = ((Activity) mContext)
						.getLayoutInflater();
				view = inflater.inflate(R.layout.posts_adapter, parent, false);
				viewHolder.btn_wholesale = (Button) view
						.findViewById(R.id.btn_wholesale);
				viewHolder.btn_make_a_call = (Button) view
						.findViewById(R.id.btn_make_a_call);
				viewHolder.btn_interested = (ImageButton) view
						.findViewById(R.id.btn_interested);

				viewHolder.post_images = (ImageView) view
						.findViewById(R.id.post_images);
				viewHolder.wholesalerLogo = (ImageView) view
						.findViewById(R.id.wholesaler_logo);
				// viewHolder.wholesalerName = (TextView)
				// view.findViewById(R.id.wholesaler_name);
				viewHolder.textWholesaler = (TextView) view
						.findViewById(R.id.textWholesaler);
				viewHolder.textWholesalerCatgory = (TextView) view
						.findViewById(R.id.textWholesalerCatgory);
				viewHolder.postDescription = (ExpandableTextView) view
						.findViewById(R.id.post_description);

				view.setTag(viewHolder);
			} else {
				// view = convertView;
				viewHolder = (ViewHolder) view.getTag();
			}

			final Post post = mPosts.get(position);
			final String getImages = post.getmPostImages();
			final String getClientId = post.getmClientId();
			final String interested = post.getmIsInterested();
			// ViewHolder viewHolder = (ViewHolder) view.getTag();

			// if (post.mPostImages.size() != 0) {

			if (!interested.equalsIgnoreCase("0")) {
				/*
				 * viewHolder.btn_interested.setText("INTERESTED");
				 * viewHolder.btn_interested.setTextColor(Color.WHITE);
				 * viewHolder.btn_interested.setBackgroundColor(mContext
				 * .getResources().getColor(R.color.pay1_red));
				 * 
				 * Drawable d = mContext.getResources().getDrawable(
				 * R.drawable.white_heart_icon); d.setBounds(new Rect(0, 0, 0,
				 * 0)); viewHolder.btn_interested
				 * .setCompoundDrawablesWithIntrinsicBounds(d, null, null,
				 * null);
				 */
				viewHolder.btn_interested.setImageDrawable(mContext
						.getResources().getDrawable(R.drawable.selected_heart));
				viewHolder.btn_interested.setEnabled(false);

			} else {
				viewHolder.btn_interested.setImageDrawable(mContext
						.getResources()
						.getDrawable(R.drawable.unselected_heart));
				viewHolder.btn_interested.setEnabled(true);
			}

			viewHolder.post_images.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Log.d("position", "position  " + position);
					try {
						mGoogleHelper.SendEventGoogleAnalytics(
								mContext,
								String.valueOf(post.getmId()) + " "
										+ post.getmPostTitle(),
								"Post Image Clicked", "ButtonEvent");
						dbHelper.addStatsToTable(post.getmId(), Utility
								.getSharedPreferences(mContext,
										Constants.SHAREDPREFERENCE_RETAILER_ID));

						ArrayList<String> imageArrayList = new ArrayList<String>();
						try {
							JSONArray imageArray = new JSONArray(getImages);
							for (int i = 0; i < imageArray.length(); i++) {
								imageArrayList.add(imageArray.getString(i));
							}
						} catch (Exception e) {
							e.printStackTrace();
						}

						Intent slideIntent = new Intent(mContext,
								ImagesSlideActivity.class);

						Bundle b = new Bundle();
						b.putInt("position", 0);
						b.putStringArrayList("Array", imageArrayList);
						b.putInt("requestFrom", 0);
						slideIntent.putExtras(b);
						mContext.startActivity(slideIntent);

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			try {
				JSONArray imageArray = new JSONArray(getImages);

				Picasso.with(mContext).load(imageArray.get(0).toString())
						.placeholder(R.drawable.product_loading)
						.into(viewHolder.post_images);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// }
			// if (viewHolder.wholesalerLogo != null) {
			try {
				Picasso.with(mContext).load(post.getmWholesalerLogo())
						.placeholder(R.drawable.default_logo)
						.into(viewHolder.wholesalerLogo);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// }

			viewHolder.btn_wholesale.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mGoogleHelper.SendEventGoogleAnalytics(
							mContext,
							String.valueOf(post.getmId()) + " "
									+ post.getmPostTitle(),
							"Wholesale Button Clicked", "ButtonEvent");
					Intent intent = new Intent(mContext,
							WholesalerProfileActivity.class);
					intent.putExtra("wholeSalerId", getClientId);
					mContext.startActivity(intent);
				}
			});

			viewHolder.wholesalerLogo.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mGoogleHelper.SendEventGoogleAnalytics(
							mContext,
							String.valueOf(post.getmId()) + " "
									+ post.getmPostTitle(),
							"Wholesale Logo Clicked", "ButtonEvent");
					Intent intent = new Intent(mContext,
							WholesalerProfileActivity.class);
					intent.putExtra("wholeSalerId", getClientId);
					mContext.startActivity(intent);
				}
			});

			viewHolder.btn_interested.setTag(position);
			viewHolder.btn_interested.setOnClickListener(onClickListener);

			/*
			 * viewHolder.btn_interested.setOnClickListener(new
			 * OnClickListener() {
			 * 
			 * @Override public void onClick(View v) {
			 * ShowInterestDialog(post.getmId(),viewHolder.btn_interested);
			 * 
			 * } });
			 */

			viewHolder.btn_make_a_call
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {

							mGoogleHelper.SendEventGoogleAnalytics(
									mContext,
									String.valueOf(post.getmId()) + " "
											+ post.getmPostTitle(),
									"Call Button Clicked", "ButtonEvent");


							String permission = "android.permission.CALL_PHONE";
							int res = mContext.checkCallingOrSelfPermission(permission);
							if (res == PackageManager.PERMISSION_GRANTED) {

								Intent callIntent = new Intent(Intent.ACTION_CALL);
								callIntent.setData(Uri.parse("tel:" + post.getmContactNo()));
								mContext.startActivity(callIntent);
							}
/*
							showTwoButtonDialog(
									mContext,
									"Do you want the wholesaler to call you on "
											+ Utility
													.getUserMobileNumber(mContext)
											+ " ?", "Confirmation", "Yes",
									"No", 0, post.getmId(),
									post.getmClientId(), post.getmContactNo());*/

						}
					});

			viewHolder.textWholesaler.setText(post.getmWholesalerName());

			viewHolder.postDescription.setText(post.getmPostDescription());

		} catch (Exception exc) {

		}
		// view.setTag(viewHolder);
		return view;
	}

	private class ViewHolder {
		ImageView post_images, wholesalerLogo;
		ImageButton btn_interested;
		TextView textWholesaler, textWholesalerCatgory;
		ExpandableTextView postDescription;
		Button btn_wholesale, btn_make_a_call;
	}

	public class ClickToCallWholesalerTask extends
			AsyncTask<String, String, String> implements OnDismissListener {
		private static final String TAG = "C2D";
		ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair.add(new BasicNameValuePair("retailerID", Utility
					.getSharedPreferences(mContext,
							Constants.SHAREDPREFERENCE_RETAILER_ID)));
			listValuePair.add(new BasicNameValuePair("postId", params[0]));
			listValuePair.add(new BasicNameValuePair("retailermobile", Utility
					.getUserMobileNumber(mContext)));
			listValuePair.add(new BasicNameValuePair("wsmobile", params[2]));
			listValuePair
					.add(new BasicNameValuePair("wholesaler_id", params[1]));

			String response = RequestClass.getInstance().readPay1Request(
					mContext, Constants.C2D_URL + "posts/clickToCall",
					listValuePair);
			listValuePair.clear();

			Log.e("C2D Posts:", "" + response);

			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			super.onPostExecute(result);
			try {
				if (!result.startsWith("Error")) {

					JSONObject responseObject = new JSONObject(result);

					String status = responseObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						if (responseObject.getBoolean("type")) {

							Constants.showOneButtonSuccessDialog(mContext,
									responseObject.getString("msg"),
									Constants.C2D_SETTINGS);

							/*
							 * PostDBHelper postDBHelper = new PostDBHelper(
							 * mContext); postDBHelper.UpdatePost(PostIdUpdate);
							 * notifyDataSetChanged();
							 */} else {
							Constants.showOneButtonFailureDialog(mContext,
									responseObject.getString("errorMsg"), TAG,
									Constants.OTHER_ERROR);
						}
					} else {
						Constants.showOneButtonFailureDialog(mContext,
								Constants.checkCode(result), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(mContext,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(mContext,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				Constants.showOneButtonFailureDialog(mContext,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(mContext);
			this.dialog.setMessage("Connecting...");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							ClickToCallWholesalerTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			ClickToCallWholesalerTask.this.cancel(true);
			dialog.cancel();
		}
	}

	public class ShowInterestTask extends AsyncTask<String, String, String>
			implements OnDismissListener {
		private static final String TAG = "C2D";
		ProgressDialog dialog;
		Button btn_interested;

		public ShowInterestTask(Button button) {
			super();
			// TODO Auto-generated constructor stub
			this.btn_interested = button;
		}

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair.add(new BasicNameValuePair("retailerID", Utility
					.getSharedPreferences(mContext,
							Constants.SHAREDPREFERENCE_RETAILER_ID)));
			listValuePair.add(new BasicNameValuePair("postId", params[0]));
			listValuePair.add(new BasicNameValuePair("message", params[1]));

			String response = RequestClass.getInstance().readPay1Request(
					mContext, Constants.C2D_URL + "posts/interests",
					listValuePair);
			listValuePair.clear();

			Log.e("C2D Posts:", "" + response);

			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			super.onPostExecute(result);
			try {
				if (!result.startsWith("Error")) {

					JSONObject responseObject = new JSONObject(result);

					String status = responseObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						if (responseObject.getBoolean("type")) {
							Constants.showOneButtonSuccessDialog(mContext,
									"Request sent", Constants.C2D_SETTINGS);
							PostDBHelper postDBHelper = new PostDBHelper(
									mContext);
							postDBHelper.UpdatePost(PostIdUpdate);

							this.btn_interested.setTextColor(Color.WHITE);
							this.btn_interested.setBackgroundColor(mContext
									.getResources().getColor(R.color.pay1_red));

							Drawable d = mContext.getResources().getDrawable(
									R.drawable.white_heart_icon);
							d.setBounds(new Rect(0, 0, 0, 0));
							this.btn_interested
									.setCompoundDrawablesWithIntrinsicBounds(d,
											null, null, null);
							this.btn_interested.setEnabled(false);
							notifyDataSetChanged();

						} else {
							Constants.showOneButtonFailureDialog(mContext,
									responseObject.getString("errorMsg"), TAG,
									Constants.OTHER_ERROR);
						}
					} else {
						Constants.showOneButtonFailureDialog(mContext,
								Constants.checkCode(result), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(mContext,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(mContext,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				Constants.showOneButtonFailureDialog(mContext,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(mContext);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							ShowInterestTask.this.cancel(true);
							dialog.cancel();
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			ShowInterestTask.this.cancel(true);
			dialog.cancel();
		}
	}

	public void ShowInterestDialog(final int postId1, final Button button) {
		PostIdUpdate = postId1;
		final String postId = String.valueOf(postId1);
		final Dialog mDialog = new Dialog(mContext);
		mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mDialog.getWindow().setBackgroundDrawableResource(
				android.R.color.transparent);
		mDialog.setContentView(R.layout.show_interested_dialog);
		Button buttonInterest = (Button) mDialog
				.findViewById(R.id.buttonInterest);
		ImageView imageViewClose = (ImageView) mDialog
				.findViewById(R.id.imageViewClose);
		final EditText editTextMsg = (EditText) mDialog
				.findViewById(R.id.editTextMsg);
		editTextMsg.getHint();
		buttonInterest.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mDialog.dismiss();
				if (editTextMsg.getText().toString().length() != 0) {

					new ShowInterestTask(button).execute(postId, editTextMsg
							.getText().toString());
				} else {
					new ShowInterestTask(button).execute(postId, editTextMsg
							.getHint().toString());
				}
			}
		});

		imageViewClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mDialog.dismiss();
			}
		});

		mDialog.show();
	}

	private void showTwoButtonDialog(final Context context, String message,
			String title, String buttonOK, String buttonCancel, final int type,
			final int postId, final String wsId, final String wsContactNumber) {
		try {
			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.dialog_two_button);

			TextView textView_Title = (TextView) dialog
					.findViewById(R.id.textView_Title);
			TextView text = (TextView) dialog
					.findViewById(R.id.textView_Message);
			text.setText(message);
			textView_Title.setText(title);

			Button dialogButtonOk = (Button) dialog
					.findViewById(R.id.button_Ok);
			dialogButtonOk.setText(buttonOK);
			dialogButtonOk.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					new ClickToCallWholesalerTask().execute(
							String.valueOf(postId), wsId, wsContactNumber);
				}
			});

			Button dialogButtonCancel = (Button) dialog
					.findViewById(R.id.button_Cancel);
			dialogButtonCancel.setText(buttonCancel);
			dialogButtonCancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			dialog.show();
		} catch (Exception exception) {
		}
	}

	private void InitGoogleAnalytics() {
		mGoogleHelper = new GoogleAnalyticsHelper();
		mGoogleHelper.init(mContext);
	}
}