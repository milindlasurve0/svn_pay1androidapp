package com.mindsarray.pay1.utilities;


public class GridMenuItem {
	int image;
	String title;
	int id, mRechargeType;

	public GridMenuItem(int image, String title, int id, int mRechargeType) {
		super();
		this.image = image;
		this.title = title;
		this.id = id;
		this.mRechargeType = mRechargeType;
	}

	public int getmRechargeType() {
		return mRechargeType;
	}

	public void setmRechargeType(int mRechargeType) {
		this.mRechargeType = mRechargeType;
	}

	public GridMenuItem() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getImage() {
		return image;
	}

	public void setImage(int image) {
		this.image = image;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
