package com.mindsarray.pay1.servicehandler;

import java.util.ArrayList;
import java.util.Timer;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

public class ChatIntentService extends IntentService {
	public static final String TAG = "Chat service";
	private Timer myTimer = null;
	int counter = 0;

	public ChatIntentService() {
		super("CHAT_SERVICE");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		try {
			/*
			final Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(Utility.getLastSendingTime(ChatIntentService.this));
			java.util.Date rcvdate = cal.getTime();
			//int sendingHour = rcvdate.getHours();
			int sendingMinute = rcvdate.getMinutes();
			
			
			cal.setTimeInMillis(Utility.getLastRecieveTime(ChatIntentService.this));
			java.util.Date sndDate = cal.getTime();
			//int recievingHour = sndDate.getHours();
			int recievingMinute = sndDate.getMinutes();
			long diff=Utility.getLastSendingTime(ChatIntentService.this)-Utility.getLastRecieveTime(ChatIntentService.this);
			long diffMinutes = diff / (60 * 1000) % 60; 
			long diffSeconds = diff / 1000 % 60;
			*/
			long current=System.currentTimeMillis();
			
			long currentDiff=current-Utility.getLastSendingTime(ChatIntentService.this);
			long currentDiffSeconds=currentDiff / 1000 % 60;
			//if((currentDiffSeconds)>60){
			new SendMailTask().execute();
				
				//Toast.makeText(ChatIntentService.this, "Mail Sent", Toast.LENGTH_LONG).show();
				
				Log.d("Counter", "Counter    mail Sent");
				/*Utility.setLastSendingTime(ChatIntentService.this, current);
			}else{
			stopSelf();
			}*/
			
		} catch (Exception exception) {
		}
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		stopSelf();
	}

	public class SendMailTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair.add(new BasicNameValuePair("method",
					"ccNotResponding"));
			listValuePair.add(new BasicNameValuePair("support_id", Utility
					.getChatServiceID(ChatIntentService.this,
							Constants.SHAREDPREFERENCE_CHAT_SERVICE_ID)));
			listValuePair.add(new BasicNameValuePair("user", Utility
					.getMobileNumber(ChatIntentService.this,
							Constants.SHAREDPREFERENCE_MOBILE_NUMBER)));
			listValuePair.add(new BasicNameValuePair("time", Utility
					.getChatServiceTime(ChatIntentService.this,
							Constants.SHAREDPREFERENCE_CHAT_SERVICE_TIME)));
			listValuePair.add(new BasicNameValuePair("device_type", "android"));
			String response = RequestClass.getInstance().readPay1Request(
					ChatIntentService.this, Constants.API_URL, listValuePair);
			listValuePair.clear();
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Utility.setLastSendingTime(ChatIntentService.this, System.currentTimeMillis());
			try {
				Log.d("Counter", "Counter    mail Sent to admin");
				stopSelf();
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = array.getJSONObject(0);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

					} else {
						// Constants.showOneButtonFailureDialog(
						// ChatService.this,
						// Constants.checkCode(replaced), TAG,
						// Constants.OTHER_ERROR);
					}

				} else {
					// Constants.showOneButtonFailureDialog(ChatService.this,
					// Constants.ERROR_INTERNET, TAG,
					// Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				stopSelf();
				// e.printStackTrace();
				// Constants.showOneButtonFailureDialog(ChatService.this,
				// Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
				// Constants.OTHER_ERROR);
			} catch (Exception e) {
				stopSelf();
				// TODO: handle exception
				// Constants.showOneButtonFailureDialog(ChatService.this,
				// Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
				// Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}
	}

}
