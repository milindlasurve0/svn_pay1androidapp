package com.mindsarray.pay1;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mindsarray.pay1.SettingsUpdatePinActivity.UpdatPinTask;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.EditTextValidator;
import com.mindsarray.pay1.utilities.Utility;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class VerificationActivity extends Activity {
	ImageView imageView_Edit, imageView_Resend;
	EditText editText_OTP, editText_password, editText_confirm_password;
	Button button_Submit, button_Cancel;
	TextView textView_Mobile;
	String newPin, confirmPin, oldPin, userMobile;
	private static final String TAG = "Update pin";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.verification_activity);

		imageView_Edit = (ImageView) findViewById(R.id.imageView_Edit);
		imageView_Resend = (ImageView) findViewById(R.id.imageView_Resend);
		editText_OTP = (EditText) findViewById(R.id.editText_OTP);
		editText_password = (EditText) findViewById(R.id.editText_password);
		editText_confirm_password = (EditText) findViewById(R.id.editText_confirm_password);
		textView_Mobile = (TextView) findViewById(R.id.textView_Mobile);
		button_Submit = (Button) findViewById(R.id.button_Submit);
		button_Cancel = (Button) findViewById(R.id.button_Cancel);

		
		
		button_Cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				editText_OTP.setText("");
				editText_password.setText("");
				editText_confirm_password.setText("");
			}
		});
		
		imageView_Edit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		imageView_Resend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			userMobile = bundle.getString("userMobile");
			textView_Mobile.setText("+91 - "+userMobile);
		
		}

		button_Submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (userMobile == null) {
					return;
				}
				oldPin = editText_OTP.getText().toString().trim();
				confirmPin = editText_confirm_password.getText().toString()
						.trim();
				newPin = editText_password.getText().toString().trim();

				if (!checkValidation(VerificationActivity.this)) {

				} else {

					if (!newPin.equalsIgnoreCase(oldPin)) {
						if (newPin.equalsIgnoreCase(confirmPin)) {
							new UpdatPinTask().execute();
						} else {
							Constants.showOneButtonFailureDialog(
									VerificationActivity.this,
									"Password does not match.", "Wrong Pin", 8);
						}
					} else {

						Constants.showOneButtonFailureDialog(
								VerificationActivity.this,
								"OTP and New Password are same.", "Wrong Pin", 8);
					}

				}
			}

		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(VerificationActivity.this,
				RegistrationLayout.class);
		startActivity(intent);
	}

	public class UpdatPinTask extends AsyncTask<String, String, String>
			implements OnDismissListener {

		private ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair
					.add(new BasicNameValuePair("method", "updatePassword"));
			listValuePair.add(new BasicNameValuePair("mobileNo", userMobile));
			listValuePair.add(new BasicNameValuePair("otp", oldPin));
			listValuePair.add(new BasicNameValuePair("password", newPin));
			listValuePair
					.add(new BasicNameValuePair("confirm_password", confirmPin));
			listValuePair.add(new BasicNameValuePair("device_type", "android"));
			String response = RequestClass.getInstance()
					.readPay1Request(VerificationActivity.this,
							Constants.API_URL, listValuePair);
			listValuePair.clear();
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}

			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = array.getJSONObject(0);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						editText_confirm_password.setText("");
						editText_OTP.setText("");
						editText_password.setText("");
						Utility.setIsAppUsed(VerificationActivity.this, true);
						Utility.setUserMobileNumber(VerificationActivity.this, userMobile);
						Constants
								.showOneButtonSuccessDialog(
										VerificationActivity.this,
										"Password set Succesfully. Please Login Again.",
										Constants.CHANGE_PIN_SETTINGS);
						
						// finish();
					} else {
						Constants.showOneButtonFailureDialog(
								VerificationActivity.this,
								Constants.checkCode(replaced), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(
							VerificationActivity.this,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(VerificationActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(VerificationActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			dialog = new ProgressDialog(VerificationActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							UpdatPinTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			UpdatPinTask.this.cancel(true);
			dialog.cancel();
		}

	}

	private boolean checkValidation(Context context) {
		boolean ret = true;

		if (!EditTextValidator.hasText(context, editText_OTP,
				Constants.ERROR_PIN_BLANK_FIELD)) {
			ret = false;
			editText_OTP.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context, editText_password,
				Constants.ERROR_PIN_BLANK_FIELD)) {
			ret = false;
			editText_password.requestFocus();
			return ret;
		} else if (!EditTextValidator.hasText(context,
				editText_confirm_password, Constants.ERROR_PIN_BLANK_FIELD)) {
			ret = false;
			editText_confirm_password.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidPin(context, editText_OTP,
				Constants.ERROR_LOGIN_PIN_VALID_FIELD)) {
			ret = false;
			editText_OTP.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidPin(context, editText_password,
				Constants.ERROR_LOGIN_PIN_VALID_FIELD)) {
			ret = false;
			editText_password.requestFocus();
			return ret;
		} else if (!EditTextValidator.isValidPin(context,
				editText_confirm_password,
				Constants.ERROR_LOGIN_PIN_VALID_FIELD)) {
			ret = false;
			editText_confirm_password.requestFocus();
			return ret;
		} else
			return ret;
	}

}
