package com.mindsarray.pay1;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;
import com.squareup.picasso.Picasso;

public class ProceedCashPaymentActivity extends Activity {
	TextView textViewRupee, textViewRefId, textViewProvider,
			textViewExpiryDate,textViewMobNumber;
	Button btnPayment;
	String amount, ref_id, mobile_number,operator;
	ImageView imageLogo;
	private static final String TAG = "C2D Payments ";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.proceed_cash_payment);

		imageLogo = (ImageView) findViewById(R.id.imageLogo);
		textViewRupee = (TextView) findViewById(R.id.textViewRupee);
		textViewRefId = (TextView) findViewById(R.id.textViewRefId);
		textViewProvider = (TextView) findViewById(R.id.textViewProvider);
		textViewExpiryDate = (TextView) findViewById(R.id.textViewExpiryDate);
		textViewMobNumber = (TextView) findViewById(R.id.textViewMobNumber);
		btnPayment = (Button) findViewById(R.id.btnPayment);

		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			String details = bundle.getString("details");
			try {
				JSONObject detailsObject = new JSONObject(details);
				operator=detailsObject.getString("operator");
				amount = detailsObject.getString("amount");
				ref_id = detailsObject.getString("id");
				mobile_number = detailsObject.getString("mobile");
				textViewMobNumber.setText(mobile_number);
				try {
					Picasso.with(ProceedCashPaymentActivity.this)
							.load(detailsObject.getString("img_url"))
							.placeholder(R.drawable.ic_deafult_c2d).fit()
							.into(imageLogo);
				} catch (Exception e) {
				}

				textViewRupee.setText("Rs. "
						+ detailsObject.getString("amount").toUpperCase());

				textViewExpiryDate.setText("Expiry: "
						+ detailsObject.getString("expiry_time"));

				textViewRefId.setText(detailsObject.getString("id"));

				textViewProvider.setText(detailsObject
						.getString("company_name").toUpperCase());

			} catch (JSONException je) {

			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		btnPayment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				try {
					final Dialog dialog = new Dialog(
							ProceedCashPaymentActivity.this);
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.getWindow().setBackgroundDrawableResource(
							android.R.color.transparent);
					dialog.setContentView(R.layout.dialog_success_two_button);
					// dialog.setTitle(null);
					TextView textView_Title = (TextView) dialog
							.findViewById(R.id.textView_Title);
					textView_Title.setText("Online Payment");
					// set the custom dialog components - text, image
					// and button
					TextView textView_Message = (TextView) dialog
							.findViewById(R.id.textView_Message);
					textView_Message
							.setText("Are you sure \nyou want to continue?");

					ImageView closeButton = (ImageView) dialog
							.findViewById(R.id.imageView_Close);
					// if button is clicked, close the custom dialog
					closeButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							dialog.dismiss();
						}
					});

					Button button_Ok = (Button) dialog
							.findViewById(R.id.button_Ok);
					button_Ok.setText("Yes");
					// if button is clicked, close the custom dialog
					button_Ok.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							dialog.dismiss();
							new PayCashToRetailers().execute();
						}
					});

					Button button_Cancel = (Button) dialog
							.findViewById(R.id.button_Cancel);
					button_Cancel.setText("No");
					// if button is clicked, close the custom dialog
					button_Cancel
							.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									dialog.dismiss();
								}
							});

					dialog.show();
				} catch (Exception exception) {
				}

			}
		});

	}

	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants.showNavigationBar(ProceedCashPaymentActivity.this);
	
		super.onResume();
	}
	
	public class PayCashToRetailers extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(ProceedCashPaymentActivity.this);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(false);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							PayCashToRetailers.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();

		}

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair
					.add(new BasicNameValuePair("method", "cashpgPayment"));
			listValuePair.add(new BasicNameValuePair("amount", amount));
			listValuePair.add(new BasicNameValuePair("mobileNumber",
					mobile_number));
			listValuePair.add(new BasicNameValuePair("operator", operator));
			listValuePair.add(new BasicNameValuePair("ref_id", ref_id));
			listValuePair.add(new BasicNameValuePair("device_type", "android"));
			String response = RequestClass.getInstance().readPay1Request(
					ProceedCashPaymentActivity.this, Constants.API_URL,
					listValuePair);

			listValuePair.clear();
			return response;
		}

		private ProgressDialog dialog;

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();

			}

			try {
				if (!result.isEmpty()) {
					String replaced = "";
					if (!result.startsWith("Error")) {
						replaced = result.replace("(", "").replace(")", "")
								.replace(";", "");

						JSONArray array=new JSONArray(replaced);
						JSONObject jsonObject = array.getJSONObject(0);

						if (jsonObject.getString("status").equalsIgnoreCase(
								"success")) {

							Constants.showOneButtonSuccessDialog(
									ProceedCashPaymentActivity.this,
									"Request sent successfully.\nTransaction id : "
											+ jsonObject
													.getString("description"),
									Constants.DEAL_SETTINGS);
							
							String balance = jsonObject.getString("balance");
							Utility.setBalance(ProceedCashPaymentActivity.this,
									Constants.SHAREDPREFERENCE_BALANCE, balance);
							
							
						} else {
							Constants.showOneButtonFailureDialog(
									ProceedCashPaymentActivity.this,
									Constants.checkCode(replaced), TAG,
									Constants.OTHER_ERROR);
						}

					} else {
						Constants.showOneButtonFailureDialog(
								ProceedCashPaymentActivity.this,
								Constants.checkCode(replaced), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(
							ProceedCashPaymentActivity.this,
							Constants.ERROR_INTERNET, TAG,
							Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				// e.printStackTrace();
				Constants.showOneButtonFailureDialog(
						ProceedCashPaymentActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				// TODO: handle exception
				Constants.showOneButtonFailureDialog(
						ProceedCashPaymentActivity.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}

		}

	}

}
