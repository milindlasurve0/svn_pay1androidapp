package com.mindsarray.pay1;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MoneyTransferReceipt extends Activity {
	TextView textTransferStatus, textTransferInfo, textAmount, textNickName,
			textTransId, textSentFrom, textRemarks;
	Button buttonAddNewUser;
	ImageView imageGoback;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.money_transfer_reciept);
		findViewById();
		buttonAddNewUser.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		imageGoback.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		textTransferStatus.setText(getIntent().getExtras().getString(""));
		textTransferInfo.setText(getIntent().getExtras().getString(""));
		textAmount.setText(getIntent().getExtras().getString(""));
		textNickName.setText(getIntent().getExtras().getString(""));
		textTransId.setText(getIntent().getExtras().getString(""));
		textSentFrom.setText(getIntent().getExtras().getString(""));
		textRemarks.setText(getIntent().getExtras().getString(""));

	}

	private void findViewById() {
		// TODO Auto-generated method stub
		textTransferStatus = (TextView) findViewById(R.id.textTransferStatus);
		textTransferInfo = (TextView) findViewById(R.id.textTransferInfo);
		textAmount = (TextView) findViewById(R.id.textAmount);
		textNickName = (TextView) findViewById(R.id.textNickName);
		textTransId = (TextView) findViewById(R.id.textTransId);
		textSentFrom = (TextView) findViewById(R.id.textSentFrom);
		textRemarks = (TextView) findViewById(R.id.textRemarks);
		buttonAddNewUser = (Button) findViewById(R.id.buttonAddNewUser);
		imageGoback = (ImageView) findViewById(R.id.imageGoback);
	}
}
