package com.mindsarray.pay1;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mindsarray.pay1.LoginActivity.LoginTask;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class RegistrationLayout extends Activity {
	private static final String TAG = "Registration";
	EditText editText_mobileNumber;
	TextView textView_TC, textView_PP;
	Button button_Submit;
	String userMobile;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registration_layout);

		editText_mobileNumber = (EditText) findViewById(R.id.editText_mobileNumber);
		textView_TC = (TextView) findViewById(R.id.textView_TC);
		textView_PP = (TextView) findViewById(R.id.textView_PP);
		button_Submit = (Button) findViewById(R.id.button_Submit);

		button_Submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				userMobile = editText_mobileNumber.getText().toString();
				if (userMobile.length() == 10
						&& (userMobile.startsWith("7")
								|| userMobile.startsWith("8") || userMobile
									.startsWith("9"))) {
					new CheckUserLevel().execute();

				} else {
					
					int ecolor = R.color.black;
					ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
					SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
							"Enter correct number");
					ssbuilder.setSpan(fgcspan, 0, "Enter correct number".length(), 0);
					
					editText_mobileNumber.setError(ssbuilder);
				}
			}
		});
		
		
		editText_mobileNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub
				if (actionId == EditorInfo.IME_ACTION_GO) {
					try {
						// TODO Auto-generated method stub
						userMobile = editText_mobileNumber.getText().toString();
						if (userMobile.length() == 10
								&& (userMobile.startsWith("7")
										|| userMobile.startsWith("8") || userMobile
											.startsWith("9"))) {
							new CheckUserLevel().execute();

						} else {
							
							int ecolor = R.color.black;
							ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
							SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
									"Enter correct number");
							ssbuilder.setSpan(fgcspan, 0, "Enter correct number".length(), 0);
							
							editText_mobileNumber.setError(ssbuilder);
						}
					} catch (Exception e) {
						// e.printStackTrace();
					}
					return true;
				}
				return false;
			}
		});

	}

	public class CheckUserLevel extends AsyncTask<String, String, String> {

	
		ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();

			listValuePair
					.add(new BasicNameValuePair("method", "checkRetailerExist"));
			listValuePair.add(new BasicNameValuePair("mobileNo", userMobile));

			String response = RequestClass.getInstance().readPay1Request(
					RegistrationLayout.this, Constants.API_URL, listValuePair);
			listValuePair.clear();
			return response;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			dialog = new ProgressDialog(RegistrationLayout.this);
			dialog.setMessage("Please wait");
			dialog.show();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			try {
				if (!result.startsWith("Error")) {
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");
					JSONArray array = new JSONArray(replaced);
					JSONObject jsonObject = array.getJSONObject(0);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {
						Utility.setUserMobileNumber(RegistrationLayout.this, userMobile);
						String code=jsonObject.getString("code");
						
						if(code.equalsIgnoreCase("0")){
							Utility.setUserMobileNumber(RegistrationLayout.this, userMobile);
							Constants.showTwoButtonDialog(RegistrationLayout.this, "Your number is not registered with us. To signup with us click on Yes to continue.", "Registration", "YES", "NO", 1);
							
							
							/*Intent intent=new Intent(RegistrationLayout.this,RegistrationActivity.class);
							startActivity(intent);*/
						}else if(code.equalsIgnoreCase("2")){
							Utility.setUserMobileNumber(RegistrationLayout.this, userMobile);
							ShowDialogForOtp(RegistrationLayout.this, "OTP sent to your number", TAG);
						}else {
							Utility.setUserMobileNumber(RegistrationLayout.this, userMobile);
							Utility.setIsAppUsed(RegistrationLayout.this, true);
							Intent intent=new Intent(RegistrationLayout.this,LoginActivity.class);
							intent.putExtra("retailer_mobile", userMobile);
							startActivity(intent);
							finish();
						}
						
											
					} else {
						Constants.showOneButtonFailureDialog(
								RegistrationLayout.this,
								jsonObject.getString("description"), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(
							RegistrationLayout.this, Constants.ERROR_INTERNET,
							TAG, Constants.OTHER_ERROR);

				}
			} catch (JSONException e) {
				 e.printStackTrace();
				Constants.showOneButtonFailureDialog(RegistrationLayout.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				e.printStackTrace();
				Constants.showOneButtonFailureDialog(RegistrationLayout.this,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
			
		
		}
	}
	
	
	public void ShowDialogForOtp(Context mContext,String message,String title){
		final Dialog dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_failure_one_button);
		TextView textView_Title = (TextView) dialog
				.findViewById(R.id.textView_Title);
		TextView text = (TextView) dialog
				.findViewById(R.id.textView_Message);
		text.setText(message);
		textView_Title.setText(title);
		ImageView closeButton = (ImageView) dialog
				.findViewById(R.id.imageView_Close);
		closeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		Button dialogButtonOk = (Button) dialog
				.findViewById(R.id.button_Ok);
		dialogButtonOk.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				
				Intent intent=new Intent(RegistrationLayout.this,VerificationActivity.class);
				intent.putExtra("userMobile",userMobile);
				startActivity(intent);
			}
		});
		dialog.show();
	}
	
	
	

}
