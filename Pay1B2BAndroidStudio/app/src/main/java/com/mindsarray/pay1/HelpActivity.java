package com.mindsarray.pay1;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1.constant.Constants;

public class HelpActivity extends Activity implements OnClickListener {
	Button btnCall, btnSms, btnEmail;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.help_activity);
		findViewById();
		btnCall.setOnClickListener(this);
		btnSms.setOnClickListener(this);
		btnEmail.setOnClickListener(this);
		/*
		 * GCMIntentService gcmIntentService=new GCMIntentService(); Intent
		 * intent=new Intent(); intent.putExtra("Test", "Hi this test");
		 * gcmIntentService.onMessage(HelpActivity.this,intent);
		 */
//		 tracker = Constants.setAnalyticsTracker(this, tracker);
//		 easyTracker.set(Fields.SCREEN_NAME, "Help Activity");
		tracker = Constants.setAnalyticsTracker(this, tracker);
	}
	public static GoogleAnalytics analytics; public static Tracker tracker;	
	@Override
		public void onStart() {
			super.onStart();
			// Pay1GA.getGaTracker().send(MapBuilder.createAppView()//.build());
			// EasyTracker.getInstance(this).activityStart(this);
//			tracker.setScreenName("Help Activity"); 
			Constants.trackOnStart(tracker, "Help Activity");
		}

	private void findViewById() {
		// TODO Auto-generated method stub
		btnCall = (Button) findViewById(R.id.btnCall);
		btnSms = (Button) findViewById(R.id.btnSms);
		btnEmail = (Button) findViewById(R.id.btnEmail);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants.showNavigationBar(HelpActivity.this);
		super.onResume();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.btnCall) {
			Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.setData(Uri.parse("tel:+912261512288"));
			startActivity(callIntent);
		} else if (v.getId() == R.id.btnSms) {

			Intent sendIntent = new Intent(Intent.ACTION_VIEW);
			sendIntent.putExtra("sms_body", "PAY1 HELP");
			sendIntent.putExtra("address", "09223178889");
			sendIntent.setType("vnd.android-dir/mms-sms");
			startActivity(sendIntent);
		} else if (v.getId() == R.id.btnEmail) {
			String to = "help@pay1.in";
			String subject = "";
			String message = "";
			Intent email = new Intent(Intent.ACTION_SEND);
			email.putExtra(Intent.EXTRA_EMAIL, new String[] { to });
			email.putExtra(Intent.EXTRA_SUBJECT, subject);
			email.putExtra(Intent.EXTRA_TEXT, message);
			// need this to prompts email client only
			email.setType("message/rfc822");
			startActivity(Intent.createChooser(email, "Choose an Email client"));
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}
}
