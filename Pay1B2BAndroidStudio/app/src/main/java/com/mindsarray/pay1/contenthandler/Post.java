package com.mindsarray.pay1.contenthandler;

import java.util.ArrayList;

import org.json.JSONArray;

public class Post {
	
	private static final String TAG = "Post";
	
	//public ArrayList<String> mPostImages;
	public String mPostImages;
	public String mWholesalerLogo;
	public String mWholesalerName;
	public String mPostDescription;
	public String mPostTitle;
	public int mId;
	public String mClientId;
	public String mCategoryId;
	public String mTime;
	public String mContactNo;
	public String mCategoryName;
	public String mIsInterested;
	
	
	
	public String getmIsInterested() {
		return mIsInterested;
	}

	public void setmIsInterested(String mIsInterested) {
		this.mIsInterested = mIsInterested;
	}

	public String getmPostImages() {
		return mPostImages;
	}

	public void setmPostImages(String mPostImages) {
		this.mPostImages = mPostImages;
	}

	public String getmWholesalerLogo() {
		return mWholesalerLogo;
	}

	public void setmWholesalerLogo(String mWholesalerLogo) {
		this.mWholesalerLogo = mWholesalerLogo;
	}

	public String getmWholesalerName() {
		return mWholesalerName;
	}

	public void setmWholesalerName(String mWholesalerName) {
		this.mWholesalerName = mWholesalerName;
	}

	public String getmPostDescription() {
		return mPostDescription;
	}

	public void setmPostDescription(String mPostDescription) {
		this.mPostDescription = mPostDescription;
	}

	public String getmPostTitle() {
		return mPostTitle;
	}

	public void setmPostTitle(String mPostTitle) {
		this.mPostTitle = mPostTitle;
	}

	public int getmId() {
		return mId;
	}

	public void setmId(int mId) {
		this.mId = mId;
	}

	public String getmClientId() {
		return mClientId;
	}

	public void setmClientId(String mClientId) {
		this.mClientId = mClientId;
	}

	public String getmCategoryId() {
		return mCategoryId;
	}

	public void setmCategoryId(String mCategoryId) {
		this.mCategoryId = mCategoryId;
	}
	
	
	public String getmCategoryName() {
		return mCategoryName;
	}

	public void setmCategoryName(String mCategoryName) {
		this.mCategoryName = mCategoryName;
	}

	public String getmTime() {
		return mTime;
	}

	public void setmTime(String mTime) {
		this.mTime = mTime;
	}

	public String getmContactNo() {
		return mContactNo;
	}

	public void setmContactNo(String mContactNo) {
		this.mContactNo = mContactNo;
	}

	public static String getTag() {
		return TAG;
	}

	/*
	
	public Post(ArrayList<String> postImages, String wholesalerLogo, String wholesalerName, String postDescription,
			String postTitle, String id, String clientId, String categoryId, String time,String contactNo){
		mPostImages = postImages;
		mWholesalerLogo = wholesalerLogo;
		mWholesalerName = wholesalerName;
		mPostDescription = postDescription;
		mPostTitle = postTitle;
		mId = id;
		mClientId = clientId;
		mCategoryId = categoryId;
		mTime = time;
		mContactNo=contactNo;
	}
	*/
	/*public void setPostImages(ArrayList<String> postImages){
		mPostImages = postImages;
	} 
	
	public void setWholesalerLogo(String wholesalerLogo){
		mWholesalerLogo = wholesalerLogo;
	}
	
	public void setWholesalerName(String wholesalerName){
		mWholesalerName = wholesalerName;
	}
	
	public void setPostDescription(String postDescription){
		mPostDescription = postDescription;
	}*/
	
	public static ArrayList<String> jsonArrayToImagesStringArray(JSONArray jsonArray){
		ArrayList<String> images = new ArrayList<String>();
		
		try{
			if(jsonArray != null){
				for (int i = 0; i < jsonArray.length(); i++){ 
					//images.add(jsonArray.getJSONObject(i).getString("post_filename").replace("\\/", "/"));
					images.add(jsonArray.getString(i));
				} 
			}			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return images;
	}
}