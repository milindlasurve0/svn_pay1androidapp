package com.mindsarray.pay1.adapterhandler;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import android.content.Context;
import android.graphics.Color;
import android.net.ParseException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindsarray.pay1.AccountHistoryActivity;
import com.mindsarray.pay1.R;
import com.mindsarray.pay1.constant.Constants;

public class AccountHistoryAdapter extends BaseAdapter {
	Context mContext;
	LayoutInflater inflater;
	TextView textView_Perticular, textView_Account, textView_Opening,
			textView_Closing, textView_Time;
	ArrayList<HashMap<String, String>> data;
	ImageView imageOperator;

	public AccountHistoryAdapter(Context context,
			ArrayList<HashMap<String, String>> data) {
		this.mContext = context;
		this.data = data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		if (convertView == null) {
			inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.accounthistory_adapter, null);
		}

		if (position % 2 == 1) {
			view.setBackgroundColor(Color.WHITE);
		} else {

			view.setBackgroundColor(mContext.getResources().getColor(
					R.color.app_faint_theme_secondry));
		}
		view.setBackgroundColor(Color.WHITE);
		imageOperator = (ImageView) view.findViewById(R.id.imageOperator);
		textView_Perticular = (TextView) view
				.findViewById(R.id.textView_Perticular);
		textView_Account = (TextView) view.findViewById(R.id.textView_Account);
		textView_Opening = (TextView) view.findViewById(R.id.textView_Opening);
		textView_Closing = (TextView) view.findViewById(R.id.textView_Closing);
		textView_Time = (TextView) view.findViewById(R.id.textView_Time);

		try {

			HashMap<String, String> map = new HashMap<String, String>();
			map = data.get(position);
			String str = map.get(AccountHistoryActivity.PERTICULAR).toString()
					.trim();
			if ((str.equalsIgnoreCase("Recharge"))) {
				imageOperator
						.setImageResource(Constants.getBitmapById(
								Integer.parseInt(map
										.get(AccountHistoryActivity.REFID)),
								mContext));
				textView_Perticular.setVisibility(View.GONE);
				imageOperator.setVisibility(View.VISIBLE);
				if ((map.get(AccountHistoryActivity.CONFIRM_FLAG).toString()
						.trim().equalsIgnoreCase("0"))) {
					view.setBackgroundColor(mContext.getResources().getColor(
							R.color.Red));
				}
			} else {
				imageOperator.setVisibility(View.GONE);
				textView_Perticular.setVisibility(View.VISIBLE);
				textView_Perticular.setText(str);

				if ((map.get(AccountHistoryActivity.TYPE).toString().trim()
						.equalsIgnoreCase("21"))) {
					view.setBackgroundColor(mContext.getResources().getColor(
							R.color.Red));
				}
				if ((map.get(AccountHistoryActivity.TYPE).toString().trim()
						.equalsIgnoreCase("2") && map
						.get(AccountHistoryActivity.CONFIRM_FLAG).toString()
						.trim().equalsIgnoreCase("1"))) {
					view.setBackgroundColor(mContext.getResources().getColor(
							R.color.Red));
				}
			}

			SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
			String dt = map.get(AccountHistoryActivity.TIME).toString().trim()
					.replaceAll("-", "/");
			Date date = new Date(dt);// new Date(new Date());

			textView_Time.setText(dateFormat.format(date));

			double d = Double
					.parseDouble(map.get(AccountHistoryActivity.DEBIT));
			double c = Double.parseDouble(map
					.get(AccountHistoryActivity.CREDIT));

			DecimalFormat decimalFormat = new DecimalFormat("00.00");
			if (d == 0) {
				textView_Account.setText(decimalFormat.format(c) + " Cr");
			} else {
				textView_Account.setText(decimalFormat.format(d) + " Dr");
			}
			double op = 0, cl = 0;
			String o_ = map.get(AccountHistoryActivity.OPENING).toString()
					.trim();
			String c_ = map.get(AccountHistoryActivity.CLOSING).toString()
					.trim();
			if (!o_.equalsIgnoreCase("null") && !c_.equalsIgnoreCase("null")) {
				op = Double
						.parseDouble(map.get(AccountHistoryActivity.OPENING));
				cl = Double
						.parseDouble(map.get(AccountHistoryActivity.CLOSING));

				if (cl - op > 0) {
					if (d == 0) {
						textView_Account.setText(decimalFormat.format(c)
								+ " Cr");
					} else {
						textView_Account.setText(decimalFormat.format(d)
								+ " Cr");
					}
				} else {
					textView_Account.setText(decimalFormat.format(d) + " Dr");
				}
			} else {
				if ((position + 1) != data.size()) {
					HashMap<String, String> map1 = new HashMap<String, String>();
					map1 = data.get(position + 1);
					op = Double.parseDouble(map1
							.get(AccountHistoryActivity.OPENING));
					cl = Double.parseDouble(map1
							.get(AccountHistoryActivity.CLOSING));
				}
			}

			textView_Opening.setText("" + decimalFormat.format(op));
			textView_Closing.setText("" + decimalFormat.format(cl));

		} catch (ParseException e) {
			// e.printStackTrace();
		} catch (Exception exception) {
			// exception.printStackTrace();
		}

		return view;
	}
}
