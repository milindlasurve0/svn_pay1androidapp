package com.mindsarray.pay1;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mindsarray.pay1.SettingsDebugActivity.SendLogs;
import com.mindsarray.pay1.constant.Constants;
import com.mindsarray.pay1.requesthandler.RequestClass;
import com.mindsarray.pay1.utilities.Utility;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ChangeMobileNumberActivity extends Activity {
	EditText editNewMobileNumber, editPassword, editMobileNumber;
	Button mBtnChangeNumber;
	private static final String TAG = "Change  Mobile Number";
	Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.change_mobile_number);
		mContext = ChangeMobileNumberActivity.this;
		editNewMobileNumber = (EditText) findViewById(R.id.editNewMobileNumber);
		editPassword = (EditText) findViewById(R.id.editPassword);
		editMobileNumber = (EditText) findViewById(R.id.editMobileNumber);
		mBtnChangeNumber = (Button) findViewById(R.id.mBtnChangeNumber);

		mBtnChangeNumber.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (editMobileNumber.getText().toString().trim().length() != 10) {

					String errorMessage = "Please enter correct mobile number";

					int ecolor = R.color.black;
					ForegroundColorSpan fgcspan = new ForegroundColorSpan(
							ecolor);
					SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
							errorMessage);
					ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
					editMobileNumber.setError(ssbuilder);

					editMobileNumber.setFocusable(true);
					return;
				} else {
					if (!editMobileNumber
							.getText()
							.toString()
							.trim()
							.equalsIgnoreCase(
									Utility.getMobileNumber(
											mContext,
											Constants.SHAREDPREFERENCE_MOBILE_NUMBER))) {
						String errorMessage = "Please enter your registered mobile number";

						int ecolor = R.color.black;
						ForegroundColorSpan fgcspan = new ForegroundColorSpan(
								ecolor);
						SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
								errorMessage);
						ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
						editMobileNumber.setError(ssbuilder);

						editMobileNumber.setFocusable(true);
						return;
					} else {

					}
				}

				if (editNewMobileNumber.getText().toString().trim().length() != 10) {
					String errorMessage = "Please enter correct mobile number";

					int ecolor = R.color.black;
					ForegroundColorSpan fgcspan = new ForegroundColorSpan(
							ecolor);
					SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
							errorMessage);
					ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
					editNewMobileNumber.setError(ssbuilder);

					editNewMobileNumber.setFocusable(true);

					return;
				} else {

				}

				if (editPassword.getText().toString().trim().length() < 4) {

					String errorMessage = "Please enter correct password";

					int ecolor = R.color.black;
					ForegroundColorSpan fgcspan = new ForegroundColorSpan(
							ecolor);
					SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
							errorMessage);
					ssbuilder.setSpan(fgcspan, 0, errorMessage.length(), 0);
					editPassword.setError(ssbuilder);

					editPassword.setFocusable(true);
					return;
				} else {

				}

				if (editMobileNumber
						.getText()
						.toString()
						.trim()
						.equalsIgnoreCase(
								editNewMobileNumber.getText().toString().trim())) {
					Toast.makeText(mContext, "Both numbers should not be same",
							Toast.LENGTH_LONG).show();
					return;

				} else {

				}

				new ChangeMobileNumberTask().execute(editNewMobileNumber
						.getText().toString().trim(), editPassword.getText()
						.toString().trim(),editMobileNumber.getText().toString().trim());

			}
		});

	}

	public class ChangeMobileNumberTask extends
			AsyncTask<String, String, String> implements OnDismissListener {

		private ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listValuePair = new ArrayList<NameValuePair>();
			listValuePair.add(new BasicNameValuePair("method",
					"changeMobileNumber"));
			listValuePair.add(new BasicNameValuePair("oldNumber",params[2]
					));
			listValuePair.add(new BasicNameValuePair("newNumber", params[0]));
			listValuePair.add(new BasicNameValuePair("password", params[1]));

			String response = RequestClass.getInstance().readPay1Request(
					mContext, Constants.API_URL, listValuePair);

			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Log.e("post execute", result);
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}

			try {
				if (!result.startsWith("Error")) {
					// result = result.substring(result.indexOf("([{"),
					// result.lastIndexOf("}])") + 3);
					String replaced = result.replace("(", "").replace(")", "")
							.replace(";", "");

					JSONArray jsonArray = new JSONArray(replaced);
					JSONObject jsonObject = jsonArray.getJSONObject(0);
					String status = jsonObject.getString("status");
					if (status.equalsIgnoreCase("success")) {

						Utility.setLoginFlag(ChangeMobileNumberActivity.this,
								Constants.SHAREDPREFERENCE_IS_LOGIN, false);
						Utility.setBalance(ChangeMobileNumberActivity.this,
								Constants.SHAREDPREFERENCE_BALANCE, null);
						Utility.setUserName(ChangeMobileNumberActivity.this,
								Constants.SHAREDPREFERENCE_USER_NAME, null);
						Utility.setProfileID(ChangeMobileNumberActivity.this,
								Constants.SHAREDPREFERENCE_PROFILE_ID, null);
						Utility.setMobileNumber(ChangeMobileNumberActivity.this,
								Constants.SHAREDPREFERENCE_MOBILE_NUMBER, null);
						Utility.setCookieVersion(ChangeMobileNumberActivity.this,
								Constants.SHAREDPREFERENCE_COOKIE_VERSION, 0);
						Utility.setCookieName(ChangeMobileNumberActivity.this,
								Constants.SHAREDPREFERENCE_COOKIE_NAME_RESPONSE, null);
						Utility.setCookieValue(ChangeMobileNumberActivity.this,
								Constants.SHAREDPREFERENCE_COOKIE_VALUE_RESPONSE, null);
						Utility.setCookieDomain(ChangeMobileNumberActivity.this,
								Constants.SHAREDPREFERENCE_COOKIE_DOMAIN, null);
						Utility.setCookiePath(ChangeMobileNumberActivity.this,
								Constants.SHAREDPREFERENCE_COOKIE_PATH, null);
						Utility.setCookieExpiry(ChangeMobileNumberActivity.this,
								Constants.SHAREDPREFERENCE_COOKIE_EXPIRY, null);
						Utility.setCookieObject(ChangeMobileNumberActivity.this, null);
						Utility.setSharedPreference(ChangeMobileNumberActivity.this,
								Constants.SHAREDPREFERENCE_DEVICE_SERIAL_NO,
								"");

						Utility.setSharedPreference(ChangeMobileNumberActivity.this,
								Constants.SHAREDPREFERENCE_RETAILER_ID,
								"");
						Utility.setSharedPreference(ChangeMobileNumberActivity.this, Constants.SHAREDPREFERENCE_LAST_5_TRANSACTIONS, "");
						Utility.setSharedPreference(ChangeMobileNumberActivity.this, Constants.SHAREDPREFERENCE_COMPLAINT_STATS, "");
						Intent intent=new Intent(ChangeMobileNumberActivity.this,
								MainActivity.class);
						intent.addCategory(Intent.CATEGORY_HOME);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
								| Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);

						finish();




						startActivity(new Intent(mContext,
								OTPVerificationNumber.class));
					} else {
						Constants.showOneButtonFailureDialog(mContext,
								jsonObject.getString("description"), TAG,
								Constants.OTHER_ERROR);
					}

				} else {
					Constants.showOneButtonFailureDialog(mContext,
							Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
							Constants.OTHER_ERROR);
				}
			} catch (JSONException e) {
				e.printStackTrace();
				Constants.showOneButtonFailureDialog(mContext,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			} catch (Exception e) {
				e.printStackTrace();
				Constants.showOneButtonFailureDialog(mContext,
						Constants.ERROR_MESSAGE_FOR_ALL_REQUEST, TAG,
						Constants.OTHER_ERROR);
			}
		}

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(mContext);
			this.dialog.setMessage("Please wait.....");
			this.dialog.setCancelable(true);
			this.dialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							ChangeMobileNumberTask.this.cancel(true);
						}
					});
			this.dialog.show();
			super.onPreExecute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			ChangeMobileNumberTask.this.cancel(true);
			dialog.cancel();
		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (Constants.isExpanded) {
			Constants.collapsView();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Constants.showNavigationBar(ChangeMobileNumberActivity.this);

		super.onResume();
	}
}
