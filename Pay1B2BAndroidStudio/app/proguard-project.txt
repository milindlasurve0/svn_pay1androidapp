# To enable ProGuard in your project, edit project.properties
# to define the proguard.config property as described in that file.
#
# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in ${sdk.dir}/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the ProGuard
# include property in project.properties.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}








-dontwarn javax.jdo.**
-dontwarn com.google.api.client.googleapis.extensions.android.gms.**
-dontwarn org.apache.xmpp.**
-dontwarn org.jivesoftware.**
-dontwarn com.google.android.** 
-dontwarn com.google.analytics.** 
-dontwarn com.viewpagerindicator.** 
-dontwarn com.google.tagmanager.** 
-dontwarn com.squareup.okhttp.**
-dontwarn com.eze.api.**
-dontskipnonpubliclibraryclassmembers

-keepattributes Signature
 
# For using GSON @Expose annotation
-keepattributes *Annotation*
 
# Gson specific classes
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }
-keep class com.google.gson.FieldNamingStrategy { *; }
 
# Application classes that will be serialized/deserialized over Gson
-keep class com.mindsarray.pay1.requesthandler { *; }

-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

-dontwarn org.apache.http.**  
-keep class org.apache.http.** { *;}  
